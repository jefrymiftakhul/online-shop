<?php

return array(
    "CIMBC" => "CIMB Clicks",
    "DANAMON" => "Danamon Online",
    "BCAKP" => "BCA KlikPay",
    "BCAPC" => "BCA Payment Code",
    "BRIMC" => "BRI Mocash",
    "ATM137" => "Transfer melalui ATM Bersama",
    "BRIEP" => "BRI Epay",
    "MANDIRIPC" => "Mandiri Payment Code",
    "MANDIRICP" => "Mandiri ClickPay",
    "TCASH" => "Telkomsel Cash",
    "VMCARD" => "Visa/Master Card",
    "XLTUNAI" => "XL Tunai",
    "DOMPETKU" => "Indosat Dompetku",
    "MANDIRIEC" => "Mandiri ECash",
    "FINPAY" => "Finpay",
    "BEBASBAYAR" => "BebasBayar",
);
    