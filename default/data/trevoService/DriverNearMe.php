<?php

    /**
     *
     * @author Khumbaka
     * @since  Oct 31, 2016
     */
    return array(
        'name' => 'DriverNearMe (Trevo Member)',
        'description' => '',
        'important_info' => '',
        'note' => '',
        'input' => array(
            'session_id' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'latitude' => array(
                'data_type' => 'double',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'longitude' => array(
                'data_type' => 'double',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
        ),
        'output' => array(
            'err_code' => array(
                'data_type' => 'integer',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'err_message' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'data' => array(
                'list_driver' => array(
                    'driver_id' => array(
                        'data_type' => 'bigint',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),                    
                    'name' => array(
                        'data_type' => 'string',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                    'vechicle_type' => array(
                        'data_type' => 'string',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                    'latitude' => array(
                        'data_type' => 'double',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                    'longitude' => array(
                        'data_type' => 'double',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                ),
            ),
        ),
    );
    