<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 17, 2016
     */
    return array(
        'Name' => 'SubmitRating (Trevo Member)',
        'description' => '',
        'important_info' => '',
        'note' => '',
        'input' => array(
            'session_id' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'transaction_id' => array(
                'data_type' => 'bigint',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'driver_id' => array(
                'data_type' => 'bigint',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'review_rating' => array(
                'data_type' => 'bigint',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'review_comment' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
        ),
        'output' => array(
            'err_code' => array(
                'data_type' => 'integer',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'err_message' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'data' => array(
                'transaction_id' => array(
                    'data_type' => 'bigint',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'driver_id' => array(
                    'data_type' => 'bigint',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'driver_name' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'review_rating' => array(
                    'data_type' => 'bigint',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'review_comment' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'created_date' => array(
                    'data_type' => 'datetime',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),                
            ),
        ),
    );
    