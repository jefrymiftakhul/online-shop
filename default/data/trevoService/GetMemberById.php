<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 11, 2016
     */
    return array(
        'name' => 'GetMemberById (Trevo Member)',
        'description' => '',
        'important_info' => '',
        'note' => '',
        'input' => array(
            'session_id' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'member_id' => array(
                'data_type' => 'bigint',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),            
        ),
        'output' => array(
            'err_code' => array(
                'data_type' => 'integer',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'err_message' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'data' => array(
                'member_id' => array(
                    'data_type' => 'bigint',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'trevo_device_id' => array(
                    'data_type' => 'bigint',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'trevo_phone_number' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'email' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'name' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'gender' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'date_of_birth' => array(
                    'data_type' => 'datetime',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'address' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'phone' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'subscribe_product' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'subscribe_service' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'subscribe_gold' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'member_address' => array(
                    'billing' => array(
                        'id' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'name' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'phone' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'address' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'postal' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'email' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'country' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'province' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'city' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'districts' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                    ),
                ),                
            ),
        ),
    );
    