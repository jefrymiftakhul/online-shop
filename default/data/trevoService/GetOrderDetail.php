<?php

    /**
     *
     * @author Delvo
     * @since  Nov 03, 2016
     */
    return array(
        'Name' => 'GetOrderDetail (Trevo)',
        'description' => '',
        'important_info' => '',
        'note' => '',
        'input' => array(
            'session_id' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'transaction_id' => array(
                'data_type' => 'bigint',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
        ),
        'output' => array(
            'err_code' => array(
                'data_type' => 'integer',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'err_message' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'data' => array(
                'transaction_id' => array(
                    'data_type' => 'bigint',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'member_id' => array(
                    'data_type' => 'bigint',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'driver_id' => array(
                    'data_type' => 'bigint',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'total_price' => array(
                    'data_type' => 'double',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'created_date' => array(
                    'data_type' => 'datetime',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'pickup_time' => array(
                    'data_type' => 'datetime',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'pickup_location' => array(
                    'address' => array(
                        'data_type' => 'string',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                    'latitude' => array(
                        'data_type' => 'double',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                    'longitude' => array(
                        'data_type' => 'double',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                ),
                'arrival_time' => array(
                    'data_type' => 'datetime',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'destination_location' => array(
                    'address' => array(
                        'data_type' => 'string',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                    'latitude' => array(
                        'data_type' => 'double',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                    'longitude' => array(
                        'data_type' => 'double',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                ),
                'passenger_detail' => array(
                    'name' => array(
                        'data_type' => 'string',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                    'phone_number' => array(
                        'data_type' => 'string',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                ),
            ),
        ),
    );
    