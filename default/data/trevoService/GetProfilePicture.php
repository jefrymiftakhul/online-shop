<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 14, 2016
     */
    return array(
        'Name' => 'GetProfilePicture (Trevo Member)',
        'description' => '',
        'important_info' => '',
        'note' => '',
        'input' => array(
            'session_id' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'member_id' => array(
                'data_type' => 'bigint',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),            
        ),
        'output' => array(
            'err_code' => array(
                'data_type' => 'integer',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'err_message' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'data' => array(
                'member_id' => array(
                    'data_type' => 'bigint',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'picture_url' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),                
            ),
        ),
    );
    