<?php

    /**
     *
     * @author Delvo
     * @since  Oct 04, 2016
     */
    return array(
        'name' => 'GetReviewTypeOrder (Trevo Member)',
        'description' => '',
        'important_info' => '',
        'note' => '',
        'input' => array(
            'session_id' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ), 
        ),
        'output' => array(
            'err_code' => array(
                'data_type' => 'integer',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'err_message' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'data' => array(
                'review_list' => array(
                    'data_type' => 'array',
                    'rules' => array('required'),
                    'description' => 'Array Jenis Review',
                    'default' => '',
                    'mandatory' => '',
                ),        
            ),
        ),
    );
    