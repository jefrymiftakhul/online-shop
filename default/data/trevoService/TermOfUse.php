<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 18, 2016
     */
    return array(
        'name' => 'TermOfUse (Trevo Driver)',
        'description' => '',
        'important_info' => '',
        'note' => '',
        'input' => array(
            'session_id' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
        ),
        'output' => array(
            'err_code' => array(
                'data_type' => 'integer',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'err_message' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'data' => array(
                'term_of_use' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
            ),
        ),
    );
    