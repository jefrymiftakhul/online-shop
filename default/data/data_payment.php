<?php

    return array(
        'bank_transfer' => array(
            'name' => 'Bank Transfer',
            'icon' => 'os-bank-transfer',
            'product_code' => 'IT',
            'have_detail_bank' => true,
            'useful_info' => file_get_contents(dirname(__FILE__) . "/dataPayment/bank-transfer" . EXT),
            'detail_bank' => array(
                'bca' => array(
                    'icon' => 'it-bca',
                    'code' => 'bca',
                    'name' => 'BCA',
                    'account_number' => '107-056-9333',
                    'account_holder' => 'PT. Indonesia 62',
//                    'useful_info' => file_get_contents(dirname(__FILE__) . "/dataPayment/bank-transfer-bca" . EXT),
                ),
                'mandiri' => array(
                    'icon' => 'it-mandiri',
                    'code' => 'mandiri',
                    'name' => 'MANDIRI',
                    'account_number' => '140-00-5562626-2',
                    'account_holder' => 'PT. Indonesia 62',
                ),
                'bri' => array(
                    'icon' => 'it-bri',
                    'code' => 'bri',
                    'name' => 'BRI',
                    'account_number' => '0096-01-003233-30-8',
                    'account_holder' => 'PT. Indonesia 62',
                ),
                'bukopin' => array(
                    'icon' => 'it-bukopin',
                    'code' => 'bukopin',
                    'name' => 'BUKOPIN',
                    'account_number' => '111-120-0224',
                    'account_holder' => 'PT. Indonesia 62',
                ),
                'bii' => array(
                    'icon' => 'it-bii',
                    'code' => 'bii',
                    'name' => 'BII',
                    'account_number' => '2-200-009-188',
                    'account_holder' => 'PT. Indonesia 62',
                ),
                'cimb_niaga' => array(
                    'icon' => 'it-cimb-niaga',
                    'code' => 'cimb_niaga',
                    'name' => 'CIMB NIAGA',
                    'account_number' => '800134631300',
                    'account_holder' => 'PT. Indonesia 62',
                ),
                'danamon' => array(
                    'icon' => 'it-danamon',
                    'code' => 'danamon',
                    'name' => 'DANAMON',
                    'account_number' => '359-602-1380',
                    'account_holder' => 'PT. Indonesia 62',
                ),
                'permata' => array(
                    'icon' => 'it-permata',
                    'code' => 'permata',
                    'name' => 'PERMATA',
                    'account_number' => '701734750',
                    'account_holder' => 'PT. Indonesia 62',
                ),
                'uob' => array(
                    'icon' => 'it-uob',
                    'code' => 'uob',
                    'name' => 'UOB',
                    'account_number' => '426-300-2053',
                    'account_holder' => 'PT. Indonesia 62',
                ),
            )
        ),
        'credit_card' => array(
            'icon' => 'it-visa-mastercard',
            'code' => 'mastervisa',
            'name' => 'Kartu Kredit',
            'product_code' => 'VT',
            'request_api' => true,
            'useful_info' => file_get_contents(dirname(__FILE__) . "/dataPayment/cc-visa" . EXT),
        ),
        'mandiri_ib' => array(
            'icon' => 'it-mandiri-ib',
            'code' => 'mandiri_ib',
            'product_code' => 'SG',
            'request_api' => true,
            'useful_info' => file_get_contents(dirname(__FILE__) . "/dataPayment/mandiri-ib" . EXT),
        ),
//        'internet_banking' => array(
//            'icon' => 'os-internet-banking',
//            'detail' => array(
//                'mandiri_ib' => array(
//                    'code' => 'mandiri_ib',
//                    'name' => 'MANDIRI IBANKING',
//                ),
//                'bca_klik_pay' => array(
//                    'code' => 'bca_klik_pay',
//                    'name' => 'BCA KLIK PAY',
//                ),
//            )
//        ),
        'paypal' => array(
            'icon' => 'it-paypal',
            'code' => 'paypal',
            'product_code' => 'PP',
            'request_api' => true,
        ),
    );
    