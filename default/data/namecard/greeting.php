<?php
return array(
    'title' => 'Mengapa Kartu Nama Digital?',
    'content_type' => 'ordered', // list / none / biarkan kosong
    'content' => array(
        'Pengguna aplikasi mobile Android di Indonesia bertambah secara signifikan dari tahun ke tahun',
        'Saat ini orang sudah sangat familiar dengan mobile apps',
        'Kartu Nama Digital tidak bisa hilang atau terselip, karena terinstall di handphone. Seandainya hilang bisa langsung install kembali',
        'Informasi lebih lengkap, detail, disertai dengan gambar, dan yang paling penting adalah up to date (Nomor HP atau email atau alamat dapat berubah langsung diketahui kerabat)',
        'Bisa berinteraksi lebih dekat dengan kerabat karena dilengkapi fitur News dan Contact Us',
        'Meningkatkan level atau standarisasi dari pemilik Kartu Nama Digital tersebut',
    )
);

