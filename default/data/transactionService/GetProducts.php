<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Mar 31, 2016
     */
    return array(
        'description' => '',
        'important_info' => '',
        'note' => '',
        'input' => array(
            'session_id' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'product_type' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
        ),
        'output' => array(
            'err_code' => array(
                'data_type' => 'integer',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'err_message' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'data' => array(
                'product_total' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'filter' => array(
                    'order_by' => array(
                        'diskon' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),  
                        'min_price' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),  
                        'max_price' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),  
                        'is_new' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),  
                        'popular' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),  
                        'A-Z' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),  
                        'Z-A' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),  
                    ),
                    'range_price' => array(
                        'min' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),  
                        'max' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),  
                    ),
                    'attribute' => array(
                        'code' => array(
                            'attribute_category_id' => array(
                                'data_type' => 'string',
                                'rules' => array('required'),
                                'description' => '',
                                'default' => '',
                                'mandatory' => '',
                            ),  
                            'attribute_category_name' => array(
                                'data_type' => 'string',
                                'rules' => array('required'),
                                'description' => '',
                                'default' => '',
                                'mandatory' => '',
                            ), 
                            'attribute_category_type' => array(
                                'data_type' => 'string',
                                'rules' => array('required'),
                                'description' => '',
                                'default' => '',
                                'mandatory' => '',
                            ), 
                            'attribute_category_url_key' => array(
                                'data_type' => 'string',
                                'rules' => array('required'),
                                'description' => '',
                                'default' => '',
                                'mandatory' => '',
                            ), 
                            'attribute' => array(
                                'attribute_key' => array(
                                    'data_type' => 'string',
                                    'rules' => array('required'),
                                    'description' => '',
                                    'default' => '',
                                    'mandatory' => '',
                                ),  
                                'attribute_url_key' => array(
                                    'data_type' => 'string',
                                    'rules' => array('required'),
                                    'description' => '',
                                    'default' => '',
                                    'mandatory' => '',
                                ), 
                                'file_path' => array(
                                    'data_type' => 'string',
                                    'rules' => array('required'),
                                    'description' => '',
                                    'default' => '',
                                    'mandatory' => '',
                                ), 
                            ),
                        ),
                    ),
                ),
            ),
        ),
    );
    