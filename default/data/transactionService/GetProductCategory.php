<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Mar 31, 2016
     */
    return array(
        'description' => '',
        'important_info' => '',
        'note' => '',
        'input' => array(
            'session_id' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'product_type' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
        ),
        'output' => array(
            'err_code' => array(
                'data_type' => 'integer',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'err_message' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'data' => array(
                'category_id' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'url_key' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'code' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'name' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),                
                'subnav' => array(
                    'category_id' => array(
                        'data_type' => 'string',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                    'url_key' => array(
                        'data_type' => 'string',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                    'code' => array(
                        'data_type' => 'string',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                    'name' => array(
                        'data_type' => 'string',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                    'icon' => array(
                        'data_type' => 'string',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),                    
                    'subnav' => array(
                        'category_id' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'url_key' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'code' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'name' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'icon' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),                 
                    ),
                ),
            ),
        ),
    );
    