<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Mar 31, 2016
     */
    return array(
        'description' => '',
        'important_info' => '',
        'note' => '',
        'input' => array(
            'session_id' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'product_type' => array(
                'data_type' => 'integer',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
        ),
        'output' => array(
            'err_code' => array(
                'data_type' => 'integer',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'err_message' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'data' => array(
                'products_deals' => array(
                    'product_type' => array(
                        'data_type' => 'string',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                    'products_deal_type' => array(
                        'data_type' => 'string',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                    'products_deal_name' => array(
                        'data_type' => 'string',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                    'products' => array(
                        'product_id' => array(
                            'data_type' => 'integer',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'url_key' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'product_code' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'product_name' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'product_sku' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'weight' => array(
                            'data_type' => 'integer',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'image_url' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'flag' => array(
                            'hot' => array(
                                'data_type' => 'integer',
                                'rules' => array('required'),
                                'description' => '',
                                'default' => '',
                                'mandatory' => '',
                            ),
                            'new' => array(
                                'data_type' => 'integer',
                                'rules' => array('required'),
                                'description' => '',
                                'default' => '',
                                'mandatory' => '',
                            ),
                        ),
                        'is_available' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'quick_overview' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'stock' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'show_minimum_stock' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'overview' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'spesification' => array(
                            'data_type' => 'integer',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'faq' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'detail_price' => array(
                            'sell_price' => array(
                                'data_type' => 'integer',
                                'rules' => array('required'),
                                'description' => '',
                                'default' => '',
                                'mandatory' => '',
                            ),
                            'sell_price_promo' => array(
                                'data_type' => 'integer',
                                'rules' => array('required'),
                                'description' => '',
                                'default' => '',
                                'mandatory' => '',
                            ),
                            'promo_text' => array(
                                'data_type' => 'integer',
                                'rules' => array('required'),
                                'description' => '',
                                'default' => '',
                                'mandatory' => '',
                            ),
                            'promo_type' => array(
                                'data_type' => 'string',
                                'rules' => array('required'),
                                'description' => '',
                                'default' => '',
                                'mandatory' => '',
                            ),
                        ),
                    ),
                ),
            ),  
        ),
    );
    