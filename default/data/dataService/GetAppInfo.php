<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Mar 31, 2016
     */
    return array(
        'description' => '',
        'important_info' => '',
        'note' => '',
        'input' => array(
            'session_id' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
        ),
        'output' => array(
            'err_code' => array(
                'data_type' => 'integer',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'err_message' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'data' => array(
                'org_code' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'org_name' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => ''
                ),
                'item_favicon' => array(
                    'data_type' => 'integer',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'item_image' => array(
                    'data_type' => 'integer',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'have_product' => array(
                    'data_type' => 'integer',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'have_service' => array(
                    'data_type' => 'integer',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'have_gold' => array(
                    'data_type' => 'integer',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'have_zopim' => array(
                    'data_type' => 'integer',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'have_register' => array(
                    'data_type' => 'integer',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'have_deposit' => array(
                    'data_type' => 'integer',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'have_topup_transfer' => array(
                    'data_type' => 'integer',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'have_member_get_member' => array(
                    'data_type' => 'integer',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),  
                'list_product_type' => array(
                    'product' => array(
                        'data_type' => 'string',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                    'service' => array(
                        'data_type' => 'string',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                    'gold' => array(
                        'data_type' => 'string',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                ),  
                'list_payment_type'=>array(
                    'bank_transfer' => array(
                        'bank_transfer' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                    ),
                    'credit_card' => array(
                        'visa_master' => array(
                            'data_type' => 'string',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                    ),
                    'internet_banking' =>array(
                        'permata_va' => array(
                            'data_type' => 'integer',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'mandiri_ib' => array(
                            'data_type' => 'integer',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'paypal' => array(
                            'data_type' => 'integer',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                        'bca_klik_pay' => array(
                            'data_type' => 'integer',
                            'rules' => array('required'),
                            'description' => '',
                            'default' => '',
                            'mandatory' => '',
                        ),
                    ),
                ),
            ),
        ),
    );
    