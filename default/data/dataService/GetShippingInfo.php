<?php
   
    return array(
        'description' => '',
        'important_info' => '',
        'note' => '',
        'input' => array(
            'session_id' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ), 
            'product_type' => array(
                'data_type' => 'string',
                'rules' => array('optional'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'district' => array(
                'data_type' => 'string',
                'rules' => array('optional'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
        ),
        'output' => array(
            'err_code' => array(
                'data_type' => 'integer',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'err_message' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'data' => array(
                'shipping_info' => array(
                    'shipping_type' => array(
                        'data_type' => 'string',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                    'price' => array(
                        'data_type' => 'string',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                ),
            ),
        ),
    );
    