<?php

    return array(
        0 => "SUCCESS",
        // General Error
        1000 => "Unknown Error",
        1001 => "Arguments is not valid.",
        1002 => "Arguments[0] is not valid.",
        1003 => "Engine name is empty.",
        1004 => "Service name unavailable",
        1005 => "Auth id is not valid",
        1006 => "IP Address is not registered",
        1007 => "SessionID is required",
        1008 => "Session Unknown. Please contact our administrator",
		1009 => "Merchant Not Found",
        // <editor-fold defaultstate="collapsed" desc="MEMBER">

        /* LOGIN */
        'DATERR10001' => "Gagal Login. Kesalahan dalam email atau password",
        10001 => "Gagal Login. Kesalahan dalam email atau password",
        'DATERR10002' => "Gagal Login. Email anda tidak pernah ter-verifikasi",
        10002 => "Gagal Login. Email anda tidak pernah ter-verifikasi",
        /* REGISTER */
        'DATERR10010' => "Gagal Melakukan Register. Mohon maaf, email sudah digunakan.",
        10010 => "Gagal Melakukan Register. Mohon maaf, email sudah digunakan.",
        'DATERR10011' => "Gagal Melakukan Register. Mohon maaf, Password tidak cocok dengan yang ada di konfirmasi.",
        10011 => "Gagal Melakukan Register. Mohon maaf, Password tidak cocok dengan yang ada di konfirmasi.",
        'DATERR10012' => "Gagal Melakukan Register. DB[ERR-REG] Tolong hubungi administrator kami.",
        10012 => "Gagal Melakukan Register. DB[ERR-REG] Tolong hubungi administrator kami.",
        'DATERR10013' => "Gagal Melakukan Register. Name tidak dapat kosong.",
        10013 => "Gagal Melakukan Register. Name tidak dapat kosong.",
        'DATERR10014' => "Gagal Melakukan Register. Email tidak dapat kosong.",
        10014 => "Gagal Melakukan Register. Email tidak dapat kosong.",
        'DATERR10015' => "Gagal Melakukan Register. Password password tidak dapat kosong.",       
        10015 => "Gagal Melakukan Register. Password password tidak dapat kosong.",       
        10016 => "Gagal Melakukan Register. Mohon maaf, email sudah digunakan dan belum ter-verifikasi.",
        // </editor-fold>
        // Transaction
        // Sell
        1201 => "No Item for Transaction",
        1202 => "Item Data not Found",
        1203 => "Item Price not Match",
        1204 => "Item Stock not Enough",
        //PG
        1301 => "Failed Login PG",
        1302 => "Failed To Get Payment Charge",
        1303 => "Payment Type not Defined",
        1304 => "Payment Type Code not Defined",
        1305 => "Failed To Payment",
        1306 => "Failed To Confirm",
        //retrieve
        1401 => "Order Code Null",
        1402 => "Email Null",
        //TREVO
        2000 => "Error DB",
        2001 => "Member Not Exist",        
        2002 => "Driver History Not Found",
        2003 => "Driver Not Exist",
        2004 => "Order Not Exist",
        2005 => "Device Not Exist",
        2006 => "Wrong Device",
        2007 => "Phone Number Already Added",
        2008 => "Please set pick to true",
        2009 => "Balance Not Enough",
        2010 => "Detect New Version",
        2011 => "Login Failed. Your email has not been verified",
        2012 => "Invalid User Or Password",
        2013 => "There is no one driver near you",
        2014 => "No Image Found",
        2015 => "Error push notif",
        2016 => "Error curl notif",
        2017 => "Error, Driver already pick order",
        2018 => "Privacy Policy Not Found",
        2019 => "Term Of Use Not Found",
        2020 => "You cannot order twice while your order have been picked",
        2021 => "Email Not Register",
        2999 => "",
        
        //ITTRONMALL
        3000 => "CMS Slide Not found",
        3001 => "File Data Payment Type Not Available",
        3002 => "error on check price",
    );
    