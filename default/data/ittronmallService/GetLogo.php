<?php

    return array(
        'name' => 'Register (IttronMall)',
        'description' => '',
        'important_info' => '',
        'note' => '',
        'input' => array(
            'session_id' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'org_id' => array(
                'data_type' => 'integer',
                'rules' => array(),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
        ),
        'output' => array(
            'err_code' => array(
                'data_type' => 'integer',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'err_message' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'data' => array(
                'drawer_background_without_logo' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'drawer_background_with_logo' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'top_logo' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'top_logo_colored' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
            ),
        ),
    );
    