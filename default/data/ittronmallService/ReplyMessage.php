<?php

    return array(
        'name' => 'GetMessageDetail (IttronMall)',
        'description' => '',
        'important_info' => '',
        'note' => '',
        'input' => array(
            'session_id' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
//            'member_id' => array(
//                'data_type' => 'integer',
//                'rules' => array('required'),
//                'description' => '',
//                'default' => '',
//                'mandatory' => '',
//            ),
            'message_id' => array(
                'data_type' => 'string',
                'rules' => array(),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'from' => array(
                'data_type' => 'string',
                'rules' => array(),
                'description' => 'department_id/member_id',
                'default' => '',
                'mandatory' => '',
            ),
            'to' => array(
                'data_type' => 'string',
                'rules' => array(),
                'description' => 'department_id/member_id',
                'default' => '',
                'mandatory' => '',
            ),
            'subject' => array(
                'data_type' => 'string',
                'rules' => array(),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'body' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'action_to' => array(
                'data_type' => 'string',
                'rules' => array(),
                'description' => 'member/department, default=department',
                'default' => 'department',
                'mandatory' => '',
            ),
        ),
        'output' => array(
            'err_code' => array(
                'data_type' => 'integer',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'err_message' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'data' => array(
                'app_id' => array(
                    'data_type' => 'integer',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
            ),
        ),
    );
    