<?php

    return array(
        'name' => 'GetProfile (IttronMall)',
        'description' => '',
        'important_info' => '',
        'note' => '',
        'input' => array(
            'session_id' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'app_id' => array(
                'data_type' => 'integer',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'start_date' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'end_date' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
        ),
        'output' => array(
            'err_code' => array(
                'data_type' => 'integer',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'err_message' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'data' => array(
                'app_id' => array(
                    'data_type' => 'integer',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'app_name' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'transactions ' => array(
                    'transaction_status' => array(
                        'data_type' => 'string',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
//                    'product_category' => array(
//                        'data_type' => 'string',
//                        'rules' => array('required'),
//                        'description' => '',
//                        'default' => '',
//                        'mandatory' => '',
//                    ),
//                    'product_name' => array(
//                        'data_type' => 'string',
//                        'rules' => array('required'),
//                        'description' => '',
//                        'default' => '',
//                        'mandatory' => '',
//                    ),
//                    'sku' => array(
//                        'data_type' => 'string',
//                        'rules' => array('required'),
//                        'description' => '',
//                        'default' => '',
//                        'mandatory' => '',
//                    ),
                    'nta' => array(
                        'data_type' => 'integer',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                    'sell_price' => array(
                        'data_type' => 'integer',
                        'rules' => array('required'),
                        'description' => '',
                        'default' => '',
                        'mandatory' => '',
                    ),
                ),
            ),
        ),
    );
    