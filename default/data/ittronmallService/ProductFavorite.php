<?php
    return array(
        'name' => 'Product Review (IttronMall)',
        'description' => '',
        'important_info' => '',
        'note' => '',
        'input' => array(
            'session_id' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'member_id' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'product_id' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            )
        ),
        'output' => array(
            'err_code' => array(
                'data_type' => 'integer',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'err_message' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
        ),
    );
    