<?php

    return array(
        'name' => 'Register (IttronMall)',
        'description' => '',
        'important_info' => '',
        'note' => '',
        'input' => array(
            'session_id' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'parent_id' => array(
                'data_type' => 'integer',
                'rules' => array(),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'name' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => 'Store name',
                'default' => '',
                'mandatory' => '',
            ),
            'username' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => 'Username for login admin area.',
                'default' => '',
                'mandatory' => '',
            ),
            'password' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => 'Password for login admin area.',
                'default' => '',
                'mandatory' => '',
            ),
            'domain' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
        ),
        'output' => array(
            'err_code' => array(
                'data_type' => 'integer',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'err_message' => array(
                'data_type' => 'string',
                'rules' => array('required'),
                'description' => '',
                'default' => '',
                'mandatory' => '',
            ),
            'data' => array(
                'app_id' => array(
                    'data_type' => 'integer',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'username' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
                'password' => array(
                    'data_type' => 'string',
                    'rules' => array('required'),
                    'description' => '',
                    'default' => '',
                    'mandatory' => '',
                ),
            ),
        ),
    );
    