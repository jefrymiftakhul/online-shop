<?php

/**
 *
 * @author Riza 
 * @since  Nov 12, 2015
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class Reload_Controller extends SixtyTwoHallFamilyController {

    const __CONTROLLER = 'reload/';
    const __is_test = true;

    public function __construct() {
        parent::__construct();
    }

    public function reload_gold_weight() {
        $app = CApp::instance();
        $category_id = carr::get($_GET, 'category');
        $gold_pecahan = array();
        if ($category_id) {
            $gold_pecahan = product::get_weight_gold($category_id);
        }
        $html = '';
        $html .= "<select id='gold_weight' name='gold_weight' class='select-62hallfamily'>";
        $html .= "<option value='' disabled selected hidden></option>";
        if (count($gold_pecahan) > 0) {
            foreach ($gold_pecahan as $weight) {
                $html .= "<option value='" . $weight . "'>" . $weight . " gr</option>";
            }
        }
        $html .= "</select>";
        $app->add($html);
        echo $app->render();
    }

    public function reload_member_address() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $member_address_id = carr::get($_GET, 'member_address_payment');
        if ($member_address_id == null) {
            $member_address_id = carr::get($_GET, 'member_address_shipping');
        }
        $q = "
			select
				ma.type as type,
				ma.name as name,
				ma.phone as phone,
				ma.address as address,
				ma.postal as postal,
				ma.email as email,
				p.province_id as province_id,
				p.name as province,
				c.city_id as city_id,
				c.name as city,
				d.districts_id as districts_id,
				d.name as districts
			from
				member_address as ma
				inner join province as p on p.province_id=ma.province_id
				inner join city as c on c.city_id=ma.city_id
				inner join districts as d on d.districts_id=ma.districts_id
			where
				member_address_id=" . $db->escape($member_address_id) . "
		";
        $r = cdbutils::get_row($q);
        if ($r) {
            $data = array();
            $data['type'] = $r->type;
            $data['name'] = $r->name;
            $data['phone'] = $r->phone;
            $data['address'] = $r->address;
            $data['postal'] = $r->postal;
            $data['email'] = $r->email;
            $data['province_id'] = $r->province_id;
            $data['province'] = $r->province;
            $data['city_id'] = $r->city_id;
            $data['city'] = $r->city;
            $data['districts_id'] = $r->districts_id;
            $data['districts'] = $r->districts;
            $json = json_encode($data);
            $app->add_js("reload_member_address(" . $json . ");");
        }
        echo $app->render();
    }

    public function reload_province() {
        $country_id = carr::get($get, 'country_pay');
        $from = 'pay';
        if ($country_id == null) {
            $country_id = carr::get($get, 'country_shipping');
            $from = 'shipping';
        }
        $control_province_select = $app->add_field()->set_label('Propinsi <red>*</red>')->add_control('province_' . $from, 'province-select')->add_class('width-40-percent');
        $control_province_select->set_all(false);
        $control_province_select->set_country_id($country_id);
        $listener_province_select = $control_province_select->add_listener('change');
        $listener_province_select
                ->add_handler('reload')
                ->set_target('div_province_' . $from)
                ->set_url(curl::base() . self::__CONTROLLER . 'reload_city')
                ->add_param_input(array('province_' . $from));
        $listener_province_select = $control_province_select->add_listener('ready');
        $listener_province_select
                ->add_handler('reload')
                ->set_target('div_province_' . $from)
                ->set_url(curl::base() . self::__CONTROLLER . 'reload_city')
                ->add_param_input(array('province_' . $from));
        echo $app->render();
    }

    public function reload_city($city_id = '') {
        $app = CApp::instance();
        $get = $_GET;
        $province_id = carr::get($get, 'province_pay');
        $page = carr::get($get, 'page');
        $raja_ongkir = carr::get($get, 'raja_ongkir');
        $from = 'pay';
        $addition_param = '';

        if ($province_id == null) {
            $province_id = carr::get($get, 'province_shipping');
            $from = 'shipping';
        }
        $control_city_select = $app->add_field()->set_label('Kota <red>*</red>')->add_control('city_' . $from, 'city-select')->add_class('width-40-percent');
        $control_city_select->set_all(false);
        $control_city_select->set_province_id($province_id);
        if ($from == 'shipping') {
            if ($page == 'gold') {
                $addition_param = '&page=' . $page;
                $control_city_select->set_page($page);
            }
        }
        if (strlen($city_id) > 0) {
            $control_city_select->set_value($city_id);
        }
        $listener_change_city_select = $control_city_select->add_listener('change');
        $listener_change_city_select
                ->add_handler('reload')
                ->set_target('div_districts_' . $from)
                ->set_url(curl::base() . self::__CONTROLLER . 'reload_districts?' . $addition_param)
                ->add_param_input(array('city_' . $from));
        $listener_ready_city_select = $control_city_select->add_listener('ready');
        $listener_ready_city_select
                ->add_handler('reload')
                ->set_target('div_districts_' . $from)
                ->set_url(curl::base() . self::__CONTROLLER . 'reload_districts?' . $addition_param)
                ->add_param_input(array('city_' . $from));
		if($raja_ongkir==1 && $from =='shipping'){
			$listener_change_city_select
                ->add_handler('reload')
                ->set_target('service_reload')
                ->set_url(curl::base() . 'raja_ongkir/get_cost')
                ->add_param_input(array('service_type','city_shipping','weight_shipping','origin_shipping'));
			$listener_ready_city_select
                ->add_handler('reload')
                ->set_target('service_reload')
                ->set_url(curl::base() . 'raja_ongkir/get_cost')
                ->add_param_input(array('service_type','city_shipping','weight_shipping','origin_shipping'));
		}
        $app->add_js("
				if($('#check_same').is(':checked')){
					province_select2_make_same();
					city_select2_make_same();
					districts_select2_make_same();
					$.cresenity.reload('div_shipping_price','" . curl::base() . 'reload/reload_shipping_price?' . $addition_param . "','get',{'shipping_type_id':$.cresenity.value('#shipping_type_id'),'city_shipping':$.cresenity.value('#city_shipping')});
				}
			");
        echo $app->render();
    }

    public function reload_control_city() {
        $app = CApp::instance();
        $get = $_GET;
        $province_id = carr::get($get, 'province');

        $control_city_select = $app->add_field()->add_control('city', 'city-select')
                ->add_class("select-62hallfamily")
                ->custom_css('width', '100%')
                ->custom_css('height', '44px');
        $control_city_select->set_all(false);
        $control_city_select->set_province_id($province_id);
        $listener_change_city_select = $control_city_select->add_listener('change');
        $listener_change_city_select
                ->add_handler('reload')
                ->set_target('div_districts')
                ->set_url(curl::base() . self::__CONTROLLER . 'reload_control_districts')
                ->add_param_input(array('city'));
        $listener_ready_city_select = $control_city_select->add_listener('ready');
        $listener_ready_city_select
                ->add_handler('reload')
                ->set_target('div_districts')
                ->set_url(curl::base() . self::__CONTROLLER . 'reload_control_districts')
                ->add_param_input(array('city'));

        echo $app->render();
    }

    public function reload_districts() {
        $app = CApp::instance();
        $get = $_GET;
        $city_id = carr::get($get, 'city_pay');
        $page = carr::get($get, 'page');
        $from = 'pay';
        $addition_param = '';
        if ($city_id == null) {
            $city_id = carr::get($get, 'city_shipping');
            $from = 'shipping';
        }
        if ($from == 'shipping') {
            if ($page == 'gold') {
                $addition_param = '&page=' . $page;
            }
        }
        $control_districts_select = $app->add_field()->set_label('Kecamatan <red>*</red>')->add_control('districts_' . $from, 'districts-select')->add_class('width-40-percent');
        $control_districts_select->set_all(false);
        $control_districts_select->set_city_id($city_id);
        $listener_change_districts_select = $control_districts_select->add_listener('change');
        $listener_ready_districts_select = $control_districts_select->add_listener('ready');
        if ($from == 'shipping') {
            $listener_change_districts_select->add_handler('reload')->set_target('div_shipping_price')->add_param_input('total_weight')->add_param_input('shipping_type_id')->add_param_input('districts_shipping')->set_url(curl::base() . 'reload/reload_shipping_price?' . $addition_param);
            $listener_ready_districts_select->add_handler('reload')->set_target('div_shipping_price')->add_param_input('total_weight')->add_param_input('shipping_type_id')->add_param_input('districts_shipping')->set_url(curl::base() . 'reload/reload_shipping_price?' . $addition_param);
        } $app->add_js("
				if($('#check_same').is(':checked')){
					province_select2_make_same();
					city_select2_make_same();
					districts_select2_make_same();
				}
			");
        echo $app->render();
    }

    public function reload_control_districts() {
        $app = CApp::instance();
        $get = $_GET;
        $city_id = carr::get($get, 'city');

        $control_districts_select = $app->add_field()->add_control('districts', 'districts-select')
                ->add_class("select-62hallfamily")
                ->custom_css('width', '100%')
                ->custom_css('height', '44px');
        $control_districts_select->set_all(false);
        $control_districts_select->set_city_id($city_id);
        $listener_change_districts_select = $control_districts_select->add_listener('change');
        echo $app->render();
    }

    public function reload_shipping_price() {
        $app = CApp::instance();
        $db = CDatabase::instance();
//        cdbg::Var_dump('a');
        $request = $_GET;
        $shipping_type_id = carr::get($request, 'shipping_type_id');
        $districts_id = carr::get($request, 'districts_shipping');
        $total_weight = carr::get($request, 'total_weight');
        $page = carr::get($request, 'page');
        $city_id = cdbutils::get_value('select city_id from districts where districts_id=' . $db->escape($districts_id));
        if ($total_weight < 1) {
            $total_weight = 1;
        }
        $data_shipping_price = shipping::get_shipping_price($shipping_type_id, $districts_id);
        $shipping_price = 'Maaf Pengiriman ke Wilayah Anda Tidak Tersedia';

        if (count($data_shipping_price) > 0) {
            $shipping_price = $data_shipping_price->sell_price;
        }
        if ($page == 'gold') {
            $shipping_price = 'Maaf Pengiriman ke Wilayah Anda Tidak Tersedia';
            if ($city_id == '353') {
                $shipping_price = 45000;
            }
            if ($city_id == '375') {
                $shipping_price = 30000;
            }
            //$province_id=cdbutils::get_value("select province_id from city where city_id=".$db->escape($city_id));
//            if($city_id=='203'){
//                $shipping_price = ctransform::format_currency(30000);
//            }
//            if($city_id=='204'){
//                $shipping_price = ctransform::format_currency(30000);
//            }
        }
        //cdbg::var_dump($shipping_price);
        if ($shipping_price == 'Maaf Pengiriman ke Kota Anda Tidak Tersedia') {
            $field = $app->add_field()->set_label(clang::__('Biaya Pengiriman'))->add('<label>' . $shipping_price . '</label>');
            $app->add_control('shipping_price', 'hidden')
                    ->custom_css('width', '200px')
                    ->set_value('-');
        } else {
            if (is_numeric($shipping_price)) {
                $app->add_field()->set_label(clang::__('Biaya Pengiriman'))
                    ->add_control('shipping_price', 'text')
                    ->custom_css('width', '200px')
                    ->set_value('Rp. ' . ctransform::format_currency($shipping_price))
                    ->set_attr('readonly', true);
            }
            else {
                $alert = $app->add_div()->add_class('alert alert-warning');
                $alert->add($shipping_price);
            }
            
//            $app->add_field()->set_label(clang::__('Biaya Pengiriman'))
//                    ->add_control('shipping_price', 'text')
//                    ->custom_css('width', '200px')
//                    ->set_value('Rp. ' . $shipping_price)
//                    ->set_attr('readonly', true);
            
        }
        echo $app->render();
    }

    public function reload_stock_price_product($code = '', $product_id = '') {
        $app = CApp::instance();
        $request = $_GET;
        $attribute = array();
        foreach ($request as $key_req => $value_req) {
            if (substr($key_req, 0, 8) == 'att_cat_') {
                $attribute[substr($key_req, 8)] = $value_req;
            }
        }
        // get data product 
        $product_simple = product::get_simple_product_from_attribute($product_id, $attribute);
        if ($product_simple == null) {
            echo $app->render();
            return;
        }
        $data_product = product::get_product($product_simple->product_id);

        $stock = carr::get($data_product, 'stock', 0);
        $price = product::get_price($data_product['detail_price']);

        $app->add('<div id="stock-price">');
        $app->add('<span class="font-gray-dark">Tersisa ' . $stock . '</span>');
        $app->add('<br>');

        $app->add('<br>');

        if ($price['promo_price'] > 0) {
            $app->add('<strike class="font-black"> Rp. ' . ctransform::format_currency($price['price']) . '</strike>');
            $app->add('<div class="font-red font-size24 bold"> Rp. ' . ctransform::format_currency($price['promo_price']) . '</div>');
        } else {
            $app->add('<div class="font-red font-size24 bold"> Rp. ' . ctransform::format_currency($price['price']) . '</div>');
        }
        $app->add('</div>');

        $listener = $app->add_listener('ready');

        $handler = $listener->add_handler('custom')
                ->set_js("
                            $('#qty').trigger('touchspin.updatesettings', 
                                {
                                max: " . $stock . "
                                }
                             );
                             
                             if(" . $stock . " == 0){
                                 $('#div_qty').hide();
                                 $('.btn-add-cart').hide();
                             } else {
                                 $('#div_qty').show();
                                 $('.btn-add-cart').show();
                             
                             }
                             
                            ");

        //$app->add(cdbg::var_dump($data_simple_attribute));
        echo $app->render();
    }

    public function get_count_page($total) {

        $count_page = (int) ($total / 24);

        if ($total % 24 != 0) {
            $count_page += 1;
        }

        return $count_page;
    }

    public function get_start_end($page, $count, $total_page = 0) {
        $start = 0;
        if ($page > 1) {
            $start = ($page - 1) * 24;
        }
        $end = 23;
        if ($page > 1) {
            $end = ($page * 24) - 1;
        }
        if ($end > $count - 1) {
            $end = $count - 1;
        }

        return array('start' => $start, 'end' => $end);
    }

    public function reload_product_filter() {
        $app = CApp::instance();
        $get = $_GET+$_POST;

        $filter_page = carr::get($get, 'filter_page', '');

        $page = 1;
        if (isset($get['page'])) {
            $page = $get['page'];
        }
        $visibility = carr::get($get, 'visibility');
        $filter_page = carr::get($get, 'filter_page');
        $filter_name = carr::get($get, 'filter_name');
        $filter_product_category_id = carr::get($get, 'filter_product_category_id', NULL);
        $filter_category_lft = carr::get($get, 'filter_category_lft');
        $filter_category_rgt = carr::get($get, 'filter_category_rgt');
        $filter_location = carr::get($get,'product_location',array());
        //prelove
        $prelove_last_deal = carr::get($get, 'last_deal');
        $prelove_upcoming_deal = carr::get($get, 'upcoming_deal');
        $prelove_end_deal = carr::get($get, 'end_deal');
        $prelove_new = carr::get($get, 'new');
        $prelove_used = carr::get($get, 'used');      
        
        // kinerja
        $filter_product_type = carr::get($get, 'filter_product_type');
        // End kinerja
        
        $arr_loc_prov=array();
        $arr_loc=array();
        //get all pr
        foreach($filter_location as $loc_val){
            $loc_ex=explode('_',$loc_val);
            $prov=carr::get($loc_ex,'0');
            if($prov=='pr'){
                $val_prov=carr::get($loc_ex,'1');
                $arr_loc_prov[$val_prov]=$val_prov;
                $arr_loc[$val_prov]='all';
            }
        }
        
        //get all location match with pr
        foreach($filter_location as $loc_val){
            $loc_ex=explode('_',$loc_val);
            $prov=carr::get($loc_ex,'0');
            if($prov!==null && $prov!='pr'){
                $exist=carr::get($arr_loc_prov,$prov);
                if($exist==null){
                    $val_city=carr::get($loc_ex,'1');
                    $arr_loc[$prov][]=$val_city;
                }
            }
        }        
        $param_input = $get;
        unset($param_input['capp_current_container_id']);

        $list_product_id = carr::get($get, 'arr_product_id');
        $list_filter = carr::get($get, 'list_filter', '');

        $list_filter_price = carr::get($get, 'price', NULL);
        if (strlen($list_filter_price) > 0) {
            $filter_price = explode(',', $list_filter_price);
        }
        if (empty($filter_price)) {
            $filter_price = array();
        }
        $filter_product = carr::get($get, 'sortby');
        $arr_attribute = array();
        foreach ($param_input as $cat => $value) {
            if (substr($cat, 0, strlen('catalog_filter_')) == 'catalog_filter_') {
                $cat_attr = substr($cat, strlen('catalog_filter_'));
                $cat_arr = explode('_', $cat_attr);
                $cat_id = carr::get($cat_arr, 0);
                $attr_id = carr::get($cat_arr, 1);
                if (strlen($value) > 0 && $value > 0) {
                    if (!isset($arr_attribute[$cat_id])) {
                        $arr_attribute[$cat_id] = array();
                    }

                    $arr_attribute[$cat_id][] = $value;
                }
            }
        }

        $options = array(
            "attributes" => $arr_attribute,
            "min_price" => carr::get($filter_price, 0),
            "max_price" => carr::get($filter_price, 1),
            "visibility" => $visibility,
            "product_type" => $filter_page,
            "search" => $filter_name,
            "product_category_lft" => $filter_category_lft,
            "product_category_rgt" => $filter_category_rgt,
            "product_location" => $arr_loc,
            "sortby" => $filter_product,
            "prelove_last_deal"=>$prelove_last_deal,
            "prelove_upcoming_deal"=>$prelove_upcoming_deal,
            "prelove_end_deal"=>$prelove_end_deal,
            "prelove_new"=>$prelove_new,
            "prelove_used"=>$prelove_used,
            // "debug"=>true,
        );
        
        if (strlen($filter_product_category_id) > 0) {
            $options['product_category_id'] = $filter_product_category_id;
        }

        $total_product = product::get_count($options);

//        die($total_product);
        // debug
//        if (self::__is_test) {
//            $total_product = 12;
//        }
        // end debug
        
        //$arr_product = product::get_product_list_from_attribute($list_product_id, $arr_attribute, $filter_price, $filter_product, $visibility, $filter_page, $filter_name, $filter_category_lft, $filter_category_rgt);
        //cdbg::var_dump(count($arr_product));

        $container_product = $app;

        $arr_start_end = $this->get_start_end($page, $total_product);
        $options["limit_start"] = carr::get($arr_start_end, "start", 0);
        $options["limit_end"] = carr::get($arr_start_end, "end", 10);
        
        if ($filter_page == 'luckyday') {
            $options["stock"] = 1;
        }

        $result = product::get_result($options);
        
        if ($filter_page == 'kinerja') {
            $show = carr::get($get, 'show');
            $page_number = carr::get($get, 'page_number');

            $post_data['session_id'] = Session::instance()->get('api_session_id');
            $post_data['product_code'] = 'KP';
            $post_data['product_type'] = $filter_product_type;
            $post_data['category_id'] = $filter_product_category_id;
            if (strlen($show) > 0) {
                $post_data['show'] = $show;
            }
            if (strlen($page_number) > 0) {
                $post_data['page_number'] = $page_number;
            }

            $api = CApiServerSixtyTwoHallFamily::instance('transaction', array('GetProducts'));
            $api->set_post_data($post_data);
            $api->set_has_return(TRUE);
            $api->exec();
            $api_data = $api->get_response();
            
            $api_err_code = carr::get($api_data, 'err_code');
            $api_err_message = carr::get($api_data, 'err_message');
            $data = carr::get($api_data, 'data');
            
            cdbg::var_dump($api_err_code);
            cdbg::var_dump($api_err_message);
            die(__METHOD__);

            if ($api_err_code == 0) {
                $product = carr::get($data, 'product');
                $product_total = carr::get($data, 'product_total');
                cdbg::var_dump($result);
                die(__METHOD__);
                $result = $product;
                $arr_start_end = $this->get_start_end($page, $product_total);
                $options["limit_start"] = carr::get($arr_start_end, "start", 0);
                $options["limit_end"] = carr::get($arr_start_end, "end", 10);
            }
        }

        cdbg::var_dump($filter_page);
        cdbg::var_dump($result);

        $index = $arr_start_end['start'];
        //for ($index; $index <= $arr_start_end['end']; $index++) {
        $container_product_row = $app->add_div()->add_class("row");
        // debug
//        if (self::__is_test) {
//            if ($filter_page == 'luckyday') {
//                $result = $this->dummy_product_result();
//            }
//        }
        // end debug
        $i=2;
		$a = 3;
        foreach ($result as $row) {
            if ($index % 4 == 0 or $index == 0) {
                 $container_product = $app->add_div()->add_class('row');
//                $container_product = $container_product_row->add_div();
            }
            $product_id = cobj::get($row, 'product_id', NULL);
            $url_key = cobj::get($row, 'url_key', NULL);
            
            $key = $url_key;
            if($key==null) $key = $product_id;
                
            $image = cobj::get($row, 'image_name', NULL);
            $name = cobj::get($row, 'name', NULL);
            //$flag = carr::get($arr_product[$index], 'flag', NULL);
            $flag = $hot = (cobj::get($row, 'is_hot_item', NULL) > 0) ? 'Hot' : NULL;

//            if (!empty(cobj::get($row, 'new_product_date', NULL))) {
            if (strlen(cobj::get($row, 'new_product_date', NULL)) > 0) {
                if (strtotime(cobj::get($row, 'new_product_date', NULL)) >= strtotime(date('Y-m-d'))) {
                    $flag = $new = 'New';
                }
            }

            $stock = cobj::get($row, 'stock', NULL);
            $show_minimum_stock = cobj::get($row, 'show_minimum_stock', NULL);
            $is_available = cobj::get($row, 'is_available', NULL);

            $detail_price = array();
            $detail_price["sell_price_start"] = cobj::get($row, 'sell_price_start', NULL);
            $detail_price["sell_price_end"] = cobj::get($row, 'sell_price_end', NULL);
            $detail_price["vendor_sell_price"] = cobj::get($row, 'vendor_sell_price', NULL);
            $detail_price["vendor_nta"] = cobj::get($row, 'vendor_nta', NULL);
            $detail_price["vendor_commission_value"] = cobj::get($row, 'vendor_commission_value', NULL);
            $detail_price["upselling"] = cobj::get($row, 'upselling', NULL);
            $detail_price["ho_sell_price"] = cobj::get($row, 'ho_sell_price', NULL);
            $detail_price["channel_commission_full"] = cobj::get($row, 'channel_commission_full', NULL);
            $detail_price["channel_commision_ho"] = cobj::get($row, 'channel_commision_ho', NULL);
            $detail_price["promo_total"] = cobj::get($row, 'promo_total', NULL);
            $detail_price["channel_commission_share"] = cobj::get($row, 'channel_commission_share', NULL);
            $detail_price["channel_commission_share_ho"] = cobj::get($row, 'channel_commission_share_ho', NULL);
            $detail_price["channel_commission_value"] = cobj::get($row, 'channel_commission_value', NULL);
            $detail_price["channel_updown"] = cobj::get($row, 'channel_updown', NULL);
            $detail_price["channel_sell_price"] = cobj::get($row, 'channel_sell_price', NULL);
            $detail_price["channel_profit"] = cobj::get($row, 'channel_profit', NULL);
            $detail_price["promo_text"] = cobj::get($row, 'promo_text', NULL);


            if ($filter_page == 'gold') {
                $purchase_price = cobj::get($row, 'purchase_price', NULL);
                $sell_price = cobj::get($row, 'channel_sell_price', NULL);
                $element_product = $container_product->add_element('62hall-gold-product', 'product-gold-' . $row->product_id);
                $element_product->set_image($image)
                        ->set_product_id($row->product_id)
                        ->set_using_class_div_gold(true)
                        ->set_column(4)
                        ->set_name($name)
                        ->set_stock($stock)
                        ->set_price_sell('J : Rp. ' . ctransform::format_currency($sell_price))
                        ->set_price_buy('B : Rp. ' . ctransform::format_currency($purchase_price));
            }
            elseif($filter_page == 'luckyday') {
                $element_product = $container_product->add_element('62hall-luckyday-product', 'product-luckyday-' . $product_id);
                $element_product
                        ->set_product_id($product_id)
                        ->set_product_url($url_key)
                        ->set_column(4)
                        ->set_using_class_div_gold(true)
                        ->set_product($row);
//                
            }
            elseif($filter_page == 'prelove') {
                if($i==2){
                    $i=0;
                    $div_row=$container_product->add_div()->add_class('row');
                }
                $div_col=$div_row->add_div()->add_class('col-md-6');
                $product_prelove=prelove_product::get($product_id);
                $element_product = $div_col->add(CElement_Prelove_Card::factory()->set_product($product_prelove));
                $i++;
//                
            }
            elseif($filter_page == 'lokal') {
                if($a==3){
                    $a=0;
                    $div_row=$container_product->add_div()->add_class('row produk-lokal');
                }
                $div_col=$div_row->add_div()->add_class('col-md-4')->custom_css('margin-bottom','20px');
                $product_lokal = product::get_product($product_id);
                $element_product = $div_col->add(CElement_Lokal_Card::factory()->set_product($product_lokal));
                $i++;
//                
            }elseif($filter_page == 'event') {
                $div_row=$container_product->add_div()->add_class('row produk-event margin-bottom-20');
                $div_12 = $div_row->add_div()->add_class('col-xs-12');
                $product_event = product::get_product($product_id);
                $element_product = $div_12->add(CElement_Event_Card::factory()->set_product($product_event));
//                
            }
            elseif($filter_page == 'voucher') {
                if($a==3){
                    $a=0;
                    $div_row=$container_product->add_div()->add_class('row produk-voucher');
                }
                $div_col=$div_row->add_div()->add_class('col-md-4')->custom_css('margin-bottom','20px');
                $product_event = product::get_product($product_id);
                $element_product = $div_col->add(CElement_Card::factory()->set_product($product_event)->set_page('voucher')->html());
                $i++;
            }
            elseif($filter_page == 'grosir') {
                if($a==3){
                    $a=0;
                    $div_row=$container_product->add_div()->add_class('row produk-grosir');
                }
                $div_col=$div_row->add_div()->add_class('col-md-4')->custom_css('margin-bottom','20px');
				$product_grosir = product::get_product($product_id);
                $element_product = $div_col->add(CElement_Card::factory()->set_product($product_grosir)->set_page('grosir'));
                $i++;
//                
            }
//            elseif($filter_page == 'kinerja') {
//                $element_product = $container_product->add_element('62hall-product', 'product-' . $row->product_id);
//                $element_product->set_key($key)
//                        ->set_image($image)
//                        ->set_column(4)
//                        ->set_name($name)
//                        ->set_flag($flag)
//                        ->set_label_flag($flag)
//                        ->set_stock($stock)
//                        ->set_min_stock($show_minimum_stock)
//                        ->set_price($detail_price)
//                        ->set_available($is_available);
//            }
            else {
                $element_product = $container_product->add_element('62hall-product', 'product-' . $row->product_id);
                $element_product->set_key($key)
                        ->set_image($image)
                        ->set_column(4)
                        ->set_name($name)
                        ->set_flag($flag)
                        ->set_label_flag($flag)
                        ->set_stock($stock)
                        ->set_min_stock($show_minimum_stock)
                        ->set_price($detail_price)
                        ->set_available($is_available);

                if ($filter_page == 'service') {
                    $element_product->set_location_detail(curl::base() . 'service/product/item/');
                }
                if ($filter_page == 'shinjuku') {
                    $element_product->set_location_detail(curl::base() . 'shinjuku/product/item/');
                }
                if ($filter_page == 'namecard') {
                    $element_product->set_location_detail(curl::base() . 'namecard/product/item/');
                }
            }
            $index++;
        }

        // pagination
        $paginationcontainer = $app->add_div()
                ->add_class('pagination col-md-12 padding-0')
                ->add('<center>');

        $count_page = $this->get_count_page($total_product);


        //?page=1&visibility=" . $visibility . '&filter_page=' . $filter_page . '&filter_name=' . $filter_name . '&filter_category_lft=' . $filter_category_lft . '&filter_category_rgt=' . $filter_category_rgt);
        unset($param_input['page']);
        unset($param_input['visibility']);
        unset($param_input['filter_page']);
        unset($param_input['filter_name']);
        unset($param_input['filter_category_lft']);
        unset($param_input['filter_category_rgt']);


        $param_input = array_keys($param_input);

        if ($count_page > 0) {
            if ($page > 1) {
                $previus_page = $page - 1;
                $paginationcontainer->add_action()
                        ->set_label('<i class="fa fa-fast-backward"></i>')
                        ->add_class('font-gray-dark')
                        ->add_listener('click')
                        ->add_handler('reload')
                        ->set_target('page-product-category')
                        ->add_param_input($param_input)
                        ->set_url(curl::base() . "reload/reload_product_filter/?source=paging&page=1&visibility=" . $visibility . '&filter_page=' . $filter_page . '&filter_name=' . urlencode(addslashes($filter_name)) . '&filter_category_lft=' . $filter_category_lft . '&filter_category_rgt=' . $filter_category_rgt);
                $paginationcontainer->add_action()
                        ->set_label('<i class="fa fa-backward"></i>')
                        ->add_class('font-gray-dark')
                        ->add_listener('click')
                        ->add_handler('reload')
                        ->set_target('page-product-category')
                        ->add_param_input($param_input)
                        ->set_url(curl::base() . "reload/reload_product_filter/?source=paging&page=" . $previus_page . "&visibility=" . $visibility . '&filter_page=' . $filter_page . '&filter_name=' . urlencode(addslashes($filter_name)) . '&filter_category_lft=' . $filter_category_lft . '&filter_category_rgt=' . $filter_category_rgt);
            }


            $start_page = $page - 5;
            if ($start_page < 1) {
                $start_page = 1;
            }

            $end_page = $page + 5;
            if ($end_page > $count_page) {
                $end_page = $count_page;
            }

//            $end_page = $count_page;
//            if ($count_page > 10) {
//                $end_page = $page + 9;
//                if ($end_page > $count_page) {
//                    $end_page = $count_page;
//                }
//            }
//            
//            $start_page = 1;
//            if ($count_page > 10) {
//                $start_page = $page;
//            }

            for ($i = $start_page; $i <= $end_page; $i++) {
                $this_page = $page;
                $class = NULL;
                $class = 'font-black';
                if ($this_page == $i) {
                    $class = 'btn-pagination bg-red font-white';
                }
                $paginationcontainer->add_action()
                        ->set_label($i)
                        ->add_class($class)
                        ->add_listener('click')
                        ->add_handler('reload')
                        ->set_target('page-product-category')
                        ->add_param_input($param_input)
                        ->set_url(curl::base() . "reload/reload_product_filter/?source=paging&page=" . $i . "&visibility=" . $visibility . '&filter_page=' . $filter_page . '&filter_name=' . urlencode(addslashes($filter_name)) . '&filter_category_lft=' . $filter_category_lft . '&filter_category_rgt=' . $filter_category_rgt);
            }
            if ($page < $count_page) {
                $next_page = $page + 1;
                $paginationcontainer->add_action()
                        ->set_label('<i class="fa fa-forward"></i>')
                        ->add_class('font-gray-dark')
                        ->add_listener('click')
                        ->add_handler('reload')
                        ->set_target('page-product-category')
                        ->add_param_input($param_input)
                        ->set_url(curl::base() . "reload/reload_product_filter/?source=paging&page=" . $next_page . "&visibility=" . $visibility . '&filter_page=' . $filter_page . '&filter_name=' . urlencode(addslashes($filter_name)) . '&filter_category_lft=' . $filter_category_lft . '&filter_category_rgt=' . $filter_category_rgt);
                $paginationcontainer->add_action()
                        ->set_label('<i class="fa fa-fast-forward"></i>')
                        ->add_class('font-gray-dark')
                        ->add_listener('click')
                        ->add_handler('reload')
                        ->set_target('page-product-category')
                        ->add_param_input($param_input)
                        ->set_url(curl::base() . "reload/reload_product_filter/?source=paging&page=" . $count_page . "&visibility=" . $visibility . '&filter_page=' . $filter_page . '&filter_name=' . urlencode(addslashes($filter_name)) . '&filter_category_lft=' . $filter_category_lft . '&filter_category_rgt=' . $filter_category_rgt);
            }
        }

        $app->add_control('page', 'hidden')
                ->set_value($page);

        $app->add_control('arr_product_id', 'hidden')
                ->set_value($list_product_id);

        $app->add_control('list_filter', 'hidden')
                ->set_value($list_filter);

        if ($total_product == 0) {
            $message = $app->add_div()
                    ->add_class('font-size16')
                    ->add(clang::__('Produk tidak ditemukan'));
        }

        echo $app->render();
    }

    public function reload_paging() {
        
    }
    
    public function dummy_product_result() {
        $result = array();
        
        $dummy_product = array(
            "product_id" => "192060",
            "product_type_id" => "5",
            "product_category_id" => "183",
            "vendor_id" => "104",
            "country_manufacture_id" => "94",
            "code" => "nmccompany",
            "name" => "Company Card",
            "url_key" => "company-card",
            "new_product_date" => "2016-09-15",
            "currency" => "idr",
            "purchase_price_nta" => null,
            "purchase_price" => null,
            "sell_price_nta" => "2500000",
            "sell_price" => "2600000",
            "sell_price_start" => null,
            "sell_price_end" => null,
            "ho_upselling" => "0",
            "ho_commission" => "0",
            "ho_commission_type" => null,
            "weight" => "1",
            "stock" => "0",
            "description" => "lorem ipsum",
            "overview" => "<p>Lorem ipsum rincian company card<\/p>\n",
            "spesification" => "",
            "faq" => "",
            "is_active" => "1",
            "is_available" => "1",
            "is_stock" => "0",
            "is_hot_item" => "0",
            "show_minimum_stock" => "0",
            "alert_minimum_stock" => "0",
            "parent_id" => null,
            "sku" => "nmccompany1",
            "product_data_type" => "simple",
            "status_product" => "FINISHED",
            "is_selection" => "0",
            "filename" => "company-card.png",
            "image_name" => "default_image_ProductImageParent_20160830_company-card.png",
            "file_path" => "http://admin.mybigmall.local/res/show/NjI2MlJXUFNDXkJtX19XVVNtZkBZVkNRQntbU1FXZlNEV1hGaQAGAwACDgEGbVVdW0JXXE8fVVNEVhhCWFU=",
            "visibility" => "not_visibility_individually",
            "featured_image" => null,
            "description_voucher" => null,
            "is_voucher" => "0",
            "is_generate_code" => "0",
            "is_email" => "0",
            "image_fixed" => "0",
            "status" => "1",
            "quota" => "2000",
            "slot" => "450",
        );
        
        for($i=0; $i < 12; $i++){
            $dummy_product['product_id'] = $i;
            $dummy_product['quota'] = rand(1000, 1500);
            $dummy_product['slot'] = rand(0, 1000);
            $result[] = (object) $dummy_product;
        }
        
        return $result;
    }
    
    public function reload_video(){
        $app=CApp::instance();
        $get=$_GET;
        
        $url_key=carr::get($get,'url_key');
        $url_prev=carr::get($get,'url_prev');
        $url_next=carr::get($get,'url_next');
        $el_cms_video=$app->add_element('62hall-cms-video','cms_video');
        $el_cms_video->set_url_key($url_key);
        echo $app->render();
    }
    
    public function reload_video_gallery(){
        $app=CApp::instance();
        
        $get = $_GET;
        $active = carr::get($get, 'active');
        
        $el_video_gallery=$app->add_element('62hall-cms-video-gallery','cms_video_gallery');
        $el_video_gallery->set_active($active);
        
        echo $app->render();
    }
}
