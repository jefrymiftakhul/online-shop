<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Mar 31, 2016
     */
    class Controller_ApiDoc_General extends CController {

        public function __construct() {
            parent::__construct();
        }

        public function index() {
            $app = CApp::instance();

            $api = CApiServerSixtyTwohallFamily::instance('Data', array());
            $api->set_generate_doc(true);
            $services = $api->get_service();
            $container = $app->add_div()->add_class('container');
            $container->add("<h4>" . clang::__('Services') . "</h4>");
            $a = 0;
            foreach ($services as $service_k => $service_v) {
//                if ($a == 8) {
//                    cdbg::var_dump($service_k);
                    $key = $service_k;
                    $description = carr::get($service_v, 'description');
                    $name = carr::get($service_v, 'name', $key);
                    $input = carr::get($service_v, 'input', array());
                    $output = carr::get($service_v, 'output', array());
//                    if($a >= 32) {
//                        $name .= ' <b>(TREVO)</b>';
//                    }
                    $widget = $container->add_widget()->set_title($name)
                            ->set_collapse(true);

                    $desc_wrapper = $widget->add_div();
                    $desc_wrapper->add("<h4 class='os-title'>" . clang::__('Description') . "</h4>");
                    if (strlen($description) > 0) {
                        $desc_wrapper->add($description);
                    }
                    else {
                        $desc_wrapper->add('No Description Available');
                    }

                    $input_wrapper = $widget->add_div();
                    $all_input = array();
                    $all_input = $this->generate_output($input, 0);
//                cdbg::var_dump($all_input);exit;
                    $input_wrapper->add("<h4>" . clang::__('Input') . "</h4>");
                    $table = $input_wrapper->add_table()->set_footer(false)->set_apply_data_table(false);
                    $table->add_column('key')->set_label('Key')->set_width(200);
                    $table->add_column('type')->set_label('Type')->set_width(100);
                    $table->add_column('rules')->set_label('Rules')->set_width(100);
                    $table->add_column('desc')->set_label('Desc')->set_width(300);
//                foreach ($input as $input_k => $input_v) {
//                    $rules = carr::get($input_v, 'rules', array());
//                    $all_input[] = array(
//                        'key' => $input_k,
//                        'type' => carr::get($input_v, 'data_type'),
//                        'rules' => implode(', ', $rules),
//                    );
//                }
                    $table->set_data_from_array($all_input);

                    $output_wrapper = $widget->add_div();
                    $all_output = array();
                    $all_output = $this->generate_output($output, 0);
//                cdbg::var_dump($all_output);
                    $output_wrapper->add("<h4>" . clang::__('Output') . "</h4>");
                    $table = $output_wrapper->add_table()->set_footer(false)->set_apply_data_table(false);
                    $table->add_column('key')->set_label('Key')->set_width(200);
                    $table->add_column('type')->set_label('Type')->set_width(100);
                    $table->add_column('rules')->set_label('Rules')->set_width(100);
                    $table->add_column('desc')->set_label('Desc')->set_width(300);
                    $table->set_data_from_array($all_output);

                    $output_wrapper = $widget->add_div();
                    $container->add('<br/>');
//                }
                $a++;
            }
//            $app->add($services);

            echo $app->render();
        }

        public function generate_output($output, $depth) {
            $res = array();

            foreach ($output as $output_k => $output_v) {
                $rules = carr::get($output_v, 'rules');
                $desc = carr::get($output_v, 'description');
                $str_nbsp = '';
                for ($i = 0; $i < $depth; $i++) {
                    $str_nbsp.='&nbsp &nbsp';
                }
                if (!$rules) {
                    //cdbg::var_dump($output_v);
                    $temp_res = $this->generate_output($output_v, $depth + 1);
                    $res[] = array(
                        'key' => $str_nbsp . $output_k,
                        'type' => 'array',
                    );

                    $res = array_merge($res, $temp_res);
                }
                else {

                    $res[] = array(
                        'key' => $str_nbsp . $output_k,
                        'type' => carr::get($output_v, 'data_type'),
                        'rules' => implode(', ', $rules),
                        'desc' => $desc,
                    );
                }
            }
            return $res;
        }

    }
    