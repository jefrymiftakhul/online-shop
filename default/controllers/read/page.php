<?php

/**
 *
 * @author Riza 
 * @since  Nov 12, 2015
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class Page_Controller extends ProductController {

    private $store = NULL;
    private $domain = NULL;

    public function __construct() {
        parent::__construct();
        $org_id = $this->org_id();
        $data_org = org::get_org($org_id);

        if (!empty($data_org)) {
            $this->store = $data_org->name;
            $this->domain = $data_org->domain;
        }
    }

    public function static_indo_day($day) {
        $res = '';
        switch ($day) {
            case 'Mon' : {
                    $res = 'Senin';
                    break;
                }
            case 'Tue' : {
                    $res = 'Selasa';
                    break;
                }
            case 'Wed' : {
                    $res = 'Rabu';
                    break;
                }
            case 'Thu' : {
                    $res = 'Kamis';
                    break;
                }
            case 'Fri' : {
                    $res = 'Jumat';
                    break;
                }
            case 'Sat' : {
                    $res = 'Sabtu';
                    break;
                }
            case 'Sun' : {
                    $res = 'Minggu';
                    break;
                }
        }
        return $res;
    }

    public function index($page_name = NULL) {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $user = $app->user();
        $org_id = $this->org_id();
        if (ccfg::get('have_same_cms_first_parent') > 0) {
            $org = org::get_first_parent_org($org_id);
            if ($org != null) {
                $org_id = $org->org_id;
            }
        }
        $menu_list = $this->menu_list();
        $link_list = $this->list_menu();

        // SHOPPING CART 
        $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

        $container = $app->add_div()->add_class('container container-page-read');
        $page_container = $container->add_div()->add_class('row');

        $sidebar_page_container = $page_container->add_div()->add_class('col-md-3');
        $sidebar_page = $sidebar_page_container->add_div()->add_class('row');

        $page = $page_container->add_div()->add_class('col-md-9');

        $content = "Under Maintenance";
        $post_id = null;
        $query = "
                    select 
                        *
                    from
                        cms_post 
                    where 
                        post_status = 'publish'
                        and post_name = " . $db->escape($page_name) . "
                        and org_id = " . $org_id . "
                    ";
        $r = $db->query($query);
        if ($r->count() > 0) {
            $post_id = $r[0]->cms_post_id;
            $content = $r[0]->post_content;
            $post_name = $r[0]->post_name;

            //check page 
            $session = Session::instance();
            $arr_member_category = array('agent', 'dealer', 'cabang');
            $member_category = $session->get('member_category');
            if ($post_name == 'sertifikat-mybigmall.co') {
                if (!in_array(strtolower($member_category), $arr_member_category)) {
                    curl::redirect(curl::base());
                }
            }
        }

        $menu_service = cms::nav_menu('menu_footer_service');
        $arr_menu_service = array();
        if (is_array($menu_service)) {
            $arr_menu_service = array(
                "title_menu" => 'Layanan Pelanggan',
            );
            foreach ($menu_service as $key => $val) {
                $menu = array(
                    'name' => 'menu_' . $val['menu_object_id'],
                    'label' => $val['menu_name'],
                    'url' => $val['menu_url'],
                );
                $arr_menu_service['menu'][] = $menu;
            }
        }
        $menu_about = cms::nav_menu('menu_footer_about');
        $arr_menu_about = array();
        if (is_array($menu_about)) {
            $arr_menu_about = array(
                "title_menu" => $this->store,
            );
            foreach ($menu_about as $key => $val) {
                $menu = array(
                    'name' => 'menu_' . $val['menu_object_id'],
                    'label' => $val['menu_name'],
                    'url' => $val['menu_url'],
                );
                $arr_menu_about['menu'][] = $menu;
            }
        }
        // menu page
        $menu_page = array();
        if (count($arr_menu_service) > 0) {
            $menu_page[] = $arr_menu_service;
        }
        if (count($arr_menu_about) > 0) {
            $menu_page[] = $arr_menu_about;
        }


        $element_sidebar = $sidebar_page->add_element('62hall-page-sidebar')
                ->set_list_menu($menu_page)
                ->set_active('menu_' . $post_id);

        // $content = $this->tentang();

        $div_content = $page->add_div()->add_class('container-page-content')->add('<article>' . $content . '</article>');

        $app->add_js("
            jQuery('.content-read-slide').owlCarousel({
                autoplay: true,
                responsive:{
                    0:{
                        items:1
                    },
                    400:{
                        items:1
                    },
                    640:{
                        items:1
                    },
                    1000:{
                        items:1
                    },
                },
                nav : true,
                navText : ['<div class=\"slide-arrow-wrap\"><i class=\"fa fa-chevron-left\"></i></div>','<div class=\"slide-arrow-wrap\"><i class=\"fa fa-chevron-right\"></i></div>'],
                pagination: false,
                theme : 'custom-carousel',
                loop: true,        
            });
        ");

        echo $app->render();
    }

    public function member_promo($page_name = '') {
        $app = CApp::instance();
        $db = CDatabase::instance();

        $query = "
                    select 
                        *
                    from
                        cms_post 
                    where 
                        post_status = 'publish'
                        and post_name = " . $db->escape($page_name) . "
                        and org_id = " . CF::org_id() . "
                    ";
        $r = cdbutils::get_row($query);

        $content = cobj::get($r, 'post_content');
        $title = cobj::get($r, 'post_title', 'Under Maintenance');
        $url_location = cobj::get($r, 'url_location');

        $container = $app->add_div()->add_class('container container-page-promo ');
        $container_title = $container->add_div()->add_class('container-title')->add($title);
        $container_content = $container->add_div()->add_class('container-content');
        if (strlen($url_location) > 0) {
            $container_content->add_div()->add_class('container-content-img')->add("<img src='" . $url_location . "'>");
        }

        $container_content->add_div()->add($content);

        echo $app->render();
    }

    public function category($category = '', $post = '') {
        $app = CApp::instance();
        $title = '';
        $request = $_GET;
        $page = carr::get($request, 'page');
        $menu_list = $this->menu_list();
        $link_list = $this->list_menu();

        $the_post = cms::get_post('', $category, $page);

        if (strlen($post) > 0) {
            $the_post = cms::get_post($post, $category);
            $this->view_post($the_post);

            return FALSE;
        }
        // SEARCH 
//        $app->add_listener('ready')
//                ->add_handler('reload')
//                ->set_target('search')
//                ->set_url(curl::base() . "home/search");

        // SHOPPING CART 
        $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

        $container = $app->add_div()->add_class('container container-page-read');
        $page_container = $container->add_div()->add_class('row');

        $sidebar_page_container = $page_container->add_div()->add_class('col-md-3');
        $sidebar_page = $sidebar_page_container->add_div()->add_class('row');

        $page = $page_container->add_div('div_main_content')->add_class('col-md-9');

        $menu_service = cms::nav_menu('menu_footer_service');
        $arr_menu_service = array();
        if (is_array($menu_service)) {
            $arr_menu_service = array(
                "title_menu" => 'Layanan Pelanggan',
            );
            foreach ($menu_service as $key => $val) {
                $menu = array(
                    'name' => 'menu_' . $val['menu_object_id'],
                    'label' => $val['menu_name'],
                    'url' => $val['menu_url'],
                );
                $arr_menu_service['menu'][] = $menu;
            }
        }
        $menu_about = cms::nav_menu('menu_footer_about');
        $arr_menu_about = array();
        if (is_array($menu_about)) {
            $arr_menu_about = array(
                "title_menu" => $this->store,
            );
            foreach ($menu_about as $key => $val) {
                $menu = array(
                    'name' => 'menu_' . $val['menu_object_id'],
                    'label' => $val['menu_name'],
                    'url' => $val['menu_url'],
                );
                $arr_menu_about['menu'][] = $menu;
            }
        }
        // menu page
        $menu_page = array();
        if (count($arr_menu_service) > 0) {
            $menu_page[] = $arr_menu_service;
        }
        if (count($arr_menu_about) > 0) {
            $menu_page[] = $arr_menu_about;
        }


        $element_sidebar = $sidebar_page->add_element('62hall-page-sidebar')
                ->set_list_menu($menu_page)
                ->set_active('menu_15');

        $this->view_category($category, $the_post, $page);
    }

    public function tag($tag = '') {
        if (strlen($tag) == 0) {
            curl::redirect(curl::base());
            return;
        }
        $app = CApp::instance();
        $title = '';
        $request = $_GET;
        $page = carr::get($request, 'page');
        $menu_list = $this->menu_list();
        $link_list = $this->list_menu();

        $the_post = cms::get_post('', '', '', $tag);

        // SHOPPING CART 
        $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_menu($menu_list)
                ->set_link($link_list);
        $container = $app->add_div()->add_class('container');
        $div_title = $container->add_div()->add_class("row-fluid");
        $div_title->add('<h1 class="font-size24 margin-top-20">' . 'Tag:' . $tag . '</h1>');
        $div_main_content = $container->add_div('div_main_content')->add_class("row-fluid");


        $this->view_category('', $the_post, $div_main_content, $tag);
    }

    public function view_category($category = '', $the_post = array(), $div_main_content = null, $tag = '') {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $org_id = $this->org_id();
        $get = $_GET;
        if ($div_main_content == null) {
            $div_main_content = $app;
        }
        if (strlen($category) == 0) {
            $category = carr::get($get, 'category');
        }
        if (strlen($tag) == 0) {
            $tag = carr::get($get, 'tag');
        }
        $active = carr::get($get, 'active', 1);
        if (count($the_post) == 0) {
            $the_post = cms::get_post('', $category, $active);
        }
        $category_name = ucfirst($category);

        $row = $div_main_content->add_div()->add_class('row container-page-read');
        if (count($the_post) > 0) {
            foreach ($the_post as $the_post_k => $the_post_v) {
                $title = $the_post_v['post_title'];
                $content = $the_post_v['post_content'];
                $excerpt = $the_post_v['post_excerpt'];
                $permalink = $the_post_v['post_permalink'];
                $date = $the_post_v['post_date'];
                $time = $the_post_v['post_time'];
                $author = $the_post_v['post_author'];
                $thumbnail = $the_post_v['post_thumbnail'];

                $tag_str = '';
                $q = "select * from cms_post_tag where cms_post_id=" . $db->escape(carr::get($the_post_v, 'cms_post_id'));
                $r = $db->query($q);
                foreach ($r as $row2) {
                    if (strlen($row2->tag) == 0)
                        continue;
                    if (strlen($tag_str) > 0) {
                        $tag_str.=', ';
                    }
                    $tag_str.='<a href="' . curl::base() . 'read/page/tag/' . urlencode($row2->tag) . '">' . htmlspecialchars($row2->tag) . '</a>';
                }
                if (strlen($tag_str) > 0) {
                    $post_author = '<span class="post-tag">&nbsp;|&nbsp; Tag: ' . $tag_str . '</span>';
                }

                $content = $row->add_div()->add_class('col-md-4 col-sm-6');
                $content_post = $content->add_div()->add_class('content-read');
                $content_post->add_div()->add_class('content-read-img')->add("<a href='" . $permalink . "'><img src='" . $thumbnail . "'></a>");
                $content_post->add_div()->add_class('content-read-title')->add("<a href='" . $permalink . "'>" . $title . "</a>");
                $content_post->add_div()->add_class('content-read-content')->add($excerpt);
                $content_post->add_div()->add_class('content-read-written')->add(clang::__('written by') . ' ' . $author);
            }
            $q_join_tag = '';
            if (strlen($tag) > 0) {
                $q_join_tag = ' inner join cms_post_tag as pt on pt.cms_post_id=p.cms_post_id and pt.tag=' . $db->escape($tag);
            }
            $q = "	
				SELECT
					count(p.cms_post_id) as total_data
				FROM 
					cms_post p
					LEFT JOIN cms_term_relationships tr ON tr.cms_post_id=p.cms_post_id
					LEFT JOIN cms_term_taxonomy tt ON tt.cms_term_taxonomy_id = tr.cms_term_taxonomy_id
					LEFT JOIN cms_terms t ON t.cms_terms_id=tt.cms_terms_id
                                        " . $q_join_tag . "
				WHERE 
					p.status > 0
					AND p.post_type = 'post' AND p.post_status ='publish'";
            if (strlen($category_name) > 0) {
                $q .= " AND t.slug=" . $db->escape($category_name);
            }
            if (strlen($org_id) > 0) {
                $q .= " AND p.org_id=" . $db->escape($org_id);
            }
            $total_data = cdbutils::get_value($q);

            $element_paging = $div_main_content->add_element('62hall-paging', 'paging');
            $element_paging->set_total_data($total_data);
            $element_paging->set_data_per_page(6);
            $element_paging->set_active($active);
            $element_paging->set_reload_target('div_main_content');
            $element_paging->set_url_reload(curl::base() . 'read/page/view_category/');
            $element_paging->set_options_get('category', $category);
        }
        echo $app->render();
    }

    public function view_post($the_post) {
        $app = CApp::instance();
        $org_id = $this->org_id();
        $menu_list = $this->menu_list();
        $link_list = $this->list_menu();

        // SEARCH 
        $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('search')
                ->set_url(curl::base() . "home/search");

        // SHOPPING CART 
        $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

        $content = "Under Maintenance";

        $container = $app->add_div()->add_class('container container-page-read');
        $page_container = $container->add_div()->add_class('row');

        $sidebar_page_container = $page_container->add_div()->add_class('col-md-3');
        $sidebar_page = $sidebar_page_container->add_div()->add_class('row');

        $page = $page_container->add_div()->add_class('col-md-9 container-page-content');


        $menu_service = cms::nav_menu('menu_footer_service');
        $arr_menu_service = array(
            "title_menu" => 'Layanan Pelanggan',
        );
        foreach ($menu_service as $key => $val) {
            $menu = array(
                'name' => 'menu_' . $val['menu_object_id'],
                'label' => $val['menu_name'],
                'url' => $val['menu_url'],
            );
            $arr_menu_service['menu'][] = $menu;
        }

        $menu_about = cms::nav_menu('menu_footer_about');
        $arr_menu_about = array(
            "title_menu" => $this->store,
        );
        foreach ($menu_about as $key => $val) {
            $menu = array(
                'name' => 'menu_' . $val['menu_object_id'],
                'label' => $val['menu_name'],
                'url' => $val['menu_url'],
            );
            $arr_menu_about['menu'][] = $menu;
        }
        // menu page
        $menu_page = array();
        $menu_page[] = $arr_menu_service;
        $menu_page[] = $arr_menu_about;


        $element_sidebar = $sidebar_page->add_element('62hall-page-sidebar')
                ->set_list_menu($menu_page)
                ->set_active('menu_15');

        if ($the_post != NULL) {
            $title = $the_post['post_title'];
            $content = $the_post['post_content'];
            $date = $the_post['post_date'];
            $time = $the_post['post_time'];
            $author = $the_post['post_author'];
            $category = $the_post['term_name'];
            $image = $the_post['post_thumbnail'];

            $date = Date('d F Y', strtotime($date));

            $div_content = $page->add_div()->add_class('content-read');
            $div_content->add_div()->add_class('content-read-title')->add($title);
            $txt = $div_content->add_div()->add_class('content-read-text');
            $txt->add_div()->add_class('content-desc-post')->add($date . '&nbsp;&nbsp;&nbsp;<i class="fa fa-pencil"></i> ' . clang::__('written by') . ' ' . $author);

            $content = "<img class='image-in-post' src='" . $image . "' align='left'>" . $content;

            $txt->add_div()->add($content);
        }

        echo $app->render();
    }

}
