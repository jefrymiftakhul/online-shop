<div id="14527716675697895383322372775719" class=" font-size24 font-red bold col-md-5 padding-0">						
Kebijakan & Privacy
</div>					
<div id="14527716675697895383af2155920531" class=" col-md-7 padding-0 margin-top-20" style="border-bottom:3px dashed #ccc;">					
</div>	
<div id="14527716675697895383eda267919051" class=" col-md-12 padding-0 margin-top-20 margin-bottom-20">
    Yang dimaksud dengan kebijakan privasi ini adalah acuan atau aturan yang melindungi dan mengatur penggunaan data dan informasi penting lainnya untuk semua pihak yang terlibat dalam www.62hallfamily.com ini. Semua pihak yang melakukan akses pada website ini, secara otomatis dianggap telah membaca, memahami dan memberikan persetujuannya terhadap pengumpulan dan penggunaan data pribadi pengguna seperti yang akan diuraikan di bawah ini.<br><br>
    <div style="margin-left-30">
1.www.62hallfamily.com melindungi segala informasi yang diberikan oleh customer pada saat melakukan transaksi pembelian baik berupa biodata customer maupun data pengiriman.<br>
2.www.62hallfamily.com melindungi segala informasi yang diberikan oleh customer pada saat ingin menjadi member atau anggota berupa informasi secara lengkap tentang keanggotaan<br>
3.www.62hallfamily.com berhak menggunakan data dari customer dan member untuk peningkatan kualitas pelayanan dan mutu dari website ini<br>
4.www.62hallfamily.com tidak bertanggung jawab jika salah satu customer ataupun member melakukan penukaran data sendiri tanpa sepengetahuan www.62hallfamily.com dan terjadi penyimpangan didalamnya<br>
5.Setiap customer ataupun member dapat melakukan penghentian langganan dengan melakukan unsubscribe <br>
6.www.62hallfamily.com dapat memberitahukan data dan informasi yang dimilikinya bila diwajibkan oleh institusi yang berwenang berdasarkan hukum dan ketentuan yang berlaku di Republik Indonesia.<br>
7.Customer ataupun pengguna website www.62hallfamily.com membebaskan manajemen ataupun pihak terkait di dalamnya, jika ditemukan adanya pelanggaran kebijakan hak cipta atau hak kekayaaan intelektual dikarenakan ketidakmampuan www.62hallfamily.com dalam memahaminya. Customer ataupun pengguna website dapat menyampaikan keberatan atas produk dan jasa tersebut yang diindikasikan melakukan pelanggaran.<br>
8.Customer dapat mengirimkan saran dan kritik serta keperluan lainnya melalui kontak di Hubungi Kami.<br><br>
    </div>
Kebijakan Privasi ini dapat diubah atau dilakukan pembaharuan untuk dapat meningkatkan kualitas kenyamanan dan keamanan untuk semua pihak yang terlibat dalam penggunaan website ini. www.62hallfamily.com sangat menyarankan setiap pihak yang terlibat dalam penggunaan website ini untuk membaca dan memahami setiap halaman dari Kebijakan Privasi ini dari waktu ke waktu untuk dapat mengerti setiap perubahan atau pembaharuan yang dilakukan. Customer yang melakukan pengaksesan website ini dianggap telah melakukan persetujuan untuk seluruh isi kebijakan privasi ini.
</div>