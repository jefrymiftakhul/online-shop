<?php

/**
 * @author: ITRodex5
 * @since:   2016-11-15 11:41:46
 * @last modified by:   ITRodex5
 * @last modified time: 2016-11-15 12:07:40
 * @filename: home.php
 * @filepath: C:\xampp\htdocs_pipo\application\62hallfamily\lasvegas\controllers\home.php
 */

/**
 * @var Home_Controller
 */
class Home_Controller extends ProductController {

    function __construct() {
        parent::__construct();
        $db=CDatabase::instance();
        $org_id=CF::org_id();
        $org=org::get_org($org_id);
        //update viewed
        $org_viewed=cobj::get($org,'viewed');
        
        $data_viewed=array(
            "viewed"=>$org_viewed+1,
        );
//        $db->update('org',$data_viewed,array("org_id"=>$org_id));
    }

    public function index() {
        $app = CApp::instance();
        $org_id = CF::org_id();
        //dummy org
        // $org_id = '620019';

        $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

        $slide_list_top = slide::get_slide($org_id, $this->page());
        $slide = $app->add_div()->add_class('home-slideshow row');
        $slide = $slide->add_div()->add_class('col-md-12');
        $element_slide_show = $slide->add_element('62hall-slide-show', 'home_slideshow');
        $element_slide_show->set_slides($slide_list_top);


        $container = $app->add_div()->add_class('container');
        $container_adds = $container->add_div()->add_class('container-adds');
        $container_adds_content = $container_adds->add_div()->add_class('row');

        $data_advertise = advertise::get_advertise($org_id, $this->page());
        $ads_top = carr::get($data_advertise, 0, array());
        $ads_bottom = carr::get($data_advertise, 1, array());


        foreach ($ads_top as $key => $value) {
            $image_url = carr::get($value, 'image_url');
            $col_span = carr::get($value, 'col_span');
            $url_link = carr::get($value, 'url_link');

            if ($col_span == '1') {
                $column = 'col-md-6';
            } else {
                $column = 'col-md-12';
            }

            $container_adds_content->add_div()->add_class($column)->add("<a href='" . $url_link . "'><img class='img-responsive' src='" . $image_url . "'></a>");
        }

        $container_deal = $container->add_div()->add_class('container-deal');

        $container_adds_bottom = $container->add_div()->add_class('row');
        foreach ($ads_bottom as $key => $value) {
            $image_url = carr::get($value, 'image_url');
            $col_span = carr::get($value, 'col_span');
            $url_link = carr::get($value, 'url_link');
            $column = 'col-md-12';

            $container_adds_bottom->add_div()->add_class($column . ' container-adds-bottom')->add("<a href='" . $url_link . "'><img class='img-responsive' src='" . $image_url . "'></a>");
        }


        $deal_new = deal::get_deal($org_id, $this->page(), 'deal_new', array('sortby' => 'priority'));
        $deal_best_sell = deal::get_deal($org_id, $this->page(), 'deal_best_sell', array('sortby' => 'priority'));

        $container_deal_title = $container_deal->add_div()->add_class('container-deal-title');
        $container_deal_title_txt = $container_deal_title->add_div()->add_class('deal-title');
        $container_deal_view_all = $container_deal_title->add_div()->add_class('deal-view-all');
        $container_deal_title_txt->add(clang::__('New Product'));
        $container_deal_view_all->add("<a href='" . curl::base() . "show_product/show/deal_new'><button class='btn btn-warning btn-view-all-product'>" . clang::__('View All Product') . "</button></a>");
        $container_deal_content = $container_deal->add_div()->add_class('container-deal-content');
        $element_deal = $container_deal_content->add_element('62hall-slide-product', 'new_product')->add_class('slide-deal');
        $element_deal->set_list_item($deal_new);

        $container_deal = $container->add_div()->add_class('container-deal');
        $container_deal_title = $container_deal->add_div()->add_class('container-deal-title');
        $container_deal_title_txt = $container_deal_title->add_div()->add_class('deal-title');
        $container_deal_view_all = $container_deal_title->add_div()->add_class('deal-view-all');
        $container_deal_title_txt->add(clang::__('Best Seller'));
        $container_deal_view_all->add("<a href='" . curl::base() . "show_product/show/deal_best_sell'><button class='btn btn-warning btn-view-all-product'>" . clang::__('View All Product') . "</button></a>");
        $container_deal_content = $container_deal->add_div()->add_class('container-deal-content');
        $element_deal = $container_deal_content->add_element('62hall-slide-product', 'best_seller')->add_class('slide-deal');
        $element_deal->set_list_item($deal_best_sell);

        echo $app->render();
    }

}
