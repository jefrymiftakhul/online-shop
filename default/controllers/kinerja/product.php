<?php

/**
 * Description of product
 *
 * @author Ecko Santoso
 * @since 02 Sep 16
 */
class Product_Controller extends KinerjaController {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function item($product_id) {
        $db = CDatabase::instance();
        
        if ($product_id == null) {
            curl::redirect(curl::base());
        }
        $app = CApp::instance();
        $user = $app->user();
        $org_id = $this->org_id();
        $menu_list = $this->menu_list();
        $link_list = $this->list_menu();
        $error = 0;
        $error_message = '';

        // SHOPPING CART
        $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_menu($menu_list);
        $element_menu->set_link($link_list);
        
        $main_product = $app->add_div()->add_class('main-product');
            $product_div = $main_product->add_div()->add_class('container');
        $main_overview = $app->add_div()->add_class('main-product-info');
            $overview_div = $main_overview->add_div()->add_class('container');
        $main_related = $app->add_div()->add_class('main-related');
            $related_div = $main_related->add_div()->add_class('container');

//        $container = $app->add_div()->add_class('container margin-top-20');

        kinerjamall::login();
        
        $api_session_id = Session::instance()->get('api_session_id');
        try {
            $session = CApiServer_Session::instance('session', $api_session_id);
        }
        catch (Exception $e) {
            $err_code++;
            $err_message = $e->getMessage();
        }
        $args = array();
        $args['mode']='transaction';
        $args['method']='GetProductDetail';
        $args['product_id']=$product_id;
        $response = kinerjamall::get_product_detail($session,$args,'KP');
       
        $error = carr::get($response,'err_code');
        $error_message = carr::get($response,'err_message');
        
        $product = carr::get($response, 'data');
        Session::instance()->set('last_kinerja_product_detail', $product);
        if (count($product) == 0) {
            $error++;
            $error_message = "Product not found please select other product";
        }
        if ($error == 0) {
            $product_image = $product_div->add_div()->add_class('col-md-5');
            $product_detail = $product_div->add_div()->add_class('col-md-7');
            
            // GALERY PRODUCT
            $data_galery_product = carr::get($product, 'images');
            $element_galery_product = $product_image->add_element('62hall-galery-product', 'galery-product-' . $product_id);
            $element_galery_product->set_images($data_galery_product);
            
            // DETAIL PRODUCT
            $data_detail_product = $product;
            $data_attribute_category = array(); // to do
            $element_detail_product = $product_detail->add_element('62hall-detail-product', 'detail-product-' . $product_id);
            $element_detail_product->set_key($product_id)
                    ->set_detail_list($data_detail_product)
                    ->set_attribute_list($data_attribute_category);
            
            // OVERVIEW PRODUCT
            $overview_product = $overview_div->add_div()->add_class('container');
            $element_overview = $overview_product->add_element('62hall-overview-product', 'tab-product');
            $element_overview->set_key($product_id);
            
            // RELATED PRODUCT
//            $product_related_list = product::get_product_related($product_id);
//            $element_related_product = $related_div->add_element('62hall-slide-product', 'related-product');
//            $element_related_product
//                    ->set_title(clang::__('Rekomendasi Produk'))
//                    ->set_item_display(5)
//                    ->set_list_item($product_related_list);
        }
        else {
            $app->add($error_message);
        }
        
        echo $app->render();
    }
    
    public function item_old($product_id) {
        $app = CApp::instance();
        $user = $app->user();
        $db = CDatabase::instance();
        $org_id = $this->org_id();
        $menu_list = $this->menu_list();
        $link_list = $this->list_menu();

        $error = 0;
        $error_message = '';

        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_menu($menu_list);
        $element_menu->set_link($link_list);
        $element_menu->set_url_menu(curl::base() . 'luckyday/product/category/');

        $container = $app->add_div()->add_class('container margin-top-20');

        
        kinerjamall::login();
        
        $api_session_id = Session::instance()->get('api_session_id');
        try {
            $session = CApiServer_Session::instance('session', $api_session_id);
        }
        catch (Exception $e) {
            $err_code++;
            $err_message = $e->getMessage();
        }
        $args = array();
        $args['mode']='transaction';
        $args['method']='GetProductDetail';
        $args['product_id']=$product_id;
        $response = kinerjamall::get_product_detail($session,$args,'KP');
       
        $error = carr::get($response,'err_code');
        $error_message = carr::get($response,'err_message');
        
        
        $msg = cmsg::flash_all();
        if (strlen($msg) > 0) {
            $container->add_div('div-info')->add($msg);
        }
        
        $product = carr::get($response,'data');
        
       
        if ($error == 0) {
            $product_image = $container->add_div()->add_class('col-md-4 padding-0');
            $product_detail = $container->add_div()->add_class('col-md-4');
            $product_info = $container->add_div()->add_class('col-md-4 margin-top-20');

            // GALERY PRODUCT
           
           
            //$page_image=$product_real->product_type_name;
            $element_galery_product = $product_image->add_element('62hall-galery-product', 'galery-product-' . $product_id);
            $element_galery_product->set_images($data_galery_product);
            $element_galery_product->set_page('kinerja');


            // DETAIL PRODUCT
            $data_detail_product = $product;
            $data_attribute_category = array();//todo : set variations
            $element_detail_product = $product_detail->add_element('62hall-kinerja-detail', 'detail-kinerja-' . $product_id);

            $element_detail_product->set_key($product_id)
                    ->set_detail_list($data_detail_product)
                    ->set_attribute_list($data_attribute_category);

            // PRODUCT INFO
            $element_detail_info = $product_info->add_element('62hall-detail-info', 'detail-info' . $product_id);
            //share fb
            $org = org::get_org($org_id);
            $url = '';
            $http = 'http';
            if(isset($_SERVER['HTTPS'])) {
                $http = 'https';
                
            }
            if ($org) {
                $url = $http.'://' . $org->domain . '/kinerja/product/item/' . $product_id;
            }
            $url_image = '';
            $description = '';
            $q = "SELECT * FROM product WHERE product_id=" . $db->escape($product_id);
            $r = $db->query($q);
            if ($r->count() > 0) {
                $row = $r[0];
                $url_image = $row->file_path;
                $description = $row->description;
            }
            $facebook_app_id = ccfg::get('facebook_app_id');
            $additional_head = '
                                <meta property="fb:app_id" content="' . $facebook_app_id . '">
                                <meta property="og:url" content="' . $url . '">
                                <meta property="og:title" content="' . $data_detail_product['name'] . '-' . $org->name . '">
                                <meta property="og:site_name" content="' . $url . '">
                                <meta property="og:description" content="' . htmlspecialchars(strip_tags(carr::get($data_detail_product, 'overview'))) . '">
                                <meta property="og:type" content="article">
                                <meta property="og:image" content="' . image::get_image_url($data_detail_product['image_path'], 'view_all') . '">

                            ';
            $app->set_additional_head($additional_head);
            $element_detail_info->set_url_shared_facebook($url);
            $element_detail_info->set_url_shared_twitter($url);
            $element_detail_info->set_text_twitter($data_detail_product['name'] . '-' . $org->name);
            // OVERVIEW PRODUCT
//            $overview_product = $app->add_div()->add_class('container');
//            $element_overview = $overview_product->add_element('62hall-overview-product', 'tab-product');
//            $element_overview->set_key($product_id);
//            $app->add_br();


            // PICKED FOR YOU
//            $pickedforyou = $app->add_div()->add_class('container');
//            $pickedforyou->add('<h4 class="border-bottom-gray padding bold font-red">PRODUK TERKAIT</h4>');
//            $app->add_br();

            //$picked_for_you_list = deal::get_deal($org_id, $this->page(), $this->deal_pick_for_you);
            $picked_for_you_list = array();
            
            $q="
                select
                    pr.product_related_by_id as product_id
                from
                    product_related as pr
                    left join product as p on p.product_id=pr.product_id
                where
                    p.status>0
                    and pr.status>0
                    and p.is_active>0
                    and p.status_confirm='CONFIRMED'
                    and pr.product_id=".$db->escape($product_id)."
            ";    
            $r=$db->query($q);
            if($r->count()>0){
                foreach($r as $row){
                    $picked_for_you_list[]=product::get_product($row->product_id);
                }
            }
            $element_picked_for_you = $app->add_element('62hall-slide-product', 'picked-for-you');
            $element_picked_for_you->set_count_item(6)
                    ->set_page($this->page())
                    ->set_list_item($picked_for_you_list);

            $app->add_js("
                $('input').blur();
            ");
        } else {
            $container->add($error_message);
        }

        echo $app->render();
    }
}
