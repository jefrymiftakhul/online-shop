<?php

/**
 *
 * @author Riza 
 * @since  Nov 12, 2015
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class Shopping_Controller extends SixtyTwoHallFamilyController {

    private $store = NULL;
    private $domain = NULL;

    public function __construct() {
        parent::__construct();
        $org_id = $this->org_id();
        $data_org = org::get_org($org_id);

        if (!empty($data_org)) {
            $this->store = $data_org->name;
            $this->domain = $data_org->domain;
        }
    }

    public function index() {
        $app = CApp::instance();
        $user = $app->user();
        $org_id = $this->org_id();
        $menu_list = $this->menu_list();
        $link_list = $this->list_menu();

        // SEARCH 
        $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('search')
                ->set_url(curl::base() . "home/search");

        // SHOPPING CART 
        $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

        $product_category = product_category::get_product_category_menu($this->page);

        // Menu
        $menu_list = array(
            'name' => 'KATEGORI',
            'menu' => $product_category
        );

        $link_list = array(
            array(
                'label' => 'GOLD',
                'url' => '/gold/home',
            ),
            array(
                'label' => 'JASA',
                'url' => '/service/home',
            )
        );

        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_menu($menu_list)
                ->set_link($link_list);

        $container = $app->add_div()->add_class('container');

        $shopping_container = $container->add_div()->add_class('col-md-12 padding-0');

        $sidebar_page_container = $shopping_container->add_div()->add_class('col-md-4 padding-0 margin-top-30 margin-bottom-30');
        $sidebar_page = $sidebar_page_container->add_div()->add_class('col-md-11 padding-0 categories border-1 margin-10');

        $page = $shopping_container->add_div()->add_class('col-md-8');

        // menu page
        $menu_page = array(
            array(
                'title_menu' => 'Layanan Pelanggan',
                'menu' => array(
                    array(
                        'name' => 'shopping',
                        'label' => 'Belanja di ' . $this->store,
                        'url' => curl::base() . 'page/shopping',
                    ),
                    array(
                        'name' => 'buyproduct',
                        'label' => 'Beli Produk di ' . $this->store,
                        'url' => curl::base() . 'page/buyproduct',
                    ),
                    array(
                        'name' => 'buyservice',
                        'label' => 'Beli Jasa di ' . $this->store,
                        'url' => curl::base() . 'page/buyservice',
                    ),
                    array(
                        'name' => 'paymentinfo',
                        'label' => 'Informasi Pembayaran',
                        'url' => curl::base() . 'page/paymentinfo',
                    ),
                    array(
                        'name' => 'contantus',
                        'label' => 'Hubungi Kami',
                        'url' => curl::base() . 'page/contactus',
                    ),
                ),
            ),
            array(
                'title_menu' => $this->store,
                'menu' => array(
                    array(
                        'name' => 'aboutus',
                        'label' => 'Tentang ' . $this->store,
                        'url' => curl::base() . 'page/aboutus',
                    ),
                    array(
                        'name' => 'memberprofit',
                        'label' => 'Keuntungan Anggota',
                        'url' => curl::base() . 'page/memberprofit',
                    ),
                    array(
                        'name' => 'promo',
                        'label' => 'Promo ' . $this->store,
                        'url' => curl::base() . 'page/promo',
                    ),
                    array(
                        'name' => 'news',
                        'label' => 'Berita ' . $this->store,
                        'url' => curl::base() . 'page/news',
                    ),
                    array(
                        'name' => 'termsconditions',
                        'label' => 'Syarat dan Ketentuan',
                        'url' => curl::base() . 'page/termsconditions',
                    ),
                ),
            ),
        );

        $element_sidebar = $sidebar_page->add_element('62hall-page-sidebar')
                ->set_list_menu($menu_page)
                ->set_active('shopping');

        $page = $shopping_container->add_div()->add_class('col-md-8 padding-0 margin-top-40');

        $title = $page->add_div()->add_class('col-md-12 padding-0');
        $title->add_div()->add_class('font-size24 font-red bold col-md-5 padding-0')
                ->add(clang::__('Belanja di ') . $this->store);
        $title->add_div()
                ->add_class('col-md-7 padding-0 margin-top-20')
                ->custom_css('border-bottom', '3px dashed #ccc');

        $harga_murah = $page->add_div()->add_class('col-md-12 padding-0 margin-top-30 margin-bottom-20');
        $harga_murah_content = $harga_murah->add_div()->add_class('col-md-8');
        $harga_murah_content->add_div()->add_class('font-size24 font-red text-right')
                ->add(clang::__('HARGA MURAH') . '<br>' . clang::__('DAN BERSAING'));
        $harga_murah_content->add_div()->add_class('text-right')
                ->add(clang::__("Harga yang tertera di website " . $this->store . " merupakan harga 'up to date'") . '<br>' .
                        clang::__('dan murah. Kami senatiasa menawarkan fitur - fitur yang') . '<br>' .
                        clang::__('memanjakan Anda untuk belanja dengan harga - harga yang murah.'));
        $harga_murah_icon = $harga_murah->add_div()->add_class('col-md-4')
                ->custom_css('margin-top', '-10px')
                ->add('<center><img style="width:150px" src="' . curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/footer/i-03.png' . '" /></center>');

        $produk_lengkap = $page->add_div()->add_class('col-md-12 padding-0 margin-top-30 margin-bottom-20');
        $produk_lengkap_icon = $produk_lengkap->add_div()->add_class('col-md-4')
                ->custom_css('margin-top', '-10px')
                ->add('<center><img style="width:200px" src="' . curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/footer/i-02.png' . '" /></center>');
        $produk_lengkap_content = $produk_lengkap->add_div()->add_class('col-md-8');
        $produk_lengkap_content->add_div()->add_class('font-size24 font-red')
                ->add(clang::__('PRODUK LENGKAP') . '<br>' . clang::__('DAN BERKUALITAS'));
        $produk_lengkap_content->add_div()
                ->add(clang::__("Harga yang tertera di website " . $this->store . " merupakan harga 'up to date'") . '<br>' .
                        clang::__('dan murah. Kami senatiasa menawarkan fitur - fitur yang') . '<br>' .
                        clang::__('memanjakan Anda untuk belanja dengan harga - harga yang murah.'));

        $mudah_dan_handal = $page->add_div()->add_class('col-md-12 padding-0 margin-top-30 margin-bottom-20');
        $mudah_dan_handal_content = $mudah_dan_handal->add_div()->add_class('col-md-8');
        $mudah_dan_handal_content->add_div()->add_class('font-size24 font-red text-right')
                ->add(clang::__('MUDAH DAN') . '<br>' . clang::__('HANDAL'));
        $mudah_dan_handal_content->add_div()->add_class('text-right')
                ->add(clang::__($this->store . " menyediakan kemudahan belanja melalui shopping online") . '<br>' .
                        clang::__('klik buy dan menyediakan berbagai metode pembayaran, melalui') . '<br>' .
                        clang::__('Online Payment, Kartu Kredit, Bank Transfer, Bayar Tunai ditempat') . '<br>' .
                        clang::__('(COD), dan Cicilan 0%.'));
        $mudah_dan_handal_icon = $mudah_dan_handal->add_div()->add_class('col-md-4')
                ->custom_css('margin-top', '-10px')
                ->add('<center><img style="width:180px" src="' . curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/footer/i-04.png' . '" /></center>');

        $asuransi = $page->add_div()->add_class('col-md-12 padding-0 margin-top-30 margin-bottom-40');
        $asuransi_lengkap_icon = $asuransi->add_div()->add_class('col-md-4')
                ->custom_css('margin-top', '-10px')
                ->add('<center><img style="width:200px" src="' . curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/footer/i-01.png' . '" /></center>');
        $asuransi_content = $asuransi->add_div()->add_class('col-md-8');
        $asuransi_content->add_div()->add_class('font-size24 font-red')
                ->add(clang::__('ASURANSI') . '<br>' . clang::__('PENGIRIMAN'));
        $asuransi_content->add_div()
                ->add(clang::__($this->store . " menjaga keamanan produk dalam proses'") . '<br>' .
                        clang::__('pengiriman dengan standard packing yang aman. Dipastikan') . '<br>' .
                        clang::__('produk yang Anda beli sampai dalam kondisi yang baik.'));


        echo $app->render();
    }

}
