<?php

/**
 *
 * @author Riza 
 * @since  Nov 12, 2015
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class Home_Controller extends GoldController {

    private $_count_item = 12;
    // ADVERTISE 
    private $advertise_1 = 0;

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $app = CApp::instance();
        $user = $app->user();
        $org_id = $this->org_id();
        $link_list = $this->list_menu();

        // string

        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_link($link_list)
                ->set_absolute_menu('gold-search')
                ->set_position_absolute_menu('right');

        // SLIDE
        $slide_list = slide::get_slide($org_id, $this->page());

        $element_slide_show = $app->add_element('62hall-slide-show', 'element_slide_show');
        $element_slide_show->set_type('gold');
        $element_slide_show->set_slides($slide_list);

        $subcribe = $app->add_div()->add_class('bg-gray');
        $subcribe_container = $subcribe->add_div()->add_class('container padding-30');

        $message = $subcribe_container->add_div('message-subscribe-gold');

        $form_subscribe = $subcribe_container->add_form()
                ->add_class('form-62hallfamily');

        $form_subscribe->add_control('action_subscribe', 'hidden')
                ->set_value('page-gold');

        $form_subscribe->add('<div class="font-red bold font-size24 col-md-12">DAFTARKAN EMAIL ANDA</div>');

        $form_left = $form_subscribe->add_div()->add_class('col-md-5 font-size23')
                ->add('Dapatkan update harga harian logam mulia');
        $form_right = $form_subscribe->add_div()->add_class('col-md-7');

        $form_right_input = $form_right->add_div()->add_class('col-md-9');
        $form_right_input->add_control('email_subscribe_gold', 'text')
                ->set_placeholder('Enter your email address');

        $action = $form_right->add_div()->add_class('col-md-3');
        $action->add_action()
                ->set_label(clang::__('Daftar'))
                ->add_class('btn-auth btn-62hallfamily bg-red pull-right')
                ->custom_css('border-radius', '5px !important')
                ->custom_css('width', '100%')
                ->add_listener('click')
                ->add_handler('reload')
                ->add_param_input(array('email_subscribe_gold', 'action_subscribe'))
                ->set_target('message-subscribe-gold')
                ->set_url(curl::base() . 'subscribe/user_subscribe/gold');



        // PRODUCT GOLD
        $gold = $app->add_div()->add_class('bg-white');
        $gold_container = $gold->add_div()->add_class('container margin-30');
        $product_gold = $gold_container->add_div('page-product-gold')->add_class('col-md-8');
        $sidebar = $gold_container->add_div()->add_class('col-md-4');

        $product_gold->add_listener('ready')
                ->add_handler('reload')
                ->set_target('page-product-gold')
                ->set_url(curl::base() . "gold/home/get_page_product_gold/1");

        $list_price = gold_price::get_list_gold_price();

        $element_listprice = $sidebar->add_element('62hall-sidebar-listprice');
        $element_listprice->set_header('HARGA EMAS TERKINI')
                ->set_list_price($list_price);

        $quick_contact = quick_contact::get_quick_contact($org_id);
        if (count($quick_contact) > 0) {
            $element_listprice = $sidebar->add_element('62hall-sidebar-contact');
            $element_listprice->set_header('Quick Contact')
                    ->set_store($this->store())
                    ->set_address($quick_contact['address'])
                    ->set_phone($quick_contact['phone'])
                    ->set_bbm($quick_contact['pin_bb'])
                    ->set_wa($quick_contact['whatsapp'])
                    ->set_email($quick_contact['email'])
                    ->set_barcode($quick_contact['image_url']);
        }

        $app->add_js("$('input').blur();");

        echo $app->render();
    }

    //<editor-fold defaultstate="collapsed" desc="Pagination">
    public function get_count_page($total) {
        $limit = $this->_count_item;

        $count_page = (int) ($total / $limit);

        if ($total % $limit != 0) {
            $count_page += 1;
        }

        return $count_page;
    }

    public function get_start_limit($page) {
        $start = 0;
        if ($page > 1) {
            $start = ($page - 1) * $this->_count_item;
        }

        return $start;
    }

    //</editor-fold>

    public function get_page_product_gold($page) {
        $app = CApp::instance();
        $org_id = $this->org_id();
        $db = CDatabase::instance();

        $start = $this->get_start_limit($page);

        $arr_gold = array();
        $q = "
				select
					*
				from
					product_type
				where
					name='gold'
			";
        $r = $db->query($q);
        if ($r->count() > 0) {
            $q_base = "
					select
						*
					from
						product 
					where
						status>0												
						and is_active>0						
						and status_confirm='CONFIRMED'
						and product_type_id=" . $db->escape($r[0]->product_type_id) . "
					order by
										weight asc
									limit
										" . $start . "," . $this->_count_item . "
				";
            $r = $db->query($q_base);
            if ($r->count() > 0) {
                foreach ($r as $row) {
                    $buy = 'Sold Out';
                    if ($row->stock > 0) {
                        $buy = 'B : Rp. ' . ctransform::format_currency($row->sell_price);
                    }
                    $arr_gold[] = array(
                        "id" => $row->product_id,
                        "name" => $row->name,
                        "stock" => $row->stock,
                        "buy" => $buy,
                        "sell" => 'J : Rp. ' . ctransform::format_currency($row->purchase_price),
                        "image" => $row->image_name,
                    );
                }
            }
        }
        $total_item = count($arr_gold);
        $item_product_row = $app->add_div()->add_class('row');
        foreach ($arr_gold as $data_product) {
            $element_product = $app->add_element('62hall-gold-product', 'product-gold-' . $data_product['id']);
            $element_product->set_image($data_product['image'])
                    ->set_product_id($data_product['id'])
                    ->set_using_class_div_gold(true)
                    ->set_column(4)
                    ->set_name($data_product['name'])
                    ->set_stock($data_product['stock'])
                    ->set_price_sell($data_product['sell'])
                    ->set_price_buy($data_product['buy']);
        }

        // pagination
        $paginationcontainer = $app->add_div()
                ->add_class('pagination col-md-12');
        $paginationcontainer->add('<center>');

        $count_page = $this->get_count_page($total_item);

        if ($count_page > 0) {
            for ($i = 1; $i <= $count_page; $i++) {
                $class = NULL;
                if ($page == $i) {
                    $class = 'btn-pagination bg-red font-white';
                }
                $paginationcontainer->add_action()
                        ->set_label($i)
                        ->add_class($class)
                        ->add_listener('click')
                        ->add_handler('reload')
                        ->set_target('page-product-gold')
                        ->set_url(curl::base() . "gold/home/get_page_product_gold/" . $i);
            }
        }

        echo $app->render();
    }

}
