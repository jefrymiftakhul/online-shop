<?php

/**
 *
 * @author Riza 
 * @since  Nov 19, 2015
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class Product_Controller extends GoldController {

    CONST __CONTROLLER = "gold/product/";

    public function __construct() {
        parent::__construct();
    }

    public function set_session() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $post = $this->input->post();

        $transaction_type = carr::get($post, 'transaction_type');
        $product_id = carr::get($post, 'product_id');
        $qty = carr::get($post, 'qty');

        $error = 0;
        $error_message = '';

        if ($post == null) {
            $error++;
            $error_message = 'Please Fill Contact';
        }

        $arr_check = array(
            'post_code_shipping', 'districts_shipping', 'city_shipping', 'province_shipping', 'city_shipping', 'phone_shipping', 'address_shipping', 'email_shipping', 'name_shipping',
            'post_code_pay', 'districts_pay', 'city_pay', 'province_pay', 'city_pay', 'phone_pay', 'address_pay', 'email_pay', 'name_pay',
        );

        if ($error == 0) {
            $field = NULL;
            foreach ($arr_check as $key_post) {
                if (strlen(carr::get($post, $key_post)) == 0) {
                    if ($key_post == 'name_pay') {
                        $field = 'Nama Lengkap Pembayaran';
                    }
                    if ($key_post == 'email_pay') {
                        $field = 'Email Pembayaran';
                    }
                    if ($key_post == 'address_pay') {
                        $field = 'Alamat Pembayaran';
                    }
                    if ($key_post == 'phone_pay') {
                        $field = 'No. HP Pembayaran';
                    }
                    if ($key_post == 'province_pay') {
                        $field = 'Propinsi Pembayaran';
                    }
                    if ($key_post == 'city_pay') {
                        $field = 'Kota Pembayaran';
                    }
                    if ($key_post == 'districts_pay') {
                        $field = 'Kecamatan Pembayaran';
                    }
                    if ($key_post == 'post_code_pay') {
                        $field = 'Kode Pos Pembayaran';
                    }
                    if ($key_post == 'name_shipping') {
                        $field = 'Nama Lengkap Pengiriman';
                    }
                    if ($key_post == 'email_shipping') {
                        $field = 'Email Pengiriman';
                    }
                    if ($key_post == 'address_shipping') {
                        $field = 'Alamat Pengiriman';
                    }
                    if ($key_post == 'phone_shipping') {
                        $field = 'No. HP Pengiriman';
                    }
                    if ($key_post == 'province_shipping') {
                        $field = 'Propinsi Pengiriman';
                    }
                    if ($key_post == 'city_shipping') {
                        $field = 'Kota Pengiriman';
                    }
                    if ($key_post == 'districts_shipping') {
                        $field = 'Kecamatan Pengiriman';
                    }
                    if ($key_post == 'post_code_shipping') {
                        $field = 'Kode Pos Pengiriman';
                    }

                    $error++;
                    $error_message = $field . ' belum diisi.';
                }
            }
        }


        if ($error == 0) {
            $post_shipping_price = carr::get($post, 'shipping_price');
            if ($post_shipping_price == 'Rp. Maaf Pengiriman ke Wilayah Anda Tidak Tersedia') {
                $error++;
                $error_message = 'Maaf Pengiriman ke Kota Anda Tidak Tersedia';
            }
        }

        if ($error == 0) {
            if ($qty <= 0) {
                $error++;
                $error_message = "Maaf Quantity tidak boleh 0";
            }
        }
        if ($error == 0) {
            //get data post
            $session = CApiClientSession::factory('PG');
            $page = carr::get($post, 'page');
            $transaction_type = carr::get($post, 'transaction_type');

            $name_pay = carr::get($post, 'name_pay');
            $email_pay = carr::get($post, 'email_pay');
            $address_pay = carr::get($post, 'address_pay');
            $phone_area_pay = carr::get($post, 'phone_area_pay');
            $phone_pay = carr::get($post, 'phone_pay');
            $province_pay = carr::get($post, 'province_pay');
            $city_pay = carr::get($post, 'city_pay');
            $district_pay = carr::get($post, 'districts_pay');
            $post_code_pay = carr::get($post, 'post_code_pay');

            $name_shipping = carr::get($post, 'name_shipping');
            $email_shipping = carr::get($post, 'email_shipping');
            $address_shipping = carr::get($post, 'address_shipping');
            $phone_area_shipping = carr::get($post, 'phone_area_shipping');
            $phone_shipping = carr::get($post, 'phone_shipping');
            $province_shipping = carr::get($post, 'province_shipping');
            $city_shipping = carr::get($post, 'city_shipping');
            $district_shipping = carr::get($post, 'districts_shipping');
            $post_code_shipping = carr::get($post, 'post_code_shipping');
            $post_shipping_price = carr::get($post, 'shipping_price');
            if ($post_shipping_price == null) {
                $error++;
                $error_message = "Please Select Shipping";
            }
            $shipping_price = str_replace('Rp. ', '', $post_shipping_price);
            $shipping_price = ctransform::unformat_currency($shipping_price);
            $province_pay_text = '';
            $city_pay_text = '';
            $district_pay_text = '';
            $province_shipping_text = '';
            $city_shipping_text = '';
            $district_shipping_text = '';
            $product_id = carr::get($post, 'product_id');
            $qty = carr::get($post, 'qty');
            $check_same = carr::get($post, 'check_same');


            $transaction = array();
            $transaction['page'] = $page;
            $transaction['type'] = $transaction_type;
            $transaction['session_card_id'] = null;
            if ($province_pay) {
                $q = "
						select
							*
						from
							province
						where
							province_id=" . $province_pay . "
					";
                $r = $db->query($q);
                if ($r->count() > 0) {
                    $province_pay_text = $r[0]->name;
                }
            }
            if ($city_pay) {
                $q = "
						select
							*
						from
							city
						where
							city_id=" . $city_pay . "
					";
                $r = $db->query($q);
                if ($r->count() > 0) {
                    $city_pay_text = $r[0]->name;
                }
            }
            if ($district_pay) {
                $q = "
						select
							*
						from
							districts
						where
							districts_id=" . $district_pay . "
					";
                $r = $db->query($q);
                if ($r->count() > 0) {
                    $district_pay_text = $r[0]->name;
                }
            }


            if ($province_shipping) {
                $q = "
						select
							*
						from
							province
						where
							province_id=" . $province_shipping . "
					";
                $r = $db->query($q);
                if ($r->count() > 0) {
                    $province_shipping_text = $r[0]->name;
                }
            }
            if ($city_shipping) {
                $q = "
						select
							*
						from
							city
						where
							city_id=" . $city_shipping . "
					";
                $r = $db->query($q);
                if ($r->count() > 0) {
                    $city_shipping_text = $r[0]->name;
                }
            }
            if ($district_shipping) {
                $q = "
						select
							*
						from
							districts
						where
							districts_id=" . $district_shipping . "
					";
                $r = $db->query($q);
                if ($r->count() > 0) {
                    $district_shipping_text = $r[0]->name;
                }
            }

            $arr_contact = array();
            $arr_contact_billing = array();
            $arr_contact_shipping = array();
            $arr_name_pay = explode(' ', $name_pay, 2);
            $arr_name_shipping = explode(' ', $name_shipping, 2);
            $first_name_pay = $name_pay;
            $last_name_pay = '';
            $first_name_shipping = '';
            $last_name_shipping = '';
            if (count($arr_name_pay) == 2) {
                $first_name_pay = $arr_name_pay[0];
                $last_name_pay = $arr_name_pay[1];
            }
            if (count($arr_name_shipping) == 2) {
                $first_name_shipping = $arr_name_shipping[0];
                $last_name_shipping = $arr_name_shipping[1];
            }
            //handling billing
            if ($error == 0) {
                if (strlen($first_name_pay) > 20) {
                    $error++;
                    $error_message = 'Billing first name must be least than equal to 20 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($last_name_pay) > 20) {
                    $error++;
                    $error_message = 'Billing last name must be least than equal to 20 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($address_pay) > 200) {
                    $error++;
                    $error_message = 'Billing address must be least than equal to 200 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($city_pay_text) > 100) {
                    $error++;
                    $error_message = 'Billing city must be least than equal to 100 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($post_code_pay) > 10) {
                    $error++;
                    $error_message = 'Billing postal code must be least than equal to 10 characters.';
                }
            }
            if ($error == 0) {
                if (strlen($phone_area_pay . $phone_pay) > 19) {
                    $error++;
                    $error_message = 'Billing phone must be least than equal to 19 characters.';
                }
            }

            //handling shipping
            if ($error == 0) {
                if (!($city_shipping == '375') || ($city_shipping == '353')) {
                    $province_id = cdbutils::get_value("select province_id from city where city_id=" . $db->escape($city_shipping));
                    if (!$province_id == '17') {
                        $error++;
                        $error_message = 'Maaf Pengiriman ke Kota Anda Tidak Tersedia (Pengiriman hanya Surabaya, Sidoarjo dan Propinsi Bali)';
                    }
                }
            }
            if ($error == 0) {
                if (strlen($first_name_shipping) > 20) {
                    $error++;
                    $error_message = 'Shipping first name must be least than equal to 20 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($last_name_shipping) > 20) {
                    $error++;
                    $error_message = 'Shipping last name must be least than equal to 20 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($address_shipping) > 200) {
                    $error++;
                    $error_message = 'Shipping address must be least than equal to 200 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($city_shipping_text) > 100) {
                    $error++;
                    $error_message = 'Shipping city must be least than equal to 100 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($post_code_shipping) > 10) {
                    $error++;
                    $error_message = 'Shipping postal code must be least than equal to 10 characters.';
                }
            }
            if ($error == 0) {
                if (strlen($phone_area_shipping . $phone_shipping) > 19) {
                    $error++;
                    $error_message = 'Shipping phone must be least than equal to 19 characters.';
                }
            }

            if ($error == 0) {
                $arr_contact_billing['first_name'] = $first_name_pay;
                $arr_contact_billing['last_name'] = $last_name_pay;
                $arr_contact_billing['address'] = $address_pay;
                $arr_contact_billing['email'] = $email_pay;
                $arr_contact_billing['province_id'] = $province_pay;
                $arr_contact_billing['province'] = $province_pay_text;
                $arr_contact_billing['city_id'] = $city_pay;
                $arr_contact_billing['city'] = $city_pay_text;
                $arr_contact_billing['district_id'] = $district_pay;
                $arr_contact_billing['district'] = $district_pay_text;
                $arr_contact_billing['postal_code'] = $post_code_pay;
                $arr_contact_billing['phone'] = $phone_area_pay . $phone_pay;
                $arr_contact_billing['country_code'] = 'IDN';

                $arr_contact_shipping['first_name'] = $first_name_pay;
                $arr_contact_shipping['last_name'] = $last_name_pay;
                $arr_contact_shipping['address'] = $address_pay;
                $arr_contact_shipping['email'] = $email_pay;
                $arr_contact_shipping['province_id'] = $province_pay;
                $arr_contact_shipping['province'] = $province_pay_text;
                $arr_contact_shipping['city_id'] = $city_pay;
                $arr_contact_shipping['city'] = $city_pay_text;
                $arr_contact_shipping['district_id'] = $district_pay;
                $arr_contact_shipping['district'] = $district_pay_text;
                $arr_contact_shipping['postal_code'] = $post_code_pay;
                $arr_contact_shipping['phone'] = $phone_area_pay . $phone_pay;
                $arr_contact_shipping['country_code'] = 'IDN';

                if ($check_same == null) {
                    $arr_contact_shipping['first_name'] = $first_name_shipping;
                    $arr_contact_shipping['last_name'] = $last_name_shipping;
                    $arr_contact_shipping['address'] = $address_shipping;
                    $arr_contact_shipping['email'] = $email_shipping;
                    $arr_contact_shipping['province_id'] = $province_shipping;
                    $arr_contact_shipping['province'] = $province_shipping_text;
                    $arr_contact_shipping['city_id'] = $city_shipping;
                    $arr_contact_shipping['city'] = $city_shipping_text;
                    $arr_contact_shipping['district_id'] = $district_shipping;
                    $arr_contact_shipping['district'] = $district_shipping_text;
                    $arr_contact_shipping['postal_code'] = $post_code_shipping;
                    $arr_contact_shipping['phone'] = $phone_area_shipping . $phone_shipping;
                    $arr_contact_shipping['country_code'] = 'IDN';
                }

                $product_code = '';
                $product_name = '';
                $product_price = '';
                $product = product::get_product($product_id);
                if ($product) {
                    $item = array();
                    $product_name = $product['name'];
                    $product_code = $product['code'];
                    $product_price = $product['detail_price']['channel_sell_price'];
                    if ($page == 'gold') {
                        if ($transaction_type == 'sell') {
                            $product_price = $product['detail_price']['channel_purchase_price'];
                        }
                    }


                    $item['item_id'] = $product_id;
                    $item['item_code'] = $product_code;
                    $item['item_name'] = $product_name;
                    $item['qty'] = $qty;
                    $item['price'] = $product_price;
                    $transaction['item'][] = $item;

                    if ($page == 'gold') {
                        if ($transaction_type == 'sell') {
                            $product_id = NULL;
                            $product_code = 'DSG';
                            $product_name = 'Deal Sell Gold';
                            $product_price = 250000;
                            $qty = 1;
                        }
                    }
                    $item['item_id'] = $product_id;
                    $item['item_code'] = $product_code;
                    $item['item_name'] = $product_name;
                    $item['qty'] = $qty;
                    $item['price'] = $product_price;
                    $transaction['item_payment'][] = $item;
                    if ($shipping_price > 0) {
                        $item['item_id'] = NULL;
                        $item['item_code'] = 'SHP';
                        $item['item_name'] = 'Shipping to ' . $city_shipping_text;
                        $item['qty'] = 1;
                        $item['price'] = $shipping_price;
                        $transaction['item_payment'][] = $item;
                    }
                }
                $total_shipping = $shipping_price;
                $total_item = ($qty * $product_price);
                $grand_total = $total_item + $total_shipping;
                $amount_payment = $grand_total;
                if ($page == 'gold') {
                    if ($transaction_type == 'sell') {
                        $amount_payment = 250000;
                    }
                }
                $transaction['total_item'] = $total_item;
                $transaction['total_shipping'] = $total_shipping;
                $transaction['amount_payment'] = $amount_payment;

                $fee_shipping_text = 'Rp. ' . ctransform::format_currency($shipping_price);
                if ($shipping_price == 0) {
                    $fee_shipping_text = 'Gratis';
                }


                $arr_contact['first_name'] = $arr_contact_billing['first_name'];
                $arr_contact['last_name'] = $arr_contact_billing['last_name'];
                $arr_contact['email'] = $arr_contact_billing['email'];
                $arr_contact['phone'] = $arr_contact_billing['phone'];
                $arr_contact['billing_address'] = $arr_contact_billing;
                $arr_contact['shipping_address'] = $arr_contact_shipping;
                $arr_contact['same_billing_shipping'] = $check_same;
                $transaction['contact_detail'] = $arr_contact;
                $session->set('transaction', $transaction);
                curl::redirect('payment/form_payment');
            }
        }
        if ($error > 0) {
            //cmsg::add('error',$error_message );
            $session_php = Session::instance();
            $session_php->set('message_update_contact', $error_message);
            $session_php->set('error_update_contact', $error);

            $data_post = array();
            $data_post['name_pay'] = carr::get($post, 'name_pay');
            $data_post['email_pay'] = carr::get($post, 'email_pay');
            $data_post['address_pay'] = carr::get($post, 'address_pay');
            $data_post['phone_pay'] = carr::get($post, 'phone_pay');
            $data_post['province_pay'] = carr::get($post, 'province_pay');
            $data_post['city_pay'] = carr::get($post, 'city_pay');
            $data_post['districts_pay'] = carr::get($post, 'districts_pay');
            $data_post['post_code_pay'] = carr::get($post, 'post_code_pay');

            $data_post['name_shipping'] = carr::get($post, 'name_shipping');
            $data_post['email_shipping'] = carr::get($post, 'email_shipping');
            $data_post['address_shipping'] = carr::get($post, 'address_shipping');
            $data_post['phone_shipping'] = carr::get($post, 'phone_shipping');
            $data_post['province_shipping'] = carr::get($post, 'province_shipping');
            $data_post['city_shipping'] = carr::get($post, 'city_shipping');
            $data_post['districts_shipping'] = carr::get($post, 'districts_shipping');
            $data_post['post_code_shipping'] = carr::get($post, 'post_code_shipping');

            $session_php->set('post_update_contact', $data_post);


            curl::redirect('gold/product/' . $transaction_type . '/' . $product_id . '/' . $qty);
        }
    }

    public function sell($product_id = null, $qty = 0) {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $error = 0;
        $error_message = "";
        $session_php = Session::instance();
        if(ccfg::get("transaction_member_only")>0){
            $member_id = $session_php->get('member_id');
            if($member_id == null && strlen($member_id)==0){
                cmsg::add('error',clang::__("Harap login member sebelum melakukan transaksi"));
                curl::redirect('gold/home');
            }
        }
        $org_id = $this->org_id();
        $link_list = $this->list_menu();


        $data_set_value = array();
        if (count($session_php->get('post_update_contact'))) {
            $data_set_value = $session_php->get_once('post_update_contact');
        }

        $country_id = '';
        $q = "
				select
					*
				from
					country
				where
					name=" . $db->escape('Indonesia') . "
			";
        $r = $db->query($q);
        if ($r->count() > 0) {
            $country_id = $r[0]->country_id;
        }
        $q = "
				select
					p.name as product,
					p.*,
					pt.*
				from
					product as p
					inner join product_type as pt on pt.product_type_id=p.product_type_id
				where
					p.status>0										
					and p.is_active>0
					and p.status_confirm='CONFIRMED'
					and p.product_id=" . $db->escape($product_id) . "
					and pt.name=" . $db->escape($this->page()) . "
			";
        $product = $db->query($q);
        if ($product->count() == 0) {
            $error++;
            $error_message = "Product not found please select other product";
        }
        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_link($link_list);


        $err_code = $session_php->get_once('error_update_contact');
        $message = $session_php->get_once('message_update_contact');

        $app->add("<br>");

        if (!empty($message)) {
            $alert = 'success';
            if ($err_code > 0) {
                $alert = 'danger';
            }
            $container = $app->add_div()->add_class('container');
            $alert_message = $container->add_div()->add_class('alert alert-' . $alert)
                    ->add('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' . $message);
        }


        if ($error == 0) {
            $product = $product[0];
            $form = $app->add_form('form_transaction')->set_action(curl::base() . self::__CONTROLLER . 'set_session')->add_class('form-62hallfamily form-gold-contact padding-0');
            $form->add_control('page', 'hidden')->set_value('gold');
            $form->add_control('transaction_type', 'hidden')->set_value('sell');
            $form->add_control('product_id', 'hidden')->set_value($product->product_id);
            $form->add_control('product_transaction', 'hidden');
            $container = $form->add_div()->add_class('container');

            $product_image = $container->add_div()->add_class('col-md-4');
            $product_detail = $container->add_div()->add_class('col-md-8');

            $data_galery_product = product::get_product_image($product_id);

            $image = image::get_image_url($product->image_name, 'gold');

            $product_image->add("<img width='100%' src='" . $image . "'>");
            $product_detail->add("
					<p class='margin-bottom-nospace'><label class='font-size24'><b>" . $product->product . ' ' . $product->weight . ' gr' . "</b></label></p>
					<p class='margin-top-nospace margin-bottom-nospace'><label class='font-gray font-big'>KODE : " . $product->code . "</label></p>
					<p class='margin-top-nospace margin-bottom-nospace'><label class='font-big bold'>Harga Jual Satuan : Rp. " . ctransform::format_currency($product->purchase_price) . "</label></p>
					<br>
				");
            $div_container_qty = $product_detail->add_div()->add_class('col-md-12 padding-0');
            $div_qty = $div_container_qty->add_div()->add_class('col-md-2 padding-0');
            $control_qty = $div_qty->add_control('qty', 'text')->set_value($qty)
                    ->custom_css('width', '100px')
                    ->custom_css('height', '34px');
            ;
            $div_total = $div_container_qty->add_div()->add_class('col-md-10');
            $div_total->add("
					<label class='font-red font-size24'>Total : Rp. <span id='lbl_total'>" . ctransform::format_currency($qty * $product->purchase_price) . "</span></label>
				");
            $product_detail->add("<br>&nbsp<br>");

            $element_update_contact = $product_detail->add_element('62hall-update-contact', 'element_update_contact');
            $element_update_contact->set_country_id($country_id)
                    ->set_data_value($data_set_value);
            $element_update_contact->set_using_shipping(true);

            $div_submit = $product_detail->add_div('div_submit')->add_class('col-md-12');
            $submit_button = $div_submit->add_field()->add_action('submit_button')->set_label(clang::__('Lanjutkan'))->add_class('btn-62hallfamily margin-top-10 border-3-red bg-red pull-right');

            $product_detail->add("
					<br>&nbsp
				");

            $app->add_js("
					$('#qty').change(function(){
						$('#lbl_total').text($.cresenity.format_currency($(this).val()*" . $product->purchase_price . "));
					});
					$('#qty').TouchSpin({
						verticalbuttons: true
					});
					$('#submit_button').click(function(){
						$('#form_transaction').submit();
					});
				");
        }


        echo $app->render();
    }

    public function buy($product_id = "", $qty = "1") {
        if (strlen($qty) == 0) {
            curl::redirect(curl::base());
        }
        if (strlen($product_id) == 0) {
            curl::redirect(curl::base());
        }


        $app = CApp::instance();
        $db = CDatabase::instance();
        $error = 0;
        $error_message = "";
        $session_php = Session::instance();
        if(ccfg::get("transaction_member_only")>0){
            $member_id = $session_php->get('member_id');
            if($member_id == null && strlen($member_id)==0){
                cmsg::add('error',clang::__("Harap login member sebelum melakukan transaksi"));
                curl::redirect('gold/home');
            }
        }

        $org_id = $this->org_id();
        $link_list = $this->list_menu();


        $data_set_value = array();
        if (count($session_php->get('post_update_contact'))) {
            $data_set_value = $session_php->get_once('post_update_contact');
        }

        $country_id = '';
        $q = "
				select
					*
				from
					country
				where
					name=" . $db->escape('Indonesia') . "
			";
        $r = $db->query($q);
        if ($r->count() > 0) {
            $country_id = $r[0]->country_id;
        }
        $q = "
				select
					p.name as product,
					p.*,
					pt.*
				from
					product as p
					inner join product_type as pt on pt.product_type_id=p.product_type_id
				where
					p.status>0					
					and p.is_active>0					
					and p.status_confirm='CONFIRMED'
					and p.product_id=" . $db->escape($product_id) . "
					and pt.name=" . $db->escape($this->page()) . "
			";
        $product = $db->query($q);
        if ($product->count() == 0) {
            $error++;
            $error_message = "Product not found please select other product";
        }
        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_link($link_list);

        $err_code = $session_php->get_once('error_update_contact');
        $message = $session_php->get_once('message_update_contact');

        $app->add("<br>");

        if (!empty($message)) {
            $alert = 'success';
            if ($err_code > 0) {
                $alert = 'danger';
            }
            $container = $app->add_div()->add_class('container');
            $alert_message = $container->add_div()->add_class('alert alert-' . $alert)
                    ->add('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' . $message);
        }

        if ($error == 0) {
            $product = $product[0];

            $form = $app->add_form('form_transaction')->set_action(curl::base() . self::__CONTROLLER . 'set_session')->add_class('form-62hallfamily form-gold-contact padding-0');
            $form->add_control('page', 'hidden')->set_value('gold');
            $form->add_control('transaction_type', 'hidden')->set_value('buy');
            $form->add_control('product_id', 'hidden')->set_value($product->product_id);
            $container = $form->add_div()->add_class('container');

            $product_image = $container->add_div()->add_class('col-md-4');
            $product_detail = $container->add_div()->add_class('col-md-8');

            $data_galery_product = product::get_product_image($product_id);

            $image = image::get_image_url($product->image_name, 'gold');

            $product_image->add("<img width='100%' src='" . $image . "'>");
            $product_detail->add("
					<p class='margin-bottom-nospace'><label class='font-size24'>" . $product->product . ' ' . $product->weight . ' gr' . "</label></p>
					<p class='margin-top-nospace margin-bottom-nospace'><label class='font-gray font-big'>KODE : " . $product->code . "</label></p>
					<p class='margin-top-nospace margin-bottom-nospace'><label class='font-big bold'>Harga Beli Satuan : Rp. " . ctransform::format_currency($product->sell_price) . "</label></p>
					<br>
				");
            $div_container_qty = $product_detail->add_div()->add_class('col-md-12 padding-0');
            $div_qty = $div_container_qty->add_div()->add_class('col-md-2 padding-0');
            $control_qty = $div_qty->add_control('qty', 'text')->set_value($qty)
                    ->custom_css('width', '100px')
                    ->custom_css('height', '34px');
            $div_total = $div_container_qty->add_div()->add_class('col-md-10');
            $div_total->add("
					<label class='font-red font-size24'>Total : Rp. <span id='lbl_total'>" . ctransform::format_currency($qty * $product->sell_price) . "</span></label>
				");
            $product_detail->add("<br>&nbsp<br>");
            $element_update_contact = $product_detail->add_element('62hall-update-contact', 'element_update_contact');
            $element_update_contact->set_country_id($country_id)
                    ->set_data_value($data_set_value);

            $div_submit = $product_detail->add_div('div_submit')->add_class('col-md-12');
            $submit_button = $div_submit->add_field()->add_action('submit_button')->set_label(clang::__('Lanjutkan'))->add_class('btn-62hallfamily margin-top-10 border-3-red bg-red pull-right');

            $product_detail->add("
					<br>&nbsp
				");

            $app->add_js("
					$('#qty').change(function(){
						$('#lbl_total').text($.cresenity.format_currency($(this).val()*" . $product->sell_price . "));
					});
					$('#qty').TouchSpin({
						verticalbuttons: true
					});
					$('#submit_button').click(function(){
						$('#form_transaction').submit();
					
					});
				");
        }

        echo $app->render();
    }

}
