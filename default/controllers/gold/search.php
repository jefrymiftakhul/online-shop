<?php

/**
 *
 * @author Riza 
 * @since  Jan 27, 2016
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class Search_Controller extends GoldController {

    private $_count_item = 12;
    // ADVERTISE 
    private $advertise_1 = 0;

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $user = $app->user();
        $org_id = $this->org_id();
        $link_list = $this->list_menu();

        $request = array_merge($_GET, $_POST);
        $request_category_id = carr::get($request, 'category', 'All');
        $request_weight = carr::get($request, 'gold_weight', 'All');

        $options = array();
        $options['category_id'] = $request_category_id;
        $options['gold_weight'] = $request_weight;


        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_link($link_list)
                ->set_absolute_menu('gold-search')
                ->set_position_absolute_menu('right')
                ->set_options($options);

        // SLIDE
        $slide_list = slide::get_slide($org_id, $this->page());

        $element_slide_show = $app->add_element('62hall-slide-show', 'element_slide_show');
        $element_slide_show->set_type('gold');
        $element_slide_show->set_slides($slide_list);

        $subcribe = $app->add_div()->add_class('bg-gray');
        $subcribe_container = $subcribe->add_div()->add_class('container padding-30');

        $message = $subcribe_container->add_div('message-subscribe-gold');

        $form_subscribe = $subcribe_container->add_form()
                ->add_class('form-62hallfamily');

        $form_subscribe->add_control('action_subscribe', 'hidden')
                ->set_value('page-gold');

        $form_subscribe->add('<div class="font-red bold font-size24 col-md-12">DAFTARKAN EMAIL ANDA</div>');

        $form_left = $form_subscribe->add_div()->add_class('col-md-5 font-size23')
                ->add('Dapatkan update harga harian logam mulia');
        $form_right = $form_subscribe->add_div()->add_class('col-md-7');

        $form_right_input = $form_right->add_div()->add_class('col-md-9');
        $form_right_input->add_control('email_subscribe_gold', 'text')
                ->set_placeholder('Enter your email address');

        $action = $form_right->add_div()->add_class('col-md-3');
        $action->add_action()
                ->set_label(clang::__('Daftar'))
                ->add_class('btn-auth btn-62hallfamily bg-red pull-right')
                ->custom_css('border-radius', '5px !important')
                ->custom_css('width', '100%')
                ->add_listener('click')
                ->add_handler('reload')
                ->add_param_input(array('email_subscribe_gold', 'action_subscribe'))
                ->set_target('message-subscribe-gold')
                ->set_url(curl::base() . 'subscribe/user_subscribe/gold');

        // PRODUCT GOLD
        $gold = $app->add_div()->add_class('bg-white');
        $gold_container = $gold->add_div()->add_class('container margin-30');
        $product_gold = $gold_container->add_div('page-product-gold')->add_class('col-md-12');


        $product_gold->add_listener('ready')
                ->add_handler('reload')
                ->set_target('page-product-gold')
                ->set_url(curl::base() . "gold/search/get_page_product_gold/1/" . $request_category_id . "/" . $request_weight);

        $app->add_js("$('input').blur();");

        echo $app->render();
    }

    //<editor-fold defaultstate="collapsed" desc="Pagination">
    public function get_count_page($total) {
        $limit = $this->_count_item;

        $count_page = (int) ($total / $limit);
        if ($total % $limit != 0) {
            $count_page += 1;
        }

        return $count_page;
    }

    public function get_start_limit($page) {
        $start = 0;
        if ($page > 1) {
            $start = ($page - 1) * $this->_count_item;
        }

        return $start;
    }

    //</editor-fold>

    public function get_page_product_gold($page, $category = 'All', $weight = 'All') {
        $app = CApp::instance();
        $org_id = $this->org_id();
        $db = CDatabase::instance();

        $start = $this->get_start_limit($page);
        $arr_gold = array();
        $q = "
                select
                        *
                from
                        product_type
                where
                        name='gold'
                ";
        $r = $db->query($q);
        if ($r->count() > 0) {
            $q_base = "
                        select
                                *
                        from
                                product 
                        where
                                status>0
								and is_active>0								
                                and status_confirm='CONFIRMED'
                                and product_type_id=" . $db->escape($r[0]->product_type_id);

            if ($category != 'All') {
                $q_base .= ' and product_category_id=' . $db->escape($category);
            }
            if ($weight != 'All') {
                $q_base .= ' and weight=' . $db->escape($weight);
            }

            $q_base .= " order by
                                    weight asc
                                    limit
                                    " . $start . "," . $this->_count_item . "
                                ";

            $r = $db->query($q_base);
            if ($r->count() > 0) {
                foreach ($r as $row) {
                    $arr_gold[] = array(
                        "id" => $row->product_id,
                        "name" => $row->name,
                        "stock" => $row->stock,
                        "buy" => 'B : Rp. ' . ctransform::format_currency($row->sell_price),
                        "sell" => 'J : Rp. ' . ctransform::format_currency($row->purchase_price),
                        "image" => $row->image_name,
                    );
                }
            }
        }
        $total_item = count($arr_gold);

        $item_product = $app->add_div()->add_class('row');
        foreach ($arr_gold as $data_product) {

            $element_product = $app->add_element('62hall-gold-product', 'product-gold-' . $data_product['id']);
            $element_product->set_image($data_product['image'])
                    ->set_product_id($data_product['id'])
                    ->set_name($data_product['name'])
                    ->set_using_class_div_gold(true)
                    ->set_stock($data_product['stock'])
                    ->set_column(4)
                    ->set_price_sell($data_product['sell'])
                    ->set_price_buy($data_product['buy']);
        }

        // pagination
        $paginationcontainer = $app->add_div()
                ->add_class('pagination col-md-12');
        $paginationcontainer->add('<center>');

        $count_page = $this->get_count_page($total_item);
        if ($count_page > 0) {
            for ($i = 1; $i <= $count_page; $i++) {
                $class = 'font-blank';
                if ($page == $i) {
                    $class = 'btn-pagination bg-red font-white';
                }
                $paginationcontainer->add_action()
                        ->set_label($i)
                        ->add_class($class)
                        ->add_listener('click')
                        ->add_handler('reload')
                        ->set_target('page-product-gold')
                        ->set_url(curl::base() . "gold/search/get_page_product_gold/" . $i . '/' . $category . '/' . $weight);
            }
        }

        echo $app->render();
    }

}
