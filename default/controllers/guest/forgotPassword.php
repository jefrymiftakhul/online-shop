<?php

class Controller_Guest_ForgotPassword extends SixtyTwoHallFamilyController {

    CONST __CONTROLLER = 'guest/forgotPassword/';

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $app = CApp::instance();
        $db = CDatabase::instance();

        $err_code = 0;
        $err_message = "";

        $email = '';

        $post = $this->input->post();
        if ($post != NULL) {
            $email = carr::get($post, 'email');
            $check = cdbutils::get_row("SELECT * FROM member WHERE email = " . $db->escape($email) . " AND status > 0 AND is_verified = 1");
            if ($check != NULL) {

                $member_id = $check->member_id;
                $name = $check->name;
                $pass = $this->__generate_password();


                $html_content = '<p>Your new password.</p>
                                    <table>
                                        <tr>
                                            <td>Account Name</td>
                                            <td> : </td>
                                            <td>' . $name . '</td>
                                        </tr>
                                        <tr>
                                            <td>Email Account</td>
                                            <td> : </td>
                                            <td>' . $email . '</td>
                                        </tr>
                                        <tr>
                                            <td>New Password</td>
                                            <td> : </td>
                                            <td>' . $pass . '</td>
                                        </tr>
                                    </table>
                                    <small>For security reason, please log-in then change your password.</small>';

                try {
                    $this->send_email($email, array(
                        'logo' => curl::base() . 'application/62hallfamily/default/media/img/logo.png',
                        'subject' => ccfg::get('title') . ' - Reset Password!',
                        'content' => $html_content
                            )
                    );

                    $data = array(
                        'password' => md5($pass)
                    );

                    $update = $db->update("member", $data, array('member_id' => $member_id));
                } catch (Exception $e) {
                    $err_code++;
                    $err_message = $e->getMessage();
                }
            } else {
                $err_code++;
                $err_message = "Email tidak ditemukan!";
            }

//            if ($err_code == 0) {
//                curl::redirect(self::__CONTROLLER.'success/'.$email);
//                cmsg::add('Success', 'Your new password email has been successfully delivered');
//            }
//            else {
//                cmsg::add('error', $err_message);
////                curl::redirect(self::__CONTROLLER);
//            }
        } else {
            $err_code++;
            $err_message = "Email tidak boleh kosong";
        }

//        $form = $app->add_form()->add_class('form-62hallfamily');
//        $div = $form->add_div()->add_class('container');
//        $container = $div->add_div()->add_class('col-md-12');
//        
//        $div_0 = $form->add_div()->add_class('container');
//        $div_info = $div_0->add_div()->add_class('col-md-12');
//        $div_info->add('<div class="font-size20">Silahkan masukkan email anda, kami akan mengirimkan password baru.</div><small class="font-red">Pastikan email anda valid.</small>');
//        
//        $div_1 = $form->add_div()->add_class('container');
//        $left_div_1 = $div_1->add_div()->add_class('col-md-5');
//        $left_div_1->add_field()->set_label(clang::__("Your Email").' <red>*</red>')->add_control('email', 'text')->set_value($email)->add_validation('required')->add_class('col-md-12 col-xs-12');
//        
//        $div_action = $form->add_div()->add_class('container');
//        $full_div = $div_action->add_div()->add_class('col-md-5');
//        $full_div->add_action()->set_submit(true)->set_label(clang::__("Send"))->set_confirm(true)->add_class('btn-62hallfamily bg-red pull-right margin-top-10 margin-bottom-10');
//        
//        echo $app->render();

        $return = array(
            'error' => $err_code,
            'message' => $err_message,
            'email' => $email,
        );

        echo cjson::encode($return);
    }

//    
//    public function success($email) {
//        $app = CApp::instance();
//        
//        $div_form = $app->add_div()->add_class('row');
//        $container = $div_form->add_div()->add_class('col-md-5 col-center');
//        
//        $div_form_top = $container->add_div()->add_class('row');
//        $container_top = $div_form_top->add_div()->add_class('col-md-12 text-center');
//        $container_top->add('<span><i class="icon-check-big"></i></span>');
//        
//        $div_0 = $container->add_div()->add_class('row');
//        $div_info = $div_0->add_div()->add_class('col-md-12');
//        $div_info->add('<p class="text-success">An email for new password has been successfully delivered to '.$email.'</p><small class="text-success">Please check your email.</small>');
//        
//        echo $app->render();
//    }
//    
    public function email_format($base_class) {
        $classname = ucfirst($base_class) . "Email";
        $file = CF::get_files('libraries', "FormatEmail/" . $classname);
        include_once $file[0];
        $tbclass = new $classname();
        return $tbclass;
    }

    private function send_email($recipient, $data) {
        $tbclass = $this->email_format("forgetpassword");
        $format = $tbclass::format($recipient, $data);
        return cmail::send_smtp($recipient, $format['subject'], $format['message']);
    }

    private function __generate_password() {
        $str = str_split("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
        shuffle($str);
        $salt = '';
        foreach (array_rand($str, 8) as $key) {
            $salt .= $str[$key];
        }
        return $salt;
    }

}
