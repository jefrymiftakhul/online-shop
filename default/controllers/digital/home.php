<?php

/**
 *
 * @author Riza 
 * @since  Nov 12, 2015
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class Home_Controller extends DigitalController {

    public function __construct() {
        parent::__construct();
        kinerjamall::kp_api_init();
    }
    
    public function index() {
        $app = CApp::instance();
        
        echo $app->render();
    }

    public function category($category_id = '') {
        
        $app       = CApp::instance();
        $user      = $app->user();
        $db        = CDatabase::instance();
        $org_id    = $this->org_id();
        $menu_list = $this->menu_list();
        $link_list = $this->list_menu();
        CFBenchmark::start('benchmark_category');

        // SHOPPING CART
        $app->add_listener('ready')
            ->add_handler('reload')
            ->set_target('shopping-cart')
            ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

        // Menu
        $data_product_category = product_category::get_product_category_menu($this->page());
        cdbg::var_dump($data_product_category);
        die(__METHOD__);

        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_menu($menu_list);
        $element_menu->set_link($link_list);

        $container_menu_sm = $app->add_div()->add_class('visible-xs visible-sm container');

        $container               = $app->add_div()->add_class('container');
        $category_menu_container = $container->add_div()->add_class('col-md-4 padding-0 margin-30');
        $category_menu           = $category_menu_container->add_div()->add_class('categories hidden-sm hidden-xs border-1 padding-left-right10');
        $category_container      = $container->add_div()->add_class('col-md-8 margin-30 padding-0');

        $first_parent = product_category::get_first_parent_category($category_id);
        
        if ($first_parent == null) {
            curl::redirect(curl::base());
            return;
        }
        // CATEGORY ADVERTISE
        $list_image_cms_product_category = cms_product_category::get_cms_product_category($org_id, $category_id);
        $advertise_category              = null;
        if (count($list_image_cms_product_category) == 0 && $first_parent != null) {
            $list_image_cms_product_category = cms_product_category::get_cms_product_category($org_id, $first_parent->product_category_id);
        }

        if (count($list_image_cms_product_category) > 0) {

            $advertise_category         = $category_container->add_div('advertise-category')->add_class('col-md-12 padding-0');
            $element_advertise_category = $advertise_category->add_element('62hall-category-advertise', 'category-advertise');
            $element_advertise_category->set_list_advertise($list_image_cms_product_category);
        }

        $title                     = $category_container->add_div()->add_class('col-md-12 padding-0');
        $data_category_menu_choise = product_category::get_parent_category($category_id);

        $data_category_menu_choise_name = '';
        if ($data_category_menu_choise != null) {
            $data_category_menu_choise_name = $data_category_menu_choise->name;
        }
        $title->add('<h4 class="border-bottom-gray bold">' . $data_category_menu_choise_name . '</h4>');

        $product_category = $category_container->add_div('page-product-category')->add_class('col-md-12 padding-0');

        // CATEGORY MENU

        $category_menu_list    = product_category::get_product_category_menu($this->page(), $first_parent->product_category_id);
        $child_menu_list       = product_category::get_product_category_menu($this->page(), $category_id);
        $element_category_menu = $category_menu->add_element('62hall-category-menu', 'category-menu-' . $category_id);
        $element_category_menu->set_key($category_id)
                              ->set_head_key($first_parent->product_category_id)
                              ->set_head_menu($first_parent->name)
                              ->set_list_menu($category_menu_list);

        $list = array();
        if (count($child_menu_list) > 0) {
            $list[''] = "Pilih Subcategory ...";
            foreach ($child_menu_list as $key => $value) {
                $sm_category_id = carr::get($value, 'category_id');
                $sm_name        = carr::get($value, 'name');

                $list[$sm_category_id] = $sm_name;
            }
        }

        $container_menu_sm_wrapper = $container_menu_sm->add_div()->add_class('menu-sm-wrapper col-md-12');
        $container_menu_sm_wrapper->add('<h4 class="bold">' . $data_category_menu_choise_name . '</h4>');

        if (count($child_menu_list) > 0) {
            $control  = $container_menu_sm_wrapper->add_div()->add_class('form-62hallfamily')->add_control('sm_category_select', 'select')->add_class('select-category-menu select-62hallfamily')->set_list($list)->set_value($category_id);
            $listener = $control->add_listener('change');
            $listener->add_handler('custom')
                     ->set_js("
                        var sm_category = jQuery('#sm_category_select').val();
                        window.location = '" . curl::base() . "product/category/'+sm_category;
                    ");
        }

        // CATEGORY FILTER
        //$data_product = cms_product::get_page_category($org_id, $this->page(), $category_id);

        $options = array(
            'product_type'        => $this->page(),
            'product_category_id' => $category_id,
            'visibility'          => 'catalog',
        );
//        $products = product::get_result($options);
        $list_attribute = product::get_attributes_array($options);

        $benchmark = CFBenchmark::get('benchmark_category');
        if (isset($_GET['benchmark'])) {
            cdbg::var_dump($benchmark);
        }
        $min_price = product::get_min_price($options);
        $max_price = product::get_max_price($options);
        if ($min_price == null) {
            $min_price = 0;
        }
        if ($max_price == null) {
            $max_price = 0;
        }
        $range_price = array(
            'min' => $min_price,
            'max' => $max_price,
        );

        $list_filter_product = array(
            'diskon'    => clang::__('Diskon'),
            'min_price' => clang::__('Termurah'),
            'max_price' => clang::__('Termahal'),
            'is_new'    => clang::__('Terbaru'),
            'popular'   => clang::__('Popular'),
            'A-Z'       => clang::__('A-Z'),
            'Z-A'       => clang::__('Z-A'),
        );
        $q = "
                select
                    *
                from
                    product_category
                where
                    product_category_id=" . $db->escape($category_id) . "
            ";
        $r            = $db->query($q);
        $category_lft = '';
        $category_rgt = '';
        if ($r->count() > 0) {
            $row          = $r[0];
            $category_lft = $row->lft;
            $category_rgt = $row->rgt;
        }
        $category_filter = $category_menu_container->add_div()->add_class('categories border-1 padding-left-right10 margin-top-30');
        $element_filter  = $category_filter->add_element('62hall-filter', 'category-filter')
                                           ->set_key($category_id)
                                           ->set_list_filter_product($list_filter_product)
                                           ->set_list_filter($list_attribute)
                                           ->set_product_visibility('catalog')
                                           ->set_filter_category_lft($category_lft)
                                           ->set_filter_category_rgt($category_rgt)
                                           ->set_filter_page($this->page())
                                           ->set_min_price($range_price['min'])
                                           ->set_max_price($range_price['max']);

        $filter_name = '';
        $product_category->add_listener('ready')
                         ->add_handler('reload')
                         ->set_target('page-product-category')
                         //->add_param_input('arr_product_id')
                         ->set_url(curl::base() . "reload/reload_product_filter?source=category&visibility=catalog&sortby=diskon&filter_page=" . $this->page() . '&filter_category_lft=' . $category_lft . '&filter_category_rgt=' . $category_rgt);

        echo $app->render();
    }
    
    public function tab_pulsa(){
        $app = CApp::instance();
        $db = CDatabase::instance();
        
        $k = $app->add_div()->add_class("input-box");
        $msg = cmsg::flash_all();
                if (strlen($msg) > 0) {
                     $k->add($msg);
                     $k->add_js("
                             $(document).ready(function() {
                                    $('.alert').delay(10000).slideUp();
                              });
                             ");
                }
        $k->add_control("number_input_pulsa", "text")->set_placeholder("No. Handphone")->add_class("pulsa-input-text number-input"); 
        $k->add_control("operator_name", "hidden");
        $k->add_div()->add_class("provider-logo")->custom_css("display", "inline")
          ->add_div()->add_class("img-logo")->add("<span id='logo-provider'></span>");
        $app->add_div("price_div")->add_class("input-box"); 
        $app->add_div("result_div"); 
        
        
        $a = '$(".number-input").on("keyup change",function(){
            this.value = this.value.replace(/[^0-9\.]/g,"");
            var input_number = $("#number_input_pulsa").val();
            var number_code = input_number.slice(0,4);';
        $pulsa = $db->query("select * from provider where provider_type='pulsa' and status > 0 ");
        if (count($pulsa) > 0){
            foreach ($pulsa as $key => $val) {
                $name = cobj::get($val, "var_name");
                $low_name =  strtolower($name);
                $prefix = cobj::get($val, "prefix");
                 $a .= 'var '.$low_name.'_code = new Array('.$prefix.');
                 for (var i = 0; i < '.$low_name.'_code.length; i++) {

                        if ('.$low_name.'_code.indexOf( number_code )>=0) {
                            $("#logo-provider").addClass("prov-'.$low_name.'");
                            $("#operator_name").val("'.$name.'");

                        }
                        else{
                            $("#logo-provider").removeClass("prov-'.$low_name.'");
                        }
                    }';
            }
        }
     
        $a .= 'var check_prov = document.getElementById("logo-provider").className;
   //            console.log(check_prov);
               if (check_prov!=""){
                   get_data();
               }
               else{
                   $("#price_div").html("");
                   $("#result_div").html("");
               }
           });';
        $a .='function get_data(){
               var check_prov = document.getElementById("logo-provider").className;
               if (check_prov!=""){
                  var price_selected = $("#code").val();
               }

                $.ajax({
                   url:"/pulsa/home/reload_price/price_list",
                   data:{
                       operator_name:$("#operator_name").val(),
                       price_select : price_selected,
                   },
                   type:"GET",
                   dataType:"json",
                           success: function(data){
                                       $("#price_div").html(data.html);
                                       var script = $.cresenity.base64.decode(data.js);
                                       eval(script);  
                                   },
                           error:function(data){

                                    },
               });
           }';
        $app->add_js($a);
        echo $app->render();
    }
    
    public function tab_game() {
        $app = CApp::instance();
        
        $app->add_div()->add_class("input-box")->add_control("number_input_game", "text")->set_placeholder("No. Handphone")->add_class("pulsa-input-text number-input"); 
        
        $control_game_provider=$app->add_div("game_price_div")->add_class("input-box")->add_control("game-provider", "provider-select")->set_type('game_online')->add_class("select-pulsa");
        $control_game_provider->add_listener('change')
                ->add_handler('reload')
                ->set_target('div_game_nominal')
                ->add_param_input(array('game-provider'))
                ->set_url(curl::base().'pulsa/home/reload_game_product');
        $control_game_provider->add_listener('ready')
                ->add_handler('reload')
                ->set_target('div_game_nominal')
                ->add_param_input(array('game-provider'))
                ->set_url(curl::base().'pulsa/home/reload_game_product');
        $app->add_div("div_game_nominal")->add_class("input-box");
        $app->add_div("div_game_result"); 
        
        echo $app->render();
    }

    public function reload_game_product(){
        $app=CApp::instance();
        $db=CDatabase::instance();
        $get=$_GET;
        $provider_id=carr::get($get,'game-provider');
        $control_game_product=$app->add_control("game_product_id", "product-select")->set_placeholder("Product")->add_class("select-pulsa")->set_provider_id($provider_id); 
        $control_game_product->add_listener('change')
                        ->add_handler('reload')
                        ->set_target('div_game_result')
                        ->set_url('/pulsa/home/reload_price/total?type=game_online&provider_id='.$provider_id)
                        ->add_param_input(array('game_product_id'));
        $control_game_product->add_listener('ready')
                        ->add_handler('reload')
                        ->set_target('div_game_result')
                        ->set_url('/pulsa/home/reload_price/total?type=game_online&provider_id='.$provider_id)
                        ->add_param_input(array('game_product_id'));
        
        echo $app->render();
    }
    
    public function reload_price($type=null) {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();
        $get = $_GET;
        
        $err_code = 0;
        $err_message = '';

        $operator_name = carr::get($get, "operator_name");
        $product_category_code = 'pulsa';
        $product_code = 'RB';
        if (ccfg::get('pulsa_product_code') != NULL) {
            $product_code = ccfg::get('pulsa_product_code');
        }
        
        $method = 'get_price';
        
        if ($type == "price_list"){
             $price_select = carr::get($get, "price_select");
             if ($price_select != null){
                 return false;
             }
             $provider_id  = cdbutils::get_value("select provider_id from provider where var_name ='".$operator_name."'");
            
             $provider = $db->query("select * from product where provider_id =".$provider_id);
             if ($product_code == 'KY') {
                 $provider = $db->query("SELECT sku_ky AS sku, name FROM product WHERE status > 0 AND provider_id =".$db->escape($provider_id)." AND sku_ky IS NOT NULL");
             }
             
             $data_arr = array();
             $none = array("none"=>"Select nominal");
             foreach ($provider as $k_prov => $v_prov){
                 $code = cobj::get($v_prov,"sku");
                 $name = cobj::get($v_prov,"name");
                 $data_arr[$code] =  $name;                 
             }
            $data_arr = array_merge($none,$data_arr);
             
            if (strlen($operator_name) > 0){
                $a = $app->add_control("code", "select")->add_class("select-pulsa")->set_list($data_arr)->set_value("none");
                $a->add_listener('change')
                        ->add_handler('reload')
                        ->set_target('result_div')
                        ->set_url('/pulsa/home/reload_price/total')
                        ->add_param_input(array('code'));
//                $a->add_listener('ready')
//                        ->add_handler('reload')
//                        ->set_target('total_div')
//                        ->set_url('/pulsa/home/reload_price/total')
//                        ->add_param_input(array('code'));
            }
            
        }
        elseif($type == "total"){
            $code = carr::get($get, "code");
            $type_tab = carr::get($get, "type");
            $provider_id=carr::get($get,"provider_id");
            $price_pulsa=null;
            if($type_tab=='game_online'){
                $product_id=carr::get($get,"game_product_id");
                $product=cdbutils::get_row("select * from product where product_id=".$product_id);
                $code=cobj::get($product,'sku');
            }
            if ($code != "none"){
                if($provider_id ==null){
                    $provider_id = cdbutils::get_value("select provider_id from product where sku ='".$code."' limit 1");
                    if ($product_code == 'KY') {
                        $provider_id = cdbutils::get_value("select provider_id from product where sku_ky =".$db->escape($code)." limit 1");
                    }
                }
                $provider = cdbutils::get_row("select * from provider where provider_id =".$provider_id);

                $method = 'get_price';
                $request['operator_name'] = cobj::get($provider,"var_name");
                $request['product_category_code'] = cobj::get($provider,"provider_type");
                
                $response = pulsa_api::api($method, $product_code, $request);
                $api_err_code = carr::get($response, 'err_code');
                
                if ($api_err_code > 0) {
                    $err_code = $api_err_code;
                    $err_message = 'Failed to connect server, please try again later ['.$api_err_code.']';
                    $err_message = carr::get($response, 'err_message');
                }
                
                if ($err_code == 0) {
                    $data = carr::get($response, "data");
                    $data = carr::get($data, "products");

                    foreach ($data as $key => $value) {
                        $code_pulsa = carr::get($value, "code");
                        $name = carr::get($value, "name");
                        $nominal_list[$code] = $name;
                        if ($code_pulsa == $code){
                            $price_pulsa = carr::get($value, "price");
                        }
                    }   
                    $product_id = cdbutils::get_value("select product_id from product where sku='".$code."'"); 
                    if ($product_code == 'KY') {
                        $product_id = cdbutils::get_value("select product_id from product where sku_ky='".$code."'"); 
                    }
                    
                    if ($price_pulsa == NULL) {
                        $err_code++;
                        $err_message = clang::__("Product").' '.$code.' '.clang::__("Tidak di temukan mohon pilih product yang lain");
                    }
                    else {
                        $hid_type_tab = 'pulsa';
                        if($type_tab == 'game_online'){
                            $hid_type_tab = 'game_online';
                        }
                                                
                        $app->add_control('product_id','hidden')->set_value($product_id);
                        $app->add_control('type_tab','hidden')->set_value($hid_type_tab);
                        $product = product::get_product($product_id);

                        $sell_price_pulsa = $price_pulsa + $product['detail_price']['ho_upselling'];
                        $app->add_control("price_pulsa", "hidden")->set_value($sell_price_pulsa);
                         $total = $app->add_div("total_div")->add_class("input-box")->custom_css("text-align", "left");
                         $total->add("<span style='margin-left: 6px;'>Total Pembayaran : Rp. ".  ctransform::format_currency($sell_price_pulsa)." </span>");
                         $act_payment = $app->add_action()
                                            ->add_class("btn-paynow") 
                                            ->set_label(clang::__('BELI SEKARANG'));
                         //$act_payment->set_link(curl::base() . 'pulsa/home/set_session_pulsa')->set_submit(false);

                         $email = $session->get('email');
        //                         if (!empty($email)) {
        //                              $app->add("<input type='submit' value='BELI SEKARANG' class='btn-paynow'>");
        //                         } else {
        //                             $act_payment->add_listener('click')
        //                                     ->add_handler('dialog')
        //                                     ->set_title('LOGIN')
        //                                     ->set_url(curl::base() . "pulsa/home/form_login_pulsa/" . $product_id)
        //                                 ;
        //                         }
                    }
                }
                if ($err_code > 0) {
                    $app->add($err_message);
                }
            }
        }
        $app->add_js('
                    $(".btn-paynow").click(function(){
                        var err_code=0;
                        var err_message="";
                        var type_tab=$("#type_tab").val();
                        var game_number=$("#number_input_game").val();
                        var pulsa_number = $("#number_input_pulsa").val();
                        var price_pulsa = $("#price_pulsa").val();
                        var product_id = $("#product_id").val();
                        var phone_number=pulsa_number;
                        if(type_tab=="game_online"){
                            phone_number=game_number;
                        }
                        if(phone_number.length==0){
                            err_code++;
                            err_message="No Handphone Tidak Boleh Kosong";
                        }
                        
                        if(err_code==0){
                            $.ajax({
                                url:"/pulsa/home/set_session_pulsa",
                                data:{
                                    phone_number : phone_number,
                                    price_pulsa : price_pulsa,
                                    pulsa_product_id : product_id,
                                },
                                type:"GET",
                                success: function(data){
                                    data=JSON.parse(data);
                                    if(data.err_code=="0"){
                                        window.location.href="'.curl::base().'pulsa/updatecontact/update_contact";
                                    }else{
                                        alert(data.err_message);
                                    }
                                },
                                error:function(data){
                                },
                            })
                        }else{
                            alert(err_message);
                        }
                    });
                   ');
        echo $app->render();
        
    }
    public function form_login_pulsa($product_id=null) {
        $app = CApp::instance();
        $session = session::instance();
        $phone_number = $session->get("phone-number");
        $request = array_merge($_POST, $_GET);
       
        $login_element = CElement_Login::factory()
                ->set_session(false)
                ->set_trigger_button(FALSE)
                ->set_is_login(FALSE)
                ->set_icon(FALSE)
                ->set_guest(TRUE)
                ->set_action_next(TRUE)
                ->set_reload(TRUE)
                ->set_action_url(curl::base() . 'auth/next_login')
                ->set_redirect(curl::base() . 'pulsa/updatecontact/update_contact/'. $product_id);
        if ($this->have_register() > 0) {
            $login_element->set_register_button(TRUE);
        }
        $app->add($login_element);
        echo $app->render();
    }
    public function set_session_pulsa(){
        $db=CDatabase::instance();
        $err_code=0;
        $err_message='';
        $param = $_GET+$_POST;
        $product_id = carr::get($param, "pulsa_product_id");
        $price_pulsa = carr::get($param, "price_pulsa");
        $phone_number = carr::get($param, "phone_number");
        $date_now=date('Y-m-d');
        $product_code_pulsa='RB';
        if(ccfg::get('pulsa_product_code')!=null){
            $product_code_pulsa=ccfg::get('pulsa_product_code');
        }
//        cdbg::var_dump($param);
//        die;
        //check transaction today for phone number and product
        $q="
            select
                p.name as product
            from
                transaction as t 
                inner join transaction_detail as td on td.transaction_id=t.transaction_id
                inner join product as p on p.product_id=td.product_id
                inner join product_type as pt on pt.product_type_id=p.product_type_id
            where
                t.status>0
                and td.status>0
                and t.order_status in('SUCCESS','PENDING','CONFIRMED')
                and pt.name='pulsa'
                and td.product_id=".$db->escape($product_id)."
                and td.pulsa_phone_number=".$db->escape($phone_number)."
                and date(t.date)=".$db->escape($date_now)."    
        ";
        $exist=cdbutils::get_row($q);
        $result=array();
        $session_php = Session::instance();
        if($exist==null or $product_code_pulsa!='RB'){
            $session_php->set('phone-number', $phone_number);
            $session_php->set('price_pulsa', $price_pulsa);
            $session_php->set('pulsa_product_id', $product_id);
        }else{
            $err_code++;
            $err_message=clang::__("Nomor handphone").' '.$phone_number.' '.clang::__("sudah melakukan pembelian").' '.cobj::get($exist,'product').' '.clang::__("hari ini");
        }
        $result['err_code']=$err_code;
        $result['err_message']=$err_message;

        echo json_encode($result);
    }


}
