<?php

class Reissued_Controller extends CController {

    public function __construct() {
        parent::__construct();
        $app = CApp::instance();
    }
    
    public function process(){
        $db=CDatabase::instance();
        $post=$_POST;
        $err_code=0;
        $err_message="";
        if($post!=null){
            $transaction_id=carr::get($post,'transaction_id');
            if(strlen($transaction_id)==0){
                $err_code++;
                $err_message=clang::__("Transaction Not Found");
            }
            if($err_code==0){
                $q="
                    select
                        *
                    from
                        transaction
                    where
                        transaction_id=".$db->escape($transaction_id)."
                ";    
                
            }
            
            
            if($err_code==0){
                $api_pulsa=pulsa_api::issued_pulsa($transaction_id);
                $err_code=$api_pulsa['err_code'];
                $err_message=$api_pulsa['err_message'];
            }
        }else{
            $err_code++;
            $err_message=clang::__("No Post Data");
        }
        $res=array();
        $res['err_code']=$err_code;
        $res['err_message']=$err_message;
        
        echo json_encode($res);
    }
}
