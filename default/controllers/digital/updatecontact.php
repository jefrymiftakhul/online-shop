<?php

class UpdateContact_Controller extends PulsaController {

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    public function update_contact() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $menu_list = $this->menu_list();
        $link_list = $this->list_menu();
        $session_php = Session::instance();
        if(ccfg::get("transaction_member_only")>0){
            $member_id = $session_php->get('member_id');
            if($member_id == null && strlen($member_id)==0){
                cmsg::add('error',clang::__("Harap login member sebelum melakukan transaksi"));
                curl::redirect('pulsa/home');
            }
        }
        $request = array_merge($_POST, $_GET);
        $product_id = $session_php->get("pulsa_product_id");  
        $price_pulsa = $session_php->get('price_pulsa');
        $phone_number = $session_php->get("phone-number");  
        
        if(strlen($phone_number)==0){
            cmsg::add('error',clang::__("No Handphone Tidak Boleh Kosong"));
            curl::redirect('pulsa/home');
        }
        $data_set_value = array();
        if (count($session_php->get('post_update_contact'))) {
            $data_set_value = $session_php->get_once('post_update_contact');
        }

        $country_id = '';
        $q = "
                select
                 *
                from
                    country
                where
                    name=" . $db->escape('Indonesia') . "
            ";
        $r = $db->query($q);
        if ($r->count() > 0) {
            $country_id = $r[0]->country_id;
        }


//        // SEARCH 
//        $app->add_listener('ready')
//                ->add_handler('reload')
//                ->set_target('search')
//                ->set_url(curl::base() . "home/search");
//
//        // SHOPPING CART 
//        $app->add_listener('ready')
//                ->add_handler('reload')
//                ->set_target('shopping-cart')
//                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");
//

        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_menu($menu_list);
        $element_menu->set_link($link_list);
        $element_menu->set_url_menu(curl::base() . 'pulsa/product/category/');
        $attribute = array();
        foreach ($request as $key_req => $value_req) {
            if (substr($key_req, 0, 8) == 'att_cat_') {
                $attribute[substr($key_req, 8)] = $value_req;
            }
        }
        // get data product 
        $product_simple = product::get_simple_product_from_attribute($product_id, $attribute);

        $form = $app->add_form('form_transaction')
                ->set_action(curl::base() . 'pulsa/updatecontact/set_session')
                ->add_class('form-62hallfamily form-gold-contact padding-0');
        $form->add_control('page', 'hidden')
               ->set_value("pulsa"); 
        $form->add_control('phone-number', 'hidden')
                ->set_value($phone_number);
        $form->add_control('price_pulsa', 'hidden')
                ->set_value($price_pulsa);
        $form->add_control('transaction_type', 'hidden')->set_value('buy');

        $form->add_control('product_id', 'hidden')
                ->set_value($product_id);

        $container = $form->add_div()->add_class('container');

        // UPDATE CONTACT
        $view_update_contact = $container->add_div()
                ->add_class('col-md-7 margin-top-30 margin-bottom-30')
                ->custom_css('padding-left', '0px');

        $err_code = $session_php->get_once('error_update_contact');
        $message = $session_php->get_once('message_update_contact');

        if (!empty($message)) {
            $alert = 'success';
            if ($err_code > 0) {
                $alert = 'danger';
            }
            $alert_message = $view_update_contact->add_div()->add_class('alert alert-' . $alert)
                    ->add('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' . $message);
        }

        $element_update_contact = $view_update_contact->add_element('62hall-update-contact', 'element_update_contact');
        $element_update_contact->set_country_id($country_id)
                ->set_data_value($data_set_value);
        $element_update_contact->set_using_shipping(false);



        // RINGKASAN PEMESASAN
        $total_barang = 0;
        $total_harga = 0;
        $view_shopping_cart = $container->add_div()->add_class('col-md-5 ringkasan-pemesanan margin-top-30 margin-bottom-30');
        $div_shopping_cart = $view_shopping_cart->add_div()->add_class('col-md-12 container-cart margin-bottom-20');

        $div_shopping_cart->add_div()->add_class('font-size22 bold font-red margin-bottom-10')->add('Ringkasan Pemesanan');
       
        $count_item = 1;
        if ($count_item > 0) {
//            die(__CLASS__);
            $items = $div_shopping_cart->add_div()->add_class('horizontal-overflow')->custom_css('max-height', '250px');
//         
            $product = product::get_product($product_id);
            $sell_price_pulsa = $price_pulsa;
            
            $ho_sell_price_pulsa = $price_pulsa  + $product['detail_price']['channel_updown'];
            $cart_list[] = array(
                'product_id' => $product_id,
                'name' => $product['name'],
                'image' => $this->get_image($product['image_path']),
                'sell_price' => $sell_price_pulsa,
                'price' => $ho_sell_price_pulsa,
                'qty' => 1,
                'sub_total' => $sell_price_pulsa,
            );
           
            foreach ($cart_list as $data_cart) {
                $total_barang += $data_cart['qty'];
                $total_harga += $data_cart['sub_total'];
                $items->add('
                                <div class="item col-md-12 col-xs-12 padding-left-right0 padding-top-10">
                                    <div class="col-md-2 col-xs-3 padding-0 margin-right-10">
                                    <img class="image-item" src="'.$data_cart['image'].'"/>
                                    </div>
                                    <div class="col-md-9 col-xs-9 content padding-0 bold">
                                        ' . $data_cart['name'] . '
                                        <br>
                                        <div class="font-red normal">
                                        Rp.' . ctransform::format_currency($data_cart['sell_price']) . '
                                        <br>
                                        <div class="font-black">
                                        Jumlah : ' . $data_cart['qty'] . '
                                        </div>
                                         <div class="font-black">
                                        No. Handphone : ' . $phone_number . '
                                        </div>
                                        <div class="font-red col-md-12 col-xs-12 padding-0 margin-top-10 bold">
                                            <div class="col-md-6 col-xs-6 padding-0">
                                                Sub Total
                                            </div>
                                            <div class="col-md-6 col-xs-6 padding-0 text-right">
                                                Rp.' . ctransform::format_currency($data_cart['sub_total']) . '
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>'
                );
            }
        }

        $container_total = $div_shopping_cart->add_div()->add_class('margin-top-10 col-md-12 padding-0');
        $container_total->add_div()->add_class('col-md-5 padding-0 bold')->add('TOTAL PULSA');
        $container_total->add_div()->add_class('col-md-7 padding-0 bold')->add(': ' . $total_barang);
        $container_total->add_div()->add_class('col-md-5 padding-0 bold font-red')->add('TOTAL HARGA');
        $container_total->add_div()->add_class('col-md-7 padding-0 bold font-red')->add(': Rp. ' . ctransform::format_currency($total_harga));

        $view_shopping_cart->add_action('submit_button')
                ->set_label(clang::__('Lanjutkan Pembayaran'))
                ->add_class('btn-62hallfamily col-md-12 bg-red border-3-red font-size22 link')
                ->set_submit(TRUE);

        echo $app->render();
    }
    public function set_session() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $post = $this->input->post();
        $cart = Cart62hall_Product::factory();
        $product_id = carr::get($post, 'product_id');

        $error = 0;
        $error_message = '';

        $arr_check = array(
            'post_code_shipping', 'districts_shipping', 'city_shipping', 'province_shipping', 'city_shipping', 'phone_shipping', 'address_shipping', 'email_shipping', 'name_shipping',
            'post_code_pay', 'districts_pay', 'city_pay', 'province_pay', 'city_pay', 'phone_pay', 'address_pay', 'email_pay', 'name_pay',
        );

        if ($post == null) {
            $error++;
            $error_message = 'Please Fill Contact';
        }

        if ($error == 0) {
            $field = NULL;
            foreach ($arr_check as $key_post) {
                if (strlen(carr::get($post, $key_post)) == 0) {
                    if ($key_post == 'name_pay') {
                        $field = 'Nama Lengkap Pembayaran';
                    }
                    if ($key_post == 'email_pay') {
                        $field = 'Email Pembayaran';
                    }
                    if ($key_post == 'address_pay') {
                        $field = 'Alamat Pembayaran';
                    }
                    if ($key_post == 'phone_pay') {
                        $field = 'No. HP Pembayaran';
                    }
                    if ($key_post == 'province_pay') {
                        $field = 'Propinsi Pembayaran';
                    }
                    if ($key_post == 'city_pay') {
                        $field = 'Kota Pembayaran';
                    }
                    if ($key_post == 'districts_pay') {
                        $field = 'Kecamatan Pembayaran';
                    }
                    if ($key_post == 'post_code_pay') {
                        $field = 'Kode Pos Pembayaran';
                    }
                    if ($key_post == 'name_shipping') {
                        $field = 'Nama Lengkap Pengiriman';
                    }
                    if ($key_post == 'email_shipping') {
                        $field = 'Email Pengiriman';
                    }
                    if ($key_post == 'address_shipping') {
                        $field = 'Alamat Pengiriman';
                    }
                    if ($key_post == 'phone_shipping') {
                        $field = 'No. HP Pengiriman';
                    }
                    if ($key_post == 'province_shipping') {
                        $field = 'Propinsi Pengiriman';
                    }
                    if ($key_post == 'city_shipping') {
                        $field = 'Kota Pengiriman';
                    }
                    if ($key_post == 'districts_shipping') {
                        $field = 'Kecamatan Pengiriman';
                    }
                    if ($key_post == 'post_code_shipping') {
                        $field = 'Kode Pos Pengiriman';
                    }

                    $error++;
                    $error_message = $field . ' belum diisi.';
                }
            }
        }

        if ($error == 0) {
            //get data post
            $session = CApiClientSession::factory('PG');
            $page = carr::get($post, 'page');
            $phone_number = carr::get($post, 'phone-number');
            $price_pulsa = carr::get($post, 'price_pulsa');
            $transaction_type = carr::get($post, 'transaction_type');

            $name_pay = carr::get($post, 'name_pay');
            $email_pay = carr::get($post, 'email_pay');
            $address_pay = carr::get($post, 'address_pay');
            $phone_area_pay = carr::get($post, 'phone_area_pay');
            $phone_pay = carr::get($post, 'phone_pay');
            $province_pay = carr::get($post, 'province_pay');
            $city_pay = carr::get($post, 'city_pay');
            $district_pay = carr::get($post, 'districts_pay');
            $post_code_pay = carr::get($post, 'post_code_pay');

            $name_shipping = carr::get($post, 'name_shipping');
            $email_shipping = carr::get($post, 'email_shipping');
            $address_shipping = carr::get($post, 'address_shipping');
            $phone_area_shipping = carr::get($post, 'phone_area_shipping');
            $phone_shipping = carr::get($post, 'phone_shipping');
            $province_shipping = carr::get($post, 'province_shipping');
            $city_shipping = carr::get($post, 'city_shipping');
            $district_shipping = carr::get($post, 'districts_shipping');
            $post_code_shipping = carr::get($post, 'post_code_shipping');

            $post_shipping_type_id = carr::get($post, 'shipping_type_id');
            $post_shipping_price = carr::get($post, 'shipping_price');
            $shipping_price = str_replace('Rp. ', '', $post_shipping_price);
            $shipping_price = ctransform::unformat_currency($shipping_price);

            $product_id = carr::get($post, 'product_id');
            $check_same = carr::get($post, 'check_same');

            $cart_list = $cart->get_cart();

            $transaction = array();
            $transaction['page'] = $page;
            $transaction['phone_number'] = $phone_number;
            $transaction['type'] = 'buy';
            $transaction['session_card_id'] = null;
            $province_pay_text = NULL;
            if ($province_pay) {
                $q = "
                                select
                                        *
                                from
                                        province
                                where
                                        province_id=" . $province_pay . "
                        ";
                $r = $db->query($q);
                if ($r->count() > 0) {
                    $province_pay_text = $r[0]->name;
                }
            }
            $city_pay_text = NULL;
            if ($city_pay) {
                $q = "
                                select
                                        *
                                from
                                        city
                                where
                                        city_id=" . $city_pay . "
                        ";
                $r = $db->query($q);
                if ($r->count() > 0) {
                    $city_pay_text = $r[0]->name;
                }
            }
            $district_pay_text = NULL;
            if ($district_pay) {
                $q = "
                                select
                                        *
                                from
                                        districts
                                where
                                        districts_id=" . $district_pay . "
                        ";
                $r = $db->query($q);
                if ($r->count() > 0) {
                    $district_pay_text = $r[0]->name;
                }
            }

            $province_shipping_text = NULL;
            if ($province_shipping) {
                $q = "
                                select
                                        *
                                from
                                        province
                                where
                                        province_id=" . $province_shipping . "
                        ";
                $r = $db->query($q);
                if ($r->count() > 0) {
                    $province_shipping_text = $r[0]->name;
                }
            }

            $city_shipping_text = NULL;
            if ($city_shipping) {
                $q = "
                                select
                                        *
                                from
                                        city
                                where
                                        city_id=" . $city_shipping . "
                        ";
                $r = $db->query($q);
                if ($r->count() > 0) {
                    $city_shipping_text = $r[0]->name;
                }
            }
            $district_shipping_text = NULL;
            if ($district_shipping) {
                $q = "
                                select
                                        *
                                from
                                        districts
                                where
                                        districts_id=" . $district_shipping . "
                        ";
                $r = $db->query($q);
                if ($r->count() > 0) {
                    $district_shipping_text = $r[0]->name;
                }
            }

            $arr_contact = array();
            $arr_contact_billing = array();
            $arr_contact_shipping = array();
            $arr_name_pay = explode(' ', $name_pay, 2);
            $arr_name_shipping = explode(' ', $name_shipping, 2);
            $first_name_pay = $name_pay;
            $last_name_pay = '';
            $first_name_shipping = $name_shipping;
            $last_name_shipping = '';
            if (count($arr_name_pay) == 2) {
                $first_name_pay = $arr_name_pay[0];
                $last_name_pay = $arr_name_pay[1];
            }

            if (count($arr_name_shipping) == 2) {
                $first_name_shipping = $arr_name_shipping[0];
                $last_name_shipping = $arr_name_shipping[1];
            }

            //handling billing
            if ($error == 0) {
                if (strlen($first_name_pay) > 20) {
                    $error++;
                    $error_message = 'Billing first name must be least than equal to 20 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($last_name_pay) > 20) {
                    $error++;
                    $error_message = 'Billing last name must be least than equal to 20 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($address_pay) > 200) {
                    $error++;
                    $error_message = 'Billing address must be least than equal to 200 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($city_pay_text) > 100) {
                    $error++;
                    $error_message = 'Billing city must be least than equal to 100 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($post_code_pay) > 10) {
                    $error++;
                    $error_message = 'Billing postal code must be least than equal to 10 characters.';
                }
            }
            if ($error == 0) {
                if (strlen($phone_area_pay . $phone_pay) > 19) {
                    $error++;
                    $error_message = 'Billing phone must be least than equal to 19 characters.';
                }
            }

            //handling shipping
            if ($error == 0) {
                if (strlen($first_name_shipping) > 20) {
                    $error++;
                    $error_message = 'Shipping first name must be least than equal to 20 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($last_name_shipping) > 20) {
                    $error++;
                    $error_message = 'Shipping last name must be least than equal to 20 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($address_shipping) > 200) {
                    $error++;
                    $error_message = 'Shipping address must be least than equal to 200 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($city_shipping_text) > 100) {
                    $error++;
                    $error_message = 'Shipping city must be least than equal to 100 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($post_code_shipping) > 10) {
                    $error++;
                    $error_message = 'Shipping postal code must be least than equal to 10 characters.';
                }
            }
            if ($error == 0) {
                if (strlen($phone_area_shipping . $phone_shipping) > 19) {
                    $error++;
                    $error_message = 'Shipping phone must be least than equal to 19 characters.';
                }
            }
            if ($error == 0) {
                $arr_contact_billing['first_name'] = $first_name_pay;
                $arr_contact_billing['last_name'] = $last_name_pay;
                $arr_contact_billing['address'] = $address_pay;
                $arr_contact_billing['email'] = $email_pay;
                $arr_contact_billing['province_id'] = $province_pay;
                $arr_contact_billing['province'] = $province_pay_text;
                $arr_contact_billing['city_id'] = $city_pay;
                $arr_contact_billing['city'] = $city_pay_text;
                $arr_contact_billing['district_id'] = $district_pay;
                $arr_contact_billing['district'] = $district_pay_text;
                $arr_contact_billing['postal_code'] = $post_code_pay;
                $arr_contact_billing['phone'] = $phone_area_pay . $phone_pay;
                $arr_contact_billing['country_code'] = 'IDN';

                $arr_contact_shipping['first_name'] = $first_name_pay;
                $arr_contact_shipping['last_name'] = $last_name_pay;
                $arr_contact_shipping['address'] = $address_pay;
                $arr_contact_shipping['email'] = $email_pay;
                $arr_contact_shipping['province_id'] = $province_pay;
                $arr_contact_shipping['province'] = $province_pay_text;
                $arr_contact_shipping['city_id'] = $city_pay;
                $arr_contact_shipping['city'] = $city_pay_text;
                $arr_contact_shipping['district_id'] = $district_pay;
                $arr_contact_shipping['district'] = $district_pay_text;
                $arr_contact_shipping['postal_code'] = $post_code_pay;
                $arr_contact_shipping['phone'] = $phone_area_pay . $phone_pay;
                $arr_contact_shipping['country_code'] = 'IDN';

                if ($check_same == null) {
                    $arr_contact_shipping['first_name'] = $first_name_shipping;
                    $arr_contact_shipping['last_name'] = $last_name_shipping;
                    $arr_contact_shipping['address'] = $address_shipping;
                    $arr_contact_shipping['email'] = $email_shipping;
                    $arr_contact_shipping['province_id'] = $province_shipping;
                    $arr_contact_shipping['province'] = $province_shipping_text;
                    $arr_contact_shipping['city_id'] = $city_shipping;
                    $arr_contact_shipping['city'] = $city_shipping_text;
                    $arr_contact_shipping['district_id'] = $district_shipping;
                    $arr_contact_shipping['district'] = $district_shipping_text;
                    $arr_contact_shipping['postal_code'] = $post_code_shipping;
                    $arr_contact_shipping['phone'] = $phone_area_shipping . $phone_shipping;
                    $arr_contact_shipping['country_code'] = 'IDN';
                }

                $product_code = '';
                $product_name = '';
                $product_price = '';
                $qty = 1;
                $total_item = 0;
                $item = array();
                $product = product::get_product($product_id);
                $sell_price_pulsa = $price_pulsa;
//                $item['price_pulsa'] = $price_pulsa;
//                $item['sell_price_pulsa'] = $sell_price_pulsa;
//                $transaction['item_pulsa'][] = $item;
                
                
                $product_name = $product['name'];
                $product_price = $product['detail_price']['channel_sell_price'];
                $product_code = $product['code'];
                $item['item_id'] = $product_id;
                $item['item_code'] = $product_code;
                $item['item_name'] = $product_name;
                $item['qty'] = $qty;
                $item['price'] = $sell_price_pulsa;
                $transaction['item'][] = $item;


                $item_payment_name = $product_name;
                if (strlen($item_payment_name) > 50) {
                    $item_payment_name = substr($item_payment_name, 0, 49);
                }

                $item['item_id'] = $product_id;
                $item['item_code'] = $product_code;
                $item['item_name'] = $item_payment_name;
                $item['qty'] = $qty;
                $item['price'] = $sell_price_pulsa;
                $transaction['item_payment'][] = $item;

                $total_shipping = 0;
                $total_item = ($qty * $product_price);
                $grand_total = $total_item + $total_shipping;
                $amount_payment = $grand_total;

                $transaction['total_item'] = $total_item;
                $transaction['total_shipping'] = $total_shipping;
                $transaction['amount_payment'] = $amount_payment;


                $arr_contact['first_name'] = $arr_contact_billing['first_name'];
                $arr_contact['last_name'] = $arr_contact_billing['last_name'];
                $arr_contact['email'] = $arr_contact_billing['email'];
                $arr_contact['phone'] = $arr_contact_billing['phone'];
                $arr_contact['billing_address'] = $arr_contact_billing;
                $arr_contact['shipping_address'] = $arr_contact_shipping;
                $arr_contact['same_billing_shipping'] = $check_same;
                $transaction['contact_detail'] = $arr_contact;
                $session->set('transaction', $transaction);
                curl::redirect('payment/form_payment');
            }
        }
        if ($error > 0) {
            //cmsg::add('error',$error_message );
            $session_php = Session::instance();
            $session_php->set('message_update_contact', $error_message);
            $session_php->set('error_update_contact', $error);

            $data_post = array();
            $data_post['name_pay'] = carr::get($post, 'name_pay');
            $data_post['email_pay'] = carr::get($post, 'email_pay');
            $data_post['address_pay'] = carr::get($post, 'address_pay');
            $data_post['phone_pay'] = carr::get($post, 'phone_pay');
            $data_post['province_pay'] = carr::get($post, 'province_pay');
            $data_post['city_pay'] = carr::get($post, 'city_pay');
            $data_post['districts_pay'] = carr::get($post, 'districts_pay');
            $data_post['post_code_pay'] = carr::get($post, 'post_code_pay');

            $data_post['name_shipping'] = carr::get($post, 'name_shipping');
            $data_post['email_shipping'] = carr::get($post, 'email_shipping');
            $data_post['address_shipping'] = carr::get($post, 'address_shipping');
            $data_post['phone_shipping'] = carr::get($post, 'phone_shipping');
            $data_post['province_shipping'] = carr::get($post, 'province_shipping');
            $data_post['city_shipping'] = carr::get($post, 'city_shipping');
            $data_post['districts_shipping'] = carr::get($post, 'districts_shipping');
            $data_post['post_code_shipping'] = carr::get($post, 'post_code_shipping');

            $session_php->set('post_update_contact', $data_post);

            curl::redirect('pulsa/updatecontact/update_contact/' . $product_id);
        }
    }
    private function get_image($image_name) {
        $ch = curl_init(image::get_image_url($image_name, 'view_all'));
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_exec($ch);
        $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($retcode == 500 or $image_name == NULL) {
            $image_name = curl::base() . 'application/62hallfamily/default/media/img/product/no-image.png';
        } else {
            $image_name = image::get_image_url($image_name, 'view_all');
        }
        curl_close($ch);
        return $image_name;
    }
    
}