<?php

/**
 *
 * @author Riza
 * @since  Nov 19, 2015
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class Product_Controller extends ProductController {

    private $_count_item = 16;
    private $deal_pick_for_you = "pick_for_you";
    private $type_select = 'select';
    private $type_image = 'image';

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        curl::redirect(curl::base());
    }

    public function item($product_id = null) {
        $db = CDatabase::instance();
        if (!is_numeric($product_id)) {
            $product_id = cdbutils::get_value("select product_id from product where status > 0 and url_key= " . $db->escape($product_id));
        }
        if ($product_id == null) {
            curl::redirect(curl::base());
        }
        $app = CApp::instance();
        $user = $app->user();
        $org_id = $this->org_id();
        $menu_list = $this->menu_list();
        $link_list = $this->list_menu();
        $error = 0;
        $error_message = '';


        // SHOPPING CART
        $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

//        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
//        $element_menu->set_menu($menu_list);
//        $element_menu->set_link($link_list);

        $main_product = $app->add_div()->add_class('main-product');
        $product_div_wrap = $main_product->add_div()->add_class('container container-lasvegas');
        $product_total = $product_div_wrap->add_div()->add_class('container-product-total');
        $row = $product_total->add_div()->add_class('row');
        $col1 = $row->add_div()->add_class('col-sm-6 col-md-6');
        $col2 = $row->add_div()->add_class('col-sm-6 col-md-6');
        // $col2->add_div()->add_class('txt-product-total')->add("<a href='".curl::base()."'>".clang::__('See another product')."</a>");
        $product_div_wrap->add_div()->add_class('separate-show-product');

        $product_div = $product_div_wrap->add_div()->add_class('row');


        $main_overview = $app->add_div()->add_class('main-product-info');
        $overview_div = $main_overview->add_div()->add_class('container');
        $main_related = $app->add_div()->add_class('main-related');
        $related_div = $main_related->add_div()->add_class('container');

        $q = "select
                p.name as product,
                p.*,
                pt.*
            from
                product as p
                inner join product_type as pt on pt.product_type_id=p.product_type_id
            where
                p.status>0
                and p.is_active=1
                and p.status_confirm='CONFIRMED'
                and p.product_id=" . $db->escape($product_id) . "
                and pt.name=" . $db->escape($this->page()) . "
            ";
        $product = $db->query($q);
        if ($product->count() == 0) {
            $error++;
            $error_message = "Product not found please select other product";
        }

        if ($error == 0) {
            //add viewed
            $viewed = cobj::get($product, 'viewed');
            //$db->update("product", array("viewed" => $viewed + 1), array("product_id" => $product_id));

            $product_image = $product_div->add_div()->add_class('col-xs-12 col-sm-6 col-md-8');
            $product_detail = $product_div->add_div()->add_class('col-xs-12 col-sm-6 col-md-4');

            $product_row = cdbutils::get_row($q);
            $viewed = cobj::get($product_row, 'viewed');
            //$db->update("product", array("viewed" => $viewed + 1), array("product_id" => $product_id));
            $product_category_id = cobj::get($product_row, 'product_category_id');

            // BREADCRUMB
            $div_breadcrumb = $col1;
            $data_category = cdbutils::get_row('select * from product_category where status > 0 and product_category_id = ' . $db->escape($product_category_id));
            $first_parent = false;
            $parent_id = cobj::get($data_category, 'parent_id');
            if ($parent_id == null) {
                $first_parent = true;
            }
            $arr_breadcumb[] = array(
                'label' => $data_category->name,
                'url' => $data_category->url_key,
            );
            while ($first_parent == false) {
                $data_category = cdbutils::get_row('select * from product_category where status > 0 and product_category_id = ' . $db->escape($parent_id));
                $parent_id = cobj::get($data_category, 'parent_id');
                $name = cobj::get($data_category, 'name');
                $url_key = cobj::get($data_category, 'url_key');
                if ($parent_id == null) {
                    $first_parent = true;
                }
                $arr = array(
                    'label' => $name,
                    'url' => $url_key,
                );
                array_unshift($arr_breadcumb, $arr);
            }
            $home = array(
                'label' => 'Home',
                'url' => '/',
            );
            array_unshift($arr_breadcumb, $home);
            $html = '<ul class="breadcumb-lasvegas">';
            foreach ($arr_breadcumb as $key => $value) {
                $html .= '<li>';
                $url = carr::get($value, 'url');
                if ($url == '/') {
                    $html .= '<a href="/">';
                } else if (strlen($url) > 0) {
                    $html .= '<a href="' . curl::base() . 'search?keyword=&category=' . $url . '">';
                } else {
                    $html .= '<a>';
                }
                $html .= carr::get($value, 'label');
                $html .= '</a>';
                $html .= '</li>';
            }
            $html .= '</ul>';

            $div_breadcrumb->add($html);

            // GALERY PRODUCT
            $data_galery_product = product::get_product_image($product_id);
            $element_galery_product = $product_image->add_element('62hall-galery-product', 'galery-product-' . $product_id);
            $element_galery_product->set_images($data_galery_product);

            // DETAIL PRODUCT
            $data_detail_product = product::get_product($product_id);
            $data_attribute_category = product::get_attribute_category_all($product_id);

            $element_detail_product = $product_detail->add_element('62hall-detail-product', 'detail-product-' . $product_id);
            $element_detail_product->set_key($product_id)
                    ->set_detail_list($data_detail_product)
                    ->set_attribute_list($data_attribute_category);

            // OVERVIEW PRODUCT
            $overview_product = $overview_div->add_div();
            $element_overview = $overview_product->add_element('62hall-overview-product', 'tab-product');
            $element_overview->set_key($product_id);


            // RELATED PRODUCT
            $product_related_list = product::get_product_related($product_id);

            if (count($product_related_list) > 0) {
                $container_deal = $related_div->add_div('related')->add_class('container-deal');
                $container_deal_title = $container_deal->add_div('related-title')->add_class('container-deal-title');
                $container_deal_title_txt = $container_deal_title->add_div('related-title-deal')->add_class('deal-title');
                $container_deal_view_all = $container_deal_title->add_div('related-deal-view')->add_class('deal-view-all');
                $container_deal_title_txt->add(clang::__('Related') . " <span class='hidden-xs'>" . clang::__('Product') . "</span>");

                if (CF::domain() !== 'shinjuku-hairmake.com') {
                    $container_deal_view_all->add("<a href='" . curl::base() . "show_product/show/deal_best_sell'><button class='btn btn-warning btn-view-all-product'>" . clang::__('View All Product') . "</button></a>");
                }
                $container_deal_content = $container_deal->add_div()->add_class('container-deal-content');
                $element_deal = $container_deal_content->add_element('62hall-slide-product', 'related_product')->add_class('slide-deal');
                $element_related_product = $related_div->add_element('62hall-swiper-product', 'related-product');
                $element_related_product
//                    ->set_limit(5)
                        ->set_list_item($product_related_list);
            }
        } else {
            $app->add($error_message);
        }

//        cdbg::var_dump($product_id);
        echo $app->render();
    }

    //<editor-fold defaultstate="collapsed" desc="Product Overview">
    public function overview($product_id) {
        $app = CApp::instance();

        $data_detail_product = product::get_product($product_id);
        $overview = carr::get($data_detail_product, 'overview', null);

        if (strlen($overview) == 0) {
            $overview = clang::__('Tidak ada rincian untuk barang ini');
        }

        $app->add($overview);

        echo $app->render();
    }

    public function info_seller($product_id) {
        $app = CApp::instance();

        $data_detail_product = product::get_product($product_id);
        $vendor = carr::get($data_detail_product, 'vendor_detail', null);

        if (count($vendor) == 0) {
            $vendor = clang::__('Tidak ada informasi penjual untuk barang ini');
        }

        $vendor_name = carr::get($vendor, 'name');

        $app->add($vendor_name);

        echo $app->render();
    }

    public function spesifikasi($product_id) {
        $app = CApp::instance();

        $data_detail_product = product::get_product($product_id);
        $spesification = carr::get($data_detail_product, 'spesification', null);

        if (strlen($spesification) == 0) {
            $spesification = clang::__('Tidak ada spesifikasi untuk barang ini');
        }

        $app->add($spesification);

        echo $app->render();
    }

    public function shipping_information($product_id) {
        $app = CApp::instance();
        $shipping_day = '(2-28 hari)';

        $shipping_text = 'ALL INDONESIA ' . $shipping_day;
        $shipping_text = 'JAWA (2-14 hari kerja)<br/>LUAR JAWA (14-28 hari kerja)';
        if (strlen($product_id) > 0) {
            $arr_product_city = shipping::get_shipping_city_info($product_id);
            if (count($arr_product_city) > 0) {
                $shipping_text = '<ul class="shipping-info">';
                foreach ($arr_product_city as $province => $val) {
                    $city = carr::get($val, 'city');
                    $city_name = '';
                    if (!isset($city['all'])) {
                        $city = array_keys($city);
                        $city = array_map('strtolower', $city);
                        $city = array_map('ucwords', $city);
                        $city_name = implode(',', $city);
                        //$city_name=ucwords(strtolower($city_name),',');
                    }
                    $shipping_text .= '<li>' . $province . ' ' . $shipping_day;
                    if (strlen($city_name) > 0) {
                        $shipping_text .= ' (' . $city_name . ') ';
                    }
                    $shipping_text .= '</li>';
                }
                $shipping_text .= '</ul>';
            }
        }

        // $app->add($shipping_text);
//old lasvegas
//        $container = $app->add_div()->add_class('container-shipping-information');
//        $container_left = $container->add_div()->add_class('shipping-icon');
//        $container_right = $container->add_div()->add_class('shipping-text');
//
//        $container_left->add_div()->add_class('ico-payment-cost');
//        $container_right->add_div()->add_class('shipping-title')->add(clang::__('Biaya Pengiriman'));
//        $txt_shipping = "
//            <ul class='shipping-information-list'>
//                <li>Gratis untuk area Surabaya</li>
//                <li>Untuk area di luar kota Surabaya akan dikirim melalui JNE dan dikenakan biaya pengiriman</li>
//            </ul>
//        ";
//        $container_right->add($txt_shipping);
//
//        $container = $app->add_div()->add_class('container-shipping-information');
//        $container_left = $container->add_div()->add_class('shipping-icon');
//        $container_right = $container->add_div()->add_class('shipping-text');
//        $container_left->add_div()->add_class('ico-delivery');
//        $container_right->add_div()->add_class('shipping-title')->add(clang::__('Lama Pengiriman'));
//        $txt_shipping = "
//            <ul class='shipping-information-list'>
//                <li>Area kota Surabaya : 1-3 hari kerja</li>
//                <li>Di luar kota Surabaya : 2-6 hari kerja</li>
//                <li>Dihitung setelah transaksi pembelian berhasil</li>
//            </ul>
//        ";
//        $container_right->add($txt_shipping);
//
//        $container = $app->add_div()->add_class('container-shipping-information');
//        $container_left = $container->add_div()->add_class('shipping-icon');
//        $container_right = $container->add_div()->add_class('shipping-text');
//        $container_left->add_div()->add_class('ico-gift');
//        $container_right->add_div()->add_class('shipping-title')->add(clang::__('Pesanan Sampai'));
//        $txt_shipping = "
//            <ul class='shipping-information-list'>
//                <li>Sebelum menandatangani pesanan, Anda setuju untuk memerika barang untuk memastikan bila ada kerusakan, kekeliruan, atau kekurangan.</li>
//                <li>Simpan bukti penerimaan saat menerima barang tersebut</li>
//            </ul>
//        ";
//        $container_right->add($txt_shipping);
//end old lasvegas
        $html_shipping_info = cms_options::get('shipping_info');

        $app->add($html_shipping_info);
        echo $app->render();
    }

    public function reviews($product_id) {
        $app = CApp::instance();
        $db = CDatabase::instance();

        $q = "SELECT review.review as review_text, member.name as member_name, review.created as review_date, member.image_name
                FROM review
                JOIN transaction_detail 
                    ON transaction_detail.transaction_detail_id = review.transaction_detail_id
                JOIN transaction 
                    ON transaction.transaction_id = transaction_detail.transaction_id
                JOIN member 
                    ON transaction.member_id = member.member_id
                JOIN product 
                    ON transaction_detail.product_id = product.product_id
                WHERE review.status > 0 
                    AND transaction_detail.product_id = " . $db->escape($product_id);
         
        $data = $db->query($q);

        if (count($data) == 0) {
            $app->add_div()->add(clang::__("There is no review for this product"));
        }

        foreach ($data as $key => $value) {
            $get_image = cobj::get($value, 'image_name');
            if (empty($get_image)) {
                $image = curl::base() . 'application/lapakbangunan/default/media/img/imbuilding/user_gray_150x150.png';
            } else {
                $image = image::get_image_url_front($get_image, 'view_profile');
            }

            $member_name = cobj::get($value, 'member_name');
            $review_txt = cobj::get($value, 'review_text');
            $review_date = cobj::get($value, 'review_date');
            $review_date = Date('d F Y H:i:s', strtotime($review_date));
            $container = $app->add_div()->add_class('container-product-review');
            $container_left = $container->add_div()->add_class('product-review-icon');
            $container_right = $container->add_div()->add_class('product-review-content');

            $container_left->add_div()->add_class('image-photo-review')->add("<img src='" . $image . "'>");
            $container_right->add_div()->add_div()->add_class('txt-review-name')->add($member_name);
            $container_right->add_div()->add_div()->add_class('txt-review-date')->add($review_date);
            $container_right->add_div()->add_div()->add_class('txt-review-msg')->add($review_txt);
        }
        echo $app->render();
    }

    public function faq($product_id) {
        $app = CApp::instance();

        $data_detail_product = product::get_product($product_id);
        $faq = carr::get($data_detail_product, 'faq', null);

        if (strlen($faq) == 0) {
            $faq = clang::__('Tidak ada FAQ untuk barang ini');
        }

        $app->add($faq);

        echo $app->render();
    }

    //</editor-fold>

    public function category($category_id = null) {
        $app = CApp::instance();
        $user = $app->user();
        $db = CDatabase::instance();
        $org_id = $this->org_id();
        CFBenchmark::start('benchmark_category');
        // SHOPPING CART
        $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");
        $container = $app->add_div()->add_class('container');
        $r_breadcrumb = $container->add_div()->add_class('row');
        $c_breadcrumb = $r_breadcrumb->add_div()->add_class('col-md-12');
        $breadcrumb = lapakbangunan::get_breadcrumb($category_id);
        $c_breadcrumb->add($breadcrumb);

        $container_row = $container->add_div()->add_class('row');
        $category_menu_container = $container_row->add_div()->add_class('col-md-3');
        $category_container_wrap = $container_row->add_div()->add_class('col-md-9');
        $category_container = $category_container_wrap->add_div()->add_class('row');

        $first_parent = product_category::get_first_parent_category($category_id);


        if ($first_parent == null) {
            curl::redirect(curl::base());
            return;
        }

        // CATEGORY ADVERTISE
        $list_image_cms_product_category = cms_product_category::get_cms_product_category($org_id, $category_id);
        $advertise_category = null;
        if (count($list_image_cms_product_category) == 0 && $first_parent != null) {
            $list_image_cms_product_category = cms_product_category::get_cms_product_category($org_id, $first_parent->product_category_id);
        }

        if (count($list_image_cms_product_category) > 0) {

            $advertise_category = $category_container->add_div('advertise-category')->add_class('col-md-12');
            $element_advertise_category = $advertise_category->add_element('62hall-category-advertise', 'category-advertise');
            $element_advertise_category->set_list_advertise($list_image_cms_product_category);
        }

        $sorting_wrap = $category_container->add_div()->add_class('col-md-12');
        $sorting_row = $sorting_wrap->add_div()->add_class('row');
        $sorting_left = $sorting_row->add_div()->add_class('col-md-5');
        $sorting_right = $sorting_row->add_div()->add_class('col-md-7');

        $sort_list = array(
            'diskon' => clang::__('Diskon'),
            'min_price' => clang::__('Termurah'),
            'max_price' => clang::__('Termahal'),
            'is_new' => clang::__('Terbaru'),
            'popular' => clang::__('Popular'),
            'A-Z' => clang::__('A-Z'),
            'Z-A' => clang::__('Z-A'),
        );

        $view_list = array(
            '9' => 9,
            '18' => 18,
            '36' => 36,
            '54' => 54,
            '81' => 81
        );


        $sort_view = $sorting_left->add_div()->add_class('container-sort-view');
        $sort_view->add_div()->add_class('txt-sort-by')->add(clang::__('Sort By'));
        $sort_view->add_div()->add_class('opt-sort-by')->add_control('category_sort_by', 'select')->set_applyjs('select2')->add_class('im-select select-filter')->set_list($sort_list);
        $sort_view->add_div()->add_class('txt-view')->add(clang::__('View'));
        $sort_view->add_div()->add_class('opt-view')->add_control('category_sort_view', 'select')->set_applyjs('select2')->add_class('im-select select-filter')->set_list($view_list);

        $container_pagination_top = $sorting_right->add_div()->add_class('container-pagination-top');

        $data_category_menu_choise = product_category::get_parent_category($category_id);

        $data_category_menu_choise_name = '';
        if ($data_category_menu_choise != null) {
            $data_category_menu_choise_name = $data_category_menu_choise->name;
        }


        $product_category = $category_container->add_div('page-product-category')->add_class('col-md-12');

        $txt_breadcrumb = lapakbangunan::get_breadcrumb($category_id, true);

        // CATEGORY MENU
        $category_menu_list = product_category::get_product_category_menu($this->page(), $first_parent->product_category_id);
        $child_menu_list = product_category::get_product_category_menu($this->page(), $category_id);
        $element_category_menu = $category_menu_container->add_element('62hall-category-menu', 'category-menu-' . $category_id);
        $element_category_menu->set_key($category_id)
                ->set_head_key($first_parent->product_category_id)
                ->set_head_menu($first_parent->name)
                ->set_list_menu($category_menu_list)
                ->set_menu_map($txt_breadcrumb);

        // CATEGORY FILTER
        //$data_product = cms_product::get_page_category($org_id, $this->page(), $category_id);

        $options = array(
            'product_type' => $this->page(),
            'product_category_id' => $category_id,
            'visibility' => 'catalog',
        );

        $list_attribute = product::get_attributes_array($options);

        $benchmark = CFBenchmark::get('benchmark_category');

        if (isset($_GET['benchmark'])) {
            cdbg::var_dump($benchmark);
        }

        $min_price = product::get_min_price($options);
        $max_price = product::get_max_price($options);
        if ($min_price == null) {
            $min_price = 0;
        }
        if ($max_price == null) {
            $max_price = 0;
        }
        $range_price = array(
            'min' => $min_price,
            'max' => $max_price,
        );

        $list_filter_product = array(
            'diskon' => clang::__('Diskon'),
            'min_price' => clang::__('Termurah'),
            'max_price' => clang::__('Termahal'),
            'is_new' => clang::__('Terbaru'),
            'popular' => clang::__('Popular'),
            'A-Z' => clang::__('A-Z'),
            'Z-A' => clang::__('Z-A'),
        );
        $q = "
                select
                    *
                from
                    product_category
                where
                    product_category_id=" . $db->escape($category_id) . "
            ";
        $r = $db->query($q);
        $category_lft = '';
        $category_rgt = '';
        if ($r->count() > 0) {
            $row = $r[0];
            $category_lft = $row->lft;
            $category_rgt = $row->rgt;
        }


        $element_filter = $category_menu_container->add_element('62hall-filter', 'category-filter')
                ->set_key($category_id)
                ->set_list_filter_product($list_filter_product)
                ->set_list_filter($list_attribute)
                ->set_product_visibility('catalog')
                ->set_filter_category_lft($category_lft)
                ->set_filter_category_rgt($category_rgt)
                ->set_filter_page($this->page())
                ->set_min_price($range_price['min'])
                ->set_max_price($range_price['max']);

        $filter_name = '';
        $product_category->add_listener('ready')
                ->add_handler('reload')
                ->set_target('page-product-category')
                ->set_url(curl::base() . "reload/reload_product_filter?view_limit=9&source=category&visibility=catalog&sortby=diskon&filter_page=" . $this->page() . '&filter_category_lft=' . $category_lft . '&filter_category_rgt=' . $category_rgt);

        echo $app->render();
    }

    public function get_breadcrumb($category_id) {
        $data = product_category::get_parent_category($category_id);



        cdbg::var_dump($data);
        die;
    }

    private function generate_list($type, $data = array()) {
        $list = array();
        if (is_array($data)) {
            foreach ($data as $key => $row_data) {
                if ($type == $this->type_select) {
                    $value = carr::get($row_data, 'attribute_key');
                }
                if ($type == $this->type_image) {
                    $value['key'] = carr::get($row_data, 'attribute_key');
                    $value['value'] = carr::get($row_data, 'file_path');
                }

                $list[$key] = $value;
            }
        }

        return $list;
    }

    public function generate_attribute($code, $product_id) {
        $app = CApp::instance();

        $request = $_GET;

        $param = array();
        foreach ($request as $key => $row_request) {
            if ($key != 'capp_current_container_id') {
                $param[] = $row_request;
            }
        }

        $attribute_category = product::get_attribute_category_all($product_id);
        $arr_keys = array_keys($attribute_category);
        $last_key_code = end($arr_keys);
        $attibute_data = carr::get($attribute_category, $code);
        $id = carr::get($attibute_data, 'attribute_category_id');
        $type = carr::get($attibute_data, 'attribute_category_type');
        $prev = carr::get($attibute_data, 'prev_attribute_category');
        $next = carr::get($attibute_data, 'next_attribute_category');

        $attribute = product::get_attribute_list($product_id, $id, $param);

        $list = $this->generate_list($type, carr::get($attribute, 'data'));
        $arr_param = array();
        if (is_array($prev)) {
            foreach ($prev as $row_prev) {
                $temp_attibute_data = $attribute_category[$row_prev];
                $temp_id = carr::get($temp_attibute_data, 'attribute_category_id');
                $arr_param[] = 'att_cat_' . $temp_id;
            }
        }

        if ($type == $this->type_select) {
            $div_container = $app->add_div('container-' . $code);
            $select = $div_container->add_field()
                    ->add_control('att_cat_' . $id, 'select')
                    ->add_class('select-62hallfamily margin-bottom-10 att_cat_' . $id)
                    ->custom_css('margin-left', '15px')
                    ->custom_css('width', '130px')
                    ->set_list($list);

            if (!empty($next) and count($list) > 0) {
                $listener = $select->add_listener('ready');

                $handler = $listener->add_handler('reload')
                        ->set_target('container-' . $next)
                        ->add_param_input($arr_param)
                        ->set_url(curl::base() . 'product/generate_attribute/' . $next . '/' . $product_id);

                $listener = $select->add_listener('change');

                $handler = $listener->add_handler('reload')
                        ->set_target('container-' . $next)
                        ->add_param_input($arr_param)
                        ->set_url(curl::base() . 'product/generate_attribute/' . $next . '/' . $product_id);
            }
            if ($code == $last_key_code) {
                $listener = $select->add_listener('ready');

                $handler = $listener->add_handler('reload')
                        ->set_target('stock-price')
                        ->add_param_input($arr_param)
                        ->set_url(curl::base() . 'reload/reload_stock_price_product/' . $code . '/' . $product_id);

                $listener = $select->add_listener('change');

                $handler = $listener->add_handler('reload')
                        ->set_target('stock-price')
                        ->add_param_input($arr_param)
                        ->set_url(curl::base() . 'reload/reload_stock_price_product/' . $code . '/' . $product_id);
            }
        } else if ($type == $this->type_image) {
            $div_container = $app->add_div('container-' . $code);
            $index = 1;
            foreach ($list as $key => $value) {
                $active = null;
                if ($index == 1) {
                    $active = 'active';
                    $div_container->add_control('att_cat_' . $id, 'hidden')
                            ->set_value($key);
                }
                $image = $div_container->add_div($key)
                        ->add_class('btn-colors margin-bottom-10 link att_cat_' . $id . ' ' . $active)
                        ->custom_css('display', 'inline-block')
                        ->custom_css('padding', '2px')
                        ->add('<img src="' . $value['value'] . '" style="width:20px; height:20px"/>');

                if (!empty($next) and count($list) > 0) {
                    $listener = $image->add_listener('ready');

                    $handler = $listener->add_handler('reload')
                            ->set_target('container-' . $next)
                            ->add_param_input($arr_param)
                            ->set_url(curl::base() . 'product/generate_attribute/' . $next . '/' . $product_id);

                    $listener = $image->add_listener('click');

                    $handler = $listener->add_handler('reload')
                            ->set_target('container-' . $next)
                            ->add_param_input($arr_param)
                            ->set_url(curl::base() . 'product/generate_attribute/' . $next . '/' . $product_id);
                }
                if ($code == $last_key_code) {
                    $listener = $image->add_listener('ready');

                    $handler = $listener->add_handler('reload')
                            ->set_target('stock-price')
                            ->add_param_input($arr_param)
                            ->set_url(curl::base() . 'reload/reload_stock_price_product/' . $code . '/' . $product_id);

                    foreach ($arr_param as $inp) {
                        if (strlen($data_addition) > 0) {
                            $data_addition .= ',';
                        }

                        $data_addition .= "'" . $inp . "':$.cresenity.value('#" . $inp . "')";
                    }

                    $data_addition = '{' . $data_addition . '}';

                    $listener = $image->add_listener('click');

                    $handler = $listener->add_handler('custom')
                            ->set_js("
                                    var key=$(this).attr('id');
                                    $('#" . 'att_cat_' . $id . "').val(key);

                                    $.cresenity.reload('stock-price','" . curl::base() . "reload/reload_stock_price_product/" . $code . "/" . $product_id . "','get'," . $data_addition . ");
                            ");
                }

                $index++;
            }
        }

        echo $app->render();
    }

}
