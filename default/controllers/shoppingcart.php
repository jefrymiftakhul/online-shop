<?php

class Shoppingcart_Controller extends SixtyTwoHallFamilyController {

    public function icon_shopping_cart() {
        $app = CApp::instance();
        $cart = Cart62hall_Product::factory();
        $cart_list = $cart->get_cart();
        $count_item = $cart->item_count();

        $shopping_cart = $app->add_div()->add_class('dropdown');
        $icon = $shopping_cart->add_div()->add('<a href="#" class="dropdown-toggle icon-shopping-cart margin-top-3- pull-right bold" data-toggle="dropdown" role="button" aria-expanded="false"></a>');
        $count_cart = $shopping_cart->add_div()->add_class('count font-white')
                ->add($count_item);

        $container_cart = $icon->add('<ul class="dropdown-menu font-black container-cart" role="menu">');
        $triangle = $icon->add_div()->add_class('triangle arrow-up');
        $icon->add_div()->add_class('font-big bold font-red')->add('Keranjang Belanja Anda');
        $container_item = $container_cart->add('<li>');

        $total_barang = 0;
        $total_harga = 0;
        if ($count_item > 0) {
            $items = $container_item->add_div()->add_class('horizontal-overflow')->custom_css('max-height', '205px');

            foreach ($cart_list as $data_cart) {
                $total_barang += $data_cart['qty'];
                $total_harga += $data_cart['sub_total'];

                $items->add('
                                <div class="item col-md-12 col-xs-12 padding-left-right0">
                                    <div class="col-md-3 col-xs-3 padding-0 margin-right-10">
                                    <img src="' . $data_cart['image'] . '"/>
                                    </div>
                                    <div class="col-md-8 col-xs-8 content small padding-0 bold">
                                        ' . $data_cart['name'] . '
                                        <br>
                                        <div class="small">
                                        <i>Rp.' . ctransform::format_currency($data_cart['sell_price']) . '</i>
                                        <br>
                                        <i>Jumlah ' . $data_cart['qty'] . ' Barang</i>
                                        <div class="font-red col-md-12 col-xs-12 padding-0 margin-top-20">
                                            <div class="col-md-6 col-xs-6 padding-0">
                                                Sub Total
                                            </div>
                                            <div class="col-md-6 col-xs-6 padding-0">
                                                Rp.' . ctransform::format_currency($data_cart['sub_total']) . '
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>'
                );
            }
        }

        $container_total = $container_item->add_div()->add_class('margin-top-10 col-md-12 padding-0');
        $container_total->add_div()->add_class('col-md-5 padding-0 small bold')->add('TOTAL BARANG');
        $container_total->add_div()->add_class('col-md-7 padding-0 small bold')->add(': ' . $total_barang . ' Barang');
        $container_total->add_div()->add_class('col-md-5 padding-0 small bold font-red')->add('TOTAL HARGA');
        $container_total->add_div()->add_class('col-md-7 padding-0 small bold font-red')->add(': Rp. ' . ctransform::format_currency($total_harga));
        $container_total->add_div()->add_class('col-md-12 padding-0 font-size10 bold italic')->add('<red>*</red> Harga di luar biaya kirim.');

        $container_checkout = $container_item->add_div()->add_class('col-md-12 padding-0');

        $container_checkout->add_action()
                ->set_label(clang::__('Check Out'))
                ->add_class('btn-62hallfamily-small bg-red pull-right')
                ->add_listener('click')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . 'shoppingcart/check_out');

        $container_cart = $icon->add('</li>');
        $container_cart = $icon->add('</ul>');

        echo $app->render();
    }

    public function add_item_cart($product_id) {
        $app = CApp::instance();
        $cart = Cart62hall_Product::factory();
        $request = array_merge($_POST, $_GET);

        // get data product 
        $data_product = product::get_product($product_id);
        $data_product_image = product::get_product_image($product_id);

        $cart_list = $cart->get_cart();
        if ($cart->item_count() > 0) {
            foreach ($cart_list as $key => $row_cart) {
                $list_item_code[$key] = carr::get($row_cart, "product_id");
                $list_count_item[$key] = carr::get($row_cart, "qty");
            }
        }

        if (isset($list_item_code) and in_array($product_id, $list_item_code)) {
            $index_item = array_search($product_id, $list_item_code);
            $qty = $list_count_item[$index_item] + $request['qty'];
        }

        $qty = isset($qty) ? $qty : 1;
        $image = isset($data_product_image[0]['image_path']) ? $data_product_image[0]['image_path'] : NULL;
        $name = carr::get($data_product, "name", NULL);
        $sell_price = carr::get($data_product, "sell_price", 0);
        $sub_total = $qty * $sell_price;

        $data_item = array(
            'product_id' => $product_id,
            'name' => $name,
            'image' => $image,
            'qty' => $qty,
            'sell_price' => $sell_price,
            'sub_total' => $sub_total,
        );

        if (!isset($index_item)) {
            $cart->add_item($data_item);
        } else {
            $cart->update_item($index_item, $data_item);
        }

        $this->icon_shopping_cart();
    }

}
