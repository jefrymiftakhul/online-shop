<?php

/**
 *
 * @author Riza 
 * @since  Nov 19, 2015
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class Request_Controller extends SixtyTwoHallFamilyController {

    CONST __CONTROLLER = "request/";

    public function __construct() {
        parent::__construct();
    }

    public function add() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $post = $this->input->post();
        $error = 0;
        $error_message = '';
        $submit_request = carr::get($post, 'submit_request');
        if ($post != null) {
            if ($submit_request) {

                $session_app = Session::instance();
                $member_id = $session_app->get('member_id');
                $transaction_type = carr::get($post, 'transaction_type');

                $name_pay = carr::get($post, 'name_pay');
                $email_pay = carr::get($post, 'email_pay');
                $address_pay = carr::get($post, 'address_pay');
                $phone_area_pay = carr::get($post, 'phone_area_pay');
                $phone_pay = carr::get($post, 'phone_pay');
                $province_pay = carr::get($post, 'province_pay');
                $city_pay = carr::get($post, 'city_pay');
                $district_pay = carr::get($post, 'districts_pay');
                $post_code_pay = carr::get($post, 'post_code_pay');

                $name_shipping = carr::get($post, 'name_shipping');
                $email_shipping = carr::get($post, 'email_shipping');
                $address_shipping = carr::get($post, 'address_shipping');
                $phone_area_shipping = carr::get($post, 'phone_area_shipping');
                $phone_shipping = carr::get($post, 'phone_shipping');
                $province_shipping = carr::get($post, 'province_shipping');
                $city_shipping = carr::get($post, 'city_shipping');
                $district_shipping = carr::get($post, 'districts_shipping');
                $post_code_shipping = carr::get($post, 'post_code_shipping');

                $post_shipping_type_id = carr::get($post, 'shipping_type_id');
                $post_shipping_price = carr::get($post, 'shipping_price');
                $shipping_price = str_replace('Rp. ', '', $post_shipping_price);
                $shipping_price = ctransform::unformat_currency($shipping_price);
                $page = carr::get($post, 'page');
                $product_id = carr::get($post, 'product_id');
                $qty = carr::get($post, 'qty');

                $arr_check = array(
                    'post_code_shipping', 'districts_shipping', 'city_shipping', 'province_shipping', 'city_shipping', 'phone_shipping', 'address_pay', 'email_shipping', 'name_shipping',
                    'post_code_pay', 'districts_pay', 'city_pay', 'province_pay', 'city_pay', 'phone_pay', 'address_pay', 'email_pay', 'name_pay',
                );

                $field = NULL;
                foreach ($arr_check as $key_post) {
                    if (strlen(carr::get($post, $key_post)) == 0) {
                        if ($key_post == 'name_pay') {
                            $field = 'Nama Lengkap Pembayaran';
                        }
                        if ($key_post == 'email_pay') {
                            $field = 'Email Pembayaran';
                        }
                        if ($key_post == 'address_pay') {
                            $field = 'Alamat Pembayaran';
                        }
                        if ($key_post == 'phone_pay') {
                            $field = 'No. HP Pembayaran';
                        }
                        if ($key_post == 'province_pay') {
                            $field = 'Propinsi Pembayaran';
                        }
                        if ($key_post == 'city_pay') {
                            $field = 'Kota Pembayaran';
                        }
                        if ($key_post == 'districts_pay') {
                            $field = 'Kecamatan Pembayaran';
                        }
                        if ($key_post == 'post_code_pay') {
                            $field = 'Kode Pos Pembayaran';
                        }
                        if ($key_post == 'name_shipping') {
                            $field = 'Nama Lengkap Pengiriman';
                        }
                        if ($key_post == 'email_shipping') {
                            $field = 'Email Pengiriman';
                        }
                        if ($key_post == 'address_shipping') {
                            $field = 'Alamat Pengiriman';
                        }
                        if ($key_post == 'phone_shipping') {
                            $field = 'No. HP Pengiriman';
                        }
                        if ($key_post == 'province_shipping') {
                            $field = 'Propinsi Pengiriman';
                        }
                        if ($key_post == 'city_shipping') {
                            $field = 'Kota Pengiriman';
                        }
                        if ($key_post == 'districts_shipping') {
                            $field = 'Kecamatan Pengiriman';
                        }
                        if ($key_post == 'post_code_shipping') {
                            $field = 'Kode Pos Pengiriman';
                        }

                        $error++;
                        $error_message = $field . ' belum diisi.';
                    }
                }

                if ($error == 0) {
                    $post_shipping_price = carr::get($post, 'shipping_price');

                    if ($post_shipping_price == '-') {
                        $error++;
                        $error_message = 'Maaf Pengiriman ke Kota Anda Tidak Tersedia';
                    }
                }

                //checking
                $db->begin();
                try {
                    if ($error == 0) {
                        $first_name_pay = $name_pay;
                        $last_name_pay = '';
                        $first_name_shipping = $name_shipping;
                        $last_name_shipping = '';
                        $arr_name_pay = explode(' ', $name_pay, 2);
                        $arr_name_shipping = explode(' ', $name_shipping, 2);
                        if (count($arr_name_pay) == 2) {
                            $first_name_pay = $arr_name_pay[0];
                            $last_name_pay = $arr_name_pay[1];
                        }

                        if (count($arr_name_shipping) == 2) {
                            $first_name_shipping = $arr_name_shipping[0];
                            $last_name_shipping = $arr_name_shipping[1];
                        }
                        $province_pay_text = NULL;
                        if ($province_pay) {
                            $q = "
											select
													*
											from
													province
											where
													province_id=" . $province_pay . "
									";
                            $r = $db->query($q);
                            if ($r->count() > 0) {
                                $province_pay_text = $r[0]->name;
                            }
                        }
                        $city_pay_text = NULL;
                        if ($city_pay) {
                            $q = "
											select
													*
											from
													city
											where
													city_id=" . $city_pay . "
									";
                            $r = $db->query($q);
                            if ($r->count() > 0) {
                                $city_pay_text = $r[0]->name;
                            }
                        }
                        $district_pay_text = NULL;
                        if ($district_pay) {
                            $q = "
											select
													*
											from
													districts
											where
													districts_id=" . $district_pay . "
									";
                            $r = $db->query($q);
                            if ($r->count() > 0) {
                                $district_pay_text = $r[0]->name;
                            }
                        }

                        $province_shipping_text = NULL;
                        if ($province_shipping) {
                            $q = "
											select
													*
											from
													province
											where
													province_id=" . $province_shipping . "
									";
                            $r = $db->query($q);
                            if ($r->count() > 0) {
                                $province_shipping_text = $r[0]->name;
                            }
                        }

                        $city_shipping_text = NULL;
                        if ($city_shipping) {
                            $q = "
											select
													*
											from
													city
											where
													city_id=" . $city_shipping . "
									";
                            $r = $db->query($q);
                            if ($r->count() > 0) {
                                $city_shipping_text = $r[0]->name;
                            }
                        }
                        $district_shipping_text = NULL;
                        if ($district_shipping) {
                            $q = "
											select
													*
											from
													districts
											where
													districts_id=" . $district_shipping . "
									";
                            $r = $db->query($q);
                            if ($r->count() > 0) {
                                $district_shipping_text = $r[0]->name;
                            }
                        }
                        //handling billing
                        if ($error == 0) {
                            if (strlen($first_name_pay) > 20) {
                                $error++;
                                $error_message = 'Billing first name must be least than equal to 20 characters.';
                            }
                        }

                        if ($error == 0) {
                            if (strlen($last_name_pay) > 20) {
                                $error++;
                                $error_message = 'Billing last name must be least than equal to 20 characters.';
                            }
                        }

                        if ($error == 0) {
                            if (strlen($address_pay) > 200) {
                                $error++;
                                $error_message = 'Billing address must be least than equal to 200 characters.';
                            }
                        }

                        if ($error == 0) {
                            if (strlen($city_pay_text) > 100) {
                                $error++;
                                $error_message = 'Billing city must be least than equal to 100 characters.';
                            }
                        }

                        if ($error == 0) {
                            if (strlen($post_code_pay) > 10) {
                                $error++;
                                $error_message = 'Billing postal code must be least than equal to 10 characters.';
                            }
                        }
                        if ($error == 0) {
                            if (strlen($phone_area_pay . $phone_pay) > 19) {
                                $error++;
                                $error_message = 'Billing phone must be least than equal to 19 characters.';
                            }
                        }

                        //handling shipping
                        if ($error == 0) {
                            if (strlen($first_name_shipping) > 20) {
                                $error++;
                                $error_message = 'Shipping first name must be least than equal to 20 characters.';
                            }
                        }

                        if ($error == 0) {
                            if (strlen($last_name_shipping) > 20) {
                                $error++;
                                $error_message = 'Shipping last name must be least than equal to 20 characters.';
                            }
                        }

                        if ($error == 0) {
                            if (strlen($address_shipping) > 200) {
                                $error++;
                                $error_message = 'Shipping address must be least than equal to 200 characters.';
                            }
                        }

                        if ($error == 0) {
                            if (strlen($city_shipping_text) > 100) {
                                $error++;
                                $error_message = 'Shipping city must be least than equal to 100 characters.';
                            }
                        }

                        if ($error == 0) {
                            if (strlen($post_code_shipping) > 10) {
                                $error++;
                                $error_message = 'Shipping postal code must be least than equal to 10 characters.';
                            }
                        }
                        if ($error == 0) {
                            if (strlen($phone_area_shipping . $phone_shipping) > 19) {
                                $error++;
                                $error_message = 'Shipping phone must be least than equal to 19 characters.';
                            }
                        }
                        if ($this->org_id()) {
                            $org = org::get_org($this->org_id());
                        } else {
                            $err_code++;
                            $err_message = 'Data Merchant not Valid';
                        }
                        if ($error == 0) {
                            $product = product::get_product($product_id);
                            $detail_price = $product['detail_price'];
                            $transaction_code_generate = generate_code::get_next_transaction_code();
                            //insert transaction
                            $data_transaction = array(
                                'org_id' => $this->org_id(),
                                'org_parent_id' => $org->parent_id,
                                'member_id' => $member_id,
                                'code' => $transaction_code_generate,
                                'email' => $email_pay,
                                'date' => date('Y-m-d H:i:s'),
                                'type' => $transaction_type,
                                'product_type' => $page,
                                'transaction_status' => 'REQUEST',
                                'order_status' => 'REQUEST',
                                'payment_status' => 'REQUEST',
                                'shipping_status' => 'REQUEST',
                                'vendor_nta' => $detail_price['vendor_nta'] * $qty,
                                'vendor_commission_value' => $detail_price['vendor_commission_value'] * $qty,
                                'vendor_sell_price' => $detail_price['vendor_sell_price'] * $qty,
                                'total_promo' => $detail_price['promo_amount'] * $qty,
                                'ho_upselling' => $detail_price['ho_upselling'] * $qty,
                                'ho_sell_price' => $detail_price['ho_sell_price'] * $qty,
                                'channel_commission_full' => $detail_price['channel_commission_full'] * $qty,
                                'channel_commission_ho' => $detail_price['channel_commission_ho'] * $qty,
                                'channel_commission_share' => $detail_price['channel_commission_share'] * $qty,
                                'channel_commission_share_ho' => $detail_price['channel_commission_share_ho'] * $qty,
                                'channel_commission_value' => $detail_price['channel_commission_value'] * $qty,
                                'channel_updown' => $detail_price['channel_updown'] * $qty,
                                'channel_sell_price' => $detail_price['channel_sell_price'] * $qty,
                                'channel_profit' => $detail_price['channel_profit'] * $qty,
                                'total_shipping' => $shipping_price,
                                'is_request' => 1,
                                'created' => date('Y-m-d H:i:s'),
                                'createdby' => $this->org_code(),
                                'updated' => date('Y-m-d H:i:s'),
                                'updatedby' => $this->org_code(),
                            );
                            $r = $db->insert('transaction', $data_transaction);
                            $transaction_id = $r->insert_id();

                            $data_transaction_history=array(
                                    'org_id' => $this->org_id(),
                                    'org_parent_id' => $org->parent_id,
                                    'transaction_id' => $transaction_id,
                                    'date' => date('Y-m-d H:i:s'),
                                    'transaction_status_before'=>'',
                                    'transaction_status'=>'REQUEST',
                                    'ref_table'=>'transaction',
                                    'ref_id'=>$transaction_id,
                            );
                            $db->insert('transaction_history',$data_transaction_history);

                            //insert transaction contact
                            $data_transaction_contact = array(
                                'transaction_id' => $transaction_id,
                                'billing_first_name' => $first_name_pay,
                                'billing_last_name' => $last_name_pay,
                                'billing_email' => $email_pay,
                                'billing_phone' => $phone_area_pay . $phone_pay,
                                'billing_address' => $address_pay,
                                'billing_province_id' => $province_pay,
                                'billing_province' => $province_pay_text,
                                'billing_city_id' => $city_pay,
                                'billing_city' => $city_pay_text,
                                'billing_districts_id' => $district_pay,
                                'billing_district' => $district_pay_text,
                                'billing_postal_code' => $post_code_pay,
                                'shipping_first_name' => $first_name_shipping,
                                'shipping_last_name' => $last_name_shipping,
                                'shipping_email' => $email_shipping,
                                'shipping_phone' => $phone_area_shipping . $phone_shipping,
                                'shipping_address' => $address_shipping,
                                'shipping_province_id' => $province_shipping,
                                'shipping_province' => $province_shipping_text,
                                'shipping_city_id' => $city_shipping,
                                'shipping_city' => $city_shipping_text,
                                'shipping_districts_id' => $district_shipping,
                                'shipping_district' => $district_shipping_text,
                                'shipping_postal_code' => $post_code_shipping,
                                'created' => date('Y-m-d H:i:s'),
                                'createdby' => $this->org_code(),
                                'updated' => date('Y-m-d H:i:s'),
                                'updatedby' => $this->org_code(),
                            );
                            $r = $db->insert('transaction_contact', $data_transaction_contact);

                            //insert transaction detail
                            $data_transaction_detail = array(
                                'transaction_id' => $transaction_id,
                                'vendor_id' => $product['vendor_id'],
                                'product_id' => $product_id,
                                'qty' => $qty,
                                'vendor_nta' => $detail_price['vendor_nta'],
                                'vendor_commission_value' => $detail_price['vendor_commission_value'],
                                'vendor_sell_price' => $detail_price['vendor_sell_price'],
                                'total_promo' => $detail_price['promo_amount'],
                                'ho_upselling' => $detail_price['ho_upselling'],
                                'ho_sell_price' => $detail_price['ho_sell_price'],
                                'channel_commission_full' => $detail_price['channel_commission_full'],
                                'channel_commission_ho' => $detail_price['channel_commission_ho'],
                                'channel_commission_share' => $detail_price['channel_commission_share'],
                                'channel_commission_share_ho' => $detail_price['channel_commission_share_ho'],
                                'channel_commission_value' => $detail_price['channel_commission_value'],
                                'channel_updown' => $detail_price['channel_updown'],
                                'channel_sell_price' => $detail_price['channel_sell_price'],
                                'channel_profit' => $detail_price['channel_profit'],
                                'created' => date('Y-m-d H:i:s'),
                                'createdby' => $this->org_code(),
                                'updated' => date('Y-m-d H:i:s'),
                                'updatedby' => $this->org_code(),
                            );
                            $db->insert('transaction_detail', $data_transaction_detail);
                            $booking_code = generate_code::generate_booking_code($transaction_id);
                            $data = array(
                                'booking_code' => $booking_code,
                            );
                            $db->update('transaction', $data, array('transaction_id' => $transaction_id));

                            $session_app->set('request_id', $transaction_id);
                        }
                    }
                } catch (Exception $e) {
                    $error++;
                    $error_message = clang::__("system_fail") . $e->getMessage();
                }
                if ($error == 0) {
                    $db->commit();
                } else {
                    $db->rollback();
                }
                if ($error == 0) {
                    curl::redirect('request/info');
                }
            }
        }
        $attribute = array();
        $count_item = 1;
        foreach ($post as $key_req => $value_req) {
            if (substr($key_req, 0, 8) == 'att_cat_') {
                $attribute[substr($key_req, 8)] = $value_req;
            }
        }
        // get data product 
        $product_id = carr::get($post, 'product_id');
        $page = carr::get($post, 'page');
        $qty = carr::get($post, 'qty');
        if ($qty == null) {
            $qty = 1;
        }
        $product_simple = product::get_simple_product_from_attribute($product_id, $attribute);


        $country_id = '';
        $q = "
                select
                 *
                from
                    country
                where
                    name=" . $db->escape('Indonesia') . "
            ";
        $r = $db->query($q);
        if ($r->count() > 0) {
            $country_id = $r[0]->country_id;
        }

        $session_php = Session::instance();
        if(ccfg::get("transaction_member_only")>0){
            $member_id = $session_php->get('member_id');
            if($member_id == null && strlen($member_id)==0){
                $page_url=$page;
                if($page!='product'){
                    $page_url.='/product';
                }
                $product=cdbutils::get_row("select * from product where product_id=".$db->escape($product_id));
                cmsg::add('error',clang::__("Harap login member sebelum melakukan transaksi"));
                curl::redirect($page_url.'/item/'. cobj::get($product,'url_key'));
            }
        }


        // SHOPPING CART 
        $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

        // Menu
        $product_category = product_category::get_product_category_menu($page);

        // Menu
        $menu_list = array(
            'name' => 'KATEGORI',
            'menu' => $product_category
        );

        $link_list = array(
            array(
                'label' => 'GOLD',
                'url' => '/gold/home',
            ),
            array(
                'label' => 'JASA',
                'url' => '/service/home',
            )
        );

        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_menu($menu_list);
        $element_menu->set_link($link_list);


        $form = $app->add_form('form_request')
                ->add_class('form-62hallfamily form-gold-contact padding-0');
        $form->add_control('page', 'hidden')
                ->set_value($page);
        $form->add_control('product_id', 'hidden')
                ->set_value($product_simple->product_id);
        $form->add_control('transaction_type', 'hidden')->set_value('buy');
        $form->add_control('qty', 'hidden')->set_value($qty);
        $form->add_control('submit_request', 'hidden')->set_value('1');

        $container = $form->add_div()->add_class('container');

        // UPDATE CONTACT
        $view_update_contact = $container->add_div()
                ->add_class('col-md-7 margin-top-30 margin-bottom-30')
                ->custom_css('padding-left', '0px');

        if (!empty($error_message)) {
            $alert = 'success';
            if ($error > 0) {
                $alert = 'danger';
            }
            $alert_message = $view_update_contact->add_div()->add_class('alert alert-' . $alert)->add($error_message);
        }


        $element_update_contact = $view_update_contact->add_element('62hall-update-contact', 'element_update_contact');
        $element_update_contact->set_country_id($country_id);
        if (($page == 'gold') || ($page == 'service')) {
            $element_update_contact->set_using_shipping(false);
        }


        // RINGKASAN PEMESASAN
        $total_barang = 0;
        $total_harga = 0;
        $view_shopping_cart = $container->add_div()->add_class('col-md-5 ringkasan-pemesanan margin-top-30 margin-bottom-30');
        $div_shopping_cart = $view_shopping_cart->add_div()->add_class('col-md-12 container-cart margin-bottom-20');

        $div_shopping_cart->add_div()->add_class('font-size22 bold font-red margin-bottom-10')->add('Ringkasan Pemesanan');

        if ($count_item > 0) {
            $items = $div_shopping_cart->add_div()->add_class('horizontal-overflow')->custom_css('max-height', '250px');
            $product = product::get_product($product_simple->product_id);
            $cart_list[] = array(
                'product_id' => $product_simple->product_id,
                'name' => $product['name'],
                'image' => $product['image_path'],
                'sell_price' => $product['detail_price']['channel_sell_price'],
                'price' => $product['detail_price']['ho_sell_price'],
                'qty' => 1,
                'sub_total' => $product['detail_price']['channel_sell_price'],
            );


            foreach ($cart_list as $data_cart) {
                $total_barang += $data_cart['qty'];
                $total_harga += $data_cart['sub_total'];

                $image = $this->get_image($data_cart['image']);
                $items->add('
                                <div class="item col-md-12 col-xs-12 padding-left-right0 padding-top-10">
                                    <div class="col-md-2 col-xs-3 padding-0 margin-right-10">
                                    <img class="image-item border-1" src="' . $image . '"/>
                                    </div>
                                    <div class="col-md-9 col-xs-9 content padding-0 bold">
                                        ' . $data_cart['name'] . '
                                        <br>
                                        <div class="font-black">
                                        Jumlah : ' . $data_cart['qty'] . '
                                        </div>
                                    </div>
                                </div>'
                );
            }
        }

        $container_total = $div_shopping_cart->add_div()->add_class('margin-top-10 col-md-12 padding-0');
        $container_total->add_div()->add_class('col-md-5 padding-0 bold')->add('TOTAL BARANG');
        $container_total->add_div()->add_class('col-md-7 padding-0 bold')->add(': ' . $total_barang . ' Barang');

        $view_shopping_cart->add_action('submit_button')
                ->set_label(clang::__('Lanjutkan Request'))
                ->add_class('btn-62hallfamily col-md-12 bg-red border-3-red font-size22 link')
                ->set_submit(TRUE);

        echo $app->render();
    }

    public function info() {
        $app = CApp::instance();
        $session = Session::instance();
        $request_booking_code = $session->get_once('request_id');

        $container = $app->add_div()->add_class('container margin-top-30');
        $row = $container->add_div()->add_class('row');

        $icon = $row->add_div()->add_class('col-md-3');
        $image_send = curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/send_message.png';
        $icon->add('<img src="' . $image_send . '" width="180px" />');

        $confirm = $row->add_div()->add_class('col-md-9');

        if ($request_booking_code) {
            $transaction_info = transaction::get_transaction_contact('t.transaction_id', $request_booking_code);

            $confirm->add_div()->add_class('bold font-size24')->add('Dear Bpk/Ibu. ' . $transaction_info->billing_first_name);
            $confirm->add_div()->add_class('font-size20 bold')->add('Terima Kasih Bpk/Ibu telah melakukan permintaan barang/ jasa di ' . $this->store());
            $confirm->add_br();
            $confirm->add_div()->add_class('font-size20')->add('Permintaan barang/ jasa Bpk/Ibu sedang kami proses.');
            $confirm->add_div()->add_class('font-size20')->add('Kami segera menginformasikan ketersediaan permintaan barang/ jasa Bpk/Ibu melalui email Anda.');

            $this->send_transaction('request', $transaction_info->booking_code, 'ORDER');
        } else {
            $confirm->add_div()->add_class('bold font-size24')->add('Maaf permintaan Barang/ Jasa Anda ditemukan.');
        }

        $confirm->add_br();

        $backtohome = $container->add_div()->add_class('col-md-2 col-md-offset-5');
        $button = $backtohome->add_action()
                ->set_label(clang::__('kembali ke home'))
                ->add_class('btn-62hallfamily bg-red border-3-red upper font-size20')
                ->set_link(curl::base());

        $backtohome->add_br();
        $backtohome->add_br();

        echo $app->render();
    }

    private function get_image($image_name) {
        $ch = curl_init(image::get_image_url($image_name, 'view_all'));
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_exec($ch);
        $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($retcode == 500 or $image_name == NULL) {
            $image_name = curl::base() . 'application/62hallfamily/default/media/img/product/no-image.png';
        } else {
            $image_name = image::get_image_url($image_name, 'view_all');
        }
        curl_close($ch);
        return $image_name;
    }

}
