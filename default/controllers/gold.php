<?php

/**
 *
 * @author Riza 
 * @since  Nov 12, 2015
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class Gold_Controller extends SixtyTwoHallFamilyController {

    private $page = "gold";
    private $_count_item = 12;
    // ADVERTISE 
    private $advertise_1 = 0;

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        curl::redirect('gold/home');
        return;
        $app = CApp::instance();
        $user = $app->user();
        //~~
        $org_id = ccfg::get('org_id');

        // MENU
        $link_list = array(
            array(
                'label' => 'HOME',
                'url' => '/',
            ),
            array(
                'label' => 'JASA',
                'url' => '/service',
            )
        );

        // string
        $absolute_menu = "";
        $absolute_menu .= "<div class='font-size24 col-md-12'>Anda Ingin";
        $absolute_menu .= "<form class='form-62hallfamily font-big'>";

        $absolute_menu .= "<div class='row'>";
        $absolute_menu .= "<div class='col-md-3'><input name='JB' type='radio' value='J'> Jual</div>";
        $absolute_menu .= "<div class='col-md-3'><input name='JB' type='radio' value='B'> Beli</div>";
        $absolute_menu .= "</div>";

        $absolute_menu .= "<div class='row'>";
        $absolute_menu .= "<div class='col-md-12 margin-top-20'>Jenis Logam Mulia</div>";
        $absolute_menu .= "</div>";

        $absolute_menu .= "<select class='select-62hallfamily'>";
        $absolute_menu .= "</select>";

        $absolute_menu .= "<div class='row'>";
        $absolute_menu .= "<div class='col-md-12 margin-top-10'>Pecahan</div>";
        $absolute_menu .= "</div>";

        $absolute_menu .= "<select class='select-62hallfamily'>";
        $absolute_menu .= "</select>";

        $absolute_menu .= "<div class='row'>";
        $absolute_menu .= "<div class='col-md-12 margin-top-10'>";
        $absolute_menu .= "<input type='submit' class='col-md-12 margin-top-10 btn-62hallfamily bg-yellow font-size20' value='Find'>";
        $absolute_menu .= "</div>";
        $absolute_menu .= "</div>";

        $absolute_menu .= "</form>";
        $absolute_menu .= "</div>";

        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_link($link_list)
                ->set_absolute_menu($absolute_menu)
                ->set_position_absolute_menu('right');

        // SLIDE
        $slide_list = slide::get_slide($org_id, $this->page);

        $element_slide_show = $app->add_element('62hall-slide-show', 'element_slide_show');
        $element_slide_show->set_type('product');
        $element_slide_show->set_slides($slide_list);

        $subcribe = $app->add_div()->add_class('bg-gray');
        $subcribe_container = $subcribe->add_div()->add_class('container padding-30');

        $form_subscribe = $subcribe_container->add_form()
                ->add_class('form-62hallfamily');

        $form_subscribe->add('<div class="font-red bold font-size24 col-md-12">DAFTARKAN EMAIL ANDA</div>');

        $form_left = $form_subscribe->add_div()->add_class('col-md-5 font-size23')
                ->add('Dapatkan update harga harian logam mulia');
        $form_right = $form_subscribe->add_div()->add_class('col-md-7');

        $form_right_input = $form_right->add_div()->add_class('col-md-9');
        $form_right_input->add_control('', 'text')
                ->set_placeholder('Enter your email address');

        $action = $form_right->add_div()->add_class('col-md-3');
        $action->add_action()
                ->set_label(clang::__('Daftar'))
                ->add_class('btn-auth btn-62hallfamily bg-red pull-right')
                ->custom_css('border-radius', '5px !important')
                ->custom_css('width', '100%')
                ->set_submit(false);



        // PRODUCT GOLD
        $gold = $app->add_div()->add_class('bg-white');
        $gold_container = $gold->add_div()->add_class('container margin-30');
        $product_gold = $gold_container->add_div('page-product-gold')->add_class('col-md-8');
        $sidebar = $gold_container->add_div()->add_class('col-md-4');

        $product_gold->add_listener('ready')
                ->add_handler('reload')
                ->set_target('page-product-gold')
                ->set_url(curl::base() . "gold/get_page_product_gold/1");

        $list_price = gold_price::get_list_gold_price();

        $element_listprice = $sidebar->add_element('62hall-sidebar-listprice');
        $element_listprice->set_header('HARGA EMAS TERKINI')
                ->set_list_price($list_price);

        $quict_contact = quick_contact::get_quick_contact($org_id);

        $element_listprice = $sidebar->add_element('62hall-sidebar-contact');
        $element_listprice->set_header('Quict Contact')
                ->set_address($quict_contact['address'])
                ->set_phone($quict_contact['phone'])
                ->set_bbm($quict_contact['pin_bb'])
                ->set_wa($quict_contact['whatsapp'])
                ->set_email($quict_contact['email']);

        echo $app->render();
    }

    //<editor-fold defaultstate="collapsed" desc="Pagination">
    public function get_count_page($total) {
        $limit = $this->_count_item;

        $count_page = (int) ($total / $limit);

        if ($total % $limit != 0) {
            $count_page += 1;
        }

        return $count_page;
    }

    public function get_start_limit($page) {
        $start = 0;
        if ($page > 1) {
            $start = ($page - 1) * $this->_count_item;
        }

        return $start;
    }

    //</editor-fold>

    public function get_page_product_gold($page) {
        $app = CApp::instance();
        $org_id = ccfg::get('org_id');
        $db = CDatabase::instance();

        $start = $this->get_start_limit($page);

        $arr_gold = array();
        $q = "
			select
				*
			from
				product_type
			where
				name='gold'
		";
        $r = $db->query($q);
        if ($r->count() > 0) {
            $q_base = "
				select
					*
				from
					product 
				where
					status>0
					and product_type_id=" . $db->escape($r[0]->product_type_id) . "
				order by
                                    weight asc
                                limit
                                    " . $start . "," . $this->_count_item . "
			";
            $r = $db->query($q_base);
            if ($r->count() > 0) {
                foreach ($r as $row) {
                    $arr_gold[] = array(
                        "id" => $row->product_id,
                        "name" => $row->name,
                        "buy" => 'B : Rp. ' . ctransform::format_currency($row->purchase_price),
                        "sell" => 'J : Rp. ' . ctransform::format_currency($row->sell_price),
                        "image" => $row->file_path,
                    );
                }
            }
        }
//                
//            $product_list = array(
//                array(
//                    'id' => '1',
//                    'name' => 'Logam Mulia 1 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                    
//                    'image' => 'gold.jpg'
//                ),
//                array(
//                    'id' => '2',
//                    'name' => 'Logam Mulia 2 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                     
//                    'image' => 'gold.jpg'
//                ),
//                array(
//                    'id' => '3',
//                    'name' => 'Logam Mulia 2.5 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                    
//                    'image' => 'gold.jpg'
//                ),
//                array(
//                    'id' => '4',
//                    'name' => 'Logam Mulia 3 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                  
//                    'image' => 'gold.jpg'
//                ),
//                array(
//                    'id' => '5',
//                    'name' => 'Logam Mulia 4 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                       
//                    'image' => 'gold.jpg'
//                ),
//                array(
//                    'id' => '6',
//                    'name' => 'Logam Mulia 5 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                       
//                    'image' => 'gold.jpg'
//                ),
//                array(
//                    'id' => '7',
//                    'name' => 'Logam Mulia 10 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                      
//                    'image' => 'gold.jpg'
//                ),
//                array(
//                    'id' => '8',
//                    'name' => 'Logam Mulia 25 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                     
//                    'image' => 'gold.jpg'
//                ),
//                 array(
//                    'id' => '9',
//                    'name' => 'Logam Mulia 50 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                   
//                    'image' => 'gold.jpg'
//                ),
//                array(
//                    'id' => '10',
//                    'name' => 'Logam Mulia 100 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                    
//                    'image' => 'gold.jpg'
//                ),
//                array(
//                    'id' => '11',
//                    'name' => 'Logam Mulia 250 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                    
//                    'image' => 'gold.jpg'
//                ),
//                array(
//                    'id' => '12',
//                    'name' => 'Logam Mulia 500 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                  
//                    'image' => 'gold.jpg'
//                ),
//                array(
//                    'id' => '13',
//                    'name' => 'Logam Mulia 1 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                    
//                    'image' => 'gold.jpg'
//                ),
//                array(
//                    'id' => '14',
//                    'name' => 'Logam Mulia 2 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                     
//                    'image' => 'gold.jpg'
//                ),
//                array(
//                    'id' => '15',
//                    'name' => 'Logam Mulia 2.5 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                    
//                    'image' => 'gold.jpg'
//                ),
//                array(
//                    'id' => '16',
//                    'name' => 'Logam Mulia 3 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                  
//                    'image' => 'gold.jpg'
//                ),
//                array(
//                    'id' => '17',
//                    'name' => 'Logam Mulia 4 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                       
//                    'image' => 'gold.jpg'
//                ),
//                array(
//                    'id' => '18',
//                    'name' => 'Logam Mulia 5 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                       
//                    'image' => 'gold.jpg'
//                ),
//                array(
//                    'id' => '19',
//                    'name' => 'Logam Mulia 10 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                      
//                    'image' => 'gold.jpg'
//                ),
//                array(
//                    'id' => '20',
//                    'name' => 'Logam Mulia 25 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                     
//                    'image' => 'gold.jpg'
//                ),
//                 array(
//                    'id' => '21',
//                    'name' => 'Logam Mulia 50 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                   
//                    'image' => 'gold.jpg'
//                ),
//                array(
//                    'id' => '22',
//                    'name' => 'Logam Mulia 100 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                    
//                    'image' => 'gold.jpg'
//                ),
//                array(
//                    'id' => '23',
//                    'name' => 'Logam Mulia 250 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                    
//                    'image' => 'gold.jpg'
//                ),
//                array(
//                    'id' => '24',
//                    'name' => 'Logam Mulia 500 gr',
//                    'sell' => 'Rp. 120.654',                    
//                    'buy' => 'Rp. 120.654',                  
//                    'image' => 'gold.jpg'
//                ),
//            );

        $total_item = count($arr_gold);

        foreach ($arr_gold as $data_product) {
            $item_product = $app->add_div()->add_class('col-md-3 padding');

            $element_product = $item_product->add_element('62hall-gold-product', 'product-gold-' . $data_product['id']);
            $element_product->set_image($data_product['image'])
                    ->set_name($data_product['name'])
                    ->set_price_sell($data_product['sell'])
                    ->set_price_buy($data_product['buy']);
        }

        // pagination
        $paginationcontainer = $app->add_div()
                ->add_class('pagination col-md-12');
        $paginationcontainer->add('<center>');

        $count_page = $this->get_count_page($total_item);

        if ($count_page > 0) {
            for ($i = 1; $i <= $count_page; $i++) {
                $class = NULL;
                if ($page == $i) {
                    $class = 'btn-pagination bg-red font-white';
                }
                $paginationcontainer->add_action()
                        ->set_label($i)
                        ->add_class($class)
                        ->add_listener('click')
                        ->add_handler('reload')
                        ->set_target('page-product-gold')
                        ->set_url(curl::base() . "gold/get_page_product_gold/" . $i);
            }
        }

        echo $app->render();
    }

}
