<?php

/**
 *
 * @author Riza 
 * @since  Nov 12, 2015
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class Home_Controller extends PulsaController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();
        $user = $app->user();
        $org_id = $this->org_id();
        
        // menu
        $link_list = $this->list_menu();
        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_link($link_list);
        
        // SLIDE
        $slide_list = slide::get_slide($org_id, $this->page());
        $slide_list = slide::get_slide($org_id, 'service');
        $header = $app->add_div()->add_class("dd")->add("<div ><img src='/application/62hallfamily/default/media/img/pulsa/slide.jpg' width='100%'/></div>");
        $container = $app->add_div()->add_class("container")->add_div()->add_class("row");
        $row = $container->add_div()->add_class("row");
        $cl = $row->add_div()->add_class("col-md-12")->custom_css("text-align", "center");
        $box = $cl->add_div()->add_class("box-in");
        $box_pulsa = $box->add_div()->add_class("box-pulsa");
        
        $tabs = $box_pulsa->add_tab_list()->set_tab_position('top');
            $tab_pulsa = $tabs->add_tab('pulsa')->set_label('<span class="icon-pulsa"></span><div>PULSA</div>')->set_ajax_url(curl::base().'pulsa/home/tab_pulsa');
            if (ccfg::get('disable_game_voucher') != '1') {
                $tab_game = $tabs->add_tab('game')->set_label('<span class="icon-game"></span><div>GAME</div>')->set_ajax_url(curl::base().'pulsa/home/tab_game');
            }

        //FOOTER IMG
        
        $bg =$app->add_div()->add_class("bg-white");
        $cont = $bg->add_div()->add_class("container margin-30")->custom_css("text-align", "center");
        $cont->add_div()->add_class("title-logo")->add("Beragam Pilihan Operator Pulsa");
        $cont->add("<img src='/application/62hallfamily/default/media/img/pulsa/sprite-provider.png' width='100%'/>");
  
        echo $app->render();
    }
    
    public function tab_pulsa(){
        $app = CApp::instance();
        $db = CDatabase::instance();
        
        $k = $app->add_div()->add_class("input-box");
        $msg = cmsg::flash_all();
                if (strlen($msg) > 0) {
                     $k->add($msg);
                     $k->add_js("
                             $(document).ready(function() {
                                    $('.alert').delay(10000).slideUp();
                              });
                             ");
                }
        $k->add_control("number_input_pulsa", "text")->set_placeholder("No. Handphone")->add_class("pulsa-input-text number-input"); 
        $k->add_control("operator_name", "hidden");
        $k->add_div()->add_class("provider-logo")->custom_css("display", "inline")
          ->add_div()->add_class("img-logo")->add("<span id='logo-provider'></span>");
        $app->add_div("price_div")->add_class("input-box"); 
        $app->add_div("result_div"); 
        
        
        $a = '$(".number-input").on("keyup change",function(){
            this.value = this.value.replace(/[^0-9\.]/g,"");
            var input_number = $("#number_input_pulsa").val();
            var number_code = input_number.slice(0,4);';
        $pulsa = $db->query("select * from provider where provider_type='pulsa' and status > 0 ");
        if (count($pulsa) > 0){
            foreach ($pulsa as $key => $val) {
                $name = cobj::get($val, "var_name");
                $low_name =  strtolower($name);
                $prefix = cobj::get($val, "prefix");
                 $a .= 'var '.$low_name.'_code = new Array('.$prefix.');
                 for (var i = 0; i < '.$low_name.'_code.length; i++) {

                        if ('.$low_name.'_code.indexOf( number_code )>=0) {
                            $("#logo-provider").addClass("prov-'.$low_name.'");
                            $("#operator_name").val("'.$name.'");

                        }
                        else{
                            $("#logo-provider").removeClass("prov-'.$low_name.'");
                        }
                    }';
            }
        }
     
        $a .= 'var check_prov = document.getElementById("logo-provider").className;
   //            console.log(check_prov);
               if (check_prov!=""){
                   get_data();
               }
               else{
                   $("#price_div").html("");
                   $("#result_div").html("");
               }
           });';
        $a .='function get_data(){
               var check_prov = document.getElementById("logo-provider").className;
               if (check_prov!=""){
                  var price_selected = $("#code").val();
               }

                $.ajax({
                   url:"/pulsa/home/reload_price/price_list",
                   data:{
                       operator_name:$("#operator_name").val(),
                       price_select : price_selected,
                   },
                   type:"GET",
                   dataType:"json",
                           success: function(data){
                                       $("#price_div").html(data.html);
                                       var script = $.cresenity.base64.decode(data.js);
                                       eval(script);  
                                   },
                           error:function(data){

                                    },
               });
           }';
        $app->add_js($a);
        echo $app->render();
    }
    
    public function tab_game() {
        $app = CApp::instance();
        
        $app->add_div()->add_class("input-box")->add_control("number_input_game", "text")->set_placeholder("No. Handphone")->add_class("pulsa-input-text number-input"); 
        
        $control_game_provider=$app->add_div("game_price_div")->add_class("input-box")->add_control("game-provider", "provider-select")->set_type('game_online')->add_class("select-pulsa");
        $control_game_provider->add_listener('change')
                ->add_handler('reload')
                ->set_target('div_game_nominal')
                ->add_param_input(array('game-provider'))
                ->set_url(curl::base().'pulsa/home/reload_game_product');
        $control_game_provider->add_listener('ready')
                ->add_handler('reload')
                ->set_target('div_game_nominal')
                ->add_param_input(array('game-provider'))
                ->set_url(curl::base().'pulsa/home/reload_game_product');
        $app->add_div("div_game_nominal")->add_class("input-box");
        $app->add_div("div_game_result"); 
        
        echo $app->render();
    }

    public function reload_game_product(){
        $app=CApp::instance();
        $db=CDatabase::instance();
        $get=$_GET;
        $provider_id=carr::get($get,'game-provider');
        $control_game_product=$app->add_control("game_product_id", "product-select")->set_placeholder("Product")->add_class("select-pulsa")->set_provider_id($provider_id); 
        $control_game_product->add_listener('change')
                        ->add_handler('reload')
                        ->set_target('div_game_result')
                        ->set_url('/pulsa/home/reload_price/total?type=game_online&provider_id='.$provider_id)
                        ->add_param_input(array('game_product_id'));
        $control_game_product->add_listener('ready')
                        ->add_handler('reload')
                        ->set_target('div_game_result')
                        ->set_url('/pulsa/home/reload_price/total?type=game_online&provider_id='.$provider_id)
                        ->add_param_input(array('game_product_id'));
        
        echo $app->render();
    }
    
    public function reload_price($type=null) {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();
        $get = $_GET;
        
        $err_code = 0;
        $err_message = '';

        $operator_name = carr::get($get, "operator_name");
        $product_category_code = 'pulsa';
        $product_code = 'RB';
        if (ccfg::get('pulsa_product_code') != NULL) {
            $product_code = ccfg::get('pulsa_product_code');
        }
        
        $method = 'get_price';
        
        if ($type == "price_list"){
             $price_select = carr::get($get, "price_select");
             if ($price_select != null){
                 return false;
             }
             $provider_id  = cdbutils::get_value("select provider_id from provider where var_name ='".$operator_name."'");
            
             $provider = $db->query("select * from product where status>0 and provider_id =".$provider_id);
             if ($product_code == 'KY') {
                 $provider = $db->query("SELECT sku_ky AS sku, name,sell_price,ho_upselling FROM product WHERE status > 0 AND provider_id =".$db->escape($provider_id)." AND sku_ky IS NOT NULL");
             }
             
             $data_arr = array();
             $none = array("none"=>"Select nominal");
             
             foreach ($provider as $k_prov => $v_prov){
                 $code = cobj::get($v_prov,"sku");
                 $name = cobj::get($v_prov,"name");
                 $sell_price = cobj::get($v_prov,"sell_price");
                 $ho_upselling = cobj::get($v_prov,"ho_upselling");
//                 if(($sell_price+$ho_upselling)>10000){
                    $data_arr[$code] =  $name;                 
//                 }
             }
            $data_arr = array_merge($none,$data_arr);
             
            if (strlen($operator_name) > 0){
                $a = $app->add_control("code", "select")->add_class("select-pulsa")->set_list($data_arr)->set_value("none");
                $a->add_listener('change')
                        ->add_handler('reload')
                        ->set_target('result_div')
                        ->set_url('/pulsa/home/reload_price/total')
                        ->add_param_input(array('code'));
//                $a->add_listener('ready')
//                        ->add_handler('reload')
//                        ->set_target('total_div')
//                        ->set_url('/pulsa/home/reload_price/total')
//                        ->add_param_input(array('code'));
            }
            
        }
        elseif($type == "total"){
            $code = carr::get($get, "code");
            $type_tab = carr::get($get, "type");
            $provider_id=carr::get($get,"provider_id");
            $price_pulsa=null;
            if($type_tab=='game_online'){
                $product_id=carr::get($get,"game_product_id");
                $product=cdbutils::get_row("select * from product where product_id=".$product_id);
                $code=cobj::get($product,'sku');
            }
            if ($code != "none"){
                if($provider_id ==null){
                    $provider_id = cdbutils::get_value("select provider_id from product where sku ='".$code."' limit 1");
                    if ($product_code == 'KY') {
                        $provider_id = cdbutils::get_value("select provider_id from product where sku_ky =".$db->escape($code)." limit 1");
                    }
                }
                $provider = cdbutils::get_row("select * from provider where provider_id =".$provider_id);

                $method = 'get_price';
                $request['operator_name'] = cobj::get($provider,"var_name");
                $request['product_category_code'] = cobj::get($provider,"provider_type");
                
                $response = pulsa_api::api($method, $product_code, $request);
                $api_err_code = carr::get($response, 'err_code');
                
                if ($api_err_code > 0) {
                    $err_code = $api_err_code;
                    $err_message = 'Failed to connect server, please try again later ['.$api_err_code.']';
                    $err_message = carr::get($response, 'err_message');
                }
                
                if ($err_code == 0) {
                    $data = carr::get($response, "data");
                    $data = carr::get($data, "products");

                    foreach ($data as $key => $value) {
                        $code_pulsa = carr::get($value, "code");
                        $name = carr::get($value, "name");
                        $nominal_list[$code] = $name;
                        if ($code_pulsa == $code){
                            $price_pulsa = carr::get($value, "price");
                        }
                    }   
                    $product_id = cdbutils::get_value("select product_id from product where sku='".$code."'"); 
                    if ($product_code == 'KY') {
                        $product_id = cdbutils::get_value("select product_id from product where sku_ky='".$code."'"); 
                    }
                    
                    if ($price_pulsa == NULL) {
                        $err_code++;
                        $err_message = clang::__("Product").' '.$code.' '.clang::__("Tidak di temukan mohon pilih product yang lain");
                    }
                    else {
                        $hid_type_tab = 'pulsa';
                        if($type_tab == 'game_online'){
                            $hid_type_tab = 'game_online';
                        }
                                                
                        $app->add_control('product_id','hidden')->set_value($product_id);
                        $app->add_control('type_tab','hidden')->set_value($hid_type_tab);
                        $product = product::get_product($product_id);

                        $sell_price_pulsa = $price_pulsa + $product['detail_price']['ho_upselling'];
                        $app->add_control("price_pulsa", "hidden")->set_value($sell_price_pulsa);
                         $total = $app->add_div("total_div")->add_class("input-box")->custom_css("text-align", "left");
                         $total->add("<span style='margin-left: 6px;'>Total Pembayaran : Rp. ".  ctransform::format_currency($sell_price_pulsa)." </span>");
                         $act_payment = $app->add_action()
                                            ->add_class("btn-paynow") 
                                            ->set_label(clang::__('BELI SEKARANG'));
                         //$act_payment->set_link(curl::base() . 'pulsa/home/set_session_pulsa')->set_submit(false);

                         $email = $session->get('email');
        //                         if (!empty($email)) {
        //                              $app->add("<input type='submit' value='BELI SEKARANG' class='btn-paynow'>");
        //                         } else {
        //                             $act_payment->add_listener('click')
        //                                     ->add_handler('dialog')
        //                                     ->set_title('LOGIN')
        //                                     ->set_url(curl::base() . "pulsa/home/form_login_pulsa/" . $product_id)
        //                                 ;
        //                         }
                    }
                }
                if ($err_code > 0) {
                    $app->add($err_message);
                }
            }
        }
        $app->add_js('
                    $(".btn-paynow").click(function(){
                        var err_code=0;
                        var err_message="";
                        var type_tab=$("#type_tab").val();
                        var game_number=$("#number_input_game").val();
                        var pulsa_number = $("#number_input_pulsa").val();
                        var price_pulsa = $("#price_pulsa").val();
                        var product_id = $("#product_id").val();
                        var phone_number=pulsa_number;
                        if(type_tab=="game_online"){
                            phone_number=game_number;
                        }
                        if(phone_number.length==0){
                            err_code++;
                            err_message="No Handphone Tidak Boleh Kosong";
                        }
                        
                        if(err_code==0){
                            $.ajax({
                                url:"/pulsa/home/set_session_pulsa",
                                data:{
                                    phone_number : phone_number,
                                    price_pulsa : price_pulsa,
                                    pulsa_product_id : product_id,
                                },
                                type:"GET",
                                success: function(data){
                                    data=JSON.parse(data);
                                    if(data.err_code=="0"){
                                        window.location.href="'.curl::base().'pulsa/updatecontact/update_contact";
                                    }else{
                                        alert(data.err_message);
                                    }
                                },
                                error:function(data){
                                },
                            })
                        }else{
                            alert(err_message);
                        }
                    });
                   ');
        echo $app->render();
        
    }
    public function form_login_pulsa($product_id=null) {
        $app = CApp::instance();
        $session = session::instance();
        $phone_number = $session->get("phone-number");
        $request = array_merge($_POST, $_GET);
       
        $login_element = CElement_Login::factory()
                ->set_session(false)
                ->set_trigger_button(FALSE)
                ->set_is_login(FALSE)
                ->set_icon(FALSE)
                ->set_guest(TRUE)
                ->set_action_next(TRUE)
                ->set_reload(TRUE)
                ->set_action_url(curl::base() . 'auth/next_login')
                ->set_redirect(curl::base() . 'pulsa/updatecontact/update_contact/'. $product_id);
        if ($this->have_register() > 0) {
            $login_element->set_register_button(TRUE);
        }
        $app->add($login_element);
        echo $app->render();
    }
    public function set_session_pulsa(){
        $db=CDatabase::instance();
        $err_code=0;
        $err_message='';
        $param = $_GET+$_POST;
        $product_id = carr::get($param, "pulsa_product_id");
        $price_pulsa = carr::get($param, "price_pulsa");
        $phone_number = carr::get($param, "phone_number");
        $date_now=date('Y-m-d');
        $product_code_pulsa='RB';
        if(ccfg::get('pulsa_product_code')!=null){
            $product_code_pulsa=ccfg::get('pulsa_product_code');
        }
//        cdbg::var_dump($param);
//        die;
        //check transaction today for phone number and product
        $q="
            select
                p.name as product
            from
                transaction as t 
                inner join transaction_detail as td on td.transaction_id=t.transaction_id
                inner join product as p on p.product_id=td.product_id
                inner join product_type as pt on pt.product_type_id=p.product_type_id
            where
                t.status>0
                and td.status>0
                and t.order_status in('SUCCESS','PENDING','CONFIRMED')
                and pt.name='pulsa'
                and td.product_id=".$db->escape($product_id)."
                and td.pulsa_phone_number=".$db->escape($phone_number)."
                and date(t.date)=".$db->escape($date_now)."    
        ";
        $exist=cdbutils::get_row($q);
        $result=array();
        $session_php = Session::instance();
        if($exist==null or $product_code_pulsa!='RB'){
            $session_php->set('phone-number', $phone_number);
            $session_php->set('price_pulsa', $price_pulsa);
            $session_php->set('pulsa_product_id', $product_id);
        }else{
            $err_code++;
            $err_message=clang::__("Nomor handphone").' '.$phone_number.' '.clang::__("sudah melakukan pembelian").' '.cobj::get($exist,'product').' '.clang::__("hari ini");
        }
        $result['err_code']=$err_code;
        $result['err_message']=$err_message;

        echo json_encode($result);
    }


}
