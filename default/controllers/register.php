<?php

class Register_Controller extends SixtyTwoHallFamilyController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $app = CApp::instance();

        $container = $app->add_div()->add_class('row');
        $login_container = $container->add_div()->add_class('col-md-6');
        $reg_container = $container->add_div()->add_class('col-md-6');
        $reg_container->add_element('b2c-register')->set_register_button(false);

        echo $app->render();
    }

}
