<?php

    class Shoppingcart_Controller extends ProductController {

        private $page = "product";

        public function __construct() {
            parent::__construct();
        }

        public function index() {
            $this->shopping_cart();
        }

        public function shopping_cart() {
            $app = CApp::instance();
            $user = $app->user();
            $db = CDatabase::instance();
            $session = Session::instance();
            $email = $session->get('email');

            $cart = Cart62hall_Product::factory();
            $session = Session::instance();

            $org_id = $this->org_id();
            $menu_list = $this->menu_list();
            $link_list = $this->list_menu();

            // SHOPPING CART 
            $app->add_listener('ready')
                    ->add_handler('reload')
                    ->set_target('shopping-cart')
                    ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

            // Menu
            //$product_category = product_category::get_product_category_menu($this->page());

            // Menu
//            $menu_list = array(
//                'name' => 'KATEGORI',
//                'menu' => $product_category
//            );
//
//            $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
//            $element_menu->set_menu($menu_list);
//            $element_menu->set_link($link_list);

            $container = $app->add_div()->add_class('container shopcart');
            $title = $container->add_div()->add_class('txt-shopping-cart-title')->add(clang::__('Shopping Cart'));

            $container_table = $container->add_div()->add_class('container-shopping-cart-table');
            // start container
            $container_table_row = $container_table->add_div()->add_class('container-table-wrap');
            $container_table_wrap = $container_table_row->add_div()->add_class('row');
            $container_left = $container_table_wrap->add_div()->add_class('col-xs-6 col-sm-4 col-md-4 title-left');
            $container_right = $container_table_wrap->add_div()->add_class('col-xs-6 col-sm-8 col-md-8 title-right');
            $content_left = $container_left->add_div()->add_class('content-left');
            $content_right = $container_right->add_div()->add_class('content-right');

            $content_left->add_div()->add_class('txt-table-title product-bd-right')->add(clang::__('Product'));
            $right_row = $content_right->add_div()->add_class('row');
            $right_row->add_div()->add_class('col-md-4 col-sm-3 txt-table-title price-bd-right')->add(clang::__('Price'));

            $container_qty_sub = $right_row->add_div()->add_class('col-md-7 col-sm-7');
            $container_qty_sub_row = $container_qty_sub->add_div()->add_class('row');
            $container_qty_sub_row->add_div()->add_class('col-md-6 col-sm-6 hidden-xs txt-table-title qty-bd-right')->add(clang::__('Quantity'));
            $container_qty_sub_row->add_div()->add_class('col-md-6 col-sm-6 hidden-xs txt-table-title sub-bd-right')->add(clang::__('Subtotal'));

            $right_row->add_div()->add_class('col-md-1 col-sm-2 hidden-xs txt-table-title')->add(' ');
            // end container

            $cart_list = $cart->get_cart();
            $total_barang = 0;
            $total_harga = 0;
            $stock = '';

            foreach ( $cart_list as $index => $data_cart ) {
                $total_barang += $data_cart['qty'];
                $total_harga += $data_cart['sub_total'];
                $html_attribute = '';
                $html_attribute = '<table class="table-product-attribute">';
                if ( !empty($data_cart['attribute']) and is_array($data_cart['attribute']) ) {
                    foreach ( $data_cart['attribute'] as $cat_id => $value ) {
                        $att_cat_name = cdbutils::get_value('select name from attribute_category where attribute_category_id=' . $db->escape($cat_id));
                        $attribute_key = attribute::get_attribute_key($value);
                        $html_attribute .= '<tr>';
                        $html_attribute .= '    <td>' . $att_cat_name . '</td>';
                        $html_attribute .= '    <td>&nbsp;:&nbsp;</td>';
                        $html_attribute .= '    <td>' . $attribute_key . '</td>';
                        $html_attribute .= '</tr>';
                    }
                }
                $html_attribute .= '</table>';

                $product_id = carr::get($data_cart, 'product_id');
                $product_simple = product::get_simple_product_from_attribute($product_id, carr::get($data_cart, 'attribute'));
                $product_id = $product_simple->product_id;
                $stock = $product_simple->stock;

//                $image = $this->get_image($data_cart['image']);
                $image = $data_cart['image'];

                $container_table_row = $container_table->add_div()->add_class('container-table-content-wrap');
                $container_table_wrap_top = $container_table_row->add_div()->add_class('row');

                $container_table_col = $container_table_wrap_top->add_div()->add_class('col-md-12');
                $container_table_wrap_border = $container_table_col->add_div()->add_class('container-row-border');
                $container_table_wrap = $container_table_wrap_border->add_div()->add_class('row');

                $container_left_wrap = $container_table_wrap->add_div()->add_class('col-xs-6 col-sm-4 col-md-4 col-content-left');
                $container_right = $container_table_wrap->add_div()->add_class('col-xs-6 col-sm-8 col-md-8 col-content-right');
                $container_left_row = $container_left_wrap->add_div()->add_class('row');
                $container_left = $container_left_row->add_div()->add_class('col-md-12');
                $content_left = $container_left->add_div()->add_class('content-left-column');
                $content_right = $container_right->add_div()->add_class('row');

                $content_product = $content_left->add_div()->add_class('col-cart-table col-cart-product');
                $right_row = $content_right->add_div()->add_class('content-right');
                $content_price = $right_row->add_div()->add_class('col-md-4 col-sm-3 col-cart-table col-content-price');

                $container_qty_sub = $right_row->add_div()->add_class('col-md-7 col-sm-7');
                $container_qty_sub_row = $container_qty_sub->add_div()->add_class('row');
                $content_quantity = $container_qty_sub_row->add_div()->add_class('col-md-6 col-sm-6 col-cart-table col-qty col-content-qty');
                $content_subtotal = $container_qty_sub_row->add_div()->add_class('col-md-6 col-sm-6 col-cart-table resp col-content-subtotal');

                $content_delete = $right_row->add_div()->add_class('col-md-1 col-sm-2 col-cart-table col-content-action');

                $container_product_cart = $content_product->add_div()->add_class('container-product-cart');
                $product_cart_image = $container_product_cart->add_div()->add_class('outside-element-center-vertical product-cart-image');
                $product_cart_table = $container_product_cart->add_div()->add_class('product-cart-table');

                $product_cart_image->add("<img class='element-center-vertical' src='" . $image . "'>");
                $product_cart_table->add('<input type="hidden" class="product_id" value="' . $data_cart['product_id'] . '"/>');
                $product_cart_table->add_div()->add_class('txt-product-cart-title')->add($data_cart['name']);
                $product_cart_table->add($html_attribute);

                $col_price = $content_price->add_div()->add_class('content-cart-price');
                $col_price->add('Rp. <span class="sell_price" >' . ctransform::format_currency($data_cart['sell_price']) . '</span><br>');
                if ( !empty($data_cart['price']) ) {
                    $col_price->add('<strike>Rp. <span class="promo_price normal font-size16" >' . ctransform::format_currency($data_cart['price']) . '</span></strike><br>');
                }

                $content_quantity->add_div()->add_class('container-quantity')->add('<input type="text" id="product_id_' . $data_cart['product_id'] . '" name="" class="qty-touchpin" value="' . $data_cart['qty'] . '">');

                $content_subtotal->add_div()->add_class('container-subtotal')->add('Rp. <span class="subtotal" >' . ctransform::format_currency($data_cart['sub_total']) . '</span>');

                $content_delete->add_div()->add_class('container-delete-cart')->add_action()
                        ->set_label('<div class="icon-trash-red"></div>')
                        ->set_link(curl::base() . "products/shoppingcart/delete_item_cart/" . $index);
               
            }
            

            $footer_container_wrap = $container->add_div()->add_class('col-md-12');
            $footer_container = $footer_container_wrap->add_div()->add_class('row shoppingcart-footer');
            $footer_button_wrap = $footer_container->add_div()->add_class('col-md-12');
            $footer_button = $footer_button_wrap->add_div()->add_class('container-footer-button');
            $footer_button_row = $footer_button->add_div()->add_class('row');
            $footer_left = $footer_button_row->add_div()->add_class('col-sm-6  col-xs-12');
            $footer_right = $footer_button_row->add_div()->add_class('col-sm-6 col-xs-12 text-right');

            $act_home = $footer_left->add_action()
                    ->set_label("<i class='fa fa-arrow-left'></i> " . clang::__('Lanjutkan Belanja'))
                    ->add_class('btn-imbuilding-light btn-primary btn-shoppingcart-footer btn-shoppingcart-lanjutkan')
                    ->set_link(curl::base() . "home");

            $act_home = $footer_right->add_action()
                    ->set_label("<i class='fa fa-trash-o'></i> " . clang::__('Clear Cart'))
                    ->add_class('btn-imbuilding-light btn-primary btn-clear-cart  btn-shoppingcart-footer')
                    ->set_link(curl::base() . "products/shoppingcart/clear_item/");

            $total_title = $footer_container->add_div()->add_class('col-sm-4 col-xs-12');
            $total_title->add('<div class="shoppingcart-text-total">Total Belanja Anda</div>');
            $total_title->add('<div class="font-gray-dark shoppingcart-text-total-info"><red>*</red> Diluar Biaya Pengiriman</div>');

            $total_price = $footer_container->add_div()->add_class('col-sm-4 col-xs-12 padding-top-10 text-center');
            $total_price->add('<div class="font-size24 txt-shopping-cart-price bold text-right">Rp. <span id="totalharga">' . ctransform::format_currency($total_harga) . '</span></div>');

            $action = $footer_container->add_div()->add_class('col-sm-4 col-xs-12 padding-top-10 text-right');

            $act_payment = $action->add_action()
                    ->set_label(clang::__('PROSES PEMBAYARAN'))
                    ->add_class('btn-imlasvegas btn-shoppingcart-pembayaran btn-shoppingcart-footer btn-modal');

            // cek session
            if ( count($cart_list) == 0 ) {
                $act_payment->add_listener('click')
                        ->add_handler('dialog')
                        ->set_title('<center>Keranjang Belanja</center>')
                        ->set_url(curl::base() . "products/shoppingcart/dialog_empty_cart");
            }
            else if ( !empty($email) ) {
                $act_payment->set_link(curl::base() . "products/updatecontact");
            }
            else {
                $act_payment->add_listener('click')
                        ->add_handler('dialog')
                        ->set_title('LOGIN')
                        ->set_url(curl::base() . "auth/form_login");
            }
            //titip js
            //$('#product_id_" . $data_cart['product_id'] . "').TouchSpin({
                        //    min: 1,
                        //    max: " . $stock . ",
                        //    verticalbuttons: true
                        //}).change(function(){
                        //    $('#product_id').attr('value',$(this).val());
                        //});
            


            $app->add_js("
                        $('.qty-touchpin').each(function() {
                            $(this).TouchSpin({
                                min: 1,
                                max: ".$stock.",
                                // max: $(this).attr('data-max')
                                //verticalbuttons: true,
                            })
                        });
                        
                        $('.qty-touchpin').change(function () {
                            var product_id = $(this).parents('.container-table-content-wrap').find('.product_id').val();
                            var qty = $(this).val();
                            var o_subtotal = $(this).parents('.container-table-content-wrap').find('.subtotal');
                            var sell_price = $.cresenity.unformat_currency($(this).parents('.container-table-content-wrap').find('.sell_price').html());
                            var promo_price = $.cresenity.unformat_currency($(this).parents('.container-table-content-wrap').find('.promo_price').html());
                            var subtotal = $.cresenity.unformat_currency(qty)*sell_price;
                            o_subtotal.html($.cresenity.format_currency(subtotal));
                            var ctrl_id = $(this).attr('id');
                            var xhr = $(this).data('control_xhr');
                            if(xhr) xhr.abort();
                            $(this).data('control_xhr', $.ajax({
                                    method: 'POST',
                                    url: '" . curl::base() . 'products/shoppingcart/change_qty' . "',
                                    data: {
                                        product_id: product_id, 
                                        qty: qty,
                                        control_id: ctrl_id,
                                        sell_price: sell_price,
                                        
                                    },
                                    dataType: 'json',
                                    success: function (data) {
                                
                                        var err_code = data.err_code;
                                        var err_message = data.err_message;
                                        if(err_code>0) {
                                            var random = Math.floor((Math.random() * 1000) + 1);
                                            $.app.show_dialog('62dialog'+random,'<center>Maaf</center><br /><center><div class=\"font-size14 font-black normal\">'+err_message+'</div></center>');
                                            $('#'+data.control_id).val(data.qty_before);
                                            var subtotal_i = $.cresenity.unformat_currency(data.qty_before)*sell_price;
                                            o_subtotal.html($.cresenity.format_currency(subtotal_i));
                                        } else {
                                            $('#totalharga').html($.cresenity.format_currency(data.total_price)+',00');
                                            $.cresenity.reload('shopping-cart', '" . curl::base() . 'products/shoppingcart/icon_shopping_cart' . "');
                                        }
                                    }
                                })
                            );
                            
                            
                        });
                        
                    ");

            echo $app->render();
        }

        public function change_qty() {
            $cart = Cart62hall_Product::factory();
            $app = CApp::instance();
            $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");
            $err_code = 0;
            $err_message = '';
            $request = array_merge($_POST, $_GET);
       
            $product_id = carr::get($request, 'product_id');
            $qty = carr::get($request, 'qty');
            $control_id = carr::get($request, 'control_id');

            if ( $product_id == null ) {
                $err_code++;
                $err_message = "Product ID Required";
            }
            $exists_item_qty = 0;
            if ( $err_code == 0 ) {
                if ( strlen($qty) == 0 ) {
                    $qty = 0;
                }

                $cart_list = $cart->get_cart();
                $cart_product = carr::get($cart_list, $product_id);
                $sell_price = carr::get($cart_product, 'sell_price');

                $sub_total = $qty * $sell_price;


                $product_simple = product::get_simple_product_from_attribute($product_id, carr::get($cart_product, 'attribute'));


                $product_id = cobj::get($product_simple, 'product_id');
                $stock = cobj::get($product_simple, 'stock');





                if ( $cart_product != null ) {
                    $exists_item_qty = carr::get($cart_product, 'qty');
                    if ( strlen($exists_item_qty) == 0 ) $exists_item_qty = 0;
                }

                if ( $qty > $stock ) {
                    $err_code++;
                    $err_message = "Stock tidak mencukupi";
                }
                if ( $err_code == 0 ) {
                    $data_item = array(
                        'product_id' => $product_id,
                        'name' => carr::get($cart_product, 'name'),
                        'image' => carr::get($cart_product, 'image'),
                        'qty' => $qty,
                        'sell_price' => $sell_price,
                        'price' => carr::get($cart_product, 'price'),
                        'weight' => carr::get($cart_product, 'weight'),
                        'sub_total' => $sub_total,
                        'attribute' => carr::get($cart_product, 'attribute'),
                    );

                    $cart->update_item($product_id, $data_item);
                }
            }

            $total_harga = 0;
//            if ( $err_code == 0 ) {
//                $cart_list = $cart->get_cart();
//
//                foreach ( $cart_list as $data_cart ) {
//                    $total_harga += $data_cart['sub_total'];
//                }  
//            }
//            
            if ( $err_code == 0 ) {
                $cart_list = $cart->get_cart();

                foreach ( $cart_list as $data_cart ) {
                    $total_harga += $data_cart['sub_total'];
                }  
            }
            else{
              $cart_list = $cart->get_cart();

                foreach ( $cart_list as $data_cart ) {
                    $total_harga += $data_cart['sub_total'];
                }    
                
            }
            
            $arr = array(
                "err_code" => $err_code,
                "err_message" => $err_message,
                "total_price" => $total_harga,
                "qty_before" => $exists_item_qty,
                "control_id" => $control_id,
            );

            echo json_encode($arr);
        }

        public function dialog_add_cart() {
            $app = CApp::instance();

            $app->add('<center>
                        <div class="font-size14 font-black normal">
                        1 Produk telah berhasil ditambahkan<br>
                        ke keranjang belanja Anda
                        </div>
                        <br>
                        <a class="btn-62hallfamily bg-red border-3-red font-size18 margin-right-20" href="' . curl::base() . 'home' . '">Lanjutkan Belanja</a>
                        <a class="btn-62hallfamily bg-red border-3-red font-size18" href="' . curl::base() . 'products/shoppingcart/' . '">Lihat Keranjang</a>
                        <br>
                        <br>
                        </center>');

            echo $app->render();
        }

        public function dialog_empty_cart() {
            $app = CApp::instance();

            $app->add('<center>
                        <div class="font-size14 font-black normal">
                        Keranjang Belanja masih kosong
                        </div>
                        <br>
                        <a class="btn-62hallfamily bg-red border-3-red font-size18 margin-bottom-20" href="' . curl::base() . 'home' . '">Lanjutkan Belanja</a>
                        <br>
                        <br>
                        </center>');

            echo $app->render();
        }

        public function icon_shopping_cart($render = true) {
            $app = CApp::instance();
            $cart = Cart62hall_Product::factory();
            $cart_list = $cart->get_cart();
            $count_item = count($cart_list);

            $total_barang = 0;
            if ( $count_item > 0 ) {
                foreach ( $cart_list as $data_cart ) {
                    $total_barang += $data_cart['qty'];
                }
            }

            $shopping_cart = $app->add_div()->add_class('dropdown');
            $icon = $shopping_cart->add_div()->add_class('container-shopping-cart-icon')->add('<a href="#" class="62hall-dropdown icon-shopping-cart margin-top-3 pull-right bold" role="button" aria-expanded="false"></a>');
            $count_cart = $shopping_cart->add_div()->add_class('count font-white')
                    ->add($total_barang . ' <span class="lbl-item">item(s)</span>');

            $container_cart = $icon->add('<ul class="dropdown-menu font-black container-cart 62hall-options">');
            $triangle = $icon->add_div()->add_class('triangle arrow-up');
            $icon->add_div()->add_class('font-big bold font-red ftg')->add(clang::__('Keranjang Belanja Anda'));
            $container_cart->add('<li>');

            $total_barang = 0;
            $total_harga = 0;
            if ( $count_item > 0 ) {
                $items = $container_cart->add_div()->add_class('horizontal-overflow')->custom_css('max-height', '205px');

                foreach ( $cart_list as $data_cart ) {
                    $total_barang += $data_cart['qty'];
                    $total_harga += $data_cart['sub_total'];
                    $image = cdbutils::get_value("select file_path from product where product_id =" . $data_cart['product_id']);
//                    $image = image::get_image_url($data_cart['image'],'view_all');
//                    $image = $this->get_image($data_cart['file_path']);
                    
                    //menampilkan gmbr smentara
                    
//                cdbg::var_dump($data_cart);
//                die;
//                    $image = $data_cart['file_path'];
//                    $image = $data_cart['image'];

                    $items->add('
                                <div class="item col-md-12 col-xs-12 padding-left-right0">
                                    <div class="col-md-3 col-xs-3 outside-image-cart padding-0 margin-right-10">
                                    <img src="' . $image . '"/>
                                    </div>
                                    <div class="col-md-8 col-xs-8 content small padding-0 bold">
                                        ' . $data_cart['name'] . '
                                        <br>
                                        <div class="small">
                                        <i>Rp.' . ctransform::format_currency($data_cart['sell_price']) . '</i>
                                        <br>
                                        <i>' . clang::__('Jumlah') . ' ' . $data_cart['qty'] . ' ' . clang::__('Barang') . '</i>
                                        <div class="font-red col-md-12 col-xs-12 padding-0 margin-top-20">
                                            <div class="col-md-5 col-xs-6 padding-0">
                                                ' . clang::__('Sub Total') . '
                                            </div>
                                            <div class="col-md-7 col-xs-6 padding-0 text-right">
                                                Rp.' . ctransform::format_currency($data_cart['sub_total']) . '
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>'
                    );
                }
            }

            $container_total = $container_cart->add_div()->add_class('margin-top-10 row padding-0');
            $container_total->add_div()->add_class('col-md-6 col-xs-6 padding-right-0 font-size11 bold')->add(clang::__('TOTAL BARANG'));
            $container_total->add_div()->add_class('col-md-6 col-xs-6 padding-left-0 font-size11 bold')->add(': ' . $total_barang . ' ' . clang::__('Barang'));

            $container_total = $container_cart->add_div()->add_class('row padding-0');
            $container_total->add_div()->add_class('col-md-6 col-xs-6 padding-right-0 font-size11 bold font-red')->add(clang::__('TOTAL HARGA'));
            $container_total->add_div()->add_class('col-md-6 col-xs-6 padding-left-0 font-size11 bold font-red')->add(': Rp. ' . ctransform::format_currency($total_harga));

            $container_total = $container_cart->add_div()->add_class('row padding-0');
            $container_total->add_div()->add_class('col-md-12 font-size10 bold italic')->add('<red>*</red> ' . clang::__('Harga di luar biaya kirim.'));

            $container_checkout = $container_cart->add_div()->add_class('col-md-12 padding-0');

            $container_checkout->add_action()
                    ->set_label(clang::__('Check Out'))
                    ->add_class('btn-imlasvegas-secondary pull-right')
                    ->set_link(curl::base() . "products/shoppingcart/");
            
            $app->add_js('
            //jQuery(".icon-shopping-cart").on("click", function(){]
//            console.log("load");
            
            jQuery(".shopping-cart").click(function(){
//            console.log("a");
               //console.log(jQuery(this).parent().find(".container-cart").css("display"));
                if (jQuery(this).children().find(".container-cart").css("display") == "none") {
                    jQuery(this).children().find(".dropdown-menu").show();
                }
                else {
                    jQuery(this).children().find(".dropdown-menu").hide();
                }
                           
                        
            });');
            $container_cart = $icon->add('</li>');
            $container_cart = $icon->add('</ul>');
            if ( $render ) {
                echo $app->render();
            }
        }

        public function add_item_cart($product_id) {
            $err_code = 0;
            $err_message = '';
            $app = CApp::instance();
            $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");
            
            $cart = Cart62hall_Product::factory();
            $request = array_merge($_POST, $_GET);
            // dapatkan pilihan attribut dari GET
            $qty = carr::get($request, 'qty');
            if ( strlen($qty) == 0 ) {
                $qty = 0;
            }
            if ( $qty > 0 ) {
                $attribute = array();
                foreach ( $request as $key_req => $value_req ) {
                    if ( substr($key_req, 0, 8) == 'att_cat_' ) {
                        $attribute[substr($key_req, 8)] = $value_req;
                    }
                }
                // get data product 
                $product_simple = product::get_simple_product_from_attribute($product_id, $attribute);
                $product_id = $product_simple->product_id;
                $stock = $product_simple->stock;

                $qty = carr::get($request, 'qty');
                if ( strlen($qty) == 0 ) $qty = 0;

                $data_product = product::get_product($product_id);
                $data_price = $data_product['detail_price'];

                $arr_price = product::get_price($data_price);

                // get data image
                $data_product_image = product::get_product_image($product_id);

                $cart_list = $cart->get_cart();

                $exist_item = carr::get($cart_list, $product_id);

                $exists_item_qty = 0;
                if ( $exist_item != null ) {
                    $exists_item_qty = carr::get($exist_item, 'qty');
                    if ( strlen($exists_item_qty) == 0 ) $exists_item_qty = 0;
                }

                if ( $qty + $exists_item_qty > $stock ) {
                    $err_code++;
                    $err_message = "Stock tidak mencukupi";
                }
                if ( $err_code == 0 ) {





                    $image = isset($data_product_image[0]['image_path']) ? $data_product_image[0]['image_path'] : NULL;
//                    var_dump($image);
                    $name = carr::get($data_product, "name", NULL);

                    if ( $arr_price['promo_price'] > 0 || isset($arr_price['promo_value']) ) {
                        $sell_price = $arr_price['promo_price'];
                        $old_price = $arr_price['price'];
                    }
                    else {
                        $sell_price = $arr_price['price'];
                        $old_price = 0;
                    }

                    $weight = $data_product['weight'];


                    //            $product_attribute=product::get_product_list_from_attribute($product_id,$attribute);
                    $data_item = array(
                        'product_id' => $product_id,
                        'name' => $name,
                        'image' => $image,
                        'sell_price' => $sell_price,
                        'price' => $old_price,
                        'weight' => $weight,
                        'attribute' => $attribute,
                    );


                    if ( $exist_item == null ) {
                        $sub_total = $qty * $sell_price;
                        $data_item['qty'] = $qty;
                        $data_item['sub_total'] = $sub_total;
                        $cart->add_item($data_item);
                    }
                    else {
                        $qty = $exist_item['qty'] + $qty;
                        $sub_total = $qty * $sell_price;
                        $data_item['qty'] = $qty;
                        $data_item['sub_total'] = $sub_total;
                        $cart->update_item($product_id, $data_item);
                    }
                }

                if ( $err_code == 0 ) {
                    $this->icon_shopping_cart(false);

                    $dialog_html = '<center><div class="font-size14 font-black normal">Produk telah berhasil ditambahkan <br> ke keranjang belanja Anda</div><br>
                <div class="btn-shopingcart-wrapper">
                <a class="btn-imlasvegas-light btn-dialog-lanjutkan-belanja" href="' . curl::base() . 'home' . '">Lanjutkan Belanja</a>
                </div>
                <div class="btn-shopingcart-wrapper">
                <a class="btn-imlasvegas-light btn-dialog-lihat-keranjang" href="' . curl::base() . 'products/shoppingcart/' . '">Lihat Keranjang</a>
                </div><br><br></center>';
                    $dialog_html = str_replace("\r\n", '', $dialog_html);

                    $js = "
                        $.app.show_dialog('62dialog','<center>Tambah Belanjaan</center>','" . addslashes($dialog_html) . "'); 
                    ";
                }
                else {
                    $this->icon_shopping_cart(false);
                    $js = "
                        $.app.show_dialog('62dialog','<center>Maaf</center><br /><center><div class=\"font-size14 font-black normal\">" . addslashes($err_message) . "</div></center>');
                    ";
                }
            }
            else {
                $this->icon_shopping_cart(false);

                $dialog_html = '<center><div class="font-size14 font-black normal">Jumlah produk Anda masih kosong</div><br></center>';
                $dialog_html = str_replace("\r\n", '', $dialog_html);
                $js = "
                        $.app.show_dialog('62dialog','<center>Keranjang Belanja</center>','" . addslashes($dialog_html) . "'); 
                    ";
            }
            $app->add_js($js);

            echo $app->render();
        }

        public static function delete_item_cart($item_id) {
            $app = CApp::instance();
            $cart = Cart62hall_Product::factory();

            $error = 0;
            $error_message = "";
            $cart_list = array();

            if ( $error == 0 ) {
                try {
                    $cart_list = $cart->get_item($item_id);
                }
                catch ( Exception $e ) {
                    $error++;
                    $error_message = "Code is not exist";
                }
            }

            if ( $error == 0 ) {
                $cart->delete_item($item_id);
            }

            curl::redirect('products/shoppingcart/');
        }

        public static function clear_item() {
            $app = CApp::instance();
            $cart = Cart62hall_Product::factory();

            $error = 0;
            $error_message = "";

            if ( $error == 0 ) {
                try {
                    $cart_list = $cart->clear_item();
                }
                catch ( Exception $e ) {
                    $error++;
                    $error_message = "Clear item failed";
                }
            }

            curl::redirect('products/shoppingcart/');
        }

        private function get_image($image_name) {
            $ch = curl_init(image::get_image_url($image_name, 'view_all'));
            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_exec($ch);
            $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            if ( $retcode == 500 or $image_name == NULL ) {
                $image_name = curl::base() . 'application/62hallfamily/default/media/img/product/no-image.png';
            }
            else {
                $image_name = image::get_image_url($image_name, 'view_all');
            }
            curl_close($ch);
            return $image_name;
        }

    }
    