<?php

class UpdateContact_Controller extends ProductController {

    private $page = "product";

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->update_contact();
    }

    public function set_session() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $post = $this->input->post();
        $cart = Cart62hall_Product::factory();
        $error = 0;
        $error_message = '';
        
        $arr_check = array(
            'post_code_shipping', 'districts_shipping', 'city_shipping', 'province_shipping', 'city_shipping', 'phone_shipping', 'address_shipping', 'email_shipping', 'name_shipping',
            'post_code_pay', 'districts_pay', 'city_pay', 'province_pay', 'city_pay', 'phone_pay', 'address_pay', 'email_pay', 'name_pay',
        );

        if ($post == null) {
            $error++;
            $error_message = 'Please Fill Contact';
        }
        if ($error == 0) {
            $field = NULL;
            foreach ($arr_check as $key_post) {
                if (strlen(carr::get($post, $key_post)) == 0) {
                    if ($key_post == 'name_pay') {
                        $field = 'Nama Lengkap Pembayaran';
                    }
                    if ($key_post == 'email_pay') {
                        $field = 'Email Pembayaran';
                    }
                    if ($key_post == 'address_pay') {
                        $field = 'Alamat Pembayaran';
                    }
                    if ($key_post == 'phone_pay') {
                        $field = 'No. HP Pembayaran';
                    }
                    if ($key_post == 'province_pay') {
                        $field = 'Propinsi Pembayaran';
                    }
                    if ($key_post == 'city_pay') {
                        $field = 'Kota Pembayaran';
                    }
                    if ($key_post == 'districts_pay') {
                        $field = 'Kecamatan Pembayaran';
                    }
                    if ($key_post == 'post_code_pay') {
                        $field = 'Kode Pos Pembayaran';
                    }
                    if ($key_post == 'name_shipping') {
                        $field = 'Nama Lengkap Pengiriman';
                    }
                    if ($key_post == 'email_shipping') {
                        $field = 'Email Pengiriman';
                    }
                    if ($key_post == 'address_shipping') {
                        $field = 'Alamat Pengiriman';
                    }
                    if ($key_post == 'phone_shipping') {
                        $field = 'No. HP Pengiriman';
                    }
                    if ($key_post == 'province_shipping') {
                        $field = 'Propinsi Pengiriman';
                    }
                    if ($key_post == 'city_shipping') {
                        $field = 'Kota Pengiriman';
                    }
                    if ($key_post == 'districts_shipping') {
                        $field = 'Kecamatan Pengiriman';
                    }
                    if ($key_post == 'post_code_shipping') {
                        $field = 'Kode Pos Pengiriman';
                    }

                    $error++;
                    $error_message = $field . ' belum diisi.';
                }
            }
        }


        $session = Session::instance();
        $session = $session->get();
        
        //rajaongkir
        $post_shipping_price = carr::get($session, 'shipping_data');
        $post_shipping_price = carr::get($post_shipping_price, 'cost');
        
        //cek from post if session is null
        if($post_shipping_price === null){
            $post_shipping_price = carr::get($post,'shipping_price');
        }
        
        if ($error == 0) {
            if($post_shipping_price === null){
                $error++;
                $error_message= clang::__('Shipping not found for your city');
            }

        }
        
        if ($error == 0){
            if ($post_shipping_price === '-') {
                $error++;
                $error_message = 'Maaf Pengiriman ke Kota Anda Tidak Tersedia';
            }
        }
        
        if ($error == 0) {
            //get data post
            $session = CApiClientSession::factory('PG');
            $page = carr::get($post, 'page');
            $transaction_type = carr::get($post, 'transaction_type');

            $name_pay = carr::get($post, 'name_pay');
            $email_pay = carr::get($post, 'email_pay');
            $address_pay = carr::get($post, 'address_pay');
            $phone_area_pay = carr::get($post, 'phone_area_pay');
            $phone_pay = carr::get($post, 'phone_pay');
            $province_pay = carr::get($post, 'province_pay');
            $city_pay = carr::get($post, 'city_pay');
            $district_pay = carr::get($post, 'districts_pay');
            $post_code_pay = carr::get($post, 'post_code_pay');

            $name_shipping = carr::get($post, 'name_shipping');
            $email_shipping = carr::get($post, 'email_shipping');
            $address_shipping = carr::get($post, 'address_shipping');
            $phone_area_shipping = carr::get($post, 'phone_area_shipping');
            $phone_shipping = carr::get($post, 'phone_shipping');
            $province_shipping = carr::get($post, 'province_shipping');
            $city_shipping = carr::get($post, 'city_shipping');
            $district_shipping = carr::get($post, 'districts_shipping');
            $post_code_shipping = carr::get($post, 'post_code_shipping');

            $post_shipping_type_id = carr::get($post, 'shipping_type_id');
            $shipping_price = str_replace('Rp. ', '', $post_shipping_price);
            $shipping_price = ctransform::unformat_currency($shipping_price);

            $check_same = carr::get($post, 'check_same');
            $lat = carr::get($post, 'lat');
            $lng = carr::get($post, 'lng');
            $map_location = carr::get($post, 'map_location');

            $cart_list = $cart->get_cart();
            $session_cart_id = $cart->session_id();

            $transaction = array();
            $transaction = array();
            $transaction['page'] = $page;
            $transaction['type'] = $transaction_type;
            $transaction['session_card_id'] = $session_cart_id;
            $province_pay_text = NULL;
            if ($province_pay) {
                $q = "
                                select
                                        *
                                from
                                        province
                                where
                                        province_id=" . $province_pay . "
                        ";
                $r = $db->query($q);
                if ($r->count() > 0) {
                    $province_pay_text = $r[0]->name;
                }
            }
            $city_pay_text = NULL;
            if ($city_pay) {
                $q = "
                                select
                                        *
                                from
                                        city
                                where
                                        city_id=" . $city_pay . "
                        ";
                $r = $db->query($q);
                if ($r->count() > 0) {
                    $city_pay_text = $r[0]->name;
                }
            }
            $district_pay_text = NULL;
            if ($district_pay) {
                $q = "
                                select
                                        *
                                from
                                        districts
                                where
                                        districts_id=" . $district_pay . "
                        ";
                $r = $db->query($q);
                if ($r->count() > 0) {
                    $district_pay_text = $r[0]->name;
                }
            }

            $province_shipping_text = NULL;
            if ($province_shipping) {
                $q = "
                                select
                                        *
                                from
                                        province
                                where
                                        province_id=" . $province_shipping . "
                        ";
                $r = $db->query($q);
                if ($r->count() > 0) {
                    $province_shipping_text = $r[0]->name;
                }
            }

            $city_shipping_text = NULL;
            if ($city_shipping) {
                $q = "
                                select
                                        *
                                from
                                        city
                                where
                                        city_id=" . $city_shipping . "
                        ";
                $r = $db->query($q);
                if ($r->count() > 0) {
                    $city_shipping_text = $r[0]->name;
                }
            }
            $district_shipping_text = NULL;
            if ($district_shipping) {
                $q = "
                                select
                                        *
                                from
                                        districts
                                where
                                        districts_id=" . $district_shipping . "
                        ";
                $r = $db->query($q);
                if ($r->count() > 0) {
                    $district_shipping_text = $r[0]->name;
                }
            }

            $arr_contact = array();
            $arr_contact_billing = array();
            $arr_contact_shipping = array();
            $arr_name_pay = explode(' ', $name_pay, 2);
            $arr_name_shipping = explode(' ', $name_shipping, 2);
            $first_name_pay = $name_pay;
            $last_name_pay = '';
            $first_name_shipping = $name_shipping;
            $last_name_shipping = '';
            if (count($arr_name_pay) == 2) {
                $first_name_pay = $arr_name_pay[0];
                $last_name_pay = $arr_name_pay[1];
            }

            if (count($arr_name_shipping) == 2) {
                $first_name_shipping = $arr_name_shipping[0];
                $last_name_shipping = $arr_name_shipping[1];
            }
            //handling billing
            if ($error == 0) {
                if (strlen($first_name_pay) > 20) {
                    $error++;
                    $error_message = 'Billing first name must be least than equal to 20 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($last_name_pay) > 20) {
                    $error++;
                    $error_message = 'Billing last name must be least than equal to 20 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($address_pay) > 200) {
                    $error++;
                    $error_message = 'Billing address must be least than equal to 200 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($city_pay_text) > 100) {
                    $error++;
                    $error_message = 'Billing city must be least than equal to 100 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($post_code_pay) > 10) {
                    $error++;
                    $error_message = 'Billing postal code must be least than equal to 10 characters.';
                }
            }
            if ($error == 0) {
                if (strlen($phone_area_pay . $phone_pay) > 19) {
                    $error++;
                    $error_message = 'Billing phone must be least than equal to 19 characters.';
                }
            }

            //handling shipping
            if ($error == 0) {
                if (strlen($first_name_shipping) > 20) {
                    $error++;
                    $error_message = 'Shipping first name must be least than equal to 20 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($last_name_shipping) > 20) {
                    $error++;
                    $error_message = 'Shipping last name must be least than equal to 20 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($address_shipping) > 200) {
                    $error++;
                    $error_message = 'Shipping address must be least than equal to 200 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($city_shipping_text) > 100) {
                    $error++;
                    $error_message = 'Shipping city must be least than equal to 100 characters.';
                }
            }

            if ($error == 0) {
                if (strlen($post_code_shipping) > 10) {
                    $error++;
                    $error_message = 'Shipping postal code must be least than equal to 10 characters.';
                }
            }
            if ($error == 0) {
                if (strlen($phone_area_shipping . $phone_shipping) > 19) {
                    $error++;
                    $error_message = 'Shipping phone must be least than equal to 19 characters.';
                }
            }

            if ($error == 0) {

                $arr_contact_billing['first_name'] = $first_name_pay;
                $arr_contact_billing['last_name'] = $last_name_pay;
                $arr_contact_billing['address'] = $address_pay;
                $arr_contact_billing['email'] = $email_pay;
                $arr_contact_billing['province_id'] = $province_pay;
                $arr_contact_billing['province'] = $province_pay_text;
                $arr_contact_billing['city_id'] = $city_pay;
                $arr_contact_billing['city'] = $city_pay_text;
                $arr_contact_billing['district_id'] = $district_pay;
                $arr_contact_billing['district'] = $district_pay_text;
                $arr_contact_billing['postal_code'] = $post_code_pay;
                $arr_contact_billing['phone'] = $phone_area_pay . $phone_pay;
                $arr_contact_billing['country_code'] = 'IDN';

                $arr_contact_shipping['first_name'] = $first_name_pay;
                $arr_contact_shipping['last_name'] = $last_name_pay;
                $arr_contact_shipping['address'] = $address_pay;
                $arr_contact_shipping['email'] = $email_pay;
                $arr_contact_shipping['province_id'] = $province_pay;
                $arr_contact_shipping['province'] = $province_pay_text;
                $arr_contact_shipping['city_id'] = $city_pay;
                $arr_contact_shipping['city'] = $city_pay_text;
                $arr_contact_shipping['district_id'] = $district_pay;
                $arr_contact_shipping['district'] = $district_pay_text;
                $arr_contact_shipping['postal_code'] = $post_code_pay;
                $arr_contact_shipping['phone'] = $phone_area_pay . $phone_pay;
                $arr_contact_shipping['country_code'] = 'IDN';

                if ($check_same == null) {
                    $arr_contact_shipping['first_name'] = $first_name_shipping;
                    $arr_contact_shipping['last_name'] = $last_name_shipping;
                    $arr_contact_shipping['address'] = $address_shipping;
                    $arr_contact_shipping['email'] = $email_shipping;
                    $arr_contact_shipping['province_id'] = $province_shipping;
                    $arr_contact_shipping['province'] = $province_shipping_text;
                    $arr_contact_shipping['city_id'] = $city_shipping;
                    $arr_contact_shipping['city'] = $city_shipping_text;
                    $arr_contact_shipping['district_id'] = $district_shipping;
                    $arr_contact_shipping['district'] = $district_shipping_text;
                    $arr_contact_shipping['postal_code'] = $post_code_shipping;
                    $arr_contact_shipping['phone'] = $phone_area_shipping . $phone_shipping;
                    $arr_contact_shipping['country_code'] = 'IDN';
                }



                $product_code = '';
                $product_name = '';
                $product_price = '';

                $total_item = 0;
                foreach ($cart_list as $row_cart_list) {
                    $product_id = carr::get($row_cart_list, 'product_id');
                    $qty = carr::get($row_cart_list, 'qty');

                    $data_product = product::get_product($product_id);
                    $detail_price = $data_product['detail_price'];
                    $arr_price = product::get_price($detail_price);

                    $price = 0;
                    if ($arr_price['promo_price'] > 0) {
                        $price = $arr_price['promo_price'];
                    } else {
                        $price = $arr_price['price'];
                    }

                    if (count($data_product) > 0) {
                        $item = array();

                        $item['item_id'] = $product_id;
                        $item['item_code'] = carr::get($data_product, 'code');
                        $item['item_name'] = carr::get($data_product, 'name');
                        $item['qty'] = $qty;
                        $item['price'] = $price;
                        $transaction['item'][] = $item;

                        //cut 50 char for CC
                        $item_payment_name = carr::get($data_product, 'name');
                        if (strlen($item_payment_name) > 50) {
                            $item_payment_name = substr($item_payment_name, 0, 49);
                        }
                        $item['item_id'] = $product_id;
                        //sku but use product id instead for CC
                        $item['item_code'] = $product_id;
                        $item['item_name'] = $item_payment_name;
                        $item['qty'] = $qty;
                        $item['price'] = $price;
                        $transaction['item_payment'][] = $item;
                    }
                    $subtotal_item = 0;
                    $subtotal_item = ($qty * $price);
                    $total_item += $subtotal_item;
                }
                if ($shipping_price > 0) {
                    $item['item_id'] = NULL;
                    $item['item_code'] = 'SHP';
                    $item['item_name'] = 'Shipping to ' . $city_shipping_text;
                    $item['qty'] = 1;
                    $item['price'] = $shipping_price;
                    $transaction['item_payment'][] = $item;
                }

                $grand_total = $total_item + $shipping_price;
                $amount_payment = $grand_total;

                $transaction['total_item'] = $total_item;
                $transaction['total_shipping'] = $shipping_price;
                $transaction['amount_payment'] = $amount_payment;

                $fee_shipping = 0;
                $fee_shipping_text = 'Rp. ' . ctransform::format_currency($fee_shipping);
                if ($fee_shipping == 0) {
                    $fee_shipping_text = 'Gratis';
                }

                $grand_total = $total_item + $fee_shipping;

                $arr_contact['first_name'] = $arr_contact_billing['first_name'];
                $arr_contact['last_name'] = $arr_contact_billing['last_name'];
                $arr_contact['email'] = $arr_contact_billing['email'];
                $arr_contact['phone'] = $arr_contact_billing['phone'];
                $arr_contact['billing_address'] = $arr_contact_billing;
                $arr_contact['shipping_address'] = $arr_contact_shipping;
                $arr_contact['same_billing_shipping'] = $check_same;
                $transaction['contact_detail'] = $arr_contact;
                $transaction['lat'] = $lat;
                $transaction['lng'] = $lng;
                $transaction['map_location'] = $map_location;
                $session->set('transaction', $transaction);
                
            }
        }

        $session_php = Session::instance();
        $session_php->set('message_update_contact', $error_message);
        $session_php->set('error_update_contact', $error);

        $data_post = array();
        $data_post['name_pay'] = carr::get($post, 'name_pay');
        $data_post['email_pay'] = carr::get($post, 'email_pay');
        $data_post['address_pay'] = carr::get($post, 'address_pay');
        $data_post['phone_pay'] = carr::get($post, 'phone_pay');
        $data_post['province_pay'] = carr::get($post, 'province_pay');
        $data_post['city_pay'] = carr::get($post, 'city_pay');
        $data_post['districts_pay'] = carr::get($post, 'districts_pay');
        $data_post['post_code_pay'] = carr::get($post, 'post_code_pay');

        $data_post['name_shipping'] = carr::get($post, 'name_shipping');
        $data_post['email_shipping'] = carr::get($post, 'email_shipping');
        $data_post['address_shipping'] = carr::get($post, 'address_shipping');
        $data_post['phone_shipping'] = carr::get($post, 'phone_shipping');
        $data_post['province_shipping'] = carr::get($post, 'province_shipping');
        $data_post['city_shipping'] = carr::get($post, 'city_shipping');
        $data_post['districts_shipping'] = carr::get($post, 'districts_shipping');
        $data_post['post_code_shipping'] = carr::get($post, 'post_code_shipping');
        $session_php->set('post_update_contact', $data_post);

        if ($error == 0) {
            curl::redirect('payment/form_payment');
        } else {
            curl::redirect(curl::base() . 'products/updatecontact');
        }
    }

    public function update_contact() {
        $app = CApp::instance();
        $user = $app->user();
        $db = CDatabase::instance();
        $cart = Cart62hall_Product::factory();
        $cart_list = $cart->get_cart();
        $count_item = $cart->item_count();
        $org_id = $this->org_id();
        $menu_list = $this->menu_list();
        $link_list = $this->list_menu();

        if (count($cart_list) == 0) {
            curl::redirect(curl::base());
        }

        $session_php = Session::instance();
        $transaction = $session_php->get('session_post');
        $contact = carr::get($transaction, 'contact_detail');
        $contact_billing = carr::get($contact, 'billing_address');
        $contact_shipping = carr::get($contact, 'shipping_address');
        $contact = carr::get($transaction, 'contact_detail');
        $contact_billing = carr::get($contact, 'billing_address');
        $contact_shipping = carr::get($contact, 'shipping_address');

        $first_name_pay = carr::get($contact_billing, 'first_name');
        $last_name_pay = carr::get($contact_billing, 'last_name');
        $name_pay = $first_name_pay . ' ' . $last_name_pay;
        $email_pay = carr::get($contact_billing, 'email');
        $address_pay = carr::get($contact_billing, 'address');
        $phone_pay = carr::get($contact_billing, 'phone');
        $province_pay = carr::get($contact_billing, 'province_id');
        $city_pay = carr::get($contact_billing, 'city_id');
        $district_pay = carr::get($contact_billing, 'district_id');
        $post_code_pay = carr::get($contact_billing, 'postal_code');

        $first_name_shipping = carr::get($contact_shipping, 'first_name');
        $last_name_shipping = carr::get($contact_shipping, 'last_name');
        $name_shipping = $first_name_shipping . ' ' . $last_name_shipping;
        $email_shipping = carr::get($contact_shipping, 'email');
        $address_shipping = carr::get($contact_shipping, 'address');
        $phone_shipping = carr::get($contact_shipping, 'phone');
        $province_shipping = carr::get($contact_shipping, 'province_id');
        $city_shipping = carr::get($contact_shipping, 'city_id');
        $district_shipping = carr::get($contact_shipping, 'district_id');
        $post_code_shipping = carr::get($contact_shipping, 'postal_code');
        $phone_pay = substr($phone_pay, 3);
        $phone_shipping = substr($phone_shipping, 3);

        $sess_lat = carr::get($transaction, 'lat');
        $sess_lng = carr::get($transaction, 'lng');
        $sess_map_location = carr::get($transaction, 'map_location');

        $arr_session = array(
            'name_pay' => $name_pay, 
            'email_pay' => $email_pay, 
            'address_pay' => $address_pay, 
            'phone_pay' => $phone_pay, 
            'province_pay' => $province_pay, 
            'city_pay' => $city_pay, 
            'districts_pay' => $district_pay, 
            'name_shipping' => $name_shipping, 
            'email_shipping' => $email_shipping, 
            'address_shipping' => $address_shipping, 
            'phone_shipping' => $phone_shipping, 
            'province_shipping' => $province_shipping,
            'city_shipping' => $city_shipping, 
            'districts_shipping' => $district_shipping,
            'post_code_pay' => $post_code_pay,
            'post_code_shipping' => $post_code_shipping,
        );

        $data_set_value = array();

        if (count($session_php->get('post_update_contact'))) {
            $data_set_value = $session_php->get_once('post_update_contact');
        }
        if (count($transaction) > 0 && count($data_set_value) == 0) {
            $data_set_value = $arr_session;
        }

        $country_id = '';
        $q = "
                select
                 *
                from
                    country
                where
                    name=" . $db->escape('Indonesia') . "
            ";
        $r = $db->query($q);
        if ($r->count() > 0) {
            $country_id = $r[0]->country_id;
        }

        //~~
        $org_id = $app->org_id();


        // SHOPPING CART 
        $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

        // Menu
        $product_category = product_category::get_product_category_menu($this->page());

        $form = $app->add_form('form_transaction')
                ->set_action(curl::base() . 'products/updatecontact/set_session')
                ->add_class('form-62hallfamily form-gold-contact form- padding-0');
        $form->add_control('page', 'hidden')
                ->set_value('product');
        $form->add_control('transaction_type', 'hidden')->set_value('buy');

        $container = $form->add_div()->add_class('container');

        $container_row = $container->add_div()->add_class('row-update-contact-full row');
        // UPDATE CONTACT
        $view_shopping_cart = $container_row->add_div()->add_class('col-md-5 col-md-push-7 ringkasan-pemesanan margin-top-30 margin-bottom-30');
        $view_update_contact = $container_row->add_div()
                ->add_class('col-md-7 col-md-pull-5 margin-top-30 margin-bottom-30');
                
        $err_code = $session_php->get_once('error_update_contact');
        $message = $session_php->get_once('message_update_contact');

        if (!empty($message)) {
            $alert = 'success';
            if ($err_code > 0) {
                $alert = 'danger';
            }
            $alert_message = $view_update_contact->add_div()->add_class('alert alert-' . $alert)
                    ->add('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' . $message);
        }

        $element_update_contact = $view_update_contact->add_element('62hall-update-contact', 'element_update_contact');
        $element_update_contact->set_country_id($country_id)
                ->set_data_value($data_set_value);
        $shipping_service=cobj::get($this->_data_org,'shipping_service');
        
        $cart = Cart62hall_Product::factory();
        $cart_list = $cart->get_cart();
        $total_weight = 0;
        if ($count_item > 0) {
            foreach ($cart_list as $data_cart) {
                $total_weight += $data_cart['weight'] * $data_cart['qty'];
            }
        }
        $total_weight = round($total_weight);
        
        if($total_weight==0){
            $total_weight=1;
        }
        
        switch($shipping_service){
            case "rajaongkir":{
                $raja_ongkir_data = array(
                    'weight' => $total_weight,
                );
                $element_update_contact->set_using_shipping(false);
                $element_update_contact->set_using_raja_ongkir(true);
                $element_update_contact->set_raja_ongkir_data($raja_ongkir_data);
                break;
            }
            default :{
                $element_update_contact->set_using_shipping(true);
            }
        }

        $div_button = $view_update_contact->add_div()->add_class('btn-payment-wrap');
        $div_button->add_action('submit_button')
                ->set_label(clang::__('LANJUTKAN PEMBAYARAN'))
                ->add_class('btn-imlasvegas-secondary')
                ->set_submit(TRUE);


        // RINGKASAN PEMESASAN
        $view_shopping_cart->add_element('62hall-product-cart', 'product_cart');

        $form->add_control('total_weight', 'hidden')->set_value($total_weight);

        echo $app->render();
    }

    private function get_image($image_name) {
        $ch = curl_init(image::get_image_url($image_name, 'view_all'));
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_exec($ch);
        $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($retcode == 500 or $image_name == NULL) {
            $image_name = curl::base() . 'application/62hallfamily/default/media/img/product/no-image.png';
        } else {
            $image_name = image::get_image_url($image_name, 'view_all');
        }
        curl_close($ch);
        return $image_name;
    }

}
