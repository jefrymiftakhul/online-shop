<?php

class Subscribe_Controller extends SixtyTwoHallFamilyController {

    public function __construct() {
        parent::__construct();
    }

    public function user_subscribe($type = null) {
        $app = CApp::instance();
        $db = CDatabase::instance();
        //$org_id = ccfg::get('org_id');
        $org_id = $this->org_id();

        $request = array_merge($_GET, $_POST);
        $email = carr::get($request, 'email_subscribe');
        $name = carr::get($request, 'name_subscribe');
        $action = carr::get($request, 'action_subscribe');

        if ($action == 'page-gold') {
            $email = carr::get($request, 'email_subscribe_gold');
        }

        if ($type == null) {
            $type = carr::get($request, 'type');
        }
        $err_code = 0;
        $message = NULL;

        if (!empty($email)) {

            if (!cvalid::email($email)) {
                $err_code++;
                $message = 'Format Email salah';
            }

            if ($err_code == 0) {
                $result = subscribe::save($type, $request);
                $err_code += $result['err_code'];
                $message = $result['message'];
            }
        } else {
            $err_code++;
            $message = "Email masih kosong";
        }


        if(isset($_GET['source'])&&$_GET['source']=='landing') {
           
            if (empty($name)) {
                $err_code++;
                $message="Nama masih kosong";
            }
            
            if($err_code==0) {
                $app->add('Terima Kasih atas pendaftaran email anda!');
                $classname = "SubscribeEmail";
                $file = CF::get_files('libraries', "FormatEmail/" . $classname);
                include_once $file[0];
                $tbclass = new $classname();
                $format = $tbclass::format($name);
                $subject = 'Hi ' . $name . ' terima kasih telah bargabung dengan KinerjaMall';
                try {
                    cmail::send_smtp($email, $subject, $format);
                } catch(Exception $ex){
                    //do nothing
                }
            } else {
                $app->add($message);
            }
            echo $app->render();
            return;
            
        }
        $this->message($err_code, $message);
    }

    public function message($err_code, $message) {
        $app = CApp::instance();

        if (strlen($message) > 0) {
            if ($err_code == 0) {
                $app->add_div()->add_class('alert alert-success')
                        ->add('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' . $message);
            } else {
                $app->add_div()->add_class('alert alert-danger')
                        ->add('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' . $message);
            }
        }

        $app->add_js("
                         $('#email_subscribe').val('');
                        ");

        echo $app->render();
    }

    public function nonactive($id = "") {
        if (strlen($id) == 0) {
            curl::redirect("master/subscribe");
        }
        $app = CApp::instance();
        $user = $app->user();
        $db = CDatabase::instance();
        $err_code = 0;
        $err_message = '';

        if ($err_code == 0) {
            $q = "SELECT * FROM subscribe WHERE subscribe_id = " . $db->escape($id);
            $check = cdbutils::get_row($q);


            $status = "Nonactivated";
            try {
                $db->update("subscribe", array("is_active" => 0, "updated" => date("Y-m-d H:i:s"),
                    "updatedby" => 'SYSTEM'), array("subscribe_id" => $id));
            } catch (Exception $e) {
                $err_code++;
                $err_message = clang::__("system_delete_fail") . $e->getMessage();
            }
        }

        if ($err_code == 0) {
            $err_message = clang::__("Subscribe") . " " . clang::__("Successfully") . " " . clang::__($status) . " !";
            cmsg::add('success', $err_message);
        } else {
            cmsg::add('error', $err_message);
        }

        $app->add($err_message);

        echo $app->render();
    }

}
