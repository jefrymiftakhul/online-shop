<?php

/**
 *
 * @author Raymond Sugiarto
 * @since  Apr 13, 2015
 * @license http://piposystem.com Piposystem
 */
class AdminSixtyTwoHallFamily_Controller extends SixtyTwoHallFamilyController {

    public function index() {
        $app = CApp::instance();
        echo $app->render();
    }

    public function country_select() {
        $country_id = "";
        $all = false;
        $ctrlid = "country_id";

        if (isset($_GET["country_id"])) {
            $country_id = $_GET["country_id"];
        }
        if (isset($_GET["all"])) {
            $all = $_GET["all"];
            if ($all === "true")
                $all = true;
            if ($all === "false")
                $all = false;
        }
        if (isset($_GET["ctrlid"])) {
            $ctrlid = $_GET["ctrlid"];
        }
        $user_id = "";
        $remember_key = "";
        if (isset($_GET["user_id"])) {
            $user_id = $_GET["user_id"];
        }
        if (isset($_GET["remember_key"])) {
            $remember_key = $_GET["remember_key"];
        }
        $options = array(
            "country_id" => $country_id,
            "all" => $all,
            "ctrlid" => $ctrlid,
            "user_id" => $user_id,
        );
        echo admin62hallfamily::country_select($options);
    }

    public function province_select() {
        $province_id = "";
        $country_id = "";
        $country_required = true;
        $all = false;
        $ctrlid = "province_id";


        if (isset($_GET["country_id"])) {
            $country_id = $_GET["country_id"];
        }
        if (isset($_GET["country_required"])) {
            $country_required = $_GET["country_required"];
            if ($country_required === "true")
                $country_required = true;
            if ($country_required === "false")
                $country_required = false;
        }
        if (isset($_GET["province_id"])) {
            $province_id = $_GET["province_id"];
        }

        if (isset($_GET["all"])) {
            $all = $_GET["all"];
            if ($all === "true")
                $all = true;
            if ($all === "false")
                $all = false;
        }
        if (isset($_GET["ctrlid"])) {
            $ctrlid = $_GET["ctrlid"];
        }
        $user_id = "";
        $remember_key = "";
        if (isset($_GET["user_id"])) {
            $user_id = $_GET["user_id"];
        }

        $options = array(
            "country_id" => $country_id,
            "country_required" => $country_required,
            "province_id" => $province_id,
            "all" => $all,
            "ctrlid" => $ctrlid,
            "user_id" => $user_id,
        );
        echo admin62hallfamily::province_select($options);
    }

    public function city_select() {
        $city_id = "";
        $province_id = "";
        $province_required = true;
        $all = false;
        $ctrlid = "city_id";


        if (isset($_GET["province_id"])) {
            $province_id = $_GET["province_id"];
        }
        if (isset($_GET["province_required"])) {
            $province_required = $_GET["province_required"];
            if ($province_required === "true")
                $province_required = true;
            if ($province_required === "false")
                $province_required = false;
        }
        if (isset($_GET["city_id"])) {
            $city_id = $_GET["city_id"];
        }

        if (isset($_GET["all"])) {
            $all = $_GET["all"];
            if ($all === "true")
                $all = true;
            if ($all === "false")
                $all = false;
        }
        if (isset($_GET["ctrlid"])) {
            $ctrlid = $_GET["ctrlid"];
        }
        $user_id = "";
        $remember_key = "";
        if (isset($_GET["user_id"])) {
            $user_id = $_GET["user_id"];
        }

        $options = array(
            "province_id" => $province_id,
            "province_required" => $province_required,
            "city_id" => $city_id,
            "all" => $all,
            "ctrlid" => $ctrlid,
            "user_id" => $user_id,
        );

        echo admin62hallfamily::city_select($options);
    }

}
