<?php

/**
 * @author M. Isman SUbakti
 * @since 05 December 2016 
**/

class Show_product_Controller extends ProductController {
	
	function __construct(){
		parent::__construct();
	}

	public function show($type = ''){
            $app = CApp::instance();
            $org_id = CF::org_id();

            $request = array_merge($_POST, $_GET);
            
            $page = carr::get($request, 'page', 1);
            $view_limit = carr::get($request, 'view_limit', 10);
            $sort_by = carr::get($request, 'sortby', 'all');
            
            $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

            $arr_sort = array(
                'all'   => clang::__('All Product'),
                'popular'   => clang::__('Popular'),
                'diskon'    => clang::__('Diskon'),
                'min_price' => clang::__('Termurah'),
                'max_price' => clang::__('Termahal'),
                'is_new'    => clang::__('Terbaru'),
                'A-Z'       => clang::__('A-Z'),
                'Z-A'       => clang::__('Z-A'),
            );
            
            
            $arr_show = array(
                '10' => 10,
                '15' => 15,
                '20' => 20,
                '25' => 25,
                '30' => 30
            );
            $limit_default = array_shift(array_values($arr_show));
            $data = deal::get_deal($org_id, $this->page(), $type,array('sortby'=>'priority'));
            
            $total_product = count($data);
            
            $showing_product = $limit_default;
            if($total_product < $limit_default){
                $showing_product = $total_product;
            }
            
            $container = $app->add_div()->add_class('container container-lasvegas');
            $product_total = $container->add_div()->add_class('container-product-total');
            $row = $product_total->add_div()->add_class('row');
            $col1 = $row->add_div()->add_class('col-md-6');
            $col2 = $row->add_div()->add_class('col-md-6');
            $col2->add_div()->add_class('txt-product-total')->add(clang::__('There are').' <span class="txt-count-product">'.$total_product.'</span> '.clang::__('Products'));
            $container->add_div()->add_class('separate-show-product');

            $container_sort = $container->add_div()->add_class('container-sort');
            $row = $container_sort->add_div()->add_class('row');
            $col1 = $row->add_div()->add_class('col-md-8');
            $col2 = $row->add_div()->add_class('col-md-4');

            $content_sort = $col1->add_div()->add_class('content-sort');
            $content_show = $col2->add_div()->add_class('content-show');

            $content_sort->add_div()->add_class('content-sort-element cs-text-sortby')->add(clang::__('Sort by'));

            $control_sortby = $content_sort->add_div()->add_class('content-sort-element cs-control-sortby');
            $control_sortby->add_div()->add_control('sort_by', 'select')->set_list($arr_sort)->set_value($sort_by)->add_class('select-imlasvegas select-imlasvegas-filter');

            $content_sort->add_div()->add_class('content-sort-element cs-text-show')->add(clang::__('Show'));

            $control_show = $content_sort->add_div()->add_class('content-sort-element cs-control-show');
            $control_show->add_div()->add_control('show', 'select')->set_list($arr_show)->set_value($view_limit)->add_class('select-imlasvegas select-imlasvegas-filter');

            //$content_show->add(clang::__('Showing').' 1-'.$showing_product.' '.clang::__('Product'));
            $content_show->add(clang::__('Showing').' <span class="txt-page-show">1-10</span> '.clang::__('Product'));

            $container_product_show = $container->add_div()->add_class('container-product-show');
            
            $div_product_result = $container_product_show->add_div('page-product-category');
            $container_product_show
                    ->add_div('loading-page-product-category')
                    ->custom_css('text-align', 'center')
                    ;

//            $element_deal = $container->add_element('62hall-slide-product', $type)->add_class('slide-deal');
//            $element_deal->set_list_item($data);
//            $element_deal->set_limit($limit_default);
            
            $keyword = '';
            if ($total_product == 0) {
                $div_product_result->add_div()->add_element('62hall-form-request');    
            }
            else {
                $handler = $div_product_result->add_listener('ready');
                        $handler->add_handler('reload')
                        ->set_target('page-product-category')
                        //->set_url(curl::base() . "reload/reload_product_filter?view_limit=".$limit_default."&source=search&visibility=search&sortby=diskon&filter_page=" . $page . '&filter_product_category_id='.$product_category_id.'&filter_name=' . urlencode($keyword))
                        ->set_url(curl::base() . "reload/reload_product_filter_deal?view_limit=".$view_limit."&page=".$page."&source=search&visibility=search&sortby=".$sort_by."&filter_page=" . $this->page() .'&filter_name=' . urlencode($keyword).'&deal_type='.$type.'')
                    ;
//                $handler->add_handler('custom')
//                        ->set_js('show_loading();');
            }
            
            
            $js = "
                function show_loading(){
                    jQuery('#loading-page-product-category').append('<img src=".curl::httpbase().'application/ittronmall/default/media/img/imlasvegas/loading.gif'." />');
                }
                function hide_loading(){
                    jQuery('#loading-page-product-category').hide();
                }
                function reload_filter() {
                    var txt_sort_by = jQuery('#sort_by').val();
                    var txt_sort_view = jQuery('#show').val();
                    // $.cresenity.reload('page-product-category', '".curl::httpbase()."reload/reload_product_filter_deal?view_limit='+txt_sort_view+'&sortby='+txt_sort_by+'&source=filter_reload&deal_type=".$type."', 'get');
                    document.location.href = '". curl::httpbase() ."/show_product/show/" . $type . "?view_limit='+txt_sort_view+'&sortby='+txt_sort_by;
                }

                jQuery('.select-imlasvegas-filter').change(function() {
                    reload_filter();
                });
            ";

            $app->add_js($js);
            
            
            echo $app->render();
	}
}