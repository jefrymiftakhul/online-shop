<?php

/**
 *
 * @author Riza 
 * @since  Nov 12, 2015
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class Reload_Controller extends SixtyTwoHallFamilyController {

    const __CONTROLLER = 'reload/';
    const __is_test = true;

    public function __construct() {
        parent::__construct();
    }

    public function reload_gold_weight() {
        $app = CApp::instance();
        $category_id = carr::get($_GET, 'category');
        $gold_pecahan = array();
        if ($category_id) {
            $gold_pecahan = product::get_weight_gold($category_id);
        }
        $html = '';
        $html .= "<select id='gold_weight' name='gold_weight' class='select-62hallfamily'>";
        $html .= "<option value='' disabled selected hidden></option>";
        if (count($gold_pecahan) > 0) {
            foreach ($gold_pecahan as $weight) {
                $html .= "<option value='" . $weight . "'>" . $weight . " gr</option>";
            }
        }
        $html .= "</select>";
        $app->add($html);
        echo $app->render();
    }

    public function reload_member_address() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $member_address_id = carr::get($_GET, 'member_address_payment');
        if ($member_address_id == null) {
            $member_address_id = carr::get($_GET, 'member_address_shipping');
        }
        $q = "
			select
				ma.type as type,
				ma.name as name,
				ma.phone as phone,
				ma.address as address,
				ma.postal as postal,
				ma.email as email,
				p.province_id as province_id,
				p.name as province,
				c.city_id as city_id,
				c.name as city,
				d.districts_id as districts_id,
				d.name as districts
			from
				member_address as ma
				inner join province as p on p.province_id=ma.province_id
				inner join city as c on c.city_id=ma.city_id
				inner join districts as d on d.districts_id=ma.districts_id
			where
				member_address_id=" . $db->escape($member_address_id) . "
		";
        $r = cdbutils::get_row($q);
        if ($r) {
            $data = array();
            $data['type'] = $r->type;
            $data['name'] = $r->name;
            $data['phone'] = $r->phone;
            $data['address'] = $r->address;
            $data['postal'] = $r->postal;
            $data['email'] = $r->email;
            $data['province_id'] = $r->province_id;
            $data['province'] = $r->province;
            $data['city_id'] = $r->city_id;
            $data['city'] = $r->city;
            $data['districts_id'] = $r->districts_id;
            $data['districts'] = $r->districts;
            $json = json_encode($data);
            $app->add_js("reload_member_address(" . $json . ");");
        }
        echo $app->render();
    }

    public function reload_province() {
        $country_id = carr::get($get, 'country_pay');
        $from = 'pay';
        if ($country_id == null) {
            $country_id = carr::get($get, 'country_shipping');
            $from = 'shipping';
        }
        $control_province_select = $app->add_field()->set_label('Propinsi')->add_control('province_' . $from, 'province-select')->add_class('width-40-percent');
        $control_province_select->set_all(false);
        $control_province_select->set_country_id($country_id);
        $listener_province_select = $control_province_select->add_listener('change');
        $listener_province_select
                ->add_handler('reload')
                ->set_target('div_province_' . $from)
                ->set_url(curl::base() . self::__CONTROLLER . 'reload_city')
                ->add_param_input(array('province_' . $from));
        $listener_province_select = $control_province_select->add_listener('ready');
        $listener_province_select
                ->add_handler('reload')
                ->set_target('div_province_' . $from)
                ->set_url(curl::base() . self::__CONTROLLER . 'reload_city')
                ->add_param_input(array('province_' . $from));
        echo $app->render();
    }

    public function reload_city($city_id = '', $districts_id = '') {
        $app = CApp::instance();
        $get = $_GET;
        $province_id = carr::get($get, 'province_pay');
        $page = carr::get($get, 'page');
        $raja_ongkir = carr::get($get, 'raja_ongkir');
        $from = 'pay';
        $addition_param = '';

        if ($province_id == null) {
            $province_id = carr::get($get, 'province_shipping');
            $from = 'shipping';
        }
        $control_city_select = $app->add_field()->set_label('Kota')->add_control('city_' . $from, 'city-select')->add_class('width-40-percent');
        $control_city_select->set_all(false);
        $control_city_select->set_province_id($province_id);
        if ($from == 'shipping') {
            if ($page == 'gold') {
                $addition_param = '&page=' . $page;
                $control_city_select->set_page($page);
            }
            if(ccfg::get('have_shipping_location_from_shipping_price')){
                if($raja_ongkir==0){
                    $control_city_select->set_same_location_shipping_price(true);
                }
            }
            
            
        }
        if ($raja_ongkir==1){
                $addition_param = '&raja_ongkir=1';
        }
        if (strlen($city_id) > 0) {
            $control_city_select->set_value($city_id);
        }
        $url_district = '';
        if (strlen($districts_id) > 0) {
            $url_district = '/'.$districts_id;
        }

        $listener_change_city_select = $control_city_select->add_listener('change');
        $listener_change_city_select
                ->add_handler('reload')
                ->set_target('div_districts_' . $from)
                ->set_url(curl::base() . self::__CONTROLLER . 'reload_districts?' . $addition_param)
                ->add_param_input(array('city_' . $from));
        $listener_ready_city_select = $control_city_select->add_listener('ready');
        $listener_ready_city_select
                ->add_handler('reload')
                ->set_target('div_districts_' . $from)
                ->set_url(curl::base() . self::__CONTROLLER . 'reload_districts'.$url_district.'?' . $addition_param)
                ->add_param_input(array('city_' . $from));
        $app->add_js("
				if($('#check_same').is(':checked')){
					province_select2_make_same();
					city_select2_make_same();
					districts_select2_make_same();
				}
			");
        echo $app->render();
    }

    public function reload_control_city() {
        $app = CApp::instance();
        $get = $_GET;
        $province_id = carr::get($get, 'province');

        $control_city_select = $app->add_field()->add_control('city', 'city-select')
                ->add_class("select-62hallfamily")
                ->custom_css('width', '100%')
                ->custom_css('height', '44px');
        $control_city_select->set_all(false);
        $control_city_select->set_province_id($province_id);
        $listener_change_city_select = $control_city_select->add_listener('change');
        $listener_change_city_select
                ->add_handler('reload')
                ->set_target('div_districts')
                ->set_url(curl::base() . self::__CONTROLLER . 'reload_control_districts')
                ->add_param_input(array('city'));
        $listener_ready_city_select = $control_city_select->add_listener('ready');
        $listener_ready_city_select
                ->add_handler('reload')
                ->set_target('div_districts')
                ->set_url(curl::base() . self::__CONTROLLER . 'reload_control_districts')
                ->add_param_input(array('city'));

        echo $app->render();
    }

    public function reload_control_city_new() {
        $app = CApp::instance();
        $get = $_GET;
        $province_id = carr::get($get, 'province');

        $control_city_select = $app->add_control('city', 'city-select')
                ->add_class("select-62hallfamily")
                ->custom_css('width', '100%')
                ->custom_css('height', '44px');
        $control_city_select->set_all(false);
        $control_city_select->set_province_id($province_id);
        $listener_change_city_select = $control_city_select->add_listener('change');
        $listener_change_city_select
                ->add_handler('reload')
                ->set_target('div_districts')
                ->set_url(curl::base() . self::__CONTROLLER . 'reload_control_districts_new')
                ->add_param_input(array('city'));
        $listener_ready_city_select = $control_city_select->add_listener('ready');
        $listener_ready_city_select
                ->add_handler('reload')
                ->set_target('div_districts')
                ->set_url(curl::base() . self::__CONTROLLER . 'reload_control_districts_new')
                ->add_param_input(array('city'));

        echo $app->render();
    }
    
    public function reload_districts($district_id = '') {
        $app = CApp::instance();
        $get = $_GET;
        $city_id = carr::get($get, 'city_pay');
        $page = carr::get($get, 'page');
        $from = 'pay';
        $addition_param = '';
        $raja_ongkir=carr::get($get,'raja_ongkir');
        if ($city_id == null) {
            $city_id = carr::get($get, 'city_shipping');
            $from = 'shipping';
        }

        $control_districts_select = $app->add_field()->set_label('Kecamatan')->add_control('districts_' . $from, 'districts-select')->add_class('width-40-percent');
        $control_districts_select->set_all(false);
        $control_districts_select->set_city_id($city_id);
        if (strlen($district_id) > 0) {
            $control_districts_select->set_value($district_id);
        }
        $listener_change_districts_select = $control_districts_select->add_listener('change');
        $listener_ready_districts_select = $control_districts_select->add_listener('ready');
        if ($from == 'pay') {
                $js_reload_districs_shipping="
                    if($('#check_same').is(':checked')){
                        districts_select2_make_same();
                    }
                ";
                $listener_change_districts_select->add_handler('custom')->set_js($js_reload_districs_shipping);
                $listener_ready_districts_select->add_handler('custom')->set_js($js_reload_districs_shipping);
        }
        if ($from == 'shipping') {
            if(ccfg::get('have_shipping_location_from_shipping_price')){
                if(strlen($raja_ongkir)==0){
                    $control_districts_select->set_same_location_shipping_price(true);
                }
            }
            
            if(strlen($raja_ongkir)==0){
                $listener_change_districts_select->add_handler('reload')->set_target('div_shipping_price')->add_param_input('total_weight')->add_param_input('page')->add_param_input('shipping_type_id')->add_param_input('districts_shipping')->set_url(curl::base() . 'reload/reload_shipping_price?' . $addition_param);
                $listener_ready_districts_select->add_handler('reload')->set_target('div_shipping_price')->add_param_input('total_weight')->add_param_input('page')->add_param_input('shipping_type_id')->add_param_input('districts_shipping')->set_url(curl::base() . 'reload/reload_shipping_price?' . $addition_param);
            }else{
                $listener_change_districts_select->add_handler('reload')->set_target('service_reload')->add_param_input('service_type')->add_param_input('city_shipping')->add_param_input('weight_shipping')->add_param_input('origin_shipping')->add_param_input('districts_shipping')->set_url(curl::base() . 'raja_ongkir/get_cost?' . $addition_param);
                $listener_ready_districts_select->add_handler('reload')->set_target('service_reload')->add_param_input('service_type')->add_param_input('city_shipping')->add_param_input('weight_shipping')->add_param_input('origin_shipping')->add_param_input('districts_shipping')->set_url(curl::base() . 'raja_ongkir/get_cost?' . $addition_param);
            }
        } 
        $js_reload="$.cresenity.reload('div_shipping_price','" . curl::base() . 'reload/reload_shipping_price?' . $addition_param . "','get',{'shipping_type_id':$.cresenity.value('#shipping_type_id'),'city_shipping':$.cresenity.value('#city_shipping')});";
        if(strlen($raja_ongkir)>0){
            $js_reload="$.cresenity.reload('service_reload','" . curl::base() . "raja_ongkir/get_cost','get',{'service_type':$.cresenity.value('#service_type'),'city_shipping':$.cresenity.value('#city_shipping'),'weight_shipping':$.cresenity.value('#weight_shipping'),'origin_shipping':$.cresenity.value('#origin_shipping')});";
        }
        
        $app->add_js("
            if($('#check_same').is(':checked')){
                province_select2_make_same();
                city_select2_make_same();
                districts_select2_make_same();
            }
        ");
        
        echo $app->render();
    }

    public function reload_control_districts() {
        $app = CApp::instance();
        $get = $_GET;
        $city_id = carr::get($get, 'city');

        $control_districts_select = $app->add_field()->add_control('districts', 'districts-select')
                ->add_class("select-62hallfamily")
                ->custom_css('width', '100%')
                ->custom_css('height', '44px');
        $control_districts_select->set_all(false);
        $control_districts_select->set_city_id($city_id);
        $listener_change_districts_select = $control_districts_select->add_listener('change');
        echo $app->render();
    }
    
    public function reload_control_districts_new() {
        $app = CApp::instance();
        $get = $_GET;
        $city_id = carr::get($get, 'city');

        $control_districts_select = $app->add_control('districts', 'districts-select')
                ->add_class("select-62hallfamily")
                ->custom_css('width', '100%')
                ->custom_css('height', '44px');
        $control_districts_select->set_all(false);
        $control_districts_select->set_city_id($city_id);
        $listener_change_districts_select = $control_districts_select->add_listener('change');
        echo $app->render();
    }

    public function reload_shipping_price() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $request = $_GET;
        $shipping_type_id = carr::get($request, 'shipping_type_id');
        $districts_id = carr::get($request, 'districts_shipping');
        $total_weight = carr::get($request, 'total_weight');
        $page = carr::get($request, 'page');
        $city_id = cdbutils::get_value('select city_id from districts where districts_id=' . $db->escape($districts_id));
        if ($total_weight < 1) {
            $total_weight = 1;
        }
        $data_shipping_price = shipping::get_shipping_price($shipping_type_id, $districts_id);
        if (isset($_GET['debug'])) {
            cdbg::var_dump($shipping_type_id);
            cdbg::var_dump($districts_id);
            cdbg::var_dump($data_shipping_price);
            exit;
        }
        $shipping_price = 'Maaf Pengiriman ke Wilayah Anda Tidak Tersedia';
        $text_shipping_price='-';
        $subtotal=0;
        $grandtotal=0;
        if ($page == 'product') {
            $cart = Cart62hall_Product::factory();
            $cart_list = $cart->get_cart();
            $total_weight = 0;
            foreach ($cart_list as $data_cart) {
                $total_weight += $data_cart['weight'] * $data_cart['qty'];
                $subtotal+=$data_cart['sub_total'];
            }
        }
        if (count($data_shipping_price) > 0) {
            $shipping_price = ctransform::format_currency($data_shipping_price->sell_price);
            $shipping_price = $data_shipping_price->sell_price;
            $shipping_weight=(ceil($total_weight / 1000));
            if($shipping_weight<=0){
                $shipping_weight=1;
            }
            $shipping_price = $shipping_price * $shipping_weight;
            $app->add_field()->set_label(clang::__('Biaya Pengiriman'))
                    ->add_control('shipping_price', 'text')
                    ->custom_css('width', '200px')
                    ->set_value('Rp. ' .$shipping_price)
                    ->set_attr('readonly', true);
            $text_shipping_price=ctransform::format_currency($shipping_price);
            $grandtotal=$subtotal+$shipping_price;
        }else{
            $field = $app->add_field()->set_label(clang::__('Biaya Pengiriman'))->add('<label>' . $shipping_price . '</label>');
            $app->add_control('shipping_price', 'hidden')
                    ->custom_css('width', '200px')
                    ->set_value('-');
            $text_shipping_price='-';
            $grandtotal=$subtotal;
        }
        
        $app->add_js("
            $('#shipping-cost').html('Rp ".$text_shipping_price."');
            $('#total-payment').html('Rp ".ctransform::format_currency($grandtotal)."');
        ");
        
        echo $app->render();
    }

    public function reload_stock_price_product($code = '', $product_id = '') {
        $app = CApp::instance();
        $request = $_GET;
        $attribute = array();
        foreach ($request as $key_req => $value_req) {
            if (substr($key_req, 0, 8) == 'att_cat_') {
                $attribute[substr($key_req, 8)] = $value_req;
            }
        }
        // get data product 
        $product_simple = product::get_simple_product_from_attribute($product_id, $attribute);
        if ($product_simple == null) {
            echo $app->render();
            return;
        }
        $data_product = product::get_product($product_simple->product_id);

        $stock = carr::get($data_product, 'stock', 0);
        $price = product::get_price($data_product['detail_price']);

        
        $availability = carr::get($data_product, 'is_available');
        $show_minimum_stock = carr::get($data_product, 'show_minimum_stock');
        $sku = carr::get($data_product, 'sku');
        $container_price = $app->add_div('stock-price')->add_class('container-price');
        $stock_info = '';

        if ($availability > 0) {
            if ($price['promo_price'] > 0) {
                $container_price->add_div()->add_class('detail-promo-price')->add('<strike>Rp '.ctransform::format_currency($price['price']).'</strike>');
                $container_price->add_div()->add_class('detail-normal-price')->add('Rp '.ctransform::format_currency($price['promo_price']));
            } else {
                $container_price->add_div()->add_class('detail-normal-price')->add('Rp '.ctransform::format_currency($price['price']));
            } 
            $stock_info = 'STOCK <span class="stock">'.$stock.' ITEM</span>';
            if ($show_minimum_stock > 0) {
                if ($stock > 0 && $stock <= $show_minimum_stock) {
                    $stock_info = 'TINGGAL <span class="stock">SISA '.$stock.' ITEM</span>';
                }
            }
        }

        $container_product_code = $container_price->add_div()->add_class('container-product-code');
        $table = "
        <table class='table-detail-product'>
            <tr>
                <td>".clang::__('Product Code')."</td>
                <td class='tb-colon'> : </td>
                <td>".$sku."</td>
            </tr>";
        
        // if (strlen($stock_info) > 0) {
        //     $table .= "<tr>
        //             <td>".clang::__('Availability')."</td>
        //             <td class='tb-colon'>:</td>
        //             <td>".$stock_info."</td>
        //         </tr>";            
        //     }    
        $table .= "
        </table>";
        $container_product_code->add($table);



        $listener = $app->add_listener('ready');

        $handler = $listener->add_handler('custom')
                ->set_js("
                            $('#qty').trigger('touchspin.updatesettings', 
                                {
                                // max: " . $stock . "
                                }
                             );
                             
                                 $('#div_qty').show();
                                 $('.btn-add-cart').show();
                                                          
                            ");

        //$app->add(cdbg::var_dump($data_simple_attribute));
        echo $app->render();
    }

    public function get_count_page($total, $limit_view = 0) {
        $limit = 24;
        if ($limit_view > 0) {
            $limit = $limit_view;
        }
        $count_page = (int) ($total / $limit);

        if ($total % $limit != 0) {
            $count_page += 1;
        }

        return $count_page;
    }

    public function get_start_end($page, $count, $limit_view = 0) {
        $limit = 24;
        if ($limit_view > 0) {
            $limit = $limit_view;
        }

        $start = 0;
        if ($page > 1) {
            $start = ($page - 1) * $limit;
        }
        $end = $limit - 1;
        if ($page > 1) {
            $end = ($page * $limit) - 1;
        }
        if ($end > $count - 1) {
            $end = $count - 1;
        }

        return array('start' => $start, 'end' => $end);
    }

    public function reload_product_filter() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $get = $_GET;
 
        $filter_page = carr::get($get, 'filter_page', '');
        
        $type = carr::get($get, 'deal_type', '');
        
// filter category id
        $filter_page_id = carr::get($get, 'filter_product_category_id','');

        $page = 1;
        if (isset($get['page'])) {
            $page = $get['page'];
        }
        $visibility = carr::get($get, 'visibility');
        $filter_page = carr::get($get, 'filter_page');
        $filter_name = carr::get($get, 'filter_name');
        $filter_category_lft = carr::get($get, 'filter_category_lft');
        $filter_category_rgt = carr::get($get, 'filter_category_rgt');
        $view_limit = carr::get($get, 'view_limit', 0);

        $param_input = $get;
        unset($param_input['capp_current_container_id']);

        $list_product_id = carr::get($get, 'arr_product_id');
        $list_filter = carr::get($get, 'list_filter', '');


        $list_filter_price = carr::get($get, 'price', NULL);
        if (strlen($list_filter_price) > 0) {
            $filter_price = explode(',', $list_filter_price);
        }
        if (empty($filter_price)) {
            $filter_price = array();
        }
        $filter_product = carr::get($get, 'sortby');
        $arr_attribute = array();

        foreach ($param_input as $cat => $value) {
            if (substr($cat, 0, strlen('catalog_filter_')) == 'catalog_filter_') {
                $cat_attr = substr($cat, strlen('catalog_filter_'));
                $cat_arr = explode('_', $cat_attr);
                $cat_id = carr::get($cat_arr, 0);
                $attr_id = carr::get($cat_arr, 1);
                if (strlen($value) > 0 && $value > 0) {
                    if (!isset($arr_attribute[$cat_id])) {
                        $arr_attribute[$cat_id] = array();
                    }

                    $arr_attribute[$cat_id][] = $value;
                }
            }
        }

        $arr_price = array();
        for ($i=0; $i <= 5; $i++) {
            $price_get = carr::get($param_input, 'price_filter_'.$i);  
            if (strlen($price_get) > 0) {
                $arr_price[] = $price_get;
            }
        }

        $options = array(
            "attributes" => $arr_attribute,
            "min_price" => carr::get($filter_price, 0),
            "max_price" => carr::get($filter_price, 1),
            "arr_price" => $arr_price,
            "visibility" => $visibility,
            "product_type" => $filter_page,
            "search" => $filter_name,
            "product_category_lft" => $filter_category_lft,
            "product_category_rgt" => $filter_category_rgt,
            "sortby" => $filter_product,
            //filter category id
            "product_category_id" => $filter_page_id
        );

        $total_product = product::get_count($options);

        $container_product = $app->add_div()->add_class('row');;

        $arr_start_end = $this->get_start_end($page, $total_product, $view_limit);

        $options["limit_start"] = carr::get($arr_start_end, "start", 0);
        $options["limit_end"] = carr::get($arr_start_end, "end", 10);
        
        if ($filter_page == 'luckyday') {
            $options["stock"] = 1;
        }

        $result = product::get_result($options);

        $index = $arr_start_end['start'];

        $container_product_row = $app->add_div()->add_class("row");

        $result_data_merge = array();
          foreach ($result->result_array() as $key => $value) {
             $result_data_merge[] =  $this->get_merge_data($value);
            }  
        
        $element_deal = $container_product_row->add_element('62hall-slide-product')->add_class('col-md-12');
            $element_deal->set_list_item($result_data_merge);
            $element_deal->set_limit($view_limit);


        // pagination
        $pagination_wrap = $app->add_div()->add_class('row pagination-wrap');
        $paginationcontainer = $pagination_wrap->add_div()
                ->add_class('pagination-category pagination col-md-12');

        $count_page = $this->get_count_page($total_product, $view_limit);

        unset($param_input['page']);
        unset($param_input['visibility']);
        unset($param_input['filter_page']);
        unset($param_input['filter_name']);
        unset($param_input['filter_category_lft']);
        unset($param_input['filter_category_rgt']);
        unset($param_input['sortby']);
        unset($param_input['view_limit']);
        
        //$param_input = array_keys($param_input);
        $param_input_query = http_build_query($param_input);
        $product_category = cdbutils::get_value('SELECT url_key FROM product_category WHERE product_category_id = ' . $db->escape($filter_page_id));
        $url = curl::base() . 'search?keyword='.$filter_name.'&category='.$product_category.'&view_limit='.$view_limit.'&sortby='.$filter_product;
        
        if ($count_page > 0) {
            if ($page > 1) {
                $previus_page = $page - 1;
                // $paginationcontainer->add_action()
                //         ->set_label('<i class="fa fa-caret-left"></i>')
                //         ->add_class('font-gray-dark')
                //         ->add_listener('click')
                //         ->add_handler('reload')
                //         ->set_target('page-product-category')
                //         //->add_param_input($param_input)
                //         ->set_url(curl::base() . "reload/reload_product_filter/?view_limit=".$view_limit."&sortby=".$filter_product."&source=paging&page=1&visibility=" . $visibility . '&filter_page=' . $filter_page . '&filter_name=' . urlencode(addslashes($filter_name)) . '&filter_category_lft=' . $filter_category_lft . '&filter_category_rgt=' . $filter_category_rgt.'&'.$param_input_query);
                // $paginationcontainer->add_action()
                //         ->set_label('Previous')
                //         ->add_class('font-gray-dark')
                //         ->add_listener('click')
                //         ->add_handler('reload')
                //         ->set_target('page-product-category')
                //         //->add_param_input($param_input)
                //         ->set_url(curl::base() . "reload/reload_product_filter/?view_limit=".$view_limit."&sortby=".$filter_product."&source=paging&page=" . $previus_page . "&visibility=" . $visibility . '&filter_page=' . $filter_page . '&filter_name=' . urlencode(addslashes($filter_name)) . '&filter_category_lft=' . $filter_category_lft . '&filter_category_rgt=' . $filter_category_rgt.'&'.$param_input_query);
                $paginationcontainer->add_action()
                        ->set_label('<i class="fa fa-caret-left"></i>')
                        ->set_link($url . '&page_view=1')
                        ->add_class('font-gray-dark');
                $paginationcontainer->add_action()
                        ->set_label('Previous')
                        ->set_link($url . '&page_view=' . $previus_page)
                        ->add_class('font-gray-dark');
            }


            $start_page = $page - 5;
            if ($start_page < 1) {
                $start_page = 1;
            }

            $end_page = $page + 5;
            if ($end_page > $count_page) {
                $end_page = $count_page;
            }

            for ($i = $start_page; $i <= $end_page; $i++) {
                $this_page = $page;
                $class = NULL;
                $class = 'font-black';
                if ($this_page == $i) {
                    $class = 'btn-pagination';
                }
                // $paginationcontainer->add_action()
                //         ->set_label($i)
                //         ->add_class($class)
                //         ->add_listener('click')
                //         ->add_handler('reload')
                //         ->set_target('page-product-category')
                //         //->add_param_input($param_input)
                //         ->set_url(curl::base() . "reload/reload_product_filter/?view_limit=".$view_limit."&sortby=".$filter_product."&source=paging&page=" . $i . "&visibility=" . $visibility . '&filter_page=' . $filter_page . '&filter_name=' . urlencode(addslashes($filter_name)) . '&filter_category_lft=' . $filter_category_lft . '&filter_category_rgt=' . $filter_category_rgt.'&'.$param_input_query);
                $paginationcontainer->add_action()
                        ->set_label($i)
                        ->set_link($url . '&page_view=' . $i)
                        ->add_class($class);
            }
            if ($page < $count_page) {
                $next_page = $page + 1;
                // $paginationcontainer->add_action()
                //         ->set_label('Next')
                //         ->add_class('font-gray-dark')
                //         ->add_listener('click')
                //         ->add_handler('reload')
                //         ->set_target('page-product-category')
                //         //->add_param_input($param_input)
                //         ->set_url(curl::base() . "reload/reload_product_filter/?view_limit=".$view_limit."&sortby=".$filter_product."&source=paging&page=" . $next_page . "&visibility=" . $visibility . '&filter_page=' . $filter_page . '&filter_name=' . urlencode(addslashes($filter_name)) . '&filter_category_lft=' . $filter_category_lft . '&filter_category_rgt=' . $filter_category_rgt.'&'.$param_input_query);
                // $paginationcontainer->add_action()
                //         ->set_label('<i class="fa fa-caret-right"></i>')
                //         ->add_class('font-gray-dark')
                //         ->add_listener('click')
                //         ->add_handler('reload')
                //         ->set_target('page-product-category')
                //         //->add_param_input($param_input)
                //         ->set_url(curl::base() . "reload/reload_product_filter/?view_limit=".$view_limit."&sortby=".$filter_product."&source=paging&page=" . $count_page . "&visibility=" . $visibility . '&filter_page=' . $filter_page . '&filter_name=' . urlencode(addslashes($filter_name)) . '&filter_category_lft=' . $filter_category_lft . '&filter_category_rgt=' . $filter_category_rgt.'&'.$param_input_query);
                $paginationcontainer->add_action()
                        ->set_label('Next')
                        ->set_link($url . '&page_view=' . $next_page)
                        ->add_class('font-gray-dark');
                $paginationcontainer->add_action()
                        ->set_label('<i class="fa fa-caret-right"></i>')
                        ->set_link($url . '&page_view=' . $count_page)
                        ->add_class('font-gray-dark');
            }
        }

        $total_page = $view_limit;
        if ($total_page == 0) {
            $total_page = 10;
        }
        
        $app->add_js("
            jQuery('.container-pagination-top').html('');
            jQuery('.pagination-wrap').clone(true).appendTo('.container-pagination-top');

//            jQuery('.txt-page-show').text('1-".$total_page."');
            jQuery('.txt-page-show').text('".++$options["limit_start"]." - ".++$options["limit_end"]."');
            jQuery('.txt-count-product').text('".ctransform::format_currency($total_product)."');
        ");

        $app->add_control('page', 'hidden')
                ->set_value($page);

        $app->add_control('arr_product_id', 'hidden')
                ->set_value($list_product_id);

        $app->add_control('list_filter', 'hidden')
                ->set_value($list_filter);

        if ($total_product == 0) {
            $message = $app->add_div()
                    ->add_class('font-size16')
                    ->add(clang::__('Produk tidak ditemukan'));
        }

        echo $app->render();
    }
    
    public function reload_product_filter_deal() {
        $app = CApp::instance();
        $get = $_GET;
        $org_id = CF::org_id();
        $deal_type = carr::get($get, 'deal_type');
        
//        cdbg::var_dump($get);
//        die(__CLASS__ . ' - ' . __FUNCTION__);

        $filter_page = carr::get($get, 'filter_page', '');

        $page = 1;
        if (isset($get['page'])) {
            $page = $get['page'];
        }
        $visibility = carr::get($get, 'visibility');
        $filter_page = carr::get($get, 'filter_page');
        
        $filter_name = carr::get($get, 'filter_name');
        $filter_category_lft = carr::get($get, 'filter_category_lft');
        $filter_category_rgt = carr::get($get, 'filter_category_rgt');
        $view_limit = carr::get($get, 'view_limit', 0);

        $param_input = $get;
        unset($param_input['capp_current_container_id']);

        $list_product_id = carr::get($get, 'arr_product_id');
        $list_filter = carr::get($get, 'list_filter', '');


        $list_filter_price = carr::get($get, 'price', NULL);
        if (strlen($list_filter_price) > 0) {
            $filter_price = explode(',', $list_filter_price);
        }
        if (empty($filter_price)) {
            $filter_price = array();
        }
        $filter_product = carr::get($get, 'sortby');
        $arr_attribute = array();

        foreach ($param_input as $cat => $value) {
            if (substr($cat, 0, strlen('catalog_filter_')) == 'catalog_filter_') {
                $cat_attr = substr($cat, strlen('catalog_filter_'));
                $cat_arr = explode('_', $cat_attr);
                $cat_id = carr::get($cat_arr, 0);
                $attr_id = carr::get($cat_arr, 1);
                if (strlen($value) > 0 && $value > 0) {
                    if (!isset($arr_attribute[$cat_id])) {
                        $arr_attribute[$cat_id] = array();
                    }

                    $arr_attribute[$cat_id][] = $value;
                }
            }
        }

        $arr_price = array();
        for ($i=0; $i <= 5; $i++) {
            $price_get = carr::get($param_input, 'price_filter_'.$i);  
            if (strlen($price_get) > 0) {
                $arr_price[] = $price_get;
            }
        }

        $options = array(
            "attributes" => $arr_attribute,
            "min_price" => carr::get($filter_price, 0),
            "max_price" => carr::get($filter_price, 1),
            "arr_price" => $arr_price,
            "visibility" => $visibility,
            "product_type" => $filter_page,
            "search" => $filter_name,
            "product_category_lft" => $filter_category_lft,
            "product_category_rgt" => $filter_category_rgt,
            "sortby" => $filter_product,
        );

        //$total_product = product::get_count($options);
        
        $result = deal::get_deal($org_id, $filter_page, $deal_type,array('sortby'=>'priority'));
//        cdbg::var_dump($result);
    
        $total_product = count($result);

        $container_product = $app->add_div()->add_class('row');;

        $arr_start_end = $this->get_start_end($page, $total_product, $view_limit);

        $options["limit_start"] = carr::get($arr_start_end, "start", 0);
        $options["limit_end"] = carr::get($arr_start_end, "end", 10);
        $options["sortby"] = 'priority';
        
        if ($filter_page == 'luckyday') {
            $options["stock"] = 1;
        }

        $result = deal::get_deal($org_id, $filter_page, $deal_type, $options);
        
        //$result = product::get_result($options);

        
        $index = $arr_start_end['start'];
        $container_product_row = $app->add_div()->add_class("row");

        //$data_list = $result->result_array();

        $element_deal = $container_product_row->add_element('62hall-slide-product')->add_class('col-md-12');
            $element_deal->set_list_item($result);
            $element_deal->set_limit($view_limit);


        // pagination
        $pagination_wrap = $app->add_div()->add_class('row pagination-wrap');
        $paginationcontainer = $pagination_wrap->add_div()->add_class('pagination-category pagination col-md-12');

        $count_page = $this->get_count_page($total_product, $view_limit);

        unset($param_input['page']);
        unset($param_input['visibility']);
        unset($param_input['filter_page']);
        unset($param_input['filter_name']);
        unset($param_input['filter_category_lft']);
        unset($param_input['filter_category_rgt']);
        unset($param_input['sortby']);
        unset($param_input['view_limit']);
        unset($param_input['deal_type']);

        $param_input = array_keys($param_input);
        $url = curl::base() . 'show_product/show/'.$deal_type.'?view_limit='.$view_limit.'&sortby='.$filter_product;
        
//        cdbg::var_dump($arr_start_end);
//        
//        cdbg::var_dump($org_id);
//        cdbg::var_dump($filter_page);
//        cdbg::var_dump($deal_type);
//        
//        cdbg::var_dump($total_product);
//        cdbg::var_dump($view_limit);
//        cdbg::var_dump($count_page);
//        
//        
//        cdbg::var_dump($param_input);
//        die(__CLASS__ . ' - ' . __FUNCTION__);

        if ($count_page > 0) {
            if ($page > 1) {
                $previus_page = $page - 1;
                // $paginationcontainer->add_action()
                //         ->set_label('<i class="fa fa-caret-left"></i>')
                //         ->add_class('font-gray-dark')
                //         ->add_listener('click')
                //         ->add_handler('reload')
                //         ->set_target('page-product-category')
                //         ->add_param_input($param_input)
                //         ->set_url(curl::base() . "reload/reload_product_filter_deal/?view_limit=".$view_limit."&sortby=".$filter_product."&source=paging&page=1&visibility=" . $visibility . '&filter_page=' . $filter_page . '&filter_name=' . urlencode(addslashes($filter_name)) . '&filter_category_lft=' . $filter_category_lft . '&filter_category_rgt=' . $filter_category_rgt.'&deal_type='.$deal_type.'');
                // $paginationcontainer->add_action()
                //         ->set_label('Previous')
                //         ->add_class('font-gray-dark')
                //         ->add_listener('click')
                //         ->add_handler('reload')
                //         ->set_target('page-product-category')
                //         ->add_param_input($param_input)
                //         ->set_url(curl::base() . "reload/reload_product_filter_deal/?view_limit=".$view_limit."&sortby=".$filter_product."&source=paging&page=" . $previus_page . "&visibility=" . $visibility . '&filter_page=' . $filter_page . '&filter_name=' . urlencode(addslashes($filter_name)) . '&filter_category_lft=' . $filter_category_lft . '&filter_category_rgt=' . $filter_category_rgt.'&deal_type='.$deal_type.'');
                $paginationcontainer->add_action()
                        ->set_label('<i class="fa fa-caret-left"></i>')
                        ->set_link($url . '&page=1')
                        ->add_class('font-gray-dark');
                $paginationcontainer->add_action()
                        ->set_label('Previous')
                        ->set_link($url . '&page=' . $previus_page)
                        ->add_class('font-gray-dark');
            }


            $start_page = $page - 5;
            if ($start_page < 1) {
                $start_page = 1;
            }

            $end_page = $page + 5;
            if ($end_page > $count_page) {
                $end_page = $count_page;
            }

            for ($i = $start_page; $i <= $end_page; $i++) {
                $this_page = $page;
                $class = NULL;
                $class = 'font-black';
                if ($this_page == $i) {
                    $class = 'btn-pagination';
                }
                // $paginationcontainer->add_action()
                //         ->set_label($i)
                //         ->add_class($class)
                //         ->add_listener('click')
                //         ->add_handler('reload')
                //         ->set_target('page-product-category')
                //         ->add_param_input($param_input)
                //         ->set_url(curl::base() . "reload/reload_product_filter_deal/?view_limit=".$view_limit."&sortby=".$filter_product."&source=paging&page=" . $i . "&visibility=" . $visibility . '&filter_page=' . $filter_page . '&filter_name=' . urlencode(addslashes($filter_name)) . '&filter_category_lft=' . $filter_category_lft . '&filter_category_rgt=' . $filter_category_rgt . '&deal_type='.$deal_type.'');
                $paginationcontainer->add_action()
                        ->set_label($i)
                        ->set_link($url . '&page=' . $i)
                        ->add_class($class);
            }
            if ($page < $count_page) {
                $next_page = $page + 1;
                // $paginationcontainer->add_action()
                //         ->set_label('Next')
                //         ->add_class('font-gray-dark')
                //         ->add_listener('click')
                //         ->add_handler('reload')
                //         ->set_target('page-product-category')
                //         ->add_param_input($param_input)
                //         ->set_url(curl::base() . "reload/reload_product_filter_deal/?view_limit=".$view_limit."&sortby=".$filter_product."&source=paging&page=" . $next_page . "&visibility=" . $visibility . '&filter_page=' . $filter_page . '&filter_name=' . urlencode(addslashes($filter_name)) . '&filter_category_lft=' . $filter_category_lft . '&filter_category_rgt=' . $filter_category_rgt. '&deal_type='.$deal_type.'');
                // $paginationcontainer->add_action()
                //         ->set_label('<i class="fa fa-caret-right"></i>')
                //         ->add_class('font-gray-dark')
                //         ->add_listener('click')
                //         ->add_handler('reload')
                //         ->set_target('page-product-category')
                //         ->add_param_input($param_input)
                //         ->set_url(curl::base() . "reload/reload_product_filter_deal/?view_limit=".$view_limit."&sortby=".$filter_product."&source=paging&page=" . $count_page . "&visibility=" . $visibility . '&filter_page=' . $filter_page . '&filter_name=' . urlencode(addslashes($filter_name)) . '&filter_category_lft=' . $filter_category_lft . '&filter_category_rgt=' . $filter_category_rgt. '&deal_type='.$deal_type.'');
                $paginationcontainer->add_action()
                        ->set_label('Next')
                        ->set_link($url . '&page=' . $next_page)
                        ->add_class('font-gray-dark');
                $paginationcontainer->add_action()
                        ->set_label('<i class="fa fa-caret-right"></i>')
                        ->set_link($url . '&page' . $count_page)
                        ->add_class('font-gray-dark');
            }
        }

        $total_page = $view_limit;
        if ($total_page == 0) {
            $total_page = 10;
        }
        $app->add_js("
            jQuery('.container-pagination-top').html('');
            jQuery('.pagination-wrap').clone(true).appendTo('.container-pagination-top');

            jQuery('.txt-page-show').text('1-".$total_page."');
            jQuery('.txt-count-product').text('".ctransform::format_currency($total_product)."');
        ");

        $app->add_control('page', 'hidden')
                ->set_value($page);

        $app->add_control('arr_product_id', 'hidden')
                ->set_value($list_product_id);

        $app->add_control('list_filter', 'hidden')
                ->set_value($list_filter);

        if ($total_product == 0) {
            $message = $app->add_div()
                    ->add_class('font-size16')
                    ->add(clang::__('Produk tidak ditemukan'));
        }

        echo $app->render();
    }

    public function reload_paging() {
        
    }
    
    public function dummy_product_result() {
        $result = array();
        
        $dummy_product = array(
            "product_id" => "192060",
            "product_type_id" => "5",
            "product_category_id" => "183",
            "vendor_id" => "104",
            "country_manufacture_id" => "94",
            "code" => "nmccompany",
            "name" => "Company Card",
            "url_key" => "company-card",
            "new_product_date" => "2016-09-15",
            "currency" => "idr",
            "purchase_price_nta" => null,
            "purchase_price" => null,
            "sell_price_nta" => "2500000",
            "sell_price" => "2600000",
            "sell_price_start" => null,
            "sell_price_end" => null,
            "ho_upselling" => "0",
            "ho_commission" => "0",
            "ho_commission_type" => null,
            "weight" => "1",
            "stock" => "0",
            "description" => "lorem ipsum",
            "overview" => "<p>Lorem ipsum rincian company card<\/p>\n",
            "spesification" => "",
            "faq" => "",
            "is_active" => "1",
            "is_available" => "1",
            "is_stock" => "0",
            "is_hot_item" => "0",
            "show_minimum_stock" => "0",
            "alert_minimum_stock" => "0",
            "parent_id" => null,
            "sku" => "nmccompany1",
            "product_data_type" => "simple",
            "status_product" => "FINISHED",
            "is_selection" => "0",
            "filename" => "company-card.png",
            "image_name" => "default_image_ProductImageParent_20160830_company-card.png",
            "file_path" => "http://admin.mybigmall.local/res/show/NjI2MlJXUFNDXkJtX19XVVNtZkBZVkNRQntbU1FXZlNEV1hGaQAGAwACDgEGbVVdW0JXXE8fVVNEVhhCWFU=",
            "visibility" => "not_visibility_individually",
            "featured_image" => null,
            "description_voucher" => null,
            "is_voucher" => "0",
            "is_generate_code" => "0",
            "is_email" => "0",
            "image_fixed" => "0",
            "status" => "1",
            "quota" => "2000",
            "slot" => "450",
        );
        
        for($i=0; $i < 12; $i++){
            $dummy_product['product_id'] = $i;
            $dummy_product['quota'] = rand(1000, 1500);
            $dummy_product['slot'] = rand(0, 1000);
            $result[] = (object) $dummy_product;
        }
        
        return $result;
    }
    
    public function reload_video(){
        $app=CApp::instance();
        $get=$_GET;
        
        $url_key=carr::get($get,'url_key');
        $url_prev=carr::get($get,'url_prev');
        $url_next=carr::get($get,'url_next');
        $el_cms_video=$app->add_element('62hall-cms-video','cms_video');
        $el_cms_video->set_url_key($url_key);
        echo $app->render();
    }
    
    public function reload_video_gallery(){
        $app=CApp::instance();
        
        $get = $_GET;
        $active = carr::get($get, 'active');
        
        $el_video_gallery=$app->add_element('62hall-cms-video-gallery','cms_video_gallery');
        $el_video_gallery->set_active($active);
        
        echo $app->render();
    }
    public function get_merge_data($obj_data = null){
//        if ($data != null){
      $db = CDatabase::instance();
//           $obj_data = $data[0];
//        cdbg::var_dump($obj_data);
//        die;
           $product_id = cobj::get($obj_data,'product_id');
           $q = "select
                                    pg.*,
                                    pgd.*
                            from
                                    promo_group as pg
                                    inner join promo_group_detail as pgd on pgd.promo_group_id=pg.promo_group_id
                            where
                                    pg.status>0
                                    and pgd.status>0
                                    and pg.status_confirm='confirmed'
									and pg.is_active>0
                                    and date(pg.date_start)<=" . $db->escape(date('Y-m-d')) . "
                                    and date(pg.date_end)>=" . $db->escape(date('Y-m-d')) . "
                                    and pgd.product_id=" . $product_id . "
                            order by
                                    pg.priority asc";
           $q_promo = cdbutils::get_row($q);
           $data_promo = array(
                        'detail_price'=>array(
                            'promo_value'=>  cobj::get($q_promo, 'value'),
                            'promo_type'=>  cobj::get($q_promo, 'type'),
                            'promo_start_date'=>  cobj::get($q_promo, 'date_start'),
                            'promo_end_date'=>  cobj::get($q_promo, 'date_end'),
                            'promo_value'=>  cobj::get($q_promo, 'value'),
                            'ho_sell_price'=>  cobj::get($obj_data, 'ho_sell_price'),
                            'channel_sell_price'=>  cobj::get($obj_data, 'channel_sell_price'),
                        )
                   );
           $obj_merged = (object) array_merge((array) $obj_data, (array) $data_promo);
         
//           $data_merg = array_merge($obj_data,$try);
            
//        }
           return $obj_merged;
    }
}
