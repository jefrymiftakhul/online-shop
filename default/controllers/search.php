<?php

/**
 *
 * @author Riza 
 * @since  Nov 19, 2015
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class Search_Controller extends SixtyTwoHallFamilyController {

    private $type_select = 'select';
    private $type_image = 'image';

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $request = array_merge($_POST, $_GET);

        $keyword = carr::get($request, 'keyword', NULL);
        $category = carr::get($request, 'category', NULL);
      
        $page = carr::get($request, 'page', NULL);
        $view_limit = carr::get($request, 'view_limit', 10);
        $page_view = carr::get($request, 'page_view', 1);
        $filter_product = carr::get($request, 'sortby', 'diskon');
      
        $product_category_id = '';

        $session = Session::instance();
        
        // log62::search($keyword,$session->get('member_id'));
        
        $app = CApp::instance();
        $user = $app->user();
        $db = CDatabase::instance();
        $org_id = $this->org_id();
        $link_list = array();
        $menu_list = array();
        $url_page = '';
        
        // SEARCH 
//        if ($page == 'product') {
//            // SHOPPING CART 
//            $app->add_listener('ready')
//                    ->add_handler('reload')
//                    ->set_target('shopping-cart')
//                    ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");
//        }
                    $app->add_listener('ready')
                    ->add_handler('reload')
                    ->set_target('shopping-cart')
                    ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

        $options = array(
            'product_type' => $page,
            'search' => $keyword,
            'visibility' => 'search',
        );

        if ($category != NULL) {
            //$product_category_id = cdbutils::get_value("SELECT product_category_id FROM product_category WHERE status > 0 and org_id = ".$org_id." and code =".$db->escape($category)." LIMIT 1");
            $product_category_id = cdbutils::get_value("SELECT product_category_id FROM product_category WHERE status > 0 and org_id = ".$org_id." and url_key =".$db->escape($category)." LIMIT 1");
                 
            $options['product_category_id'] = $product_category_id;
        }
        
        $total_product = product::get_count($options);

        $arr_sort = array(
            'diskon'    => clang::__('Diskon'),
            'min_price' => clang::__('Termurah'),
            'max_price' => clang::__('Termahal'),
            'is_new'    => clang::__('Terbaru'),
            'popular'   => clang::__('Popular'),
            'A-Z'       => clang::__('A-Z'),
            'Z-A'       => clang::__('Z-A'),
        );

        $arr_show = array(
            '10' => 10,
            '15' => 15,
            '20' => 20,
            '25' => 25,
            '30' => 30
        );

        $container = $app->add_div()->add_class('container container-lasvegas');
        $product_total = $container->add_div()->add_class('container-product-total');
        $row = $product_total->add_div()->add_class('row');
        $col1 = $row->add_div()->add_class('col-md-6');
        $col2 = $row->add_div()->add_class('col-md-6');
        $col2->add_div()->add_class('txt-product-total')->add(clang::__('There are').' <span class="txt-count-product">0</span> '.clang::__('Products'));
        $container->add_div()->add_class('separate-show-product');

        $container_sort = $container->add_div()->add_class('container-sort');
        $row = $container_sort->add_div()->add_class('row');
        $col1 = $row->add_div()->add_class('col-md-8');
        $col2 = $row->add_div()->add_class('col-md-4');

        $content_sort = $col1->add_div()->add_class('content-sort');
        $content_show = $col2->add_div()->add_class('content-show');

        $content_sort->add_div()->add_class('content-sort-element cs-text-sortby')->add(clang::__('Sort by'));

        $control_sortby = $content_sort->add_div()->add_class('content-sort-element cs-control-sortby');
        $control_sortby->add_div()->add_control('category_sort_by', 'select')->set_list($arr_sort)->set_value($filter_product)->add_class('select-imlasvegas-filter');
        
        $content_sort->add_div()->add_class('content-sort-element cs-text-show')->add(clang::__('Show'));
        
        $control_show = $content_sort->add_div()->add_class('content-sort-element cs-control-show');
        $control_show->add_div()->add_control('category_sort_view', 'select')->set_list($arr_show)->set_value($view_limit)->add_class('select-imlasvegas-filter');
        
        $content_show->add(clang::__('Showing').' <span class="txt-page-show">0</span> '.clang::__('Product'));

        $container_product_show = $container->add_div()->add_class('container-product-show');
        $div_product_result = $container_product_show->add_div('page-product-category');

        // $div_product_result = $div_search_right->add_div('page-product-category');
                
        if ($total_product == 0) {
 
            $div_product_result->add_div()->add_element('62hall-form-request');    
        }
        else {
            
            $div_product_result->add_listener('ready')
                    ->add_handler('reload')
                    ->set_target('page-product-category')
                    ->set_url(curl::base() . "reload/reload_product_filter?view_limit=".$view_limit."&page=".$page_view."&source=search&visibility=search&sortby=".$filter_product."&filter_page=" . $page . '&filter_product_category_id='.$product_category_id.'&filter_name=' . urlencode($keyword));
        }

        $data_addition = '{}';
        $js = "
            function reload_filter() {
                var txt_sort_by = jQuery('#category_sort_by').val();
                var txt_sort_view = jQuery('#category_sort_view').val();
                
                // $.cresenity.reload('page-product-category', '".curl::httpbase()."reload/reload_product_filter?view_limit='+txt_sort_view+'&sortby='+txt_sort_by+'&source=filter_reload&filter_product_category_id=".$product_category_id."', 'get');
                document.location.href = '". curl::httpbase() ."search?keyword=" . $keyword . "&category=" . $category . "&view_limit='+txt_sort_view+'&sortby='+txt_sort_by;
            }
            
            jQuery('.select-imlasvegas-filter').change(function() {
                reload_filter();
            });
        ";

        $app->add_js($js);

        echo $app->render();
    }

    private function generate_list($type, $data = array()) {
        $list = array();

        foreach ($data as $key => $row_data) {
            if ($type == $this->type_select) {
                $value = carr::get($row_data, 'attribute_key');
            }
            if ($type == $this->type_image) {
                $value['key'] = carr::get($row_data, 'attribute_key');
                $value['value'] = carr::get($row_data, 'file_path');
            }

            $list[$key] = $value;
        }

        return $list;
        
    }

    public function generate_attribute($name, $product_id) {
        $app = CApp::instance();

        $request = $_GET;

        $param = array();
        foreach ($request as $key => $row_request) {
            if ($key != 'capp_current_container_id') {
                $param[] = $row_request;
            }
        }

        $attribute_category = product::get_attribute_category_all($product_id);
        $attibute_data = $attribute_category[$name];

        $id = carr::get($attibute_data, 'attribute_category_id');
        $type = carr::get($attibute_data, 'attribute_category_type');
        $prev = carr::get($attibute_data, 'prev_attribute_category');
        $next = carr::get($attibute_data, 'next_attribute_category');

        $attribute = product::get_attribute_list($product_id, $id, $param);

        $list = $this->generate_list($type, $attribute['data']);

        foreach ($prev as $row_prev) {
            $arr_param[] = $name;
        }
        if ($type == $this->type_select) {
            $div_container = $app->add_div('container-' . $name);
            $select = $div_container->add_field()
                    ->add_control($name, 'select')
                    ->add_class('select-62hallfamily margin-bottom-10')
                    ->custom_css('margin-left', '15px')
                    ->custom_css('width', '130px')
                    ->set_list($list);

            if (!empty($next) and count($list) > 0) {
                $listener = $select->add_listener('ready');

                $handler = $listener->add_handler('reload')
                        ->set_target('container-' . $next)
                        ->add_param_input($arr_param)
                        ->set_url(curl::base() . 'product/generate_attribute/' . $next . '/' . $product_id);
                $listener = $select->add_listener('change');

                $handler = $listener->add_handler('reload')
                        ->set_target('container-' . $next)
                        ->add_param_input($arr_param)
                        ->set_url(curl::base() . 'product/generate_attribute/' . $next . '/' . $product_id);
            }
        } else if ($type == $this->type_image) {
            $div_container = $app->add_div('container-' . $name);
            $index = 1;
            foreach ($list as $key => $value) {
                $active = NULL;
                if ($index == 1) {
                    $active = 'active';
                    $div_container->add_control($name, 'hidden')
                            ->set_value($key);
                }
                $image = $div_container->add_div($key)
                        ->add_class('btn-colors margin-bottom-10 link ' . $active)
                        ->custom_css('display', 'inline-block')
                        ->custom_css('padding', '2px')
                        ->add('<img src="' . $value['value'] . '" style="width:20px; height:20px"/>');

                $index++;
            }

            if (!empty($next) and count($list) > 0) {
                $listener = $image->add_listener('ready');

                $handler = $listener->add_handler('reload')
                        ->set_target('container-' . $next)
                        ->add_param_input($arr_param)
                        ->set_url(curl::base() . 'product/generate_attribute/' . $next . '/' . $product_id);

                $listener = $image->add_listener('click');

                $handler = $listener->add_handler('reload')
                        ->set_target('container-' . $next)
                        ->add_param_input($arr_param)
                        ->set_url(curl::base() . 'product/generate_attribute/' . $next . '/' . $product_id);
            }
        }

        echo $app->render();
    }

}
