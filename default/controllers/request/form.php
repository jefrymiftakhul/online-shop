<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  May 10, 2016
     */
    class Form_Controller extends ProductController {
        
        public function __construct() {
            parent::__construct();
        }
        
        public function index(){
            $app = CApp::instance();

            $wrapper_container = $app->add_div()->add_class('container');
            $wrapper = $wrapper_container->add_div()->add_class('wrapper-request-form');
            
            $wrapper_row = $wrapper->add_div()->add_class('row');
            $left_container = $wrapper_row->add_div()->add_class('col-md-5 left-container');
            $right_container = $wrapper_row->add_div()->add_class('col-md-7 right-container');

            $image_send = curl::base() . 'application/62hallfamily/default/media/img/product_request.png';
            $left_container->add('<img src="' . $image_send . '" class="img-request-form" />');

            $right_container->add_element('62hall-form-request');

            echo $app->render();
        }
    }
    