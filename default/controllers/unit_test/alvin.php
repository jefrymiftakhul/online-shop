<?php

class Alvin_Controller extends SixtyTwoHallFamilyController {

    public function index() {
        cdbg::var_dump(image::get_image_url('tema5_image_ProductImageParent_20170706_20170706028ee724157b05d04e7bdcf237d12e60.jpg', '166x165'));
    }

    public function test_resend() {
    	$err_code = 0;
    	$err_message = '';
    	try{
    	    if(ccfg::get('new_email')){
    	        email::send('IMRegister', '84013');
    	    }else{
    	        $this->send_verification('84013');
    	    }
    	} catch (Exception $ex) {
    	    $err_code++;
    	    $err_message= $ex;
    	}
    	if ($err_code > 0) {
    		echo $err_message;
    	}
    }

    public function resend_verification() {
    	$db = CDatabase::instance();
    	$err_code = 0;
    	$err_message = '';
    	$q = "
    		SELECT
    			member_id
    		FROM
    			member
    		WHERE
    			created >= NOW() - INTERVAL 20 DAY
    			AND is_verified = 0
    			AND resend = 0
    			AND org_id = 620776
    		LIMIT 10
    	";

    	$member = $db->query($q);
    	$db->begin();
    	foreach ($member as $key => $value) {
    		try {
    			$db->update('member', ['resend' => 1], ['member_id' => $value->member_id]);
    			if(ccfg::get('new_email')){
    			    email::send('IMRegister', $value->member_id);
    			}else{
    			    $this->send_verification($value->member_id);
    			}
    		} catch (Exception $e) {
    			$err_code++;
    			$err_message = $e->getMessage();
    		}
    	}
    	if ($err_code > 0) {
    		$db->rollback();
    		echo $err_message;
    	} else {
    		$db->commit();
    		echo 'Success';
    	}
    }
}
