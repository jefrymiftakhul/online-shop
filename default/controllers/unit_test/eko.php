<?php

/**
 * Description of eko
 *
 * @author Ecko Santoso
 * @since 17 Nov 16
 */
class Eko_Controller extends CController {
    public function __construct() {
        parent::__construct();
    }
    
    public function product_category() {
        $product_category = product_category::get_product_category_menu('product');
        cdbg::var_dump($product_category);
    }
    
    public function cek_session() {        
//        $api = CApiServerSixtyTwoHallFamily::instance('session', array('Login'));
//        $api->set_post_data(array('auth_id' => ccfg::get('api_auth')));
//        $response = $api->exec();
//        cdbg::var_dump($response);
        $session = CApiServer_Session::instance('session', 'session20161128092923583b96834d751');
        cdbg::var_dump($session->get('kinerja_product_voucher'));
        die(__METHOD__);
    }
    
    public function kp_voucher() {
        $a = '{"method":"voucher_get","code":"100","success":"1","messages":{"message":"Success."},"result":{"categoryID":{},"pageNo":"0","show":"10","itemTotal":"47","pageTotal":"5","products":{"product":[{"merchant":{"name":"TOFU ISLAND","url":"http:\/\/www.instagram.com\/tofu_island\/","address":"JL.SELAM 2 NO.54","country":"Indonesia","province":"Sumatera Utara","city":"Medan","zipcode":{},"phone":"085760733230 \/ (061) 7347264"},"product":{"productID":"bXDvXBYH","productType":"voucher","categoryID":"79","productName":"TOFU ISLAND","productDesc":"ADA PILIHAN PENGAMBILAN\n-TOFU FROZEN (BEKU)\n-TOFU SUDAH SIAP MAKAN","facilityInfo":"-pemesanan dapat dilakukan dengan menghubungi nomor kontak yang tersedia.\n-1 box isi 4pcs","voucherPrint":"1","voucherValue":"30000","voucherValidStart":{},"voucherValidEnd":{},"promo":"0","productPrice":"30000","insuranceCost":{},"shippingCostType":{},"productWeight":{},"variationLabel":{},"stockType":"0","stockUnit":"-1","dailyBuyLimit":{},"buyLimit":{},"images":{"image":[{"no":"1","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/63317i6wrcz7iflhk.jpg","url":"https:\/\/www.kinerjapay.com\/images\/product\/63317i6wrcz7iflhk.jpg"},{"no":"2","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/63317mc0dfjt7sttk.jpg","url":"https:\/\/www.kinerjapay.com\/images\/product\/63317mc0dfjt7sttk.jpg"},{"no":"3","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/633178xhvqi1agedb.jpg","url":"https:\/\/www.kinerjapay.com\/images\/product\/633178xhvqi1agedb.jpg"}]},"tncs":{"tnc":[{"no":"1","desc":"Voucher tidak dapat diuangkan."},{"no":"2","desc":"Voucher tidak dapat digabungkan dengan promo lainnya."},{"no":"3","desc":"Pemesanan dilakukan melalui telepon"},{"no":"4","desc":"Telepon sebelum pengambilan"},{"no":"5","desc":"Voucher berlaku sampai 31 OKTOBER 2016"},{"no":"6","desc":"Voucher harus di print out dan diberikan saat mengambil pesanan"},{"no":"7","desc":"Pengambilan di alamat yang tertera."}]},"variations":{},"status":{}}},{"merchant":{"name":"SANTIKA DYANDRA SPA","url":"http:\/\/","address":"SANTIKA DYANDRA HOTEL AND CONVENTION - MEDAN\nJl Kapt. Maulana Lubis no.7","country":"Indonesia","province":"Sumatera Utara","city":"Medan","zipcode":"20112","phone":"061 4511999"},"product":{"productID":"bJ48nxVq","productType":"voucher","categoryID":"88","productName":"REFLEXIOLOGY","productDesc":"Perawatan dinamis  yang berasal dari pijatan dasar Refleksi, menghubungkan zona reflek di bagian organ kaki dan seluruh system dalam tubuh. Refleksi merupakan metode Reflexiology  adalah perawatan yang menawarkan perawatan pijatan yang dapat mengurangi ketegangan sakit kepala, kurang sirkulasi darah dan gangguan system pencernaan","facilityInfo":"- Ruangan yang Nyaman \n- Tersedia 2 tipe Kamar Single Room dan Couple Room \n- Tiap Kamar Tersedia Kamar mandi dengan shower terpisah dengan toilet\n- Tersedia BathTub\n- Terapis yang berpengalaman\n- Jam operasional 09.00 - 23.00\n- No.Reservasi 061 - 4511 999","voucherPrint":"1","voucherValue":"140000","voucherValidStart":"2016-09-05 00:00:00","voucherValidEnd":"2016-12-30 00:00:00","promo":"1","promoStart":"2016-09-05 00:00:00","promoEnd":"2016-12-30 00:00:00","promoPrice":"140000","productPrice":"200000","insuranceCost":{},"shippingCostType":{},"productWeight":{},"variationLabel":{},"stockType":"0","stockUnit":"-1","dailyBuyLimit":{},"buyLimit":{},"images":{"image":[{"no":"1","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/556183k1aih0a9wn2.JPG","url":"https:\/\/www.kinerjapay.com\/images\/product\/556183k1aih0a9wn2.JPG"},{"no":"2","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/5561864yzu8jf1lot.JPG","url":"https:\/\/www.kinerjapay.com\/images\/product\/5561864yzu8jf1lot.JPG"},{"no":"3","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/55618gxoak1n6c6qh.JPG","url":"https:\/\/www.kinerjapay.com\/images\/product\/55618gxoak1n6c6qh.JPG"},{"no":"4","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/55618fmbksfywv22l.jpg","url":"https:\/\/www.kinerjapay.com\/images\/product\/55618fmbksfywv22l.jpg"},{"no":"5","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/55618lntnkzp6zq1i.JPG","url":"https:\/\/www.kinerjapay.com\/images\/product\/55618lntnkzp6zq1i.JPG"}]},"tncs":{"tnc":[{"no":"1","desc":"Voucher tidak dapat diuangkan."},{"no":"2","desc":"Voucher tidak dapat digabungkan dengan promo lainnya."},{"no":"3","desc":"Voucher harus diprint dan diberikan kepada pihak Santika Hotel untuk penggunaan."},{"no":"4","desc":"HARAP MELAKUKAN RESERVASI TERLEBIH DAHULU SEBELUM MENGGUNAKAN VOUCHER KE NO : 061 - 4511 999"},{"no":"5","desc":"Voucher Berlaku sampai tgl 30 DESEMBER 2016"},{"no":"6","desc":"Voucher Berlaku all day"}]},"variations":{},"status":{}}},{"merchant":{"name":"SANTIKA DYANDRA SPA","url":"http:\/\/","address":"SANTIKA DYANDRA HOTEL AND CONVENTION - MEDAN\nJl Kapt. Maulana Lubis no.7","country":"Indonesia","province":"Sumatera Utara","city":"Medan","zipcode":"20112","phone":"061 4511999"},"product":{"productID":"bJ48nxVp","productType":"voucher","categoryID":"88","productName":"FRESH FRUIT FACIAL","productDesc":"Fresh Fruit Facial Perawatan ini menawarkan perawatan secara mendalam membersihkan, mengelupaskan dan detoksifikasi kulit lelah dan muncul dengan berseri-seri , cahaya muda . Semua produk baru disiapkan sesuai dengan kondisi kulit","facilityInfo":"- Ruangan yang Nyaman \n- Tersedia 2 tipe Kamar Single Room dan Couple Room \n- Tiap Kamar Tersedia Kamar mandi dengan shower terpisah dengan toilet\n- Tersedia BathTub\n- Terapis yang berpengalaman\n- Jam operasional 09.00 - 23.00\n- No.Reservasi 061 - 4511 999","voucherPrint":"1","voucherValue":"350000","voucherValidStart":"2016-09-05 00:00:00","voucherValidEnd":"2016-12-30 00:00:00","promo":"1","promoStart":"2016-09-05 00:00:00","promoEnd":"2016-12-30 00:00:00","promoPrice":"280000","productPrice":"350000","insuranceCost":{},"shippingCostType":{},"productWeight":{},"variationLabel":{},"stockType":"0","stockUnit":"-1","dailyBuyLimit":{},"buyLimit":{},"images":{"image":[{"no":"1","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/55618f1in184h2502.JPG","url":"https:\/\/www.kinerjapay.com\/images\/product\/55618f1in184h2502.JPG"},{"no":"2","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/55618ycz9wvydk7p1.JPG","url":"https:\/\/www.kinerjapay.com\/images\/product\/55618ycz9wvydk7p1.JPG"},{"no":"3","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/55618mlby0nbmh2rl.JPG","url":"https:\/\/www.kinerjapay.com\/images\/product\/55618mlby0nbmh2rl.JPG"},{"no":"4","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/55618cwuyerlzsr2i.jpg","url":"https:\/\/www.kinerjapay.com\/images\/product\/55618cwuyerlzsr2i.jpg"}]},"tncs":{"tnc":[{"no":"1","desc":"Voucher tidak dapat diuangkan."},{"no":"2","desc":"Voucher tidak dapat digabungkan dengan promo lainnya."},{"no":"3","desc":"Voucher harus diprint dan diberikan kepada pihak Santika Hotel untuk penggunaan."},{"no":"4","desc":"HARAP MELAKUKAN RESERVASI TERLEBIH DAHULU SEBELUM MENGGUNAKAN VOUCHER KE NO : 061 - 4511 999"},{"no":"5","desc":"Voucher Berlaku sampai tgl 30 DESEMBER 2016"},{"no":"6","desc":"Voucher Berlaku all day"}]},"variations":{},"status":{}}},{"merchant":{"name":"SANTIKA DYANDRA SPA","url":"http:\/\/","address":"SANTIKA DYANDRA HOTEL AND CONVENTION - MEDAN\nJl Kapt. Maulana Lubis no.7","country":"Indonesia","province":"Sumatera Utara","city":"Medan","zipcode":"20112","phone":"061 4511999"},"product":{"productID":"bJ48nxVn","productType":"voucher","categoryID":"88","productName":"TRADITIONAL BALINESSE MASSAGE","productDesc":"Traditional Balinasse massage adalah perawatan yang menawarkan perawatan Manjakan diri secara fisik dan spiritual dengan pijat Bali yang merupakan temuan ribuan tahun yang lalu. Dikembangkan di Bali, Indonesia, namun menarik akarnya dari pijat tradisional Cina dan India Ayurveda, pengobatan ini akan bekerja dalam untuk menenangkan jaringan yang rusak dan meringankan otot yang tegang dan nyeri sendi","facilityInfo":"- Ruangan yang Nyaman \n- Tersedia 2 tipe Kamar Single Room dan Couple Room \n- Tiap Kamar Tersedia Kamar mandi dengan shower terpisah dengan toilet\n- Tersedia BathTub\n- Terapis yang berpengalaman\n- Jam operasional 09.00 - 23.00\n- No.Reservasi 061 - 4511 999","voucherPrint":"1","voucherValue":"275000","voucherValidStart":"2016-09-05 00:00:00","voucherValidEnd":"2016-12-30 00:00:00","promo":"1","promoStart":"2016-09-03 00:00:00","promoEnd":"2016-12-30 00:00:00","promoPrice":"220000","productPrice":"275000","insuranceCost":{},"shippingCostType":{},"productWeight":{},"variationLabel":{},"stockType":"0","stockUnit":"-1","dailyBuyLimit":{},"buyLimit":{},"images":{"image":[{"no":"1","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/55618u29mmubly0bu.jpg","url":"https:\/\/www.kinerjapay.com\/images\/product\/55618u29mmubly0bu.jpg"},{"no":"2","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/55618vv9yysmo3nne.JPG","url":"https:\/\/www.kinerjapay.com\/images\/product\/55618vv9yysmo3nne.JPG"},{"no":"3","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/556189eyshn4dx068.JPG","url":"https:\/\/www.kinerjapay.com\/images\/product\/556189eyshn4dx068.JPG"},{"no":"4","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/556181qwcpelq4bt1.JPG","url":"https:\/\/www.kinerjapay.com\/images\/product\/556181qwcpelq4bt1.JPG"},{"no":"5","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/556184khfina86l3f.JPG","url":"https:\/\/www.kinerjapay.com\/images\/product\/556184khfina86l3f.JPG"}]},"tncs":{"tnc":[{"no":"1","desc":"Voucher tidak dapat diuangkan."},{"no":"2","desc":"Voucher tidak dapat digabungkan dengan promo lainnya."},{"no":"3","desc":"Voucher harus diprint dan diberikan kepada pihak Santika Hotel untuk penggunaan."},{"no":"4","desc":"HARAP MELAKUKAN RESERVASI TERLEBIH DAHULU SEBELUM MENGGUNAKAN VOUCHER KE NO : 061 - 4511 999"},{"no":"5","desc":"Voucher Berlaku sampai tgl 30 DESEMBER 2016"},{"no":"6","desc":"Voucher Berlaku all day"}]},"variations":{},"status":{}}},{"merchant":{"name":"SANTIKA DYANDRA SPA","url":"http:\/\/","address":"SANTIKA DYANDRA HOTEL AND CONVENTION - MEDAN\nJl Kapt. Maulana Lubis no.7","country":"Indonesia","province":"Sumatera Utara","city":"Medan","zipcode":"20112","phone":"061 4511999"},"product":{"productID":"bJ48nxVm","productType":"voucher","categoryID":"88","productName":"DYANDRA SIGNATURE MASSAGE","productDesc":"Dyandra Signature massage adalah perawatan yang menawarkan perawatan Pijat mewah menggunakan minyak esensial impor yang paling indah ! Perawatan ini adalah salah satu dari jenis perawatan yang menjanjikan! Cobalah! Hal ini tidak boleh terlewatkan, ketegangan otot akan menjadi relaks dengan pijatan yang lembut","facilityInfo":"- Ruangan yang Nyaman \n- Tersedia 2 tipe Kamar Single Room dan Couple Room \n- Tiap Kamar Tersedia Kamar mandi dengan shower terpisah dengan toilet\n- Tersedia BathTub\n- Terapis yang berpengalaman\n- Jam operasional 09.00 - 23.00\n- No.Reservasi 061 - 4511 999","voucherPrint":"1","voucherValue":"300000","voucherValidStart":"2016-09-05 00:00:00","voucherValidEnd":"2016-12-30 00:00:00","promo":"1","promoStart":"2016-09-05 00:00:00","promoEnd":"2016-12-30 00:00:00","promoPrice":"240000","productPrice":"300000","insuranceCost":{},"shippingCostType":{},"productWeight":{},"variationLabel":{},"stockType":"0","stockUnit":"-1","dailyBuyLimit":{},"buyLimit":{},"images":{"image":[{"no":"1","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/55618c0vy9tjanh8d.JPG","url":"https:\/\/www.kinerjapay.com\/images\/product\/55618c0vy9tjanh8d.JPG"},{"no":"2","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/556182ygqu5nhoexa.JPG","url":"https:\/\/www.kinerjapay.com\/images\/product\/556182ygqu5nhoexa.JPG"},{"no":"3","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/55618vmtkcbatw4nl.JPG","url":"https:\/\/www.kinerjapay.com\/images\/product\/55618vmtkcbatw4nl.JPG"},{"no":"4","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/5561836hmjfbzll7s.JPG","url":"https:\/\/www.kinerjapay.com\/images\/product\/5561836hmjfbzll7s.JPG"},{"no":"5","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/55618era2ynnnib5m.jpg","url":"https:\/\/www.kinerjapay.com\/images\/product\/55618era2ynnnib5m.jpg"}]},"tncs":{"tnc":[{"no":"1","desc":"Voucher tidak dapat diuangkan."},{"no":"2","desc":"Voucher tidak dapat digabungkan dengan promo lainnya."},{"no":"3","desc":"Voucher harus diprint dan diberikan kepada pihak Santika Hotel untuk penggunaan."},{"no":"4","desc":"HARAP MELAKUKAN RESERVASI TERLEBIH DAHULU SEBELUM MENGGUNAKAN VOUCHER KE NO : 061 - 4511 999"},{"no":"5","desc":"Voucher Berlaku sampai tgl 30 DESEMBER 2016"},{"no":"6","desc":"Voucher Berlaku all day"}]},"variations":{},"status":{}}},{"merchant":{"name":"SANTIKA DYANDRA SPA","url":"http:\/\/","address":"SANTIKA DYANDRA HOTEL AND CONVENTION - MEDAN\nJl Kapt. Maulana Lubis no.7","country":"Indonesia","province":"Sumatera Utara","city":"Medan","zipcode":"20112","phone":"061 4511999"},"product":{"productID":"bJ48nxVl","productType":"voucher","categoryID":"88","productName":"REVITALIZING PACKAGE","productDesc":"Revitalizing Package adalah perawatan yang menawarkan perawatan\n* Ritual Rendam Kaki Garam Laut \n* Tradisional Pijat Bali\n* Body Scrub\n* Mandi Susu Citrus \n* Aplikasi lotion","facilityInfo":"- Ruangan yang Nyaman \n- Tersedia 2 tipe Kamar Single Room dan Couple Room \n- Tiap Kamar Tersedia Kamar mandi dengan shower terpisah dengan toilet\n- Tersedia BathTub\n- Terapis yang berpengalaman\n- Jam operasional 09.00 - 23.00\n- No.Reservasi 061 - 4511 999","voucherPrint":"1","voucherValue":"675000","voucherValidStart":"2016-09-05 00:00:00","voucherValidEnd":"2016-12-30 00:00:00","promo":"1","promoStart":"2016-09-05 00:00:00","promoEnd":"2016-12-30 00:00:00","promoPrice":"472500","productPrice":"675000","insuranceCost":{},"shippingCostType":{},"productWeight":{},"variationLabel":{},"stockType":"0","stockUnit":"-1","dailyBuyLimit":{},"buyLimit":{},"images":{"image":[{"no":"1","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/55618e11qqwrlseeb.JPG","url":"https:\/\/www.kinerjapay.com\/images\/product\/55618e11qqwrlseeb.JPG"},{"no":"2","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/55618hur204sw73k9.JPG","url":"https:\/\/www.kinerjapay.com\/images\/product\/55618hur204sw73k9.JPG"},{"no":"3","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/556186jb6nxm6m4wx.JPG","url":"https:\/\/www.kinerjapay.com\/images\/product\/556186jb6nxm6m4wx.JPG"},{"no":"4","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/55618nebjl31nqrfs.JPG","url":"https:\/\/www.kinerjapay.com\/images\/product\/55618nebjl31nqrfs.JPG"},{"no":"5","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/55618ptj3esymqeqn.jpg","url":"https:\/\/www.kinerjapay.com\/images\/product\/55618ptj3esymqeqn.jpg"}]},"tncs":{"tnc":[{"no":"1","desc":"Voucher tidak dapat diuangkan."},{"no":"2","desc":"Voucher tidak dapat digabungkan dengan promo lainnya."},{"no":"3","desc":"Voucher harus diprint dan diberikan kepada pihak Santika Hotel untuk penggunaan."},{"no":"4","desc":"HARAP MELAKUKAN RESERVASI TERLEBIH DAHULU SEBELUM MENGGUNAKAN VOUCHER KE NO : 061 - 4511 999"},{"no":"5","desc":"Voucher Berlaku sampai tgl 30 DESEMBER 2016"},{"no":"6","desc":"Voucher Berlaku all day"}]},"variations":{},"status":{}}},{"merchant":{"name":"NAV KARAOKE","url":"http:\/\/","address":"Jl.Zainul Arifin No. 208 A","country":"Indonesia","province":"Sumatera Utara","city":"Medan","zipcode":{},"phone":"061 4527422"},"product":{"productID":"bnAT65fi","productType":"voucher","categoryID":"87","productName":"PROMO NYANYI 3 JAM BAYAR 2 JAM NAV KARAOKE MEDAN","productDesc":"- Nyanyi 3 jam bayar 2 jam dengan ruangan STANDART ROOM\n- kapasitas 4-6 orang\n- Voucher berlaku untuk 3 JAM\n- Voucher berlaku setiap hari Senin-Kamis . jam 10:00-15:00 WIB\n- Voucher sudah termasuk ppn 15% dan service 5%","facilityInfo":"- Tempat yang sejuk dan nyaman\n- Terletak di pusat kota\n- Tempat parkir yang luas","voucherPrint":"1","voucherValue":"81000","voucherValidStart":"2016-10-06 00:00:00","voucherValidEnd":"2016-12-15 00:00:00","promo":"1","promoStart":"2016-10-06 00:00:00","promoEnd":"2016-11-30 00:00:00","promoPrice":"81000","productPrice":"119542","insuranceCost":{},"shippingCostType":{},"productWeight":{},"variationLabel":{},"stockType":"0","stockUnit":"-1","dailyBuyLimit":{},"buyLimit":{},"images":{"image":[{"no":"1","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/42845ctu943hsz0di.jpg","url":"https:\/\/www.kinerjapay.com\/images\/product\/42845ctu943hsz0di.jpg"},{"no":"2","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/42845343wzv47ou8i.jpg","url":"https:\/\/www.kinerjapay.com\/images\/product\/42845343wzv47ou8i.jpg"},{"no":"3","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/428454qcwibqlohmd.jpg","url":"https:\/\/www.kinerjapay.com\/images\/product\/428454qcwibqlohmd.jpg"},{"no":"4","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/42845vs60l84jmsxs.jpg","url":"https:\/\/www.kinerjapay.com\/images\/product\/42845vs60l84jmsxs.jpg"}]},"tncs":{"tnc":[{"no":"1","desc":"Voucher tidak dapat diuangkan."},{"no":"2","desc":"Voucher tidak dapat digabungkan dengan promo lainnya."},{"no":"3","desc":"Voucher harus di print out\/cetak"},{"no":"4","desc":"Persediaan Room Promo Terbatas & Tidak di perbolehkan Booking (Langsung Pesan)"},{"no":"5","desc":"Voucher berlaku di Nav Karaoke Jl. Zainul arifin No. 208 Medan"},{"no":"6","desc":"Voucher sudah termasuk ppn 15% dan service 5%"},{"no":"7","desc":"Voucher Berlaku sampai tanggal 15 DESEMBER 2016"},{"no":"8","desc":"Voucher berlaku setiap hari senin- kamis ( jam 10:00-15:00 WIB)"},{"no":"9","desc":"Ruangan hanya berlaku untuk STANDART ROOM dengan kapasitas 4-6 Orang"}]},"variations":{},"status":{}}},{"merchant":{"name":"NAV KARAOKE","url":"http:\/\/","address":"Jl.Zainul Arifin No. 208 A","country":"Indonesia","province":"Sumatera Utara","city":"Medan","zipcode":{},"phone":"061 4527422"},"product":{"productID":"bnAT65fj","productType":"voucher","categoryID":"87","productName":"PAKET NYANYI 3 JAM BAYAR 2 JAM  RUANGAN MEDIUM NAV KARAOKE","productDesc":"- Nyanyi 3 jam bayar 2 jam dengan ruangan ROOM MEDIUM\n- kapasitas 6-8 orang\n- Voucher berlaku untuk 3 JAM\n- Voucher berlaku setiap hari Senin-Kamis . jam 10:00-15:00 WIB\n- Voucher sudah termasuk ppn 15% dan service 5%","facilityInfo":"- Tempat yang sejuk dan nyaman\n- Terletak di pusat kota\n- Tempat parkir yang luas","voucherPrint":"1","voucherValue":"96068","voucherValidStart":"2016-10-06 00:00:00","voucherValidEnd":"2016-12-15 00:00:00","promo":"1","promoStart":"2016-10-06 00:00:00","promoEnd":"2016-11-30 00:00:00","promoPrice":"96068","productPrice":"141277","insuranceCost":{},"shippingCostType":{},"productWeight":{},"variationLabel":{},"stockType":"0","stockUnit":"-1","dailyBuyLimit":{},"buyLimit":{},"images":{"image":[{"no":"1","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/42845gq9hiqhxazsw.jpg","url":"https:\/\/www.kinerjapay.com\/images\/product\/42845gq9hiqhxazsw.jpg"},{"no":"2","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/42845gwjb67iqwqr0.jpg","url":"https:\/\/www.kinerjapay.com\/images\/product\/42845gwjb67iqwqr0.jpg"},{"no":"3","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/428454pdkv3g4x6jz.jpg","url":"https:\/\/www.kinerjapay.com\/images\/product\/428454pdkv3g4x6jz.jpg"},{"no":"4","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/428453g9wq1dy1y1g.jpg","url":"https:\/\/www.kinerjapay.com\/images\/product\/428453g9wq1dy1y1g.jpg"}]},"tncs":{"tnc":[{"no":"1","desc":"Voucher tidak dapat diuangkan."},{"no":"2","desc":"Voucher tidak dapat digabungkan dengan promo lainnya."},{"no":"3","desc":"Voucher harus di print out\/cetak"},{"no":"4","desc":"Persediaan Room Promo Terbatas & Tidak di perbolehkan Booking (Langsung Pesan)"},{"no":"5","desc":"Voucher berlaku di Nav Karaoke Jl. Zainul arifin No. 208 Medan"},{"no":"6","desc":"Voucher sudah termasuk ppn 15% dan service 5%"},{"no":"7","desc":"Voucher Berlaku sampai tanggal 15 DESEMBER 2016"},{"no":"8","desc":"Voucher berlaku setiap hari senin- kamis ( jam 10:00-15:00 WIB)"},{"no":"9","desc":"Ruangan hanya berlaku untuk STANDART ROOM dengan kapasitas 6- 8 Orang"}]},"variations":{},"status":{}}},{"merchant":{"name":"NAV KARAOKE","url":"http:\/\/","address":"Jl.Zainul Arifin No. 208 A","country":"Indonesia","province":"Sumatera Utara","city":"Medan","zipcode":{},"phone":"061 4527422"},"product":{"productID":"bnAT65fk","productType":"voucher","categoryID":"87","productName":"PAKET NYANYI 3 JAM BAYAR 2 JAM RUANGAN LARGE NAV KARAOKE","productDesc":"- Nyanyi 3 jam bayar 2 jam dengan ruangan ROOM LARGE\n- kapasitas 8-12 orang\n- Voucher berlaku untuk 3 JAM\n- Voucher berlaku setiap hari Senin-Kamis . jam 10:00-15:00 WIB\n- Voucher sudah termasuk ppn 15% dan service 5%","facilityInfo":"- Tempat yang sejuk dan nyaman\n- Terletak di pusat kota\n- Tempat parkir yang luas","voucherPrint":"1","voucherValue":"118238","voucherValidStart":"2016-10-06 00:00:00","voucherValidEnd":"2016-12-15 00:00:00","promo":"1","promoStart":"2016-10-06 00:00:00","promoEnd":"2016-11-30 00:00:00","promoPrice":"118238","productPrice":"173880","insuranceCost":{},"shippingCostType":{},"productWeight":{},"variationLabel":{},"stockType":"0","stockUnit":"-1","dailyBuyLimit":{},"buyLimit":{},"images":{"image":[{"no":"1","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/428450cs80764s7c0.jpg","url":"https:\/\/www.kinerjapay.com\/images\/product\/428450cs80764s7c0.jpg"},{"no":"2","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/42845ox38l2xg7nzz.jpg","url":"https:\/\/www.kinerjapay.com\/images\/product\/42845ox38l2xg7nzz.jpg"},{"no":"3","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/42845cu3p8ujdyxf8.jpg","url":"https:\/\/www.kinerjapay.com\/images\/product\/42845cu3p8ujdyxf8.jpg"}]},"tncs":{"tnc":[{"no":"1","desc":"Voucher tidak dapat diuangkan."},{"no":"2","desc":"Voucher tidak dapat digabungkan dengan promo lainnya."},{"no":"3","desc":"Voucher harus di print out\/cetak"},{"no":"4","desc":"Persediaan Room Promo Terbatas & Tidak di perbolehkan Booking (Langsung Pesan)"},{"no":"5","desc":"Voucher berlaku di Nav Karaoke Jl. Zainul arifin No. 208 Medan"},{"no":"6","desc":"Voucher sudah termasuk ppn 15% dan service 5%"},{"no":"7","desc":"Voucher Berlaku sampai tanggal 15 DESEMBER 2016"},{"no":"8","desc":"Voucher berlaku setiap hari senin- kamis ( jam 10:00-15:00 WIB)"},{"no":"9","desc":"Ruangan hanya berlaku untuk STANDART ROOM dengan kapasitas 8-12  Orang"}]},"variations":{},"status":{}}},{"merchant":{"name":"PISANG GORENG KALIMANTAN (BORNEO)","url":"http:\/\/","address":"-KOMP ASIA MEGAMAS FOODCOURT \nSamping BCA \n- JL.ASIA MEGAMAS BLOK H NO 1","country":"Indonesia","province":"Sumatera Utara","city":"Medan","zipcode":{},"phone":"082162768273"},"product":{"productID":"bWKBRTAS","productType":"voucher","categoryID":"79","productName":"VOUCHER PISANG GORENG KALIMANTAN ( BORNEO )","productDesc":"Pisang goreng kalimantan isi 20 pcs","facilityInfo":"- Terletak di food court jl. asia mega mas samping bca ( 11.00-18.00WIB)\n- jl. Asia mega mas blok H NO 1 ( 15.00-22.00)","voucherPrint":"1","voucherValue":"80000","voucherValidStart":"2016-08-16 00:00:00","voucherValidEnd":"2016-11-30 00:00:00","promo":"0","productPrice":"80000","insuranceCost":{},"shippingCostType":{},"productWeight":{},"variationLabel":{},"stockType":"0","stockUnit":"-1","dailyBuyLimit":{},"buyLimit":{},"images":{"image":[{"no":"1","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/62814nek93ke4ohg0.JPG","url":"https:\/\/www.kinerjapay.com\/images\/product\/62814nek93ke4ohg0.JPG"},{"no":"2","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/62814k1tneleqxvrf.JPG","url":"https:\/\/www.kinerjapay.com\/images\/product\/62814k1tneleqxvrf.JPG"},{"no":"3","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/62814y1l7zbzhjoel.jpg","url":"https:\/\/www.kinerjapay.com\/images\/product\/62814y1l7zbzhjoel.jpg"},{"no":"4","th":"https:\/\/www.kinerjapay.com\/images\/product\/th\/62814cnpc9ypkk2v3.JPG","url":"https:\/\/www.kinerjapay.com\/images\/product\/62814cnpc9ypkk2v3.JPG"}]},"tncs":{"tnc":[{"no":"1","desc":"Voucher tidak dapat diuangkan."},{"no":"2","desc":"Voucher tidak dapat digabungkan dengan promo lainnya."},{"no":"3","desc":"Voucher harus di PRINT OUT \/ CETAK."},{"no":"4","desc":"Voucher berlaku ALL DAY."},{"no":"5","desc":"Asia mega mas food court samping bca ( 11:00- 18:00 WIB), JL. Asia mega mas blok H NO 1( 15:00-22:00 WIB)"},{"no":"6","desc":"Voucher berlaku hingga tanggal 30 NOVEMBER 2016."},{"no":"7","desc":"Voucher hanya berlaku di wilayah Medan."}]},"variations":{},"status":{}}}]}}}';
        $b = json_decode($a, TRUE);
        cdbg::var_dump($b);
    }
    
    public function build_kp_tree() {
// <editor-fold defaultstate="collapsed" desc="comment">
        $category = array(
            '0'=>array(
                    'type'=>'Product',
                    'name'=>'Electronic',
                    'group'=>array(
                    ),
                    'parentID'=>array(
                    ),
                    'id'=>'2',
            ),
            '1'=>array(
                    'type'=>'Product',
                    'name'=>'Fashion',
                    'group'=>array(
                    ),
                    'parentID'=>array(
                    ),
                    'id'=>'1',
            ),
            '2'=>array(
                    'type'=>'Product',
                    'name'=>'Hobby',
                    'group'=>array(
                    ),
                    'parentID'=>array(
                    ),
                    'id'=>'3',
            ),
            '3'=>array(
                    'type'=>'Product',
                    'name'=>'Household',
                    'group'=>array(
                    ),
                    'parentID'=>array(
                    ),
                    'id'=>'5',
            ),
            '4'=>array(
                    'type'=>'Product',
                    'name'=>'Reading',
                    'group'=>array(
                    ),
                    'parentID'=>array(
                    ),
                    'id'=>'4',
            ),
            '5'=>array(
                    'type'=>'Product',
                    'name'=>'Baterai',
                    'group'=>'Aksesoris HP',
                    'parentID'=>'21',
                    'id'=>'33',
            ),
            '6'=>array(
                    'type'=>'Product',
                    'name'=>'Casing',
                    'group'=>'Aksesoris HP',
                    'parentID'=>'21',
                    'id'=>'34',
            ),
            '7'=>array(
                    'type'=>'Product',
                    'name'=>'Charger',
                    'group'=>'Aksesoris HP',
                    'parentID'=>'21',
                    'id'=>'32',
            ),
            '8'=>array(
                    'type'=>'Product',
                    'name'=>'Data Cable',
                    'group'=>'Aksesoris HP',
                    'parentID'=>'21',
                    'id'=>'28',
            ),
            '9'=>array(
                    'type'=>'Product',
                    'name'=>'Earphone',
                    'group'=>'Aksesoris HP',
                    'parentID'=>'21',
                    'id'=>'29',
            ),
            '10'=>array(
                    'type'=>'Product',
                    'name'=>'Gantungan HP',
                    'group'=>'Aksesoris HP',
                    'parentID'=>'21',
                    'id'=>'37',
            ),
            '11'=>array(
                    'type'=>'Product',
                    'name'=>'Kartu Perdana',
                    'group'=>'Aksesoris HP',
                    'parentID'=>'21',
                    'id'=>'27',
            ),
            '12'=>array(
                    'type'=>'Product',
                    'name'=>'Memory Card',
                    'group'=>'Aksesoris HP',
                    'parentID'=>'21',
                    'id'=>'30',
            ),
            '13'=>array(
                    'type'=>'Product',
                    'name'=>'Power Bank',
                    'group'=>'Aksesoris HP',
                    'parentID'=>'21',
                    'id'=>'31',
            ),
            '14'=>array(
                    'type'=>'Product',
                    'name'=>'Screen Guard',
                    'group'=>'Aksesoris HP',
                    'parentID'=>'21',
                    'id'=>'35',
            ),
            '15'=>array(
                    'type'=>'Product',
                    'name'=>'Stylus',
                    'group'=>'Aksesoris HP',
                    'parentID'=>'21',
                    'id'=>'36',
            ),
            '16'=>array(
                    'type'=>'Product',
                    'name'=>'Tongsis & Tomsis',
                    'group'=>'Aksesoris HP',
                    'parentID'=>'21',
                    'id'=>'26',
            ),
            '17'=>array(
                    'type'=>'Product',
                    'name'=>'Cooling',
                    'group'=>'Aksesoris Komputer',
                    'parentID'=>'22',
                    'id'=>'41',
            ),
            '18'=>array(
                    'type'=>'Product',
                    'name'=>'Keyboard',
                    'group'=>'Aksesoris Komputer',
                    'parentID'=>'22',
                    'id'=>'39',
            ),
            '19'=>array(
                    'type'=>'Product',
                    'name'=>'Keyboard Protector',
                    'group'=>'Aksesoris Komputer',
                    'parentID'=>'22',
                    'id'=>'42',
            ),
            '20'=>array(
                    'type'=>'Product',
                    'name'=>'Mouse',
                    'group'=>'Aksesoris Komputer',
                    'parentID'=>'22',
                    'id'=>'38',
            ),
            '21'=>array(
                    'type'=>'Product',
                    'name'=>'Multimedia Speaker',
                    'group'=>'Aksesoris Komputer',
                    'parentID'=>'22',
                    'id'=>'40',
            ),
            '22'=>array(
                    'type'=>'Product',
                    'name'=>'Aksesoris HP',
                    'group'=>'Electronic',
                    'parentID'=>'2',
                    'id'=>'21',
            ),
            '23'=>array(
                    'type'=>'Product',
                    'name'=>'Aksesoris Komputer',
                    'group'=>'Electronic',
                    'parentID'=>'2',
                    'id'=>'22',
            ),
            '24'=>array(
                    'type'=>'Product',
                    'name'=>'HP & Gadget',
                    'group'=>'Electronic',
                    'parentID'=>'2',
                    'id'=>'20',
            ),
            '25'=>array(
                    'type'=>'Product',
                    'name'=>'Komputer',
                    'group'=>'Electronic',
                    'parentID'=>'2',
                    'id'=>'19',
            ),
            '26'=>array(
                    'type'=>'Product',
                    'name'=>'Speaker',
                    'group'=>'Electronic',
                    'parentID'=>'2',
                    'id'=>'23',
            ),
            '27'=>array(
                    'type'=>'Product',
                    'name'=>'Baju Muslim',
                    'group'=>'Fashion',
                    'parentID'=>'1',
                    'id'=>'103',
            ),
            '28'=>array(
                    'type'=>'Product',
                    'name'=>'Batik',
                    'group'=>'Fashion',
                    'parentID'=>'1',
                    'id'=>'104',
            ),
            '29'=>array(
                    'type'=>'Product',
                    'name'=>'Fashion Aksesoris',
                    'group'=>'Fashion',
                    'parentID'=>'1',
                    'id'=>'10',
            ),
            '30'=>array(
                    'type'=>'Product',
                    'name'=>'Jam Tangan',
                    'group'=>'Fashion',
                    'parentID'=>'1',
                    'id'=>'9',
            ),
            '31'=>array(
                    'type'=>'Product',
                    'name'=>'Kecantikan',
                    'group'=>'Fashion',
                    'parentID'=>'1',
                    'id'=>'11',
            ),
            '32'=>array(
                    'type'=>'Product',
                    'name'=>'Kesehatan',
                    'group'=>'Fashion',
                    'parentID'=>'1',
                    'id'=>'94',
            ),
            '33'=>array(
                    'type'=>'Product',
                    'name'=>'Pakaian',
                    'group'=>'Fashion',
                    'parentID'=>'1',
                    'id'=>'7',
            ),
            '34'=>array(
                    'type'=>'Product',
                    'name'=>'Parfum',
                    'group'=>'Fashion',
                    'parentID'=>'1',
                    'id'=>'8',
            ),
            '35'=>array(
                    'type'=>'Product',
                    'name'=>'Sepatu & Sandal',
                    'group'=>'Fashion',
                    'parentID'=>'1',
                    'id'=>'56',
            ),
            '36'=>array(
                    'type'=>'Product',
                    'name'=>'Tas',
                    'group'=>'Fashion',
                    'parentID'=>'1',
                    'id'=>'6',
            ),
            '37'=>array(
                    'type'=>'Product',
                    'name'=>'Fotografi',
                    'group'=>'Hobby',
                    'parentID'=>'3',
                    'id'=>'58',
            ),
            '38'=>array(
                    'type'=>'Product',
                    'name'=>'Gaya Hidup',
                    'group'=>'Hobby',
                    'parentID'=>'3',
                    'id'=>'115',
            ),
            '39'=>array(
                    'type'=>'Product',
                    'name'=>'Mainan',
                    'group'=>'Hobby',
                    'parentID'=>'3',
                    'id'=>'62',
            ),
            '40'=>array(
                    'type'=>'Product',
                    'name'=>'Musik',
                    'group'=>'Hobby',
                    'parentID'=>'3',
                    'id'=>'57',
            ),
            '41'=>array(
                    'type'=>'Product',
                    'name'=>'Olahraga',
                    'group'=>'Hobby',
                    'parentID'=>'3',
                    'id'=>'59',
            ),
            '42'=>array(
                    'type'=>'Product',
                    'name'=>'Otomotif',
                    'group'=>'Hobby',
                    'parentID'=>'3',
                    'id'=>'60',
            ),
            '43'=>array(
                    'type'=>'Product',
                    'name'=>'Pajangan',
                    'group'=>'Hobby',
                    'parentID'=>'3',
                    'id'=>'61',
            ),
            '44'=>array(
                    'type'=>'Product',
                    'name'=>'Dapur',
                    'group'=>'Household',
                    'parentID'=>'5',
                    'id'=>'64',
            ),
            '45'=>array(
                    'type'=>'Product',
                    'name'=>'Elektronik R. Tangga',
                    'group'=>'Household',
                    'parentID'=>'5',
                    'id'=>'97',
            ),
            '46'=>array(
                    'type'=>'Product',
                    'name'=>'Jam',
                    'group'=>'Household',
                    'parentID'=>'5',
                    'id'=>'66',
            ),
            '47'=>array(
                    'type'=>'Product',
                    'name'=>'Kebun',
                    'group'=>'Household',
                    'parentID'=>'5',
                    'id'=>'65',
            ),
            '48'=>array(
                    'type'=>'Product',
                    'name'=>'Kursi',
                    'group'=>'Household',
                    'parentID'=>'5',
                    'id'=>'68',
            ),
            '49'=>array(
                    'type'=>'Product',
                    'name'=>'Lampu',
                    'group'=>'Household',
                    'parentID'=>'5',
                    'id'=>'69',
            ),
            '50'=>array(
                    'type'=>'Product',
                    'name'=>'Lemari',
                    'group'=>'Household',
                    'parentID'=>'5',
                    'id'=>'70',
            ),
            '51'=>array(
                    'type'=>'Product',
                    'name'=>'Makanan & Minuman',
                    'group'=>'Household',
                    'parentID'=>'5',
                    'id'=>'96',
            ),
            '52'=>array(
                    'type'=>'Product',
                    'name'=>'Meja',
                    'group'=>'Household',
                    'parentID'=>'5',
                    'id'=>'67',
            ),
            '53'=>array(
                    'type'=>'Product',
                    'name'=>'Tempat Tidur',
                    'group'=>'Household',
                    'parentID'=>'5',
                    'id'=>'105',
            ),
            '54'=>array(
                    'type'=>'Product',
                    'name'=>'TV',
                    'group'=>'Household',
                    'parentID'=>'5',
                    'id'=>'72',
            ),
            '55'=>array(
                    'type'=>'Product',
                    'name'=>'HP',
                    'group'=>'HP & Gadget',
                    'parentID'=>'20',
                    'id'=>'78',
            ),
            '56'=>array(
                    'type'=>'Product',
                    'name'=>'HP Bekas',
                    'group'=>'HP & Gadget',
                    'parentID'=>'20',
                    'id'=>'25',
            ),
            '57'=>array(
                    'type'=>'Product',
                    'name'=>'Batu & Perhiasan',
                    'group'=>'I LOVE INDONESIA',
                    'parentID'=>'102',
                    'id'=>'109',
            ),
            '58'=>array(
                    'type'=>'Product',
                    'name'=>'Crafting',
                    'group'=>'I LOVE INDONESIA',
                    'parentID'=>'102',
                    'id'=>'107',
            ),
            '59'=>array(
                    'type'=>'Product',
                    'name'=>'Elektronik',
                    'group'=>'I LOVE INDONESIA',
                    'parentID'=>'102',
                    'id'=>'111',
            ),
            '60'=>array(
                    'type'=>'Product',
                    'name'=>'Fashion',
                    'group'=>'I LOVE INDONESIA',
                    'parentID'=>'102',
                    'id'=>'106',
            ),
            '61'=>array(
                    'type'=>'Product',
                    'name'=>'Hobi',
                    'group'=>'I LOVE INDONESIA',
                    'parentID'=>'102',
                    'id'=>'112',
            ),
            '62'=>array(
                    'type'=>'Product',
                    'name'=>'Household',
                    'group'=>'I LOVE INDONESIA',
                    'parentID'=>'102',
                    'id'=>'110',
            ),
            '63'=>array(
                    'type'=>'Product',
                    'name'=>'Kopi',
                    'group'=>'I LOVE INDONESIA',
                    'parentID'=>'102',
                    'id'=>'108',
            ),
            '64'=>array(
                    'type'=>'Product',
                    'name'=>'Rambut',
                    'group'=>'Kecantikan',
                    'parentID'=>'11',
                    'id'=>'12',
            ),
            '65'=>array(
                    'type'=>'Product',
                    'name'=>'Tubuh',
                    'group'=>'Kecantikan',
                    'parentID'=>'11',
                    'id'=>'14',
            ),
            '66'=>array(
                    'type'=>'Product',
                    'name'=>'Wajah',
                    'group'=>'Kecantikan',
                    'parentID'=>'11',
                    'id'=>'13',
            ),
            '67'=>array(
                    'type'=>'Product',
                    'name'=>'Computer Case',
                    'group'=>'Komputer',
                    'parentID'=>'19',
                    'id'=>'48',
            ),
            '68'=>array(
                    'type'=>'Product',
                    'name'=>'Desktop PC',
                    'group'=>'Komputer',
                    'parentID'=>'19',
                    'id'=>'43',
            ),
            '69'=>array(
                    'type'=>'Product',
                    'name'=>'Graphic Card',
                    'group'=>'Komputer',
                    'parentID'=>'19',
                    'id'=>'51',
            ),
            '70'=>array(
                    'type'=>'Product',
                    'name'=>'Laptop',
                    'group'=>'Komputer',
                    'parentID'=>'19',
                    'id'=>'44',
            ),
            '71'=>array(
                    'type'=>'Product',
                    'name'=>'Memory',
                    'group'=>'Komputer',
                    'parentID'=>'19',
                    'id'=>'47',
            ),
            '72'=>array(
                    'type'=>'Product',
                    'name'=>'Mother Board',
                    'group'=>'Komputer',
                    'parentID'=>'19',
                    'id'=>'45',
            ),
            '73'=>array(
                    'type'=>'Product',
                    'name'=>'Optical Drive',
                    'group'=>'Komputer',
                    'parentID'=>'19',
                    'id'=>'50',
            ),
            '74'=>array(
                    'type'=>'Product',
                    'name'=>'Printer',
                    'group'=>'Komputer',
                    'parentID'=>'19',
                    'id'=>'52',
            ),
            '75'=>array(
                    'type'=>'Product',
                    'name'=>'Processor',
                    'group'=>'Komputer',
                    'parentID'=>'19',
                    'id'=>'46',
            ),
            '76'=>array(
                    'type'=>'Product',
                    'name'=>'Scanner',
                    'group'=>'Komputer',
                    'parentID'=>'19',
                    'id'=>'53',
            ),
            '77'=>array(
                    'type'=>'Product',
                    'name'=>'Server',
                    'group'=>'Komputer',
                    'parentID'=>'19',
                    'id'=>'113',
            ),
            '78'=>array(
                    'type'=>'Product',
                    'name'=>'Software',
                    'group'=>'Komputer',
                    'parentID'=>'19',
                    'id'=>'91',
            ),
            '79'=>array(
                    'type'=>'Product',
                    'name'=>'Sound Card',
                    'group'=>'Komputer',
                    'parentID'=>'19',
                    'id'=>'54',
            ),
            '80'=>array(
                    'type'=>'Product',
                    'name'=>'Storage',
                    'group'=>'Komputer',
                    'parentID'=>'19',
                    'id'=>'49',
            ),
            '81'=>array(
                    'type'=>'Product',
                    'name'=>'Atasan',
                    'group'=>'Pakaian',
                    'parentID'=>'7',
                    'id'=>'16',
            ),
            '82'=>array(
                    'type'=>'Product',
                    'name'=>'Bawahan',
                    'group'=>'Pakaian',
                    'parentID'=>'7',
                    'id'=>'17',
            ),
            '83'=>array(
                    'type'=>'Product',
                    'name'=>'Bayi dan Anak',
                    'group'=>'Pakaian',
                    'parentID'=>'7',
                    'id'=>'98',
            ),
            '84'=>array(
                    'type'=>'Product',
                    'name'=>'Gaun',
                    'group'=>'Pakaian',
                    'parentID'=>'7',
                    'id'=>'18',
            ),
            '85'=>array(
                    'type'=>'Product',
                    'name'=>'Pakaian Bayi',
                    'group'=>'Pakaian',
                    'parentID'=>'7',
                    'id'=>'55',
            ),
            '86'=>array(
                    'type'=>'Product',
                    'name'=>'Buku',
                    'group'=>'Reading',
                    'parentID'=>'4',
                    'id'=>'73',
            ),
            '87'=>array(
                    'type'=>'Product',
                    'name'=>'e-Book',
                    'group'=>'Reading',
                    'parentID'=>'4',
                    'id'=>'99',
            ),
            '88'=>array(
                    'type'=>'Product',
                    'name'=>'Komik',
                    'group'=>'Reading',
                    'parentID'=>'4',
                    'id'=>'75',
            ),
            '89'=>array(
                    'type'=>'Product',
                    'name'=>'Majalah',
                    'group'=>'Reading',
                    'parentID'=>'4',
                    'id'=>'74',
            ),
            '90'=>array(
                    'type'=>'Product',
                    'name'=>'Majalah Digital',
                    'group'=>'Reading',
                    'parentID'=>'4',
                    'id'=>'100',
            ),
            '91'=>array(
                    'type'=>'Product',
                    'name'=>'Novel',
                    'group'=>'Reading',
                    'parentID'=>'4',
                    'id'=>'76',
            ),
            '92'=>array(
                    'type'=>'Product',
                    'name'=>'Video Digital',
                    'group'=>'Reading',
                    'parentID'=>'4',
                    'id'=>'101',
            ),
            '93'=>array(
                    'type'=>'Voucher',
                    'name'=>'Belanja',
                    'group'=>array(
                    ),
                    'parentID'=>array(
                    ),
                    'id'=>'80',
            ),
            '94'=>array(
                    'type'=>'Voucher',
                    'name'=>'Game',
                    'group'=>array(
                    ),
                    'parentID'=>array(
                    ),
                    'id'=>'83',
            ),
            '95'=>array(
                    'type'=>'Voucher',
                    'name'=>'Makanan',
                    'group'=>array(
                    ),
                    'parentID'=>array(
                    ),
                    'id'=>'79',
            ),
            '96'=>array(
                    'type'=>'Voucher',
                    'name'=>'Pulsa Elektrik',
                    'group'=>array(
                    ),
                    'parentID'=>array(
                    ),
                    'id'=>'84',
            ),
            '97'=>array(
                    'type'=>'Voucher',
                    'name'=>'Pulsa HP',
                    'group'=>array(
                    ),
                    'parentID'=>array(
                    ),
                    'id'=>'82',
            ),
            '98'=>array(
                    'type'=>'Voucher',
                    'name'=>'Tiket Nonton',
                    'group'=>array(
                    ),
                    'parentID'=>array(
                    ),
                    'id'=>'93',
            ),
            '99'=>array(
                    'type'=>'Voucher',
                    'name'=>'Travel',
                    'group'=>array(
                    ),
                    'parentID'=>array(
                    ),
                    'id'=>'95',
            ),
            '100'=>array(
                    'type'=>'Voucher',
                    'name'=>'Hotel',
                    'group'=>'Belanja',
                    'parentID'=>'80',
                    'id'=>'86',
            ),
            '101'=>array(
                    'type'=>'Voucher',
                    'name'=>'Karaoke',
                    'group'=>'Belanja',
                    'parentID'=>'80',
                    'id'=>'87',
            ),
            '102'=>array(
                    'type'=>'Voucher',
                    'name'=>'Salon',
                    'group'=>'Belanja',
                    'parentID'=>'80',
                    'id'=>'88',
            ),
        );
//        $key = self::recursive_array_search('102', $category);
//        cdbg::var_dump($key);
//        die();
        $category[] = array(
            'type' => 'Product',
            'name' => 'I LOVE INDONESIA',
            'group' => 'I LOVE INDONESIA',
            'parentID' => 0,
            'id' => '102'
        );
        // </editor-fold>
//        cdbg::var_dump($category);
//        die();
        $recategory = array();
        foreach ($category as $k => $v) {
            $parent_id = carr::get($v, 'parentID');
            $group = carr::get($v, 'group');
            if (is_array($parent_id)) {
                if (count($parent_id) > 0) {
                    $parent_id = carr::get($parent_id, 0);
                }
                else {
                    $parent_id = NULL;
                }
            }
            if (is_array($group)) {
                if (count($group) > 0) {
                    $group = carr::get($group, 0);
                }
                else {
                    $group = NULL;
                }
            }
            $arr_cat['category_id'] = carr::get($v, 'id');
            $arr_cat['parent_id'] = $parent_id;
            $arr_cat['name'] = carr::get($v, 'name');
            $arr_cat['type'] = carr::get($v, 'type');
            $arr_cat['group'] = $group;
            $recategory[] = $arr_cat;
        }
        cdbg::var_dump($recategory);
        die();
        $new = self::build_tree($category);
        cdbg::var_dump($new);
    }
    
    public function recursive_array_search($needle,$haystack) {
        foreach($haystack as $key=>$value) {
            $current_key=$key;
            if($needle===$value OR (is_array($value) && self::recursive_array_search($needle,$value) !== false)) {
                return $current_key;
            }
        }
        return false;
    }
    
    public function build_tree(array $array, $parentId = 0){
        $return = array();
        
        foreach ($array as $k => $v) {
            $product_category_id = carr::get($v, 'id', 0);
            $name = carr::get($v, 'name');
            $parent_id = carr::get($v, 'parentID');
            $group = carr::get($v, 'group');
            $type = carr::get($v, 'type');
            
            if (is_array($parent_id)) {
                if (count($parent_id) > 0) {
                    $parent_id = carr::get($parent_id, 0);
                }
                else {
                    $parent_id = NULL;
                }
            }
            if (is_array($group)) {
                if (count($group) > 0) {
                    $group = carr::get($group, 0);
                }
                else {
                    $group = NULL;
                }
            }
            
            $arr['id'] = $product_category_id;
            $arr['text'] = $name;            
            $arr['type'] = strtolower($type);            
            $arr['group'] = strtolower($group);
            if ($parent_id == $parentId) {
                $children = self::build_tree($array, $product_category_id);
                if ($children) {
                    $arr['subnav'] = $children;
                }
                $return[] = $arr;
            }
        }
        return $return;
    }
    
    public function build_kp_product_category() {
        // <editor-fold defaultstate="collapsed" desc="dummy data">
        $category = array(
                    '0'=>array(
                            'type'=>'Product',
                            'name'=>'Baterai',
                            'group'=>'Aksesoris HP',
                            'id'=>'33',
                    ),
                    '1'=>array(
                            'type'=>'Product',
                            'name'=>'Casing',
                            'group'=>'Aksesoris HP',
                            'id'=>'34',
                    ),
                    '2'=>array(
                            'type'=>'Product',
                            'name'=>'Charger',
                            'group'=>'Aksesoris HP',
                            'id'=>'32',
                    ),
                    '3'=>array(
                            'type'=>'Product',
                            'name'=>'Data Cable',
                            'group'=>'Aksesoris HP',
                            'id'=>'28',
                    ),
                    '4'=>array(
                            'type'=>'Product',
                            'name'=>'Earphone',
                            'group'=>'Aksesoris HP',
                            'id'=>'29',
                    ),
                    '5'=>array(
                            'type'=>'Product',
                            'name'=>'Gantungan HP',
                            'group'=>'Aksesoris HP',
                            'id'=>'37',
                    ),
                    '6'=>array(
                            'type'=>'Product',
                            'name'=>'Kartu Perdana',
                            'group'=>'Aksesoris HP',
                            'id'=>'27',
                    ),
                    '7'=>array(
                            'type'=>'Product',
                            'name'=>'Memory Card (SD)',
                            'group'=>'Aksesoris HP',
                            'id'=>'30',
                    ),
                    '8'=>array(
                            'type'=>'Product',
                            'name'=>'Power Bank',
                            'group'=>'Aksesoris HP',
                            'id'=>'31',
                    ),
                    '9'=>array(
                            'type'=>'Product',
                            'name'=>'Screen Guard',
                            'group'=>'Aksesoris HP',
                            'id'=>'35',
                    ),
                    '10'=>array(
                            'type'=>'Product',
                            'name'=>'Stylus',
                            'group'=>'Aksesoris HP',
                            'id'=>'36',
                    ),
                    '11'=>array(
                            'type'=>'Product',
                            'name'=>'Tongsis & Tomsis',
                            'group'=>'Aksesoris HP',
                            'id'=>'26',
                    ),
                    '12'=>array(
                            'type'=>'Product',
                            'name'=>'Cooling',
                            'group'=>'Aksesoris Komputer',
                            'id'=>'41',
                    ),
                    '13'=>array(
                            'type'=>'Product',
                            'name'=>'Keyboard',
                            'group'=>'Aksesoris Komputer',
                            'id'=>'39',
                    ),
                    '14'=>array(
                            'type'=>'Product',
                            'name'=>'Keyboard Protector',
                            'group'=>'Aksesoris Komputer',
                            'id'=>'42',
                    ),
                    '15'=>array(
                            'type'=>'Product',
                            'name'=>'Mouse',
                            'group'=>'Aksesoris Komputer',
                            'id'=>'38',
                    ),
                    '16'=>array(
                            'type'=>'Product',
                            'name'=>'Speaker',
                            'group'=>'Aksesoris Komputer',
                            'id'=>'40',
                    ),
                    '17'=>array(
                            'type'=>'Product',
                            'name'=>'Speaker',
                            'group'=>'Electronic',
                            'id'=>'23',
                    ),
                    '18'=>array(
                            'type'=>'Product',
                            'name'=>'Batu & Perhiasan',
                            'group'=>'Fashion',
                            'id'=>'10',
                    ),
                    '19'=>array(
                            'type'=>'Product',
                            'name'=>'Jam Tangan',
                            'group'=>'Fashion',
                            'id'=>'9',
                    ),
                    '20'=>array(
                            'type'=>'Product',
                            'name'=>'Parfum',
                            'group'=>'Fashion',
                            'id'=>'8',
                    ),
                    '21'=>array(
                            'type'=>'Product',
                            'name'=>'Tas',
                            'group'=>'Fashion',
                            'id'=>'6',
                    ),
                    '22'=>array(
                            'type'=>'Product',
                            'name'=>'Fotografi',
                            'group'=>'Hobby',
                            'id'=>'58',
                    ),
                    '23'=>array(
                            'type'=>'Product',
                            'name'=>'Mainan',
                            'group'=>'Hobby',
                            'id'=>'62',
                    ),
                    '24'=>array(
                            'type'=>'Product',
                            'name'=>'Musik',
                            'group'=>'Hobby',
                            'id'=>'57',
                    ),
                    '25'=>array(
                            'type'=>'Product',
                            'name'=>'Olahraga',
                            'group'=>'Hobby',
                            'id'=>'59',
                    ),
                    '26'=>array(
                            'type'=>'Product',
                            'name'=>'Otomotif',
                            'group'=>'Hobby',
                            'id'=>'60',
                    ),
                    '27'=>array(
                            'type'=>'Product',
                            'name'=>'Pajangan',
                            'group'=>'Hobby',
                            'id'=>'61',
                    ),
                    '28'=>array(
                            'type'=>'Product',
                            'name'=>'Dapur',
                            'group'=>'Household',
                            'id'=>'64',
                    ),
                    '29'=>array(
                            'type'=>'Product',
                            'name'=>'Jam',
                            'group'=>'Household',
                            'id'=>'66',
                    ),
                    '30'=>array(
                            'type'=>'Product',
                            'name'=>'Kebun',
                            'group'=>'Household',
                            'id'=>'65',
                    ),
                    '31'=>array(
                            'type'=>'Product',
                            'name'=>'Kursi',
                            'group'=>'Household',
                            'id'=>'68',
                    ),
                    '32'=>array(
                            'type'=>'Product',
                            'name'=>'Lampu',
                            'group'=>'Household',
                            'id'=>'69',
                    ),
                    '33'=>array(
                            'type'=>'Product',
                            'name'=>'Lemari',
                            'group'=>'Household',
                            'id'=>'70',
                    ),
                    '34'=>array(
                            'type'=>'Product',
                            'name'=>'Meja',
                            'group'=>'Household',
                            'id'=>'67',
                    ),
                    '35'=>array(
                            'type'=>'Product',
                            'name'=>'TV',
                            'group'=>'Household',
                            'id'=>'72',
                    ),
                    '36'=>array(
                            'type'=>'Product',
                            'name'=>'HP',
                            'group'=>'HP',
                            'id'=>'78',
                    ),
                    '37'=>array(
                            'type'=>'Product',
                            'name'=>'HP Bekas',
                            'group'=>'HP',
                            'id'=>'25',
                    ),
                    '38'=>array(
                            'type'=>'Product',
                            'name'=>'Rambut',
                            'group'=>'Kecantikan',
                            'id'=>'12',
                    ),
                    '39'=>array(
                            'type'=>'Product',
                            'name'=>'Tubuh',
                            'group'=>'Kecantikan',
                            'id'=>'14',
                    ),
                    '40'=>array(
                            'type'=>'Product',
                            'name'=>'Wajah',
                            'group'=>'Kecantikan',
                            'id'=>'13',
                    ),
                    '41'=>array(
                            'type'=>'Product',
                            'name'=>'Casing',
                            'group'=>'Komputer',
                            'id'=>'48',
                    ),
                    '42'=>array(
                            'type'=>'Product',
                            'name'=>'Laptop',
                            'group'=>'Komputer',
                            'id'=>'44',
                    ),
                    '43'=>array(
                            'type'=>'Product',
                            'name'=>'Memory',
                            'group'=>'Komputer',
                            'id'=>'47',
                    ),
                    '44'=>array(
                            'type'=>'Product',
                            'name'=>'Mother Board',
                            'group'=>'Komputer',
                            'id'=>'45',
                    ),
                    '45'=>array(
                            'type'=>'Product',
                            'name'=>'Optical Drive',
                            'group'=>'Komputer',
                            'id'=>'50',
                    ),
                    '46'=>array(
                            'type'=>'Product',
                            'name'=>'PC',
                            'group'=>'Komputer',
                            'id'=>'43',
                    ),
                    '47'=>array(
                            'type'=>'Product',
                            'name'=>'Printer',
                            'group'=>'Komputer',
                            'id'=>'52',
                    ),
                    '48'=>array(
                            'type'=>'Product',
                            'name'=>'Processor',
                            'group'=>'Komputer',
                            'id'=>'46',
                    ),
                    '49'=>array(
                            'type'=>'Product',
                            'name'=>'Scanner',
                            'group'=>'Komputer',
                            'id'=>'53',
                    ),
                    '50'=>array(
                            'type'=>'Product',
                            'name'=>'Software',
                            'group'=>'Komputer',
                            'id'=>'91',
                    ),
                    '51'=>array(
                            'type'=>'Product',
                            'name'=>'Sound Card',
                            'group'=>'Komputer',
                            'id'=>'54',
                    ),
                    '52'=>array(
                            'type'=>'Product',
                            'name'=>'Storage',
                            'group'=>'Komputer',
                            'id'=>'49',
                    ),
                    '53'=>array(
                            'type'=>'Product',
                            'name'=>'VGA',
                            'group'=>'Komputer',
                            'id'=>'51',
                    ),
                    '54'=>array(
                            'type'=>'Product',
                            'name'=>'Atasan',
                            'group'=>'Pakaian',
                            'id'=>'16',
                    ),
                    '55'=>array(
                            'type'=>'Product',
                            'name'=>'Bawahan',
                            'group'=>'Pakaian',
                            'id'=>'17',
                    ),
                    '56'=>array(
                            'type'=>'Product',
                            'name'=>'Gaun',
                            'group'=>'Pakaian',
                            'id'=>'18',
                    ),
                    '57'=>array(
                            'type'=>'Product',
                            'name'=>'Pakaian Bayi',
                            'group'=>'Pakaian',
                            'id'=>'55',
                    ),
                    '58'=>array(
                            'type'=>'Product',
                            'name'=>'Sepatu & Sandal',
                            'group'=>'Pakaian',
                            'id'=>'56',
                    ),
                    '59'=>array(
                            'type'=>'Product',
                            'name'=>'Buku',
                            'group'=>'Reading',
                            'id'=>'73',
                    ),
                    '60'=>array(
                            'type'=>'Product',
                            'name'=>'Komik',
                            'group'=>'Reading',
                            'id'=>'75',
                    ),
                    '61'=>array(
                            'type'=>'Product',
                            'name'=>'Majalah',
                            'group'=>'Reading',
                            'id'=>'74',
                    ),
                    '62'=>array(
                            'type'=>'Product',
                            'name'=>'Novel',
                            'group'=>'Reading',
                            'id'=>'76',
                    ),
                    '63'=>array(
                            'type'=>'Voucher',
                            'name'=>'Game',
                            'group'=> 'Reading',
                            'id'=>'83',
                    ),
                    '64'=>array(
                            'type'=>'Voucher',
                            'name'=>'Makanan',
                            'group'=> 'Reading',
                            'id'=>'79',
                    ),
                    '65'=>array(
                            'type'=>'Voucher',
                            'name'=>'Pulsa Elektrik',
                            'group'=> 'Reading',
                            'id'=>'84',
                    ),
                    '66'=>array(
                            'type'=>'Voucher',
                            'name'=>'Pulsa HP',
                            'group'=>array(
                            ),
                            'id'=>'82',
                    ),
                    '67'=>array(
                            'type'=>'Voucher',
                            'name'=>'Tiket Nonton',
                            'group'=>array(
                            ),
                            'id'=>'93',
                    ),
                    '68'=>array(
                            'type'=>'Voucher',
                            'name'=>'Hotel',
                            'group'=>'Belanja',
                            'id'=>'86',
                    ),
                    '69'=>array(
                            'type'=>'Voucher',
                            'name'=>'Karaoke',
                            'group'=>'Belanja',
                            'id'=>'87',
                    ),
                    '70'=>array(
                            'type'=>'Voucher',
                            'name'=>'Salon',
                            'group'=>'Belanja',
                            'id'=>'88',
                    ),
            );
        // </editor-fold>
        
        $product_category = array();
        $root = $this->unique_multidim_array($category, 'type');
        if (count($root) > 0) {
            foreach ($root as $root_k => $root_v) {
                $arr_product_category = array();
                $arr_product_category['type'] = carr::get($root_v, 'type');
                $arr_product_category['category_id'] = carr::get($root_v, 'id');
//                $arr_product_category['name'] = carr::get($root_v, 'name');
                $arr_product_category['name'] = carr::get($root_v, 'type');
                
                $subnav = array();                
                foreach ($category as $category_k => $category_v) {
                    $type = carr::get($category_v, 'type');
                    if ($type == carr::get($root_v, 'type')) {
                        $subnav[] = $category_v;
                    }
                }
                $subnav_1 = $this->unique_multidim_array($subnav, 'group');
                if (count($subnav_1) > 0) {
                    foreach ($subnav_1 as $subnav_1_k => $subnav_1_v) {
                        $arr_subnav_1 = array();
                        $arr_subnav_1['type'] = carr::get($subnav_1_v, 'type');
                        $arr_subnav_1['category_id'] = carr::get($subnav_1_v, 'id');
//                        $arr_subnav_1['name'] = carr::get($subnav_1_v, 'name');
                        $arr_subnav_1['name'] = carr::get($subnav_1_v, 'group');
                        // process
                        $subnav_2 = array();
                        foreach ($subnav as $subnav_k => $subnav_v) {
                            if (carr::get($subnav_1_v, 'group') == carr::get($subnav_v, 'group')){
                                $subnav_2[] = $subnav_v;
                            }
                        }
                        $arr_subnav_1['subnav'] = $subnav_2;
                        $all_subnav_1[] = $arr_subnav_1;
                    }
                }
                $arr_product_category['subnav'] = $all_subnav_1;
                $product_category[] = $arr_product_category;
            }
        }
        
        cdbg::var_dump($product_category);
        
        cdbg::var_dump($category);
    }
    
    
    public function unique_multidim_array($array, $key) { 
        $temp_array = array(); 
        $i = 0; 
        $key_array = array(); 

        foreach($array as $val) { 
            if (!in_array($val[$key], $key_array)) { 
                $key_array[$i] = $val[$key]; 
                $temp_array[$i] = $val; 
            } 
            $i++; 
        } 
        return $temp_array; 
    } 
    
    public function test_listener() {
        $app = CApp::instance();
        
        for($i=0; $i<4; $i++) {
            $container = $app->add_div('the-target-'.$i)->add_class('container')->custom_css('min-height', '300px');        
            $container->add_listener('ready')->add_handler('reload')->set_target('the-target-'.$i)->set_url(curl::base().'unit_test/eko/reload_container');
        }
        
        echo $app->render();
    }
    
    public function reload_container() {
        $app = CApp::instance();
        
        $app->add('the content is reload | '. rand(0, 10));
        
        echo $app->render();
    }
    
    public function cek_product($product_id = '') {
        if ($product_id > 0) {
            cdbg::var_dump(product::get_product($product_id));
        }
        else {
            echo 'add param product_id';
        }
    }
    
    public function json_to_php() {
        $json = '{ "err_code": "0", "err_message": "", "data": { "product": { "product_id": "516", "url_key": "handsoap-panjang-axi-dance", "product_code": "CHPA Axi Dance", "product_name": "Handsoap Panjang Axi Dance", "product_sku": "HPAXDAN", "weight": "1", "image_url": "http:\/\/admin.62hallfamily.local\/application\/admin62hallfamily\/default\/resources\/default\\image\\ProductImageParent\\20160205\\view_all\\default_image_ProductImageParent_20160205_1452079129559.jpg", "flag": { "hot": "0", "new": "0" }, "is_available": "1", "quick_overview": "", "stock": "0", "show_minimum_stock": "0", "overview": "
Bahan : 100% Katun Balita<\/span><\/p>\n", "spesification": "", "faq": "", "is_configurable": "1", "detail_price": { "sell_price_start": null, "sell_price_end": null, "sell_price": 40000, "sell_price_promo": 40000, "promo_text": 0 } }, "shipping_info": { "JAWA (2-14 hari kerja": { "city": { "all": true } }, "LUAR JAWA (14-28 hari kerja)": { "city": { "all": true } } }, "attribute": { "attribute_category_list": { "CLR": { "attribute_category_id": "1", "attribute_category_type": "select", "attribute_category_code": "CLR", "attribute_category_url_key": "color", "attribute_category_name": "Color update" }, "WRNBLT": { "attribute_category_id": "9", "attribute_category_type": "select", "attribute_category_code": "WRNBLT", "attribute_category_url_key": "warna_kombinasi", "attribute_category_name": "Warna Kombinasi" } }, "attribute": { "4": { "depth": 1, "attribute_category_id": "1", "attribute_category_type": "select", "attribute_category_code": "CLR", "attribute_category_url_key": "color", "attribute_category_name": "Color update", "attribute_id": "4", "attribute_code": "PT", "attribute_key": "putih", "attribute_url_key": "putih", "sub_attribute": { "47": { "depth": 2, "attribute_category_id": "9", "attribute_category_type": "select", "attribute_category_code": "WRNBLT", "attribute_category_url_key": "warna_kombinasi", "attribute_category_name": "Warna Kombinasi", "attribute_id": "47", "attribute_code": "PMR", "attribute_key": "putih_merah", "attribute_url_key": "putih_merah", "product_id": "509", "product_name": "Handsoap Panjang 0-3 bulan Axi Dance Putih Merah ", "price": { "price": 40000, "promo_price": 0 }, "sku": "HPADPM", "stock": "10" }, "48": { "depth": 2, "attribute_category_id": "9", "attribute_category_type": "select", "attribute_category_code": "WRNBLT", "attribute_category_url_key": "warna_kombinasi", "attribute_category_name": "Warna Kombinasi", "attribute_id": "48", "attribute_code": "PKN", "attribute_key": "putih_kuning", "attribute_url_key": "putih_kuning", "product_id": "510", "product_name": "Handsoap Panjang 0-3 bulan Axi Dance Putih Kuning", "price": { "price": 40000, "promo_price": 0 }, "sku": "HPADPK", "stock": "6" }, "49": { "depth": 2, "attribute_category_id": "9", "attribute_category_type": "select", "attribute_category_code": "WRNBLT", "attribute_category_url_key": "warna_kombinasi", "attribute_category_name": "Warna Kombinasi", "attribute_id": "49", "attribute_code": "PHJ", "attribute_key": "putih_hijau", "attribute_url_key": "putih_hijau", "product_id": "511", "product_name": "Handsoap Panjang 0-3 bulan Axi Dance Putih Hijau", "price": { "price": 40000, "promo_price": 0 }, "sku": "HPADPH", "stock": "6" }, "50": { "depth": 2, "attribute_category_id": "9", "attribute_category_type": "select", "attribute_category_code": "WRNBLT", "attribute_category_url_key": "warna_kombinasi", "attribute_category_name": "Warna Kombinasi", "attribute_id": "50", "attribute_code": "PTBR", "attribute_key": "putih_biru", "attribute_url_key": "putih_biru", "product_id": "512", "product_name": "Handsoap Panjang 0-3 bulan Axi Dance Putih Biru", "price": { "price": 40000, "promo_price": 0 }, "sku": "HPAPB", "stock": "16" } } } } }, "session_id": "session20161201112724583fa6accd8cd" } }';
        
        $jsonphp = json_decode($json);
        cdbg::var_dump($jsonphp);
    }
}
