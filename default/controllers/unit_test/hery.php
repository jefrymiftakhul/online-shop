<?php

class Hery_Controller extends CController {

    public function abc() {
        $db = CDatabase::instance();
        $q = "select * from product where product_data_type='configurable' and status>0 and is_active>0 limit 40";
        $r = $db->query($q);
        foreach ($r as $row) {
            echo $row->name;
            echo "<br/>";
            $q = "select count(*) from product where name=" . $db->escape($row->name) . " and createdby='import_product'";
            $total_insert_import = cdbutils::get_value($q);
            echo "Total insert import " . $total_insert_import . " <br/>";
            echo "<br/>";
            echo "<br/>";
        }
    }

    public function excel($csv_path = null, $org_id = null) {
        $delimiter = ',';
        if ($csv_path == null) {
            $csv_path = DOCROOT . 'application/adminittronmall/paroparoshop/upload/import/product/20170810/13/productexport_excelcsvs_paroparoshop_20170810014036_excel_simple.csv';
        }
        if ($org_id == null) {
            $org_id = CF::org_id();
        }
        $err_code = 0;
        $err_message = "";
        $data = array();
        $max_variant = 15;


        $app = CApp::instance();
        $user = $app->user();
        $db = CDatabase::instance();
        if($user==null) {
            $user = new stdClass();
            $user->username='paroparoshop';
        }
        
        if ($err_code == 0) {
            if ($org_id == null) {
                $err_code++;
                $err_message = "Org is null";
            }
        }
        if ($err_code == 0) {
            if ($user == null) {
                $err_code++;
                $err_message = "User session is ended, please login";
            }
        }
        if ($err_code == 0) {
            if (($handle = fopen($csv_path, "r")) !== FALSE) {
                $i = 0;
                $err_line = 0;
                $err_line_message = "";
                $fatal_error = false;
                $total_error_line = 0;
                $all_error = array();
                while (($lines = fgetcsv($handle, 10000, $delimiter)) !== FALSE) {
                    $err_line = 0;
                    $err_line_message = "";
                    if (count($lines) < 13) {
                        $err_line++;
                        $err_line_message = "Format CSV salah";
                        if ($i == 0) {
                            $fatal_error = true;
                            break;
                        }
                    }

                    $sku = carr::get($lines, 0);
                    $vendor_name = carr::get($lines, 1);
                    $product_name = carr::get($lines, 2);
                    $product_category_code = carr::get($lines, 3);
                    $weight = carr::get($lines, 4);
                    $description = carr::get($lines, 5);
                    $new_product_date = carr::get($lines, 6);
                    $is_hot_item = carr::get($lines, 7);
                    $is_available = carr::get($lines, 8);
                    $product_tag = carr::get($lines, 9);
                    $vidio_link = carr::get($lines, 10);
                    $sell_price_nta = carr::get($lines, 11);
                    $sell_price = carr::get($lines, 12);
                    $stock = carr::get($lines, 13);

                    $overview = null;

                    $offset = 13;
                    $variant_name = array();
                    $variant_color = array();
                    $variant_price = array();
                    $variant_stock = array();
                    for ($j = 0; $j < $max_variant; $j++) {
                        $inc = ($i * 4) + 1;
                        $variant_name[$j] = carr::get($lines, $offset + ($j * 4) + 1);
                        $variant_color[$j] = carr::get($lines, $offset + ($j * 4) + 2);
                        $variant_price[$j] = carr::get($lines, $offset + ($j * 4) + 3);
                        $variant_stock[$j] = carr::get($lines, $offset + ($j * 4) + 4);
                    }


                    // <editor-fold defaultstate="collapsed" desc="Validation Baris Excel">
                    if ($err_line == 0) {
                        if (strlen($sku) == 0) {
                            $err_line++;
                            $err_line_message = "SKU Kosong";
                        }
                    }
                    if ($err_line == 0) {
                        if (strlen($vendor_name) == 0) {
                            $err_line++;
                            $err_line_message = "Vendor Kosong";
                        }
                    }
                    if ($err_line == 0) {
                        if (strlen($product_name) == 0) {
                            $err_line++;
                            $err_line_message = "Nama Produk Kosong";
                        }
                    }
                    if ($err_line == 0) {
                        if (strlen($product_category_code) == 0) {
                            $err_line++;
                            $err_line_message = "Kategori Produk Kosong";
                        }
                    }
                    if ($err_line == 0) {
                        if (strlen($weight) == 0) {
                            $err_line++;
                            $err_line_message = "Berat Produk Kosong";
                        }
                    }
                    if ($err_line == 0) {
                        if (strlen($is_hot_item) == 0) {
                            $err_line++;
                            $err_line_message = "Hot Produk Kosong, (isikan dengan NO atau YES)";
                        }
                    }
                    if ($err_line == 0) {
                        if (strlen($is_available) == 0) {
                            $err_line++;
                            $err_line_message = "Ketersediaan Produk Kosong, (isikan dengan NO atau YES)";
                        }
                    }
                    if ($err_line == 0) {
                        if (strlen($sell_price_nta) == 0) {
                            $err_line++;
                            $err_line_message = "Sell Price NTA Produk Kosong, (isikan dengan Angka)";
                        }
                    }
                    if ($err_line == 0) {
                        if ($sell_price_nta <= 0) {
                            $err_line++;
                            $err_line_message = "Sell Price NTA Produk tidak boleh 0 atau dibawah 0";
                        }
                    }

                    $product_category_id = null;
                    if ($err_line == 0) {
                        $product_category_id = cdbutils::get_value('select product_category_id from product_category where org_id=' . CF::org_id() . ' and name=' . $db->escape(trim($product_category_code)) . ' and status>0 ');
                        if ($product_category_id == null) {
                            $err_line++;
                            $err_line_message = "Kategori Produk [" . $product_category_code . "] Tidak Ditemukan Dalam System";
                        }
                    }
                    $vendor_id = null;
                    if ($err_line == 0) {
                        $vendor_id = cdbutils::get_value('select vendor_id from vendor where org_id=' . CF::org_id() . ' and name = ' . $db->escape($vendor_name) . ' and status=1 ');
                        if ($vendor_id == null) {
                            $err_line++;
                            $err_line_message = "Vendor [" . $vendor_name . "] Tidak Ditemukan Dalam System";
                        }
                    }


                    //check variant
                    $total_variant = 0;
                    if ($err_line == 0) {
                        for ($j = 0; $j < $max_variant; $j++) {
                            if (strlen($variant_name[$j]) > 0 || strlen($variant_color[$j]) > 0 || strlen($variant_price[$j]) > 0 || strlen($variant_stock[$j]) > 0) {
                                $total_variant++;
                                if ($err_line == 0) {
                                    if (strlen($variant_name[$j]) == 0) {
                                        $err_line++;
                                        $err_line_message = "Nama Varian " . $j . " masih kosong";
                                    }
                                    if (strlen($variant_color[$j]) == 0) {
                                        $err_line++;
                                        $err_line_message = "Warna Varian " . $j . " masih kosong";
                                    }
                                    if (strlen($variant_price[$j]) == 0) {
                                        $err_line++;
                                        $err_line_message = "Harga Varian " . $j . " masih kosong";
                                    }
                                    if (strlen($variant_stock[$j]) == 0) {
                                        $err_line++;
                                        $err_line_message = "Stock Varian " . $j . " masih kosong";
                                    }
                                    if (strlen($variant_price[$j]) > 0) {
                                        if ($variant_price[$j] < $sell_price_nta) {
                                            $err_line++;
                                            $err_line_message = "Harga Varian " . $j . " tidak boleh dibawah NTA";
                                        }
                                    }
                                }
                                if ($err_line > 0) {
                                    break;
                                }
                            }
                        }
                    }
                    if ($err_line == 0) {
                        if ($total_variant == 0) {
                            //maka produk simple
                            if ($err_line == 0) {
                                if (strlen($sell_price) == 0) {
                                    $err_line++;
                                    $err_line_message = "Harga produk kosong";
                                }
                            }
                            if ($err_line == 0) {
                                if (strlen($stock) == 0) {
                                    $err_line++;
                                    $err_line_message = "Stock produk kosong";
                                }
                            }
                            if ($err_line == 0) {
                                if (strlen($stock) > 0 && $stock < 0) {
                                    $err_line++;
                                    $err_line_message = "Stock produk tidak boleh dibawah 0";
                                }
                            }
                            if ($err_line == 0) {
                                if ($sell_price < $sell_price_nta) {
                                    $err_line++;
                                    $err_line_message = "Harga produk tidak boleh dibawah NTA";
                                }
                            }
                        }
                    }
                    $current_product = null;
                    //check status product jika sku sudah ada
                    if ($err_line == 0) {
                        $current_product = cdbutils::get_row("select * from product where org_id=" . CF::org_id() . " and sku=" . $db->escape($sku) . " and status=1 ");
                        if ($current_product != null) {
                            //product ada, check active atau confirm product
//                            if ($current_product->is_active == 0) {
//                                $err_line++;
//                                $err_line_message = "SKU Sudah ada, hanya product dalam keadaan Non Active, silahkan aktifkan produk tersebut atau pilih SKU yang lain untuk menambahkan produk baru";
//                            }
//                            if ($current_product->status_confirm != 'CONFIRMED') {
//                                $err_line++;
//                                $err_line_message = "SKU Sudah ada, hanya product dalam keadaan belum dikonfirmasi, silahkan konfirmasi produk tersebut atau pilih SKU yang lain untuk menambahkan produk baru";
//                            }
                        }
                    }


                    // </editor-fold>
                    //process
                    $is_simple = $total_variant == 0;
                    $id = null;
                    if ($current_product != null) {
                        $id = $current_product->product_id;
                    }

                    if($err_line ==0) {
                        $db->begin();
                        try {
                            $data_product['org_id'] = $org_id;
                            $data_product['vendor_id'] = $vendor_id;
                            $data_product['country_manufacture'] = '';
                            $data_product['country_manufacture_id'] = null;
                            $data_product['product_type_id'] = '1';
                            $data_product['product_category_id'] = $product_category_id;
                            $data_product['name'] = $product_name;
                            $data_product['is_stock'] = '1';
                            $data_product['weight'] = $weight;
                            $data_product['currency'] = 'idr';
                            $data_product['new_product_date'] = date("Y-m-d", strtotime($new_product_date));
                            $data_product['is_hot_item'] = $is_hot_item;
                            $data_product['is_package_product'] = 0;
                            $data_product['visibility'] = 'catalog_search';
                            $data_product['is_available'] = $is_available;
                            $data_product['status_product'] = 'FINISHED';
                            $data_product['show_minimum_stock'] = '0';
                            $data_product['alert_minimum_stock'] = '0';
                            $data_product['description'] = (strlen(trim($description)) == 0) ? '-' : $description;
                            $data_product['overview'] = (strlen(trim($overview)) == 0) ? '-' : $overview;
                            $data_product['product_tag'] = $product_tag;
                            $data_product['vidio_link'] = $vidio_link;
                            $data_product['spesification'] = '';
                            $data_product['faq'] = '';
                            $data_product['is_voucher'] = '0';
                            $data_product['hot_until'] = date('Y-m-d H:i:s');
                            $url_key_ori = cstr::sanitize($product_name);
                            //check for exists urlkey
                            $is_duplicate = true;
                            $i = 0;
                            $url_key = $url_key_ori;
                            while ($is_duplicate) {
                                $url_key = $url_key_ori;
                                if ($i > 0) {
                                    $url_key = $url_key_ori . "-" . ($i + 1);
                                }
                                $q = "select * from product where url_key=" . $db->escape($url_key);
                                if (strlen($id) > 0) {
                                    $q .= " and product_id <> " . $db->escape($id);
                                }
                                $qcheck = cdbutils::get_row($q);
                                if ($qcheck != null) {
                                    $i++;
                                } else {
                                    $is_duplicate = false;
                                }
                            }
                            $data_product['url_key'] = $url_key;


                            //pricing
                            $data_product['ho_upselling'] = 0;
                            $data_product['sell_price_nta'] = $sell_price_nta;
                            $data_product['sell_price'] = ctransform::unformat_currency($sell_price);
                            $status_confirm = "CONFIRMED";
                            if (ccfg::get('have_confirm_product') > 0) {
                                $status_confirm = "PENDING";
                            }
                            $data_product['status_confirm'] = $status_confirm;
                            $data_product['product_data_type'] = 'simple';
                            $data_product['attribute_set_id'] = null;
                            $data_product['stock'] = $stock;
                            if ($is_available == 0) {
                                $data_product['stock'] = 999;
                            }
                            $data_product['input_version'] = 'im_product_warna';

                            $is_insert = $id == null;

                            $product_id = null;
                            if ($is_insert) {
                                $data_product['sku'] = generate_code::generate_product_sku();
                                $data_product['code'] = generate_code::generate_product_code();
                                $data_product['created'] = date('Y-m-d H:i:s');
                                $data_product['createdby'] = $user->username;
                                $data_product['updated'] = date('Y-m-d H:i:s');
                                $data_product['updatedby'] = $user->username;

                                $insert_product = $db->insert('product', $data_product);
                                $product_id = $insert_product->insert_id();
                            } else {

                                $data_product['created'] = date('Y-m-d H:i:s');
                                $data_product['createdby'] = $user->username;
                                $data_product['updated'] = date('Y-m-d H:i:s');
                                $data_product['updatedby'] = $user->username;

                                $db->update('product', $data_product, array('product_id' => $id));
                                $product_id = $id;
                            }
                            $db->query('update product set status=0 where parent_id=' . $db->escape($product_id));
                            if ($total_variant > 0) {
                                $product_price_first = $variant_price[0];
                                //ambil category code VARIANT
                                $attribute_category_id = cdbutils::get_value("select attribute_category_id from attribute_category where code='paro-variant'");
                                $attribute_category_color_id = cdbutils::get_value("select attribute_category_id from attribute_category where code='paro-warna'");
                                $attribute_set_detail = cdbutils::get_row("select * from attribute_set_detail where attribute_category_id=" . $db->escape($attribute_category_id));
                                $attribute_set_detail_id = $attribute_set_detail->attribute_set_detail_id;
                                $attribute_set_id = $attribute_set_detail->attribute_set_id;

                                $data_product_update = array();
                                $data_product_update['product_data_type'] = 'configurable';
                                $data_product_update['attribute_set_id'] = $attribute_set_id;
                                $data_product_update['sell_price'] = ctransform::unformat_currency($product_price_first);
                                $db->update('product', $data_product_update, array('product_id' => $product_id));
                            }

                            $check_data_product_attribute_category = cdbutils::get_row("select * from product_attribute_category where status>0 and product_id=" . $db->escape($product_id) . " and attribute_category_id=" . $db->escape($attribute_category_id) . " and attribute_set_id=" . $db->escape($attribute_set_id));
                            if ($check_data_product_attribute_category === null) {

                                //insert product_attribute_category
                                $data_product_attribute_category = array();
                                $data_product_attribute_category['product_id'] = $product_id;
                                $data_product_attribute_category['attribute_category_id'] = $attribute_category_id;
                                $data_product_attribute_category['attribute_set_id'] = $attribute_set_id;

                                $this->data_default('insert', $data_product_attribute_category);
                                $r_pac = $db->insert('product_attribute_category', $data_product_attribute_category);

                                $data_product_attribute_category = array();
                                $data_product_attribute_category['product_id'] = $product_id;
                                $data_product_attribute_category['attribute_category_id'] = $attribute_category_color_id;
                                $data_product_attribute_category['attribute_set_id'] = $attribute_set_id;

                                $this->data_default('insert', $data_product_attribute_category);
                                $r_pac = $db->insert('product_attribute_category', $data_product_attribute_category);
                            }

                            $ii = 0;

                            $row_product_new = cdbutils::get_row("select * from product where product_id=" . $db->escape($product_id));
                            $image_parent_filename = $row_product_new->filename;
                            $image_parent_image_name = $row_product_new->image_name;
                            $image_parent_file_path = $row_product_new->file_path;


                            for ($j = 0; $j <= $max_variant; $j++) {
                                //insert yang simple
                                $variant_name_i = $variant_name[$j];
                                $variant_color_i = $variant_color[$j];
                                $variant_price_i = $variant_price[$j];
                                $variant_stock_i = $variant_stock[$j];
                                if (strlen($variant_stock_i)) {
                                    $variant_stock_i = 0;
                                }
                                //check attribute/ insert not exists
                                $row_attribute = cdbutils::get_row("select * from attribute where attribute_category_id=" . $db->escape($attribute_category_id) . " and `key`=" . $db->escape($variant_name_i));
                                $attribute_id = null;
                                if ($row_attribute == null) {
                                    //insert attribute
                                    $data_attribute = array();
                                    $data_attribute['attribute_category_id'] = $attribute_category_id;
                                    $data_attribute['key'] = $variant_name_i;
                                    $data_attribute['url_key'] = cstr::sanitize($variant_name_i);
                                    $data_attribute['is_active'] = '1';
                                    $this->data_default('insert', $data_attribute);

                                    $r = $db->insert('attribute', $data_attribute);
                                    $attribute_id = $r->insert_id();
                                } else {
                                    $attribute_id = $row_attribute->attribute_id;
                                }

                                $row_attribute_color = cdbutils::get_row("select * from attribute where attribute_category_id=" . $db->escape($attribute_category_color_id) . " and `key`=" . $db->escape($variant_color_i));
                                $attribute_color_id = null;
                                if ($row_attribute_color == null) {
                                    //insert attribute
                                    $data_attribute = array();
                                    $data_attribute['attribute_category_id'] = $attribute_category_color_id;
                                    $data_attribute['key'] = $variant_color_i;
                                    $data_attribute['url_key'] = cstr::sanitize($variant_color_i);
                                    $data_attribute['is_active'] = '1';
                                    $this->data_default('insert', $data_attribute);

                                    $r = $db->insert('attribute', $data_attribute);
                                    $attribute_color_id = $r->insert_id();
                                } else {
                                    $attribute_color_id = $row_attribute_color->attribute_id;
                                }

                                //insert produk simple
                                $data_product['org_id'] = $org_id;
                                $data_product['vendor_id'] = $vendor_id;
                                $data_product['parent_id'] = $product_id;
                                $data_product['attribute_set_id'] = $attribute_set_id;
                                $data_product['country_manufacture'] = '';
                                $data_product['country_manufacture_id'] = null;
                                $data_product['product_type_id'] = '1';
                                $data_product['product_category_id'] = $product_category_id;
                                $data_product['sku'] = generate_code::generate_product_sku();

                                $data_product['code'] = generate_code::generate_product_code();

                                $data_product['name'] = $product_name . " " . $variant_name_i;
                                $data_product['is_stock'] = '1';
                                $data_product['stock'] = $variant_stock_i;
                                $data_product['weight'] = $weight;
                                $data_product['currency'] = 'idr';
                                $data_product['product_data_type'] = 'simple';
                                $data_product['new_product_date'] = date("Y-m-d", strtotime($new_product_date));
                                $data_product['is_hot_item'] = $is_hot_item;
                                $data_product['is_package_product'] = 0;
                                $data_product['visibility'] = 'not_visibility_individually';
                                $data_product['is_available'] = $is_available;
                                $data_product['status_product'] = 'FINISHED';
                                $data_product['show_minimum_stock'] = '0';
                                $data_product['alert_minimum_stock'] = '0';
                                $data_product['description'] = (strlen(trim($description)) == 0) ? '-' : $description;
                                $data_product['overview'] = '';
                                $data_product['product_tag'] = $product_tag;
                                $data_product['vidio_link'] = $vidio_link;
                                $data_product['spesification'] = '';
                                $data_product['faq'] = '';
                                $data_product['is_voucher'] = '0';
                                $data_product['hot_until'] = date('Y-m-d H:i:s');
                                $data_product['filename'] = $image_parent_filename;
                                $data_product['image_name'] = $image_parent_image_name;
                                $data_product['file_path'] = $image_parent_file_path;
                                $url_key_ori = cstr::sanitize($product_name . " " . $variant_name_i);
                                //check for exists urlkey
                                $is_duplicate = true;
                                $i = 0;
                                $url_key = $url_key_ori;
                                while ($is_duplicate) {
                                    $url_key = $url_key_ori;
                                    if ($i > 0) {
                                        $url_key = $url_key_ori . "-" . ($i + 1);
                                    }
                                    $q = "select * from product where status>0 and url_key=" . $db->escape($url_key);
                                    if (strlen($id) > 0) {
                                        $q .= " and product_id <> " . $db->escape($id);
                                    }
                                    $qcheck = cdbutils::get_row($q);
                                    if ($qcheck != null) {
                                        $i++;
                                    } else {
                                        $is_duplicate = false;
                                    }
                                }
                                $data_product['url_key'] = $url_key;


                                //pricing
                                $data_product['ho_upselling'] = 0;
                                $data_product['sell_price_nta'] = $sell_price_nta;
                                $data_product['sell_price'] = ctransform::unformat_currency($variant_price_i);
                                $status_confirm = "CONFIRMED";
                                if (ccfg::get('have_confirm_product') > 0) {
                                    $status_confirm = "PENDING";
                                }
                                $data_product['status_confirm'] = $status_confirm;
                                $data_product['input_version'] = 'im_product_warna';
                                if ($is_available == 0) {
                                    $data_product['stock'] = 999;
                                }

                                //check apakah ada product ini yang statusnya 0, 
                                //kalau ada update kalau ga ada insert
                                $data_product['status'] = 1;

                                //mencoba cari product_id_simple

                                $q = 'select p.product_id from product as p ';
                                $q.=' inner join product_group_attribute as pga1 on pga1.product_id=p.product_id and pga1.attribute_id=' . $db->escape($attribute_id) . ' and pga1.attribute_category_id=' . $db->escape($attribute_category_id);
                                $q.=' inner join product_group_attribute as pga2 on pga2.product_id=p.product_id and pga2.attribute_id=' . $db->escape($attribute_color_id) . ' and pga2.attribute_category_id=' . $db->escape($attribute_category_color_id);
                                $q.=' where pga1.status>0 and pga2.status>0 and p.parent_id=' . $db->escape($product_id);

                                $product_simple_id = cdbutils::get_value($q);

                                if ($product_simple_id !== null) {
                                    $this->data_default('update', $data_product);
                                    $r_product = $db->update('product', $data_product, array('product_id' => $product_simple_id));
                                } else {
                                    $this->data_default('insert', $data_product);
                                    $r_product = $db->insert('product', $data_product);
                                    $product_simple_id = $r_product->insert_id();

                                    $data_product_group_attribute = array();
                                    $data_product_group_attribute['vendor_id'] = $vendor_id;
                                    $data_product_group_attribute['attribute_category_id'] = $attribute_category_id;
                                    $data_product_group_attribute['attribute_id'] = $attribute_id;
                                    $data_product_group_attribute['product_id'] = $product_simple_id;
                                    $this->data_default('insert', $data_product_group_attribute);
                                    $r_pgs = $db->insert('product_group_attribute', $data_product_group_attribute);

                                    //insert warna
                                    $data_product_group_attribute = array();
                                    $data_product_group_attribute['vendor_id'] = $vendor_id;
                                    $data_product_group_attribute['attribute_category_id'] = $attribute_category_color_id;
                                    $data_product_group_attribute['attribute_id'] = $attribute_color_id;
                                    $data_product_group_attribute['product_id'] = $product_simple_id;
                                    $this->data_default('insert', $data_product_group_attribute);
                                    $r_pgs = $db->insert('product_group_attribute', $data_product_group_attribute);


                                    $diff_price = $variant_price_i - $product_price_first;

                                    $data_attribute_price = array();
                                    $data_attribute_price['product_parent_id'] = $product_id;
                                    $data_attribute_price['product_id'] = $product_simple_id;
                                    $data_attribute_price['attribute_id'] = $attribute_id;
                                    $data_attribute_price['price'] = $diff_price;
                                    $this->data_default('insert', $data_attribute_price);
                                    $r_ap = $db->insert('attribute_price', $data_attribute_price);
                                }
                            }
                            product::recalculate_stock_configurable($product_id);
                            recalculate_price::product($product_id);
                        } catch (Exception $ex) {
                            $err_line++;
                            $err_line_message = "Error, error ketika menambah produk, " . $ex->getMessage();
                        }
                    }
                    if($err_line==0) {
                        $db->commit();
                    } else {
                        $db->rollback();
                        $all_error[]="Error pada baris ".($i+1) .",". $err_line_message;
                    }
                    $i++;
                }
            }
            fclose($handle);
        }
        if($err_code>0) {
            throw new Exception($err_message);
        }
        $result=array();
        $result['err_code']=$err_code;
        $result['total_line_error']=count($all_error);
        $result['line_error']=$all_error;
        
        
        cdbg::var_dump($result);
        return $result;
        
    }

    public function pushnotif() {
        $title = 'Title';
        $message = 'Testing Testing Testing Testing';
        $registration_id = 'cRhyo3EuBjg:APA91bEizov6dzIIcaX5dKyMYjcK6EWJimWGBLp0oRqU-7qFEjDynOyBKEpaJ8Exhp_fF8Q_Cd0XY8swcjVhmspf7azsQFzYmWOXmALYAJtP0Wga9p-bK3U2roAKg0a9rAtZsPLP7yXd';
        $api_access_key = 'AIzaSyCPiyoLNOL_apPp_gD5H3TYUVo0PrS9NV8';

        $msg = array(
            'body' => $message,
            'title' => $title,
            'vibrate' => 1,
            'sound' => 1,
        );
        $fields = array(
            'registration_ids' => array($registration_id),
            'notification' => $msg
        );

//baru
        $detail = $message;
        $data = array();
        $type = 'notification';
        $reg_id = array($registration_id);
        $api_access_key = 'AAAAo-lg8Zc:APA91bFczVCBInPzih2gUxmveg_anR19zAZFyBoPZ5MDVlfb0JKdKBOrO4EQFMrmxIgsUMZ4HgFGm_M1gPH-qKBIT48bKuPcW45Ix2YzcTLCM3swV8oafDYxnt_spOsuyY1eEq2Wp-p_';

        /*
          $fields = array(
          'registration_ids' => $reg_id,
          'data' => array("message" => $detail, "data" => $data, "title" => $title, "type" => $type),
          );
         */

        $headers = array(
            'Authorization: key=' . $api_access_key,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
    }

    public function session() {
        cdbg::var_dump($_SESSION);
        die();
    }

    public function pgsession() {
        $session = CApiClientSession::instance('PG');
        $data = $session->get('transaction');
        cdbg::var_dump($data);
        die();
    }

    public function email() {
        $subject = 'Test Email 2';
        $to = 'hery@ittron.co.id';
        $message = "Testing";

        cmail::send_smtp($to, $subject, $message);
        echo "success";
    }

    public function index() {
        $subject = 'Test Email FrontEnd';
        $to = 'hery@ittron.co.id';
        $message = "Testing";

        cmail::send_smtp($to, $subject, $message);
        echo "success";
    }

    public function error() {
        echo $a;
    }

    public function send_email_transaction() {
        if (ccfg::get('new_email')) {
            email::send('IMTransaction', '12767');
        } else {
            $this->send_transaction('order', 'B9LKN531');
        }
    }

    public function recalculate_configurable() {
        $db = CDatabase::instance();
        $q = "select * from product where parent_id is null and org_id='620776'";
        $r = $db->query($q);
        foreach ($r as $row) {
            product::recalculate_stock_configurable($row->product_id);
        }
    }

}
