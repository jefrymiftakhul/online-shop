<?php

/**
 * 
 */
class Ali_Controller extends CController {

    public function __construct() {
        parent::__construct();
    }

    public function __inject_product($generate = false) {

        $db = CDatabase::instance();
        //$logs = 'D:/data/textData/';
//        $logs = APPPATH.'ittronmall/lasvegas/data_product/';
        $logs = 'D:\data_lasvegas/';
        $data = array();
        $err_code = 0;
        $err_message = '';

        if ($err_code == 0) {

            if (!is_dir($logs)) {
                $err_code++;
                $err_message = clang::__('Data not found.');
            }

            if ($err_code == 0) {
                $counter = 1;
                foreach (scandir($logs) as $filename) {
                    $exclude_file = array('.', '..');
                    if (!in_array($filename, $exclude_file)) {
                        $path = $logs . $filename;
//                        cdbg::var_dump($path);
//                        die;
                        if (is_file($path)) {
                            $data = file_get_contents($path);
                            $data = explode(PHP_EOL, $data);
//                            cdbg::var_dump($data);
//                            die;
                            $org_id = CF::org_id();
                            if (count($data) > 0) {
                                try {
//                                    cdbg::var_dump(count($data));
//                                    die;
                                    foreach ($data as $key => $value) {
                                        $db->begin();
                                        $err_log['err_code'] = 0;
                                        $value_arr = explode("\t", $value);
//                                        if (carr::get($value_arr, 4) == 'AXIOO PICOPHONE M4S B') {
//                                            cdbg::var_dump($value_arr);
//                                        }

                                        $product_category = carr::get($value_arr, 0);
                                        $product_category_id = 19;
//                                        $product_category_id = cdbutils::get_value('select product_category_id from product_category where name ="' . $product_category . '"');
                                        $country_manufactur = carr::get($value_arr, 1);
//                                        $country_manufactur_id = cdbutils::get_value('select country_id from country where name ="' . $country_manufactur . '"');
                                        $country_manufactur_id = 94;
                                        $sku = carr::get($value_arr, 2);
                                        $code = carr::get($value_arr, 3);
                                        $name = carr::get($value_arr, 4);
                                        $url_key = cstr::sanitize($name);
                                        $sell_price = carr::get($value_arr, 5);
                                        $weight = carr::get($value_arr, 6);
                                        $stock = carr::get($value_arr, 7);
                                        $short_description = carr::get($value_arr, 8);
                                        $overview = carr::get($value_arr, 9);
                                        $spesification = $this->porduct_spesification(carr::get($value_arr, 10));
                                        $hot = carr::get($value_arr, 12);
                                        $hot_until = carr::get($value_arr, 13);
                                        $show = carr::get($value_arr, 14);
                                        $alert = carr::get($value_arr, 15);
                                        $image_1 = carr::get($value_arr, 16);
//                                        cdbg::var_dump($image_1);
//                                        cdbg::var_dump($value_arr);
//                                        die;
                                        $image_2 = carr::get($value_arr, 17);
                                        $image_3 = carr::get($value_arr, 18);
                                        $image_4 = carr::get($value_arr, 19);
                                        $image_5 = carr::get($value_arr, 20);
                                        $promo_price = carr::get($value_arr, 21);
                                        $expired_promo = carr::get($value_arr, 22);
                                        $data_p = array(
                                            'org_id' => $org_id,
                                            'product_type_id' => 1,
                                            'product_category_id' => $product_category_id,
                                            'country_manufacture_id' => $country_manufactur_id,
                                            'attribute_set_id' => '',
                                            'country_manufacture' => $country_manufactur,
                                            'code' => $code,
                                            'name' => $name,
                                            'url_key' => $url_key,
                                            'currency' => 'IDR',
                                            'sell_price_nta' => 0,
                                            'sell_price' => $sell_price,
                                            'weight' => $weight,
                                            'stock' => $stock,
                                            'description' => $short_description,
                                            'overview' => $overview,
                                            'spesification' => $spesification,
                                            'is_hot_item' => $hot,
                                            'hot_until' => $hot_until,
                                            'show_minimum_stock' => $show,
                                            'alert_minimum_stock' => $alert,
                                            'sku' => $sku,
                                            'product_data_type' => '',
                                            'status_product' => 'CONFIRMED',
                                            'filename' => '',
                                            'image_name' => '',
                                            'file_path' => $image_1,
                                            'visibility' => 'catalog_search',
                                            'status_confirm' => 'CONFIRMED',
                                            'created' => date("Y-m-d H:i:s"),
                                            'createdby' => 'auto_inject_product',
                                            'updated' => date("Y-m-d H:i:s"),
                                            'updatedby' => 'auto_inject_product',
                                            'status' => 1,
                                        );
                                        $generate_image = false;
                                        if ($generate == true) {
//                                            cdbg::var_dump($data_p);
//                                            die;
                                            $check_duplicate_name = cdbutils::get_value("select sku from product where status > 0 and sku='" . $sku . "'");
                                            if ($check_duplicate_name == null) {

                                                try {
                                                    $r = $db->insert('product', $data_p);
                                                    $product_id = $r->insert_id();
                                                } catch (Exception $e) {
                                                    $err_log[]['err_msg_product'] = 'input product [' . $name . '] failed';
                                                    $err_code++;
                                                    $err_message = 'System Fail. ' . $e->getMessage();
                                                }

                                                $generate_image = true;
                                            } else {
                                                $err_code++;
                                                $generate_image = false;
                                                $err_log['err_code'] ++;
                                                $err_log[]['err_msg_product'] = 'input product [' . $name . '] ; SKU[' . $sku . '] failed ,  product sku duplicate';
                                                $err_log[]['detail'] = 'SKU[' . $sku . ']; Product_category_id[' . $product_category . '];';

                                                continue;
                                            }
                                        } else {
                                            $generate_image = true;
                                            $product_id = 0;
                                            $arr_pro[] = $data_p;
                                        }

                                        $arr_image = array($image_2, $image_3, $image_4, $image_5);
                                        if ($generate_image == true) {
                                            foreach ($arr_image as $key => $value) {
                                                if (strlen($value) == 0) {
                                                    continue;
                                                } else {
                                                    try {
                                                        $data_image = array(
                                                            'product_id' => $product_id,
                                                            'image_path' => $value,
                                                            'created' => date("Y-m-d H:i:s"),
                                                            'createdby' => 'auto_inject_product',
                                                            'updated' => date("Y-m-d H:i:s"),
                                                            'updatedby' => 'auto_inject_product',
                                                            'status' => 1,
                                                        );
                                                        if ($generate == true) {
                                                            $db->insert('product_image', $data_image);
                                                        } else {
                                                            $arr_img[] = $data_image;
                                                        }
                                                    } catch (Exception $e) {
                                                        $err_log['err_code'] ++;
                                                        $err_log[]['err_msg_product'] = 'input product [' . $name . '] failed ,  product sku duplicate';
                                                        $err_code++;
                                                        $err_message = 'System Fail. ' . $e->getMessage();
                                                    }
                                                    if ($err_code == 0) {
                                                        $db->commit();
                                                    } else {
                                                        $db->rollback();
                                                    }
                                                }
                                            }
                                        } else {
                                            continue;
                                        }
                                        if ($err_code == 0) {
                                            $db->commit();
                                            echo 'suckseed [' . $name . '] <br>';
                                        } else {
                                            $db->rollback();
                                        }
                                    }
                                } catch (Exception $e) {
                                    $err_code++;
                                    $err_message = 'System Fail. ' . $e->getMessage();
                                }
                            } else {
                                $err_code++;
                                $err_message = 'There are no data on your file.';
                            }
                        } else {
                            $err_code++;
                            $err_message = 'File not valid.';
                        }
                    }
                    $counter++;
                }
                echo $counter;

                if ($err_log['err_code'] > 0) {
//                    $err_log_arr = $err_msg_log;
                    $log_path = CF::get_dir('logs');
                    $detail = $err_log;
                    $arr = array(
                        "error_code" => $err_log['err_code'],
                        "error_message" => $err_log,
                        "type" => '',
                        "id" => '',
                        "detail" => $detail
                    );
                    if (!is_dir($log_path))
                        mkdir($log_path);
                    $log_path .= "Generate_product" . DS;
                    if (!is_dir($log_path))
                        mkdir($log_path);
                    $log_path .= date("Y-m-d") . DS;
                    if (!is_dir($log_path))
                        mkdir($log_path);

                    $filename = "time_" . date("His") . ".log.php";
                    @file_put_contents($log_path . $filename, json_encode($arr));
                }
                if ($generate == false) {
                    cdbg::var_dump($arr_pro);
//                    echo $spesification;
////                    cdbg::var_dump($data_p);
////                    cdbg::var_dump($arr_img);
                    cdbg::var_dump($arr_img);
                    die;
                }
//                cdbg::var_dump($data_a);
//                die;
            }
        }

//        if ($err_code == 0) {
//            $db->commit();
//        } else {
//            $db->rollback();
//            echo '<b>' .$err_message, '</b>';
//        }
    }

    public function preparation_injection() {
        $db = CDatabase::instance();
        $org_id = CF::org_id();
//        echo $app_code;
        cdbg::var_dump($org_id);
        die;
    }

    public function update_spec() {
        $db = CDatabase::instance();

        $data = $db->query('select * from product where status > 0 and createdby = "auto_inject_product"');
        $org_id = CF::org_id();
        $err_code = 0;
        $err_message = '';
        $data_arr = array();
        $num = 1;
        foreach ($data->result_array() as $key => $value) {
            $name = cobj::get($value, 'name');
            $spec_lama = cobj::get($value, 'spesification');
            $data_tmp = $db->query('select * from product_temp_lasvegas where status > 0');
            foreach ($data_tmp->result_array() as $k_t => $v_t) {
                $db->begin();
                $tmp_name = cobj::get($v_t, 'name');
                $spesification = cobj::get($v_t, 'spesification');
                if ($name == $tmp_name) {
                    $data_arr[] = 'data name =' . $name . ' = ' . $tmp_name . ' //// spec_lama = ' . $spec_lama . '||| spec_update =' . $spesification;
                    $cek_data[] = $data_update = array(
                        'spesification' => $this->porduct_spesification($spesification)
                    );
                    try {
                        $cek_db[] = $db->update('product', $data_update, array("name" => $tmp_name, "createdby" => "auto_inject_product"));
                    } catch (Exception $e) {
                        $err_code++;
                        $err_message = 'system failed';
                    }
                    if ($err_code == 0) {
                        $db->commit();
                        $msg_success[] = 'commit success ';
                    } else {
                        $db->rollback();
                    }
                }
            }
        }
        cdbg::var_dump($msg_success);
        cdbg::var_dump($cek_data);
        cdbg::var_dump($cek_db);
        cdbg::var_dump($data_arr);
        die;
    }

    public function update_sku() {
        $db = CDatabase::instance();

        $data = $db->query('select * from product where status > 0 and createdby = "auto_inject_product"');
        $org_id = CF::org_id();
        $err_code = 0;
        $err_message = '';
        $data_arr = array();
        $num = 1;
        foreach ($data->result_array() as $key => $value) {
            $name = cobj::get($value, 'name');
            $data_tmp = $db->query('select * from product_temp_lasvegas where status > 0');
            foreach ($data_tmp->result_array() as $k_t => $v_t) {
                $db->begin();
                $tmp_name = cobj::get($v_t, 'name');
                $tmp_sku = cobj::get($v_t, 'sku');
                if ($name == $tmp_name) {
                    $data_arr[] = 'data name =' . $name . ' = ' . $tmp_name . ' //// sku =' . $tmp_sku;
                    $cek_data[] = $data_update = array(
                        'sku' => $tmp_sku
                    );
                    try {
                        $cek_db[] = $db->update('product', $data_update, array("name" => $tmp_name, "createdby" => "auto_inject_product"));
                    } catch (Exception $e) {
                        $err_code++;
                        $err_message = 'system failed';
                    }
                    if ($err_code == 0) {
                        $db->commit();
                        $msg_success[] = 'commit success ';
                    } else {
                        $db->rollback();
                    }
                }
            }
        }
        cdbg::var_dump($msg_success);
        cdbg::var_dump($cek_data);
        cdbg::var_dump($cek_db);
        cdbg::var_dump($data_arr);
        die;
    }

    public function inject_product_byquery($generate = false) {
        $db = CDatabase::instance();
        $data = $db->query('select * from product_temp_lasvegas where status > 0');
        $org_id = CF::org_id();
        $err_code = 0;
        $err_message = '';
//        cdbg::var_dump($data->count());
//        die;
        $data_log = array();
        $num = 1;
        foreach ($data->result_array() as $key => $value) {
            $db->begin();
            $product_category = cobj::get($value, 'spesification');
//            $product_category = carr::get($value_arr, 0);
            $product_category_id = 223;
//                                        $product_category_id = cdbutils::get_value('select product_category_id from product_category where name ="' . $product_category . '"');
            $country_manufactur = 'indonesia';
//                                        $country_manufactur_id = cdbutils::get_value('select country_id from country where name ="' . $country_manufactur . '"');
            $country_manufactur_id = 94;
            $sku = cobj::get($value, 'sku');
            $code = cobj::get($value, 'code');
            $name = cobj::get($value, 'name');
            $url_key = cstr::sanitize($name);
            $sell_price = cobj::get($value, 'sell_price');
            $weight = cobj::get($value, 'weight');
            $stock = cobj::get($value, 'stock');
            $short_description = cobj::get($value, 'description');
            $overview = cobj::get($value, 'overview');
            $spesification = $this->porduct_spesification(cobj::get($value, 'spesification'));
            $hot = cobj::get($value, 'is_hot') == "YES" ? 1 : 0;
            $hot_until = cobj::get($value, 'hot_until');
            $show = cobj::get($value, 'show');
            $alert = cobj::get($value, 'alert');
            $image_1 = cobj::get($value, 'img1');
//                                        cdbg::var_dump($image_1);
//                                        cdbg::var_dump($value_arr);
//                                        die;
            $image_2 = cobj::get($value, 'img2');
            $image_3 = cobj::get($value, 'img3');
            $image_4 = cobj::get($value, 'img4');
            $image_5 = cobj::get($value, 'img5');
            $promo_price = cobj::get($value, 'promo_price');
            $expired_promo = cobj::get($value, 'expired_promo');
            $data_p = array(
                'org_id' => $org_id,
                'product_type_id' => 1,
                'product_category_id' => $product_category_id,
                'country_manufacture_id' => $country_manufactur_id,
                'attribute_set_id' => '',
                'country_manufacture' => $country_manufactur,
                'code' => $code,
                'name' => $name,
                'url_key' => $url_key,
                'currency' => 'IDR',
                'sell_price_nta' => 0,
                'sell_price' => $sell_price,
                'weight' => $weight,
                'stock' => $stock,
                'description' => $short_description,
                'overview' => $overview,
                'spesification' => $spesification,
                'is_hot_item' => $hot,
                'hot_until' => $hot_until,
                'show_minimum_stock' => $show,
                'alert_minimum_stock' => $alert,
                'sku' => $sku,
                'product_data_type' => '',
                'status_product' => 'CONFIRMED',
                'product_data_type' => 'simple',
                'filename' => '',
                'image_name' => '',
                'file_path' => $image_1,
                'visibility' => 'catalog_search',
                'status_confirm' => 'CONFIRMED',
                'created' => date("Y-m-d H:i:s"),
                'createdby' => 'auto_inject_product',
                'updated' => date("Y-m-d H:i:s"),
                'updatedby' => 'auto_inject_product',
                'status' => 1,
            );
            $generate_image = false;
            if ($generate == true) {
//                                            cdbg::var_dump($data_p);
//                                            die;
                $check_duplicate_name = cdbutils::get_value("select sku from product where status > 0 and sku='" . $sku . "'");

//                    if ($check_duplicate_name != null){
//                        cdbg::var_dump('duplicate'.$check_duplicate_name);
//                        die;
//                        $err_code++;
//                        $err_message = 'sku duplicate';
//
//
//                    }
                if ($err_code == 0) {
                    try {
                        $r = $db->insert('product', $data_p);
                        $product_id = $r->insert_id();
                    } catch (Exception $e) {
//                                                    $err_log[]['err_msg_product'] = 'input product ['.$name. '] failed';
                        $err_code++;
                        $err_message = 'System Fail. ' . $e->getMessage();
                    }

                    $generate_image = true;
                }
            } else {
                $generate_image = true;
                $product_id = 0;
                $arr_pro[] = $data_p;
            }

            $arr_image = array($image_2, $image_3, $image_4, $image_5);

            if ($generate_image == true) {
                foreach ($arr_image as $key => $v_img) {
//                        cdbg::var_dump($arr_image);
//                        die;
//                        if (strlen($v_img) == 0 || $v_img != ' ' ||  empty($v_img)) {
                    if ($v_img == ' ') {
                        continue;
                    } else {
                        try {
                            $data_image = array(
                                'product_id' => $product_id,
                                'image_path' => $v_img,
                                'created' => date("Y-m-d H:i:s"),
                                'createdby' => 'auto_inject_product',
                                'updated' => date("Y-m-d H:i:s"),
                                'updatedby' => 'auto_inject_product',
                                'status' => 1,
                            );
                            if ($generate == true) {
                                $db->insert('product_image', $data_image);
                            } else {
                                $arr_img[] = $data_image;
                            }
                        } catch (Exception $e) {
//                                                        $err_log['err_code']++;
//                                                        $err_log[]['err_msg_product'] = 'input product ['.$name. '] failed ,  product sku duplicate';
                            $err_code++;
                            $err_message = 'System Fail. ' . $e->getMessage();
                        }
//                                                    if ($err_code == 0) {
//                                                        $db->commit();
//                                                    } else {
//                                                        $db->rollback();
//                                                    }

                        if ($err_code > 0) {
                            $data_log_product_image[] = json_encode($data_image);
                        }
                    }
                }
            } else {
                continue;
            }

            if ($err_code > 0) {
                //create log
                $data_log[$sku] = array(
                    'err_message' => $err_message,
                    'data_product' => json_encode($data_p),
                    'data_product_image' => $data_log_product_image,
                );
            }

            if ($err_code == 0) {
                $db->commit();
                echo $num . '. Success [' . $name . '] <br>';
            } else {
                $db->rollback();
                continue;
            }
            $num++;
        }
        if (count($data_log) > 0) {
//                    $err_log_arr = $err_msg_log;
            $log_path = CF::get_dir('logs');
//                                        $detail = $err_log;
//                                        $arr = array(
//                                            "error_code" => $err_log['err_code'],
//                                            "error_message" => $err_log,
//                                            "type" => '',
//                                            "id" => '',
//                                            "detail" => $detail
//                                        );
            $arr = $data_log;
            if (!is_dir($log_path))
                mkdir($log_path);
            $log_path .= "Generate_product" . DS;
            if (!is_dir($log_path))
                mkdir($log_path);
            $log_path .= date("Y-m-d") . DS;
            if (!is_dir($log_path))
                mkdir($log_path);

            $filename = "time_" . date("His") . ".log.php";
            @file_put_contents($log_path . $filename, json_encode($arr));
        }
        if ($generate == false) {
            cdbg::var_dump($arr_pro);
//                    echo $spesification;
////                    cdbg::var_dump($data_p);
////                    cdbg::var_dump($arr_img);
            cdbg::var_dump($arr_img);
            die;
        }

//        cdbg::var_dump($data->result_array());
//        die;
    }

    public function inject_product($generate = false) {

        $db = CDatabase::instance();
        //$logs = 'D:/data/textData/';
        $logs = APPPATH . 'ittronmall/lasvegas/data_product/';
//        $logs = 'D:\data_lasvegas/';
        $data = array();
        $err_code = 0;
        $err_message = '';

        $data_log = array();

        if ($err_code == 0) {

            if (!is_dir($logs)) {
                $err_code++;
                $err_message = clang::__('Data not found.');
            }

            if ($err_code == 0) {
                $counter = 1;
                foreach (scandir($logs) as $filename) {
                    $exclude_file = array('.', '..');
                    if (!in_array($filename, $exclude_file)) {
                        $path = $logs . $filename;
//                        cdbg::var_dump($path);
//                        die;
                        if (is_file($path)) {
                            $data = file_get_contents($path);
                            $data = explode(PHP_EOL, $data);
//                            cdbg::var_dump($data);
//                            die;
                            $org_id = CF::org_id();
                            if (count($data) > 0) {
                                try {
//                                    cdbg::var_dump(count($data));
//                                    die;
                                    $num = 1;
                                    foreach ($data as $key => $value) {
                                        $data_log_product_image = array();
                                        $db->begin();
                                        $err_log['err_code'] = 0;
                                        $value_arr = explode("\t", $value);
//                                        if (carr::get($value_arr, 4) == 'AXIOO PICOPHONE M4S B') {
//                                            cdbg::var_dump($value_arr);
//                                        }

                                        $product_category = carr::get($value_arr, 0);
                                        $product_category_id = 465;
//                                        $product_category_id = cdbutils::get_value('select product_category_id from product_category where name ="' . $product_category . '"');
                                        $country_manufactur = carr::get($value_arr, 1);
//                                        $country_manufactur_id = cdbutils::get_value('select country_id from country where name ="' . $country_manufactur . '"');
                                        $country_manufactur_id = 94;
                                        $sku = carr::get($value_arr, 2);
                                        $code = carr::get($value_arr, 3);
                                        $name = carr::get($value_arr, 4);
                                        $url_key = cstr::sanitize($name);
                                        $sell_price = carr::get($value_arr, 5);
                                        $weight = carr::get($value_arr, 6);
                                        $stock = carr::get($value_arr, 7);
                                        $short_description = carr::get($value_arr, 8);
                                        $overview = carr::get($value_arr, 9);
                                        $spesification = $this->porduct_spesification(carr::get($value_arr, 10));
                                        $hot = carr::get($value_arr, 12);
                                        $hot_until = carr::get($value_arr, 13);
                                        $show = carr::get($value_arr, 14);
                                        $alert = carr::get($value_arr, 15);
                                        $image_1 = carr::get($value_arr, 16);
//                                        cdbg::var_dump($image_1);
//                                        cdbg::var_dump($value_arr);
//                                        die;
                                        $image_2 = carr::get($value_arr, 17);
                                        $image_3 = carr::get($value_arr, 18);
                                        $image_4 = carr::get($value_arr, 19);
                                        $image_5 = carr::get($value_arr, 20);
                                        $promo_price = carr::get($value_arr, 21);
                                        $expired_promo = carr::get($value_arr, 22);
                                        $data_p = array(
                                            'org_id' => $org_id,
                                            'product_type_id' => 1,
                                            'product_category_id' => $product_category_id,
                                            'country_manufacture_id' => $country_manufactur_id,
                                            'attribute_set_id' => '',
                                            'country_manufacture' => $country_manufactur,
                                            'code' => $code,
                                            'name' => $name,
                                            'url_key' => $url_key,
                                            'currency' => 'IDR',
                                            'sell_price_nta' => 0,
                                            'sell_price' => $sell_price,
                                            'weight' => $weight,
                                            'stock' => $stock,
                                            'description' => $short_description,
                                            'overview' => $overview,
                                            'spesification' => $spesification,
                                            'is_hot_item' => $hot,
                                            'hot_until' => $hot_until,
                                            'show_minimum_stock' => $show,
                                            'alert_minimum_stock' => $alert,
                                            'sku' => $sku,
                                            'product_data_type' => '',
                                            'status_product' => 'CONFIRMED',
                                            'filename' => '',
                                            'image_name' => '',
                                            'file_path' => $image_1,
                                            'visibility' => 'catalog_search',
                                            'status_confirm' => 'CONFIRMED',
                                            'created' => date("Y-m-d H:i:s"),
                                            'createdby' => 'auto_inject_product',
                                            'updated' => date("Y-m-d H:i:s"),
                                            'updatedby' => 'auto_inject_product',
                                            'status' => 1,
                                        );
                                        $generate_image = false;
                                        if ($generate == true) {
//                                            cdbg::var_dump($data_p);
//                                            die;
                                            $check_duplicate_name = cdbutils::get_value("select sku from product where status > 0 and sku='" . $sku . "'");
                                            if ($check_duplicate_name != null) {

                                                $err_code++;
                                                $err_message = 'sku duplicate';
                                            }

                                            if ($err_code == 0) {
                                                try {
                                                    $r = $db->insert('product', $data_p);
                                                    $product_id = $r->insert_id();
                                                } catch (Exception $e) {
//                                                    $err_log[]['err_msg_product'] = 'input product ['.$name. '] failed';
                                                    $err_code++;
                                                    $err_message = 'System Fail. ' . $e->getMessage();
                                                }

                                                $generate_image = true;
                                            }
                                        } else {
                                            $generate_image = true;
                                            $product_id = 0;
                                            $arr_pro[] = $data_p;
                                        }

                                        $arr_image = array($image_2, $image_3, $image_4, $image_5);
                                        if ($generate_image == true) {
                                            foreach ($arr_image as $key => $value) {
                                                if (strlen($value) == 0) {
                                                    continue;
                                                } else {
                                                    try {
                                                        $data_image = array(
                                                            'product_id' => $product_id,
                                                            'image_path' => $value,
                                                            'created' => date("Y-m-d H:i:s"),
                                                            'createdby' => 'auto_inject_product',
                                                            'updated' => date("Y-m-d H:i:s"),
                                                            'updatedby' => 'auto_inject_product',
                                                            'status' => 1,
                                                        );
                                                        if ($generate == true) {
                                                            $db->insert('product_image', $data_image);
                                                        } else {
                                                            $arr_img[] = $data_image;
                                                        }
                                                    } catch (Exception $e) {
//                                                        $err_log['err_code']++;
//                                                        $err_log[]['err_msg_product'] = 'input product ['.$name. '] failed ,  product sku duplicate';
                                                        $err_code++;
                                                        $err_message = 'System Fail. ' . $e->getMessage();
                                                    }
//                                                    if ($err_code == 0) {
//                                                        $db->commit();
//                                                    } else {
//                                                        $db->rollback();
//                                                    }

                                                    if ($err_code > 0) {
                                                        $data_log_product_image[] = json_encode($data_image);
                                                    }
                                                }
                                            }
                                        } else {
                                            continue;
                                        }

                                        if ($err_code > 0) {
                                            //create log
                                            $data_log[$sku] = array(
                                                'err_message' => $err_message,
                                                'data_product' => json_encode($data_p),
                                                'data_product_image' => $data_log_product_image,
                                            );
                                        }

                                        if ($err_code == 0) {
                                            $db->commit();
                                            echo $num . '. Success [' . $name . '] <br>';
                                        } else {
                                            $db->rollback();
                                        }
                                        $num++;
                                    }
                                } catch (Exception $e) {
                                    $err_code++;
                                    $err_message = 'System Fail. ' . $e->getMessage();
                                }
                            } else {
                                $err_code++;
                                $err_message = 'There are no data on your file.';
                            }
                        } else {
                            $err_code++;
                            $err_message = 'File not valid.';
                        }
                    }
                    $counter++;
                }
                echo $counter;

                if (count($data_log) > 0) {
//                    $err_log_arr = $err_msg_log;
                    $log_path = CF::get_dir('logs');
//                                        $detail = $err_log;
//                                        $arr = array(
//                                            "error_code" => $err_log['err_code'],
//                                            "error_message" => $err_log,
//                                            "type" => '',
//                                            "id" => '',
//                                            "detail" => $detail
//                                        );
                    $arr = $data_log;
                    if (!is_dir($log_path))
                        mkdir($log_path);
                    $log_path .= "Generate_product" . DS;
                    if (!is_dir($log_path))
                        mkdir($log_path);
                    $log_path .= date("Y-m-d") . DS;
                    if (!is_dir($log_path))
                        mkdir($log_path);

                    $filename = "time_" . date("His") . ".log.php";
                    @file_put_contents($log_path . $filename, json_encode($arr));
                }
                if ($generate == false) {
                    cdbg::var_dump($arr_pro);
//                    echo $spesification;
////                    cdbg::var_dump($data_p);
////                    cdbg::var_dump($arr_img);
                    cdbg::var_dump($arr_img);
                    die;
                }
//                cdbg::var_dump($data_a);
//                die;
            }
        }

//        if ($err_code == 0) {
//            $db->commit();
//        } else {
//            $db->rollback();
//            echo '<b>' .$err_message, '</b>';
//        }
    }

    public function porduct_spesification($spesifikasi = null) {

        $th_white = 'box-sizing: border-box;
                    padding: 5px;
                    text-align: left;
                    line-height: 1.42857;
                    vertical-align: top;
                    border: 1px solid rgb(221, 221, 221);
                    width: 235px;
                    background-color: rgb(249, 249, 249);
                    color : black;';
        $th_red = 'box-sizing: border-box;
                    padding: 5px;
                    text-align: left;
                    line-height: 1.42857;
                    vertical-align: top;
                    border: 1px solid rgb(221, 221, 221);
                    width: 235px;
                    background-color: #bd2828;
                    color: white;';


        $td_white = ' box-sizing: border-box;
                    padding: 5px;
                    line-height: 1.42857;
                    vertical-align: top;
                    border: 1px solid rgb(221, 221, 221);
                    
                    ';
        $td_dark = 'box-sizing: border-box;
                    padding: 5px;
                    line-height: 1.42857;
                    vertical-align: top;
                    border: 1px solid rgb(221, 221, 221);
                    background-color: rgb(249, 249, 249);';
        $data = explode(',', $spesifikasi);
        $html = '<table class="field-group-format group_specs table table-striped table-bordered table-striped">';
        $html .= '<tbody class="tbody_spec">';
        foreach ($data as $value) {
            $arr[] = $value;
            $sp[] = explode(':', $value);
        }
        $a = 0;

        foreach ($sp as $key => $value) {
            $s_k = carr::get($value, 0);
            $s_v = carr::get($value, 1);
            $tampil = true;
            if (strlen($s_v) == 0)
                $tampil = false;
            if ($a % 2 == 0) {
                $th_style = $th_red;
                $td_style = $td_dark;
            } else {
                $th_style = $th_white;
                $td_style = $td_white;
            }
            if ($tampil == true) {
                $html .= '<tr>' . '<th style="' . $th_style . '">' . $s_k . '</th><td style="' . $td_style . '">' . $s_v . '</td>' . '</tr>';
            }


            if ($tampil == true)
                $a++;
        }
        $html .= '</tbody>';

        $html .='</table>';
        return $html;
    }

    function get_avail() {
        $avail = tour::get_availability('TDS000001');
        cdbg::var_dump($avail);
    }

    function get_all($booking_code) {
        cdbg::var_dump(transaction::get_all($booking_code));
    }

    function cms_menu($org_id) {
        cdbg::var_dump(cms::get_cms_menu($org_id));
        foreach (cms::get_cms_menu($org_id) as $key => $value) {
            cdbg::var_dump($value);
        }
    }

    function cms_post($org_id) {
        cdbg::var_dump(cms::post('', $org_id));
    }

    function test_query_fk() {
        $org_id = CF::org_id();
        $page = 'product';
        $db = CDatabase::instance();
        $arr_advertise = array();
        $org_data_found = false;
        $q_base = "			select				ca.cms_advertise_id as cms_advertise_id,				cad.cms_advertise_row_id as cms_advertise_row_id			from				cms_advertise as ca				left join cms_advertise_setting as cas on cas.cms_advertise_setting_id=ca.cms_advertise_setting_id				left join cms_advertise_detail as cad on cad.cms_advertise_id=ca.cms_advertise_id			where				ca.status>0				and ca.is_active>0								and cas.product_type=" . $db->escape($page) . "		";
        if (CF::theme() == 'Imlasvegas') {
            $q_base .= ' and cas.name ="Imlasvegas" and cad.status>0 ';
        } else {
            $q_base.= 'and case when cad.status is null then 1=1 else cad.status>0 end ';
        } $q_org = $q_base;
        if ($org_id) {
            $q_org.="				and ca.org_id=" . $org_id . "				order by 					cad.priority asc			";
            $r = $db->query($q_org);
            if ($r->count() > 0) {
                $org_data_found = true;
            }
        } if (!$org_data_found) {
            $q_no_org = $q_base . "				and ca.org_id is null				order by 					cad.priority asc			";
            $r = $db->query($q_no_org);
        } cdbg::var_dump($r);
        die('asdasdads');
        if ($r->count() > 0) {
            foreach ($r as $row) {
                $arr_row = array();
                $q_row = "					select						card.cms_advertise_col_id					from						cms_advertise_row as car						inner join cms_advertise_row_detail as card on card.cms_advertise_row_id=car.cms_advertise_row_id					where						card.status>0						and car.status>0						and card.cms_advertise_row_id=" . $db->escape($row->cms_advertise_row_id) . "					order by						card.priority asc				";
                $r_row = $db->query($q_row);
                if ($r_row->count() > 0) {
                    foreach ($r_row as $row_row) {
                        $arr_col = array();
                        $q_col = "							select								image_url,								col_span,                                                                url_link							from								cms_advertise_col							where								cms_advertise_col_id=" . $db->escape($row_row->cms_advertise_col_id) . "						";
                        $r_col = $db->query($q_col);
                        if ($r_col->count() > 0) {
                            $arr_col['image_url'] = $r_col[0]->image_url;
                            $arr_col['col_span'] = $r_col[0]->col_span;
                            $arr_col['url_link'] = $r_col[0]->url_link;
                        } $arr_row[] = $arr_col;
                    }
                } $arr_advertise[] = $arr_row;
            }
        } return $arr_advertise;
    }

    function payment_list() {
        cdbg::var_dump(cms::get_payment_list());
        $email[] = 'aaaaa';
        $email[] = ccfg::get('email_dev');
        cdbg::var_dump($email);
    }

}
