<?php

    /**
     *
     * @author Khumbaka
     * @since  Mar 29, 2016
     * @license http://piposystem.com Piposystem
     */
    class Controller_Unit_Test_Mboko extends SixtyTwoHallFamilyController {

        private $_count_item = 12;

        public function subtract_minutes() {
            $order_date = date('Y-m-d H:i:s', strtotime('2016-12-13 12:03:00'));
            $now = date('Y-m-d H:i:s');
            $order_date = strtotime($order_date);
            $now = strtotime($now);
            $diff = floor(($now - $order_date) / 60);
            $second = ($now - $order_date) - $diff * 60;            
            $hold_time_minute = 15 - $diff;
            echo $hold_time_minute;
            echo '<br>';
            $hold_time_second = 59 - $second;
            echo $hold_time_second;
        }

        public function test_lock() {
            $filepath = 'C:\123';
            touch($filepath . ".lock");
//            fwrite($fp, $message);
//            flock($fp, LOCK_UN);
//            fclose($fp);
//            unlink($filepath . ".lock");
        }

        public function encrypt_email() {
            $encrypt = security::encrypt('mboko@ittron-indonesia.com');
            cdbg::var_dump($encrypt);
            $decrypt = security::decrypt($encrypt);
            cdbg::var_dump($decrypt);
        }

        public function __construct() {
            parent::__construct();
        }

        function test_privacy() {
            $result = file_get_contents('http://62hallfamily.local/api/trevomember/PrivacyPolicy?session_id=session201611101643295824414175864');
            $result = json_decode($result, true);
            echo $result['data']['privacy_policy'];
        }

        function test_term() {
            $result = file_get_contents('http://62hallfamily.local/api/trevomember/TermOfUse?session_id=session201611101643295824414175864');
            $result = json_decode($result, true);
            echo $result['data']['term_of_use'];
        }

        function test_email() {
            cmail::send_smtp('mboko@ittron-indonesia.com', 'Change Password', 'Your Password is: 123');
        }

        function gen_uuid($len = 5) {

            $hex = md5("ChangePassword" . uniqid("", true));

            $pack = pack('H*', $hex);
            $tmp = base64_encode($pack);

            $uid = preg_replace("#(*UTF8)[^A-Za-z0-9]#", "", $tmp);

            $len = max(4, min(128, $len));

            while (strlen($uid) < $len)
                $uid .= gen_uuid(22);

            return substr($uid, 0, $len);
        }

        function get_random_str($type = 'alpha') {
            $length = 3;
            if ($type == 'alpha') {
                $characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            }
            if ($type == 'numeric') {
                $characters = "0123456789";
            }
            $string = "";
            $max = strlen($characters) - 1;
            for ($p = 0; $p < $length; $p++) {
                @$string .= $characters[mt_rand(0, $max)];
            }
            return $string;
        }

        function generate_random_password() {
            $alpha = $this->get_random_str('alpha');
            $numeric = $this->get_random_str('numeric');
            return str_shuffle($alpha . $numeric);
        }

        public function test_pwd() {
//            $pwd = '1';
//            echo $this->gen_uuid();            
            echo $this->generate_random_password();
        }

        public function test_commission_member() {
            $transaction_id = 401;
            $nominal = ((10 / 100) * 103200);
            $result = member_balance::history('commissiontransactiontrevo', $transaction_id, $nominal);
            cdbg::var_dump($result);
            $result = member_balance::history('commissiontransactiontrevo2', $transaction_id, $nominal);
            cdbg::var_dump($result);
        }

        public function test_balance_driver() {
            $transaction_id = 363;
            $result = trevo_driver_balance::history('transaction', $transaction_id);
            cdbg::var_dump($result);
        }

        public function test_balance_member() {
            $transaction_id = 363;
            $result = member_balance::history('transaction', $transaction_id);
            cdbg::var_dump($result);
        }

        public function test_duration() {
            echo date('Y-m-d H:i:s');
            exit;
            $satu = 1.5;
            echo round($satu);
            exit;
            $date = date('Y-m-d H:i:s', strtotime(date('YmdHis') . '+1 minute'));
            echo $date;
        }

        public function test_commission() {
            $member_id = 251;
            $data_member = member::get_commission_member_category($member_id);
            cdbg::var_dump($data_member);
        }

        public function test_send_notif() {
            $token[] = 'eoeonpS8PYc:APA91bFqek-8DeiEJNX0WCK5M2aAlGGoYw0xOWGajSqOHEtICI-XQ48eK-MFzg7G5J_kt4neAzmZ5DYs9G0eLtYqyKD5jOj168TJRo1imPJdSfjViD430bKzoi7xIqYWVZ1eegHO9pWl';
            $result = json_decode(trevo::send_gcm_notify($token, 'coba 1', 'coba 2', 'coba 3'), true);
            cdbg::var_dump($result);
        }

        public function send_gcm_notify($reg_id, $title = null, $detail, $type) {
            if ($title == null) {
                $title = "Trevo";
            }

            $fields = array(
                'registration_ids' => $reg_id,
                'data' => array("message" => $detail, "title" => $title, "type" => $type),
            );

            $headers = array(
                'Authorization: key=' . 'AAAAZ-IBFKA:APA91bEua70wjLGlasKrbQu1_Gd-jMV_9tqVGSL3HR86CPC-_c9Wiksj8CSennIHLFK9DsKuiDQfKS9EvJN3MsoAokgCDL0AFKL7TXaIpVwdjMuogsYe4F8tj6r8J4r-wRjHcx6U_st9j2pryfs55S2rXOBK8Nd_2w',
                'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            $result = curl_exec($ch);
            if ($result === FALSE) {
                die('Problem occurred: ' . curl_error($ch));
            }

            curl_close($ch);
            return $result;
        }

        public function test_pic() {
            $dir_image = 'default_image_profile_member_';
            $image_name = 'asd.png';
            $get_image = $dir_image . $image_name;
            $get_image = 'default_image_profile_20160404_IMG_01112014_111508_0.png';
            if (empty($get_image)) {
                $image = '';
            }
            else {
                $image = image::get_image_url_front($get_image, 'view_profile');
            }
            cdbg::var_dump($image);
        }

        public function testing() {
            $data[] = array('1');
            $data[] = array('2');
            $data[] = array('3');
            cdbg::var_dump($data);
        }

        private function list_number() {
            return array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10);
        }

        public function get_start_limit($page) {
            $start = 0;
            if ($page > 1) {
                $start = ($page - 1) * $this->_count_item;
            }

            return $start;
        }

        public function get_product_gold($page) {
            $org_id = CF::org_id();
            $db = CDatabase::instance();

            $start = $this->get_start_limit($page);

            $arr_gold = array();
            $q = "
				select
					*
				from
					product_type
				where
					name='gold'
			";
            $r = $db->query($q);
            if ($r->count() > 0) {
                $q_base = "
					select
						*
					from
						product 
					where
						status>0												
						and is_active>0						
						and status_confirm='CONFIRMED'
						and product_type_id=" . $db->escape($r[0]->product_type_id) . "
					order by
										weight asc
									limit
										" . $start . "," . $this->_count_item . "
				";
                $r = $db->query($q_base);
                if ($r->count() > 0) {
                    foreach ($r as $row) {
                        $buy = 'Sold Out';
                        if ($row->stock > 0) {
                            $buy = 'B : Rp. ' . ctransform::format_currency($row->sell_price);
                        }
                        $arr_gold[] = array(
                            "id" => $row->product_id,
                            "name" => $row->name,
                            "stock" => $row->stock,
                            "buy" => $buy,
                            "sell" => 'J : Rp. ' . ctransform::format_currency($row->purchase_price),
                            "image" => $row->image_name,
                        );
                    }
                }
            }
            return $arr_gold;
        }

        public function test_elem() {
            $app = CApp::instance();
            $data_gold = $this->get_product_gold(1);
//            cdbg::var_dump($data_gold);exit;
            $data_gold_detail = product::get_product($data_gold[1]['id']);
            $app->add_element('62hall-product-gold-card')->set_product($data_gold_detail);
            echo $app->render();
        }

        public function test_gold() {
            $app = CApp::instance();
            $container = $app->add_div()->add_class('gold-container ost-form');
            $row = $container->add_div()->add_class('row');
            $item = $row->add_div()->add_class('col-lg-3 item-gold');
            $item->add_div()->add_class('title')->add('Antam 1gr');
            $item->add_div()->add_class('title-jual')->add('J : Rp. 1.545.000');
            $item->add_div()->add_class('title-beli')->add('B : Rp. 1.626.000');
            $item
                    ->add_div()
                    ->add_class('image')
                    ->add_img()
                    ->add_class('img-responsive')
                    ->set_src('http://62hallfamily.local/media/img/gambar_emas.png');
            $item
                    ->add_field()
                    ->add_div()
                    ->add_class('select')
                    ->add_control('dd1', 'dropdown')
                    ->set_list($this->list_number())
                    ->add_class('os-dropdown')
                    ->set_value(1);
            $div_button = $item->add_div()->add_class('button');
            $div_button
                    ->add_action('btn_jual')
                    ->add_class('ost-btn-submit jual')
                    ->set_label('Jual')
                    ->set_submit(false)
                    ->set_link('http://62hallfamily.local/unit_test/mboko/test_gold/1/1');
            $div_button
                    ->add_action('btn_beli')
                    ->add_class('ost-btn-submit beli')
                    ->set_label('Beli');
            echo $app->render();
        }

        public function test_dialog() {
            $err_code = 0;
            $width_col_2 = 'col-md-2';
            $width_col_3 = 'col-md-3';
            $width_col_4 = 'col-md-4';
            $width_col_6 = 'col-md-6';
            $width_col_8 = 'col-md-8';
            $width_col_9 = 'col-md-9';
            $width_col_12 = 'col-md-12';

            $app = CApp::instance();
            $member = member::get_data_member();
            $container = $app->add_div();
            $container->add_div()->add('<h4>' . clang::__('Tambah Address Book') . '</h4></br>');
            $addr_body_form = $container
                    ->add_form('form_add_address')
                    ->set_action(curl::base() . 'unit_test/mboko/test_dialog')
                    ->add_class('ost-form form-horizontal');
            $addr_div = $addr_body_form->add_div();
            $addr_div->add_field()
                    ->set_style_form_group('inline')
                    ->add_label_class($width_col_4 . ' control-label')
                    ->add_control_class($width_col_8)
                    ->set_label('<span class="red">* </span>' . clang::__('Name'))
                    ->add_control('name', 'text')->set_value(cobj::get($member, 'name'));

            echo $app->render();
        }

        public function test_call() {
            $app = CApp::instance();
            $widget = $app->add_widget();
            $widget
                    ->add_action()
                    ->set_label('buka')
                    ->add_listener('click')
                    ->add_handler('dialog')
                    ->set_url(curl::base() . 'unit_test/mboko/test_dialog');
            echo $app->render();
        }

        public function test_table() {
            $app = CApp::instance();
            $app->add('</br>');

            $table = $app->add_table('table1')->set_apply_data_table(false);
            $table->add_column('column1')->set_label('Address');
            $table->add_column('column2');

            $link = curl::base() . 'unit_test/mboko/test_table';
            $btn_edit = CAction::factory('btn_edit');
            $btn_edit->set_label('Ubah')->set_button(true)->add_class('btn-primary')->set_link($link);
            $data_detail = array(
                'column1' => 1,
                'column2' => $btn_edit
            );
            $data[] = $data_detail;
            $table->set_data_from_array($data);

            $app->add('</br>');

            echo $app->render();
        }

        public function test_base() {
            cdbg::var_dump(curl::base());
        }

        public function test_file() {
            cdbg::var_dump(filemtime('C:\xampp\htdocs_pipo\application\62hallfamily\default\image\profile\20160128\view_profile\default_image_profile_20160128_Desert.jpg'));
        }

        public function test_logo() {
            $data_org = org::get_info();
            cdbg::var_dump($data_org);
            cdbg::var_dump(carr::get($data_org, 'logo_full_path'));
        }

        public function test_msg() {
            $app = CApp::instance();
            $error_message = 'Ini adalah sebuah pesan testing error message!';
            cmsg::add('warning', $error_message);
            cmsg::add("info", $error_message);
            cmsg::add('success', $error_message);
            cmsg::add("error", $error_message);
            cmsg::add("", $error_message);
            echo $app->render();
        }

        public function index() {
            cdbg::var_dump('this is unit test');
            $amount = ctransform::format_currency(10000);
            cdbg::var_dump($amount);

            $member = member::get_data_member();
            $date_of_birth = cobj::get($member, 'date_of_birth');
            cdbg::var_dump($date_of_birth);
            $date = date::long_date_formatted($date_of_birth);
            cdbg::var_dump($date);
        }

    }
    