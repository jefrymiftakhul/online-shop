<?php

class Riza_Controller extends SixtyTwoHallFamilyController {
    
    public function index(){
        $this->send_verification(6);
    }
    
    public function dialog() {
        $app = CApp::instance();
        
        $app->add_element('62hall-login') 
                ->set_trigger_button(true) 
                ->set_icon(FALSE);
        echo $app->render();
        
    }
    
    public function tes_email_transaction($type, $booking_code, $status = NULL){
        $this->send_transaction($type, $booking_code, $status);
    }
    
    public function shopping_cart(){
        
        return curl::base() . 'page' . DS . 'shopping';
    }
    
    public function scrap() {
        $curl = CCurl::factory('http://cucigu.com');
        $response = $curl->exec()->response();
        
        
        $pattern = '#(<ul class="nav navbar-nav">)(.+?)(</ul>\s</div>)#';
        if(preg_match($pattern, $response, $matches)) {
            cdbg::var_dump($matches);
        }

        //echo htmlspecialchars($response);
    }
    
    public function eko() {
        $postdata = file_get_contents("php://input"); 
        file_put_contents('test.log', $postdata);
        echo 'OK';
    }

    public function tes_slider() {
        $app = CApp::instance();
    

        $app->add_div()->add('<div class="owl-carousel">
              <div> Your Content 1 </div>
              <div> Your Content 2 </div>
              <div> Your Content 3 </div>
              <div> Your Content 4 </div>
              <div> Your Content 5 </div>
              <div> Your Content 6 </div>
              <div> Your Content 7 </div>
            </div>');

        $app->add_js("
            $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})
            ");

        echo $app->render();
    }

    public function slide_responsive() {
        $app = CApp::instance();

        $org_id = $this->org_id();

        // BEST SELLING
        $data_best_selling = deal::get_deal(620001, 'product', 'deal_best_sell');

        $element_best_selling = $app->add_element('62hall-slide-product-responsive', 'best-selling-sm');
        $element_best_selling->set_list_item($data_best_selling);

        echo $app->render();
    }



}