<?php

/**
 *
 * @author Riza 
 * @since  Nov 12, 2015
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class Video_Controller extends ProductController {


    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $app = CApp::instance();
        $user = $app->user();
        $org_id = $this->org_id();
        $menu_list = $this->menu_list();
        $link_list = $this->list_menu();

        // ADVERTISE
        $data_advertise = advertise::get_advertise($org_id, $this->page());

        // SEARCH 
        // SHOPPING CART 
        $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

        $app->add_js("
            
            jQuery(window).resize(function(){
                var vw = jQuery(window).width();
                if(vw<=974){
                    if(kecilFired==0){
                        getSlide(0);
                        kecilFired=1;
                        besarFired=0;
                    }
                }else{
                    if(besarFired==0){
                        getSlide(1);
                        besarFired=1;
                        kecilFired=0;
                    }
                }
            });
        ");
        $product_category = product_category::get_product_category_menu($this->page());
        // Menu
        $menu_list = array(
            'name' => 'KATEGORI',
            'menu' => $product_category
        );
        
        
        
        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_menu($menu_list)
                ->set_link($link_list);
        $container_video=$app->add_div('container_video');
        $container_video->add_element('62hall-cms-video','cms_video');
        $container_gallery=$app->add_div()->add_class('container');
        $container_gallery->add_element('62hall-cms-video-gallery','cms_video_gallery');
        echo $app->render();
    }

}
