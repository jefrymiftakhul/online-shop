<?php

/**
 * Description of home
 *
 * @author Ecko Santoso
 * @since 31 Agu 16
 */
class Home_Controller extends LokalController
{
    private $product_per_row = 3;
    private $responsive_bootstrap = array(
        'col_lg' => 3,
    );
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $app = CApp::instance();
        $user = $app->user();
        $org_id = $this->org_id();
        $db = CDatabase::instance();

        // menu
        $link_list = $this->list_menu();
        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_link($link_list);
        $product_category = $app->add_div()->add_class('lokal-category-wrapper');
        $q = 'select * from product_category where status > 0 and parent_id is null and product_type_id = 10';
        $data_product_category = $db->query($q);
        $container = $app->add_div()->add_class('container produk-lokal')->add_div()->add_class('row');
        $col_4 = $container->add_div()->add_class('col-sm-12 col-md-3');
        $col_8 = $container->add_div()->add_class('col-sm-12 col-md-9');
        // filter
        $options = array(
            'product_type' => $this->page(),
            'visibility' => 'catalog',
        );

        $min_price = product::get_min_price($options);
        $max_price = product::get_max_price($options);
        if ($min_price == null) {
            $min_price = 0;
        }
        if ($max_price == null) {
            $max_price = 0;
        }
        $range_price = array(
            'min' => $min_price,
            'max' => $max_price,
        );
        // cdbg::var_dump($min_price);
        // cdbg::var_dump($min_price);
        // die();
        $category_lft = '';
        $category_rgt = '';

        $element_filter = $col_4->add_element('62hall-filter', 'category-filter');
        $element_filter->set_filter_product(false);
        $element_filter->set_filter_price(true);
        $element_filter->set_filter_location(true);
        $element_filter->set_filter_category(true);
        $element_filter->set_filter_page($this->page());
        $element_filter->set_min_price($range_price['min']);
        $element_filter->set_max_price($range_price['max']);
        $element_filter->set_list_filter_category($data_product_category);

        // Produk Lokal Terbaru
        $options = array(
            'limit' => '9',
            'product_type_id' => '10',
        );
        $lokal_product_id =  $db->query('select product_id from product where status > 0 and (org_id is null or org_id = '.$db->escape(CF::org_id()).') and product_type_id = 10 order by created desc');
        $results = array();
        if(count($lokal_product_id)>0){
            foreach ($lokal_product_id as $key => $value) {
                $results[] =  product::get_product($value->product_id);
            }
        }
        $lokal_terbaru = $col_8->add_div()->add_class('home-deal lokal-terbaru');
        $lokal_terbaru->add_div()->add(
            '<h4 class="deal-title">'
            . '<div class="deal-title-wrapper">'
            . clang::__('PRODUK LOKAL TERBARU')
            . '</div>'
            . '</h4>');
        $lokal_terbaru->add_element('lokal-product-slider')->add_class('def-slider')
            ->set_slides($results);

        // Page result product
        $lokal_terbaru = $col_8->add_div()->add_class('home-deal lokal-terbaru');
        $lokal_terbaru->add_div()->add(
            '<h4 class="deal-title">'
            . '<div class="deal-title-wrapper">'
            . clang::__('PRODUK LOKAL')
            . '</div>'
            . '</h4>');
        $product_category = $lokal_terbaru->add_div('page-product-category')->add_class('lokal-result');
        $product_category->add_listener('ready')
            ->add_handler('reload')
            ->set_target('page-product-category')
            ->add_param_input('arr_product_id')
            ->set_url(curl::base() . "reload/reload_product_filter?visibility=catalog&filter_page=" . $this->page() . '&filter_category_lft=' . $category_lft . '&filter_category_rgt=' . $category_rgt);

        echo $app->render();
    }
    public function category($category)
    {

    }
}
