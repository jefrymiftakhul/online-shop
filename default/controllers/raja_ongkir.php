<?php

/**
 * Description of raja_ongkir
 *
 * @author Imam
 */
class Raja_Ongkir_Controller extends SixtyTwoHallFamilyController{

    public function update() {
        raja_ongkir::update_db('9406fbe823f72dbfbe4d9b3c849c073e');
    }

    //put your code here
    public function set_session_shipping() {
        $app = CApp::instance();
        $session = Session::instance();
        $get = $_GET;
        $shipping_data = array();
        $json_shipping = carr::get($get, 'shipping_data');		
        if ($json_shipping !== null and strlen($json_shipping) > 0) {
            $shipping_data = json_decode($json_shipping, true);
			if($json_shipping == "INCLUDED"){
			  $shipping_data = $json_shipping;
			}
        }
        $session->set('shipping_data', $shipping_data);		
        echo $app->render();
    }
    public function get_cost_shipping() {
        $app = CApp::instance();
        $get = $_GET;
        $shipping_data = array();
        $json_shipping = carr::get($get, 'shipping_data');		
        if ($json_shipping !== null and strlen($json_shipping) > 0) {
            $shipping_data = json_decode($json_shipping, true);
			if($json_shipping == "INCLUDED"){
			  $shipping_data = $json_shipping;
			}
        }

        $cost = carr::get($shipping_data,'cost');
        $app->add("Rp ". ctransform::format_currency($cost));
        echo $app->render();
    }
     public function get_total_payment() {
         
        $app = CApp::instance();
        $get = $_GET;
        $session = Session::instance();
        $shipping_data = array();
        $cart = Cart62hall_Product::factory();
        $cart_list = $cart->get_cart();

        // COST SHIPPING
        $json_shipping = carr::get($get, 'shipping_data');		
        if ($json_shipping !== null and strlen($json_shipping) > 0) {
            $shipping_data = json_decode($json_shipping, true);
			if($json_shipping == "INCLUDED"){
			  $shipping_data = $json_shipping;
			}
        }
        $cost_shipping = carr::get($shipping_data,'cost');
        
        // TOTAL ITEM PAYMENT
   
        // IF USING CART
        $subtotal_payment = 0 ;
        foreach ($cart_list as $key => $value) {
            $subtotal_payment += carr::get($value,'sub_total');
        }
        
        // IF USING SESSION
        // $get_transaction = $session->get('session_post');
        // $get_total_item = $get_transaction['total_item'];
        
        $get_total_payment = $subtotal_payment + $cost_shipping;
        
        $app->add("Rp ". ctransform::format_currency($get_total_payment));

        echo $app->render();
    }

    public function get_cost() {
        //service_type:jne
        //city_shipping:375
        //weight_shipping:1

        $app = CApp::instance();
        $db = CDatabase::instance();
        $request = $_POST + $_GET;
        $api = '9406fbe823f72dbfbe4d9b3c849c073e';
        $origin = carr::get($request, 'city_shipping');
        $org_id=CF::org_id();
        $origin_city_org=cdbutils::get_value("select city_id from org where org_id=".$db->escape($org_id));
        $origin = cdbutils::get_value('select raja_ongkir_city_id from city where city_id = '.$db->escape($origin_city_org));
        $destination = carr::get($request, 'city_shipping');
		$destination = cdbutils::get_value('select raja_ongkir_city_id from city where city_id = '.$db->escape($destination));
        $weight = carr::get($request, 'weight_shipping');
        $shipping_type = carr::get($request, 'service_type');
        $user_courier = carr::get($request, 'courier');
        $user_service = carr::get($request, 'service');		
        $districts_shipping = carr::get($request, 'districts_shipping');
        $data_response = raja_ongkir::calculate_cost($api, $origin, $destination, $weight, $shipping_type);
        $data_response = carr::get($data_response, 0);
        if (isset($_GET['debug'])) {
            cdbg::var_dump($data_response);exit;
        }
        $id = 'lapak';
        if (count($data_response) != 0) {
            $data_costs = carr::get($data_response, 'costs');
            $data_code = carr::get($data_response, 'code');
            $origin_details = carr::get($data_response, 'origin_details');
            $destination_details = carr::get($data_response, 'destination_details');
            $data_code = carr::get($data_response, 'code');
            $app->add_div('div_reload');
            $row = $app->add_div()->add_class('row text-left');
            $col_12 = $row->add_div()->add_class('col-xs-12');
            $accordion = $col_12->add_div('accordion_'.$id)->add_class('accordion_'.$id);
            $app->add_control('hidden_shipping', 'hidden');
            
            // <editor-fold defaultstate="collapsed" desc="promo free ongkir">

            $promo_expedition=expedition_promo::calculate_promo($districts_shipping);
            $promo_expedition_amount=0;
            $is_free=0;
            if($promo_expedition!=null){
                $promo_expedition_amount=carr::get($promo_expedition,'discount',0);
                $is_free=carr::get($promo_expedition,'is_free');
            }
            
            if(count($data_costs)>0){
                foreach ($data_costs as $key => $val) {
                    $tarif = $val['cost'][0]['value']-$promo_expedition_amount;
                    if($tarif<0){
                        $tarif=0;
                    }
                    if($is_free==1){
                        $tarif=0;
                    }

                    $data_costs[$key]['cost'][0]['real_value'] = $val['cost'][0]['value'];
                    $data_costs[$key]['cost'][0]['value'] = $tarif;
                }    
            }
            
            // </editor-fold>
            
            if (count($data_costs) != 0) {
                foreach ($data_costs as $key => $value) {
                    $free_ongkir='';
                    if ($data_code == 'jne' && $value['service'] != 'REG' && $value['service'] != 'YES' && $value['service'] != 'OKE' && $value['service'] != 'CTC' && $value['service']!= 'CTCOKE' && $value['service']!='CTCSPS' && $value['service']!='CTCYES') {
                        // do nothing if jne not REG YES AND OKE
                    }
                    else {
                        $radio_selected = array();
                        $radio_selected['courier'] = $data_code;
                        $radio_selected['service'] = $value['service'];
                        $radio_selected['origin'] = $origin;
                        $radio_selected['destination'] = $destination;
                        $radio_selected['cost'] = $value['cost'][0]['value'];
                        $radio_selected['weight'] = $weight;
                        $radio_js = "$.cresenity.reload('div_reload','" . curl::base() . "raja_ongkir/set_session_shipping/','get',{'shipping_data':'" . json_encode($radio_selected) . "'});"
                                  . "$.cresenity.reload('shipping-cost','" . curl::base() . "raja_ongkir/get_cost_shipping/','get',{'shipping_data':'" . json_encode($radio_selected) . "'});"
                                . "$.cresenity.reload('total-payment','" . curl::base() . "raja_ongkir/get_total_payment/','get',{'shipping_data':'" . json_encode($radio_selected) . "'})";
//                        $radio_js = "jQuery('#hidden_shipping').val('" . json_encode($radio_selected) . "');";
                        $panel = $accordion->add_div()->add_class('panel');
                        $heading = $panel->add_div()->add_class('accordion-heading');
                        $radio = $heading->add_control('', 'radio')
                                ->add_attr('data-target', '#collapse_' . str_replace(' ', '', $value['service']).'_'.$id)
                                ->add_attr('data-toggle', 'collapse')
                                ->add_attr('data-parent', '#accordion_'.$id)
                                ->set_value($data_code . ';' . $value['description'] . ';' . $value['cost'][0]['value'])
                                ->set_label(strtoupper($data_code) . ' ' . $value['description'].' ( '.$value['service'].' )')
                                ->set_label_wrap(true)
                                ->set_inline(true)
                                ->set_name('shipping_selected');
                        $radio->add_listener('click')
                                ->add_handler('custom')
                                ->set_js($radio_js);
                        $list = $panel->add_div('collapse_' . str_replace(' ', '', $value['service']).'_'.$id)->add_attr('role', 'tabpanel');
                        $list->add_class('accordion-body collapse list_info');
                        if ($key == 0) {
                            // IF COURIER TYPE IS NOT SAME WITH USER
                            if ($user_courier != $data_code) {
                                $radio->set_checked(true);
                                $list->add_class('in');
                                $radio->add_listener('ready')
                                        ->add_handler('custom')
                                        ->set_js($radio_js);
                            }
                            else {
                                // IF COURIER TYPE SAME AND SERVICE TYPE IS SAME TOO
                                if ($value['service'] == $user_service) {
                                    $radio->set_checked(true);
                                    $list->add_class('in');
                                    $radio->add_listener('ready')
                                            ->add_handler('custom')
                                            ->set_js($radio_js);
                                }
                            }
                        }
                        else {
                            if ($user_courier == $data_code && $value['service'] == $user_service) {
                                $radio->set_checked(true);
                                $list->add_class('in');
                                $radio->add_listener('ready')
                                        ->add_handler('custom')
                                        ->set_js($radio_js);
                            }
                        }
                        
                        if(ccfg::get('is_free_ongkir') && $value['cost'][0]['value']==0){
                            $free_ongkir = '(<span style="text-decoration: line-through;">Rp. '.$value['cost'][0]['real_value'].'</span>)';
                        }
                        
                        $list->add('<p>' . clang::__('Cost :') . ' Rp. ' . ctransform::thousand_separator($value['cost'][0]['value']) .' ' . $free_ongkir.  '</p>');
                        $list->add('<p>' . clang::__('Estimate :') . ' About ' . $value['cost'][0]['etd'] . ' ' . clang::__('days') . '</p>');
                        $list->add('<p>' . clang::__('Weight :') . ' ' . $weight . ' gr</p>');
                    }
                }
            }
            else {                
                $accordion->add(clang::__("Sorry we dont find any shipping for your area"));
				$session = Session::instance();
				$session->set('shipping_data', null);	
            }
        }
        echo $app->render();
    }

}
