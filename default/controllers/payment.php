<?php

class Payment_Controller extends ProductController {

    CONST __CONTROLLER = "payment/";

    public function __construct() {
        parent::__construct();
    }

    public function redirect($transaction_code) {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $q = "
            select
                    *
            from
                    transaction
            where
                    code=" . $db->escape($transaction_code) . "
        ";
        $r = $db->query($q);
        $session_id = '';
        if ($r->count() > 0) {
            $row = $r[0];
            $session_id = $row->session_id;
            $session = CApiClientSession::instance('PG', $session_id);
            $request['transaction_id'] = $row->transaction_id;
            $request['transaction_status'] = 'PENDING';
            $request['order_status'] = 'PENDING';
            $request['payment_status'] = 'PENDING';
            $request['transaction_payment_id'] = $session->get('transaction_payment_id');
            $method = 'after_payment';

            // get page 
            $transaction = $session->get('transaction');
            $page = carr::get($transaction, 'page');
            if ($page == 'product') {
                $cart = Cart62hall_Product::factory();
                $cart->regenerate();
            }

//				$api_data = CApiClient::instance('PG', strtoupper($session->get('product_code')))->exec($method, $request);


            curl::redirect(curl::base() . 'retrieve/invoice/' . $row->booking_code . '/' . $row->email);

            // kirim email bila payment type bank_transfer
            $payment_type = $session->get('payment_type');
            if ($payment_type == 'bank_transfer') {
                $this->send_transaction('order', $row->booking_code);
            }
        }

        echo $app->render();
    }

    public function format_request($method, $product_code, $getpost) {
        $app = CApp::instance();
        $org_id = null;
        $org = $app->org();
        if ($org) {
            $org_id = $org->org_id;
        }

        $db = CDatabase::instance();
        $session = CApiClientSession::instance('PG');
        $request = $getpost;
        switch ($method) {
            case 'login':
                return $request;
            case 'get_payment_charge':
                $session_transaction = $session->get('transaction');
                $request['currency_code'] = 'IDR';
                $request['amount'] = $session_transaction['amount_payment'];
                return $request;
            case 'payment' :
                $org_id = CF::org_id();
                $domain = '';
                $q = "
						select
							*
						from
							org
						where
							org_id=" . $db->escape($org_id) . "
					";
                $r = $db->query($q);
                if ($r->count() > 0) {
                    $row = $r[0];
                    $domain = $row->domain;
                }
                if ($session->get('payment_type') == 'bank_transfer') {
                    $session_bank_transfer = $session->get('bank_transfer');
                    $request['payment_info']['bank'] = $session->get('bank');
                    $request['payment_info']['bank_transfer'] = $session_bank_transfer;
                }
                $session_transaction = $session->get('transaction');
                $request['back_url'] = 'http://' . $domain . '/payment/redirect/';
                return $request;
                break;
        }
    }

    public function api($method, $product_code, $payment_type) {
        $getpost = array_merge($_GET, $_POST);
        $getpost['product_code'] = $product_code;
        $getpost['payment_type'] = $payment_type;

        $request = $this->format_request($method, $product_code, $getpost);
        $session = CApiClientSession::instance('PG');
        $session->set('last_method', $method);
        $session->set('request_' . $method, $request);
        $api_data = CApiClient::instance('PG', strtoupper($product_code))->exec($method, $request);
        $session->set('api_data_' . $method, $api_data);
        $session->set('have_' . $method, '1');
        if (isset($api_data['err_code']) && $api_data['err_code'] > 0) {
            $app = CApp::instance();
            cmsg::add('error', '[' . $method . '] ' . $api_data['err_message'] . " (" . $api_data['err_code'] . ')');
            if ($payment_type != 'bank_transfer') {
                echo $app->render();
                return;
            }
        } else {
            $session->set('have_success_' . $method, '1');
            $this->$method($product_code, $request, $api_data);
        }
    }

    public function login($product_code, $request, $api_data) {
        $app = CApp::instance();
        $err_code = carr::get($api_data, "err_code");
        $err_msg = carr::get($api_data, "err_message");
        $session = CApiClientSession::instance('PG');
        $payment_type = $session->get('payment_type');
        if ($err_code == 0) {
            $this->api('get_payment_charge', $product_code, $payment_type);
        }
    }

    public function get_payment_charge($product_code, $request, $api_data) {
        $app = CApp::instance();
        $session = CApiClientSession::instance('PG');
        $session_transaction = $session->get('transaction');
        $total = carr::get($session_transaction, 'amount_payment');
        $payment_type = $session->get('payment_type');
        $err_code = carr::get($api_data, "err_code");
        $err_msg = carr::get($api_data, "err_message");
        $data = carr::get($api_data, "data");
        $div_payment_info = $app->add_div('div_payment_info')->add_class('col-md-12 border-1 padding-0 margin-top-20');
        $add_div_submit = 1;
        switch ($payment_type) {
            case 'bank_transfer' : {
                    $this->api('payment', $product_code, $payment_type);
                    return;
                }
            case 'paypal' :
                $div_payment_info_text = $div_payment_info->add_div('div_payment_info_text')->add_class('col-md-6 padding-left-10 padding-20');
                $div_payment_info_value = $div_payment_info->add_div('div_payment_info_value')->add_class('col-md-6 padding-left-10 padding-20 text-right');
                //$div_payment_info_space=$div_payment_info->add_div('div_payment_info_space')->add_class('col-md-4 padding-0');
                $div_payment_info_text->add("
						<p class='margin-0'><label class='font-size14 '><b>Total </b></label></p>
						<p class='margin-0'><label class='font-size14 '><b>Charge </b></label></p>
						<p class='margin-0'><label class='font-size18 '><b>Grand Total </b></label></p>
						
					");
                $div_payment_info_value->add("
						<p class='margin-0  '><label class='font-size14 font-red'><b>Rp. " . ctransform::format_currency($total) . "</b></label></p>
						<p class='margin-0  '><label class='font-size14 font-red'><b>Rp. " . ctransform::format_currency($data['total']) . "</b></label></p>
						<p class='margin-0  '><label class='font-size18 font-red'><b>Rp. " . ctransform::format_currency($total + $data['total']) . "</b></label></p>
					");
                break;
            case 'credit_card' : {
                    $div_payment_info_text = $div_payment_info->add_div('div_payment_info_text')->add_class('col-md-6 padding-left-10 padding-20');
                    $div_payment_info_value = $div_payment_info->add_div('div_payment_info_value')->add_class('col-md-6 padding-left-10 padding-20 text-right');
                    //$div_payment_info_space=$div_payment_info->add_div('div_payment_info_space')->add_class('col-md-4 padding-0');
                    $div_payment_info_text->add("
						<p class='margin-0'><label class='font-size14 '><b>Total </b></label></p>
						<p class='margin-0'><label class='font-size14 '><b>Charge MDR</b></label></p>
						<p class='margin-0'><label class='font-size18 '><b>Grand Total </b></label></p>
						
					");
                    $div_payment_info_value->add("
						<p class='margin-0  '><label class='font-size14 font-red'><b>Rp. " . ctransform::format_currency($total) . "</b></label></p>
						<p class='margin-0  '><label class='font-size14 font-red'><b>Rp. " . ctransform::format_currency($data['total']) . "</b></label></p>
						<p class='margin-0  '><label class='font-size18 font-red'><b>Rp. " . ctransform::format_currency($total + $data['total']) . "</b></label></p>
					");
                    $div_payment_info_lft = $div_payment_info->add_div()->add_class('col-md-10');
                    $div_payment_info_rgt = $div_payment_info->add_div()->add_class('col-md-2 padding-top-20 padding-left-right');

                    $div_payment_info_lft->add('
						<p class="padding-left-right padding-top-20" ><label>Pembayaran menggunakan kartu kredit ini akan dikenakan charge yang di tanggung pengguna sebesar Rp.5500,00. (Rincian biaya di kanan belum termasuk charge tambahan dari bank)</label></p>
					');
                    $div_payment_info_lft->add('
						<p class="padding-left-right" ><label>Silahkan klik tombol lanjutkan, untuk melanjutkan pembayaran...</label></p>
						<p class="padding-left-right"><label>Anda akan segera diarahkan menuju situs pembayaran online terpercaya 62pay...</label></p>
					');
                    break;
                }
            case 'mandiri_ib' : {
                    $div_payment_info_lft = $div_payment_info->add_div()->add_class('col-md-10');
                    $div_payment_info_rgt = $div_payment_info->add_div()->add_class('col-md-2 padding-top-20 padding-left-right');

                    $div_payment_info_lft->add('
						<p class="padding-left-right padding-top-20" ><label>Pembayaran menggunakan mandiri ini akan dikenakan charge yang di tanggung pengguna sebesar Rp.5500,00. (Rincian biaya di kanan belum termasuk charge)</label></p>
					');
                    $div_payment_info_lft->add('
						<p class="padding-left-right" ><label>Silahkan klik tombol lanjutkan, untuk melanjutkan pembayaran...</label></p>
						<p class="padding-left-right"><label>Anda akan segera diarahkan menuju situs pembayaran online terpercaya 62pay...</label></p>
					');
                    break;
                }
            default : {
                    $div_payment_info_lft = $div_payment_info->add_div()->add_class('col-md-10');
                    $div_payment_info_rgt = $div_payment_info->add_div()->add_class('col-md-2 padding-top-20 padding-left-right');

                    $div_payment_info_lft->add('
						<p class="padding-left-right padding-top-20" ><label>Silahkan klik tombol lanjutkan, untuk melanjutkan pembayaran...</label></p>
						<p class="padding-left-right"><label>Anda akan segera diarahkan menuju situs pembayaran online terpercaya 62pay...</label></p>
					');

                    $button_submit = $div_payment_info_rgt->add_field()->add_action('submit_button')->set_label(clang::__('Lanjutkan'))->add_class('btn-62hallfamily bg-red');
                    $add_div_submit = 0;
                    break;
                }
        }
        if ($add_div_submit > 0) {
            $div_submit = $app->add_div('div_submit')->add_class('col-md-12 border-1 padding');
            $div_submit_left = $div_submit->add_div('div_submit_left')->add_class('col-md-6 padding-left-10 padding-20');
            $div_submit_right = $div_submit->add_div('div_submit_right')->add_class('col-md-6 padding-left-10 padding-20');
            $button_submit = $div_submit_right->add_field()->add_action('submit_button')->set_label(clang::__('Lanjutkan'))->add_class('btn-62hallfamily bg-red pull-right');
        }
        $listener_button_submit = $button_submit->add_listener('click');
        $listener_button_submit
                ->add_handler('reload')
                ->set_target('div_payment_info')
                ->set_url(curl::base() . self::__CONTROLLER . 'api/payment/' . $product_code . '/' . $payment_type);
        echo $app->render();
    }

    public function payment($product_code, $request, $api_data) {
        $app = CApp::instance();
        $data = carr::get($api_data, 'data');
        $err_code = carr::get($data, 'err_code');
        if ($err_code == 0) {
            if (isset($data['redirect_url'])) {

                // get page
                $session = CApiClientSession::instance('PG');
                $transaction = $session->get('transaction');
                $page = carr::get($transaction, 'page');
                if ($page == 'product') {
                    $cart = Cart62hall_Product::factory();
                    $cart->regenerate();
                }

                $app->add_js("
                                            window.location.href='" . $data['redirect_url'] . "';
                                    ");
                echo $app->render();
            }
        }
    }

    public function insert_bank_transfer($bank, $account_number) {
        $app = CApp::instance();
        $session_app = Session::instance();
        $session = CApiClientSession::instance('PG');
        $session_id = $session->get('session_id');
        $request = $session->get('request');
        $booking_code = $session->get('booking_code');
        $transaction_id = $session->get('transaction_id');
        $transaction_email = $session->get('transaction_email');
        $reset_payment = $session->get('reset_payment');
        $session_transaction = $session->get('transaction');
        $session->clear();
        $session->set('transaction', $session_transaction);
        $session->set('payment_type', 'bank_transfer');
        $session->set('bank', $bank);
        $session->set('account_number', $account_number);
        $session->set('request', $request);
        $session->set('booking_code', $booking_code);
        $session->set('transaction_id', $transaction_id);
        $session->set('reset_payment', $reset_payment);
        $session->set('transaction_email', $transaction_email);
        $err_code = 0;
        $err_message = '';
        $api_client = CApiClient::instance('PG', 'BT');
        $total_item = $session_transaction['amount_payment'];
        if ($err_code == 0) {
            if ($total_item <= 10000) {
                $err_code++;
                $err_message = "";
            }
        }

        if ($err_code == 0) {
            if (ccfg::get('transaction_member_only') > 0) {
                $member_id = $session_app->get('member_id');
                if ($member_id == null && strlen($member_id) == 0) {
                    $err_code++;
                    $err_message = clang::__("Harap login member sebelum melakukan transaksi");
                }
            }
        }
        if ($err_code == 0) {
            if ($session->get('request') != '1' && $session->get('reset_payment') != '1') {
                $items = carr::get($session_transaction, 'item');
                $check_price = $api_client->engine()->check_price($items, $session_transaction['type']);
                $session->set('check_price_before_payment', $check_price);
                $err_code = $check_price['err_code'];
                $err_message = $check_price['err_message'];
            }
        }
        if ($err_code == 0) {
            if ($session->get('request') != '1' && $session->get('reset_payment') != '1' && $session_transaction['page'] != 'pulsa') {
                $type = carr::get($session_transaction, 'type');
                if ($type != 'sell') {
                    $items = carr::get($session_transaction, 'item');
                    // $check_stock = $api_client->engine()->check_stock($items, $session_transaction['type']);
                    // $session->set('check_stock_before_payment', $check_stock);
                    // $err_code = $check_stock['err_code'];
                    // $err_message = $check_stock['err_message'];
                }
            }
        }
        if ($err_code == 0) {
            $session_contact = carr::get($session_transaction, 'contact_detail');
            $session_contact_shipping = carr::get($session_contact, 'shipping_address');
            $city_id = carr::get($session_contact_shipping, 'city_id');
            $items = carr::get($session_transaction, 'item', array());
            $arr_item = array();
            foreach ($items as $key => $val) {
                $arr_item[] = $val['item_id'];
            }

            $check_shipping_city = shipping::check_product_available_shipping_city($arr_item, $city_id);
            $err_code = $check_shipping_city['err_code'];
            $err_message = $check_shipping_city['err_message'];
        }
        if ($err_code == 0) {
            $before_payment = $api_client->engine()->before_payment();
            $err_code = $before_payment['err_code'];
            $err_message = $before_payment['err_message'];
        }

        if ($err_code == 0) {
            $update_payment = $api_client->engine()->update_payment_code();
            $err_code = $update_payment['err_code'];
            $err_message = $update_payment['err_message'];
        }
        if ($err_code == 0) {
            $booking_code = $session->get('booking_code');
            $transaction_id = $session->get('transaction_id');
            $email = $session->get('transaction_email');
            email::send('IMTransaction', $transaction_id);

            // get page
            $page = carr::get($session_transaction, 'page');
            if ($page == 'product') {
                $cart = Cart62hall_Product::factory();
                $cart->regenerate();
            }
            $app->add_js("
					window.location.href='" . curl::httpbase() . "/retrieve/invoice/" . $booking_code . "/" . $email . "'");
        } else {
            $app->add($err_message);
        }

        echo $app->render();
    }

    public function reload_bank_detail($id) {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = CApiClientSession::instance('PG');
        $api_client = CApiClient::instance('PG', 'BT');
        $session_transaction = $session->get('transaction');
        $err_code = 0;
        $err_message = '';
        $err_stock_code = 0;

        //Validate 
        $total_item = $session_transaction['amount_payment'];

        $type = carr::get($session_transaction, 'type');
        if ($type != 'sell') {
            $items = carr::get($session_transaction, 'item');
            $check_stock = $api_client->engine()->check_stock($items, $session_transaction['type']);

            $err_stock_code = $check_stock['err_code'];
            $err_stock_err_message = $check_stock['err_message'];
//                         $session->set('check_stock_before_payment', $check_stock);
//                         $err_code = $check_stock['err_code'];
//                         $err_message = $check_stock['err_message'];
        }
        $q = "SELECT * FROM org_bank 
                WHERE status > 0 
                    AND org_bank_id = " . $db->escape($id);
        $data = cdbutils::get_row($q);

        $bank_name = cobj::get($data, 'bank');
        $branch = cobj::get($data, 'branch');
        $account_number = cobj::get($data, 'account_number');
        $account_holder = cobj::get($data, 'account_holder');

        $container = $app->add_div()->add_class('txt-bank-detail');
        $container->add_div()->add_class('txt-title-detail')->add(clang::__("Bank Transfer") . ' ' . $bank_name);
        $container->add_div()->add_class('txt-caution')->add(clang::__("Penting") . ' :');
        $text_caution = "Harap melakukan transfer maksimal 1x24 jam, jika tidak maka transaksi akan dibatalkan.";
        $container->add_div()->add_class('txt-caution-detail')->add(clang::__($text_caution));
        $text_rekening = "No Rekening: <span class='detail_rekening'>" . $account_number . "</span>";
        $container->add_div()->add_class('txt-rekening-detail')->add($text_rekening);
        $text_holder = "Atas Nama: <span class='detail_holder'>" . $account_holder . "</span>";
        $container->add_div()->add_class('txt-holder-detail')->add($text_holder);
        $text = "
                1. User ID KlikBCA yang Anda masukkan adalah User ID yang masih aktif dan terdaftar.<br>
                2. Untuk melanjutkan pembayaran, klik tombol 'Bayar' dan Anda bisa melakukan pembayaran melalui KlikBCA www.klikbca.com dengan menggunakan UserID yang Anda masukkan.<br>
                3. Transaksi akan dibatalkan jika tidak melakukan pembayaran setelah 1x24 jam.";

        $container->add_div()->add_class('txt-bank-detail-description')->add($text);

//            $div_submit = $app->add_div('div_submit')->add_class('col-md-12 margin-top-10 padding-left-10 padding-20');
//            $button_submit = $div_submit->add_field()->add_action('submit_button')->set_label(clang::__('Lanjutkan'))->add_class('btn-primary btn-imlasvegas-secondary btn-submit-payment pull-right');
//            $listener_button_submit = $button_submit->add_listener('click');
//            $listener_button_submit
//                    ->add_handler('reload')
//                    ->set_target('div_payment_info')
//                    ->set_url(curl::base() . self::__CONTROLLER . 'insert_bank_transfer/' . $bank_name . '/' . $account_number);
//
//            $listener_button_submit->add_handler('custom')
//                    ->set_js("
//                    var total = " . $total_item . ";
//                        
//                    if (total <= 10000) {
//                        var message = '" . clang::__('Pembelian minimum metode pembayaran bank transfer di atas Rp. 10,000') . "';
//                        $.app.show_dialog('body', 'Peringatan', message);
//                    }
//                    else if(" . $err_stock_code . " > 0){
//                        $.app.show_dialog('body', 'Peringatan', '<center>" . $err_stock_err_message . "</center>');
//                    }
//                    else{
//                         $.app.show_loading();
//                    }
//                    ");
        echo $app->render();
    }

    public function reload_bank_detailx($bank) {
        $app = CApp::instance();
        $bank_name = '';
        $account_number = '';
        $account_holder = 'PT INDONESIA ENAM DUA';
        switch ($bank) {
            case 'bca' : {
                    $bank_name = 'BCA';
                    $account_number = '107-056-9333';
                    break;
                }
            case 'mandiri' : {
                    $bank_name = 'MANDIRI';
                    $account_number = '140-00-5562626-2';
                    break;
                }
            case 'bri' : {
                    $bank_name = 'BRI';
                    $account_number = '0096-01-003233-30-8';
                    break;
                }
            case 'bukopin' : {
                    $bank_name = 'BUKOPIN';
                    $account_number = '111-120-0224';
                    break;
                }
            case 'bii' : {
                    $bank_name = 'BII';
                    $account_number = '2-200-009-188';
                    break;
                }
            case 'cimb_niaga' : {
                    $bank_name = 'CIMB NIAGA';
                    $account_number = '800134631300';
                    break;
                }
            case 'danamon' : {
                    $bank_name = 'DANAMON';
                    $account_number = '359-602-1380';
                    break;
                }
            case 'permata' : {
                    $bank_name = 'PERMATA';
                    $account_number = '701734750';
                    break;
                }
            case 'uob' : {
                    $bank_name = 'UOB';
                    $account_number = '426-300-2053';
                    break;
                }
        }
        $div_bank_account = $app->add_div('div_bank_account')->add_class('col-md-12 padding-left-10 padding-20');
        $div_bank_account->add("
				<p><label class='font-size18'>Bank " . $bank_name . "</label></p>
				<p><label class='font-size18'>Account Number " . $account_number . "</label></p>
				<p><label class='font-size18'>Account Holder " . $account_holder . "</label></p>
				<p><label class='font-size12'>*Jam Operasi : 08:30 s/d 21.00 WIB </label></p>
				<p><label class='font-size12'>**Mohon pastikan melakukan Transfer sesuai dengan nominal yang telah ditetapkan...</label></p>
				<p><label class='font-size12'>Transfer yang tidak sesuai dengan nominal pembayaran, akan dikenakan denda sesuai ketentuan yang berlaku...</label></p>
				
			");
        $div_submit = $app->add_div('div_submit')->add_class('col-md-12 margin-top-10 padding-left-10 padding-20');
        $button_submit = $div_submit->add_field()->add_action('submit_button')->set_label(clang::__('Lanjutkan'))->add_class('btn-62hallfamily bg-red pull-right');
        $listener_button_submit = $button_submit->add_listener('click');
        $listener_button_submit
                ->add_handler('reload')
                ->set_target('div_payment_info')
                ->set_url(curl::base() . self::__CONTROLLER . 'insert_bank_transfer/' . $bank . '/' . $account_number);

        $listener_button_submit->add_handler('custom')
                ->set_js("$.app.show_loading();");

        echo $app->render();
    }

    public function reload_bank() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $div_row = $app->add_div()->add_class('row row-bank');
        $div_bank = $div_row->add_div('div_bank')->add_class('border-0 padding-left-right0 padding-bottom-10 col-md-12');

        $q = "SELECT * FROM org_bank 
                WHERE status > 0 
                    AND org_id = " . CF::org_id();
        $data = $db->query($q);
        $div_row_bank = $div_bank->add_div()->add_class('row row-bank');
        foreach ($data as $key => $value) {
//            $bank_name = strtolower(cobj::get($value, 'bank'));
            $bank_name = cobj::get($value, 'code');
            $account_number = cobj::get($value, 'account_number');
            $org_bank_id = cobj::get($value, 'org_bank_id');
            $div_bank_list = $div_row_bank->add_div('div_bank_' . $bank_name)->add_class('border-1  col-sm-3 col-md-3');
            //$div_bank_list->add_div()->add_class('icon-payment icon-' . $bank_name);
            $div_radio_bank = $div_bank_list->add_div()->add_class('col-md-12 btn-container');
//                $div_radio_bank->add("<center>");
            $control_radio_bank_bca = $div_radio_bank->add_action('button_bank_' . $bank_name)
                    ->add_class('icon-payment icon-' . $bank_name);
//                $div_radio_bank->add("</center>");
            
            $listener_radio_bank_bca = $control_radio_bank_bca->add_listener('click');
            $listener_radio_bank_bca
                    ->add_handler('reload')
                    ->set_target('div_bank_detail')
                    ->set_url(curl::base() . self::__CONTROLLER . 'reload_bank_detail/' . $org_bank_id);
            
            $control_radio_bank_bca->add_listener('click')->add_handler('custom')->set_js("
                    //$('.payment-list-radio .bank_transfer').attr('bank_detail_payment','".$bank_name."/".$bank_name."');
                    $('.payment-list-radio_bank_transfer').click();
                    $('.payment-list-radio_bank_transfer').val('" . $bank_name . "_" . $account_number . "');
                ");
        }


        //       //bca
        //       $div_bank_bca = $div_bank->add_div('div_bank_bca')->add_class('border-1 col-md-3');
        //       $div_bank_bca->add_div()->add_class('icon-bca');
        //       $div_radio_bank_bca = $div_bank_bca->add_div()->add_class('col-md-12');
        //       $div_radio_bank_bca->add("<center>");
        //       $control_radio_bank_bca = $div_radio_bank_bca->add_control('radio_bank_bca', 'radio')->set_name('bank');
        //       $div_radio_bank_bca->add("</center>");
        //       $listener_radio_bank_bca = $control_radio_bank_bca->add_listener('click');
        //       $listener_radio_bank_bca
        //               ->add_handler('reload')
        //               ->set_target('div_bank_detail')
        //               ->set_url(curl::base() . self::__CONTROLLER . 'reload_bank_detail/bca');
        //       //mandiri
        //       $div_bank_mandiri = $div_bank->add_div('div_bank_mandiri')->add_class('border-1 col-md-3');
        //       $div_bank_mandiri->add_div()->add_class('icon-mandiri');
        //       $div_radio_bank_mandiri = $div_bank_mandiri->add_div()->add_class('col-md-12');
        //       $div_radio_bank_mandiri->add("<center>");
        //       $control_radio_bank_mandiri = $div_radio_bank_mandiri->add_control('radio_bank_mandiri', 'radio')->set_name('bank');
        //       $div_radio_bank_mandiri->add("</center>");
        //       $listener_radio_bank_mandiri = $control_radio_bank_mandiri->add_listener('click');
        //       $listener_radio_bank_mandiri
        //               ->add_handler('reload')
        //               ->set_target('div_bank_detail')
        //               ->set_url(curl::base() . self::__CONTROLLER . 'reload_bank_detail/mandiri');
        //       //BRI
        //       $div_bank_bri = $div_bank->add_div('div_bank_bri')->add_class('border-1 col-md-3');
        //       $div_bank_bri->add_div()->add_class('icon-bri');
        //       $div_radio_bank_bri = $div_bank_bri->add_div()->add_class('col-md-12');
        //       $div_radio_bank_bri->add("<center>");
        //       $control_radio_bank_bri = $div_radio_bank_bri->add_control('radio_bank_bri', 'radio')->set_name('bank');
        //       $div_radio_bank_bri->add("</center>");
        //       $listener_radio_bank_bri = $control_radio_bank_bri->add_listener('click');
        //       $listener_radio_bank_bri
        //               ->add_handler('reload')
        //               ->set_target('div_bank_detail')
        //               ->set_url(curl::base() . self::__CONTROLLER . 'reload_bank_detail/bri');
        //       //Bukopin
        //       $div_bank_bukopin = $div_bank->add_div('div_bank_bukopin')->add_class('border-1 col-md-3');
        //       $div_bank_bukopin->add_div()->add_class('icon-bukopin');
        //       $div_radio_bank_bukopin = $div_bank_bukopin->add_div()->add_class('col-md-12');
        //       $div_radio_bank_bukopin->add("<center>");
        //       $control_radio_bank_bukopin = $div_radio_bank_bukopin->add_control('radio_bank_bukopin', 'radio')->set_name('bank');
        //       $div_radio_bank_bukopin->add("</center>");
        //       $listener_radio_bank_bukopin = $control_radio_bank_bukopin->add_listener('click');
        //       $listener_radio_bank_bukopin
        //               ->add_handler('reload')
        //               ->set_target('div_bank_detail')
        //               ->set_url(curl::base() . self::__CONTROLLER . 'reload_bank_detail/bukopin');
        //       $div_bank = $app->add_div('div_bank-2')->add_class('border-0 padding-left-right0 padding-top-10 padding-bottom-10 col-md-12');
        //       //BII
        //       $div_bank_bii = $div_bank->add_div('div_bank_bii')->add_class('border-1 col-md-3');
        //       $div_bank_bii->add_div()->add_class('icon-bii');
        //       $div_radio_bank_bii = $div_bank_bii->add_div()->add_class('col-md-12');
        //       $div_radio_bank_bii->add("<center>");
        //       $control_radio_bank_bii = $div_radio_bank_bii->add_control('radio_bank_bii', 'radio')->set_name('bank');
        //       $div_radio_bank_bii->add("</center>");
        //       $listener_radio_bank_bii = $control_radio_bank_bii->add_listener('click');
        //       $listener_radio_bank_bii
        //               ->add_handler('reload')
        //               ->set_target('div_bank_detail')
        //               ->set_url(curl::base() . self::__CONTROLLER . 'reload_bank_detail/bii');
        //       //CIMB NIAGA
        //       $div_bank_cimb_niaga = $div_bank->add_div('div_bank_cimb_niaga')->add_class('border-1 col-md-3');
        //       $div_bank_cimb_niaga->add_div()->add_class('icon-cimb-niaga');
        //       $div_radio_bank_cimb_niaga = $div_bank_cimb_niaga->add_div()->add_class('col-md-12');
        //       $div_radio_bank_cimb_niaga->add("<center>");
        //       $control_radio_bank_cimb_niaga = $div_radio_bank_cimb_niaga->add_control('radio_bank_cimb_niaga', 'radio')->set_name('bank');
        //       $div_radio_bank_cimb_niaga->add("</center>");
        //       $listener_radio_bank_cimb_niaga = $control_radio_bank_cimb_niaga->add_listener('click');
        //       $listener_radio_bank_cimb_niaga
        //               ->add_handler('reload')
        //               ->set_target('div_bank_detail')
        //               ->set_url(curl::base() . self::__CONTROLLER . 'reload_bank_detail/cimb_niaga');
        //       //DANAMON
        //       $div_bank_danamon = $div_bank->add_div('div_bank_danamon')->add_class('border-1 col-md-3');
        //       $div_bank_danamon->add_div()->add_class('icon-danamon');
        //       $div_radio_bank_danamon = $div_bank_danamon->add_div()->add_class('col-md-12');
        //       $div_radio_bank_danamon->add("<center>");
        //       $control_radio_bank_danamon = $div_radio_bank_danamon->add_control('radio_bank_danamon', 'radio')->set_name('bank');
        //       $div_radio_bank_danamon->add("</center>");
        //       $listener_radio_bank_danamon = $control_radio_bank_danamon->add_listener('click');
        //       $listener_radio_bank_danamon
        //               ->add_handler('reload')
        //               ->set_target('div_bank_detail')
        //               ->set_url(curl::base() . self::__CONTROLLER . 'reload_bank_detail/danamon');
        // //PERMATA
        //       $div_bank_permata = $div_bank->add_div('div_bank_permata')->add_class('border-1 col-md-3');
        //       $div_bank_permata->add_div()->add_class('icon-permata');
        //       $div_radio_bank_permata = $div_bank_permata->add_div()->add_class('col-md-12');
        //       $div_radio_bank_permata->add("<center>");
        //       $control_radio_bank_permata = $div_radio_bank_permata->add_control('radio_bank_permata', 'radio')->set_name('bank');
        //       $div_radio_bank_permata->add("</center>");
        //       $listener_radio_bank_permata = $control_radio_bank_permata->add_listener('click');
        //       $listener_radio_bank_permata
        //               ->add_handler('reload')
        //               ->set_target('div_bank_detail')
        //               ->set_url(curl::base() . self::__CONTROLLER . 'reload_bank_detail/permata');
        //       $div_bank = $app->add_div('div_bank-3')->add_class('border-0 padding-left-right0 padding-top-10 padding-bottom-10 col-md-12');
        // //PERMATA
        //       $div_bank_uob = $div_bank->add_div('div_bank_uob')->add_class('border-1 col-md-3');
        //       $div_bank_uob->add_div()->add_class('icon-uob');
        //       $div_radio_bank_uob = $div_bank_uob->add_div()->add_class('col-md-12');
        //       $div_radio_bank_uob->add("<center>");
        //       $control_radio_bank_uob = $div_radio_bank_uob->add_control('radio_bank_uob', 'radio')->set_name('bank');
        //       $div_radio_bank_uob->add("</center>");
        //       $listener_radio_bank_uob = $control_radio_bank_uob->add_listener('click');
        //       $listener_radio_bank_uob
        //               ->add_handler('reload')
        //               ->set_target('div_bank_detail')
        //               ->set_url(curl::base() . self::__CONTROLLER . 'reload_bank_detail/uob');

        $app->add_div('div_bank_detail');
        echo $app->render();
    }

    public function reset_payment($transaction_id) {
        Session::instance()->set('62hallfamily_api_PG', null);
        $session = CApiClientSession::instance('PG');
        $db = CDatabase::instance();

        $transaction = cdbutils::get_row('select * from transaction where status>0 and transaction_id=' . $db->escape($transaction_id));


        $session->set('reset_payment', '1');

        $data_transaction = array();
        //get transaction item


        $array_item = array();
        $array_item_payment = array();
        $q = 'select i.name as item_name,i.code as item_code, td.product_id,td.qty, td.channel_sell_price as price from transaction_detail as td inner join product as i on i.product_id=td.product_id where td.transaction_id=' . $db->escape($transaction_id);
        $r = $db->query($q);


        foreach ($r as $row) {
            $item = array();
            $item['item_id'] = $row->product_id;
            $item['item_code'] = $row->item_code;
            $item['item_name'] = $row->item_name;
            $item['qty'] = $row->qty;
            $item['price'] = $row->price;
            $array_item[] = $item;
        }

        $array_item_payment = $array_item;
        if ($transaction->type == 'sell' && $transaction->product_type = 'gold') {
            $array_item_payment = array();
            $item_payment['item_id'] = null;
            $item_payment['item_code'] = 'DSG';
            $item_payment['item_name'] = 'Deal Sell Gold';
            $item_payment['qty'] = '1';
            $item_payment['price'] = '250000';
            $array_item_payment[] = $item_payment;
        }
        $total_item = 0;
        foreach ($array_item_payment as $item_payment) {
            $total_item += carr::get($item_payment, 'price') * carr::get($item_payment, 'qty');
        }
        $total_shipping = $transaction->total_shipping;
        $amount_payment = $total_item + $total_shipping;

        $transaction_contact = cdbutils::get_row('select * from transaction_contact where transaction_id=' . $db->escape($transaction_id));


        $arr_contact_billing['first_name'] = cobj::get($transaction_contact, 'billing_first_name');
        $arr_contact_billing['last_name'] = cobj::get($transaction_contact, 'billing_last_name');
        $arr_contact_billing['address'] = cobj::get($transaction_contact, 'billing_address');
        $arr_contact_billing['email'] = cobj::get($transaction_contact, 'billing_email');
        $arr_contact_billing['province_id'] = cobj::get($transaction_contact, 'billing_province_id');
        $arr_contact_billing['province'] = cobj::get($transaction_contact, 'billing_province');
        $arr_contact_billing['city_id'] = cobj::get($transaction_contact, 'billing_city_id');
        $arr_contact_billing['city'] = cobj::get($transaction_contact, 'billing_city');
        $arr_contact_billing['district_id'] = cobj::get($transaction_contact, 'billing_districts_id');
        $arr_contact_billing['district'] = cobj::get($transaction_contact, 'billing_district');
        $arr_contact_billing['postal_code'] = cobj::get($transaction_contact, 'billing_postal_code');
        $arr_contact_billing['phone'] = cobj::get($transaction_contact, 'billing_phone');
        $arr_contact_billing['country_code'] = 'IDN';

        $arr_contact_shipping['first_name'] = cobj::get($transaction_contact, 'shipping_first_name');
        $arr_contact_shipping['last_name'] = cobj::get($transaction_contact, 'shipping_last_name');
        $arr_contact_shipping['address'] = cobj::get($transaction_contact, 'shipping_address');
        $arr_contact_shipping['email'] = cobj::get($transaction_contact, 'shipping_email');
        $arr_contact_shipping['province_id'] = cobj::get($transaction_contact, 'shipping_province_id');
        $arr_contact_shipping['province'] = cobj::get($transaction_contact, 'shipping_province');
        $arr_contact_shipping['city_id'] = cobj::get($transaction_contact, 'shipping_city_id');
        $arr_contact_shipping['city'] = cobj::get($transaction_contact, 'shipping_city');
        $arr_contact_shipping['district_id'] = cobj::get($transaction_contact, 'shipping_districts_id');
        $arr_contact_shipping['district'] = cobj::get($transaction_contact, 'shipping_district');
        $arr_contact_shipping['postal_code'] = cobj::get($transaction_contact, 'shipping_postal_code');
        $arr_contact_shipping['phone'] = cobj::get($transaction_contact, 'shipping_phone');
        $arr_contact_shipping['country_code'] = 'IDN';

        $contact_detail = array();
        $contact_detail['first_name'] = carr::get($arr_contact_billing, 'first_name');
        $contact_detail['last_name'] = carr::get($arr_contact_billing, 'last_name');
        $contact_detail['email'] = carr::get($arr_contact_billing, 'email');
        $contact_detail['phone'] = carr::get($arr_contact_billing, 'phone');
        $contact_detail['billing_address'] = $arr_contact_billing;
        $contact_detail['shipping_address'] = $arr_contact_shipping;
        $contact_detail['same_billing_shipping'] = null;

        $data_transaction['page'] = $transaction->product_type;
        $data_transaction['type'] = $transaction->type;
        $data_transaction['item'] = $array_item;
        $data_transaction['item_payment'] = $array_item_payment;
        $data_transaction['total_item'] = $total_item;
        $data_transaction['total_shipping'] = $total_shipping;
        $data_transaction['amount_payment'] = $amount_payment;
        $data_transaction['contact_detail'] = $contact_detail;



        $session->set('transaction', $data_transaction);
        $session->set('transaction_email', $transaction->email);
        $session->set('transaction_id', $transaction_id);

        curl::redirect('payment/form_payment');
    }

    public function form_payment() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session_app = Session::instance();
        $member_id = $session_app->get('member_id');
        
        $file = CF::get_file('data', 'setting');
        if (file_exists($file)) {
            $setting = include $file;
            $cod_config = carr::get($setting, 'cod_config');
            $city_cod = carr::get($cod_config, 'city');
            $cod_product_type = carr::get($cod_config, 'product_type');
        }
        $error = 0;
        $error_message = '';
        if ($error == 0) {
            //get data session
            $session = CApiClientSession::instance('PG');

            $reset_payment = $session->get('reset_payment');

            $transaction = $session->get('transaction');


            // Shipping
            $shipping_transaction = carr::get($transaction, 'total_shipping');
            $total_transaction = carr::get($transaction, 'total_item');

            $page = carr::get($transaction, 'page');
            $transaction_type = carr::get($transaction, 'type');
            //contact
            $contact = carr::get($transaction, 'contact_detail');
            $contact_billing = carr::get($contact, 'billing_address');
            $contact_shipping = carr::get($contact, 'shipping_address');

            $first_name_pay = carr::get($contact_billing, 'first_name');
            $last_name_pay = carr::get($contact_billing, 'last_name');
            $name_pay = $first_name_pay . ' ' . $last_name_pay;
            $email_pay = carr::get($contact_billing, 'email');
            $address_pay = carr::get($contact_billing, 'address');
            $phone_pay = carr::get($contact_billing, 'phone');
            $province_pay_text = carr::get($contact_billing, 'province');
            $city_pay_text = carr::get($contact_billing, 'city');
            $district_pay_text = carr::get($contact_billing, 'district');
            $post_code_pay = carr::get($contact_billing, 'postal_code');

            $first_name_shipping = carr::get($contact_shipping, 'first_name');
            $last_name_shipping = carr::get($contact_shipping, 'last_name');
            $name_shipping = $first_name_shipping . ' ' . $last_name_shipping;
            $email_shipping = carr::get($contact_shipping, 'email');
            $address_shipping = carr::get($contact_shipping, 'address');
            $phone_shipping = carr::get($contact_shipping, 'phone');
            $province_shipping_text = carr::get($contact_shipping, 'province');
            $city_shipping_id = carr::get($contact_shipping, 'city_id');
            $city_shipping_text = carr::get($contact_shipping, 'city');
            $district_shipping_text = carr::get($contact_shipping, 'district');
            $post_code_shipping = carr::get($contact_shipping, 'postal_code');

            $product_transaction = carr::get($transaction, 'item');
            $product_payment = carr::get($transaction, 'item_payment');
            $check_same = carr::get($contact, 'same_billing_shipping');

            $lat = carr::get($transaction, 'lat');
            $lng = carr::get($transaction, 'lng');
            $map_location = carr::get($transaction, 'map_location');

            $session_app->set('session_post', $transaction);

            $total_qty = count($product_transaction);
            if ($total_qty == 0) {
                curl::redirect('');
                return;
            }

            // SHOPPING CART 
            $app->add_listener('ready')
                    ->add_handler('reload')
                    ->set_target('shopping-cart')
                    ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

            $product_category = product_category::get_product_category_menu('product');
            $product_type = cdbutils::get_row("select * from product_type where name=" . $db->escape($page));
            $saldo_lucky = cobj::get($product_type, 'saldo_lucky');
            // Menu

            $form = $app->add_form('form_payment')->add_class('form-62hallfamily form-gold-contact padding-0');

            //start
            $container = $form->add_div()->add_class('container');
            $container_row = $container->add_div()->add_class('row-update-contact-full row');
            // UPDATE CONTACT
            $view_shopping_cart = $container_row->add_div()->add_class('col-md-5 col-md-push-7 ringkasan-pemesanan margin-top-44 margin-bottom-30');
            $view_update_contact = $container_row->add_div()
                    ->add_class('col-md-7 col-md-pull-5 margin-top-44 margin-bottom-30');

            $contact_row = $view_update_contact->add_div()->add_class('row');
            $contact_row->add_div()->add_class('txt-title-contact')->add(clang::__('PILIH PEMBAYARAN'));

//                //------
//                $contact_row_wrap = $contact_row->add_div()->add_class('row');
//                $contact_col_1 = $contact_row_wrap->add_div()->add_class('col-xs-5 col-sm-3');
//                $contact_col_2 = $contact_row_wrap->add_div()->add_class('col-xs-7 col-sm-9');
//                $contact_col_1->add_div()->add(clang::__('Name'));
//                $contact_col_2->add_div()->add($name_pay);
//                $contact_row_wrap = $contact_row->add_div()->add_class('row');
//                $contact_col_1 = $contact_row_wrap->add_div()->add_class('col-xs-5 col-sm-3');
//                $contact_col_2 = $contact_row_wrap->add_div()->add_class('col-xs-7 col-sm-9');
//                $contact_col_1->add_div()->add(clang::__('Email'));
//                $contact_col_2->add_div()->add($email_pay);
//                $contact_row_wrap = $contact_row->add_div()->add_class('row');
//                $contact_col_1 = $contact_row_wrap->add_div()->add_class('col-xs-5 col-sm-3');
//                $contact_col_2 = $contact_row_wrap->add_div()->add_class('col-xs-7 col-sm-9');
//                $contact_col_1->add_div()->add(clang::__('Payment Address'));
//                $contact_col_2->add_div()->add($address_pay . ' Kec. ' . $district_pay_text .' - '. $post_code_pay);
//                $contact_col_2->add_div()->add($city_pay_text);
//                $contact_row_wrap = $contact_row->add_div()->add_class('row');
//                $contact_col_1 = $contact_row_wrap->add_div()->add_class('col-xs-5 col-sm-3');
//                $contact_col_2 = $contact_row_wrap->add_div()->add_class('col-xs-7 col-sm-9');
//                $contact_col_btn = $contact_col_2->add_div()->add_class('row');
//                $contact_col_btn_1 = $contact_col_btn->add_div()->add_class('col-sm-8');
//                $contact_col_btn_2 = $contact_col_btn->add_div()->add_class('col-sm-4 div-btn-change-address');
//                $contact_col_1->add_div()->add(clang::__('Phone'));
//                $contact_col_btn_1->add_div()->add($phone_shipping);
            //Hidden setelah reset payment / ganti pembayaran
//                if($reset_payment == null){
//                $contact_col_btn_2->add_div()->add("<a href='" . curl::base() . "products/updatecontact'><button type='button' class='btn btn-primary btn-imbuilding-light'>" . clang::__('Change Address') . "</button></a>");
//                }
//                $contact_row_wrap = $contact_row->add_div()->add_class('row');
//                $contact_col_1 = $contact_row_wrap->add_div()->add_class('col-xs-5 col-sm-3');
//                $contact_col_2 = $contact_row_wrap->add_div()->add_class('col-xs-7 col-sm-9');
//                //billing address
//                $contact_row = $view_update_contact->add_div()->add_class('row contact-row');
//                $contact_row->add_div()->add_class('payment-separate');
//                $contact_row->add_div()->add_class('txt-title-contact')->add(clang::__('Shipping Address'));
            //
//                $contact_row_wrap = $contact_row->add_div()->add_class('row');
//                $contact_col_1 = $contact_row_wrap->add_div()->add_class('col-xs-5 col-sm-3');
//                $contact_col_2 = $contact_row_wrap->add_div()->add_class('col-xs-7 col-sm-9');
//                $contact_col_1->add_div()->add(clang::__('Name'));
//                $contact_col_2->add_div()->add($name_shipping);
//                $contact_row_wrap = $contact_row->add_div()->add_class('row');
//                $contact_col_1 = $contact_row_wrap->add_div()->add_class('col-xs-5 col-sm-3');
//                $contact_col_2 = $contact_row_wrap->add_div()->add_class('col-xs-7 col-sm-9');
//                $contact_col_1->add_div()->add(clang::__('Email'));
//                $contact_col_2->add_div()->add($email_shipping);
//                $contact_row_wrap = $contact_row->add_div()->add_class('row');
//                $contact_col_1 = $contact_row_wrap->add_div()->add_class('col-xs-5 col-sm-3');
//                $contact_col_2 = $contact_row_wrap->add_div()->add_class('col-xs-7 col-sm-9');
//                $contact_col_1->add_div()->add(clang::__('Shipping Address'));
//                $contact_col_2->add_div()->add($address_shipping . ' Kec. ' . $district_shipping_text .' - '. $post_code_shipping);
//                $contact_col_2->add_div()->add($city_shipping_text);
//                $contact_row_wrap = $contact_row->add_div()->add_class('row');
//                $contact_col_1 = $contact_row_wrap->add_div()->add_class('col-xs-5 col-sm-3');
//                $contact_col_2 = $contact_row_wrap->add_div()->add_class('col-xs-7 col-sm-9');
//                $contact_col_btn = $contact_col_2->add_div()->add_class('row');
//                $contact_col_btn_1 = $contact_col_btn->add_div()->add_class('col-sm-8');
//                $contact_col_btn_2 = $contact_col_btn->add_div()->add_class('col-sm-4 div-btn-change-address');
//                $contact_col_1->add_div()->add(clang::__('Phone'));
//                $contact_col_btn_1->add_div()->add($phone_shipping);
            //Hidden setelah reset payment / ganti pembayaran
//                if($reset_payment == null){
//                $contact_col_btn_2->add_div()->add("<a href='" . curl::base() . "products/updatecontact'><button type='button' class='btn btn-primary btn-imbuilding-light'>" . clang::__('Change Address') . "</button></a>");
//                }
            //--------
            // RINGKASAN PEMESANAN
            $arr_product = array();
            $db = CDatabase::instance();

            foreach ($product_payment as $key => $value) {
                $item_id = carr::get($value, 'item_id');
                $qty = carr::get($value, 'qty');
                $price = carr::get($value, 'price');
                $subtotal = $qty * $price;

                if (isset($value['attribute'])) {
                    $attributes = $value['attribute'];
                } else {
                    $attributes = carr::get($value, 'attribute');
                }


                $q = "SELECT file_path,image_name
                        FROM product 
                        WHERE status > 0 
                            AND product_id = " . $db->escape($item_id);

                $image_row = cdbutils::get_row($q);

                $image = null;
                if ($image_row != null) {
                    $image = $image_row->file_path;
                    if (strlen($image) == 0) {
                        $image = image::get_image_url($image_row->image_name);
                    }
                }



                $arr = array(
                    'product_id' => $item_id,
                    'name' => carr::get($value, 'item_name'),
                    'image' => $image,
                    'sub_total' => $subtotal,
                    'qty' => $qty,
                    'sell_price' => $price,
                    'attribute' => $attributes,
                );
                $arr_product[$item_id] = $arr;
            }

            $sess_shipping = $session_app->get('shipping_data');
            $get_transaction = $session_app->get('session_post');
            $get_total_item = $get_transaction['total_item'];

            if (count($sess_shipping) > 0) {
                $shipping_price = $sess_shipping['cost'];
                $arr_shipping = array(
                    'product_id' => null,
                    'qty' => $qty,
                    'sell_price' => $shipping_price
                );
                $arr_product[''] = $arr_shipping;
            }

            $view_shopping_cart->add_element('62hall-product-cart', 'product_cart')
                    ->set_shipping_price($shipping_transaction)
                    ->set_cart_list($arr_product)
                    ->set_count_item(count($product_transaction));
            // RINGKASAN PEMESANAN END =============
            //payment method start
            $arr_payment = array();
                if(ccfg::get('have_cod')){
                    $arr_payment[] = array(
                        'name' => 'cod',
                        'title' => 'Bayar di Tempat',
//                        'icon' => 'ico-cod',
                        'url' => 'reload_cod',
                    );
                }
                
                $arr_payment[] = array(
                'name' => 'bank_transfer',
                'title' => 'Bank Transfer',
//                    'icon' => 'ico-bank-transfer',
                'url' => 'reload_bank',
                );

            $payment_first = carr::get($arr_payment, 0);
            $payment_title_first = carr::get($payment_first, 'title');
            $payment_first_val = carr::get($payment_first, 'name');
            $payment_first_url = carr::get($payment_first, 'url');

            $payment_second = carr::get($arr_payment, 1);
            $payment_title_second = carr::get($payment_second, 'title');
            $payment_second_val = carr::get($payment_second, 'name');
            $payment_second_url = carr::get($payment_second, 'url');
//                $payment_icon_first = carr::get($payment_first, 'icon');

            foreach ($arr_payment as $key => $value) {
                $label = carr::get($value, 'title');
                $url = carr::get($value, 'url');
                $name = carr::get($value, 'name');

                $payment_row = $view_update_contact->add_div()->add_class('row payment-row ' . $name);
                $payment_choice = $payment_row->add_div()->add_class('payment-choice_' . $name);
                // $payment_choice->add_div()->add_class('txt-payment-choice')->add(clang::__('Payment Method'));
                $payment_select = $payment_choice->add_div()->add_class('payment-select-wrap');
                $content_payment = $payment_choice->add_div()->add_class('content-payment');
                $container_payment_list = $content_payment->add_div()->add_class('row');

                $selected = "";
                if ($key == 0) {
                    $selected = " checked ";
                }

//                    $icon = carr::get($value, 'icon');

                $payment_list = $container_payment_list->add_div()->add_class('col-sm-6');

                $payment_list_container = $payment_list->add_div()->add_class('payment-list-container');
                $value_radio = 0;
                if ($name == 'cod'){
                    $value_radio = 'cod';
                }
                $payment_list_container->add_div()->add_class('payment-list-content')->add("<input payment='" . $url . "' payment-url='" . $url . "' " . $selected . " type='radio' name='payment-list-choice'  class='payment-list-radio_" . $name . "' value='" . $value_radio . "'/>");
//                    $payment_list_container->add_div()->add_class('payment-list-content')->add_div()->add_class($icon);
                $payment_list_container->add_div()->add_class('payment-list-content')->add_div()->add_class('text-payment')->add($label);

                $target_name = '#target_payment_' . $name;
                $target_div = $payment_choice->add_div('target_payment_' . $name);
                $app->add_js("
//                        jQuery('.payment-list-radio_" . $name . "').on('click touchstart', function(){
//                            var selected_payment = jQuery(this).attr('payment');
//                            var payment_url = jQuery(this).attr('payment-url');
//                            var target_payment = '" . $target_name . "';
//                                
//                            // jQuery('#sel_payment_type').val(selected_payment);
//                            // var payment_select = jQuery(this).find('a').html()
//                            // jQuery('a.selected-payment').html(payment_select);
//                            ajax_payment(payment_url, target_payment);
//                        });
                        
                        jQuery('.payment-list-radio_" . $name . "').click(function (){
                            $('input[type=radio]').prop('checked', false);
                            $(this).prop('checked', true);
                            if(jQuery('.payment-list-radio_" . $name . "').val() == 'cod'){
                                    $('#div_bank_detail').hide();
                                    $('#target_payment_cod').show();
                            }
                            else {
                                    $('#div_bank_detail').show();
                                    $('#target_payment_cod').hide();
                            }
                        });
                    ");
            }


            // $content_payment->add('<input type="hidden" name="sel_payment_type" id="sel_payment_type" value="'.$payment_first_val.'"/> 
            //             <a class="btn btn-default dropdown-toggle selected selected-payment" data-toggle="dropdown" href="#"><div class="'.$payment_icon_first.'"></div><div class="txt-payment-title">'.$payment_title_first.'</div></a>');
            // $content_payment->add('<ul class="dropdown-menu">');
            // foreach ($arr_payment as $key => $value) {
            //     $payment_name = carr::get($value, 'name');
            //     $payment_icon = carr::get($value, 'icon');
            //     $payment_title = carr::get($value, 'title');
            //     $payment_url = carr::get($value, 'url');
            //     $content_payment->add('<li class="payment-list" payment="'.$payment_url.'" payment-url="'.$payment_url.'">
            //         <a href="javascript:void(0);">
            //             <div class="'.$payment_icon.'"></div>
            //             <div class="txt-payment-title">'.$payment_title.'</div>  
            //         </a>
            //      </li>');
            // }
            // $content_payment->add('</ul>');
//                $payment_choice->add_div('target_payment');
//                $payment_choice->add_div('div_payment_info')->add_class('col-md-12 border-1 padding-0 margin-top-20');
            //button bayar dan kembali
            $button_row = $view_update_contact->add_div()->add_class('row btn-bottom');
            if ($reset_payment == null) {
                $button_row->add_div()->add_class('btn-payment')->add("<a href='" . curl::base() . "products/updatecontact'><button type='button' class='btn btn-primary btn-imlasvegas-secondary btn-kembali'>" . clang::__('Kembali') . "</button></a>");
            }
            $btn_bayar_sekarang = $button_row->add_div()->add_class('btn-payment');
            $btn_bayar_sekarang->add_field()->add_action('submit_button')->set_label(clang::__('Bayar Sekarang'))->add_class('btn btn-primary btn-imlasvegas-secondary btn-bayar');
            $listener_btn_bayar = $btn_bayar_sekarang->add_listener('click');
            
            $message_cod = "<div class=\'txt-alert-cod\'>COD (Bayar di Tempat) hanya bisa digunakan untuk total order dibawah Rp 3.000.000,-</div>";
            if ($city_shipping_id != 375) {
                $message_cod = 'Metode pembayaran di tempat, hanya untuk wilayah surabaya';
            }
            
            $err_stock_code = 0;
            $api_client = CApiClient::instance('PG', 'BT');
            $total_item = $transaction['amount_payment'];
            if ($transaction_type != 'sell') {
                $check_stock = $api_client->engine()->check_stock($product_transaction, $transaction['type']);

                $err_stock_code = $check_stock['err_code'];
                $err_stock_err_message = $check_stock['err_message'];
//                         $session->set('check_stock_before_payment', $check_stock);
//                         $err_code = $check_stock['err_code'];
//                         $err_message = $check_stock['err_message'];
            }
            
            $listener_btn_bayar->add_handler('custom')->set_js("
                    if ($('.payment-list-radio_bank_transfer').is(':checked')){
                        var total = " . $total_item . ";
                        var value_bank = $('.payment-list-radio_bank_transfer').val();
                        var split_value = value_bank.split('_');
                        var bank_name = split_value[0];
                        var bank_number = split_value[1];
                        
                        if (total <= 10000) {
                             var message = '" . clang::__('Pembelian minimum metode pembayaran bank transfer di atas Rp. 10,000') . "';
                             $.app.show_dialog('body', 'Peringatan', message);
                        }
                        else if(" . $err_stock_code . " > 0){
                             $.app.show_dialog('body', 'Peringatan', '<center>" . $err_stock_err_message . "</center>');
                        }
                        else if(bank_name == '0'){
                            $.app.show_dialog('body', 'Peringatan', '<center>Silakan pilih Bank dengan Klik pada Logo bank</center>');
                        }
                        else 
                        {
                              $.app.show_loading();
                              jQuery.ajax({
                                 url: '" . curl::httpbase() . self::__CONTROLLER . "insert_bank_transfer/' + bank_name + '/' + bank_number,
                                 dataType: 'json',
                                 success: function(data){
                                     $.app.hide_loading();
                                     jQuery('#div_payment_info').append(data.html);
                                     var script = $.cresenity.base64.decode(data.js);
                                     eval(script); 
                                 },
                                 error: function(e) {
                                     $.app.hide_loading();
                                     console.log(e);
                                 }
                             });
                        }
                    } 
                    else
                    {
                        var total = " . $total_transaction . ";
                        var shipping_city = " . $city_shipping_id . ";
                        var message = '" . $message_cod . "';
                        
                        if (total > 3000000) {
                            $.app.show_dialog('body', 'Peringatan', message);
                        }
                        else if (shipping_city != 375){
                            $.app.show_dialog('body', 'Peringatan', message);
                        }
                        else {
                            $.app.show_loading();
                            jQuery.ajax({
                                url: '" . curl::httpbase() . self::__CONTROLLER . "insert_cod/',
                                dataType: 'json',
                                success: function(data){
                                    $.app.hide_loading();
                                    jQuery('#div_payment_info').append(data.html);
                                    var script = $.cresenity.base64.decode(data.js);
                                    eval(script); 
                                },
                                error: function(e) {
                                    $.app.hide_loading();
                                    console.log(e);
                                }
                            });
                        }
                    }
                ");
            
            $app->add_js("
                    jQuery('#shipping-cost').html('Rp ". ctransform::format_currency($shipping_transaction)."');
                    jQuery('#total-payment').html('Rp ". ctransform::format_currency($shipping_transaction+$total_transaction)."');    
                    
                    function ajax_payment(payment_url, target_payment) {                    
                    jQuery.ajax({
                        url: '" . curl::base() . self::__CONTROLLER . "'+payment_url,
                        dataType: 'json',
                        success: function(data){            
                            jQuery(target_payment).html(data.html);
                            var script = $.cresenity.base64.decode(data.js);
                            eval(script);
                        },
                        error: function(e) {
                            console.log(e);
                        }
                    });
                    }
                    $(document).ready(function (){
                        ajax_payment('" . $payment_first_url . "', '#target_payment_" . $payment_first_val . "');
                        ajax_payment('" . $payment_second_url . "', '#target_payment_" . $payment_second_val . "');
                    });
                 ");

            //end payment method
        }

        echo $app->render();
    }

    public function reload_cod() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $get = $_GET;
        $session_app = Session::instance();
        
        //dapatkan dynamic name utk tiap-tiap org
        $org_id = CF::org_id();
        $row = cdbutils::get_row("select domain from org where org_id=" . $db->escape($org_id));
        $org_name = cobj::get($row, 'domain');

        $err_code = 0;
        $err_message = "";
        if ($err_code == 0) {
            $container = $app->add_div()->add_class('box-payment-method');
            $container->add_div()->add_class('txt-caution')->add('<b>Anda bisa melakukan pembayaran di tempat.</b>');
            $container->add_div()->add_class('txt-caution-detail')->add('Pesanan produk Anda hanya diserahkan apabila '
                    . 'Anda telah menyelesaikan pembayaran ke kurir kami. '
                    . 'COD (Bayar di Tempat) hanya bisa digunakan untuk total order dibawah Rp 3.000.000,- '
                    . 'dan untuk produk-produk yang dikirimkan oleh ' . $org_name);
//            $button_submit = $container->add_div()->add_class('btn-next-payment-wrap')->add_action('submit_button')->set_label(clang::__('Lanjutkan'))->add_class('btn-primary btn-imlasvegas-secondary');
//            $listener_button_submit = $button_submit->add_listener('click');
            // $listener_button_submit
            //         ->add_handler('reload')
            //         ->set_target('div_payment_info')
            //         ->set_url(curl::base() . self::__CONTROLLER . 'insert_cod/');

//            $session = CApiClientSession::instance('PG');
//            $transaction = $session->get('transaction');
//            $item = carr::get($transaction, 'item');
//            $total = 0;
//            foreach ($item as $key => $value) {
//                $qty = carr::get($value, 'qty');
//                $price = carr::get($value, 'price');
//                $total += $qty * $price;
//            }
//            $message = "<div class=\'txt-alert-cod\'>COD (Bayar di Tempat) hanya bisa digunakan untuk total order dibawah Rp 3.000.000,-</div>";
//            $session_contact = carr::get($transaction, 'contact_detail');
//            $shipping_city = $session_contact['shipping_address']['city_id'];
//            if ($shipping_city != 375) {
//                $message = 'Metode pembayaran di tempat, hanya untuk wilayah surabaya';
//            }

//            $listener_button_submit->add_handler('custom')
//                    ->set_js("
//                    var total = " . $total . ";
//                    var shipping_city = " . $shipping_city . ";
//                    var message = '" . $message . "';
//                        
//                    if (total > 3000000) {
//                        $.app.show_dialog('body', 'Peringatan', message);
//                    }
//                    else if (shipping_city != 375){
//                        $.app.show_dialog('body', 'Peringatan', message);
//                    }
//                    else {
//                        $.app.show_loading();
//                        jQuery.ajax({
//                            url: '" . curl::httpbase() . self::__CONTROLLER . "insert_cod/',
//                            dataType: 'json',
//                            success: function(data){
//                                $.app.hide_loading();
//                                jQuery('#div_payment_info').append(data.html);
//                                var script = $.cresenity.base64.decode(data.js);
//                                eval(script); 
//                            },
//                            error: function(e) {
//                                $.app.hide_loading();
//                                console.log(e);
//                            }
//                        });
//                    }
//                ");
        }
        if ($err_code > 0) {
            $app->add($err_message);
        }
        echo $app->render();
    }

    public function reload_deposit($type = 'deposit') {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $get = $_GET;
        $session_app = Session::instance();
        $member_id = $session_app->get('member_id');
        $grand_total = carr::get($get, 'grand_total');
        $err_code = 0;
        $err_message = "";
        if ($err_code == 0) {
            if ($member_id == null) {
                $err_code++;
                $err_message = clang::__('Anda tidak login sebagai member');
            }
        }
        if ($err_code == 0) {
            $member_balance = 0;
            if ($type == 'deposit') {
                $member_balance = member::get_member_balance($member_id);
            } elseif ($type == 'deposit_lucky') {
                $member_balance = member::get_member_balance_lucky($member_id);
            }
            if ($member_balance < $grand_total) {
                if ($member_balance == null) {
                    $member_balance = 0;
                }
                $err_code++;
                $err_message = clang::__('Saldo deposit tidak cukup sisa saldo anda') . ' Rp. ' . ctransform::format_currency($member_balance);
            }
        }
        if ($err_code == 0) {
            $row = $app->add_div()->add_class('row');
            $col_text = $row->add_div()->add_class('col-md-4');
            $col_text->add("<p><label class='font-size18'>" . clang::__("Saldo") . "</label></p>");
            $col_text->add("<p><label class='font-size18'>" . clang::__("Grand Total") . "</label></p>");
            $col_text->add("<p><label class='font-size18'>" . clang::__("Sisa Saldo") . "</label></p>");
            $col_val = $row->add_div()->add_class('col-md-8');
            $col_val->add("<p><label class='font-size18'>" . ctransform::format_currency($member_balance) . "</label></p>");
            $col_val->add("<p><label class='font-size18'>" . ctransform::format_currency($grand_total) . "</label></p>");
            $col_val->add("<p><label class='font-size18'>" . ctransform::format_currency($member_balance - $grand_total) . "</label></p>");
            $div_submit = $app->add_div('div_submit')->add_class('col-md-12 padding-left-10 padding-20');
            $button_submit = $div_submit->add_field()->add_action('submit_button')->set_label(clang::__('Lanjutkan'))->add_class('btn-62hallfamily bg-red pull-right');
            $listener_button_submit = $button_submit->add_listener('click');
            $listener_button_submit
                    ->add_handler('reload')
                    ->set_target('div_payment_info')
                    ->set_url(curl::base() . self::__CONTROLLER . 'insert_deposit/' . $type);
        }
        if ($err_code > 0) {
            $app->add($err_message);
        }
        echo $app->render();
    }

    public function insert_cod() {
        $app = CApp::instance();
        $session_app = Session::instance();
        $session = CApiClientSession::instance('PG');
        $session_id = $session->get('session_id');
        $request = $session->get('request');
        $booking_code = $session->get('booking_code');
        $transaction_id = $session->get('transaction_id');
        $transaction_email = $session->get('transaction_email');
        $reset_payment = $session->get('reset_payment');
        $session_transaction = $session->get('transaction');
        $session->clear();
        $session->set('transaction', $session_transaction);
        $session->set('payment_type', 'cod');
        $session->set('request', $request);
        $session->set('booking_code', $booking_code);
        $session->set('transaction_id', $transaction_id);
        $session->set('reset_payment', $reset_payment);
        $session->set('transaction_email', $transaction_email);
        $err_code = 0;
        $err_message = '';
        $api_client = CApiClient::instance('PG', 'BT');
        if ($err_code == 0) {
            if ($session->get('request') != '1' && $session->get('reset_payment') != '1') {
                $items = carr::get($session_transaction, 'item');
                $check_price = $api_client->engine()->check_price($items, $session_transaction['type']);
                $session->set('check_price_before_payment', $check_price);
                $err_code = $check_price['err_code'];
                $err_message = $check_price['err_message'];
            }
        }
        if ($err_code == 0) {
            if ($session->get('request') != '1' && $session->get('reset_payment') != '1' && $session_transaction['page'] != 'pulsa') {
                $type = carr::get($session_transaction, 'type');
                if ($type != 'sell') {
                    $items = carr::get($session_transaction, 'item');
                    $check_stock = $api_client->engine()->check_stock($items, $session_transaction['type']);
                    $session->set('check_stock_before_payment', $check_stock);
                    $err_code = $check_stock['err_code'];
                    $err_message = $check_stock['err_message'];
                }
            }
        }
        if ($err_code == 0) {
            $session_contact = carr::get($session_transaction, 'contact_detail');
            if ($session_contact['shipping_address']['city_id'] != 375) {
                $err_code++;
                $err_message = 'Metode pembayaran di tempat, hanya untuk wilayah surabaya';
            }
        }
        if ($err_code == 0) {
            $session_contact = carr::get($session_transaction, 'contact_detail');
            $session_contact_shipping = carr::get($session_contact, 'shipping_address');
            $city_id = carr::get($session_contact_shipping, 'city_id');
            $items = carr::get($session_transaction, 'item', array());
            $arr_item = array();
            foreach ($items as $key => $val) {
                $arr_item[] = $val['item_id'];
            }

            $check_shipping_city = shipping::check_product_available_shipping_city($arr_item, $city_id);
            $err_code = $check_shipping_city['err_code'];
            $err_message = $check_shipping_city['err_message'];
        }
        if ($err_code == 0) {
            $before_payment = $api_client->engine()->before_payment();
            $err_code = $before_payment['err_code'];
            $err_message = $before_payment['err_message'];
        }
        if ($err_code == 0) {
            $booking_code = $session->get('booking_code');
            $email = $session->get('transaction_email');
            $this->send_transaction('payment', $booking_code, 'SUCCESS');


            // get page
            $page = carr::get($session_transaction, 'page');
            if ($page == 'product') {
                $cart = Cart62hall_Product::factory();
                $cart->regenerate();
            }
            if (strlen($before_payment['winner_transaction_id']) > 0) {
                try {
                    email::send('transaction', $before_payment['winner_transaction_id']);
                } catch (Exception $ex) {
                    
                }
            }


            // try { 
            //     $emails = email::send('MemberPickOrder', $before_payment['transaction_id']);
            // }
            // catch (Exception $exc) {
            //     echo $exc->getMessage();
            // }


            $app->add_js("
					window.location.href='" . curl::httpbase() . "/retrieve/invoice/" . $booking_code . "/" . $email . "'

				");
        } else {
            $app->add($err_message);
        }
        echo $app->render();
    }

    public function insert_deposit($type) {
        $app = CApp::instance();
        $session_app = Session::instance();
        $session = CApiClientSession::instance('PG');
        $session_id = $session->get('session_id');
        $request = $session->get('request');
        $booking_code = $session->get('booking_code');
        $transaction_id = $session->get('transaction_id');
        $transaction_email = $session->get('transaction_email');
        $reset_payment = $session->get('reset_payment');
        $session_transaction = $session->get('transaction');
        $session->clear();
        $session->set('transaction', $session_transaction);
        $session->set('payment_type', $type);
        $session->set('request', $request);
        $session->set('booking_code', $booking_code);
        $session->set('transaction_id', $transaction_id);
        $session->set('reset_payment', $reset_payment);
        $session->set('transaction_email', $transaction_email);
        $err_code = 0;
        $err_message = '';
        $api_client = CApiClient::instance('PG', 'BT');
        if ($err_code == 0) {
            $member_id = $session_app->get('member_id');
            if ($member_id == null && strlen($member_id) == 0) {
                $err_code++;
                $err_message = clang::__("Harap login member sebelum melakukan transaksi menggunakan deposit");
            }
        }

        if ($err_code == 0) {
            if ($session->get('request') != '1' && $session->get('reset_payment') != '1') {
                $items = carr::get($session_transaction, 'item');
                $check_price = $api_client->engine()->check_price($items, $session_transaction['type']);
                $session->set('check_price_before_payment', $check_price);
                $err_code = $check_price['err_code'];
                $err_message = $check_price['err_message'];
            }
        }
        if ($err_code == 0) {
            if ($session->get('request') != '1' && $session->get('reset_payment') != '1' && $session_transaction['page'] != 'pulsa') {
                $type = carr::get($session_transaction, 'type');
                if ($type != 'sell') {
                    $items = carr::get($session_transaction, 'item');
                    $check_stock = $api_client->engine()->check_stock($items, $session_transaction['type']);
                    $session->set('check_stock_before_payment', $check_stock);
                    $err_code = $check_stock['err_code'];
                    $err_message = $check_stock['err_message'];
                }
            }
        }
        if ($err_code == 0) {
            $session_contact = carr::get($session_transaction, 'contact_detail');
            $session_contact_shipping = carr::get($session_contact, 'shipping_address');
            $city_id = carr::get($session_contact_shipping, 'city_id');
            $items = carr::get($session_transaction, 'item', array());
            $arr_item = array();
            foreach ($items as $key => $val) {
                $arr_item[] = $val['item_id'];
            }

            $check_shipping_city = shipping::check_product_available_shipping_city($arr_item, $city_id);
            $err_code = $check_shipping_city['err_code'];
            $err_message = $check_shipping_city['err_message'];
        }
        if ($err_code == 0) {
            $before_payment = $api_client->engine()->before_payment();
            $err_code = $before_payment['err_code'];
            $err_message = $before_payment['err_message'];
        }
        if ($err_code == 0) {
            $booking_code = $session->get('booking_code');
            $email = $session->get('transaction_email');
            $this->send_transaction('payment', $booking_code, 'SUCCESS');

            // get page
            $page = carr::get($session_transaction, 'page');
            if ($page == 'product') {
                $cart = Cart62hall_Product::factory();
                $cart->regenerate();
            }
            if (strlen($before_payment['winner_transaction_id']) > 0) {
                try {
                    email::send('transaction', $before_payment['winner_transaction_id']);
                } catch (Exception $ex) {
                    
                }
            }

            $app->add_js("
					window.location.href='http://" . $this->domain() . "/retrieve/invoice/" . $booking_code . "/" . $email . "'

				");
        } else {
            $app->add_js('
                alert(' . $err_message . ');
                
                ');
            $app->add($err_message);
        }
        echo $app->render();
    }

    public function set_session_transaction_from_database($transaction_id) {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $post = $this->input->post();
        $get = $this->input->get();
        $request = array_merge($get, $post);
        $session = CApiClientSession::instance('PG');
        $error = 0;
        $error_message = '';
        $q = "
				select
					*
				from
					transaction
				where
					transaction_id=" . $db->escape($transaction_id) . "
			";
        $r = $db->query($q);
        if ($r->count() > 0) {
            $transaction_db = $r[0];
        }
        $transaction = array();
        $arr_contact = array();
        $arr_contact_billing = array();
        $arr_contact_shipping = array();
        $transaction['page'] = $transaction_db->product_type;
        $transaction['type'] = $transaction_db->type;
        $transaction['session_card_id'] = null;

        $q = "
				select
					*
				from
					transaction_contact
				where
					transaction_id=" . $db->escape($transaction_id) . "
			";
        $r = $db->query($q);
        if ($r->count() > 0) {
            $transaction_contact = $r[0];
        }

        $arr_contact_billing['first_name'] = $transaction_contact->billing_first_name;
        $arr_contact_billing['last_name'] = $transaction_contact->billing_last_name;
        $arr_contact_billing['address'] = $transaction_contact->billing_address;
        $arr_contact_billing['email'] = $transaction_contact->billing_email;
        $arr_contact_billing['province_id'] = $transaction_contact->billing_province_id;
        $arr_contact_billing['province'] = $transaction_contact->billing_province;
        $arr_contact_billing['city_id'] = $transaction_contact->billing_city_id;
        $arr_contact_billing['city'] = $transaction_contact->billing_city;
        $arr_contact_billing['district_id'] = $transaction_contact->billing_districts_id;
        $arr_contact_billing['district'] = $transaction_contact->billing_district;
        $arr_contact_billing['postal_code'] = $transaction_contact->billing_postal_code;
        $arr_contact_billing['phone'] = $transaction_contact->billing_phone;
        $arr_contact_billing['country_code'] = 'IDN';


        $arr_contact_shipping['first_name'] = $transaction_contact->shipping_first_name;
        $arr_contact_shipping['last_name'] = $transaction_contact->shipping_last_name;
        $arr_contact_shipping['address'] = $transaction_contact->shipping_address;
        $arr_contact_shipping['email'] = $transaction_contact->shipping_email;
        $arr_contact_shipping['province_id'] = $transaction_contact->shipping_province_id;
        $arr_contact_shipping['province'] = $transaction_contact->shipping_province;
        $arr_contact_shipping['city_id'] = $transaction_contact->shipping_city_id;
        $arr_contact_shipping['city'] = $transaction_contact->shipping_city;
        $arr_contact_shipping['district_id'] = $transaction_contact->shipping_districts_id;
        $arr_contact_shipping['district'] = $transaction_contact->shipping_district;
        $arr_contact_shipping['postal_code'] = $transaction_contact->shipping_postal_code;
        $arr_contact_shipping['phone'] = $transaction_contact->shipping_phone;
        $arr_contact_shipping['country_code'] = 'IDN';

        $product_code = '';
        $product_name = '';
        $product_price = '';
        $q = "
				select
					*
				from
					transaction_detail
				where
					status>0
					and transaction_id=" . $db->escape($transaction_id) . "
			";
        $r = $db->query($q);
        if ($r->count() > 0) {
            foreach ($r as $row) {
                $item = array();
                $product = product::get_product($row->product_id);
                $product_name = $product['name'];
                $product_code = $product['code'];
                $product_price = $row->channel_sell_price;
                $qty = $row->qty;
                $item['item_id'] = $row->product_id;
                $item['item_code'] = $product_code;
                $item['item_name'] = $product_name;
                $item['qty'] = $qty;
                $item['price'] = $product_price;
                $transaction['item'][] = $item;
                if ($transaction_db->product_type == 'gold') {
                    if ($transaction_db->type == 'sell') {
                        $product_id = NULL;
                        $product_code = 'DSG';
                        $product_name = 'Deal Sell Gold';
                        $product_price = 250000;
                        $qty = 1;
                    }
                }
                $item['item_id'] = $row->product_id;
                $item['item_code'] = $product_code;
                $item['item_name'] = $product_name;
                $item['qty'] = $qty;
                $item['price'] = $product_price;
                $transaction['item_payment'][] = $item;
            }
        }
        $shipping_type_id = $transaction_db->shipping_type_id;
        $total_shipping = $transaction_db->total_shipping;
        if ($total_shipping > 0) {
            $item['item_id'] = NULL;
            $item['item_code'] = 'SHP';
            $item['item_name'] = 'Shipping to ' . $transaction_contact->shipping_city;
            $item['qty'] = 1;
            $item['price'] = $total_shipping;
            $transaction['item_payment'][] = $item;
        }
        $total_item = ($transaction_db->channel_sell_price);
        $grand_total = $total_item + $total_shipping;
        $amount_payment = $grand_total;
        if ($transaction_db->product_type == 'gold') {
            if ($transaction_db->type == 'sell') {
                $amount_payment = 250000;
            }
        }
        $transaction['total_item'] = $total_item;
        $transaction['shipping_type_id'] = $shipping_type_id;
        $transaction['total_shipping'] = $total_shipping;
        $transaction['amount_payment'] = $amount_payment;

        $arr_contact['first_name'] = $arr_contact_billing['first_name'];
        $arr_contact['last_name'] = $arr_contact_billing['last_name'];
        $arr_contact['email'] = $arr_contact_billing['email'];
        $arr_contact['phone'] = $arr_contact_billing['phone'];
        $arr_contact['billing_address'] = $arr_contact_billing;
        $arr_contact['shipping_address'] = $arr_contact_shipping;
        $transaction['contact_detail'] = $arr_contact;
        $session->set('transaction', $transaction);
        $session->set('transaction_id', $transaction_id);
        $session->set('transaction_code', $transaction_db->code);
        $session->set('booking_code', $transaction_db->booking_code);
        $session->set('transaction_email', $transaction_db->email);
        $session->set('currency_code', 'IDR');
        //check payment if exist
        $q = "
				select
					*
				from
					transaction_payment
				where
					status>0
					and payment_status='PENDING'
					and transaction_id=" . $db->escape($transaction_id) . "
			";
        $r = $db->query($q);
        if ($r->count() > 0) {
            $payment = $r[0];
            $session->set('payment_type', $payment->payment_type);
            $session->set('payment_code', $payment->payment_code);
            $session->set('transaction_payment_id', $payment->transaction_payment_id);
            //check charge if exist
            $q = "
					select
						*
					from
						transaction_payment_charge
					where
						status>0
						and transaction_payment_id=" . $db->escape($payment->transaction_payment_id) . "
				";
            $r = $db->query($q);
            if ($r->count() > 0) {
                $arr_charge = array();
                $total_charge = 0;
                foreach ($r as $row_charge) {
                    $charge['code'] = $row_charge->code;
                    $charge['name'] = $row_charge->name;
                    $charge['amount'] = $row_charge->amount;
                    $arr_charge[$row_charge->code] = $charge;
                    $total_charge += $row_charge->amount;
                }
                $session->set('detail_charge', $arr_charge);
                $session->set('total_amount_charge', $total_charge);
                $session->set('total_amount_payment', $amount_payment);
            }
        }
        $bank_transfer = carr::get($request, 'bank_transfer');
        $error = 0;
        $error_message = '';
        if ($bank_transfer == '1') {
            $bank_to = carr::get($request, 'bank_to');
            $account_number_to = carr::get($request, 'account_number_to');
            $bank_from_id = carr::get($request, 'bank_from_id');
            if ($error == 0) {
                if (($bank_to == null) || strlen($bank_to) == 0) {
                    $error++;
                    $error_message = "Bank Tujuan Kosong";
                }
            }
            if ($error == 0) {
                if (($account_number_to == null) || strlen($account_number_to) == 0) {
                    $error++;
                    $error_message = "Rekening Tujuan Kosong";
                }
            }
            if ($error == 0) {
                if (($bank_from_id == null) || strlen($bank_from_id) == 0) {
                    $error++;
                    $error_message = "Bank Dari Kosong";
                }
            }
            if ($error == 0) {
                $bank_from = "";
                if ($bank_from_id) {
                    $q = "
							select
								name
							from
								bank
							where
								bank_id=" . $db->escape($bank_from_id) . "
						";
                    $bank_from = cdbutils::get_value($q);
                }
            }
            $account_number_from = carr::get($request, 'account_number_from');
            $description = carr::get($request, 'description');
            $year_pay = carr::get($request, 'year_pay');
            $month_pay = carr::get($request, 'month_pay');
            $day_pay = carr::get($request, 'day_pay');
            if ($error == 0) {
                if (($account_number_from == null) || strlen($account_number_from) == 0) {
                    $error++;
                    $error_message = "Rekening Dari Kosong";
                }
            }
            if ($error == 0) {
                if (($year_pay == null) || strlen($year_pay) == 0) {
                    $error++;
                    $error_message = "Tanggal Kosong";
                }
            }
            if ($error == 0) {
                if (($month_pay == null) || strlen($month_pay) == 0) {
                    $error++;
                    $error_message = "Tanggal Kosong";
                }
            }
            if ($error == 0) {
                if (($day_pay == null) || strlen($day_pay) == 0) {
                    $error++;
                    $error_message = "Tanggal Kosong";
                }
            }
            $amount_transfer = carr::get($request, 'amount');
            if ($error == 0) {
                if (($amount_transfer == null) || strlen($amount_transfer) == 0) {
                    $error++;
                    $error_message = "Nilai Transfer Kosong";
                }
            }

            if ($error == 0) {
                $arr_bank_transfer = array(
                    'bank_to' => $bank_to,
                    'account_number_to' => $account_number_to,
                    'bank_from' => $bank_from,
                    'account_number_from' => $account_number_from,
                    'description' => $description,
                    'transfer_date' => $year_pay . '-' . $month_pay . '-' . $day_pay,
                );
                $session->set('bank_transfer', $arr_bank_transfer);
                $session->set('confirm', '1');
                $session->set('bank', $arr_bank_transfer['bank_to']);
                $session->set('account_number', $arr_bank_transfer['account_number_to']);
            }

            if (isset($_GET['bank_to'])) {
                unset($_GET['bank_to']);
            }
            if (isset($_GET['account_number_to'])) {
                unset($_GET['account_number_to']);
            }
            if (isset($_GET['bank_from'])) {
                unset($_GET['bank_from']);
            }
            if (isset($_GET['account_number_from'])) {
                unset($_GET['account_number_from']);
            }
            if (isset($_GET['year_pay'])) {
                unset($_GET['year_pay']);
            }
            if (isset($_GET['month_pay'])) {
                unset($_GET['month_pay']);
            }
            if (isset($_GET['day_pay'])) {
                unset($_GET['day_pay']);
            }
            if ($error == 0) {
                // $this->api('login', 'IT', 'bank_transfer');
                // update status to confirmed
                //             $session_api_data_payment = $session->get('api_data_payment');
                // if($session_api_data_payment==null){
                // 	$error++;
                // 	$error_message="Failed on API";				
                // }
                if ($error == 0) {
                    // $error_payment=carr::get($session_api_data_payment,'err_code');
                    // if ($error_payment == 0) {
                    try {
                        $db->begin();
                        $data_update_transaction = array(
                            'transaction_status' => 'CONFIRMED',
                            'order_status' => 'CONFIRMED',
                            'payment_status' => 'CONFIRMED',
                        );

                        $db->update('transaction', $data_update_transaction, array('transaction_id' => $transaction_id));
                        $data_transaction_history = array(
                            'org_id' => $transaction_db->org_id,
                            'org_parent_id' => $transaction_db->org_parent_id,
                            'transaction_id' => $transaction_id,
                            'date' => date('Y-m-d H:i:s'),
                            'transaction_status_before' => 'PENDING',
                            'transaction_status' => 'CONFIRMED',
                            'ref_table' => 'transaction',
                            'ref_id' => $transaction_id,
                        );
                        $db->insert('transaction_history', $data_transaction_history);
                        $transaction_payment_id = $session->get('transaction_payment_id');
                        $data_update_transaction_payment = array(
                            'payment_code' => generate_code::get_next_payment_code(),
                            'bank_from' => $bank_from,
                            'account_number_from' => $account_number_from,
                            'bank_transfer_description' => $description,
                            'payment_status' => 'CONFIRMED',
                        );

                        $db->update('transaction_payment', $data_update_transaction_payment, array('transaction_payment_id' => $transaction_payment_id));
                    } catch (Exception $ex) {
                        $error++;
                        $error_message = 'fail on confirm';
                    }

                    if ($error == 0) {
                        $db->commit();

                        $transaction_info = transaction::get_transaction_contact('t.transaction_id', $transaction_id);
                        // kirim email confirmed
                        email::send('IMTransaction', $transaction_id);

                        $app->add_js("
									window.location.href='" . curl::httpbase() . "/retrieve/invoice/" . $transaction_db->booking_code . "/" . $transaction_db->email . "'
								");
                    } else {
                        $db->rollback();
                        $app->add("<div class='alert alert-danger'>" . $error_message . "</div>");
                    }
                    // }				
                }
            } else {
                $button_submit = $app->add_field()
                        ->add_action('submit_button')
                        ->set_label(clang::__('Submit'))
                        ->add_class('btn-62hallfamily btn btn-primary border-3-red bg-red pull-right');

                $listener_button_submit = $button_submit->add_listener('click');
                $listener_button_submit
                        ->add_handler('reload')
                        ->set_target('div_button')
                        ->add_param_input(array('bank_transfer', 'bank_to', 'account_number_to', 'bank_from_id', 'account_number_from', 'year_pay', 'month_pay', 'day_pay', 'amount'))
                        ->set_url(curl::base() . self::__CONTROLLER . 'set_session_transaction_from_database/' . $transaction_id);

                $app->add("<div class='alert alert-danger margin-top-10'>" . $error_message . "</div>");
                $app->add_js("$.app.hide_loading();");
            }
        }
        if ($transaction_db->order_status == 'APPROVED') {
            $session->set('request', '1');
            curl::redirect(self::__CONTROLLER . 'form_payment');
        }
        echo $app->render();
    }

    public function payment_confirmation($transaction_id) {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $q = "
            select
                    t.booking_code as booking_code,
                    t.email as email,
                    t.channel_sell_price as channel_sell_price,
                    t.total_shipping as total_shipping,
                    tc.billing_first_name as first_name,
                    tc.billing_last_name as last_name,
					t.transaction_status
            from
                    transaction as t
                    inner join transaction_contact as tc on tc.transaction_id=t.transaction_id
            where
                    t.transaction_id=" . $db->escape($transaction_id) . "
			";
        $r = $db->query($q);
        if ($r->count() > 0) {
            $transaction = $r[0];
            if ($transaction->transaction_status == 'PENDING') {
                $q = "
					select
							*
					from
							transaction_payment
					where
							status>0
							and payment_status='PENDING'
							and transaction_id=" . $db->escape($transaction_id) . "
				";
                $r = $db->query($q);
                if ($r->count() > 0) {
                    $payment = $r[0];
                    $form = $app->add_form('form_payment_confirmation')->add_class('form-62hallfamily form-gold-contact padding-0');
                    $form->add_control('bank_transfer', 'hidden')->set_value('1');
                    $form->add_control('bank_to', 'hidden')->set_value($payment->bank);
                    $form->add_control('account_number_to', 'hidden')->set_value($payment->account_number);
                    $container = $form->add_div('container')->add_class('container row-fluid');
                    $div_head = $container->add_div('div_head')->add_class('col-md-12 margin-20');

                    $title = $div_head->add_div()->add_class('font-size24 bold')
                            ->add('Konfirmasi Pembayaran');
                    $head = $div_head->add_div()->add_class('font-size18 margin-top-10')
                            ->add('Terima kasih atas pembayaran yang telah dilakukan. Mohon konfirmasi pembayaran Anda dengan <br>
						mengisi data ini untuk proses pemeriksaan pembayaran dan pengiriman pesanan Anda.<br>
						Tanpa konfirmasi ini, kami tidak dapat mengetahui pembayaran Anda.');

                    $div_control = $container->add_div('div_control')->add_class('col-md-12 margin-20');
                    $div_control_left = $div_control->add_div('div_control_left')->add_class('col-md-8');
                    $div_control_left->add_field()->set_label('Kode Pesanan')->add("<p class='margin-top-10 margin-bottom-10' ><label>" . $transaction->booking_code . "</label></p>");

                    $field = $div_control_left->add_field();
                    $field->set_label('Bank Tujuan');
                    $field->add('<p class="margin-top-10 margin-bottom-10" ><label>' . strtoupper($payment->bank) . ' (' . $payment->account_number . ')</label></p>');


                    $div_control_left->add_field()->set_label('Nama Pemilik Rekening<red>*</red>')->add_control('name', 'text')->add_class('margin-10')->set_value($transaction->first_name . ' ' . $transaction->last_name);

                    $field = $div_control_left->add_field();
                    $field->set_label('Info Rekening<red>*</red>');
                    $field->add_control('account_number_from', 'text')->add_class('width-40-percent')->set_placeholder('No Rekening');
                    $control_bank = $field->add_control('bank_from_id', 'bank-select')->custom_css('width', '15%');
                    $control_bank->set_all(false);

//                        tanggal transfer
                    $tgl_transfer = $div_control_left->add_div()->add_class('col-md-12 padding-0 row margin-top-20');
                    $label_tgl_transfer = $tgl_transfer->add_div()->add_class('col-md-2 padding-0')
                            ->add('Tanggal Transfer<red>*</red>', 'datetime')
                            ->custom_css('margin-right', '22px');
                    $control_tgl_transfer = $tgl_transfer->add_div()->add_class('col-md-9');

//                        set tanggal
                    $div_tgl = $control_tgl_transfer->add_div()->add_class('col-md-2 padding-0');
                    $tgl = date('d');
                    $list_tgl = date::array_date();
                    $tgl = $div_tgl->add_control('day_pay', 'select')
                            ->set_name('day_pay')
                            ->add_class('select-62hallfamily normal')
                            ->set_list($list_tgl)
                            ->set_value($tgl);

//                        set bulan
                    $div_bln = $control_tgl_transfer->add_div()->add_class('col-md-4 padding-0');
                    $bln = date('m');
                    $list_bln = date::array_month();
                    $bln = $div_bln->add_control('month_pay', 'select')
                            ->set_name('month_pay')
                            ->add_class('select-62hallfamily normal')
                            ->set_list($list_bln)
                            ->set_value($bln);

//                        set tahun
                    $div_thn = $control_tgl_transfer->add_div()->add_class('col-md-3 padding-0');
                    $thn = date('Y');
                    $list_thn = date::array_years(70);
                    $thn = $div_thn->add_control('year_pay', 'select')
                            ->set_name('year_pay')
                            ->add_class('select-62hallfamily normal')
                            ->set_list($list_thn)
                            ->set_value($thn);

                    $nilai_transaksi = $div_control_left->add_div()->add_class('col-md-12 row');
                    $nilai_transaksi->add_field()->set_label('Nilai Transaksi')
                            ->add_div()->add_class('margin-top-10')
                            ->add('<label style="margin-left:5px">' . ctransform::format_currency(($transaction->channel_sell_price + $transaction->total_shipping)) . '</label>');

                    $nilai_transfer = $div_control_left->add_field()->set_label('Nilai Transfer<red>*</red>')
                            ->add_control('amount', 'text')
                            ->add_class('margin-top-10');

                    $catatan = $div_control_left->add_field()->set_label('Catatan')
                            ->add_control('description', 'textarea')
                            ->set_row(5)
                            ->add_class('margin-top-10');
                    $form_information = $div_control_left->add_div()->add_class('form-group');
                    $empty_label = $form_information->add('<label class="control-label"></label>');
                    $information = $form_information->add_div()->add('<red>* Wajib Diisi</red>');
//                                        
//					$div_submit = $div_control_left->add_div()->add_class('col-md-12 row margin-top-20');
//
//					$div_label = $div_submit->add_div()->add_class('col-md-2');
//
//					$div_wajib = $div_submit->add_div()->add_class('col-md-2')
//							->add('<red>* Wajib Diisi</red>');

                    $div_button = $div_control_left->add_div('div_button');
                    $button_submit = $div_button->add_field()
                            ->add_action('submit_button')
                            ->custom_css('margin-right', '8px')
                            ->set_label(clang::__('Submit'))
                            ->add_class('btn-62hallfamily btn btn-primary border-3-red bg-red pull-right');

                    $listener_button_submit = $button_submit->add_listener('click');
                    $listener_button_submit
                            ->add_handler('reload')
                            ->set_target('div_button')
                            ->add_param_input(array('bank_transfer', 'bank_to', 'account_number_to', 'bank_from_id', 'account_number_from', 'year_pay', 'month_pay', 'day_pay', 'amount', 'description'))
                            ->set_url(curl::base() . self::__CONTROLLER . 'set_session_transaction_from_database/' . $transaction_id);

                    $listener_button_submit->add_handler('custom')
                            ->set_js("
                                  $.app.show_loading();
                          ");
                } else {
                    $app->add_div()->add_class('container')->add('
							<p style="text-align:center; font-size:24px; margin-top:30px;margin-bottom:30px;"><label >Pembayaran untuk transaksi ini telah di konfirmasi</label></p>
					');
                }
            } else {
                if ($transaction->transaction_status == 'EXPIRED') {
                    $app->add_div()->add_class('container')->add('
							<p style="text-align:center; font-size:24px; margin-top:30px;margin-bottom:30px;"><label>Maaf transaksi anda sudah expired.</label></p>
					');
                } else {
                    $app->add_div()->add_class('container')->add('
							<p style="text-align:center; font-size:24px; margin-top:30px;margin-bottom:30px;"><label>Maaf transaksi anda sudah di konfirmasi.</label></p>
					');
                }
            }
        }

        $app->add_js("
                                        $('#amount').focus(function(){
                                            $('#amount').val($.cresenity.unformat_currency($('#amount').val()));
                                        });
                                        $('#amount').blur(function(){
                                            $('#amount').val($.cresenity.format_currency($('#amount').val()));
                                        });
                                        ");

        echo $app->render();
    }

    public function test_bank_transfer() {
        $session = CApiClientSession::instance('PG', '20160114201352_PG_56979f1037569');
        $app = CApp::instance();
        $div = $app->add_div('div_payment_info')->add('aaaaa');
        $listener_div = $div->add_listener('ready');
        $listener_div
                ->add_handler('reload')
                ->set_target('div_payment_info')
                ->set_url(curl::base() . self::__CONTROLLER . 'api/login/IT/bank_transfer');
        echo $app->render();
    }

}
