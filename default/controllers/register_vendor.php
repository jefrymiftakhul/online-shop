<?php

    /**
     * Description of register_vendor
     *
     * @author Imam
     */
    class Register_Vendor_Controller extends ProductController {

        //put your code here
        public function index() {
            $app = CApp::instance();
            $db=CDatabase::instance();
            $org_id=CF::org_id();
            $menu_list = $this->menu_list();
            $link_list = $this->list_menu();
           $request = $_POST;
            $err_code = 0;
            $err_message = '';
            $fullname = "";
            $display_name = "";
            $company = "";
            $address = "";
            $province_id = "";
            $city_id = "";
            $districts_id = "";
            $postal_code = "";
            $phone = "";
            $bank = "";
            $bank_account = "";
            $account_holder = "";
            $email = "";
            $password = "";
            $retype_password = "";
            if ($request != null) {
                $fullname = carr::get($request, 'fullname');
                $display_name = carr::get($request, 'display_name');
                $company = carr::get($request, 'company');
                $address = carr::get($request, 'address');
                $province_id = carr::get($request, 'province_pay');
                $city_id = carr::get($request, 'city_pay');
                $districts_id = carr::get($request, 'districts_pay');
                $postal_code = carr::get($request, 'post_code');
                $phone = carr::get($request, 'phone');
                $bank = carr::get($request, 'bank_name');
                $bank_account = carr::get($request, 'account_number');
                $account_holder = carr::get($request, 'account_holder');
                $email = carr::get($request, 'email');
                $password = carr::get($request, 'password');
                $retype_password = carr::get($request, 'retype_password');
                // Validation

                if (strlen($fullname) == 0) {
                    $err_code++;
                    $err_message = clang::__('Nama Lengkap is required');
                }

                if ($err_code == 0) {
                    if (strlen($display_name) == 0) {
                        $err_code++;
                        $err_message = clang::__('Display Name is required');
                    }
                }

                if ($err_code == 0) {
                    if (strlen($company) == 0) {
                        $err_code++;
                        $err_message = clang::__('Company is required');
                    }
                }

                if ($err_code == 0) {
                    if (strlen($address) == 0) {
                        $err_code++;
                        $err_message = clang::__('Alamat is required');
                    }
                }

                if ($err_code == 0) {
                    if (strlen($province_id) == 0) {
                        $err_code++;
                        $err_message = clang::__('Provinsi is required');
                    }
                }

                if ($err_code == 0) {
                    if (strlen($city_id) == 0) {
                        $err_code++;
                        $err_message = clang::__('Kota is required');
                    }
                }

                if ($err_code == 0) {
                    if (strlen($districts_id) == 0) {
                        $err_code++;
                        $err_message = clang::__('Kecamatan is required');
                    }
                }

                if ($err_code == 0) {
                    if (strlen($postal_code) == 0) {
                        $err_code++;
                        $err_message = clang::__('Kode Pos is required');
                    }
                }

                if ($err_code == 0) {
                    if (!is_numeric($postal_code)) {
                        $err_code++;
                        $err_message = clang::__('Kode Pos must be a number');
                    }
                }

                if ($err_code == 0) {
                    if (strlen($phone) == 0) {
                        $err_code++;
                        $err_message = clang::__('Telepon is required');
                    }
                }

                if ($err_code == 0) {
                    if (!is_numeric($phone)) {
                        $err_code++;
                        $err_message = clang::__('Telepon must be a number');
                    }
                }

                if ($err_code == 0) {
                    if (strlen($bank) == 0) {
                        $err_code++;
                        $err_message = clang::__('Bank Name is required');
                    }
                }

                if ($err_code == 0) {
                    if (strlen($bank_account) == 0) {
                        $err_code++;
                        $err_message = clang::__('No Rekening is required');
                    }
                }

                if ($err_code == 0) {
                    if (!is_numeric($bank_account)) {
                        $err_code++;
                        $err_message = clang::__('No Rekening must be a number');
                    }
                }

                if ($err_code == 0) {
                    if (strlen($bank_account) == 0) {
                        $err_code++;
                        $err_message = clang::__('Atas Nama is required');
                    }
                }

                if ($err_code == 0) {
                    if (strlen($email) == 0) {
                        $err_code++;
                        $err_message = clang::__('Email is required');
                    }
                }

                if ($err_code == 0) {
                    if (!cvalid::email($email)) {
                        $err_code++;
                        $err_message = clang::__('Email is not valid');
                    }
                }
                
                if($err_code==0){
                    $db_user=cdbutils::get_row("select * from users where status>0 and org_id=".$db->escape($org_id)." and username=".$db->escape($email));
                    $db_vendor=cdbutils::get_row("select * from vendor where status>0 and org_id=".$db->escape($org_id)." and email=".$db->escape($email));
                    $db_member=cdbutils::get_row("select * from member where status>0 and org_id=".$db->escape($org_id)." and email=".$db->escape($email));
                    if($db_user!==null || $db_vendor!==null || $db_member!==null){
                        $err_code++;
                        $err_message=clang::__('Email sudah pernah terdaftar');;
                    }
                }
                
                if ($err_code == 0) {
                    if (strlen($password) == 0) {
                        $err_code++;
                        $err_message = clang::__('Password is required');
                    }
                }

                if ($err_code == 0) {
                    if (strlen($retype_password) == 0) {
                        $err_code++;
                        $err_message = clang::__('Retype Password is required');
                    }
                }

                if ($err_code == 0) {
                    if ($retype_password != $password) {
                        $err_code++;
                        $err_message = clang::__('Retype Password not same with Password');
                    }
                }
                
                if ($err_code == 0) {
                    $db->begin();
                    try {
                        $data_insert = array();
                        $data_insert['org_id'] = $org_id;
                        $data_insert['name'] = $fullname;
                        $data_insert['name_display'] = $display_name;
                        $data_insert['company'] = $company;
                        $data_insert['address'] = $address;
                        $data_insert['country_id'] = '94';
                        $data_insert['province_id'] = $province_id;
                        $data_insert['city_id'] = $city_id;
                        $data_insert['districts_id'] = $districts_id;
                        $data_insert['postal_code'] = $postal_code;
                        $data_insert['phone'] = $phone;
                        $data_insert['bank'] = $bank;
                        $data_insert['bank_account'] = $bank_account;
                        $data_insert['account_holder'] = $account_holder;
                        $data_insert['email'] = $email;
                        $data_insert['password'] = md5($password);
                        $data_insert['password_real'] = $password;
                        $db->insert('vendor',$data_insert);
                        
                    }
                    catch (Exception $ex) {
                        $err_code++;
                        $err_message=$ex;
                    }
                    
                }
                if ($err_code== 0) {
                    $db->commit();
                }
                else {
                    $db->rollback();
                }
            }

            $product_category = product_category::get_product_category_menu($this->page());
            // Menu
            $menu_list = array(
                'name' => 'KATEGORI',
                'menu' => $product_category
            );
            // $test=shipping::check_product_available_shipping_city(100,339);
            // cdbg::var_dump($test);



            $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
            $element_menu->set_menu($menu_list)
                    ->set_link($link_list);
            $container = $app->add_div()->add_class('container');
            $full = $container->add_div()->add_class('row')->add_div()->add_class('col-xs-12');
            $form_container = $container->add_div()->add_class('row')->add_div()->add_class('col-md-8 col-md-offset-2');
            if ($request != null) {
                if ($err_code > 0) {
                    $danger = $form_container->add_div()->add_class('alert alert-danger');
                    $danger->add($err_message);
                }
                else {
                    $danger = $form_container->add_div()->add_class('alert alert-success');
                    $danger->add(clang::__('Registered Successfully'));
                    curl::redirect('home');
                }
            }

            // title informasi vendor
            $title = $form_container->add('<h4 class="title text-center">Infromasi Vendor</h4>');
            $form = $form_container->add_form()->add_class('form-62hallfamily form-gold-contact')->set_layout('horizontal');
            // form register
            $form->add_field()
                    ->set_label(clang::__('Nama Lengkap'))
                    //->set_style_form_group('inline')->set_inline_without_default(false)
                    ->add_control('fullname', 'text')
                    ->set_value($fullname)
                    ->set_placeholder(clang::__('Nama Lengkap'));
            $form->add_field()
                    //->set_style_form_group('inline')->set_inline_without_default(false)
                    ->set_label(clang::__('Nama Display'))
                    ->add_control('display_name', 'text')
                    ->set_value($display_name)
                    ->set_placeholder(clang::__('Nama Display'));
            $form->add_field()
                    ->set_label(clang::__('Company'))
                    //->set_style_form_group('inline')->set_inline_without_default(false)
                    ->add_control('company', 'text')
                    ->set_value($company)
                    ->set_placeholder(clang::__('Company'));
            $form->add_field()
                    ->set_label(clang::__('Alamat'))
                    //->set_style_form_group('inline')->set_inline_without_default(false)
                    ->add_control('address', 'text')
                    ->set_value($address)
                    ->set_placeholder(clang::__('Alamat'));
            $div_contact = $form->add_div('div_contact');
            $div_province = $div_contact->add_div('div_province');
            $control_province_select = $div_province->add_field()
                    ->set_label('Propinsi')
                    //->set_style_form_group('inline')->set_inline_without_default(false)
                    ->add_control('province_pay', 'province-select')
                    ->set_value($province_id)
                    ->add_class('width-40-percent');
                    


            $control_province_select->set_all(false);
            $control_province_select->set_country_id(94);
            $listener_province_select = $control_province_select->add_listener('change');
            $listener_province_select
                    ->add_handler('reload')
                    ->set_target('div_city_pay')
                    ->set_url(curl::base() . 'reload/reload_city/')
                    ->add_param_input(array('province_pay'));
            
            $div_city = $div_contact->add_div('div_city_pay');
            $control_city_select=$div_city->add_field()->set_label('Kota')->add_control('city_pay','city-select')->add_class('width-40-percent')
                    ->set_value($city_id);
            $control_city_select->set_all(false);
            $listener_city_select=$control_city_select->add_listener('change');
            $listener_city_select
                    ->add_handler('reload')
                    ->set_target('div_districts_pay')
                    ->set_url(curl::base() . 'reload/reload_districts')
                    ->add_param_input(array('city_pay'));

            $div_districts = $div_contact->add_div('div_districts_pay');
            $control_districts_select=$div_districts->add_field()->set_label('Kecamatan')->add_control('districts_pay','districts-select')->add_class('width-40-percent')
                    ->set_value($districts_id);
            $control_districts_select->set_all(false);


            $form->add_field()
                    ->set_label(clang::__('Kode Pos'))
                    //->set_style_form_group('inline')->set_inline_without_default(false)
                    ->add_control('post_code', 'text')
                    ->set_value($postal_code)
                    ->set_placeholder(clang::__('Kode Pos'));
            $div_telepon = $form->add_div()->add_class('form-group row');
            $label = $div_telepon->add('<span class="control-label col-md-3">Telepon</span>');

            $groupped = $div_telepon->add_div()->add_class('input-group col-md-9')->add_attr('style', 'padding-left: 15px;padding-right:15px;');
            $groupped->add('<span class="input-group-addon" id="basic-addon1">+62</span>');
            $groupped->add_control('phone', 'text')->set_value($phone);

            $form->add_field()
                    ->set_label(clang::__('Bank'))
                    //->set_style_form_group('inline')->set_inline_without_default(false)
                    ->add_control('bank_name', 'text')
                    ->set_value($bank)
                    ->set_placeholder(clang::__('Bank'));
            $form->add_field()
                    ->set_label(clang::__('No Rekening'))
                    //->set_style_form_group('inline')->set_inline_without_default(false)
                    ->add_control('account_number', 'text')
                    ->set_value($bank_account)
                    ->set_placeholder(clang::__('No Rekening'));
            $form->add_field()
                    ->set_label(clang::__('Atas Nama'))
                    //->set_style_form_group('inline')->set_inline_without_default(false)
                    ->add_control('account_holder', 'text')
                    ->set_value($account_holder)
                    ->set_placeholder(clang::__('Nama Pada Rekening'));

            // title informasi login
            $title2 = $form->add('<h4 class="title text-center">Infromasi Login</h4>');

            $form->add_field()
                    ->set_label(clang::__('Email'))
                    //->set_style_form_group('inline')->set_inline_without_default(false)
                    ->add_control('email', 'text')
                    ->set_value($email)
                    ->set_placeholder(clang::__('Email'));
            $form->add_field()
                    ->set_label(clang::__('Password'))
                    //->set_style_form_group('inline')->set_inline_without_default(false)
                    ->add_control('password', 'password')
                    ->set_value($password)
                    ->set_placeholder(clang::__('Password'));
            $form->add_field()
                    ->set_label(clang::__('Retype Password'))
                    //->set_style_form_group('inline')->set_inline_without_default(false)
                    ->add_control('retype_password', 'password')
                    ->set_value($retype_password)
                    ->set_placeholder(clang::__('Retype Password'));

            $submit = $form->add_div()->add_class('text-center')->add_action()->set_label(clang::__('SUBMIT'))->add_class('btn-login-vendor btn-62hallfamily bg-red border-3-red margin-top-20')->set_submit(true);

            echo $app->render();
        }

        public function add_city($province_id = null) {
            $app = CApp::instance();
            echo $app->render();
        }

    }
    