<?php

class Auth_Controller extends SixtyTwoHallFamilyController {

    public function __construct() {
        parent::__construct();
    }
    
    

    public function login($method=null) {
        $app = CApp::instance();
        $db = CDatabase::instance();
        //$org_id = ccfg::get('org_id');
        $org_id = $this->org_id();

        // header('Access-Control-Allow-Origin: *'); 

        $post = array_merge($_GET,$_POST);
        $err_code = 0;
        $err_message = "";
        $data = array();

        $username = '';
        $password = '';
        $redirect = '';
        $location = '';

        if ($post != null) {
            $session = Session::instance();
            $username = carr::get($post, 'email_login');
            $password = carr::get($post, 'password_login');
            $redirect = carr::get($post, 'redirect');

            if (strlen($password) == 0) {
                $err_code++;
                $err_message = "Password belum diisi!";
            }

            if (strlen($username) == 0) {
                $err_code++;
                $err_message = "Email belum diisi!";
            }

            try {
                if ($err_code == 0) {
                    // $q = '
                    //       SELECT * FROM member WHERE status > 0 AND is_active=1 and is_verified = 1 
                    //       AND email = ' . $db->escape($username) . ' AND password = ' . $db->escape(md5($password)) . '
                    //       AND org_id = ' . $db->escape($org_id) . '
                    //     ';

                    $q = '
                          SELECT m.*, mc.name category_name FROM member m 
                            LEFT JOIN member_category mc
                                ON m.member_category_id = mc.member_category_id
                            WHERE m.status > 0 AND m.is_active=1 and m.is_verified = 1 
                                AND m.email = ' . $db->escape($username) . ' AND m.password = ' . $db->escape(md5($password)) . '
                                AND m.org_id = ' . $db->escape($org_id) . '
                        ';
                    $row = cdbutils::get_row($q);
                    
                    if ($row != null) {
                        $session->set('member_id', cobj::get($row, 'member_id'));
                        $session->set('email', cobj::get($row, 'email'));
                        $session->set('name', cobj::get($row, 'name'));
                        $session->set('member_category', cobj::get($row, 'category_name'));
                    } else {
                        $q = '
                                SELECT * FROM member WHERE status > 0
                                AND email = ' . $db->escape($username) . ' AND password = ' . $db->escape(md5($password)) . '
                                AND org_id = ' . $db->escape($org_id) . '
                              ';
                        $row = cdbutils::get_row($q);

                        if ($row != NULL) {
                            $link_resend_confirm = curl::base() . 'member/confirm/recend_confirm?email=' . $username;
                            if($method=='jsonp') {
                                $link_resend_confirm = curl::base() . 'auth/resend_confirm/' . cobj::get($row, 'member_id');
                            }
                            $err_code++;
                            $err_message = 'Email anda belum di verifikasi, <a href="' . $link_resend_confirm . '"><u>Kirim ulang verifikasi email</u></a>';
                        } else {
                            $err_code++;
                            $err_message = 'Email atau Password salah';
                        }
                    }
                }
            } catch (Exception $ex) {
                $err_code++;
                $err_message = clang::__('Error, Please try again.');
                clog::write('DB Error page auth/login. ' . $ex->getMessage());
            }

            if ($err_code == 0) {
                $err_message = clang::__("Login Berhasil.");
            }
        } else {
            $err_code++;
            $err_message = 'Invalid Request';
        }

        $return = array(
            'error' => $err_code,
            'message' => $err_message,
            'data' => $data,
        );
        if($method=='jsonp') {
            //without login front
            $login_front = true; 
            $email_post = carr::get($post, 'email_login');
            $arr_email = array('superadmin', 'mybigmall', 'admin', 'finance62hall', 'putri', 'adminvn');

            if (in_array($email_post, $arr_email)) {
                $login_front = false;
            } 

            //check user vendor_id
            $q = 'SELECT vendor_id 
                    FROM users 
                    WHERE status > 0 
                        AND org_id = '.$db->escape(CF::org_id()).'
                        AND username = '.$db->escape($email_post);
            $data_query = $db->query($q);
            $vendor_id = cdbutils::get_value($q);

            if (count($data_query) > 0) {
                if (strlen($vendor_id) > 0) {
                    $login_front = false;
                }
            }

            if (!$login_front) {
                $return = array(
                    'error' => 0,
                    'message' => 'Login Berhasil',
                    'data' => ''
                );
            }


            $json = cjson::encode($return);
            $callback = carr::get($_GET,'callback');
            if(strlen($callback)>0) {
                 echo $callback . '(' . $json . ')';
                 return;
            }
        }
        echo cjson::encode($return);
    }

    public function register() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();
        //$org_id = ccfg::get('org_id');
        $org_id = $this->org_id();

        $err_code = 0;
        $message = '';
        $data = array();

        $ip = crequest::remote_address();
        $date = date("Y-m-d H:i:s");
        $request = array_merge($_POST, $_GET);

        $redirect = '';
        if ($request != null) {
            $name = carr::get($request, 'name', NULL);
            $email = carr::get($request, 'email', NULL);
            $password = carr::get($request, 'password', NULL);
            $confirm_password = carr::get($request, 'confirm_password', NULL);
            $phone = carr::get($request, 'phone', NULL);
            $gender = carr::get($request, 'gender', NULL);
            $date_birth = carr::get($request, 'date_birth', NULL);
            $month_birth = carr::get($request, 'month_birth', NULL);
            $year_birth = carr::get($request, 'year_birth', NULL);
            $is_subscribe = carr::get($request, 'is_subscribe', 0);
            $is_subscribe_gold = carr::get($request, 'is_subscribe_gold', 0);
            $is_subscribe_service = carr::get($request, 'is_subscribe_service', 0);

            $redirect = carr::get($request, 'redirect', NULL);

            $name_validate = array('confirm_password', 'password', 'name', 'email', 'phone');
            foreach ($name_validate as $validate_k => $validate_v) {
                if (strlen(carr::get($request, $validate_v)) == 0) {
                    $err_code++;
                    if ($validate_v == 'name') {
                        $validate_v = 'Nama Lengkap';
                    }
                    if ($validate_v == 'confirm_password') {
                        $validate_v = 'Ulangi Password';
                    }
                    if ($validate_v == 'password') {
                        $validate_v = 'Password';
                    }
                    if ($validate_v == 'email') {
                        $validate_v = 'Email';
                    }
                    if ($validate_v == 'phone') {
                        $validate_v = 'No. Handphone';
                    }
                    if ($validate_v == 'gender') {
                        $validate_v = 'Jenis Kelamin';
                    }
                    $message = $validate_v . ' belum diisi!';
                }
            }

            if (strlen($password) < 6) {
                $err_code++;
                $message = 'Password minimal 6 digit';
            }

            if (strlen($email) > 0) {
                if (!cvalid::email($email)) {
                    $err_code++;
                    $message = 'Format email salah';
                }
            }

            // cek tanggal
            if ($err_code == 0) {
                // validate date of birth
                $max_date = cutils::day_count($month_birth, $year_birth);

                if ($date_birth > $max_date) {
                    $err_code++;
                    $message = clang::__('Tanggal Lahir salah');
                }
            }

            if ($err_code == 0) {
                // validate user already used
                $q = "SELECT count(*) as total 
                        FROM member
                        WHERE email = " . $db->escape($email) . " 
                        and org_id = " . $db->escape($org_id) . "
                        and status > 0
                        ";
                $total = cdbutils::get_value($q);
                if (strlen($total) > 0 && $total > 0) {
                    $err_code++;
                    $message = clang::__('Email sudah pernah digunakan');
                }
            }

            if ($err_code == 0) {
                if ($confirm_password != $password) {
                    $err_code++;
                    $message = clang::__('Password tidak sesuai dengan konfirmasi password');
                }
            }

            if ($err_code == 0) {
                $db->begin();
                try {
                    if ($err_code == 0) {
                        $data_member = array(
                            'org_id' => $org_id,
                            'name' => $name,
                            'phone' => $phone,
                            'email' => $email,
                            'gender' => $gender,
                            'date_of_birth' => $year_birth . '-' . $month_birth . '-' . $date_birth,
                            'is_subscribe' => $is_subscribe,
                            'is_subscribe_gold' => $is_subscribe_gold,
                            'is_subscribe_service' => $is_subscribe_service,
                            'password' => md5($password),
                            'date_join' => $date,
                            'created' => $date,
                            'createdby' => $email,
                            'createdip' => $ip
                        );
						// $tree = CTreeDB::factory('member');
						// $tree->set_org_id($org_id);
      //                   $member_id=$tree->insert($data_register);

                        $r = $db->insert('member', $data_member);
                        $member_id = $r->insert_id();

                        if ($is_subscribe) {
                            $data_subscribe = array(
                                'member_id' => $member_id,
                                'email_subscribe' => $email
                            );
                            $result = subscribe::save('product', $data_subscribe);
                            $err_code += $result['err_code'];
                            $message = $result['message'];
                        }

                        if ($is_subscribe_gold) {
                            $data_subscribe = array(
                                'member_id' => $member_id,
                                'email_subscribe' => $email
                            );
                            $result = subscribe::save('gold', $data_subscribe);
                            $err_code += $result['err_code'];
                            $message = $result['message'];
                        }

                        if ($is_subscribe_service) {
                            $data_subscribe = array(
                                'member_id' => $member_id,
                                'email_subscribe' => $email
                            );
                            $result = subscribe::save('service', $data_subscribe);
                            $err_code += $result['err_code'];
                            $message = $result['message'];
                        }
                    }
                } catch (Exception $ex) {
                    $err_code++;
                    $message = clang::__('Error, Please call administrator!');
                    clog::write('DB Error page auth/register. ' . $ex->getMessage());
                    $db->rollback();
                }
                if ($err_code == 0) {
                    $db->commit();
                    $message = clang::__("Email verifikasi telah dikirimkan ke email Anda.");

                    $session->set('register_id', $member_id);
                    try{
                        $this->send_verification($member_id);
                    } catch (Exception $ex) {
                         $err_code++;
                         $message= $ex;
                    }
                }
            }
        } else {
            $err_code++;
            $message = 'Invalid Request';
        }

        $return = array(
            'error' => $err_code,
            'message' => $message,
        );
        echo cjson::encode($return);
    }

    public function form_login() {
        $app = CApp::instance();

        $login_element = CElement_Imlasvegas_Login::factory()
                ->set_session(false)
                ->set_trigger_button(FALSE)
                ->set_is_login(FALSE)
                ->set_icon(FALSE)
                ->set_guest(TRUE)
                ->set_action_next(TRUE)
                ->set_reload(TRUE)
                ->set_action_url(curl::base() . 'auth/next_login')
                ->set_redirect(curl::base() . 'products/updatecontact');
        if ($this->have_register() > 0) {
            $login_element->set_register_button(TRUE);
        }
        $app->add($login_element);
        echo $app->render();
    }

    public function next_login($method=null) {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();

        $post = $_POST;
        
//        if(isset($post['state-from'])) {
//            if (strlen($post['state-from']) == 0) {
//                $this->register();
//                return;
//            }
//        }
        
        
        $err_code = 0;
        $err_message = "";
        $data = array();
        $org_id = $this->org_id();
        $email = '';
        $choise_login = carr::get($post, 'choise_login', NULL);

        if (!empty($choise_login)) {
            if ($choise_login == 'guest') {
                if ($err_code == 0) {
                    // $session->set('email', $email);
                }
            } else if ($choise_login == 'member') {
                $username = carr::get($post, 'email_login');
                $password = carr::get($post, 'password_login');
                $redirect = carr::get($post, 'redirect');

                if (strlen($username) == 0) {
                    $err_code++;
                    $err_message = "Username belum diisi!";
                }
                if (strlen($password) == 0) {
                    $err_code++;
                    $err_message = "Password belum diisi!";
                }

                try {
                    if ($err_code == 0) {
                        $q = '
                              SELECT * FROM member WHERE status > 0 AND is_active=1 AND is_verified = 1 
                              AND email = ' . $db->escape($username) . ' AND password = ' . $db->escape(md5($password)) . '
							  AND org_id= ' . $db->escape($org_id) . '
                            ';
                        $row = cdbutils::get_row($q);
                        if ($row != null) {
                            $session->set('member_id', cobj::get($row, 'member_id'));
                            $session->set('email', cobj::get($row, 'email'));
                            $session->set('name', cobj::get($row, 'name'));
                        } else {
                            $q = '
                                SELECT * FROM member WHERE status > 0
                                AND email = ' . $db->escape($username) . ' AND password = ' . $db->escape(md5($password)) . '
                                AND org_id = ' . $db->escape($org_id) . '
                              ';
                            $row = cdbutils::get_row($q);

                            if ($row != NULL) {
                                $link_resend_confirm = curl::base() . 'member/confirm/recend_confirm?email=' . $username;
                                if($method=='jsonp') {
                                    $link_resend_confirm = curl::base() . 'auth/resend_confirm/' . cobj::get($row, 'member_id');
                                }
                                $err_code++;
                                $err_message = 'Email anda belum di verifikasi, <a href="' . $link_resend_confirm . '"><u>Kirim ulang verifikasi email</u></a>';
                            } else {
                                $err_code++;
                                $err_message = 'Email atau Password salah';
                            }
                        }
                    }
                } catch (Exception $ex) {
                    $err_code++;
                    $err_message = clang::__('Error, Please try again.');
                    clog::write('DB Error page auth/login. ' . $ex->getMessage());
                }

                if ($err_code == 0) {
                    $err_message = clang::__("Login Berhasil.");
                }
            }
        } else {
            $err_code++;
            $err_message = 'Anda Belum Memilih';
        }

        $return = array(
            'error' => $err_code,
            'message' => $err_message,
            'data' => $data,
        );

        echo cjson::encode($return);
    }
    
    public function get_access_token(){
        $get_data = $_GET;
        $secret_id = carr::get($get_data, 'si');

        $app_id = ccfg::get("login_facebook_app_id");

        $err_code = 0;
        $err_message = '';

        if(strlen($secret_id) == 0){
            $err_code++;
            $err_message = "Secret ID Required";
        }

        if($err_code == 0){
            $url = "https://graph.facebook.com/oauth/access_token?client_id=".$app_id."&client_secret=".$secret_id."&grant_type=client_credentials";
echo $url;
            $curl = CCurl::factory(null);
            $curl->set_url($url);
            $curl->exec();

            $has_error = $curl->has_error();
            $response = $curl->response();

            if ($has_error) {
                $err_code++;
                $err_message = $has_error;
                echo $err_message;
            }
            else {
                echo $response;
            }
        }
        else {
            echo $err_message;
        }


    }
    
    public function vToken() {
        $db = CDatabase::instance();
        $org_id = CF::org_id();

        $request = $_POST;
        $access_token_user = carr::get($request, 'access_token');
        $user_id_request = carr::get($request, 'user_id');
        $auth = carr::get($request, 'auth');
        $profile = carr::get($request, 'profile');

        $access_token = ccfg::get("login_facebook_access_token");

        $err_code = 0;
        $err_message = clang::__('Login Success');
        $response = array();
        if (strlen($access_token_user) == 0) {
            $err_code++;
            $err_message = 'Access Token is required';
        }

        if ($err_code == 0) {
            if (strlen($user_id_request) == 0) {
                $err_code++;
                $err_message = 'User ID is required';
            }
        }

        if ($err_code == 0){
            if (strlen($access_token) == 0){
                $err_code++;
                $err_message = 'Access Token in Config is required';
            }
        }

        if ($err_code == 0) {
            // $url = "https://graph.facebook.com/debug_token?input_token=" . $access_token_user . "&access_token=1694184217492984|Ahctw5a8jneAcVAAqFAjbd2o2Co";
            $url = "https://graph.facebook.com/debug_token?input_token=" . $access_token_user . "&access_token=".$access_token;
            $curl = CCurl::factory(null);
            $curl->set_url($url);
            $curl->exec();

            $has_error = $curl->has_error();
            $response = $curl->response();
            if ($has_error) {
                $err_code++;
                $err_message = 'Connection Error [' . $has_error . ']';
            }
        }

        if ($err_code == 0) {
            $json_response = json_decode($response, true);
            if (json_last_error() > 0) {
                $err_code++;
                $err_message = 'Parsing Error.';
            }
        }
        if ($err_code == 0) {
            $error_fb = carr::get($json_response, 'error');
            if (isset($error_fb)) {
                $message_fb = carr::get($error_fb, 'message');
                $err_code++;
                $err_message = $message_fb;
            }
        }

        if ($err_code == 0) {
            $data = carr::get($json_response, 'data');
            if (!isset($data)) {
                $err_code++;
                $err_message = 'Error, Data invalid';
            }
        }
        if ($err_code == 0) {
            $app_id = carr::get($data, 'app_id');
            $application = carr::get($data, 'application');
            $expires_at = carr::get($data, 'expires_at');
            $is_valid = carr::get($data, 'is_valid');
            $metadata = carr::get($data, 'metadata');
            $scopes = carr::get($data, 'scopes');
            $user_id = carr::get($data, 'user_id');

            $error = carr::get($data, 'error');
            if (isset($error)) {
                $message = carr::get($error, 'message');
                $err_code++;
                $err_message = $message;
            }
        }
        if ($err_code == 0) {
            if ($app_id != ccfg::get('login_facebook_app_id')) {
                $err_code++;
                $err_message = 'Login Failed[0]';
            }
        }

        if ($err_code == 0) {
            if ($user_id != $user_id_request) {
                $err_code++;
                $err_message = 'Login Failed[1]';
            }
        }

        if ($err_code == 0) {
            if ($is_valid != true) {
                $err_code++;
                $err_message = 'Login Failed[2]';
            }
        }

        if ($err_code == 0) {
            $arr_profile = json_decode($profile, true);
            if (json_last_error() > 0) {
                $err_code++;
                $err_message = 'Parser Error[1]';
            }
        }
        if ($err_code == 0) {
            $email = carr::get($arr_profile, 'email');
            $name = carr::get($arr_profile, 'name');
            if (strlen($email) == 0) {
                $err_code++;
                $err_message = clang::__('Email not valid');
            }

            if (strlen($name) == 0) {
                $err_code++;
                $err_message =  clang::__('Name not valid');
            }
            //check email for member
            if ($err_code == 0) {
                $q = '
                    SELECT email FROM member WHERE status > 0 AND is_verified = 1 
                    AND email = ' . $db->escape($email) . ' AND org_id = '.$db->escape($org_id).' limit 1';
                $row = cdbutils::get_row($q);
                try {
                    if (count($row) > 0) {
                        $this->login_socmed($email, 'facebook');
                    }
                    else {
                        $this->register_socmed($email, $name, 'facebook');
                    }
                }
                catch (Exception $exc) {
                    $err_code++;
                    $err_message = $exc->getMessage();
                }
            }
        }

        if ($err_code > 0){
            $response = array(
                'error' => $err_code,
                'message' => $err_message
            );

            echo json_encode($response);
        }
    }
    
    public function vTokenGoogle(){
            $request = $_POST;
            $id_token = carr::get($request, 'id_token');
            $db = CDatabase::instance();
            $org_id = CF::org_id();

            $err_code = 0;
            $err_message = '';

            if(strlen($id_token) == 0){
                $err_code++;
                $err_message = "ID Token is required";
            }
            
            if($err_code == 0) {
                $url = "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=".$id_token;

                $curl = CCurl::factory(null);
                $curl->set_url($url);
                $curl->set_opt(CURLOPT_SSL_VERIFYPEER, FALSE);
                $curl->exec();


                $has_error = $curl->has_error();
                $response = $curl->response();
                if($has_error){
                    $err_code++;
                    $err_message = "Connection Error [".$has_error."]";
                }
            }
            
            if($err_code == 0) {
                $dc_response = json_decode($response, true);
                $err_description = carr::get($dc_response, 'error_description');

                if(strlen($err_description) > 0) {
                    $err_code++;
                    $err_message = "Auth Error [".$err_description."]";
                }            
            }

            if($err_code == 0) {
                $aud = carr::get($dc_response, 'aud');

                //validation app id google
                $cfg_app_id = ccfg::get("login_google_app_id");
                if($aud != $cfg_app_id) {
                    $err_code++;
                    $err_message = 'App ID not valid';
                }
            }

            //check email for member
            if ($err_code == 0) {
                $username = carr::get($dc_response, 'name');
                $email = carr::get($dc_response, 'email');

                $q = '
                    SELECT email FROM member WHERE status > 0 AND is_verified = 1 
                    AND email = ' . $db->escape($email) . ' AND org_id = '.$db->escape($org_id).' limit 1';
                $row = cdbutils::get_row($q);
                
                try {
                    if (count($row) > 0) {
                        $this->login_socmed($email, 'google');
                    }
                    else {
                        $this->register_socmed($email, $username, 'google');
                    }
                }
                catch (Exception $exc) {
                    $err_code++;
                    $err_message = $exc->getMessage();
                }
            }

            if ($err_code > 0) {
                $result = array(
                    "error" => $err_code,
                    "err_message" => $err_message
                );
                echo json_encode($result);
            }
        }

    
    private function login_socmed($username, $socmed_type) {
        $db = CDatabase::instance();

        $post = $_POST;
        $err_code = 0;
        $err_message = "";
        $data = array();
        $org_id = CF::org_id();

        $session = Session::instance();

        if (strlen($username) == 0) {
            $err_code++;
            $err_message = clang::__("Username")." ".clang::__("is required")."!";
        }

        try {
            if ($err_code == 0) {
                $q = '
                      SELECT * FROM member WHERE status > 0 AND is_active=1 and is_verified = 1 
                      AND email = ' . $db->escape($username) . '
                      AND org_id = ' . $db->escape($org_id) . '
                    ';
                $row = cdbutils::get_row($q);
                if ($row != null) {
                    $member_id = cobj::get($row, 'member_id');
                    $session->set('member_id', $member_id);
                    $session->set('email', cobj::get($row, 'email'));
                    $session->set('name', cobj::get($row, 'name'));

                    //update login type 
                    if ($socmed_type == 'facebook') {
                        $data_update['login_facebook'] = 1;
                    }
                    if ($socmed_type == 'google') {
                        $data_update['login_google'] = 1;
                    }
                    $db->update('member', $data_update, array('member_id' => $member_id));
                }
                else {
                    $err_code++;
                    $err_message = clang::__('Login failed');
                }
            }
        }
        catch (Exception $ex) {
            $err_code++;
            $err_message = clang::__('Error, Please try again');
        }

        if ($err_code == 0) {
            $err_message = clang::__("Login Successfully");
        }
        else {
            throw new Exception("Login Failed", $err_code);
        }

       $return = array(
           'error' => $err_code,
           'message' => $err_message
       );
       echo json_encode($return);

    }
    
    public function register_socmed($email, $name, $login_from) {
        $db = CDatabase::instance();

        $err_code = 0;
        $err_message = '';
        $data = array();
        $org_id = CF::org_id();

        $ip = crequest::remote_address();
        $date = date("Y-m-d H:i:s");
        $request = array_merge($_POST, $_GET);

        if (strlen($email) > 0) {
            if (!cvalid::email($email)) {
                $err_code++;
                $err_message = clang::__('Email is not valid');
            }
        }

        if ($err_code == 0) {
            // validate email already used
            $q = "SELECT count(*) as total 
            FROM member
            WHERE email = " . $db->escape($email) . " and status > 0
                AND org_id = ".$db->escape($org_id)."
            ";
            $total = cdbutils::get_value($q);
            if (strlen($total) > 0 && $total > 0) {
                $err_code++;
                $err_message = clang::__('Email already used');
            }
        }

        if ($err_code == 0) {
            $db->begin();
            try {
                if ($err_code == 0) {
                    $data_member = array(
                        'org_id' => CF::org_id(),
                        'name' => $name,
                        'email' => $email,
                        'date_join' => Date('Y-m-d H:i:s'),
                        'is_verified' => 1,
                        'is_active' => 1,
                        'created' => $date,
                        'createdby' => $email,
                        'createdip' => $ip,
                    );

                    if ($login_from == 'facebook') {
                        $data_member['login_facebook'] = 1;
                    }
                    if ($login_from == 'google') {
                        $data_member['login_google'] = 1;
                    }

                    $r = $db->insert('member', $data_member);
                    $member_id = $r->insert_id();
                }
            }
            catch (Exception $ex) {
                $err_code++;
                $err_message = clang::__('Error, Please call administrator!');
                if (strlen($redirect) > 0) {
                    cmsg::add('error', clang::__('Error, Please call administrator!'));
                }
                $db->rollback();
            }

            if ($err_code == 0) {
                $db->commit();
                $err_message = clang::__("Register Success");
            }
        }


        if ($err_code > 0) {
            throw new Exception($err_message);
        }

        if ($err_code == 0) {
            $this->login_socmed($email, $login_from);
        }
    }


}
