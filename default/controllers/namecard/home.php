<?php

/**
 *
 * @author Riza 
 * @since  Nov 12, 2015
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class Home_Controller extends NameCardController {

    private $_count_item = 20;

    CONST __is_test = false;

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $app = CApp::instance();
        $user = $app->user();
        $org_id = $this->org_id();
        
        // menu
        $link_list = $this->list_menu();
        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_link($link_list);

        // SLIDE
        $slide_list = slide::get_slide($org_id, $this->page());
        if (self::__is_test) {
            $slide_list = slide::get_slide($org_id, 'service');
        }
        
        if (count($slide_list) > 0) {
            $element_slide_show = $app->add_element('62hall-slide-show', 'element_slide_show');
            $element_slide_show->set_type('namecard');
            $element_slide_show->set_slides($slide_list);
        }

        $greeting_value = namecard::greeting();

        $greeting = $app->add_div()->add_class('bg-white namecard');
        $greeting_container = $greeting->add_div()->add_class('container margin-30');
        if ($greeting != NULL) {
            $title = carr::get($greeting_value, 'title');
            $type = carr::get($greeting_value, 'content_type');
            $content = carr::get($greeting_value, 'content');
            if (strlen($title) > 0) {
                $greeting_container->add('<h1 class="title text-center bold">' . $title . '</h1>');
            }
            if (strlen($type) > 0 && $type == 'ordered') {
                if ($content != NULL) {
                    $greeting_container->add('<ol>');
                    foreach ($content as $con_val) {
                        $greeting_container->add('<li>' . $con_val . '</li>');
                    }
                    $greeting_container->add('</ol>');
                }
            }
        }

        // PRODUCT NAMECARD
        $namecard = $app->add_div()->add_class('bg-white namecard');
        $namecard_container = $namecard->add_div()->add_class('container margin-30');
        $namecard_container->add('<h1 class="title text-center bold">Produk Kartu Nama Digital</h1><br>');
        $product_namecard = $namecard_container->add_div('page-product-namecard')->add_class('col-md-8 col-md-offset-2');

        $product_namecard->add_listener('ready')
                ->add_handler('reload')
                ->set_target('page-product-namecard')
                ->set_url(curl::base() . "namecard/home/get_page_product_namecard/1");

        $app->add_js("$('input').blur();");

        echo $app->render();
    }

    public function get_start_limit($page) {
        $start = 0;
        if ($page > 1) {
            $start = ($page - 1) * $this->_count_item;
        }

        return $start;
    }

    //</editor-fold>

    public function get_page_product_namecard($page) {
        $app = CApp::instance();
        $org_id = $this->org_id();
        $db = CDatabase::instance();

        $start = $this->get_start_limit($page);

        $arr_namecard = array();
        $q = "
				select
					*
				from
					product_type
				where
					name='namecard'
			";
        
        $r = $db->query($q);
        if ($r->count() > 0) {
            $q_base = "
					select
						*
					from
						product 
					where
						status>0												
						and is_active>0
						and product_type_id=" . $db->escape($r[0]->product_type_id) . "
					order by
										weight asc
									limit
										" . $start . "," . $this->_count_item . "
				";
            $r = $db->query($q_base);
            if ($r->count() > 0) {
                foreach ($r as $row) {
//                    cdbg::var_dump($row);
                    $arr_namecard[] = array(
                        "id" => $row->product_id,
                        "product_url" => $row->url_key,
                        "name" => $row->name,
                        "stock" => $row->stock,
                        "price" => 'Rp. ' . ctransform::format_currency($row->sell_price),
                        "image" => $row->image_name,
                    );
                }
            }
        }
        $total_item = count($arr_namecard);
        $item_product_row = $app->add_div()->add_class('row');
        foreach ($arr_namecard as $data_product) {
            $element_product = $app->add_element('62hall-namecard-product', 'product-namecard-' . $data_product['id']);
            $element_product->set_image($data_product['image'])
                    ->set_product_id($data_product['id'])
                    ->set_product_url($data_product['product_url'])
                    ->set_column(2)
                    ->set_using_class_div_gold(true)
                    ->set_name($data_product['name'])
                    ->set_price_sell($data_product['price']);
        }

        // pagination
        $paginationcontainer = $app->add_div()
                ->add_class('pagination col-md-12');
        $paginationcontainer->add('<center>');

//        $count_page = $this->get_count_page($total_item);

//        if ($count_page > 0) {
//            for ($i = 1; $i <= $count_page; $i++) {
//                $class = NULL;
//                if ($page == $i) {
//                    $class = 'btn-pagination bg-red font-white';
//                }
//                $paginationcontainer->add_action()
//                        ->set_label($i)
//                        ->add_class($class)
//                        ->add_listener('click')
//                        ->add_handler('reload')
//                        ->set_target('page-product-gold')
//                        ->set_url(curl::base() . "namecard/home/get_page_product_namecard/" . $i);
//            }
//        }

        echo $app->render();
    }

}
