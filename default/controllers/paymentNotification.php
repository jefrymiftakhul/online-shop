<?php

/**
 *
 * @author Raymond Sugiarto
 * @since  Oct 21, 2015
 * @license http://piposystem.com Piposystem
 */
class paymentNotification_Controller extends SixtyTwoHallFamilyController {

    public function __construct() {
        parent::__construct();
    }

    public function index($post = array()) {
        $db = CDatabase::instance();
        $request = array_merge($_POST, $_GET, $post);
        $callback_client_log["action"] = "PAYMENT_NOTIFICATION";
        $callback_client_log["request"] = json_encode($request);
        $callback_client_log["reconcile_id"] = carr::get($request, "reconcile_id");
        $callback_client_log["reconcile_datetime"] = carr::get($request, "reconcile_datetime");
        $r = $db->insert("callback_client_log", $callback_client_log);
        $callback_client_log_id = $r->insert_id();

        $err_code = 0;
        $err_message = '';

        $return = array();
        $data = array();

        $req_err_code = carr::get($request, "err_code");
        $req_err_message = carr::get($request, "err_message");
        $req_data = carr::get($request, "data");

        // SUCCESS
        if ($req_err_code == '90000') {
            if ($err_code == 0) {
                $payment_code = carr::get($req_data, "payment_code");
                $q = "
						select
							*
						from
							transaction_payment
						where
							status>0
							and payment_code=" . $db->escape($payment_code) . "
						order by
							transaction_payment_id desc
					";
                $r = $db->query($q);
                if ($r->count() > 0) {
                    $row = $r[0];
                    $transaction_payment_id = $row->transaction_payment_id;
                    $transaction_id = $row->transaction_id;
                    $transaction=cdbg::get_row("select * from transaction where transaction_id=".$db->escape($transaction_id));
                    $org_id=cobj::get($transaction,'org_id');
                    $org_parent_id=cobj::get($transaction,'org_parent_id');
                } else {
                    $err_code++;
                    $err_message = 'Payment code invalid. Payment doesnt exist';
                }
            }

            if ($err_code == 0) {
                $currency_code = carr::get($req_data, "currency_code");
                $bank = carr::get($req_data, "bank");
                $payment_type = carr::get($req_data, "payment_type");
                $account_number = carr::get($req_data, "account_number");
                $bill_key = carr::get($req_data, "bill_key");
                $indomaret_payment_code = carr::get($req_data, "indomaret_payment_code");

                $status_payment = carr::get($req_data, "status_payment");


                $db->begin();
                try {

                    $status_transaction = $status_payment;
                    $status_order = $status_payment;
                    

                    if ($status_payment == 'SUCCESS') {
                        $transaction['date_success'] = date("Y-m-d H:i:s");
                        $transaction_payment['date_success'] = date("Y-m-d H:i:s");
                    }
                    $transaction['transaction_status'] = $status_transaction;
                    $transaction['order_status'] = $status_order;
                    $transaction['payment_status'] = $status_payment;
                    $db->update('transaction', $transaction, array('transaction_id' => $transaction_id));


                    $transaction_payment["currency_code"] = $currency_code;
                    $transaction_payment["bank"] = $bank;
                    $transaction_payment["account_number"] = $account_number;
                    $transaction_payment["bill_key"] = $bill_key;
                    $transaction_payment["indomaret_payment_code"] = $indomaret_payment_code;
                    $transaction_payment["payment_vendor"] = carr::get($req_data, "payment_vendor");
                    $transaction_payment["amount"] = carr::get($req_data, "amount");
                    $transaction_payment["total"] = carr::get($req_data, "total");
                    $transaction_payment["payment_status"] = $status_payment;
                    $transaction_payment["updated"] = date("Y-m-d H:i:s");
                    $transaction_payment["data_notif"] = json_encode($req_data);
                    $db->update("transaction_payment", $transaction_payment, array("transaction_payment_id" => $transaction_payment_id));
                    
                    //insert history
                    $data_transaction_history=array(
                        'org_id' => $org_id,
                        'org_parent_id' => $org_parent_id,
                        'transaction_id' => $transaction_id,
                        'date' => date('Y-m-d H:i:s'),
                        'transaction_status_before'=>'PENDING',
                        'transaction_status'=>$status_transaction,
                        'ref_table'=>'transaction',
                        'ref_table'=>$transaction_id,
                    );
                    if($payment_type=='bank_transfer'){
                        $data_transaction_history['transaction_status_before']='CONFIRMED';
                    }
                    $db->insert('transaction_history',$data_transaction_history);
                    
                    $query ="
                        select
                                *
                        from
                                transaction
                        where
                                transaction_id=".$db->escape($transaction_id)."
                        ";

                    $data_transaction = cdbutils::get_row($query);
                        
                    
                    if ($status_payment == 'SUCCESS') {
                        $q = "
								select
									td.*,
									p.*
								from
									transaction_detail as td
									inner join product as p on p.product_id=td.product_id
								where
									td.status>0
									and td.transaction_id=" . $db->escape($transaction_id) . "
							";
                        $r = $db->query($q);
                        if ($r->count() > 0) {
                            foreach ($r as $row) {
                                if ($row->is_voucher > 0) {
                                    for ($i = 1; $i <= $row->qty; $i++) {
                                        $transaction_voucher = array(
                                            "transaction_id" => $transaction_id,
                                            "vendor_id" => $row->vendor_id,
                                            "product_id" => $row->product_id,
                                        );
                                        $r_tv = $db->insert('transaction_voucher', $transaction_voucher);
                                        $transaction_voucher_id = $r_tv->insert_id();
                                        $voucher_code = generate_code::generate_voucher_code($transaction_voucher_id);
                                        $update_voucher_code = array(
                                            "voucher_code" => $voucher_code,
                                        );
                                        $db->update('transaction_voucher', $update_voucher_code, array("transaction_voucher_id" => $transaction_voucher_id));
                                    }
                                }
                            }
                        }
                    }else{
						stock_process::execute('transaction_failed',$transaction_id);
					}
                } catch (Exception $exc) {
                    $err_code++;
                    $err_message = 'DB Error[0]. ' . $exc->getMessage();
                }

                if ($err_code == 0) {
                    $db->commit();
                } else {
                    $db->rollback();
                }
                
                if($payment_type != 'bank_transfer'){
                    if($status_payment == "SUCCESS"){
                            // send email
                            $this->send_transaction('payment', $data_transaction->booking_code, 'SUCCESS');
                    }
                    else{
                            $this->send_transaction('payment', $data_transaction->booking_code, 'FAILED');
                    }
                }
                
            }
        }
        // FAILED
        else {
            clog::write("ERROR paymentNotification.php. " . json_encode($request));
        }

        $return = array(
            'err_code' => $err_code,
            'err_message' => $err_message,
            'data' => $data,
        );
        // update log
        $db->update("callback_client_log", array("response" => json_encode($return), "update_time" => date("Y-m-d H:i:s")), array("callback_client_log_id" => $callback_client_log_id));

        echo cjson::encode($return);
    }

    public function test() {
        $str = '{"data":{"currency_code":"IDR","payment_code":"2SB6GT4R9D","vendor_code":"bd0065e2-c62b-4176-91bf-5b124f237d70","bill_key":"","indomaret_payment_code":"","payment_date":"2016-01-07 21:22:40","payment_type":"CC","payment_type_name":"credit_card","payment_vendor":"VT","payment_vendor_name":"Veritrans","bank":"mandiri","account_number":"","amount":"600000","charge":"12350","total":"612350","status_payment":"SUCCESS","status_callback":"UNDONE","module":"","module_discount":"0","module_ref_id":"","module_ref_code":"","module_json_data":"","module_status":"UNDONE","module_product_category_code":"","module_product_code":"","items":[{"item_code":"","item_name":"zxc","price":"600000","qty":"1","subtotal":"600000"},{"item_code":"CONVFEE","item_name":"Convenience Fee","price":"12350","qty":"1","subtotal":"12350"}],"is_used":"0","is_settled":"1","is_challenge":"0","status_transaction":""},"err_code":"90000","err_message":"SUCCESS","action":"PAYMENT_GATEWAY","reconcile_id":"dacf2dca1c2249e2274f297a00c7dff9","reconcile_datetime":"2016-01-07 21:23:16"}';
        $arr = json_decode($str);
        $arr = (array) $arr;
        $arr['data'] = (array) $arr['data'];
        $this->index($arr);
    }

}
