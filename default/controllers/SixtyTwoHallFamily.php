<?php

/**
 *
 * @author Raymond Sugiarto
 * @since  Apr 13, 2015
 * @license http://piposystem.com Piposystem
 */
class SixtyTwoHallFamily_Controller extends SixtyTwoHallFamilyController {

    public function index() {
        $app = CApp::instance();

        echo $app->render();
    }

}
