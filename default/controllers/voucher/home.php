<?php

/**
 * Description of home
 *
 * @author Ecko Santoso
 * @since 31 Agu 16
 */
class Home_Controller extends VoucherController
{
    private $product_per_row = 6;
    private $responsive_bootstrap = array(
        'col_lg' => 3,
    );
    private $page = 'voucher';
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $app = CApp::instance();
        $user = $app->user();
        $org_id = $this->org_id();
        $db = CDatabase::instance();

// ===============================================================================================================
        //         menu
        $link_list = $this->list_menu();
        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_link($link_list);
        $voucher_category = $app->add_div()->add_class('green-category-wrapper');
        $q = 'select * from product_category where status > 0 and parent_id is null and product_type_id = 13';
        $data_voucher_category = $db->query($q);

        //    getting full path
        // $current_menu = crouter::routed_uri();
        $current_menu = CFRouter::$controller;
        if ($data_voucher_category != null) {
            $voucher_category->add('<ul class="ul-green-category">');
            foreach ($data_voucher_category as $key => $value) {
                $voucher_category->add('<li>');
                $voucher_category->add('<a href="' . curl::base() . $this->page() . '/product/category/' . $value->url_key . '">');
                $voucher_category->add(strtoupper(clang::__($value->name)));
                $voucher_category->add('</a>');
                $voucher_category->add('</li>');
            }
            $voucher_category->add('</ul>');
        }

        $container = $app->add_div()->add_class('container produk-lokal')->add_div()->add_class('row');
        $col_12 = $container->add_div()->add_class('col-xs-12');
        // Produk voucher Terbaru
        $voucher_product_id = $db->query("select product_id from product where status > 0 and (org_id is null or org_id = ".$db->escape(CF::org_id()).") and status_confirm = 'CONFIRMED' and product_type_id = 13 and (end_bid_date is null or end_bid_date > ".$db->escape(date('Y-m-d H:i:s')).") and url_key is not null order by created desc limit 10");
        $results = array();
        if (count($voucher_product_id) > 0) {
            foreach ($voucher_product_id as $key => $value) {
                $results[] = product::get_product($value->product_id);
            }
        }

        $voucher_terbaru = $col_12->add_div()->add_class('home-deal voucher-terbaru');
        $voucher_terbaru->add_div()->add(
            '<h4 class="deal-title">'
            . '<div class="deal-title-wrapper">'
            . clang::__('VOUCHER TERBARU')
            . '</div>'
            . '</h4>');
        $voucher_terbaru->add_element('product-slider')->add_class('def-slider')
            ->set_slides($results)
            ->set_slide_to_show(4)
            ->set_slide_to_scroll(4)
            ->set_page($this->page);
// Produk Terlaris

        $voucher_product_id = $db->query("select product_id from product where status > 0 and (org_id is null or org_id = ".$db->escape(CF::org_id()).") and status_confirm = 'CONFIRMED' and product_type_id = 13 and (end_bid_date is null or end_bid_date > ".$db->escape(date('Y-m-d H:i:s')).") and url_key is not null order by created desc limit 10");
        $results = array();
        if (count($voucher_product_id) > 0) {
            foreach ($voucher_product_id as $key => $value) {
                $results[] = product::get_product($value->product_id);
            }
        }
        $voucher_terlaris = $col_12->add_div()->add_class('home-deal voucher-terlaris');
        $voucher_terlaris->add_div()->add(
            '<h4 class="deal-title">'
            . '<div class="deal-title-wrapper">'
            . clang::__('VOUCHER TERLARIS')
            . '</div>'
            . '</h4>');
        $voucher_terlaris->add_element('product-slider')->add_class('def-slider')
            ->set_slides($results)
            ->set_slide_to_show(4)
            ->set_slide_to_scroll(4)
            ->set_page($this->page);
        // Produk Termurah
        $voucher_product_id = $db->query("select product_id from product where status > 0 and (org_id is null or org_id = ".$db->escape(CF::org_id()).") and status_confirm = 'CONFIRMED' and product_type_id = 13 and (end_bid_date is null or end_bid_date > ".$db->escape(date('Y-m-d H:i:s')).") and url_key is not null order by sell_price asc limit 10");
        $results = array();
        if (count($voucher_product_id) > 0) {
            foreach ($voucher_product_id as $key => $value) {
                $results[] = product::get_product($value->product_id);
            }
        }
        $voucher_termurah = $col_12->add_div()->add_class('home-deal voucher-termurah');
        $voucher_termurah->add_div()->add(
            '<h4 class="deal-title">'
            . '<div class="deal-title-wrapper">'
            . clang::__('VOUCHER TERMURAH')
            . '</div>'
            . '</h4>');
        $voucher_termurah->add_element('product-slider')->add_class('def-slider')
            ->set_slides($results)
            ->set_slide_to_show(4)
            ->set_slide_to_scroll(4)
            ->set_page($this->page);

        echo $app->render();
    }
    public function category($category)
    {

    }
}
