<?php

defined('SYSPATH') OR die('No direct access allowed.');

class verifyEmail_Controller extends SixtyTwoHallFamilyController {

    private $store = NULL;

    public function __construct() {
        $get = $_GET;
        $api = carr::get($get, 'api');
        parent::__construct(true);
        //$org_id = ccfg::get('org_id'); 
        $org_id = $this->org_id();
        $data_org = org::get_org($org_id);

        if (!empty($data_org)) {
            $this->store = $data_org->name;
        }
    }

    public function index() {
        
    }

    public function confirm($code) {
        $get = $_GET;
        $api = carr::get($get, 'api');
        $param_get = '';
        if ($api != null) {
            $param_get = "&api=1";
        }
        $db = CDatabase::instance();
        $date_now = date('Y-m-d H:i:s');
        $cek_valid = cdbutils::get_row("select * from email_verification where verify_code=" . $db->escape($code));
        if (count($cek_valid) > 0) {
            $data = cdbutils::get_row("select * from email_verification where verify_code=" . $db->escape($code) . " and expired_date >= " . $db->escape($date_now));
            if ($data != null) {
                if ($data->is_verified == 0) {
                    $db->update("email_verification", array("is_verified" => 1), array("email_verification_id" => $data->email_verification_id));
                }
                curl::redirect("verifyEmail/success?email=" . $cek_valid->email . $param_get);
            } else {
                curl::redirect("verifyEmail/expired?email=" . $cek_valid->email . $param_get);
            }
        } else {
            curl::redirect("verifyEmail/code_404");
        }
    }

    public function test() {
        echo verify_email::generate_link(1);
    }

    public function success() {
        $db = CDatabase::instance();
        $request = array_merge($_GET, $_POST);
        $email = carr::get($request, "email");
        $org_id = CF::org_id();
        $data_member = cdbutils::get_row("select * from member where email=" . $db->escape($email) . " and org_id=" . $db->escape($org_id) . " order by member_id desc limit 1");
        if ($data_member != null) {
            $db->update("member", array("is_verified" => 1), array("member_id" => $data_member->member_id));
        }
        $app = CApp::instance();

        if (ccfg::get('no_web_front') == 0) {
            $div_container = $app->add_div()->add_class('container margin-top-30');
            $div_form = $div_container->add_div()->add_class('row');
            $container = $div_form->add_div()->add_class('col-md-12 col-center');

            $row = $container->add_div()->add_class('row');
            $icon = $row->add_div()->add_class('col-md-3');
            $image_send = curl::base() . 'application/ittronmall/default/media/img/62hallfamily/send_message.png';
            $icon->add('<img src="' . $image_send . '" width="180px" />');

            $confirm = $row->add_div()->add_class('col-md-9');

            $confirm->add_div()->add_class('font-size22 bold upper div-verify-email')->add('VERIFIKASI PENDAFTARAN MEMBER <span class="font-red upper">' . $this->store . '</span> SELESAI');
            $confirm->add_div()->add_class('font-size20 div-verify-email')->add('Silahkan klik tombol lanjut untuk proses login dan Anda dapat langsung');
            $confirm->add_div()->add_class('font-size20 div-verify-email')->add('menggunakan layanan kami setelah proses login');
            $confirm->add_br();
            $confirm->add_br();
            $div_action = $confirm->add_div()->add('<center>');

            if (ccfg::get('no_web_front') == 0) {
                $action = $div_action->add_action()
                        ->set_label(clang::__('Lanjut'))
                        ->add_class('btn-62hallfamily bg-red border-3-red font-size20')
                        ->set_link(curl::base());
            }

            $confirm->add_br();
            $confirm->add_br();
        } else {
            $app->add('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">');
            $app->add('<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>');

            $container = $app->add_div()->add_class('container');
            $row = $container->add_div()->add_class('row');
            $icon = $row->add_div()->add_class('col-xs-12 col-sm-12 col-md-3');
            $content = $row->add_div()->add_class('col-xs-12 col-sm-12 col-md-9')->add_div()->add_class('row');

            $image_send = curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/send_message.png';
            $icon->add('<img src="' . $image_send . '" width="180" />');

            $title = $content->add_div()->add_class('col-md-12');
            $message = $content->add_div()->add_class('col-md-12');

            $title->add('VERIFIKASI PENDAFTARAN MEMBER <span style="color:red;">' . strtoupper($this->store) . '</span> SELESAI');
            $message->add('Silahkan login pada aplikasi android anda untuk menggunakan layanan kami.');

            $container->custom_css('display', 'flex');
            $container->custom_css('align-items', 'center');
            $icon->custom_css('margin', '10px 0');
            $icon->custom_css('text-align', 'center');
            $content->custom_css('margin', '10px 0');
            $title->custom_css('font-size', '22px');
            $title->custom_css('font-weight', 'bold');
            $title->custom_css('margin', '10px');
            $message->custom_css('font-size', '18px');
            $message->custom_css('margin', '10px');
        }

        echo $app->render();
    }

    public function expired() {
        $db = CDatabase::instance();
        $request = array_merge($_GET, $_POST);
        $email = carr::get($request, "email");
        $data_member = cdbutils::get_row("select * from member where status = 1 and email=" . $db->escape($email));
        if (count($data_member) > 0) {
            $new_url = verify_email::generate_link($data_member->member_id);
            $message = "Konfirmasi email anda (" . $email . ") telah expired. <a href='" . $new_url . "'>Resend email confirmation</a>";
            $this->page_error($message);
        } else {
            curl::redirect("verifyEmail/code_404");
        }
    }

    public function code_404() {
        $this->page_error("Kode Konfirmasi anda tidak terdaftar!!!!");
    }

    public function page_error($message) {
        $app = CApp::instance();

        $div = $app->add_div()->add_class('container');
        $div->add('<h1 class="text-danger">Opps!!</h1>');
        $alert = $div->add_div()->add_class('alert alert-danger');
        $alert->add($message);

        echo $app->render();
    }

}
