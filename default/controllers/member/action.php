<?php

/**
 * Description of action
 *
 * @author Ecko Santoso
 * @since 27 Okt 16
 */
class Action_Controller extends SixtyTwoHallFamilyController {
    public function __construct() {
        parent::__construct();
    }
    
    public function login(){
        $app = CApp::instance();
        
        if (member::get_data_member() != false) {
            curl::redirect('home');
        }
        
        $container = $app->add_div()->add_class('container');
        $col_12 = $container->add_div()->add_class('col-md-12 col-sm-12 col-xs-12');
        $col_12->add('<h2>'.clang::__('Login').'</h2>');
        $element_login = $col_12->add_element('62hall-login');
        
        echo $app->render();
    }
    
    public function register(){
        $app = CApp::instance();
        $org_id = CF::org_id();
        $data_org = org::get_org($org_id);
        $store = '';
        $have_product = 0;
        $have_gold = 0;
        $have_service = 0;
        $have_register = 0;
        $have_deposit = 0;
        
        if (!empty($data_org)) {
            $store = $data_org->name;
            $have_product = $data_org->have_product;
            $have_gold = $data_org->have_gold;
            $have_service = $data_org->have_service;
            $have_register = $data_org->have_register;
            $have_deposit = $data_org->have_deposit;
        }
        
        if (member::get_data_member() != false) {
            curl::redirect('home');
        }
        
        $container = $app->add_div()->add_class('container');
        $col_12 = $container->add_div()->add_class('col-md-12 col-sm-12 col-xs-12');
        $col_12->add('<h2>'.clang::__('Register').'</h2>');
        $element_register = $col_12->add_element('62hall-register');
        $element_register->set_force_html(TRUE);
        $element_register->set_icon(FALSE);
        $element_register->set_store($store);
        $element_register->set_have_gold($have_gold);
        $element_register->set_have_service($have_service);
        
        echo $app->render();
    }
    
    public function status_order() {
        $app = CApp::instance();
        
        $container = $app->add_div()->add_class('container');
        $col_12 = $container->add_div()->add_class('col-md-12 col-sm-12 col-xs-12');
        $col_12->add('<h2>'.clang::__('Check Order Status').'</h2>');
        
        $form = $col_12->add_form()->set_action(curl::base() . 'retrieve/invoice');
        
        $form->add_field()->set_label(clang::__('Order Number'))
                ->add_control('', 'text')
                ->set_name('nomor-pesanan');
        $form->add_field()->set_label(clang::__('Email'))
                ->add_control('', 'text')
                ->set_name('email');
        
        $form->add_field()->add_action('submit_button')->set_label(clang::__('View'))->add_class('btn btn-62hallfamily bg-red font-white border-3-red pull-right upper')->set_submit(true);
        
        echo $app->render();
    }
}
