<?php

/**
 *
 * @author Riza 
 * @since  Nov 27, 2015
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class Confirm_Controller extends SixtyTwoHallFamilyController {

    public $store = NULL;

    public function __construct() {
        parent::__construct();
        $org_id = ccfg::get('org_id');
        $data_org = org::get_org($org_id);

        if (!empty($data_org)) {
            $this->store = $data_org->name;
        }
    }

    public function index() {
        $app = CApp::instance();
        $session = Session::instance();
        //$org_id = ccfg::get('org_id');
        $org_id = $this->org_id();

        // email
        $register_id = $session->get('register_id');
        ;
        $data_email = member::get_email_member($register_id);
        $email = isset($data_email->email) ? $data_email->email : NULL;

        // SEMBUNYIKAN DAFTAR/ MASUK
        $app->add_js("
                jQuery('.usermenu').css('display', 'none');
                    ");

        $container = $app->add_div()->add_class('container margin-top-30');
        $row = $container->add_div()->add_class('row');

        $icon = $row->add_div()->add_class('col-md-3');
        $image_send = curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/send_message.png';
        $icon->add('<img src="' . $image_send . '" width="180px" />');

        $confirm = $row->add_div()->add_class('col-md-9');

        $confirm->add_div()->add_class('upper bold font-size24')->add('TERIMA KASIH BPK/IBU TELAH MENDAFTAR DI <span class="font-red upper">' . $this->store . '</span>');
        $confirm->add_div()->add_class('font-size20')->add('Segera konfirmasi email Bpk/Ibu ' . $email);
        $link_konfirm = $confirm->add_action()
                ->set_label(clang::__('<u>Kirim ulang link konfirmasi</u>'))
                ->add_class('font-size20 font-red margin-left-15-')
                ->set_link(curl::base() . 'member/confirm/recend_confirm');
        $confirm->add_br();
        $confirm->add_br();

        $backtohome = $container->add_div()->add_class('col-md-2 col-md-offset-5');
        $button = $backtohome->add_action()
                ->set_label(clang::__('kembali ke home'))
                ->add_class('btn-62hallfamily bg-red border-3-red upper font-size20')
                ->set_link(curl::base());

        $backtohome->add_br();
        $backtohome->add_br();

        echo $app->render();
    }

    public function recend_confirm() {
        $app = CApp::instance();
        $session = Session::instance();
        $org_id = $this->org_id();
        $db = CDatabase::instance();

        // SEMBUNYIKAN DAFTAR/ MASUK
        $app->add_js("
                jQuery('.usermenu').css('display', 'none');
                    ");

        $email_get = carr::get($_GET, 'email', NULL);

        // email
        $register_id = $session->get('register_id');
        $data_email = member::get_email_member($register_id);
        $email = isset($data_email->email) ? $data_email->email : $email_get;

        if (empty($register_id)) {
            $register_id = cdbutils::get_value("select member_id from member where status > 0 and org_id=" . $db->escape($org_id) . " and email=" . $db->escape($email));
        }

        $this->send_verification($register_id);

        $container = $app->add_div()->add_class('container margin-top-30');
        $row = $container->add_div()->add_class('row');

        $icon = $row->add_div()->add_class('col-md-3');
        $image_send = curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/send_message.png';
        $icon->add('<img src="' . $image_send . '" width="180px" />');

        $confirm = $row->add_div()->add_class('col-md-9');

        $confirm->add_div()->add_class('upper bold font-size24')->add('LINK KONFIRMASI TELAH DIKIRIMKAN KEMBALI KE EMAIL BPK/IBU.');
        $confirm->add_div()->add_class('font-size20')->add('Segera konfirmasi email Anda ' . $email);
        $confirm->add_br();

        $backtohome = $container->add_div()->add_class('col-md-2 col-md-offset-5');
        $button = $backtohome->add_action()
                ->set_label(clang::__('kembali ke home'))
                ->add_class('btn-62hallfamily bg-red border-3-red upper font-size20')
                ->set_link(curl::base());

        $backtohome->add_br();
        $backtohome->add_br();

        echo $app->render();
    }

}
