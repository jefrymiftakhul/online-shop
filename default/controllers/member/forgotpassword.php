<?php

/**
 *
 * @author Riza 
 * @since  Des 18, 2015
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class ForgotPassword_Controller extends SixtyTwoHallFamilyController {

    private $store = NULL;

    public function __construct() {
        parent::__construct(true);
    }

    public function index() {
        $app = CApp::instance();
        $db = CDatabase::instance();

        $err_code = 0;
        $err_message = "";

        $email = '';

        $post = $this->input->post();
        $get = $this->input->get();
        
        $request = array_merge($post, $get);
        $email = carr::get($request, 'email');

        if ($email != NULL) {
             $data_member = cdbutils::get_row("SELECT * FROM member WHERE email = " . $db->escape($email) . " AND status > 0 AND is_verified = 1 AND org_id = ".$db->escape(CF::org_id()));
             if ($data_member != NULL) {

                 $member_id = $data_member->member_id;
                 $name = $data_member->name;
                 $token = $this->__generate_token($data_member->email);
                 $tommorow_time = strtotime(date('Y-m-d H:i:s') . "+1 days");
                 $expired_date = date('Y-m-d H:i:s', $tommorow_time);

                 $content = '
                                 <b style="font-size:22px">Hy ' . $data_member->name . ',</b>
                                 <div style="margin-top:30px;font-size:18px;">
                                 Siap mengganti passsword anda?
                                 <br><br>
                                 <a href="' . curl::httpbase() . 'member/forgotpassword/change/' . $token . '" style="font-size:18px;margin-top:20px;background:#df3f49;padding:10px 20px;color:white;text-decoration:none;">
                                     Ganti Sekarang
                                 </a>
                                 </div>
                                 <div style="margin-top:20px;font-size:18px">
                                     Anda mempunyai 24 jam untuk mengganti password Anda.<br>
                                     Setelah lewat dari jangka waktu, Anda dapat meminta link yang baru.
                                 </div>
                                 ';

                 try {
                     $this->send_email($email, array(
                         'logo' => curl::httpbase() . 'application/adminittronmall/' . $this->get_org_code() . '/upload/logo/item_image/' . $this->data_org()->item_image,
                         'subject' => $this->store() . ' - Forgot Password',
                         'content' => $content,
                             )
                     );
                 } catch (Exception $e) {
                     $err_code++;
                     $err_message = $e->getMessage();
                 }

                 if ($err_code == 0) {
                     $data_request_forgot_password = array(
                         'member_id' => $member_id,
                         'token' => $token,
                         'expired_date' => $expired_date,
                         'created' => date('Y-m-d H:i:s'),
                         'createdby' => 'system',
                         'updated' => date('Y-m-d H:i:s'),
                         'updateby' => 'system',
                         'status' => 1,
                     );

                     $insert = $db->insert('request_forgot_password', $data_request_forgot_password);
                 }
             } else {
                 $err_code++;
                 $err_message = "Email tidak ditemukan!";
             }
//            try {
//                email::send('forgot',$email);
//            } catch (Exception $e) {
//                $err_code++;
//                $err_message = $e->getMessage();
//            }
        } else {
            $err_code++;
            $err_message = "Email tidak boleh kosong";
        }

        $return = array(
            'error' => $err_code,
            'message' => $err_message,
            'email' => $email,
        );

        $get_type = carr::get($get, 'get_type');
        if ($get_type == 'jsonp') {
            $json = json_encode($return);
            $callback = carr::get($_GET,'callback');
            if(strlen($callback)>0) {
                echo $callback . '(' . $json . ')';
                return;
            }
        }

        echo cjson::encode($return);
    }

    public function change($token) {
        $db = CDatabase::instance();
        $session = Session::instance();

        $data_request_password = cdbutils::get_row("SELECT * FROM request_forgot_password WHERE token = " . $db->escape($token) . " AND status > 0");
        $data_member = cdbutils::get_row("SELECT * FROM member WHERE member_id = " . $db->escape($data_request_password->member_id) . " AND status > 0 AND is_verified = 1");

        $err_code = 0;
        $err_message = "";
        $db->begin();
        if ($data_request_password) {

            if (strtotime(date('Y-m-d H:i:s')) < $data_request_password->expired_date) {
                $err_code++;
                $err_message = "Masa berlaku token Anda telah habis. Lakukan reset password kembali!";
                $session->set('err_forgot_password', $err_message);
            }

            if ($data_request_password->is_done > 0) {
                $err_code++;
                $err_message = "Link sudah pernah digunakan";
                $session->set('err_forgot_password', $err_message);
            }

            if (empty($data_member)) {
                $err_code++;
                $err_message = "Data member tidak ditemukan";
                $session->set('err_forgot_password', $err_message);
            }

            if ($err_code == 0) {
                $member_id = $data_member->member_id;
                $name = $data_member->name;
                $email = $data_member->email;
                $password = $this->__generate_password();

                $content = '
                                    <b style="font-size:22px">Hy ' . $name . ',</b>
                                    <div style="margin-top:40px;">
                                    <p>Password baru Anda.</p>
                                    <table>
                                        <tr>
                                            <td>Member Email</td>
                                            <td> : </td>
                                            <td>' . $email . '</td>
                                        </tr>
                                        <tr>
                                            <td>Password Baru</td>
                                            <td> : </td>
                                            <td>' . $password . '</td>
                                        </tr>
                                    </table>
                                    </div>
                                    ';

                try {
                    $this->send_email($email, array(
                        'logo' => curl::httpbase() . 'application/adminittronmall/' . $this->get_org_code() . '/upload/logo/item_image/' . $this->data_org()->item_image,
                        'subject' => $this->store() . ' - New Password',
                        'content' => $content,
                            )
                    );
                } catch (Exception $e) {
                    $err_code++;
                    $err_message = $e->getMessage();
                }

                $data = array(
                    'password' => md5($password)
                );

                $update = $db->update("member", $data, array('member_id' => $member_id));
                $update_users = $db->update('users', $data, array('member_id' => $member_id));
                $update_request_forgot_pass = $db->update("request_forgot_password", array('is_done' => 1), array('request_forgot_password_id' => $data_request_password->request_forgot_password_id));
            }
        } else {
            $err_code++;
            $err_message = "Token tidak ditemukan!";
            $session->set('err_forgot_password', $err_message);
        }

        if ($err_code > 0) {
            $db->rollback();
            curl::redirect(curl::base() . 'member/forgotpassword/fail');
        } else {
            $db->commit();
            curl::redirect(curl::base() . 'member/forgotpassword/success');
        }
    }

    public function success() {
        $app = CApp::instance();

        $container = $app->add_div()->add_class('container');
        $row = $container->add_div()->add_class('row');
        $success_container = $row->add_div()->add_class('col-md-12 margin-top-30');

        $icon = $success_container->add_div()->add_class('col-md-3');
        $image_send = curl::base() . 'application/ittronmall/default/media/img/62hallfamily/send_message.png';
        $icon->add('<img src="' . $image_send . '" width="180px" />');

        $right_form = $success_container->add_div()->add_class('col-md-9');
        $message = $right_form->add_div()->add_class('font-size24 bold')
                ->add('UBAH PASSWORD MEMBER BAPAK / IBU <span class="font-red upper">' . $this->store() . '</span> BERHASIL.');
        $right_form->add_div()->add_class('font-size20 font-red')
                ->add('Periksa kembali email Bpk/Ibu untuk melihat password baru.');
        if(!ccfg::get('compromall_system')){
            $right_form->add_div()->add_class('font-size20')
                    ->add('Silahkan klik tombol Masuk dan Bpk/Ibu dapat langsung menggunakan layanan kami setelah proses login');
        }
        $right_form->add_br();
        $div_action = $right_form->add_div()->add('<center>');
        
        if(!ccfg::get('compromall_system')){
            if(!ccfg::get('no_web_front')) {
                $action = $div_action->add_action()
                        ->set_label(clang::__('Kembali ke Home'))
                        ->add_class('btn-reset-password btn-62hallfamily bg-red border-3-red font-size20 upper')
                        ->set_link(curl::base() . 'home');
            }
        }


        $right_form->add_br();

        echo $app->render();
    }

    public function fail() {
        $app = CApp::instance();
        $session = Session::instance();
        $err_message = $session->get('err_forgot_password');

        $container = $app->add_div()->add_class('container');
        $row = $container->add_div()->add_class('row');
        $error_container = $row->add_div()->add_class('col-md-12 margin-top-30');

        $icon = $error_container->add_div()->add_class('col-md-3');
        $image_send = curl::base() . 'application/ittronmall/default/media/img/62hallfamily/send_message.png';
        $icon->add('<img src="' . $image_send . '" width="180px" />');

        $right_form = $error_container->add_div()->add_class('col-md-9');
        $message = $right_form->add_div()->add_class('font-size24 bold')
                ->add('GANTI PASSWORD MEMBER BAPAK / IBU <span class="font-red upper">' . $this->store() . '</span> GAGAL.');
        $right_form->add_div()->add_class('font-size20 font-red normal')
                ->add($err_message);
        $right_form->add_div()->add_class('font-size20 normal')
                ->add('Silahkan ulangi kembali reset password Bpk/Ibu.');
        $right_form->add_br();
        $div_action = $right_form->add_div()->add('<center>');

        if(!ccfg::get('compromall_system')){
            $action = $div_action->add_action()
                    ->set_label(clang::__('Kembali ke Home'))
                    ->add_class('btn-62hallfamily bg-red border-3-red font-size20 upper')
                    ->set_link(curl::base() . 'home');
        }
        $right_form->add_br();

        echo $app->render();
    }

    public function email_format($base_class) {
        $classname = ucfirst($base_class) . "Email";
        $file = CF::get_files('libraries', "FormatEmail/" . $classname);
        include_once $file[0];
        $tbclass = new $classname();
        return $tbclass;
    }

    private function send_email($recipient, $data) {
        $tbclass = $this->email_format("forgetpassword");
        $format = $tbclass::format($recipient, $data);
        return cmail::send_smtp($recipient, $format['subject'], $format['message']);
    }

    private function __generate_password() {
        $str = str_split("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
        shuffle($str);
        $salt = '';
        foreach (array_rand($str, 8) as $key) {
            $salt .= $str[$key];
        }
        return $salt;
    }

    private function __generate_token($email = NULL) {
        $data = $email . date('Y-m-d H:i:s');

        return md5($data);
    }

}
