<?php

/**
 *
 * @author Riza 
 * @since  Des 18, 2015
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class Account_Controller extends ProductController {

    private $page = 'product';
    private $store = NULL;
    private $type_payment = "payment";
    private $type_shipping = "shipping";
    private $display_page = 6;

    public function __construct() {
        parent::__construct();
        // $org_id = ccfg::get('org_id');
        $org_id = $this->org_id();
        $data_org = org::get_org($org_id);

        if (!empty($data_org)) {
            $this->store = $data_org->name;
        }
    }

    public function index() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();

        $org_id = $this->org_id();
        $menu_list = $this->menu_list();
        $link_list = $this->list_menu();

        $member_id = $session->get('member_id');
        $name = $session->get('name');

        $request = array_merge($_GET, $_POST);
        $file = $_FILES;
        $act = carr::get($request, 'act');
        $page = 'product';
        $page_account = 'info';
        $message = '';
        if (isset($request['page_account'])) {
            $page_account = $request['page_account'];
        }

        $ip = crequest::remote_address();
        $date = date("Y-m-d H:i:s");
        $data_email = member::get_email_member($member_id);
        $email = cobj::get($data_email, 'email');

        $err_code = 0;
        $message = NULL;

        if ($act) {
            $name = carr::get($request, 'name', NULL);
            $date_birth = carr::get($request, 'date_birth', NULL);
            $month_birth = carr::get($request, 'month_birth', NULL);
            $year_birth = carr::get($request, 'year_birth', NULL);
            $email = carr::get($request, 'mail', NULL);
            $phone = carr::get($request, 'phone', NULL);
            $gender = carr::get($request, 'gender', NULL);

            $name_validate = array('name', 'mail', 'phone', 'gender');
            foreach ($name_validate as $validate) {
                if (strlen(carr::get($request, $validate)) == 0) {
                    $err_code++;

                    if ($validate == 'name') {
                        $validate = 'Nama Lengkap';
                    }
                    if ($validate == 'mail') {
                        $validate = 'Email';
                    }
                    if ($validate == 'phone') {
                        $validate = 'No. Handphone';
                    }
                    if ($validate == 'gender') {
                        $validate = 'Jenis Kelamin';
                    }

                    $message = $validate . ' belum diisi!';
                }
            }

            if (strlen(str_replace(' ', '', carr::get($request, 'name'))) == 0) {
                $err_code++;
                $message = 'Nama Lengkap tidak boleh kosong';
            }

            if (strlen($email) > 0) {
                if (!cvalid::email($email)) {
                    $err_code++;
                    $message = 'Format Email salah';
                }
            }

            // cek tanggal
            if ($err_code == 0) {
                // validate date of birth
                $max_date = cutils::day_count($month_birth, $year_birth);

                if ($date_birth > $max_date) {
                    $err_code++;
                    $message = clang::__('Format Tanggal Lahir salah');
                }
            }

            $file_name_generated = NULL;
            if (!empty($_FILES['image_name']['tmp_name'])) {
                $resource = CResources::factory("image", "profile");
                $resource->add_size('view_profile', array(
                    'width' => '150',
                    'height' => '150',
                    'proportional' => true,
                    'crop' => true,
                ));
                $resource->add_size('view_thumbnail', array(
                    'width' => '30',
                    'height' => '30',
                    'proportional' => true,
                    'crop' => true,
                ));
                $filename = $_FILES['image_name']['name'];
                $path = file_get_contents($_FILES['image_name']['tmp_name']);
                $file_name_generated = $resource->save($filename, $path);
                $image_url = $resource->get_url($file_name_generated);
            }

            if ($err_code == 0) {
                $db->begin();
                try {
                    if ($err_code == 0) {
                        $data_member = array(
                            'name' => $name,
                            'date_of_birth' => $year_birth . '-' . $month_birth . '-' . $date_birth,
                            'email' => $email,
                            'phone' => $phone,
                            'gender' => $gender,
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => $email,
                            'updatedip' => crequest::remote_address(),
                        );
                        if (!empty($_FILES['image_name']['tmp_name'])) {
                            $data_member = array(
                                'image_name' => $file_name_generated,
                            );
                        }
                        $db->update('member', $data_member, array('member_id' => $member_id));

                        $message = "Penyimpan Berhasil";
                    }
                } catch (Exception $ex) {
                    $err_code++;
                    $message = clang::__('Error, Please call administrator!');
                    clog::write('DB Error page auth/register. ' . $ex->getMessage());
                    $db->rollback();
                }
            }
            if ($err_code == 0) {
                $db->commit();
            } else {
                $db->rollback();
            }
        }

        $session->set('error_change_account', $err_code);
        $session->set('message_change_account', $message);

        if (!empty($member_id)) {

            // SEARCH 
//            $app->add_listener('ready')
//                    ->add_handler('reload')
//                    ->set_target('search')
//                    ->set_url(curl::base() . "home/search");

            // SHOPPING CART 
            $app->add_listener('ready')
                    ->add_handler('reload')
                    ->set_target('shopping-cart')
                    ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

            // GET CATEGORY
            $product_category = product_category::get_product_category_menu($page);
            // FILE LOCATION
            $query = 'SELECT image_name
                            FROM member 
                            WHERE status > 0 AND member_id = ' . $db->escape($member_id);
            $get_image = cdbutils::get_value($query);

            if (empty($get_image)) {
                $image = curl::base() . 'application/lapakbangunan/default/media/img/imbuilding/user_gray_150x150.png';
            } else {
                $image = image::get_image_url_front($get_image, 'view_profile');
            }

            //start
            $container = $app->add_div()->add_class('container container-account');
            $row = $container->add_div()->add_class('row');
            $left = $row->add_div()->add_class('col-md-4 float-left');
            $right = $row->add_div()->add_class('col-md-8 float-left width-med750-account');

            $left->add_div()->add_class('account-container-menu-border');
            $container_menu = $left->add_div()->add_class('account-container-menu');
            $container_member = $container_menu->add_div()->add_class('account-container-member');
            $container_member->add_div()->add_class('member-photo')->add_div()->add("<img src='".$image."'>");
            $member_info = $container_member->add_div()->add_class('member-info')->add_div()->add_class('account-member-info');
            $member_info->add_div()->add_class('txt-member-info')->add($name);
            $member_info->add_div()->add_class('txt-member-email')->add($email);

            $member_info->add_div()->add_action()
                    ->set_label(clang::__("Keluar"))
                    ->add_class('btn-imlasvegas-secondary btn-logout')
                    ->set_link(curl::base() . 'member/account/logout');

            $container_menu_xs = $container_menu->add_div()->add_class('container-menu-xs visible-xs');
            $container_menu_xs->add_div()->add_class('menu-xs-selected hidden-xs')->add(clang::__('Account Page'));
            $container_menu_xs->add_div()->add_class('menu-xs-icon hidden-xs')->add('+');
            $menu = $container_menu->add_div()->add_class('member-menu');
            $menu->add('<ul>');

            //ACCOUNT PAGE
            $menu->add('    <li>');
            $content_menu = $menu->add_div()->add_class('content-menu');
            $content_menu->add_action('info')
                    ->add_class('menu-member')
                    ->set_label(clang::__('Account Page'))
                    ->add_listener('click')
                    ->add_handler('reload')
                    ->set_target('page-account-container')
                    ->set_url(curl::base() . 'member/account/info/');
            $menu->add('    </li>');

            //CHANGE ACCOUNT
            $menu->add('    <li>');
            $content_menu = $menu->add_div()->add_class('content-menu');
            $content_menu->add_action('change')
                    ->add_class('menu-member')
                    ->set_label(clang::__('Change Account'))
                    ->add_listener('click')
                    ->add_handler('reload')
                    ->set_target('page-account-container')
                    ->set_url(curl::base() . 'member/account/change/');
            $menu->add('    </li>');

            //CHANGE PASSWORD
            $menu->add('    <li>');
            $content_menu = $menu->add_div()->add_class('content-menu');
            $content_menu->add_action('change_password')
                    ->add_class('menu-member')
                    ->set_label(clang::__('Change Password'))
                    ->add_listener('click')
                    ->add_handler('reload')
                    ->set_target('page-account-container')
                    ->set_url(curl::base() . 'member/account/change_password/');
            $menu->add('    </li>');
            
            //ADDRESS BOOK
            $menu->add('    <li>');
            $content_menu = $menu->add_div()->add_class('content-menu');
            $content_menu->add_action('address_book')
                    ->add_class('menu-member')
                    ->set_label(clang::__('Address Book'))
                    ->add_listener('click')
                    ->add_handler('reload')
                    ->set_target('page-account-container')
                    ->set_url(curl::base() . 'member/account/address_book/');
            $menu->add('    </li>');

            //MY ORDER
            $menu->add('    <li>');
            $content_menu = $menu->add_div()->add_class('content-menu');
            $content_menu->add_action('order_me')
                    ->add_class('menu-member')
                    ->set_label(clang::__('My Order'))
                    ->add_listener('click')
                    ->add_handler('reload')
                    ->set_target('page-account-container')
                    ->set_url(curl::base() . 'member/account/order_me/');
            $menu->add('    </li>');

            //MY ORDER
            $menu->add('    <li>');
            $content_menu = $menu->add_div()->add_class('content-menu');
            $content_menu->add_action('review')
                    ->add_class('menu-member')
                    ->set_label(clang::__('Review'))
                    ->add_listener('click')
                    ->add_handler('reload')
                    ->set_target('page-account-container')
                    ->set_url(curl::base() . 'member/account/review/');
            $menu->add('    </li>');

            //MEMBER PROMO
            // $menu->add('    <li>');
            // $content_menu = $menu->add_div()->add_class('content-menu');
            // $content_menu->add_action('member_promo')
            //         ->add_class('menu-member')
            //         ->set_label(clang::__('Member Promo'))
            //         ->add_listener('click')
            //         ->add_handler('reload')
            //         ->set_target('page-account-container')
            //         ->set_url(curl::base() . 'member/account/member_promo/');
            // $menu->add('    </li>');

            //SEND MESSAGE
            $menu->add('    <li>');
            $content_menu = $menu->add_div()->add_class('content-menu');
            $content_menu->add_action('send_message')
                    ->add_class('menu-member')
                    ->set_label(clang::__('Send Message'))
                    ->add_listener('click')
                    ->add_handler('reload')
                    ->set_target('page-account-container')
                    ->set_url(curl::base() . 'member/account/send_message/');
            $menu->add('    </li>');

            //INBOX
            $menu->add('    <li>');
            $content_menu = $menu->add_div()->add_class('content-menu');
            $content_menu->add_action('inbox')
                    ->add_class('menu-member')
                    ->set_label(clang::__('Inbox'))
                    ->add_listener('click')
                    ->add_handler('reload')
                    ->set_target('page-account-container')
                    ->set_url(curl::base() . 'member/account/inbox/');
            $menu->add('    </li>');

            //MODUL
            $menu->add('    <li>');
            $content_menu = $menu->add_div()->add_class('content-menu');
            $content_menu->add_action('manual_book')
                    ->add_class('menu-member')
                    ->set_label(clang::__('Manual Book'))
                    ->add_listener('click')
                    ->add_handler('reload')
                    ->set_target('page-account-container')
                    ->set_url(curl::base() . 'member/account/manual_book/');
            $menu->add('    </li>');

            $menu->add('</ul>');


            $right->add_div('page-account-container');

            $id = carr::get($request, 'id');
            $txt_param = '';
            if (strlen($id) > 0) {
                $txt_param = "?id=".$id;
            }            
            // READY GET HALAMAN AKUN
            $right->add_listener('ready')
                    ->add_handler('reload')
                    ->set_target('page-account-container')
                    ->set_url(curl::base() . 'member/account/' . $page_account.$txt_param);

            //end

            $apage_account = $page_account;
            if ($page_account == 'reply_inbox') {
                $apage_account = 'inbox';
            }
            $js = ("
                set_active('" . $apage_account . "');
                
                function set_active(menu){
                    jQuery('.menu-member').parents('li').removeClass('active');
                    jQuery('#'+menu).parents('li').addClass('active');
                    jQuery('.menu-xs-selected').text(jQuery('#'+menu).text());
                    jQuery('.member-menu').removeClass('active');
                }
                
                jQuery('.menu-member').click(function(){
                    id = $(this).attr('id');
                    set_active(id);
                });

                jQuery('.menu-xs-selected').click(function() {
                    jQuery('.member-menu').toggleClass('active ');
                });

            ");

            $app->add_js($js);
        } else {
            curl::redirect(curl::base());
        }

        echo $app->render();
    }

    public function register_downline() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();
        $member_id = $session->get('member_id');
        $ip = crequest::remote_address();
        $request = $_POST;
        $error = 0;
        $err_message = '';
        $org_id=CF::org_id();
        if ($request != null) {
            if ($error == 0) {
                try {
                    $balance_out=800000;
                    $commision=300000;
                    $db->begin();
                    $q="
                        select
                            mb.balance_idr
                        from
                            member as m
                            left join member_balance as mb on mb.member_id=m.member_id
                        where
                            m.member_id=".$db->escape($member_id)."
                    ";
                    $member=cdbutils::get_row($q);
                    if($member){
                        $balance=$member->balance_idr;
                        if($balance<$balance_out){
                            $error++;
                            $err_message="Balance not Enough";
                        }
                    }else{
                        $error++;
                        $err_message='Data Member not Found';
                    }
                    $name = carr::get($request, 'name', NULL);
                    $email = carr::get($request, 'email', NULL);
                    $password = carr::get($request, 'password', NULL);
                    $confirm_password = carr::get($request, 'confirm_password', NULL);
                    $phone = carr::get($request, 'phone', NULL);
                    $gender = carr::get($request, 'gender', NULL);
                    $date_birth = carr::get($request, 'date_birth', NULL);
                    $month_birth = carr::get($request, 'month_birth', NULL);
                    $year_birth = carr::get($request, 'year_birth', NULL);
                    $is_subscribe = carr::get($request, 'is_subscribe', 0);
                    $is_subscribe_gold = carr::get($request, 'is_subscribe_gold', 0);
                    $is_subscribe_service = carr::get($request, 'is_subscribe_service', 0);

                    $name_validate = array('confirm_password', 'password', 'name', 'email', 'phone', 'gender');
                    foreach ($name_validate as $validate_k => $validate_v) {
                        if (strlen(carr::get($request, $validate_v)) == 0) {
                            $error++;
                            if ($validate_v == 'name') {
                                $validate_v = 'Nama Lengkap';
                            }
                            if ($validate_v == 'confirm_password') {
                                $validate_v = 'Ulangi Password';
                            }
                            if ($validate_v == 'password') {
                                $validate_v = 'Password';
                            }
                            if ($validate_v == 'email') {
                                $validate_v = 'Email';
                            }
                            if ($validate_v == 'phone') {
                                $validate_v = 'No. Handphone';
                            }
                            if ($validate_v == 'gender') {
                                $validate_v = 'Jenis Kelamin';
                            }
                            $err_message = $validate_v . ' belum diisi!';
                        }
                    }

                    if (strlen($password) < 6) {
                        $error++;
                        $err_message = 'Password minimal 6 digit';
                    }

                    if (strlen($email) > 0) {
                        if (!cvalid::email($email)) {
                            $error++;
                            $err_message = 'Format email salah';
                        }
                    }

                    // cek tanggal
                    if ($error == 0) {
                        // validate date of birth
                        $max_date = cutils::day_count($month_birth, $year_birth);

                        if ($date_birth > $max_date) {
                            $error++;
                            $err_message = clang::__('Tanggal Lahir salah');
                        }
                    }

                    if ($error == 0) {
                        // validate user already used
                        $q = "SELECT count(*) as total 
                                FROM member
                                WHERE email = " . $db->escape($email) . " 
                                and org_id = " . $db->escape($org_id) . "
                                and status > 0
                                ";
                        $total = cdbutils::get_value($q);
                        if (strlen($total) > 0 && $total > 0) {
                            $error++;
                            $err_message = clang::__('Email sudah pernah digunakan');
                        }
                    }

                    if ($error == 0) {
                        if ($confirm_password != $password) {
                            $error++;
                            $err_message = clang::__('Password tidak sesuai dengan konfirmasi password');
                        }
                    }
                    
                    if($error==0){
                        $parent_id=$member_id;
                        $data_register = array(
                            'org_id' => $org_id,
                            'parent_id' => $parent_id,
                            'name' => $name,
                            'phone' => $phone,
                            'email' => $email,
                            'gender' => $gender,
                            'date_of_birth' => $year_birth . '-' . $month_birth . '-' . $date_birth,
                            'is_subscribe' => $is_subscribe,
                            'is_subscribe_gold' => $is_subscribe_gold,
                            'is_subscribe_service' => $is_subscribe_service,
                            'password' => md5($password),
                            'created' => date('Y-m-d H:i:s'),
                            'createdby' => $email,
                            'createdip' => $ip
                        );
                        $tree = CTreeDB::factory('member');
                        $tree->set_org_id($org_id);
                        $new_member_id=$tree->insert($data_register, $parent_id);
                        
                        // subscribe
                        if ($is_subscribe) {
                            $data_subscribe = array(
                                'member_id' => $new_member_id,
                                'email_subscribe' => $email
                            );
                            $result = subscribe::save('product', $data_subscribe);
                            $err_code += $result['err_code'];
                            $message = $result['message'];
                        }

                        if ($is_subscribe_gold) {
                            $data_subscribe = array(
                                'member_id' => $new_member_id,
                                'email_subscribe' => $email
                            );
                            $result = subscribe::save('gold', $data_subscribe);
                            $err_code += $result['err_code'];
                            $message = $result['message'];
                        }

                        if ($is_subscribe_service) {
                            $data_subscribe = array(
                                'member_id' => $new_member_id,
                                'email_subscribe' => $email
                            );
                            $result = subscribe::save('service', $data_subscribe);
                            $err_code += $result['err_code'];
                            $message = $result['message'];
                        }
                        
                        
                        
                        //cut balance
                        $saldo=$balance-$balance_out;
                        $data_balance=array();
                        $data_balance['balance_idr']=$saldo;
                        $db->update('member_balance',$data_balance,array('org_id'=>$org_id,'member_id'=>$member_id));
                        
                        
                        $data_history=array();
                        $data_history['member_id']=$member_id;
                        $data_history['org_id']=$org_id;
                        $data_history['currency']='IDR';
                        $data_history['balance_out']=$balance_out;
                        $data_history['saldo']=$saldo;
                        $data_history['history_date']=date('Y-m-d H:i:s');
                        $data_history['module']='Register Downline';
                        $data_history['description']='Register Downline '.$name.'['.$email.']';
                        $db->insert('member_balance_history',$data_history);
                        
                        //commision
                        $saldo+=$commision;
                        $data_balance=array();
                        $data_balance['balance_idr']=$saldo;
                        $db->update('member_balance',$data_balance,array('org_id'=>$org_id,'member_id'=>$member_id));

                        $data_history=array();
                        $data_history['member_id']=$member_id;
                        $data_history['org_id']=$org_id;
                        $data_history['currency']='IDR';
                        $data_history['balance_in']=$commision;
                        $data_history['saldo']=$saldo;
                        $data_history['history_date']=date('Y-m-d H:i:s');
                        $data_history['module']='Register Downline';
                        $data_history['description']='Commision Register Downline '.$name.'['.$email.']';
                        $db->insert('member_balance_history',$data_history);
                        
                    }
                } catch (Exception $ex) {
                    $error++;
                    $err_message = clang::__('Error, Please call administrator!');
                    clog::write('DB Error page member/account/register_downline/. ' . $ex->getMessage());
                }
            }
            if ($error == 0) {
                $db->commit();
                $err_message = "Downline Successfully Registered";
                $session->set('error_register_downline', $error);
                $session->set('error_register_downline_message', $err_message);
                curl::redirect('member/account?page_account=downline_me');
            } else {
                $db->rollback();
                $session->set('error_register_downline', $error);
                $session->set('error_register_downline_message', $err_message);
                curl::redirect('member/account?page_account=register_downline');
            }
        }
        $error = $session->get_once('error_register_downline');
        $err_message = $session->get_once('error_register_downline_message');
        $form = $app->add_form('form_register_downline')->add_class('form-62hallfamily form-account ')->set_action(curl::base() . 'member/account/register_downline/');
        if (strlen($err_message) > 0) {
            $alert = 'success';
            if ($error > 0) {
                $alert = 'danger';
            }
            $alert_message = $form->add_div()->add_class('alert alert-' . $alert)->add('<a class="close" data-dismiss="alert">×</a>')->add($err_message);
        }
        $div_container = $form->add_div('container');
        $div_header = $div_container->add_div('div_header');
        $div_content = $div_container->add_div('div_content')->add_class('margin-top-30 col-md-12');
        $div_info = $div_container->add_div()->add_class('row')->add_div('div_info')->add_class('margin-top-10 col-md-12');
        $div_action = $div_container->add_div()->add_class('row')->add_div('div_action')->add_class('margin-top-10 col-md-12');
        $div_header->add('<h1><strong>Daftar Downline</strong></h1>');

        $div_content->add_field()->set_label('Nama Lengkap<red>*</red>')->add_control('name', 'text');
        $div_content->add_field()->set_label('Email<red>*</red>')->add_control('email', 'text');
        $div_content->add_field()->set_label('Password<red>*</red>')->add_control('password', 'text');
        $div_content->add_field()->set_label('Ulangi Password<red>*</red>')->add_control('confirm_password', 'text');
        $field = $div_content->add_field()->set_label('No Handphone<red>*</red>');
        $field->add_control('phone_area', 'text')->add_class('control-phone-area')->set_attr('readonly', 'true')->set_value('+62');
        $field->add_control('phone', 'text')->add_class('control-phone-number');

        $field = $div_content->add_field()->set_label('Tanggal Lahir<red>*</red>');
        $list_day = date::array_date();
        $list_month = date::array_month();
        $list_year = date::array_years(70);
        $field->add_control('date_birth', 'select')->add_class('control-day')->set_list($list_day);
        $field->add_control('month_birth', 'select')->add_class('control-month')->set_list($list_month);
        $field->add_control('year_birth', 'select')->add_class('control-year')->set_list($list_year);

        $gender = $div_content->add_field()->set_label('Jenis Kelamin<red>*</red>');

        $gender->add('<div class="col-md-2 margin-top-10"><input class="margin-left-15- " type="radio" value="male" name="gender"/> <span class="normal font-black">Pria</span></div>');
        $gender->add('<div class="col-md-3 margin-top-10"><input type="radio" value="female" name="gender"/> <span class="normal font-black">Wanita</span></div>');

        if ($this->have_product()) {
            $field = $div_content->add_field();
            $field->add('<div class="col-md-12 margin-top-10 padding-left-0"><input class="margin-left-0" type="checkbox" name="is_subscribe" value="1"/> <span class="normal font-black">Saya ingin menerima penawaran produk ' . $this->store . ' melalui email</span></div>');
        }

        if ($this->have_gold()) {
            $field = $div_content->add_field();
            $field->add('<div class="col-md-12 padding-left-0"><input class="margin-left-0" type="checkbox" name="is_subscribe_gold" value="1"/> <span class="normal font-black">Saya ingin menerima penawaran gold ' . $this->store . ' melalui email</span></div>');
        }

        if ($this->have_service()) {
            $field = $div_content->add_field();
            $field->add('<div class="col-md-12 padding-left-0"><input class="margin-left-0" type="checkbox" name="is_subscribe_service" value="1"/> <span class="normal font-black">Saya ingin menerima penawaran service ' . $this->store . ' melalui email</span></div>');
        }

        $fee_info = $div_content->add_div()->add_class('row margin-top-10');
        $fee_info->add('Adanya biaya pendaftaran : 800.000');

        $link_syarat_ketentuan = curl::base() . 'read/page/index/syarat-dan-ketentuan';
        $link_kebijakan_privacy = curl::base() . 'read/page/index/kebijakan-privasi';
        $subcribe = $div_content->add_div()->add_class('row margin-top-10');
        $subcribe->add('Dengan menekan Daftar Akun, saya mengkonfirmasi telah menyetujui <br><a target="_blank" href="' . $link_syarat_ketentuan . '" class="font-red"><u>Syarat dan Ketentuan</u></a>, serta <a target="_blank" href="' . $link_kebijakan_privacy . '" class="font-red"><u>Kebijakan Privasi</u></a> ' . $this->store);

        $div_action
                ->add_action()
                ->add_class('btn-62hallfamily-medium bg-red border-3-red font-size14 pull-left')
                ->set_label('SIMPAN')
                ->set_submit(true);

        echo $app->render();
    }

    public function downline_me() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();
        $member_id = $session->get('member_id');
        $q = "select * from member where member_id=" . $db->escape($member_id);
        $member = cdbutils::get_row($q);
        $downlines = $db->query('select * from member where lft>=' . $db->escape($member->lft) . ' and rgt<=' . $db->escape($member->rgt) . ' order by lft asc');
        $depth = 0;
        $i = 0;
        $app->add_div()->add_class('padding-0 font-size24 bold')->add('Downline Saya');
        $arr = array();
        $member_depth_before = -1;
        $member_depth_first = -1;
        $downline_container = $app->add_div()->add_class('my-downline-container');

        foreach ($downlines as $downline) {
            if ($member_depth_first < 0) {
                $member_depth_first = $downline->depth;
            }
            $depth = $downline->depth - $member_depth_first;
            if ($depth > 4) {
                $depth = 4;
            }
            if ($member_depth_before != $downline->depth) {
                if ($member_depth_before < $downline->depth) {
                    $arr[] = $i;
                    $i = 0;
                }
                if ($member_depth_before > $downline->depth) {
                    for ($j = 0; $j < $member_depth_before - $downline->depth; $j++) {
                        unset($arr[count($arr) - 1]);
                    }
                    $i = $arr[count($arr) - 1];
                }
            }
            $arr[count($arr) - 1] = $i + 1;
            $label = '';
            foreach ($arr as $v) {
                if (strlen($label) > 0)
                    $label.='.';
                $label .=$v;
            }

            $wrapper = $downline_container->add_div()->add_class('downline-wrapper');
            $container = $wrapper->add_div()->add_class('downline-container downline-depth-' . $depth . ' real-downline-depth-' . $downline->depth);
            $div = $container->add_div()->add_class('downline-div-front downline-depth-' . $depth . '')->add($label);
            $div = $container->add_div()->add_class('downline-div-arrow-right downline-depth-' . $depth . '');
            $div = $container->add_div()->add_class('downline-div-arrow-right-white downline-depth-' . $depth . '');
            $div = $container->add_div()->add_class('downline-div downline-depth-' . $depth . '');
            $div->add(strtoupper($downline->name));

            $i++;
            $member_depth_before = $downline->depth;
        }

        echo $app->render();
    }

    public function topup_detail($topup_id = '') {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $q = 'SELECT 
            * 
          FROM 
            topup 
          WHERE status > 0 
          AND topup_id ='.$db->escape($topup_id);
        $detail_topup = cdbutils::get_row($q);
        $code = cobj::get($detail_topup,'code');
        $bank_name = cobj::get($detail_topup,'bank_name');
        $acc_no = cobj::get($detail_topup,'acc_no');
        $acc_name = cobj::get($detail_topup,'acc_name');
        $nominal = cobj::get($detail_topup,'nominal');
        $note = cobj::get($detail_topup,'note');
        $status_confirm = cobj::get($detail_topup,'status_confirm');
        $label_type = 'label-default';
        switch ($status_confirm) {
          case 'PENDING' : {
              $label_type = 'label-warning';
              $status_confirm = clang::__('Pending');
              break;
          }
          case 'CONFIRMED' : {
              $label_type = 'label-info';
              $status_confirm = clang::__('Confirmed');
              break;
          }
          case 'APPROVED' : {
              $label_type = 'label-success';
              $status_confirm = clang::__('Approved');
              break;
          }
          case 'REJECTED' : {
              $label_type = 'label-danger';
              $status_confirm = clang::__('Rejected');
              break;
          }
        }
        
        $container = $app->add_div()->add_class('container');
        $form = $app->add_form()->set_layout('horizontal');
        $form->add_field()->set_label(clang::__('Status'))
                ->set_style_form_group('inline')->set_inline_without_default(false)
                ->add_control('', 'label')
                ->set_value($status_confirm)
                ->add_class($label_type);
        
        $form->add_field()->set_label(clang::__('Kode Topup'))
                ->set_style_form_group('inline')->set_inline_without_default(false)
                ->add_control('', 'label')
                ->set_value($code);
        
        $form->add_field()->set_label(clang::__('Bank Tujuan'))
                ->set_style_form_group('inline')->set_inline_without_default(false)
                ->add_control('', 'label')
                ->set_value($bank_name);
        
        $form->add_field()->set_label(clang::__('Nomor Account Bank'))
                ->set_style_form_group('inline')->set_inline_without_default(false)
                ->add_control('', 'label')
                ->set_value($acc_no);
        
        $form->add_field()->set_label(clang::__('Nama Pada Bank'))
                ->set_style_form_group('inline')->set_inline_without_default(false)
                ->add_control('', 'label')
                ->set_value($acc_name);
        
        $form->add_field()->set_label(clang::__('Nominal'))
                ->set_style_form_group('inline')->set_inline_without_default(false)
                ->add_control('', 'label')
                ->set_value($nominal);
        
        $form->add_field()->set_label(clang::__('Note'))
                ->set_style_form_group('inline')->set_inline_without_default(false)
                ->add_control('', 'label')
                ->set_value($note);
        echo $app->render();
    }
    
    public function topup_confirm($topup_id = '') {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();
        $member_id = $session->get('member_id');
        $org_id = CF::org_id();
        
        if(strlen($topup_id)==0) {
            $danger = $app->add_div()->add_class('alert alert-danger');
            $danger->add(clang::__('Top Up id not found'));
            echo $app->render();
            return;
        }
        
        if(strlen($member_id)==0) {
            $danger = $app->add_div()->add_class('alert alert-danger');
            $danger->add(clang::__('Member Not Found'));
            echo $app->render();
            return;
        }
        
        $request = $_POST;
        $err_code = 0;
        $err_message = '';
        $q = "
            select
                *
            from
                topup
            where
                status>0
                and org_id=" . $db->escape($org_id) . "
                and status_confirm='PENDING'
                and topup_id=" . $db->escape($topup_id) . "
        ";
        
        $topup = cdbutils::get_row($q);
        
        if($request!=null){
            $payment_from = carr::get($request, 'payment_from');
            $nominal_confirm = carr::get($request, 'nominal_confirm');
            $description = carr::get($request, 'description');
            
            if ($err_code == 0) {
                if (strlen($payment_from) == 0) {
                    $err_code++;
                    $err_message = clang::__("Please Fill 'Pembayaran Dari'");
                }
                if ((strlen($nominal_confirm) == 0) || ($nominal_confirm <= 0)) {
                    $err_code++;
                    $err_message = clang::__("Please Fill 'Nominal Transfer'");
                }
                $q = "
                    select
                        *
                    from
                        member
                    where
                        member_id=" . $db->escape($member_id) . "
                ";
                $member = cdbutils::get_row($q);
                $member_name = '';
                if ($member) {
                    $member_name = $member->name;
                }
                if ($err_code == 0) {
                    try {
                        $db->begin();
                        $data_topup = array();
                        $data_topup['payment_from'] = $payment_from;
                        $data_topup['nominal_real'] = ctransform::unformat_currency($nominal_confirm);
                        $data_topup['note'] = $description;
                        $data_topup['status_confirm'] = 'CONFIRMED';
                        $data_topup['updated'] = date('Y-m-d H:i:s');
                        $data_topup['updatedby'] = $member_name;
                        $db->update('topup', $data_topup, array("topup_id" => $topup->topup_id));
                    } catch (Exception $ex) {
                        $err_code++;
                        $err_message = clang::__('Error, Please call administrator!');
                        clog::write('DB Error page member/account/topup_confirm/. ' . $ex->getMessage());
                    }
                }
				if ($err_code == 0) {
				  try{
					  email::send('topup', $topup_id);
				  }catch (Exception $exc) {
					  // dont show anything when error sending mail
				  } 
				}
                if ($err_code == 0) {
                    $db->commit();
                    cmsg::add('success',clang::__('Konfirmasi Topup Berhasil'));
                    return curl::redirect('member/account');
                } else {
                    $db->rollback();
                    cmsg::add('danger',$err_message);
                    return curl::redirect('member/account');
                }
            }
            
        }
        
        $container = $app->add_div()->add_class('container');
        $row = $app->add_div()->add_class('row');
        $col_12 = $row->add_div()->add_class('col-xs-12');
        $col_12->add('<h1><strong>Confirm Top Up</strong></h1>');
        
        $form = $app->add_form()->add_class('form-62hallfamily')->set_layout('horizontal')->set_action(curl::base() . 'member/account/topup_confirm/' . $topup_id);
        
        $form->add_field()->set_label('Pembayaran Dari<red>*</red>')
                ->set_style_form_group('inline')->set_inline_without_default(false)
                ->add_control('payment_from', 'text');
        $form->add_field()->set_label(clang::__('Pembayaran Untuk'))
                ->set_style_form_group('inline')->set_inline_without_default(false)
                ->add_control('payment_to','label')->set_value($topup->bank_name . '-' . $topup->acc_name . '-' . $topup->acc_no);
        $form->add_field()->set_label(clang::__('Nominal'))
                ->set_style_form_group('inline')->set_inline_without_default(false)
                ->add_control('nominal','label')->set_value(ctransform::thousand_separator($topup->bank_nominal));
        $form->add_field()->set_label('Nominal Transfer<red>*</red>')
                ->set_style_form_group('inline')->set_inline_without_default(false)
                ->add_control('nominal_confirm', 'currency');
        $form->add_field()->set_label('Catatan<red>*</red>')->set_style_form_group('inline')
                ->set_inline_without_default(false)
                ->add_control('description', 'textarea');

        $form->add('<p><label><red>*Wajib Diisi</red></label></p>');
        $form
                ->add_action()
                ->add_class('btn-62hallfamily-medium bg-red border-3-red font-size14 pull-left')
                ->set_label('SIMPAN')
                ->set_submit(true);
        
        echo $app->render();
    }
    
    public function topup_confirm_backup($topup_id = '') {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();
        $member_id = $session->get('member_id');
        $org_id = CF::org_id();
        if (strlen($topup_id) == 0) {
            $app->add_div()->add_class('container')->add('<label>Data TopUp Not Found</label>');
            echo $app->render();
            return;
        }
        if (strlen($member_id) == 0) {
            $app->add_div()->add_class('container')->add('<label>Member Not Found</label>');
            echo $app->render();
            return;
        }
        $error = $session->get_once('error_topup');
        $err_message = $session->get_once('error_topup_message');
        $request = $_POST;
        $q = "
            select
                *
            from
                topup
            where
                status>0
                and org_id=" . $db->escape($org_id) . "
                and status_confirm='PENDING'
                and topup_id=" . $db->escape($topup_id) . "
        ";
        $topup = cdbutils::get_row($q);
        if ($topup == null) {
            $app->add_div()->add_class('container')->add('<label>TopUp Not Valid</label>');
            echo $app->render();
            return;
        }

        if ($request != null) {
            $payment_from = carr::get($request, 'payment_from');
            $nominal_confirm = carr::get($request, 'nominal_confirm');
            $description = carr::get($request, 'description');
            //error checking
            if ($error == 0) {
                if (strlen($payment_from) == 0) {
                    $error++;
                    $err_message = "Please Fill 'Pembayaran Dari'";
                }
                if ((strlen($nominal_confirm) == 0) || ($nominal_confirm <= 0)) {
                    $error++;
                    $err_message = "Please Fill 'Nominal Transfer'";
                }
                $q = "
                    select
                        *
                    from
                        member
                    where
                        member_id=" . $db->escape($member_id) . "
                ";
                $member = cdbutils::get_row($q);
                $member_name = '';
                if ($member) {
                    $member_name = $member->name;
                }
                if ($error == 0) {
                    try {
                        $db->begin();
                        $data_topup = array();
                        $data_topup['payment_from'] = $payment_from;
                        $data_topup['nominal_real'] = ctransform::unformat_currency($nominal_confirm);
                        $data_topup['note'] = $description;
                        $data_topup['status_confirm'] = 'CONFIRMED';
                        $data_topup['updated'] = date('Y-m-d H:i:s');
                        $data_topup['updatedby'] = $member_name;
                        $db->update('topup', $data_topup, array("topup_id" => $topup->topup_id));
                    } catch (Exception $ex) {
                        $error++;
                        $err_message = clang::__('Error, Please call administrator!');
                        clog::write('DB Error page member/account/topup_confirm/. ' . $ex->getMessage());
                    }
                }
                if ($error == 0) {
                    $db->commit();
                    $err_message = "Confirm Top Up Sukses";
                    $session->set('error_topup', $error);
                    $session->set('error_topup_message', $err_message);
                    curl::redirect('member/account?page_account=history');
                } else {
                    $db->rollback();
                    $session->set('error_topup', $error);
                    $session->set('error_topup_message', $err_message);
                    curl::redirect('member/account?page_account=history');
                }
            }
        }
        $form = $app->add_form('form_topup')->add_class('form-62hallfamily form-account')->set_action(curl::base() . 'member/account/topup_confirm/' . $topup_id);
        if (strlen($err_message) > 0) {
            $alert = 'success';
            if ($error > 0) {
                $alert = 'danger';
            }
            $alert_message = $form->add_div()->add_class('alert alert-' . $alert)->add('<a class="close" data-dismiss="alert">×</a>')->add($err_message);
        }

        $div_container = $form->add_div('container');

        $div_header = $div_container->add_div('div_header');
        $div_content = $div_container->add_div('div_content')->add_class('margin-top-30 col-md-12');
        $div_info = $div_container->add_div('div_info')->add_class('margin-top-10 col-md-12');
        $div_action = $div_container->add_div('div_action')->add_class('margin-top-10 col-md-12');
        $div_header->add('<h1><strong>Confirm Top Up</strong></h1>');

        
        $div_content->add_field()->set_label('Pembayaran Dari<red>*</red>')->add_control('payment_from', 'text');
        $div_payment_to = $div_content->add_field()->set_label('Pembayaran Untuk<red>*</red>')->add_div()->add_class('margin-top-10');
        $div_payment_to->add($topup->bank_name . '-' . $topup->acc_name . '-' . $topup->acc_no);
        $div_nominal = $div_content->add_field()->set_label('Nominal TopUp<red>*</red>')->add_div()->add_class('margin-top-10');
        $div_nominal->add(ctransform::format_currency($topup->bank_nominal));
        $div_content->add_field()->set_label('Nominal Transfer<red>*</red>')->add_control('nominal_confirm', 'currency');
        $div_content->add_field()->set_label('Catatan<red>*</red>')->add_control('description', 'textarea');

        $div_info->add('<p><label><red>*Wajib Diisi</red></label></p>');
        $div_action
                ->add_action()
                ->add_class('btn-62hallfamily-medium bg-red border-3-red font-size14 pull-left')
                ->set_label('SIMPAN')
                ->set_submit(true);


        echo $app->render();
    }

    public function topup() {
      $app = CApp::instance();
              $db = CDatabase::instance();
              $session = Session::instance();
              $member_id = $session->get('member_id');

              $request = $_POST;
              $error = 0;
              $err_message = '';
              if ($request != null) {
                  $org_bank_id = carr::get($request, 'org_bank');
                  $payment_from = carr::get($request, 'payment_from');
                  $org_topup_nominal_id = carr::get($request, 'org_topup_nominal');
                  $description = carr::get($request, 'description');
                  $org_id = CF::org_id();
                  //error checking
                  if ($error == 0) {
                      if (strlen($org_bank_id) == 0) {
                          $error++;
                          $err_message = clang::__("Payment To is required");
                      }
                  }
                  if ($error == 0) {
                      if (strlen($org_topup_nominal_id) == 0) {
                          $error++;
                          $err_message = clang::__("Amount is required");
                      }
                  }
                  if ($error == 0) {
                      $q = "
                              select
                                      *
                              from
                                      org_bank
                              where
                                      org_bank_id=" . $db->escape($org_bank_id) . " AND status > 0
                      ";
                      $org_bank = cdbutils::get_row($q);
                      $org_bank_name = '';
                      $org_bank_acc_no = '';
                      $org_bank_acc_name = '';
                      if ($org_bank) {
                          $org_bank_name = $org_bank->bank;
                          $org_bank_acc_no = $org_bank->account_number;
                          $org_bank_acc_name = $org_bank->account_holder;
                      }
                      else {
                          $error++;
                          $err_message = "Data Not Found [1]";
                      }
                      $q = "
                              select
                                      *
                              from
                                      org_topup_nominal
                              where
                                      org_topup_nominal_id=" . $db->escape($org_topup_nominal_id) . " AND status > 0
                      ";
                      $org_topup_nominal = cdbutils::get_row($q);
                      $nominal = 0;
                      if ($org_topup_nominal) {
                          $nominal = $org_topup_nominal->nominal;
                      }
                      else {
                          $error++;
                          $err_message = "Data Not Found [1]";
                      }

                      if ($error == 0) {
                          try {
                              $db->begin();
                              $code = generate_code::get_next_topup_code();
                              $multiple = 1;
                              $data_topup = array();
                              $data_topup['org_id'] = $org_id;
                              $data_topup['member_id'] = $member_id;
                              $data_topup['org_bank_id'] = $org_bank_id;
                              $data_topup['org_topup_nominal_id'] = $org_topup_nominal_id;
                              $data_topup['code'] = $code;
                              $data_topup['currency_code'] = 'IDR';
                              $data_topup['bank_name'] = $org_bank_name;
                              $data_topup['acc_no'] = $org_bank_acc_no;
                              $data_topup['acc_name'] = $org_bank_acc_name;
                              $data_topup['nominal'] = $nominal;
                              $data_topup['multiple'] = $multiple;
                              $data_topup['nominal_total'] = $nominal * $multiple;
                              $data_topup['bank_nominal'] = $nominal * $multiple;
                              $data_topup['note'] = $description;
                              $data_topup['status_confirm'] = 'PENDING';
                              $data_topup['confirmed'] = date('Y-m-d H:i:s');
                              $data_topup['payment_from'] = '';
                              $data_topup['created'] = date('Y-m-d H:i:s');
                              $data_topup['createdby'] = $member_id;
                              $data_topup['updated'] = date('Y-m-d H:i:s');
                              $data_topup['updatedby'] = $member_id;                              
                              $r = $db->insert('topup', $data_topup);
                              $topup_id = $r->insert_id();
                                                          
                                                          // update nominal plus digit random
                                                          $digit_random=$topup_id%1000;
                                                          $data_update=array();
                                                          
                                                          $data_update['bank_nominal']=($nominal * $multiple)+$digit_random;
                                                          $db->update('topup', $data_update,array("topup_id"=>$topup_id));
                                                          
                          }
                          catch (Exception $ex) {
                              $error++;
                              $err_message = 'DB-ERROR. '.clang::__('Error, Please call administrator!');
                          }
                      }
                  }
				  if($error==0){
						  try{
							  email::send('topup', $topup_id);
						  }catch (Exception $exc) {
							  throw new Exception('ERR[EMAIL-1] ' .$exc->getMessage());
							//do not do anything if error sending email
						  }   
				  }
                  if ($error > 0) {
					  $db->rollback();
                      cmsg::add('error', $err_message);
                  }
                  else {
					  $db->commit();
                      cmsg::add('success', clang::__("Top Up Request") .' '.$code .' ' .clang::__("has been successfully. Please confirm your request top up in history menu"));
                      curl::redirect(curl::base() .'member/account');
                  }
              }

              $container = $app->add_div()->add_class('row');
              $row_12 = $container->add_div()->add_class('col-sm-12');
              $row_12->add('<h4 class="member-title">' .clang::__('Top Up').'</h4>');
              $form = $app->add_form('form_topup')->add_class('form-62hallfamily form-account')->set_action(curl::base() . 'member/account/topup/')->set_layout('horizontal');
              $form->add_field()
                      ->set_style_form_group('inline')->set_inline_without_default(false)
                      ->set_label(clang::__('Payment To').' <red>*</red>')
                      ->add_control('org_bank', 'org-bank-select')
                      ->custom_css('width','100%')
                      ->add_dropdown_class('select2-lastm');
              $form->add_field()
                      ->set_style_form_group('inline')->set_inline_without_default(false)
                      ->set_label(clang::__('Amount').' <red>*</red>')
                      ->add_control('org_topup_nominal', 'org-topup-nominal-select')
                      ->custom_css('width','100%')
                      ->add_dropdown_class('select2-lastm');
              $form->add_field()
                      ->set_style_form_group('inline')->set_inline_without_default(false)
                      ->set_label(clang::__('Message'))
                      ->add_control('description', 'textarea')
                      ->custom_css('width','100%');
              $form->add_action()
                      ->add_class('btn-62hallfamily bg-red border-3-red margin-top-20')
                      ->set_label(clang::__('SAVE'))
                      ->set_submit(true);
              echo $app->render();
    }
    
    public function topup_backup() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();
        $member_id = $session->get('member_id');
        if (strlen($member_id) == 0) {
            $app->add_div()->add_class('container')->add('<label>Member Not Found</label>');
            echo $app->render();
            return;
        }
        $request = $_POST;
        $error = 0;
        $err_message = '';
        if ($request != null) {
            $org_bank_id = carr::get($request, 'org_bank');
            $org_topup_nominal_id = carr::get($request, 'org_topup_nominal');
            $description = carr::get($request, 'description');
            $org_id = CF::org_id();
            //error checking
            if ($error == 0) {
                if (strlen($org_bank_id) == 0) {
                    $error++;
                    $err_message = "Please Select 'Pembayaran Untuk'";
                }
                if (strlen($org_topup_nominal_id) == 0) {
                    $error++;
                    $err_message = "Please Select 'Nominal'";
                }
                $q = "
                    select
                        *
                    from
                        org_bank
                    where
                        org_bank_id=" . $db->escape($org_bank_id) . "
                ";
                $org_bank = cdbutils::get_row($q);
                $org_bank_name = '';
                $org_bank_acc_no = '';
                $org_bank_acc_name = '';
                if ($org_bank) {
                    $org_bank_name = $org_bank->bank;
                    $org_bank_acc_no = $org_bank->account_number;
                    $org_bank_acc_name = $org_bank->account_holder;
                } else {
                    $error++;
                    $err_message = "Data Not Found [1]";
                }
                $q = "
                    select
                        *
                    from
                        org_topup_nominal
                    where
                        org_topup_nominal_id=" . $db->escape($org_topup_nominal_id) . "
                ";
                $org_topup_nominal = cdbutils::get_row($q);
                $nominal = 0;
                if ($org_topup_nominal) {
                    $nominal = $org_topup_nominal->nominal;
                } else {
                    $error++;
                    $err_message = "Data Not Found [1]";
                }
                $q = "
                    select
                        *
                    from
                        member
                    where
                        member_id=" . $db->escape($member_id) . "
                ";
                $member = cdbutils::get_row($q);
                $member_name = '';
                if ($member) {
                    $member_name = $member->name;
                }
                if ($error == 0) {
                    try {
                        $db->begin();
                        $multiple = 1;
                        $data_topup = array();
                        $data_topup['org_id'] = $org_id;
                        $data_topup['member_id'] = $member_id;
                        $data_topup['org_bank_id'] = $org_bank_id;
                        $data_topup['org_topup_nominal_id'] = $org_topup_nominal_id;
                        $data_topup['code'] = generate_code::get_next_topup_code();
                        $data_topup['currency_code'] = 'IDR';
                        $data_topup['bank_name'] = $org_bank_name;
                        $data_topup['acc_no'] = $org_bank_acc_no;
                        $data_topup['acc_name'] = $org_bank_acc_name;
                        $data_topup['nominal'] = $nominal;
                        $data_topup['multiple'] = $multiple;
                        $data_topup['nominal_total'] = $nominal * $multiple;
                        $data_topup['bank_nominal'] = $nominal * $multiple;
                        $data_topup['note'] = $description;
                        $data_topup['status_confirm'] = 'PENDING';
                        $data_topup['created'] = date('Y-m-d H:i:s');
                        $data_topup['createdby'] = $member_name;
                        $data_topup['updated'] = date('Y-m-d H:i:s');
                        $data_topup['updatedby'] = $member_name;
                        $db->insert('topup', $data_topup);
                    } catch (Exception $ex) {
                        $error++;
                        $err_message = clang::__('Error, Please call administrator!');
                        clog::write('DB Error page member/account/topup/. ' . $ex->getMessage());
                    }
                }
                if ($error == 0) {
                    $db->commit();
                    $err_message = "Request Top Up Sukses";
                    $session->set('error_topup', $error);
                    $session->set('error_topup_message', $err_message);
                    curl::redirect('member/account?page_account=topup');
                } else {
                    $db->rollback();
                    $session->set('error_topup', $error);
                    $session->set('error_topup_message', $err_message);
                    curl::redirect('member/account?page_account=topup');
                }
            }
        }
        $error = $session->get_once('error_topup');
        $err_message = $session->get_once('error_topup_message');
        $form = $app->add_form('form_topup')->add_class('form-62hallfamily form-account')->set_action(curl::base() . 'member/account/topup/');
        if (strlen($err_message) > 0) {
            $alert = 'success';
            if ($error > 0) {
                $alert = 'danger';
            }
            $alert_message = $form->add_div()->add_class('alert alert-' . $alert)->add('<a class="close" data-dismiss="alert">×</a>')->add($err_message);
        }
        $div_container = $form->add_div('container');
        $div_header = $div_container->add_div('div_header');
        $div_content = $div_container->add_div('div_content')->add_class('margin-top-30 col-md-12');
        $div_info = $div_container->add_div('div_info')->add_class('margin-top-10 col-md-12');
        $div_action = $div_container->add_div('div_action')->add_class('margin-top-10 col-md-12');
        $div_header->add('<h1><strong>Top Up</strong></h1>');
        $div_content->add_field()->set_label('Pembayaran Untuk<red>*</red>')->add_control('org_bank', 'org-bank-select');
        $div_content->add_field()->set_label('Nominal<red>*</red>')->add_control('org_topup_nominal', 'org-topup-nominal-select');

        $div_content->add_field()->set_label('Catatan<red>*</red>')->add_control('description', 'textarea');

        $div_info->add('<p><label><red>*Wajib Diisi</red></label></p>');
        $div_action
                ->add_action()
                ->add_class('btn-62hallfamily-medium bg-red border-3-red font-size14 pull-left')
                ->set_label('SIMPAN')
                ->set_submit(true);

        echo $app->render();
    }

    public function history() {
        $app = CApp::instance();
        $session = Session::instance();
        $member_id = $session->get('member_id');
        if (strlen($member_id) == 0) {
            $app->add_div()->add_class('container')->add('<label>Member Not Found</label>');
            echo $app->render();
            return;
        }
        if ($member_id) {
            $tabs = $app->add_tab_list('tabs-horizontal')->add_class('tabs-horizontal tabs-member-history');
            if ($this->have_topup_transfer() > 0) {
            }
            if ($this->have_deposit() > 0) {
                $tab_history_saldo = $tabs->add_tab('deposit')
                        ->set_label('<b>DEPOSIT</b>')
                        ->set_ajax_url(curl::base() . "member/account/history_deposit/");
            }
        }
        echo $app->render();
    }
    
    public function history2($tab = '') {
            $app = CApp::instance();

            $tabs = $app->add_tab_list();
            $tab = $tabs->add_tab('topup')->set_label(clang::__('Top Up'))->set_ajax_url(curl::base() . "member/account/history_topup");
            $tab = $tabs->add_tab('balance')->set_label(clang::__('Balance'))->set_ajax_url(curl::base() . "master/product/detail_product_data")->set_nopadding(true);
            echo $app->render();
        }
    
    public function history_topup2() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();

        $query = 'SELECT tp.*
                FROM topup tp
                WHERE tp.member_id = ' . $db->escape($session->get('member_id')) . '
                    AND tp.status > 0
                ORDER BY tp.created DESC';

        $div_container = $app->add_div()->add_class('dataTable-bigmall')->add_form();
        $table = $div_container->add_table()->set_widget_title(false);
        $table->set_ajax(true)->set_quick_search(TRUE);
        $table->set_dom("<'row'<'col-sm-6 col-xs-6 show-entry-bigmall'l><'col-sm-6 col-xs-6 search-entry-bigmall'f>><'row'<'col-sm-12'tr>><'row'<'col-md-6 col-sm-12'i><'col-md-6 col-sm-12'p>>");
        $table->set_data_from_query($query);
        $table->add_column('created')->set_label('DATE')->set_width(150);
        $table->add_column('payment_from')->set_label('FROM');
        $table->add_column('bank_name')->set_label('BANK');
        $table->add_column('acc_no')->set_label('ACCOUNT NO');
        $table->add_column('acc_name')->set_label('ACCOUNT NAME');
        $table->add_column('nominal')->set_label('AMOUNT')->add_transform('format_currency')->set_align('right');
        $table->add_column('status_confirm')->set_label('STATUS');
        $table->set_action_style('btn-dropdown')->add_btn_dropdown_class('btn-blue btn-sm');
        $action = $table->add_row_action();
        $btn = $action->set_icon('error')->set_label(clang::__('Action'))->set_link(curl::httpbase().'member/history/cancelWithdraw?id={member_withdraw_id}');
        $table->set_key("topup_id");
        $table->cell_callback_func(array('Account_Controller', 'cell_callback_topup'), __FILE__);

        echo $app->render();
    }
    
    public static function cell_callback_topup($table, $col, $row, $text) {

            if ($col == 'status_confirm') {
                $label_class = 'primary';
                $icon = '';
                if ($text == 'REJECTED') {
                    $label_class = 'danger';
                    $icon = '';
                }
                else if ($text == 'SUCCESS') {
                    $label_class = 'success';
                    $icon = '';
                }
                $label_status = '<h4 class="label-status"><span class="label label-' . $label_class . '">';
                $label_status .= $text;
                $label_status .= ' ' . $icon . '</span></h4>';
                $text = $label_status;
            }

            return $text;
        }
    
    public function history_topup() {
        $app = CApp::instance();
        $session = Session::instance();
        $member_id = $session->get('member_id');
        if (strlen($member_id) == 0) {
            $app->add_div()->add_class('container')->add('<label>Member Not Found</label>');
            echo $app->render();
            return;
        }
        $div_filter = $app->add_div('filter');
        $arr_show = array(
            '1' => '01',
            '2' => '02',
            '3' => '03',
            '4' => '04',
            '5' => '05',
        );


        $div_filter_date = $div_filter->add_div()->add_class('col-lg-12 padding-0 margin-0');
        $div_filter_date_left = $div_filter_date->add_div()->add_class('col-lg-9 padding-0 margin-0');
        $div_filter_date_right = $div_filter_date->add_div()->add_class('col-lg-3 padding-0 margin-0');
        $field_date = $div_filter_date_left->add_field();
        $field_date->add('<label>Tanggal Transaksi</label>');
        $control_date_start = $field_date->add_control('date_start', 'date')->add_class('width-30-percent margin-left-10 display-inline')->set_value(date('Y-m-d', strtotime("-30 days")));
        $field_date->add('<label class="margin-left-10">s/d</label>');
        $control_date_end = $field_date->add_control('date_end', 'date')->add_class('width-30-percent margin-left-10 display-inline')->set_value(date('d-m-Y'));
        //button
        $control_button_search = $div_filter_date_right->add_action()
                ->add_class('btn-62hallfamily-medium bg-red border-3-red font-size14 pull-right')
                ->set_label('Cari');
        $control_button_search->add_listener('click')
                ->add_handler('reload')
                ->set_target('content')
                ->add_param_input(array('date_start', 'date_end'))
                ->set_url(curl::base() . 'member/account/history_topup_data/');

        $control_button_search->add_listener('ready')
                ->add_handler('reload')
                ->set_target('content')
                ->add_param_input(array('date_start', 'date_end'))
                ->set_url(curl::base() . 'member/account/history_topup_data/');
        $div_content = $app->add_div('content');
        echo $app->render();
    }

    public function history_topup_data() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();
        $member_id = $session->get('member_id');
        if (strlen($member_id) == 0) {
            $app->add_div()->add_class('container')->add('<label>Member Not Found</label>');
            echo $app->render();
            return;
        }
        $get = $_GET;
        $date_start = carr::get($get, 'date_start');
        $date_end = carr::get($get, 'date_end');

        $content = $app->add_div()->add_class('col-md-12 margin-top-20 margin-bottom-20 padding-0');
        $content->add('<table class="table-62hallfamily-default">');
        $content->add('<tr class="table-header">');
        $content->add('<th>');
        $content->add('#');
        $content->add('</th>');
        $content->add('<th width="100px">');
        $content->add('TANGGAL <br>TOP UP');
        $content->add('</th>');
        $content->add('<th width="100px">');
        $content->add('KODE <br>TOP UP');
        $content->add('</th>');
        $content->add('<th>');
        $content->add('NO. REKENING');
        $content->add('</th>');
        $content->add('<th>');
        $content->add('TUJUAN');
        $content->add('</th>');
        $content->add('<th>');
        $content->add('NOMINAL');
        $content->add('</th>');
        $content->add('<th>');
        $content->add('STATUS');
        $content->add('</th>');
        $content->add('<th>');
        $content->add('ACTION');
        $content->add('</th>');
        //get data
        $q = "
            select
                *
            from
                topup
            where
                status>0
                and member_id=" . $db->escape($member_id) . "
                and org_id=" . $this->org_id() . "
        ";
        if (strlen($date_start) > 0) {
            $q.="
                and date(created)>=" . $db->escape(ctransform::unformat_date($date_start)) . "
            ";
        }
        if (strlen($date_end) > 0) {
            $q.="
                and date(created)<=" . $db->escape(ctransform::unformat_date($date_end)) . "
            ";
        }
        $q .= " ORDER BY created DESC";
        $r = $db->query($q);
        $no = 1;
        if ($r->count() > 0) {
            foreach ($r as $row) {
                $list_action = array();
                $action = array();
                $action['label'] = 'Detail';
                $action['link'] = curl::base() . 'member/account/topup_detail/' . $row->topup_id;
                $list_action[] = $action;
                $date_topup = ctransform::format_date($row->created);
                $code = $row->code;
                $acc_no = $row->acc_no;
                $acc_name = $row->acc_name;
                $status_confirm = $row->status_confirm;
                $nominal = $row->bank_nominal;
                $status = '<span class="label label-warning">'.clang::__('Pending').'</label>';
                switch ($status_confirm) {
                    case 'PENDING' : {
                            $action = array();
                            $action['label'] = 'Confirm';
                            $action['link'] = curl::base() . 'member/account/topup_confirm/' . $row->topup_id;
                            $list_action[] = $action;
                            break;
                        }
                    case 'CONFIRMED' : {
                            $status = '<span class="label label-info">'.clang::__('Confirmed').'</label>';
                            break;
                        }
                    case 'APPROVED' : {
                            $status = '<span class="label label-success">'.clang::__('Accepted').'</label>';
                            break;
                        }
                    case 'REJECTED' : {
                            $status = '<span class="label label-danger">'.clang::__('Rejected').'</label>';
                            break;
                        }
                }
                $content->add('<tr>');
                $content->add('<td class="horizontal-align-center">');
                $content->add($no . '.');
                $content->add('</td>');
                $content->add('<td class="horizontal-align-center">');
                $content->add($date_topup);
                $content->add('</td>');
                $content->add('<td class="horizontal-align-center">');
                $content->add($code);
                $content->add('</td>');
                $content->add('<td class="horizontal-align-center">');
                $content->add($acc_no);
                $content->add('</td>');
                $content->add('<td class="horizontal-align-center">');
                $content->add($acc_name);
                $content->add('</td>');
                $content->add('<td class="horizontal-align-center">');
                $content->add(ctransform::thousand_separator($nominal));
                $content->add('<td class="horizontal-align-center text-center">');
                $content->add($status);
                $content->add('</td>');
                $content->add('<td class="horizontal-align-center">');
                $action_list = $content->add_action_list()->set_style("btn-dropdown")->add_class('btn-action');
                foreach ($list_action as $val) {
                    $control_action = $action_list->add_action()->set_label(carr::get($val, 'label'));
                    $control_action
                            ->add_listener('click')
                            ->add_handler('reload')
                            ->set_target('page-account-container')
                            ->set_url(carr::get($val, 'link'));
                }
                $content->add('</td>');
                $content->add('</tr>');
                $no++;
            }
        }
        echo $app->render();
    }

    public function history_deposit() {
        $app = CApp::instance();
        $session = Session::instance();
        $member_id = $session->get('member_id');
        if (strlen($member_id) == 0) {
            $app->add_div()->add_class('container')->add('<label>Member Not Found</label>');
            echo $app->render();
            return;
        }
        $div_filter = $app->add_div('filter');
        $div_content = $app->add_div('content');
        $arr_show = array(
            '1' => '01',
            '2' => '02',
            '3' => '03',
            '4' => '04',
            '5' => '05',
        );
        $field_show = $div_filter->add_field();
        $field_show->add('<label>Show</label>');
        $field_show->add_control('show', 'select')->add_class('width-10-percent margin-left-10 display-inline')->set_list($arr_show);
        $field_show->add('<label class="margin-left-10">Entries</label>');

        $div_filter_date = $div_filter->add_div()->add_class('col-lg-12 padding-0 margin-0');
        $div_filter_date_left = $div_filter_date->add_div()->add_class('col-lg-9 padding-0 margin-0');
        $div_filter_date_right = $div_filter_date->add_div()->add_class('col-lg-3 padding-0 margin-0');
        $field_date = $div_filter_date_left->add_field();
        $field_date->add('<label>Tanggal Transaksi</label>');
        $control_date_start = $field_date->add_control('date_start', 'date')->add_class('width-30-percent margin-left-10 display-inline')->set_value(date('d-m-Y'));
        $field_date->add('<label class="margin-left-10">s/d</label>');
        $control_date_end = $field_date->add_control('date_end', 'date')->add_class('width-30-percent margin-left-10 display-inline')->set_value(date('d-m-Y'));
        //button
        $control_button_search = $div_filter_date_right->add_action()
                ->add_class('btn-62hallfamily-medium bg-red border-3-red font-size14 pull-right')
                ->set_label('Cari');
        $control_button_search->add_listener('click')
                ->add_handler('reload')
                ->set_target('content')
                ->add_param_input(array('date_start', 'date_end'))
                ->set_url(curl::base() . 'member/account/history_deposit_data/');

        $control_button_search->add_listener('ready')
                ->add_handler('reload')
                ->set_target('content')
                ->add_param_input(array('date_start', 'date_end'))
                ->set_url(curl::base() . 'member/account/history_deposit_data/');
        echo $app->render();
    }

    public function history_deposit_data() {
        $app = CApp::instance();
        $session = Session::instance();
        $db = CDatabase::instance();
        $get = $_GET;
        $date_start = carr::get($get, 'date_start');
        $date_end = carr::get($get, 'date_end');
        $member_id = $session->get('member_id');
        if (strlen($member_id) == 0) {
            $app->add_div()->add_class('container')->add('<label>Member Not Found</label>');
            echo $app->render();
            return;
        }

        $content = $app->add_div()->add_class('col-md-12 margin-top-20 padding-0');
        $content->add('<table class="table-62hallfamily-gray">');
        $content->add('<tr>');
        $content->add('<th>');
        $content->add('#');
        $content->add('</th>');
        $content->add('<th width="100px">');
        $content->add('TANGGAL');
        $content->add('</th>');
        $content->add('<th width="100px">');
        $content->add('DESKRIPSI');
        $content->add('</th>');
        $content->add('<th>');
        $content->add('MASUK');
        $content->add('</th>');
        $content->add('<th>');
        $content->add('KELUAR');
        $content->add('</th>');
        $content->add('<th>');
        $content->add('SALDO');
        $content->add('</th>');
        //get data
        $q = "
            select
                *
            from
                member_balance_history
            where
                status>0
                and member_id=" . $db->escape($member_id) . "
                and org_id=" . $this->org_id() . "
        ";
        if (strlen($date_start) > 0) {
            $q.="
                and date(history_date)>=" . $db->escape(ctransform::unformat_date($date_start)) . "
            ";
        }
        if (strlen($date_end) > 0) {
            $q.="
                and date(history_date)<=" . $db->escape(ctransform::unformat_date($date_end)) . "
            ";
        }
        $r = $db->query($q);
        $no = 1;
        if ($r->count() > 0) {
            foreach ($r as $row) {
                $date = ctransform::format_date($row->history_date);
                $description = $row->description;
                $balance_in = $row->balance_in;
                $balance_out = $row->balance_out;
                $saldo = $row->saldo;
                $content->add('<tr>');
                $content->add('<td class="horizontal-align-center">');
                $content->add($no . '.');
                $content->add('</td>');
                $content->add('<td class="horizontal-align-center">');
                $content->add($date);
                $content->add('</td>');
                $content->add('<td class="horizontal-align-center">');
                $content->add($description);
                $content->add('</td>');
                $content->add('<td class="horizontal-align-center">');
                $content->add(ctransform::format_currency($balance_in));
                $content->add('</td>');
                $content->add('<td class="horizontal-align-center">');
                $content->add(ctransform::format_currency($balance_out));
                $content->add('</td>');
                $content->add('<td class="horizontal-align-center">');
                $content->add(ctransform::format_currency($saldo));
                $content->add('</td>');
                $content->add('</tr>');
                $no++;
            }
        }
        echo $app->render();
    }

    public function info() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();
        $member_id = $session->get('member_id');

        $request = array_merge($_POST, $_GET);
        $product_subscribe = carr::get($request, 'product_subscribe', 0);
        $service_subscribe = carr::get($request, 'service_subscribe', 0);
        $gold_subscribe = carr::get($request, 'gold_subscribe', 0);
        $act = carr::get($request, 'act');
        $org_name=cdbutils::get_value("select name from org where org_id=".$db->escape(CF::org_id()));
        $err_code = 0;
        if (strlen($act) > 0) {
            $data_email = member::get_email_member($member_id);
            $email = $data_email->email;


            $db->begin();
            try {
                if ($product_subscribe != '1')
                    $product_subscribe = '0';

                $data_newletter = array(
                    'is_subscribe' => $product_subscribe
                );

                $db->update('member', $data_newletter, array('member_id' => $member_id));

                $data_subscribe = array(
                    'member_id' => $member_id,
                    'email_subscribe' => $email,
                    'is_subscribe' => $product_subscribe,
                );
                $result = subscribe::save('product', $data_subscribe);
                $err_code += $result['err_code'];
               
                $err_code += $result['err_code'];
            } catch (Exception $ex) {
                clog::write('DB Error page member/newsletter. ' . $ex->getMessage());
                $db->rollback();
            }
            if ($err_code == 0) {
                $db->commit();
                cmsg::add('success', 'Setting newsletter anda berhasil diubah');
            }
        }

        // MEMBER
        $query = 'SELECT *
                        FROM member 
                        WHERE status > 0 AND member_id = ' . $db->escape($member_id);
        $data_member = cdbutils::get_row($query);

        // MEMBER ADDRESS PAYMENT
        $query = 'SELECT 
                        ma.*, p.name as province, c.name as city, d.name as districts
                    FROM 
                        member_address ma
                    LEFT JOIN 
                        province p
                    ON
                        p.province_id = ma.province_id
                    LEFT JOIN 
                        city c
                    ON
                        c.city_id = ma.city_id
                    LEFT JOIN 
                        districts d
                    ON
                        d.districts_id = ma.districts_id
                    WHERE 
                        ma.status > 0 
                        AND member_id = ' . $db->escape($member_id) . '
                        AND type = ' . $db->escape($this->type_payment) . '
                    ORDER BY
                        is_active DESC
                ';
        $data_address_payment = cdbutils::get_row($query);

         // MEMBER ADDRESS SHIPPING
        $query = 'SELECT 
                        ma.*, p.name as province, c.name as city, d.name as districts
                    FROM 
                        member_address ma
                    LEFT JOIN 
                        province p
                    ON
                        p.province_id = ma.province_id
                    LEFT JOIN 
                        city c
                    ON
                        c.city_id = ma.city_id
                    LEFT JOIN 
                        districts d
                    ON
                        d.districts_id = ma.districts_id
                    WHERE 
                        ma.status > 0 
                        AND member_id = ' . $db->escape($member_id) . '
                        AND type = ' . $db->escape($this->type_shipping) . '
                    ORDER BY
                        is_active DESC
                ';
        $data_address_shipping = cdbutils::get_row($query);

        $title = clang::__("Account Page");
        $row = $app->add_div()->add_class('row');
        $content = $row->add_div()->add_class('col-md-12 container-account-info');
        $content->add_div()->add_class('txt-info-title')->add($title);
        $subtitle = $content->add_div()->add_class('txt-info-subtitle');
        $subtitle->add_div()->add('Hi, '.$data_member->name.'!');
        $subtitle->add_div()->add('Dari Halaman Akun ini Anda dapat melihat profil, newsletter, dan memperbarui informasi akun anda.');
        $content->add_div()->add_class('member-info-separate');

        $container_profil = $content->add_div()->add_class('row');
        $container_title = $container_profil->add_div()->add_class('col-xs-9');
        $container_button = $container_profil->add_div()->add_class('col-xs-3');
        $container_title->add_div()->add_class('txt-account-title')->add(clang::__('My Profil'));
        $container_button->add_div()->add_action()
                ->add_class('change-account btn-primary btn-imbuilding-light pull-right')
                ->set_label(clang::__('Change'))
                ->add_listener('click')
                ->add_handler('reload')
                ->set_target('page-account-container')
                ->set_url(curl::base() . 'member/account/change');

        $date_birth = $data_member->date_of_birth;
        $text_birth = '';
        if (strlen($date_birth) > 0) {
            $date = Date('d', strtotime($date_birth));
            $month = Date('n', strtotime($date_birth));
            $year = Date('Y', strtotime($date_birth));
            $arr_month = date::array_month();
            $tmonth = $arr_month[$month];

            $text_birth = $date.' '.$tmonth.' '.$year;
        }

        $arr_profil[] = array(
            'name' => clang::__('Full Name'),
            'content' => $data_member->name
        );
        $arr_profil[] = array(
            'name' => clang::__('Email'),
            'content' => $data_member->email
        );
        $arr_profil[] = array(
            'name' => clang::__('Date of Birth'),
            'content' => $text_birth
        );
        $arr_profil[] = array(
            'name' => clang::__('Phone'),
            'content' => $data_member->phone
        );
        $arr_profil[] = array(
            'name' => clang::__('Gender'),
            'content' => ucfirst(clang::__($data_member->gender))
        );

        foreach ($arr_profil as $key => $value) {
            $aname = carr::get($value, 'name');
            $acontent = carr::get($value, 'content');
            $container_label = $content->add_div()->add_class('row');
            $container_left = $container_label->add_div()->add_class('col-sm-3');
            $container_right = $container_label->add_div()->add_class('col-sm-9');
            $container_left->add_div()->add_class('txt-account-label')->add($aname);
            $content_right = $container_right->add_div()->add_class('txt-account-label');
            $content_right->add_div()->add_class('txt-label-colon')->add(':&nbsp;');
            $content_right->add_div()->add_class('txt-label-text')->add($acontent);
        }

        $content->add_div()->add_class('member-info-separate');

        $container_profil = $content->add_div()->add_class('row');
        $container_title = $container_profil->add_div()->add_class('col-xs-9');
        $container_button = $container_profil->add_div()->add_class('col-xs-3');
        $container_title->add_div()->add_class('txt-account-title')->add(clang::__('Payment Address'));
        $container_button->add_div()->add_action()
                ->add_class('address-book btn-primary btn-imbuilding-light pull-right')
                ->set_label(clang::__('Change'))
                ->add_listener('click')
                ->add_handler('reload')
                ->set_target('page-account-container')
                ->set_url(curl::base() . 'member/account/address_book/'.$member_id);

        if ($data_address_payment) {
            $arr_payment[] = array(
                'name' => clang::__('Full Name'),
                'content' => $data_address_payment->name,
            );
            $arr_payment[] = array(
                'name' => clang::__('Phone'),
                'content' => $data_address_payment->phone,
            );
            $arr_payment[] = array(
                'name' => clang::__('Address'),
                'content' => $data_address_payment->address.'<br>'.$data_address_payment->province . ' - ' . $data_address_payment->city . ' - ' . $data_address_payment->districts,
            );
            $arr_payment[] = array(
                'name' => clang::__('Postal Code'),
                'content' => $data_address_payment->postal,
            );
            foreach ($arr_payment as $key => $value) {
                $aname = carr::get($value, 'name');
                $acontent = carr::get($value, 'content');
                $container_label = $content->add_div()->add_class('row');
                $container_left = $container_label->add_div()->add_class('col-sm-3');
                $container_right = $container_label->add_div()->add_class('col-sm-9');
                $container_left->add_div()->add_class('txt-account-label')->add($aname);
                $content_right = $container_right->add_div()->add_class('txt-account-label');
                $content_right->add_div()->add_class('txt-label-colon')->add(':&nbsp;');
                $content_right->add_div()->add_class('txt-label-text')->add($acontent);
            }
        }

        $content->add_div()->add_class('member-info-separate');

        $container_profil = $content->add_div()->add_class('row');
        $container_title = $container_profil->add_div()->add_class('col-sm-9');
        $container_button = $container_profil->add_div()->add_class('col-sm-3');
        $container_title->add_div()->add_class('txt-account-title')->add(clang::__('Shipping Address'));
        $container_button->add_div()->add_action()
                ->add_class('address-book btn-primary btn-imbuilding-light pull-right')
                ->set_label(clang::__('Change'))
                ->add_listener('click')
                ->add_handler('reload')
                ->set_target('page-account-container')
                ->set_url(curl::base() . 'member/account/address_book/'.$member_id);

        if ($data_address_shipping) {
            $arr_payment[] = array(
                'name' => clang::__('Full Name'),
                'content' => $data_address_shipping->name,
            );
            $arr_payment[] = array(
                'name' => clang::__('Phone'),
                'content' => $data_address_shipping->phone,
            );
            $arr_payment[] = array(
                'name' => clang::__('Address'),
                'content' => $data_address_shipping->address.'<br>'.$data_address_shipping->province . ' - ' . $data_address_shipping->city . ' - ' . $data_address_shipping->districts,
            );
            $arr_payment[] = array(
                'name' => clang::__('Postal Code'),
                'content' => $data_address_shipping->postal,
            );
            foreach ($arr_payment as $key => $value) {
                $aname = carr::get($value, 'name');
                $acontent = carr::get($value, 'content');
                $container_label = $content->add_div()->add_class('row');
                $container_left = $container_label->add_div()->add_class('col-sm-3');
                $container_right = $container_label->add_div()->add_class('col-sm-9');
                $container_left->add_div()->add_class('txt-account-label')->add($aname);
                $content_right = $container_right->add_div()->add_class('txt-account-label');
                $content_right->add_div()->add_class('txt-label-colon')->add(':&nbsp;');
                $content_right->add_div()->add_class('txt-label-text')->add($acontent);
            }
        }

        $content->add_div()->add_class('member-info-separate');

        $container_profil = $content->add_div()->add_class('row');
        $container_title = $container_profil->add_div()->add_class('col-sm-12');
        $container_title->add_div()->add_class('txt-account-title')->add(clang::__('Subscriber'));

        $product_checked = NULL;
        if ($data_member->is_subscribe == 1) {
            $product_checked = 'checked="checked"';
        }

        $txt_newsletter = 'Daftar sekarang untuk mendapatkan email promosi reguler kami yang dikemas dengan penawaran spesial dan eksklusif.';

        $container_newsletter = $container_profil->add_div()->add_class('col-md-12  txt-info-subtitle');
        $container_newsletter->add_div()->add($txt_newsletter);
        $container_newsletter->add_div()->add("<label class='checkbox-inline'><input type='checkbox' value='1' ".$product_checked." id='product_subscribe'><span>Ya, saya ingin menerima penawaran Murah & Promosi dari ".$org_name."</span></label>");

        $form = $container_newsletter->add_div()->add_class('container-btn-newsletter');
        $form->add_control('act', 'hidden')
                ->set_value('action');

        $action = $form->add_action()
                ->set_label(clang::__('Submit'))
                ->add_class('btn-imbuilding-light btn-primary')
                ->add_listener('click')
                ->add_handler('reload')
                ->set_target('page-account-container')
                ->add_param_input('product_subscribe')
                ->add_param_input('act')
                ->set_url(curl::base() . 'member/account/info');

        $js = ("
            $('.change-account').click(function(){
                set_active('change');
            });
            $('.address-book').click(function(){
                set_active('address_book');
            });
        ");

        $app->add_js($js);

        echo $app->render();
    }

    public function change() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();
        $member_id = $session->get('member_id');
        $org_id = $this->org_id();

        $request = array_merge($_POST, $_GET);
        $act = carr::get($request, 'act');

        // MEMBER
        $query = 'SELECT *
                        FROM member 
                        WHERE status > 0 AND member_id = ' . $db->escape($member_id);
        $data_member = cdbutils::get_row($query);

        $title = clang::__("Change Account");
        $err_code = $session->get_once('error_change_account');
        $message = $session->get_once('message_change_account');


        $row = $app->add_div()->add_class('row');
        $content = $row->add_div()->add_class('col-md-12');
        $content->add_div()->add_class('txt-info-title')->add($title);
        $content->add_div()->add_class('member-info-separate');
        
        if (!empty($message)) {
            $alert = 'success';
            if ($err_code > 0) {
                $alert = 'danger';
            }
            $alert_message = $content->add_div()->add_class('alert alert-' . $alert)->add('<a class="close" data-dismiss="alert">×</a>')->add($message);
        }


        $form = $content->add_form()
                ->set_enctype('multipart/form-data')
                ->set_action(curl::base() . 'member/account?page_account=change')
                ->add_class('form-62hallfamily form-imbuilding');
        
        $foto = $form->add_field();
        $foto_label = $foto->add_div()->add_class('col-md-3 txt-label')
                ->add(clang::__('Image Profile'));
        $file = $foto->add_div()->add_class('col-md-7')
                ->add('<input type="file" id="image_name" name="image_name" accept="image/*"/>');

        if (!empty($get_image)) {
            $remove = $form->add_div()->add_class('col-md-3')
                    ->add_action()
                    ->set_label('Hapus')
                    ->add_class('btn-danger btn-imbuilding')
                    ->set_link(curl::base() . 'member/account?act=delete');
        }

        $nama = $form->add_div()->add_class('margin-top-20')->add_field();
        $name_label = $nama->add_div()->add_class('col-md-3 txt-label')
                ->add(clang::__('Nama Lengkap') . '<red>*</red>');
        $name_input = $nama->add_div()->add_class('col-md-7');
        $name_input->add_control('name', 'text')
                ->set_name('name')
                ->add_class('col-md-7 col-xs-12')
                ->set_value($data_member->name);

        $birth = $form->add_div()->add_class('margin-top-20')->add_field();
        $birth_label = $birth->add_div()->add_class('col-md-3 txt-label')
                ->add(clang::__('Tanggal Lahir') . '<red>*</red>');
        $birth_input = $birth->add_div()->add_class('col-md-7');

        $tgl = substr($data_member->date_of_birth, 8, 2);
        $date = (!empty($tgl)) ? $tgl : date('d');
        $list_tgl = date::array_date();
        $birth_input->add_control('date_birth', 'select')
                ->set_name('date_birth')
                ->custom_css('margin-right', '5px')
                ->custom_css('width', '55px')
                ->custom_css('display', 'inline-block')
                ->set_list($list_tgl)
                ->set_value($date);

        $bln = substr($data_member->date_of_birth, 5, 2);
        $month = (!empty($bln)) ? $bln : date('m');
        $list_bln = date::array_month();
        $birth_input->add_control('month_birth', 'select')
                ->set_name('month_birth')
                ->custom_css('margin-right', '5px')
                ->custom_css('width', '130px')
                ->custom_css('display', 'inline-block')
                ->set_list($list_bln)
                ->set_value($month);

        $thn = substr($data_member->date_of_birth, 0, 4);
        $year = (!empty($thn)) ? $thn : date('Y');
        $list_thn = date::array_years(70);
        $birth_input->add_control('year_birth', 'select')
                ->set_name('year_birth')
                ->custom_css('width', '70px')
                ->custom_css('display', 'inline-block')
                ->set_list($list_thn)
                ->set_value($year);

        $email = $form->add_div()->add_class('margin-top-20')->add_field();
        $email_label = $email->add_div()->add_class('col-md-3 txt-label')
                ->add(clang::__('Email') . '<red>*</red>');
        $email_input = $email->add_div()->add_class('col-md-7');
        $email_input->add_control('mail', 'text')
                ->set_name('mail')
                ->add_class('col-md-7 col-xs-12')
                ->set_value($data_member->email);

        $handphone = $form->add_div()->add_class('margin-top-20')->add_field();
        $handphone_label = $handphone->add_div()->add_class('col-md-3 txt-label')
                ->add(clang::__('No Handphone') . '<red>*</red>');
        $handphone_input = $handphone->add_div()->add_class('col-md-7');

        $handphone_number = $handphone_kode = $handphone_input->add_div()->add_class('col-md-5 col-xs-9 padding-0');
        $handphone_number->add_control('phone', 'text')
                ->set_name('phone')
                ->set_value($data_member->phone);

        $gender = $form->add_div()->add_class('margin-top-20')->add_field();
        $gender_label = $gender->add_div()->add_class('col-md-3 txt-label')
                ->add(clang::__('Jenis Kelamin') . '<red>*</red>');

        $check_pria = "";
        $check_wanita = "";
        if ($data_member->gender == 'male') {
            $check_pria = 'checked';
        }
        if ($data_member->gender == 'female') {
            $check_wanita = 'checked';
        }
        $gender_input = $gender->add_div()->add_class('col-md-7')->custom_css('margin-top', '5px');
        $gender_input->add('<div class="col-md-2 col-xs-6"><input value="male" class="gender margin-left-15- " type="radio" name="gender" ' . $check_pria . '/> Pria</div>');
        $gender_input->add('<div class="col-md-3 col-xs-6"><input value="female" class="gender" type="radio" name="gender" ' . $check_wanita . '/> Wanita</div>');

        $form->add_div()->add_class('margin-top-30 font-size12')
                ->add('<red>*Wajib Diisi</red>');

        $form->add_control('act', 'hidden')
                ->set_value('act');

        $action = $form->add_div()->add_class('col-md-12 margin-top-20 margin-bottom-20')->add_field()
                ->add_action()
                ->set_label(clang::__('SAVE'))
                ->add_class('btn-imlasvegas-secondary save-change-account')

                ->set_submit(TRUE);

        $app->add_js(
                "
                    $('input').blur();
                    "
        );

        echo $app->render();
    }

    public function change_password() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();
        $member_id = $session->get('member_id');

        $request = array_merge($_POST, $_GET);

        $err_code = 0;
        $message = NULL;

        if (!empty($request['password'])) {
            $password = carr::get($request, 'password', NULL);
            $confirm_password = carr::get($request, 'confirm_password', NULL);
            $old_password = carr::get($request, 'old_password', NULL);
            $email = $session->get('email');

            if ($err_code == 0) {
                if (strlen($old_password) == 0) {
                    $err_code++;
                    $message = "Password lama kosong";
                }
            }
            if ($err_code == 0) {
                if (strlen($password) == 0) {
                    $err_code++;
                    $message = "Password baru kosong";
                }
            }
            if ($err_code == 0) {
                if (strlen($confirm_password) == 0) {
                    $err_code++;
                    $message = "Konfirmasi password baru kosong";
                }
            }
            // cek password lama
            $cek_password = cdbutils::get_value("select 1 from member where status>0 and password=" . $db->escape(md5($old_password)) . " and member_id=" . $db->escape($member_id));
            if ($err_code == 0) {
                if ($cek_password != 1) {
                    $err_code++;
                    $message = "Password Lama tidak sesuai";
                }
            }

            if ($err_code == 0) {
                if ($password != $confirm_password) {
                    $err_code++;
                    $message = "Konfirmasi password tidak sama";
                }
            }
            if ($err_code == 0) {
                if (strlen($password) == 0) {
                    $err_code++;
                    $message = "Password belum diisi";
                }
            }
            if ($err_code == 0) {
                if (strlen($confirm_password) == 0) {
                    $err_code++;
                    $message = "Ulangi Password belum diisi";
                }
            }

            if ($err_code == 0) {
                $db->begin();
                try {
                    if ($err_code == 0) {
                        $data = array(
                            'password' => md5($password),
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => $email,
                            'updatedip' => crequest::remote_address(),
                        );
                        $db->update('member', $data, array('member_id' => $member_id));

                        $message = "Pengubahan Password Barhasil";
                    }
                } catch (Exception $ex) {
                    $err_code++;
                    $message = clang::__('Error, Please call administrator!');
                    clog::write('DB Error page auth/register. ' . $ex->getMessage());
                    if (strlen($redirect) > 0) {
                        cmsg::add('error', clang::__('Error, Please call administrator!'));
                    }
                }
            }
            if ($err_code == 0) {
                $db->commit();
            } else {
                $db->rollback();
            }
            $session = Session::instance();
            $session->set('error_change_password', $err_code);
            $session->set('message_change_password', $message);
            curl::redirect(curl::base() . 'member/account?page_account=change_password');
        }

        $title = clang::__("Ubah Password");

        $title_container = $app->add_div()->add_class('txt-info-title')
                ->add($title);

        $content = $app->add_div()->add_class('col-md-12 padding-0 margin-top-20');


        $err_code = $session->get_once('error_change_password');
        $message = $session->get_once('message_change_password');
        if (!empty($message)) {
            $alert = 'success';
            if ($err_code > 0) {
                $alert = 'danger';
            }
            $alert_message = $content->add_div()->add_class('alert alert-' . $alert)->add('<a class="close" data-dismiss="alert">×</a>')->add($message);
        }

        $form = $content->add_form();
        $form->add_class('form-62hallfamily margin-0 form-imbuilding');

        $old_password = $form->add_field();
        $old_password_label = $old_password->add_div()->add_class('col-md-3 txt-label')
                ->add(clang::__('Old Password') . ' <red>*</red>');
        $old_password_input = $old_password->add_div()->add_class('col-md-7');
        $old_password_input->add_control('old_password', 'password')
                ->set_name('old_password')
                ->add_validation('required')
                ->add_class('col-md-7 col-xs-12');

        $password = $form->add_div()->add_class('margin-top-20')->add_field();
        $password_label = $password->add_div()->add_class('col-md-3 txt-label')
                ->add(clang::__('New Password') . ' <red>*</red>');
        $password_input = $password->add_div()->add_class('col-md-7');
        $password_input->add_control('password', 'password')
                ->set_name('password')
                ->add_validation('required')
                ->add_class('col-md-7 col-xs-12');

        $confirm_password = $form->add_div()->add_class('margin-top-20')->add_field();
        $confirm_password_label = $confirm_password->add_div()->add_class('col-md-3 txt-label')
                ->add(clang::__('Confirm Password') . ' <red>*</red>');
        $confirm_password_input = $confirm_password->add_div()->add_class('col-md-7');
        $confirm_password_input->add_control('confirm_password', 'password')
                ->set_name('confirm_password')
                ->add_validation('required')
                ->add_class('col-md-7 col-xs-12');

        $action = $form->add_div()->add_class('col-md-12 margin-top-20')->add_field()
                ->add_action()
                ->set_label(clang::__('CHANGE PASSWORD'))
                ->add_class('btn-imlasvegas-secondary')
                ->set_submit(true);


        $form->set_ajax_submit(true)->set_ajax_submit_target('page-account-container')->set_action(curl::base() . 'member/account/change_password');

        echo $app->render();
    }

    public function address_book() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();
        $member_id = $session->get('member_id');

        $request = array_merge($_POST, $_GET);

        $err_code = 0;

        if (!isset($message)) {
            $message = NULL;
        }

        $title = clang::__("Buku Alamat");

        $title_container = $app->add_div()->add_class('txt-info-title')
                ->add($title);

        $content = $app->add_div()->add_class('col-md-12 margin-top-20 padding-0');

        $message = $session->get_once('message_address_book');
        $err_code = $session->get_once('err_code_address_book');
        if (!empty($message)) {
            $alert = 'success';
            if ($err_code > 0) {
                $alert = 'danger';
            }
            $alert_message = $content->add_div()->add_class('alert alert-' . $alert)->add($message);
        }

        $alamat_pembayaran_container = $content->add_div()->add_class('col-md-12 margin-bottom-30');
        $alamat_pembayaran_header = $alamat_pembayaran_container->add_div()->add_class('col-md-12 container padding-0 margin-top-10 margin-bottom-10 border-bottom');
        $alamat_pembayaran_title = $alamat_pembayaran_header->add_div()->add_class('col-md-8 padding-0 font-size24 normal')
                ->add(clang::__('Alamat Pembayaran'));
        $alamat_pembayaran_add = $alamat_pembayaran_header->add_div()->add_class('col-md-4 padding-0')
                ->add_action()
                ->add_class('btn-modal font-size16 padding-0 font-red pull-right')
                ->set_label(clang::__('Tambah Alamat Baru'))
                ->add_listener('click')
                ->add_handler('dialog')
                ->set_title(clang::__('Tambah Alamat Baru'))
                ->set_url(curl::base() . 'member/account/dialog_address/' . $this->type_payment);

        // MEMBER ADDRESS
        $query = 'SELECT 
                            ma.*, p.name as province, c.name as city, d.name as districts
                        FROM 
                            member_address ma
                        LEFT JOIN 
                            province p
                        ON
                            p.province_id = ma.province_id
                        LEFT JOIN 
                            city c
                        ON
                            c.city_id = ma.city_id
                        LEFT JOIN 
                            districts d
                        ON
                            d.districts_id = ma.districts_id
                        WHERE 
                            ma.status > 0 
                            AND member_id = ' . $db->escape($member_id) . '
                            AND type = ' . $db->escape($this->type_payment) . '
                        ORDER BY
                            is_active DESC
                    ';
        $data_member_address = $db->query($query);
            
        $alamat_pembayaran_content = $alamat_pembayaran_container->add_div()->add_class('col-md-12 container padding-0');
        if (!empty($data_member_address)) {
            foreach ($data_member_address as $index => $row_member_address) {
                $class_border = NULL;
                if ($index !== 0) {
                    $class_border = 'border-top';
                }

                $class_item = 'font-gray-dark';
                if ($row_member_address->is_active) {
                    $class_item = 'font-black bold';
                }

                $alamat_pembayaran = $alamat_pembayaran_content->add_div()->add_class('col-md-12 container padding-0 ' . $class_border);
                $alamat_pembayaran_item = $alamat_pembayaran->add_div()->add_class('col-md-9 padding-0 margin-top-10 margin-bottom-10');
                $item_container = $alamat_pembayaran_item->add_div()->add_class('col-md-12 padding-0');
                $item = $item_container->add_div()->add_class($class_item);
                $item->add($row_member_address->name);
                $item->add_br();
                $item->add($row_member_address->address);
                $item->add_br();
                $item->add($row_member_address->province . ' - ' . $row_member_address->city . ' - ' . $row_member_address->districts);

                $alamat_pembayaran_action = $alamat_pembayaran->add_div()->add_class('col-md-3 padding-0 margin-top-10');
                $action_container = $alamat_pembayaran_action->add_div()->add_class('padding-0');

                if ($row_member_address->is_active == 0) {
                    $action_container->add_div()->add_class('col-md-12 container padding-0')
                            ->add_action()
                            ->set_label('<b>' . clang::__('Jadikan Alamat Utama') . '</b>')
                            ->add_class('font-size12 bold padding-0 font-black pull-right member-address-default')
                            ->set_link(curl::base() . 'member/account/set_main_address/' . $this->type_payment . '/' . $row_member_address->member_address_id);
                }
                
                $div_action_edit = $action_container->add_div()->add_class('col-md-6 col-xs-6 container')
                        ->add_action()
                        ->add_class('btn-modal btn-62hallfamily-small bg-red border-3-red font-size16 padding-0 pull-right margin-bottom-10')
                        ->set_label(clang::__('Ubah'))
                        ->custom_css('width', '70px')
                        ->add_listener('click')
                        ->add_handler('dialog')
                        ->set_url(curl::base() . 'member/account/dialog_address/' . $this->type_payment . '/' . $row_member_address->member_address_id)
                        ->set_title('Ubah Alamat');

                $action_container->add_div()->add_class('col-md-6 padding-0')
                        ->add_action()
                        ->add_class('btn-62hallfamily-small bg-red border-3-red font-size16 padding-0 pull-right margin-bottom-10')
                        ->set_label(clang::__('Hapus'))
                        ->custom_css('width', '70px')
                        ->add_listener('click')
                        ->add_handler('reload')
                        ->set_target('page-account-container')
                        ->set_url(curl::base() . 'member/account/delete_address_book/' . $row_member_address->member_address_id);
            }
        }

        $alamat_pengiriman_container = $content->add_div()->add_class('col-md-12 margin-bottom-30');
        $alamat_pengiriman_header = $alamat_pengiriman_container->add_div()->add_class('col-md-12 container padding-0 margin-top-10 margin-bottom-10 border-bottom');
        $alamat_pengiriman_title = $alamat_pengiriman_header->add_div()->add_class('col-md-8 padding-0 font-size24 normal')
                ->add(clang::__('Alamat Pengiriman'));

        $alamat_pengiriman_add = $alamat_pengiriman_header->add_div()->add_class('col-md-4 padding-0')
                ->add_action()
                ->add_class('btn-modal font-size16 padding-0 font-red pull-right')
                ->set_label(clang::__('Tambah Alamat Baru'))
                ->add_listener('click')
                ->add_handler('dialog')
                ->set_title('Tambah Alamat Baru')
                ->set_url(curl::base() . 'member/account/dialog_address/' . $this->type_shipping);
   

        // MEMBER ADDRESS
        $query = 'SELECT 
                            ma.*, p.name as province, c.name as city, d.name as districts
                        FROM 
                            member_address ma
                        LEFT JOIN 
                            province p
                        ON
                            p.province_id = ma.province_id
                        LEFT JOIN 
                            city c
                        ON
                            c.city_id = ma.city_id
                        LEFT JOIN 
                            districts d
                        ON
                            d.districts_id = ma.districts_id
                        WHERE 
                            ma.status > 0 
                            AND member_id = ' . $db->escape($member_id) . '
                            AND type = ' . $db->escape($this->type_shipping) . '
                        ORDER BY
                            is_active DESC
                    ';
      
        $data_member_address = $db->query($query);
        
        $alamat_pengiriman_content = $alamat_pengiriman_container->add_div()->add_class('col-md-12 container padding-0');
        if (!empty($data_member_address)) {
            foreach ($data_member_address as $index => $row_member_address) {
                $class_border = NULL;
                if ($index !== 0) {
                    $class_border = 'border-top';
                }

                $class_item = 'font-gray-dark';
                if ($row_member_address->is_active) {
                    $class_item = 'font-black bold';
                }

                $alamat_pengiriman = $alamat_pengiriman_content->add_div()->add_class('col-md-12 container padding-0 ' . $class_border);

                $alamat_pengiriman_item = $alamat_pengiriman->add_div()->add_class('col-md-9 padding-0 margin-top-10 margin-bottom-10');
                $item_container = $alamat_pengiriman_item->add_div()->add_class('col-md-12 padding-0');
                $item = $item_container->add_div()->add_class($class_item);
                $item->add($row_member_address->name);
                $item->add_br();
                $item->add($row_member_address->address);
                $item->add_br();
                $item->add($row_member_address->province . ' - ' . $row_member_address->city . ' - ' . $row_member_address->districts);

                $alamat_pengiriman_action = $alamat_pengiriman->add_div()->add_class('col-md-3 padding-0 margin-top-10');
                $action_container = $alamat_pengiriman_action->add_div()->add_class('col-md-12 padding-0');

                if ($row_member_address->is_active == 0) {
                    $action_container->add_div()->add_class('col-md-12 container padding-0')
                            ->add_action()
                            ->set_label('<b>' . clang::__('Jadikan Alamat Utama') . '</b>')
                            ->add_class('font-size12 bold padding-0 font-black pull-right member-address-default')
                            ->set_link(curl::base() . 'member/account/set_main_address/' . $this->type_shipping . '/' . $row_member_address->member_address_id);
                }
                
                $action_container->add_div()->add_class('col-md-6 col-xs-6 container')
                        ->add_action()
                        ->add_class('btn-modal btn-62hallfamily-small bg-red border-3-red font-size16 padding-0 pull-right margin-bottom-10')
                        ->set_label(clang::__('Ubah'))
                        ->custom_css('width', '70px')
                        ->add_listener('click')
                        ->add_handler('dialog')
                        ->set_url(curl::base() . 'member/account/dialog_address/'. $this->type_shipping .'/' . $row_member_address->member_address_id)
                        ->set_title('Ubah Alamat');
                
                        
                $action_container->add_div()->add_class('col-md-6 padding-0')
                        ->add_action()
                        ->add_class('btn-62hallfamily-small bg-red border-3-red font-size16 padding-0 pull-right margin-bottom-10')
                        ->set_label(clang::__('Hapus'))
                        ->custom_css('width', '70px')
                        ->add_listener('click')
                        ->add_handler('reload')
                        ->set_target('page-account-container')
                        ->set_url(curl::base() . 'member/account/delete_address_book/' . $row_member_address->member_address_id);
            }
        }

        echo $app->render();
    }

    public function dialog_address($type, $member_address_id=NULL) {
        $app = CApp::instance();
        $session = Session::instance();
        $member_id = $session->get('member_id');
        $db = CDatabase::instance();

        $element_address = $app->add_element('62hall-modal-address', 'address')
                ->set_type($type);
        
              
        if (strlen($member_address_id) > 0) {
            
            $query = 'SELECT 
                            ma.*, p.name as province, c.name as city, d.name as districts
                        FROM 
                            member_address ma
                        LEFT JOIN 
                            province p
                        ON
                            p.province_id = ma.province_id
                        LEFT JOIN 
                            city c
                        ON
                            c.city_id = ma.city_id
                        LEFT JOIN 
                            districts d
                        ON
                            d.districts_id = ma.districts_id
                        WHERE 
                            ma.status > 0 
                            AND member_address_id = ' . $db->escape($member_address_id) . '
                    ';
            
            $row_member_address = cdbutils::get_row($query);
            
            $element_address->set_title(clang::__('Edit Alamat'))
                    ->set_key($row_member_address->member_address_id)
                    ->set_type($type)
                    ->set_name($row_member_address->name)
                    ->set_address($row_member_address->address)
                    ->set_email($row_member_address->email)
                    ->set_phone($row_member_address->phone)
                    ->set_province($row_member_address->province_id)
                    ->set_city($row_member_address->city_id)
                    ->set_districts($row_member_address->districts_id)
                    ->set_postal($row_member_address->postal);
        }

        $element_address->set_submit_url(curl::base() . 'member/account/save_address_book');

        echo $app->render();
    }

    public function save_address_book() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();

        $member_id = $session->get('member_id');
        $email = $session->get('email');

        $request = array_merge($_POST, $_GET);

        $err_code = 0;
        $message = NULL;

        $name_validate = array(
            'name' => 'Nama Lengkap',
            'email' => 'Email',
            'phone' => 'Nomor Handphone',
            'address' => 'Alamat',
            'province' => 'Provinsi',
            'city' => 'Kota',
            'districts' => 'Kecamatan',
            'postal' => 'Kode Pos',
        );
        foreach ($name_validate as $validate => $label) {
            if (strlen(carr::get($request, $validate)) == 0) {
                $err_code++;
                $message = $label . ' belum diisi!';
            }
        }

        $key = carr::get($request, 'key', NULL);
        $type = carr::get($request, 'type', NULL);
        $name = carr::get($request, 'name', NULL);
        $email = carr::get($request, 'email', NULL);
        $phone = carr::get($request, 'phone', NULL);
        $address = carr::get($request, 'address', NULL);
        $province = carr::get($request, 'province', NULL);
        $city = carr::get($request, 'city', NULL);
        $districts = carr::get($request, 'districts', NULL);
        $postal = carr::get($request, 'postal', NULL);

        if (strlen($email) > 0) {
            if (!cvalid::email($email)) {
                $err_code++;
                $message = 'Format email salah';
            }
        }

        if ($err_code == 0) {
            $db->begin();
            try {
                if ($err_code == 0) {
                    if (empty($key)) {
                        $is_active = '0';

                        $total_address = cdbutils::get_value("select count(*) from member_address where status>0 and type=" . $db->escape($type) . " and member_id=" . $db->escape($member_id));
                        if ($total_address == 0) {
                            $is_active = '1';
                        }

                        $data = array(
                            'member_id' => $member_id,
                            'type' => $type,
                            'name' => $name,
                            'email' => $email,
                            'phone' => $phone,
                            'address' => $address,
                            'postal' => $postal,
                            'province_id' => $province,
                            'city_id' => $city,
                            'districts_id' => $districts,
                            'is_active' => $is_active,
                            'created' => date('Y-m-d H:i:s'),
                            'createdby' => $email,
                            'createdip' => crequest::remote_address(),
                        );
                        $db->insert('member_address', $data);
                        $message = "Penambahan Alamat Barhasil";
                    } else {
                        $data = array(
                            'type' => $type,
                            'name' => $name,
                            'email' => $email,
                            'phone' => $phone,
                            'address' => $address,
                            'postal' => $postal,
                            'province_id' => $province,
                            'city_id' => $city,
                            'districts_id' => $districts,
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => $email,
                            'updatedip' => crequest::remote_address(),
                        );
                        $db->update('member_address', $data, array('member_address_id' => $key));
                    }
                }
            } catch (Exception $ex) {
                $err_code++;
                $message = clang::__('Error, Please call administrator!');
                clog::write('DB Error page addressbook/save. ' . $ex->getMessage());
                $db->rollback();
            }
        }
        if ($err_code == 0) {
            $db->commit();
            $message = "Pengubahan Alamat Barhasil";
        } else {
            $db->rollback();
        }

        $session->set('message_address_book', $message);
        $session->set('err_code_address_book', $err_code);

        curl::redirect(curl::base() . 'member/account?page_account=address_book');
    }

    public function delete_address_book($member_address_id) {
        $db = CDatabase::instance();
        $session = Session::instance();
        $member_id = $session->get('member_id');
        $email = $session->get('email');

        $err_code = 0;
        $message = NULL;

        $db->begin();
        try {
            if ($err_code == 0) {
                $data = array(
                    'status' => 0,
                    'updated' => date('Y-m-d H:i:s'),
                    'updatedby' => $email,
                    'updatedip' => crequest::remote_address(),
                );
                $db->update('member_address', $data, array('member_address_id' => $member_address_id));
            }
        } catch (Exception $ex) {
            $err_code++;
            $message = clang::__('Error, Please call administrator!');
            clog::write('DB Error page addressbook/delete. ' . $ex->getMessage());
            $db->rollback();
        }
        if ($err_code == 0) {
            $db->commit();
            $message = "Penghapusan Alamat Berhasil";
        } else {
            $db->rollback();
        }

        $session->set('message_address_book', $message);
        $session->set('err_code_address_book', $err_code);

        curl::redirect(curl::base() . 'member/account?page_account=address_book');
    }

    public function set_main_address($type, $member_address_id) {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();
        $member_id = $session->get('member_id');

        $err_code = 0;

        try {
            $query = "
                        UPDATE 
                            member_address
                        SET 
                            is_active = 0
                        WHERE
                            status > 0
                            AND type = " . $db->escape($type) . "
                            AND member_id = " . $db->escape($member_id) . "
                        ";
            echo $query;
            $db->query($query);

            $query = "
                        UPDATE 
                            member_address
                        SET 
                            is_active = 1
                        WHERE
                            status > 0
                            AND type = " . $db->escape($type) . "
                            AND member_address_id = " . $db->escape($member_address_id) . "
                        ";

            $db->query($query);
        } catch (Exception $ex) {
            $err_code++;
            $message = clang::__('Error, Please call administrator!');
            clog::write('DB Error page addressbook/set_main_address. ' . $ex->getMessage());
            $db->rollback();
        }

        if ($err_code == 0) {
            $db->commit();
            $message = "Penghapusan Alamat Berhasil";
        } else {
            $db->rollback();
        }

        curl::redirect(curl::base() . 'member/account?page_account=address_book');
    }

    public function order_me() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();
        $member_id = $session->get('member_id');

        // MEMBER
        $query = 'SELECT *
                        FROM member 
                        WHERE status > 0 AND member_id = ' . $db->escape($member_id);
        $data_member = cdbutils::get_row($query);

        $title = clang::__("Pesanan Saya");

        $title_container = $app->add_div()->add_class('txt-info-title')
                ->add($title);

        $content = $app->add_div()->add_class('col-md-12 margin-top-20 padding-0 container-table-book');

        $data_transaction = transaction::get_transaction('member_id', $member_id);

        // Table Pesanan ukuran xs
        
        $content->add('<table class="table-62hallfamily-gray visible-xs">');
        $content->add('<tr>');
        $content->add('<th width="23px">');
        $content->add('#');
        $content->add('</th>');
        $content->add('<th width="125px">');
        $content->add('Tanggal Pembayaran');
        $content->add('</th>');
        $content->add('<th width="90px">');
        $content->add('Total');
        $content->add('</th>');
  
        if (count($data_transaction) > 0) {
            $index = 1;
            foreach ($data_transaction as $transaction) {
                $content->add('<tr>');
                $content->add('<td>');
                $content->add($index);
                $content->add('</td>');
				$content->add('<td>');
				$content->add('Tanggal :<br>');
                $content->add(ctransform::format_date($transaction->date).'<br><br>');
				$content->add('Kode :<br>');
                $content->add('<b>' . $transaction->booking_code . '</b><br><br>');
                $data_product = transaction::get_transaction_detail('transaction_id', $transaction->transaction_id);

                $sub_total = 0;
                if (count($data_product) > 0) {
                    foreach ($data_product as $product) {
                        $content->add($product->name . '<br>');
                        $content->add('Rp. ' . ctransform::format_currency($product->channel_sell_price) . ' /item<br>');

                        $sub_total = $product->qty * $product->channel_sell_price;

                        $content->add('Sub Total: Rp.' . ctransform::format_currency($sub_total) . '<br><br>');
                    }
                }

                $content->add('</td>');
                $content->add('<td>');

                $data_payment = transaction::get_transaction_payment('transaction_id', $transaction->transaction_id);

                $total_item = '-';
                if ($data_payment != null) {
                    $total_item = ctransform::format_currency($data_payment->total_item);
                }

                $content->add('<font color="#fe0100">Rp. ' . $total_item . '</font><br><br><br><br>');

                $content->add('<div id="' . $transaction->transaction_id . '" class="detail btn-62hallfamily-small bg-blue font-white bold border-3-blue link btn-primary btn-imbuilding-light" style="text-align:center;width:80px;margin-top: 0px">Detail</div>');

                $action = $content->add_action()
                        ->set_label('Status')
                        ->add_class('status btn-62hallfamily-small bg-green font-white bold border-3-green link btn-primary btn-imbuilding-light')
                        ->custom_css('width', '80px')
                        ->add_listener('click')
                        ->add_handler('dialog')
                        ->set_title('Status Pemesanan')
                        ->set_url(curl::base() . 'member/account/status_order_me/' . $transaction->transaction_id);

                $content->add('</td>');

                $content->add('</tr>');

                $index++;
            }
        }
      // End Table pesanan ukuran xs  
       
        
        $content->add('<table class="table-62hallfamily-gray hidden-xs">');
        $content->add('<tr>');
        $content->add('<th>');
        $content->add('#');
        $content->add('</th>');
        $content->add('<th width="120px">');
        $content->add('KODE PESANAN');
        $content->add('</th>');
        $content->add('<th width="120px">');
        $content->add('TANGGAL TRANSAKSI');
        $content->add('</th>');
        $content->add('<th>');
        $content->add('DESKRIPSI');
        $content->add('</th>');
        $content->add('<th>');
        $content->add('TOTAL');
        $content->add('</th>');
        $content->add('<th>');
        $content->add('TINDAKAN');
        $content->add('</th>');

        if (count($data_transaction) > 0) {
            $index = 1;
            foreach ($data_transaction as $transaction) {
                $content->add('<tr>');
                $content->add('<td>');
                $content->add($index);
                $content->add('</td>');
                $content->add('<td>');
                $content->add('<b>' . $transaction->booking_code . '</b>');
                $content->add('</td>');
                $content->add('<td>');
                $content->add(ctransform::format_date($transaction->date));
                $content->add('</td>');
                $content->add('<td>');

                $data_product = transaction::get_transaction_detail('transaction_id', $transaction->transaction_id);

                $sub_total = 0;
                if (count($data_product) > 0) {
                    foreach ($data_product as $product) {
                        $content->add($product->name . '<br>');
                        $content->add('Rp. ' . ctransform::format_currency($product->channel_sell_price) . ' /item<br>');

                        $sub_total = $product->qty * $product->channel_sell_price;

                        $content->add('Sub Total: Rp.' . ctransform::format_currency($sub_total) . '<br><br>');
                    }
                }

                $content->add('</td>');
                $content->add('<td>');

                $data_payment = transaction::get_transaction_payment('transaction_id', $transaction->transaction_id);

                $total_item = '-';
                if ($data_payment != null) {
                    $total_item = ctransform::format_currency($data_payment->total_item);
                }

                $content->add('Rp. ' . $total_item);

                $content->add('</td>');

                $content->add('<td>');

                $content->add('<div id="' . $transaction->transaction_id . '" class="detail btn-62hallfamily-small bg-blue font-white bold border-3-blue link btn-primary btn-imbuilding-light" style="text-align:center;width:80px">Detail</div>');

                $action = $content->add_action()
                        ->set_label('Status')
                        ->add_class('status btn-62hallfamily-small bg-green font-white bold border-3-green link btn-primary btn-imbuilding-light')
                        ->custom_css('width', '80px')
                        ->add_listener('click')
                        ->add_handler('dialog')
                        ->set_title('Status Pemesanan')
                        ->set_url(curl::base() . 'member/account/status_order_me/' . $transaction->transaction_id);

                $content->add('</td>');

                $content->add('</tr>');

                $index++;
            }
        }

        
        $app->add_js("
                
                $('.detail').click(function () {
                    var id = $(this).attr('id');
                    $.cresenity.reload('page-account-container', '" . curl::base() . 'member/account/detail_order_me/' . "'+id);
                });
                    ");

        $content->add('</table>');

        echo $app->render();
    }

    public function detail_order_me($transaction_id) {
        $app = CApp::instance();
        $db = CDatabase::instance();

        $title = clang::__("Rincian Pemesanan");

        $title_container = $app->add_div()->add_class('col-md-12 padding-0 font-size24 bold  border-bottom-gray')
                ->add($title);

        $content = $app->add_div()->add_class('col-md-12 padding-0');


        $data_detail_product = transaction::get_transaction_detail_transaction('t.transaction_id', $transaction_id);
        
        
        // Detai order tampilan xs
        
        $content->add('<table class="table-62hallfamily tabel-small-show margin-top-20 visible-xs">');
        $content->add('<tr>');
        $content->add('<th width="160px">');
        $content->add('Tanggal Pembayaran');
        $content->add('</th>');
        $content->add('<th width="80px">');
        $content->add('Jumlah');
        $content->add('</th>');
        $content->add('</tr>');
       
        $total_payment = 0;
        if (count($data_detail_product) > 0) {
            $index = 1;
            foreach ($data_detail_product as $product) {
                $content->add('<tr>');

                $content->add('<td>');
                $content->add('Tanggal: <br>');
                $content->add(ctransform::format_date($product->date));
                $content->add('<br><br>Kode: <br>');
                $content->add('<b>' . $product->booking_code . '</b><br><br>');
                $content->add('<img style="width:85px; height:auto;" src="' . $product->file_path . '"><br><br>');
                $content->add($product->name . '<br>');
                $content->add('Rp. ' . ctransform::format_currency($product->channel_sell_price) . ' /item<br>');
   
                $content->add('</td>');
                $content->add('<td class="text-right">');
                $content->add($product->qty.'<br><br><br><br>');
                $sub_total = $product->qty * $product->channel_sell_price;
                $total_payment += $sub_total;
                $content->add('<font color="#953735"><b>Rp.'.ctransform::format_currency($sub_total));

                $content->add('</b></font></td>');

                $content->add('</tr>');

                $index++;
            }
        }

        $data_transaction = cdbutils::get_row('select * from transaction where transaction_id = ' . $db->escape($transaction_id));
        $data_payment = transaction::get_transaction_payment('transaction_id', $transaction_id);

        $content->add('<tr>');
        $content->add('<td class="border-none text-right">');
        $content->add('Jumlah Pembayaran');
        $content->add('</td>');
        $content->add('<td class="border-none text-right">');
        // $total_item = "-";
        // if ($data_payment != null) {
        //     $total_item = ctransform::format_currency($data_payment->total_item);
        // }

        $content->add('<font color="#953735"><b>Rp. ' . ctransform::format_currency($total_payment));
        $content->add('</b></font></td>');
        $content->add('</tr>');
        $content->add('<tr>');
        $content->add('<td class="border-none text-right">');
        $content->add('Biaya Pengiriman');
        $content->add('</td>');
        $content->add('<td class="border-none text-right">');
        $content->add('Rp. ' . ctransform::format_currency($data_transaction->total_shipping));
        $content->add('</td>');
        $content->add('</tr>');
         $content->add('<tr class=" border-gray-dark-2 bg-gray-dark-young padding-top-10 padding-bottom-10 margin-bottom-20 margin-top-20">');
        $content->add('<td colspan="1" class="border-none text-right font-size16 bold">');
        $content->add('Total Yang Harus Dibayar');
        $content->add('</td>');
        
        $total_harga = $data_transaction->total_shipping;
        if ($data_payment != null) {
            $total_harga += $total_payment;
        }
        
        $content->add('<td colspan="1" class="border-none text-right font-size14 bold"  style="  color: var(--red-maroon);">');
        $content->add('Rp. ' . ctransform::format_currency($total_harga));
        $content->add('</td>');
        $content->add('</tr>');
        $content->add('</table>');
        
        //End detail order ukuran xs

         // Detai order tampilan small
        
        $content->add('<table class="table-62hallfamily margin-top-20 tabel-big-show hidden-xs">');
        $content->add('<tr>');
        $content->add('<th width="120px">');
        $content->add('Tanggal Pembayaran');
        $content->add('</th>');
        $content->add('<th width="180px">');
        $content->add('Deskripsi Produk');
        $content->add('</th>');
        $content->add('<th  width="80px">');
        $content->add('Jumlah');
        $content->add('</th>');
        $content->add('<th>');
        $content->add('Harga');
        $content->add('</th>');

        $total_payment = 0;
        if (count($data_detail_product) > 0) {
            $index = 1;
            foreach ($data_detail_product as $product) {
                $content->add('<tr>');
                $content->add('<td>');
                $content->add('Tanggal :<br>'.$product->date.'<br><br>');
                $content->add('Kode : <br><b>' . $product->booking_code . '</b>');
                $content->add('</td>');
                $content->add('<td>');
                $content->add('<img style="width:85px; height:auto;" src="' . $product->file_path . '"><br><br>');
                $content->add($product->name . '<br>');
                $content->add('Rp. ' . ctransform::format_currency($product->channel_sell_price) . ' /item<br>');
                $content->add('</td>');
                $content->add('<td>');
                $content->add($product->qty);
                $content->add('</td>');
                $content->add('<td class="text-right">');

                $sub_total = $product->qty * $product->channel_sell_price;
                $total_payment += $sub_total;
                $content->add(ctransform::format_currency($sub_total));

                $content->add('</td>');

                $content->add('</tr>');

                $index++;
            }
        }

        $data_transaction = cdbutils::get_row('select * from transaction where transaction_id = ' . $db->escape($transaction_id));
        $data_payment = transaction::get_transaction_payment('transaction_id', $transaction_id);

        $content->add('<tr>');
        $content->add('<td colspan="2" class="border-none text-right">');
        $content->add('Jumlah Pembayaran');
        $content->add('</td>');
        $content->add('<td colspan="2" class="border-none text-right">');
        // $total_item = "-";
        // if ($data_payment != null) {
        //     $total_item = ctransform::format_currency($data_payment->total_item);
        // }

        $content->add('Rp. ' . ctransform::format_currency($total_payment));
        $content->add('</td>');
        $content->add('</tr>');
        $content->add('<tr>');
        $content->add('<td colspan="2" class="border-none text-right">');
        $content->add('Biaya Pengiriman');
        $content->add('</td>');
        $content->add('<td colspan="2" class="border-none text-right">');
        $content->add('Rp. ' . ctransform::format_currency($data_transaction->total_shipping));
        $content->add('</td>');
        $content->add('<tr class=" border-gray-dark-2 bg-gray-dark-young padding-top-10 padding-bottom-10 margin-bottom-20 margin-top-20">');
        $content->add('<td colspan="2" class="border-none text-right font-size16 bold">');
        $content->add('Total Yang Harus Dibayar');
        $content->add('</td>');
        
        $total_harga = $data_transaction->total_shipping;
        if ($data_payment != null) {
            $total_harga += $total_payment;
        }
        
        $content->add('<td colspan="2" class="border-none text-right font-size16 bold"  style="  color: var(--red-maroon);">');
        $content->add('Rp. ' . ctransform::format_currency($total_harga));
        $content->add('</td>');
        $content->add('</tr>');
        $content->add('</table><br>');
        
//        $footer_container = $app->add_div()->add_class('col-md-12 border-gray-dark-2 bg-gray-dark-young padding-top-10 padding-bottom-10 margin-bottom-20 margin-top-20');
//        $footer_container_row = $footer_container->add_div()->add_class('row');
//        $total_title = $footer_container_row->add_div()->add_class('col-xs-8 col-md-7');
//        $total_title->add('<div class="font-size13 bold" style=" !important;">Total Yang Harus Dibayar</div>');
//
//        $total_harga = $data_transaction->total_shipping;
//        if ($data_payment != null) {
//            $total_harga += $total_payment;
//        }
//
//        $total_price = $footer_container_row->add_div()->add_class('col-xs-4 col-md-5 text-right');
//        $total_price->add('<div class="font-size12 bold" style="  color: var(--red-maroon);">Rp. ' . ctransform::format_currency($total_harga) . '</div>');

        $address_row = $app->add_div()->add_class('row');
        $address = $address_row->add_div()->add_class('col-md-12');
        $title_address = $address->add_div()->add_class('col-md-12 padding-0 ');
        $title_payment = $title_address->add_div()->add_class('col-md-4 padding-0 font-size20 bold ')
                ->add(clang::__('Alamat Pembayaran'));
       

        $data_contact = transaction::get_transaction_contact('t.transaction_id', $transaction_id);

        $address_container = $title_address->add_div()->add_class('col-md-12 row margin-top-10 margin-bottom-20');

        $billing_address = $address_container->add_div()->add_class('col-md-4 padding-0 padding-bottom-10 border-bottom-gray');
        $name = $billing_address->add_div()->add('Nama : '.$data_contact->billing_first_name . ' ' . $data_contact->billing_last_name);
        $email = $billing_address->add_div()->add('Email : '.$data_contact->billing_email);
        $phone = $billing_address->add_div()->add('Nomer Telepon : '.$data_contact->billing_phone);
        $address = $billing_address->add_div()->add('Alamat : '.$data_contact->billing_address);
        $province = $billing_address->add_div()->add('Provinsi : '.$data_contact->billing_province);
        $city = $billing_address->add_div()->add('Kota : '.$data_contact->billing_city);
        $district = $billing_address->add_div()->add('Kecamatan : '.$data_contact->billing_district.'<br><br>');

         $title_shipping = $title_address->add_div()->add_class('col-md-4 padding-0 font-size20 bold ')
                ->add(clang::__('Alamat Pengiriman'));
         
        $shipping_container = $title_address->add_div()->add_class('col-md-12 row margin-top-10 ');
   
        $shipping_address = $shipping_container->add_div()->add_class('col-md-4 padding-0 margin-bottom-20 border-bottom-gray');
        $name = $shipping_address->add_div()->add($data_contact->shipping_first_name . ' ' . $data_contact->shipping_last_name);
        $email = $shipping_address->add_div()->add($data_contact->shipping_email);
        $phone = $shipping_address->add_div()->add($data_contact->shipping_phone);
        $address = $shipping_address->add_div()->add($data_contact->shipping_address);
        $province = $shipping_address->add_div()->add($data_contact->shipping_province);
        $city = $shipping_address->add_div()->add($data_contact->shipping_city);
        $district = $shipping_address->add_div()->add($data_contact->shipping_district.'<br><br>');

          $title_metode = $title_address->add_div()->add_class('col-md-4 padding-0 font-size20 bold')
                ->add(clang::__('Metode Pembayaran'));
          
        $payment_metode = $title_address->add_div()->add_class('col-md-12 row margin-top-10 margin-bottom-20');

        $payment_type = '-';
        $payment_status = '-';

        if ($data_payment != null) {

            $payment_type = str_replace('_', ' ', $data_payment->payment_type);
            $payment_status = $data_payment->payment_status;
            if($data_payment->payment_type=='bank_transfer') {
                $payment_type .= '(' . $data_payment->bank . '-' . $data_payment->account_number . ')';
            }
        }
        
        $type_payment = $payment_metode->add_div()->add(ucwords($payment_type));


        $type_payment = $payment_metode->add_div()->add('Status : ' . ucfirst($payment_status));

        echo $app->render();
    }

    public function status_order_me($transaction_id) {
        $app = CApp::instance();
        $db = CDatabase::instance();

        $row = $app->add_div()->add_class('row');
        $content = $row->add_div()->add_class('col-md-12 margin-bottom-20');
        
        // Table Status Order Me xs
        
        $content->add('<table class="table-62hallfamily-gray tabel-small-show" style="font-size:12px">');
        $content->add('<tr>');
        
        $content->add('<th>');
        $content->add('Tanggal Pembayaran');
        $content->add('</th>');
     
        $content->add('<th>');
        $content->add('Total');
        $content->add('</th>');
        $content->add('</tr>');
 

        $data_transaction = transaction::get_transaction('transaction_id', $transaction_id);
        $data_transaction_detail = transaction::get_transaction_detail('transaction_id', $transaction_id);

        $query = "
                    select 
                        *
                    from 
                        transaction_payment
                    where 
                    status > 0
                    and transaction_id = " . $db->escape($transaction_id) . "
                    ";

        $data_payment = array();
        $r = $db->query($query);
        if ($r->count() > 0) {
            foreach ($r as $row) {
                $data_payment[] = $row;
            }
        }

        $no = 0;
        foreach ($data_payment as $index => $row_payment) {
            if ($index == 0) {
                $no++;
                $content->add('<tr>');
               
                $content->add('<td>');
                $content->add('Tanggal : <br>' . ctransform::format_datetime($row_payment->date) . '<br><br>');
                $content->add('Kode Transaksi <br> : ' . $data_transaction[0]->booking_code.'<br><br>');
     
                foreach ($data_transaction_detail as $transaction_detail) {
                    $content->add($transaction_detail->name . '<br>');
                }

               
                $content->add('</td>');
                $content->add('<td>');
                $content->add('Rp. ' . ctransform::format_currency($row_payment->total_payment).'<br><br>');
 
                $content->add($row_payment->payment_status . '<br>');
                $content->add('(' . ctransform::format_datetime($row_payment->date) . ')');
                $content->add('</td>');

            } else {
                $no++;
                foreach ($data_transaction_detail as $transaction_detail) {
                    $content->add($transaction_detail->name . '<br>');
                }

                $content->add('</td>');
                $content->add('<tr>');
                $content->add('<td>');
                $content->add('Rp. ' . ctransform::format_currency($row_payment->total_payment));

                $content->add($row_payment->payment_status . '<br>');
                $content->add('(' . ctransform::format_datetime($row_payment->date) . ')');

                $content->add('</td>');
            }
        }
//
//        $query = "
//                    select 
//                        *
//                    from 
//                        transaction_payment
//                    where 
//                    status > 0
//                    and transaction_id = " . $db->escape($transaction_id) . "
//                    and date_success is not null
//                    ";
//
//        $data_payment_success = cdbutils::get_row($query);
//
//        if (count($data_payment_success) > 0) {
//            
//            if ($data_payment_success->payment_type != 'cod') {
//                $no++;
//                $content->add('<td>');
//                $content->add($no);
//                $content->add('</td>');
//                $content->add('<td>');
//                $content->add('Tanggal : ' . ctransform::format_datetime($row_payment->date_success) . '<br>');
//                $content->add('Kode Pembayaran : ' . $row_payment->payment_code);
//                $content->add('</td>');
//                $content->add('<td>');
//
//                foreach ($data_transaction_detail as $transaction_detail) {
//                    $content->add($transaction_detail->name . '<br>');
//                }
//
//                $content->add('</td>');
//                $content->add('<td>');
//                $content->add('Rp. ' . ctransform::format_currency($row_payment->total_payment));
//                $content->add('</td>');
//                $content->add('<td>');
//
//                $content->add($data_payment_success->payment_status . '<br>');
//                $content->add('(' . ctransform::format_datetime($data_payment_success->date_success) . ')');
//
//                $content->add('</td>');
//                $content->add('</tr>');
//            }
//        }
//
//        $data_transaction_shipping = transaction::get_transaction_shipping('transaction_id', $transaction_id);
//
//        if (count($data_transaction_shipping) > 0) {
//            foreach ($data_transaction_shipping as $transaction_shipping) {
//                $no++;
//                $content->add('<tr>');
//                $content->add('<td>');
//                $content->add($no);
//                $content->add('</td>');
//                $content->add('<td>');
//                $content->add($transaction_shipping->description);
//                $content->add('</td>');
//                $content->add('<td>');
//
//                foreach ($data_transaction_detail as $transaction_detail) {
//                    $content->add($transaction_detail->name . '<br>');
//                }
//
//                $content->add('</td>');
//                $content->add('<td>');
//                $content->add('Rp. ' . ctransform::format_currency($row_payment->total_payment));
//                $content->add('</td>');
//                $content->add('<td>');
//
//                $shipping_status = NULL;
//                if ($transaction_shipping->shipping_status == 'ON_SHIPPING') {
//                    $shipping_status = 'DI KIRIM';
//                }
//
//                if ($transaction_shipping->shipping_status == 'DELIVERED') {
//                    $shipping_status = 'SAMPAI';
//                }
//
//                $content->add($shipping_status . '<br>');
//                $content->add('(' . ctransform::format_datetime($transaction_shipping->created) . ')');
//
//                $content->add('</td>');
//                $content->add('</tr>');
//            }
//        }
        $content->add('</tr>');
        $content->add('</table>');
        
        // end table status order me xs

        $content->add('<table class="table-62hallfamily-gray tabel-big-show" style="font-size:12px">');
        $content->add('<tr>');
        $content->add('<th width="30px">');
        $content->add('#');
        $content->add('</th>');
        $content->add('<th>');
        $content->add('Tanggal Pembayaran');
        $content->add('</th>');
        $content->add('<th>');
        $content->add('Deskripsi Produk');
        $content->add('</th>');
        $content->add('<th>');
        $content->add('Total');
        $content->add('</th>');
        $content->add('<th>');
        $content->add('Tindakan');
        $content->add('</th>');

        $data_transaction = transaction::get_transaction('transaction_id', $transaction_id);
        $data_transaction_detail = transaction::get_transaction_detail('transaction_id', $transaction_id);

        $query = "
                    select 
                        *
                    from 
                        transaction_payment
                    where 
                    status > 0
                    and transaction_id = " . $db->escape($transaction_id) . "
                    ";

        $data_payment = array();
        $r = $db->query($query);
        if ($r->count() > 0) {
            foreach ($r as $row) {
                $data_payment[] = $row;
            }
        }

        $no = 0;
        foreach ($data_payment as $index => $row_payment) {
            if ($index == 0) {
                $no++;
                $content->add('<tr>');
                $content->add('<td>');
                $content->add($no);
                $content->add('</td>');
                $content->add('<td>');
                $content->add('Tanggal : ' . ctransform::format_datetime($row_payment->date) . '<br>');
                $content->add('Kode Transaksi : ' . $data_transaction[0]->booking_code);
                $content->add('</td>');
                $content->add('<td>');

                foreach ($data_transaction_detail as $transaction_detail) {
                    $content->add($transaction_detail->name . '<br>');
                }

                $content->add('</td>');
                $content->add('<td>');
                $content->add('Rp. ' . ctransform::format_currency($row_payment->total_payment));
                $content->add('</td>');
                $content->add('<td>');

                $content->add($row_payment->payment_status . '<br>');
                $content->add('(' . ctransform::format_datetime($row_payment->date) . ')');
                $content->add('</td>');
                $content->add('</tr>');
            } else {
                $no++;
                $content->add('<tr>');
                $content->add('<td>');
                $content->add($no);
                $content->add('</td>');
                $content->add('<td>');
                $content->add('</td>');
                $content->add('<td>');
                foreach ($data_transaction_detail as $transaction_detail) {
                    $content->add($transaction_detail->name . '<br>');
                }

                $content->add('</td>');
                $content->add('<td>');
                $content->add('Rp. ' . ctransform::format_currency($row_payment->total_payment));
                $content->add('</td>');
                $content->add('<td>');

                $content->add($row_payment->payment_status . '<br>');
                $content->add('(' . ctransform::format_datetime($row_payment->date) . ')');

                $content->add('<td>');
                $content->add('</td>');
            }
        }

        $query = "
                    select 
                        *
                    from 
                        transaction_payment
                    where 
                    status > 0
                    and transaction_id = " . $db->escape($transaction_id) . "
                    and date_success is not null
                    ";

        $data_payment_success = cdbutils::get_row($query);

        if (count($data_payment_success) > 0) {
            if ($data_payment_success->payment_type != 'cod') {
                $no++;
                $content->add('<tr>');
                $content->add('<td>');
                $content->add($no);
                $content->add('</td>');
                $content->add('<td>');
                $content->add('Tanggal : ' . ctransform::format_datetime($row_payment->date_success) . '<br>');
                $content->add('Kode Pembayaran : ' . $row_payment->payment_code);
                $content->add('</td>');
                $content->add('<td>');

                foreach ($data_transaction_detail as $transaction_detail) {
                    $content->add($transaction_detail->name . '<br>');
                }

                $content->add('</td>');
                $content->add('<td>');
                $content->add('Rp. ' . ctransform::format_currency($row_payment->total_payment));
                $content->add('</td>');
                $content->add('<td>');

                $content->add($data_payment_success->payment_status . '<br>');
                $content->add('(' . ctransform::format_datetime($data_payment_success->date_success) . ')');

                $content->add('</td>');
                $content->add('</tr>');
            }
        }

        $data_transaction_shipping = transaction::get_transaction_shipping('transaction_id', $transaction_id);

        if (count($data_transaction_shipping) > 0) {
            foreach ($data_transaction_shipping as $transaction_shipping) {
                $no++;
                $content->add('<tr>');
                $content->add('<td>');
                $content->add($no);
                $content->add('</td>');
                $content->add('<td>');
                $content->add($transaction_shipping->description);
                $content->add('</td>');
                $content->add('<td>');

                foreach ($data_transaction_detail as $transaction_detail) {
                    $content->add($transaction_detail->name . '<br>');
                }

                $content->add('</td>');
                $content->add('<td>');
                $content->add('Rp. ' . ctransform::format_currency($row_payment->total_payment));
                $content->add('</td>');
                $content->add('<td>');

                $shipping_status = NULL;
                if ($transaction_shipping->shipping_status == 'ON_SHIPPING') {
                    $shipping_status = 'DI KIRIM';
                }

                if ($transaction_shipping->shipping_status == 'DELIVERED') {
                    $shipping_status = 'SAMPAI';
                }

                $content->add($shipping_status . '<br>');
                $content->add('(' . ctransform::format_datetime($transaction_shipping->created) . ')');

                $content->add('</td>');
                $content->add('</tr>');
            }
        }

        $content->add('</table>');
        
        echo $app->render();
    }

    public function review() {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();
        $member_id = $session->get('member_id');

        $title = clang::__("Review");

        $row = $app->add_div()->add_class('row');
        $content = $row->add_div()->add_class('col-md-12');
        $content->add_div()->add_class('txt-info-title')->add($title);
        $content->add_div()->add_class('member-info-separate');
        

        $err_code = $session->get_once('error_review');
        $message = $session->get_once('message_review');
        if (!empty($message)) {
            $alert = 'success';
            if ($err_code > 0) {
                $alert = 'danger';
            }
            $alert_message = $content->add_div()->add_class('alert alert-' . $alert)->add('<a class="close" data-dismiss="alert">×</a>')->add($message);
        }

        $data = $this->get_review();
        if (count($data) > 0) {
            $container = $content->add_div()->add_class('container-review');
            $container_top = $container->add_div()->add_class('container-review-top');
            $top = $container_top->add_div()->add_class('row');
            $top_left = $top->add_div()->add_class('col-xs-8 col-md-10');
            $top_right = $top->add_div()->add_class('col-xs-4 col-md-2');
            $top_left_menu = $top_left->add_div()->add_class('row');
            $top_right_menu = $top_right->add_div()->add_class('row');
            $top_left_menu->add_div()->add_class('col-sm-5 rcol-1')->add(clang::__('Product Description'));
            $top_left_menu->add_div()->add_class('col-sm-4 rcol-2 hidden-xs')->add(clang::__('Payment Date'));
            $top_left_menu->add_div()->add_class('col-sm-3 rcol-3 hidden-xs')->add(clang::__('Total'));
            $top_right_menu->add_div()->add_class('col-sm-12 rcol-4')->add(clang::__('Review'));
        }

        foreach ($data as $key => $value) {
            $product_name = cobj::get($value, 'product_name');
            $transaction_date = cobj::get($value, 'transaction_date');
            $booking_code = cobj::get($value, 'booking_code');
            $total = cobj::get($value, 'total');
            $image_name = cobj::get($value, 'image_name');
            $file_path = $this->get_image($image_name);

            $tr_id = cobj::get($value, 'transaction_detail_id');

            $content_review = $container->add_div()->add_class('container-review-content');
            $top = $content_review->add_div()->add_class('row');
            $top_left = $top->add_div()->add_class('col-xs-8 col-md-10');
            $top_right = $top->add_div()->add_class('col-xs-4 col-md-2');
            $top_left_menu = $top_left->add_div()->add_class('row row-review');
            $top_right_menu = $top_right->add_div()->add_class('row row-review');
            $top_left_col_1 = $top_left_menu->add_div()->add_class('col-sm-5 ccol-1');
            $top_left_col_2 = $top_left_menu->add_div()->add_class('col-sm-4 ccol-2');
            $top_left_col_3 = $top_left_menu->add_div()->add_class('col-sm-3 ccol-3');
            $top_right_col_2 = $top_right_menu->add_div()->add_class('col-sm-12 ccol-4');

            $container_description = $top_left_col_1->add_div()->add_class('container-review-description');
            $container_payment_date = $top_left_col_2->add_div()->add_class('container-review-payment-date');
            $container_total = $top_left_col_3->add_div()->add_class('container-review-total');
            $container_review = $top_right_col_2->add_div()->add_class('container-review-review');

            $table = '<table class="table-description">';
            // $table .= "<tr><td>Warna</td><td>:</td><td>Red</td></tr>";
            $table .= '</table>';        
            $description_image = $container_description->add_div()->add_class('review-description-image');
            $description_image->add("<img src='".$file_path."'>");
            $description_text = $container_description->add_div()->add_class('review-description-text');
            $description_text->add_div()->add_class('description-title')->add($product_name);
            $description_text->add_div()->add_class('description-table')->add($table);

            $container_payment_date->add('Tanggal: '.$transaction_date.'<br>Kode Transaksi: '.$booking_code);

            $container_total->add('Rp '.ctransform::format_currency($total));

            $container_review->add("<button data-id='".$tr_id."' class='btn btn-imbuilding-light btn-add-review btn-primary'>".clang::__('Review')."</button>");           
        }

        $app->add_js("
            jQuery('.btn-add-review').click(function() {
                var id = jQuery(this).attr('data-id');
                var ajax_url = '".curl::httpbase()."member/account/add_review/?id='+id;
                $.app.show_dialog('body', 'Add Review', ajax_url);
            }); 
        ");

        echo $app->render();
    }

    private function get_image($image) {
        $retcode=200;
        if ($retcode == 500 or $image == NULL) {
            $image = curl::base() . 'application/lapakbangunan/default/media/img/product/no-image.png';
        } else {
            $image = image::get_image_url($image, 'view_all');
        }
        return $image;
    }

    public function member_promo() {
        $app = CApp::instance();
        $div_main_content = $app->add_div('div_main_content');
        $this->member_promo_content($div_main_content);
    }

    function get_review($id = '') {
        $db = CDatabase::instance();
        $session = Session::instance();
        $member_id = $session->get('member_id');
        $q = "SELECT p.image_name, t.transaction_id, td.transaction_detail_id, p.name AS product_name, t.date AS transaction_date, t.booking_code, (td.channel_sell_price * td.qty) AS total, p.file_path, t.channel_sell_price, td.qty
                FROM transaction t
                JOIN transaction_detail td 
                    ON t.transaction_id = td.transaction_id
                JOIN product p 
                    ON p.product_id = td.product_id
                WHERE t.member_id = ".$db->escape($member_id)." 
                    AND t.transaction_status = ".$db->escape('DELIVERED');

        if (strlen($id) > 0) {
            $q .= ' AND td.transaction_detail_id = '.$db->escape($id);
        }

        $data = $db->query($q);
        
        return $data;
    }

    public function member_promo_content($div_main_content = null) {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();
        $member_id = $session->get('member_id');
        $db = CDatabase::instance();

        $get = $_GET;
        if ($div_main_content == null) {
            $div_main_content = $app;
        }

        $active = carr::get($get, 'active', 1);
        $limit = $this->display_page;
        $offset = ($active - 1) * $limit;

        $err_code = 0;
        $message = '';

        $title = clang::__("Member Promo");

        $q = "SELECT *
            FROM cms_post
            WHERE post_status = 'publish' 
                AND post_type = 'news_promo_member'
                AND status > 0
                AND org_id = ".$db->escape(CF::org_id());
        $cdata = $db->query($q);
        $total_page = count($cdata);

        $q .= " LIMIT ".$offset.", ".$limit;
        $data = $db->query($q);

        $row = $div_main_content->add_div()->add_class('row');
        $container = $row->add_div()->add_class('col-md-12');
        $title_container = $container->add_div()->add_class('txt-info-title')
                ->add($title);

        $container->add_div()->add_class('member-info-separate');

        $content_row = $container->add_div()->add_class('row');

        foreach ($data as $key => $value) {
            $image = cobj::get($value, 'url_location');
            $title = cobj::get($value, 'post_title');
            $excerpt = cobj::get($value, 'post_excerpt');
            $post_name = cobj::get($value, 'post_name');
            $txt_content = substr($excerpt, 0, 120);
            $dot = '';
            if (strlen($excerpt) > 120) {
                $dot = '...';
            }
            $txt_content = $txt_content.$dot;
            $content = $content_row->add_div()->add_class('col-md-4 col-sm-6');
            $content_promo = $content->add_div()->add_class('content-promo');
            $content_promo->add_div()->add_class('member-promo-img')->add("<a target='_blank' href='".curl::base()."read/page/member_promo/".$post_name."'><img src='".$image."'></a>");
            $content_promo->add_div()->add_class('member-promo-title')->add("<a target='_blank' href='".curl::base()."read/page/member_promo/".$post_name."'>".$title."</a>");
            $content_promo->add_div()->add_class('member-promo-content')->add($txt_content);
        }

        if ($total_page > $limit) {
            $element_paging = $div_main_content->add_element('62hall-paging', 'paging');
            $element_paging->set_total_data($total_page);
            $element_paging->set_data_per_page($limit);
            $element_paging->set_active($active);
            $element_paging->set_reload_target('div_main_content');
            $element_paging->set_url_reload(curl::base() . 'member/account/member_promo/');
        }

        echo $app->render();
    }

    public function logout() {
        $app = CApp::instance();
        $session = Session::instance();
        $session->delete('member_id');
        $session->delete('email');
        curl::redirect(curl::base());
    }

    public function send_message() {
        $app = CApp::instance();
        $session = Session::instance();
        $db = CDatabase::instance();
        $err_code = 0;
        $err_message = 0;
        $post_data = $_POST;
        $container = $app->add_div()->add_class('row');
        $container = $container->add_div()->add_class("col-md-12");

        $container->add_div()
            ->add_class("txt-info-title")
            ->add(clang::__("Send Message"));
        $container->add_div()->add_class('member-info-separate');

        $err_code = $session->get_once('error_send_message');
        $message = $session->get_once('message_send_message');
        if (!empty($message)) {
            $alert = 'success';
            if ($err_code > 0) {
                $alert = 'danger';
            }
            $alert_message = $container->add_div()->add_class('alert alert-' . $alert)->add('<a class="close" data-dismiss="alert">×</a>')->add($message);
        }

        if ($post_data != null) {
            $to = carr::get($post_data, "to");
            $subject = carr::get($post_data, "subject");
            $message = carr::get($post_data, 'message');
            
            if (strlen($to) == 0) {
                $err_code++;
                $err_message = clang::__("Receiver message is required");
            }                    

            if (strlen($subject) == 0) {
                $err_code++;
                $err_message = clang::__("Subject message is required");
            }

            if (strlen($message) == 0) {
                $err_code++;
                $err_message = clang::__("Message content is required");
            }

            if ($err_code == 0) {
                $from = $session->get("member_id");
                try {
                    msg_ticket::send_to_department($from, $to, $subject, $message);
                }
                catch (Exception $exc) {
                    $err_code++;
                    $err_message = $exc->getMessage();
                }
            }

            //send email to admin
            if ($err_code == 0) {
                message::send_email($from, $to, $subject, $message);
            }

            if ($err_code == 0) {
                $err_message = "Message successfully sent.";
            }

            $session = Session::instance();
            $session->set('error_send_message', $err_code);
            $session->set('message_send_message', $err_message);
            curl::redirect(curl::base() . 'member/account?page_account=send_message');
        }

        $container_form = $app->add_div()->add_class('row');
        $div = $container_form->add_div()->add_class("box-send-message");
        $form = $div->add_form()->add_class('form-send-message')->set_action(curl::base().'member/account/send_message');
        
        $div_form = $form->add_div();            
        $row_field = $div_form->add_div()->add_class('col-md-12');

        $arr_list = msg_ticket::get_department();
        $list_to = array();
        foreach ($arr_list as $key => $value) {
            $name = carr::get($value, 'name');
            $list_to[$key] = $name;
        }

        $row = $row_field->add_div()->add_class('row');
        $left = $row->add_div()->add_class('col-md-2');
        $right = $row->add_div()->add_class('col-md-7');
        $left->add("<span class='form-label'>".clang::__('To')."</span>");
        $right->add_control('to', 'select')->add_class('validate[required] im-field')->set_list($list_to);

        $row = $row_field->add_div()->add_class('row');
        $left = $row->add_div()->add_class('col-md-2');
        $right = $row->add_div()->add_class('col-md-7');
        $left->add("<span class='form-label'>".clang::__('Subject')."</span>");
        $right->add_control('subject', 'text')->add_class('validate[required] im-field');

        $row = $row_field->add_div()->add_class('row');
        $left = $row->add_div()->add_class('col-md-2');
        $right = $row->add_div()->add_class('col-md-7');
        $left->add("<span class='form-label'>".clang::__('Message')."</span>");
        $right->add_control('message', 'textarea')->add_class('validate[required] im-field');

        $row = $row_field->add_div()->add_class('row');
        $left = $row->add_div()->add_class('col-md-2');
        $right = $row->add_div()->add_class('col-md-7');

        $btn_submit = $right->add_div();
        $btn_submit->add_action()
            ->set_submit(true)
            ->set_label(clang::__("Send"))
            ->add_class('btn-imlasvegas-secondary btn-send-message');

        echo $app->render();
    }

    public function inbox() {
        $app = CApp::instance();
        $session = Session::instance();
        $db = CDatabase::instance();
        $member_id = $session->get('member_id');
        
        $container = $app->add_div()->add_class('row');
        $container = $container->add_div()->add_class("col-md-12");

        $container->add_div()
            ->add_class("txt-info-title")
            ->add(clang::__("Inbox"));
        $container->add_div()->add_class('member-info-separate');

//        $query = 'SELECT m.*, d.name FROM message m
//                    JOIN department_msg d on d.department_msg_id = m.department_msg_id
//                    WHERE m.status > 0
//                    AND m.member_id = ' . $db->escape($session->get('member_id')).'
//                    ORDER BY m.created asc';
       
        $query = 'SELECT m.*, w.body as body_message , d.name FROM message m
                    JOIN department_msg d on d.department_msg_id = m.department_msg_id
                    JOIN message_detail w on w.message_id = m.message_id
                    WHERE m.status > 0
                    AND m.member_id = ' . $db->escape($session->get('member_id')).'
                    ORDER BY w.created desc limit 1';

        $data = $db->query($query);
        $container_table = $container->add_div()->add_class('container-table-inbox');
        $table = "<table>";

        foreach ($data as $key => $value) {
            $name = cobj::get($value, 'name');
            $created = cobj::get($value, 'created');
            $created = Date('d-m-Y', strtotime($created));
            $message = cobj::get($value, 'body_message');
            $message_id = cobj::get($value, 'message_id');
            if (strlen($message) > 100) {
                $message = substr($message, 0, 100).'...';
            }

            $table .= " <tr>";
            $table .= "     <td class='td-name'>".$name."</td>";
            $table .= "     <td class='td-date'>".$created."</td>";
            $table .= "     <td class='td-message'>".$message."</td>";
            $table .= "     <td class='td-btn-reply'><a href='".curl::base()."member/account/?page_account=reply_inbox&id=".$message_id."'><button class='btn btn-primary btn-imbuilding-light' type='button'>".clang::__('Reply')."</button></a></td>";
            $table .= " </tr>";
        }

        $table .= "</table>";



        $container_table->add($table);
        echo $app->render();
    }

    public function reply_inbox() {
        $request = $_GET;
        $message_id = carr::get($request, "id");
        $err_code = 0;
        $err_message = "";
        $app = CApp::instance();
        $db = CDatabase::instance();
        $session = Session::instance();
        $member_id = $session->get('member_id');
        $org_id=CF::org_id();
        $org=org::get_org($org_id);
        $org_code=cobj::get($org,'code');
        $item_image=cobj::get($org,'item_image');
        if (strlen($message_id) == 0) {
            $err_code++;
            $err_message = "Message ID is required";
        }
        
        // FILE LOCATION
            $query = 'SELECT image_name
                            FROM member 
                            WHERE status > 0 AND member_id = ' . $db->escape($member_id);
            $get_image = cdbutils::get_value($query);

            if (empty($get_image)) {
                $image = curl::base() . 'application/lapakbangunan/default/media/img/imbuilding/user_gray_150x150.png';
            } else {
                $image = image::get_image_url_front($get_image, 'view_profile');
            }

        if ($err_code == 0) {
            try {
                $data_message = msg_ticket::get_history($message_id);
            }
            catch (Exception $exc) {
                $err_code++;
                $err_message = $exc->getMessage();
            }
        }

        if ($err_code == 0) {
            $container = $app->add_div()->add_class('row');
            $container = $container->add_div()->add_class("col-md-12");

            $container->add_div()
                ->add_class("txt-info-title")
                ->add(clang::__("Inbox"));
            $container->add_div()->add_class('member-info-separate');

            $err_code = $session->get_once('error_send_message');
            $message = $session->get_once('message_send_message');
            if (!empty($message)) {
                $alert = 'success';
                if ($err_code > 0) {
                    $alert = 'danger';
                }
                $alert_message = $container->add_div()->add_class('alert alert-' . $alert)->add('<a class="close" data-dismiss="alert">×</a>')->add($message);
            }

            $container = $app->add_div()->add_class('row');
            $container = $container->add_div()->add_class("col-md-12 el-container-message");

            $prev_date = "";

            foreach ($data_message as $key => $value) {
                $message_type = carr::get($value, 'message_type');
                $department_name = carr::get($value, 'department_name');
                $body = carr::get($value, 'body');
                $member_name = Session::instance()->get("name");
                $created = carr::get($value, 'created');
                $hour = Date("H:i", strtotime($created));
                $date = org::full_date($created);
                if ($prev_date != $date) {
                    $container->add_div()->add_class("el-message-date-top")->add($date);
                }

                $prev_date = $date;

                if ($message_type == 'TO_MEMBER') {
                    $box_message_container = $container->add_div()->add_class("el-box-message-container");
                    $box_message = $box_message_container->add_div()->add_class("el-box-message");
                    $div_wrap=$box_message->add_div()->add_class('ico-user-wrap');
                    if(CF::domain()=='tokolasvegas.com'){
                        $div_wrap->add_div()->add_class("ico-user-admin");
                    }else{
                        $logo = curl::base() . 'application/adminittronmall/' . $org_code . '/upload/logo/item_image/' . $item_image;
                        $div_wrap->add('<img class="img-user-admin" src="'.$logo.'">');
                    }
                    $box_message_1 = $box_message->add_div();
                    $box_message_2 = $box_message->add_div()->add_class("el-box-message-right");

                    $text_message = $box_message_2->add_div();
                    $text_message->add_div()->add($body)->add_class("el-text-message");

                    $box_message_2->add_div()->add_class("el-message-admin");
                    $box_message_2->add_div()->add(clang::__("Send at")." ".$hour." (".$department_name.")")->add_class("el-message-date");                        
                }

                if ($message_type == 'TO_DEPARTMENT'){
                    $box_message_container = $container->add_div()->add_class("el-box-message-container el-right");
                    $box_message = $box_message_container->add_div()->add_class("el-box-message");
                    $box_message->add_div()->add_class('ico-user-wrap')->add_div()->add("<img src=".$image.">");
                    $box_message_1 = $box_message->add_div()->add_class('el-box-container-message');
                    $box_message_2 = $box_message->add_div()->add_class("el-box-message-right");

                    $text_message = $box_message_1->add_div();
                    $text_message->add_div()->add($body)->add_class("el-text-message member");

                    $box_message_detail = $box_message_1->add_div()->add_class("el-message-desc-container");
                    $box_message_detail->add_div()->add(clang::__("Send at")." ".$hour." (".$member_name.")")->add_class("el-message-date");
                    $box_message_detail->add_div()->add_class("el-message-member");

                }
            }

            $q = "SELECT subject FROM message WHERE message_id = ".$db->escape($message_id);
            $subject = cdbutils::get_value($q);


            $container = $app->add_div()->add_class("row");
            $form = $container->add_form()->add_class("col-md-12");
            $form->set_action(curl::base()."member/account/reply_to_department");
            $form->add_control('message', 'textarea')->add_class("txt-send-message")->set_row(3);
            
            $form->add_control("subject", "hidden")->set_value($subject);
            $form->add_control("message_id", "hidden")->set_value($message_id);


            $btn_submit = $form->add_div();
            $btn_submit->add_action()
                ->set_submit(true)
                ->set_label(clang::__("Send"))
                ->set_confirm(true)
                ->add_class('btn-imbuilding-light pull-right btn-send-message');

            echo $app->render();
        }   
    }

    function reply_to_department() {
        $request = $_POST;
        $message_id = carr::get($request, "message_id");
        $subject = carr::get($request, "subject");
        $body = carr::get($request, "message");

        $err_code = 0;
        $err_message = "";

        if (strlen($message_id) == 0) {
            $err_code++;
            $err_message = "Message ID is required";
        }

        if (strlen($subject) == 0) {
            $err_code++;
            $err_message = "Subject is required";
        }

        if (strlen($body) == 0) {
            $err_code++;
            $err_message = "Message is required";
        }

        if ($err_code == 0) {
            try {
                msg_ticket::reply_to_department($message_id, $subject, $body);
            }
            catch (Exception $exc) {
                $err_code++;
                $err_message = $exc->getMessage();
            }                    
        }

        //send email to admin
        if ($err_code == 0) {
            $db = CDatabase::instance();
            $q = "SELECT member_id, department_msg_id 
                    FROM message 
                    WHERE status > 0 
                        AND message_id = ".$db->escape($message_id);
            $data = cdbutils::get_row($q);
            $from = cobj::get($data, 'member_id');
            $to = cobj::get($data, 'department_msg_id');
            if (strlen($from) > 0 && strlen($to) > 0) {
                message::send_email($from, $to, $subject, $body);
            }
        }

        if ($err_code == 0) {
            $err_message = "Message successfully sent.";
        }

        $session = Session::instance();
        $session->set('error_send_message', $err_code);
        $session->set('message_send_message', $err_message);
        curl::redirect(curl::base() . 'member/account?page_account=reply_inbox&id='.$message_id);
    }

    function add_review() {
        $app = CApp::instance();
        $request = $_GET;
        $id = carr::get($request, 'id');
        $db = CDatabase::instance();

        $container = $app->add_div();

        $data = $this->get_review($id);

        foreach ($data as $key => $value) {
            $img = cobj::get($value, 'image_name');
            $img = $this->get_image($img);
            $product_name = cobj::get($value, 'product_name');
            $transaction_date = cobj::get($value, 'transaction_date');
            $booking_code = cobj::get($value, 'booking_code');
            $channel_sell_price = cobj::get($value, 'channel_sell_price');
            $qty = cobj::get($value, 'qty');

            $date = Date('d-m-Y', strtotime($transaction_date));
        }
        
        if (count($data) > 0) {
            $q = "SELECT * 
                    FROM review
                    WHERE status > 0 
                        AND transaction_detail_id = ".$db->escape($id);

            $data_review = cdbutils::get_row($q);
            $review_title = cobj::get($data_review, 'title');
            $review_text = cobj::get($data_review, 'review');


            $table = "<table>";
            $table .= "<tr><td>Tanggal</td><td>&nbsp;:&nbsp;</td><td>".$date."</td></tr>";
            $table .= "<tr><td>Kode Transaksi</td><td>&nbsp;:&nbsp;</td><td>".$booking_code."</td></tr>";
            $table .= "</table>";

            $container->add($table);

            $container_review = $container->add_div()->add_class('container-review-product');
            $modal_image = $container_review->add_div()->add_class('modal-review-image');
            $modal_description = $container_review->add_div()->add_class('modal-review-description');
            $modal_image->add("<img src='".$img."'>");
            $modal_description->add_div()->add_class('review-description-title')->add($product_name);
            $modal_description->add_div()->add_class('review-description-price')->add('Rp '.ctransform::format_currency($channel_sell_price));
            $modal_description->add_div()->add_class('review-description-qty')->add('Jumlah : '.$qty);

            $is_disabled = false;
            if (strlen($review_text) > 0) {
                $is_disabled = true;
            }

            $form = $container->add_form()->set_action(curl::base().'member/account/save_review');
            $form->add_control('id', 'hidden')->set_value($id);
            $form->add_div()->add('Title')->add_class('txt-modal-review');
            $form->add_div()->add_control('title', 'text')->add_class('im-field')->set_value($review_title)->set_disabled($is_disabled);
            $form->add_div()->add(clang::__('Your Review'))->add_class('txt-modal-review');
            $form->add_div()->add_control('review', 'textarea')->add_class('im-field')->set_value($review_text)->set_disabled($is_disabled);
            if (strlen($review_text) == 0) {
                $form->add_div()->add_class('modal-container-btn')->add("<button class='btn btn-primary btn-imbuilding-light ' type='submit'>".clang::__('Send')."</button>");
            }
        }

        echo $app->render();
    }

    function save_review() {
        $app = CApp::instance();
        $session = Session::instance();
        $db = CDatabase::instance();
        $err_code = 0;
        $err_message = '';
        $post = $_POST;
       
        if ($post != null) {
            $id = carr::get($post, 'id');
            $title = carr::get($post, 'title');
            $review = carr::get($post, 'review');

            if ($err_code == 0) {
                if (strlen($id) == 0) {
                    $err_code++;
                    $err_message = clang::__('ID required');
                }
            }
            if ($err_code == 0) {
                if (strlen($title) == 0) {
                    $err_code++;
                    $err_message = clang::__('Title required');
                }
            }
            if ($err_code == 0) {
                if (strlen($review) == 0) {
                    $err_code++;
                    $err_message = clang::__('Review required');
                }
            }

            if ($err_code == 0) {
                $q = "SELECT * 
                        FROM review 
                        WHERE status > 0 
                            AND transaction_detail_id = ".$db->escape($id);

                $data_review = cdbutils::get_row($q);
                $arr_data = array(
                    'transaction_detail_id' => $id,
                    'title' => $title,
                    'review' => $review,
                );

                $db->begin();
                try {
                    $arr_data['created'] = Date('Y-m-d H:i:s');
                    $arr_data['createdby'] = $session->get('name');
                    $arr_data['updated'] = Date('Y-m-d H:i:s');
                    $arr_data['updatedby'] = $session->get('name');
                    $db->insert('review', $arr_data);
                } catch (Exception $e) {
                    $err_code++;
                    $err_message = $e->getMessage();
                }
            }
            
            if ($err_code == 0) {
                $db->commit();
            }            
            else {
                $db->rollback();
            }
        }
        else {
            $err_code++;
            $err_message = clang::__('Post data required');
        }

        if ($err_code == 0) {
            $err_message = "Review successfully send.";
        }

        $session = Session::instance();
        $session->set('error_review', $err_code);
        $session->set('message_review', $err_message);
        curl::redirect(curl::base() . 'member/account?page_account=review');
    }

    public function manual_book() {
        $app = CApp::instance();
        $domain = CF::domain();
        $session = Session::instance();
        $db = CDatabase::instance();
        $member_id = $session->get('member_id');
        
        $container = $app->add_div()->add_class('row');
        $container = $container->add_div()->add_class("col-md-12");

        $container->add_div()
            ->add_class("txt-info-title")
            ->add(clang::__("Manual Book"));

        $container->add_div()->add_class('member-info-separate');

        $code = 'default';
        $file_name = 'Modul_Halaman_Akun_Member_Ittronmall.pdf';
        
        $container->add('<iframe width="100%" height="450" src="//'.$domain.'/application/ittronmall/'.$code.'/data/help/'.$file_name.'"></iframe>');
        echo $app->render();
    }

}
