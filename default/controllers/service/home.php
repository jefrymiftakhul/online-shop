<?php

/**
 *
 * @author Riza 
 * @since  Nov 12, 2015
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class Home_Controller extends ServiceController {

    // ADVERTISE 
    private $advertise_1 = 0;
    // PRODUCT CATEGORY
    private $category_product1 = "LINE1";

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $app = CApp::instance();
        $user = $app->user();
        $org_id = $this->org_id();
        $link_list = $this->list_menu();

        // menu


        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_link($link_list)
                ->set_absolute_menu('service-search');


        // SLIDE
        $slide_list = slide::get_slide($org_id, $this->page());

        $element_slide_show = $app->add_element('62hall-slide-show', 'element_slide_show');
        $element_slide_show->set_type('product');
        $element_slide_show->set_slides($slide_list);

        // SUBSCRIBE

        $register_service = $app->add_div()->add_class('container');
        $row_register = $register_service->add_div()->add_class('col-md-12 border-1 margin-top-30 margin-bottom-30')
                ->custom_css('padding-left', '0px');
        $register_image = $row_register->add_div()->add_class('col-md-5')
                ->custom_css('padding', '0px');
        $register_input = $row_register->add_div()->add_class('col-md-7');
        $div_register_input = $register_input->add_div()->add_class('font-size24 padding-left-right20 margin-top-30');
        $div_register_input->add('Apakah Anda ingin <span class="font-red bold">Pelayanan/Jasa Anda</span><br>Dapat <span class="font-red bold">Menarik Customer Baru?</span>');
        $div_register_input->add_br();
        $div_register_input->add_br();
        $div_register_input->add('Apakah Anda ingin <span class="font-red bold">Meningkatkan Reputasi</span><br>Jasa/ Pelayanan Anda ?');
        $div_register_input->add_br();
        $div_register_input->add_br();

        $register_service_element = CElement_Service_Register::factory()
                ->set_trigger_button(true)
                ->set_icon(FALSE);
        $app->add_js($register_service_element->js());

        $div_register_input->add($register_service_element->html());
        $div_register_input->add_br();
        $div_register_input->add_br();

        $image_register = curl::base() . 'application/62hallfamily/default/media/img/service/register_service.jpg';
        $register_image->add('<img src="' . $image_register . '" width="100%" />');

        // ADVERTISE
        $data_advertise = advertise::get_advertise($org_id, $this->page());

        $advertise_top = isset($data_advertise[$this->advertise_1]) ? $data_advertise[$this->advertise_1] : array();

        $element_advertise = $app->add_element('62hall-advertise', 'advertise-top');
        $element_advertise->set_list($advertise_top);


        // MENU PRODUCT LINE 
        $menu_product = $app->add_div()->add_class('bg-white menu-product');

        $menu_product_container = $menu_product->add_div()->add_class('container margin-top-20 margin-bottom-20');
        $row_menu_product_container = $menu_product_container->add_div()->add_class('row-fluid');

        $menu = $row_menu_product_container->add_div('menuproduct-line1-container')->add_class('col-xs-12 col-md-2 col-lg-2');
        ;
        $menu_image = $row_menu_product_container->add_div('menuimage-line1-container')->add_class('col-xs-4 col-sm-4 col-md-2 col-lg-2 padding-right-0');
        $product = $row_menu_product_container->add_div('service-line1-container')->add_class('col-xs-12 col-sm-8 col-md-8 col-lg-8');

        // Menu Line 1
        $data_category_line1 = cms_product::get_cms_product($org_id, $this->page(), $this->category_product1);

        $element_menu_line1 = $menu->add_element('62hall-menu-product', 'menu-service-top')
                        ->set_menu($data_category_line1)
                        ->set_target('service-line1-container')
                        ->set_target_image('menuimage-line1-container')->set_product_type_name('service');

        // Ready Get
        $first_category_line1 = isset($data_category_line1[0]['cms_product_product_category_id']) ? $data_category_line1[0]['cms_product_product_category_id'] : NULL;

        if (!empty($first_category_line1)) {
            $listener = $app->add_listener('ready');
            $listener->add_handler('reload')
                    ->set_target('service-line1-container')
                    ->set_url(curl::base() . "service/home/get_service_category/" . $first_category_line1);
            $listener->add_handler('reload')
                    ->set_target('menuimage-line1-container')
                    ->set_url(curl::base() . "service/home/get_service_category_image/" . $first_category_line1);
        }

        $app->add_js("$('input').blur();");

        echo $app->render();
    }

    public function get_service_category_image($category_id = null) {
        if ($category_id == null) {
            $request = array_merge($_GET, $_POST);
            $category_id = carr::get($request, 'category_id');
        }
        $app = CApp::instance();

        $data_product_line1 = cms_product::get_cms_product_product_category($category_id);
        $image_url = carr::get($data_product_line1, 'image_url');

        $app->add('<img style="height: 532px" width="100%" src="' . $image_url . '"/>');

        echo $app->render();
    }

    public function get_service_category($category_id = null) {
        if ($category_id == null) {
            $request = array_merge($_GET, $_POST);
            $category_id = carr::get($request, 'category_id');
        }
        $app = CApp::instance();

        $data_product_line1 = cms_product::get_cms_product_product_category($category_id);
        $list_product = carr::get($data_product_line1, 'data');

        $elemet_product = $app->add_element('62hall-menu-product-category', 'menu-product-category-' . $category_id)
                ->set_list_product($list_product)->set_product_type_name('service')
                ->set_location_detail(curl::base() . 'service/product/item/');

        echo $app->render();
    }

}
