<?php

/**
 *
 * @author M. Isman Subakti
 * @since  Oct 03, 2016
 * @license http://ittron.co.id ITtron Indonesia
 */
class Product_information_Controller extends ProductController {

    private $store = NULL;
    private $domain = NULL;

    public function __construct() {
        parent::__construct();
        $org_id = $this->org_id();
        $data_org = org::get_org($org_id);

        if (!empty($data_org)) {
            $this->store = $data_org->name;
            $this->domain = $data_org->domain;
        }
    }

    public function static_indo_day($day) {
        $res = '';
        switch ($day) {
            case 'Mon' : {
                    $res = 'Senin';
                    break;
                }
            case 'Tue' : {
                    $res = 'Selasa';
                    break;
                }
            case 'Wed' : {
                    $res = 'Rabu';
                    break;
                }
            case 'Thu' : {
                    $res = 'Kamis';
                    break;
                }
            case 'Fri' : {
                    $res = 'Jumat';
                    break;
                }
            case 'Sat' : {
                    $res = 'Sabtu';
                    break;
                }
            case 'Sun' : {
                    $res = 'Minggu';
                    break;
                }
        }
        return $res;
    }

    public function index() {
        $app = CApp::instance();
        $title = '';

        $menu_list = $this->menu_list();
        $link_list = $this->list_menu();

        // SEARCH 
        $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('search')
                ->set_url(curl::base() . "home/search");

        // SHOPPING CART 
        $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_menu($menu_list)
                ->set_link($link_list);
        

        $container = $app->add_div()->add_class('container');
        $div_title = $container->add_div()->add_class("row-fluid");
        $div_title->add('<h1 class="font-size24 margin-top-20">Informasi Terbaru</h1>');
        $div_main_content = $container->add_div('div_main_content')->add_class("row-fluid");

        $this->view_product($div_main_content);
    }

    function view_product($div_main_content = null) {
        $app = CApp::instance();
        $db = CDatabase::instance();

        $limit = 5;

        if ($div_main_content == null) {
            $div_main_content = $app;
        }

        $get = $_GET;
        $gactive = carr::get($get, 'active');

        $active = 1;
        if (strlen($gactive) > 0) {
            $active = $gactive;
        }

        $offset = ($active - 1) * $limit;

        $date = Date("Y-m-d", strtotime("+1 month", strtotime(Date('Y-m-d'))));

        $q = "SELECT cpi.*, p.name as product_name, p.image_name, p.url_key 
                FROM cms_product_information cpi
                JOIN product p 
                    ON p.product_id = cpi.product_id
                WHERE cpi.status > 0
                    AND cpi.date_information < ".$db->escape($date)."
                    AND (cpi.org_id = 0 OR cpi.org_id IS NULL OR cpi.org_id = ".$db->escape(CF::org_id()).")";

        $q .= " ORDER BY date_information desc ";

        $data = $db->query($q);
        $total_data = count($data);

        $q .= " LIMIT ".$offset.", ".$limit;
        $ldata = $db->query($q);

        foreach ($ldata as $key => $value) {
            $image = curl::httpbase() . 'application/62hallfamily/default/media/img/product/no-image.png';
            if (strlen($value->image_name) > 1) {
                // $image = $value->image_name;
                $image = image::get_image_url($value->image_name, 'view_all');
            }

            $product_name = cobj::get($value, 'product_name');
            $description  = cobj::get($value, 'description');
            $date_information = cobj::get($value, 'date_information');
            $url_key = cobj::get($value, 'url_key');

            $arr_month = array('Januari', 'Febuari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
            $month = $arr_month[(Date('m', strtotime($date_information)) - 1)];

            $date_information = $this->static_indo_day(date('D', strtotime($date_information))).', '.Date('d', strtotime($date_information)).' '.$month.' '.Date('Y', strtotime($date_information));

            $container = $div_main_content->add_div()->add_class('product-information-container');
            $row = $container->add_div()->add_class('row');
            $col1 = $row->add_div()->add_class('col-sm-2 text-center');
            $col2 = $row->add_div()->add_class('col-sm-10');

            $col1->add_div()->add_class('img-container')->add("<a href='".curl::base()."product/item/".$url_key."'><img class='img-responsive' src='".$image."'/></a>");
            $content = $col2->add_div()->add_class('product-information-content');
            $content->add_div()->add_class('content-title')->add("<a class='font-red' href='".curl::base()."product/item/".$url_key."'>".$product_name."</a>");
            $content->add_div()->add_class('content-date')->add($date_information);
            $content->add_div()->add_class('content-text')->add($description);
            $content->add_div()->add_action()->add_class('btn-62hallfamily bg-red font-white border-3-red margin')->set_link(curl::base().'product/item/'.$url_key)->set_label(clang::__('Detail'));
        }       

        $element_paging = $div_main_content->add_element('62hall-paging', 'paging');
        $element_paging->set_total_data($total_data);
        $element_paging->set_data_per_page($limit);
        $element_paging->set_active($active);
        $element_paging->set_reload_target('div_main_content');
        $element_paging->set_url_reload(curl::base() . 'product_information/view_product/');

        echo $app->render();
    }

}
