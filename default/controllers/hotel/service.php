<?php
class Service_Controller extends HotelController
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $app = CApp::instance();
        $service_container = $app->add_div()->add_class('service-container');
        $service_container->add_div()->add_class("service-text-title")->add(clang::__("Hotel"));
        $air_element = CElement_Hotel_Search::factory('sunholiday')->set_show_info(true)->set_subtitle(clang::__('Cari Hotel Sekarang'))->set_show(true)->set_partner_hotel(true);
        $service_container->add($air_element);
        echo $app->render();
    }
}
