<?php

/**
 *
 * @author Riza 
 * @since  Nov 19, 2015
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class Product_Controller extends ShinjukuController {

    private $_count_item = 16;
    private $deal_pick_for_you = "pick_for_you";
    private $type_select = 'select';
    private $type_image = 'image';

    public function __construct() {
        parent::__construct();
    }

    public function index($product_id) {
        echo $product_id;
    }

    public function item($product_id) {
        $db = CDatabase::instance();
        if(!is_numeric($product_id)) {
            $product_id = cdbutils::get_value("select product_id from product where url_key= ".$db->escape($product_id));
        }
        if ($product_id == null) {
            curl::redirect(curl::base());
        }
        
        $app = CApp::instance();
        
        $user = $app->user();
        $org_id = $this->org_id();
        $menu_list = $this->menu_list();
        $link_list = $this->list_menu();

        $error = 0;
        $error_message = '';

        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_menu($menu_list);
        $element_menu->set_link($link_list);
        $element_menu->set_url_menu(curl::base() . 'shinjuku/product/category/');

        $container = $app->add_div()->add_class('container margin-top-20');

        $q = "
                    select
                            p.name as product,
                            p.*,
                            pt.*
                    from
                            product as p
                            inner join product_type as pt on pt.product_type_id=p.product_type_id
                    where
                            p.status>0
							and is_active>0
                            and p.status_confirm='CONFIRMED'
                            and p.product_id=" . $db->escape($product_id) . "
                            and pt.name=" . $db->escape($this->page()) . "
            ";
        $product = $db->query($q);
        if ($product->count() == 0) {
            $error++;
            $error_message = "Product not found please select other product";
        }

        if ($error == 0) {
            $product_image = $container->add_div()->add_class('col-md-4 padding-0');
            $product_detail = $container->add_div()->add_class('col-md-4');
            $product_info = $container->add_div()->add_class('col-md-4 margin-top-20');

            // GALERY PRODUCT
            $data_galery_product = product::get_product_image($product_id);

            $element_galery_product = $product_image->add_element('62hall-galery-product', 'galery-product-' . $product_id);
            $element_galery_product->set_images($data_galery_product);
            $element_galery_product->set_page('shinjuku');


            // DETAIL PRODUCT
            $data_detail_product = product::get_product($product_id);
            $data_attribute_category = product::get_attribute_category_all($product_id);
            $element_detail_product = $product_detail->add_element('62hall-detail-shinjuku', 'detail-shinjuku-' . $product_id);

            $element_detail_product->set_key($product_id)
                    ->set_detail_list($data_detail_product)
                    ->set_attribute_list($data_attribute_category);

            // PRODUCT INFO
            $element_detail_info = $product_info->add_element('62hall-detail-info', 'detail-info' . $product_id);
            //share fb
            $org = org::get_org($org_id);
            $url = '';
            if ($org) {
                $url = 'http://' . $org->domain . '/shinjuku/item/' . $product_id;
            }
            $url_image = '';
            $description = '';
            $q = "
						select
								*
						from
								product
						where
								product_id=" . $db->escape($product_id) . "
				";
            $r = $db->query($q);
            if ($r->count() > 0) {
                $row = $r[0];
                $url_image = $row->file_path;
                $description = $row->description;
            }
            $facebook_app_id = ccfg::get('facebook_app_id');
            $additional_head = '

						<meta property="fb:app_id" content="' . $facebook_app_id . '">
						<meta property="og:url" content="' . $url . '">
						<meta property="og:title" content="' . $data_detail_product['name'] . '-' . $org->name . '">
						<meta property="og:site_name" content="' . $url . '">
						<meta property="og:description" content="' . htmlspecialchars(strip_tags(carr::get($data_detail_product, 'overview'))) . '">
						<meta property="og:type" content="article">
						<meta property="og:image" content="' . image::get_image_url($data_detail_product['image_path'], 'view_all') . '">

				';
            $app->set_additional_head($additional_head);
            $element_detail_info->set_url_shared_facebook($url);
            $element_detail_info->set_url_shared_twitter($url);
            $element_detail_info->set_text_twitter($data_detail_product['name'] . '-' . $org->name);
            // OVERVIEW PRODUCT
            $overview_product = $app->add_div()->add_class('container');
            $element_overview = $overview_product->add_element('62hall-overview-product', 'tab-product');
            $element_overview->set_key($product_id);
            $app->add_br();


            // PICKED FOR YOU
            $pickedforyou = $app->add_div()->add_class('container');
            $pickedforyou->add('<h4 class="border-bottom-gray padding bold font-red">PRODUK TERKAIT</h4>');
            $app->add_br();

            $picked_for_you_list = deal::get_deal($org_id, $this->page(), $this->deal_pick_for_you);

            $element_picked_for_you = $app->add_element('62hall-slide-product', 'picked-for-you');
            $element_picked_for_you->set_count_item(6)
                    ->set_list_item($picked_for_you_list);

            $app->add_js("$('input').blur();");
        } else {
            $container->add($error_message);
        }

        echo $app->render();
    }

    //<editor-fold defaultstate="collapsed" desc="Product Overview">
    public function overview($product_id) {
        $app = CApp::instance();

        $data_detail_product = product::get_product($product_id);
        $overview = carr::get($data_detail_product, 'overview', NULL);

        $app->add($overview);

        echo $app->render();
    }

    public function spesifikasi($product_id) {
        $app = CApp::instance();

        $data_detail_product = product::get_product($product_id);
        $spesification = carr::get($data_detail_product, 'spesification', NULL);

        $app->add($spesification);

        echo $app->render();
    }

    public function reviews($product_id) {
        $app = CApp::instance();

        echo $app->render();
    }

    public function faq($product_id) {
        $app = CApp::instance();

        $data_detail_product = product::get_product($product_id);
        $faq = carr::get($data_detail_product, 'faq', NULL);

        $app->add($faq);

        echo $app->render();
    }

    //</editor-fold>

    public function category($category_id = NULL) {
        $app = CApp::instance();
        $user = $app->user();
        $db = CDatabase::instance();
        $org_id = $this->org_id();
        $menu_list = $this->menu_list();
        $link_list = $this->list_menu();

        $request = $_GET;
        $request_category_id = carr::get($request, 'category');
        if ($request_category_id) {
            $category_id = $request_category_id;
        }

        // Menu
        $data_product_category = product_category::get_product_category_menu($this->page());

        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_menu($menu_list);
        $element_menu->set_link($link_list);
        $element_menu->set_url_menu(curl::base() . 'shinjuku/product/category/');
        $first_parent = product_category::get_first_parent_category($category_id);

        $container = $app->add_div()->add_class('container');
        $category_menu_container = $container->add_div()->add_class('col-md-4 padding-0 margin-30');
        $category_container = $container->add_div()->add_class('col-md-8 margin-30 padding-0');
        $list_image_cms_product_category = cms_product_category::get_cms_product_category($org_id, $category_id);
        if (count($list_image_cms_product_category) == 0 && $first_parent != null) {
            $list_image_cms_product_category = cms_product_category::get_cms_product_category($org_id, $first_parent->product_category_id);
            if (count($list_image_cms_product_category) > 0) {
                $advertise_category = $category_container->add_div('advertise-category')->add_class('col-md-12 padding-0');
                $element_advertise_category = $advertise_category->add_element('62hall-category-advertise', 'category-advertise');
                $element_advertise_category->set_list_advertise($list_image_cms_product_category);
            }
        }

        $data_category_menu_choise = product_category::get_parent_category($category_id);

        if ($data_category_menu_choise) {
            $category_menu = $category_menu_container->add_div()->add_class('categories border-1 padding-left-right10');

            $title = $category_container->add_div()->add_class('col-md-12 padding-0');

            $title->add('<h4 class="border-bottom-gray bold">' . $data_category_menu_choise->name . '</h4>');

            $product_category = $category_container->add_div('page-product-category')->add_class('col-md-12 padding-0');


            // CATEGORY MENU
            $category_menu_list = product_category::get_product_category_menu($this->page(), $first_parent->product_category_id);

            $element_category_menu = $category_menu->add_element('62hall-category-menu', 'category-menu-' . $category_id);
            $element_category_menu->set_key($category_id)
                    ->set_head_key($first_parent->product_category_id)
                    ->set_head_menu($first_parent->name)
                    ->set_list_menu($category_menu_list)
                    ->set_url_menu(curl::base() . 'shinjuku/product/category/');


            // CATEGORY FILTER
            $options = array(
                'product_type' => $this->page(),
                'product_category_id' => $category_id,
                'visibility' => 'catalog',
            );
            $list_attribute = product::get_attributes_array($options);

            $min_price = product::get_min_price($options);
            $max_price = product::get_max_price($options);
            if ($min_price == null) {
                $min_price = 0;
            }
            if ($max_price == null) {
                $max_price = 0;
            }
            $range_price = array(
                'min' => $min_price,
                'max' => $max_price,
            );
            $list_filter_product = array(
                'diskon' => clang::__('Diskon'),
                'min_price' => clang::__('Termurah'),
                'max_price' => clang::__('Termahal'),
                'is_new' => clang::__('Terbaru'),
                'popular' => clang::__('Popular'),
                'A-Z' => clang::__('A-Z'),
                'Z-A' => clang::__('Z-A'),
            );
            $q = "
					select
							*
					from
							product_category
					where
							product_category_id=" . $db->escape($category_id) . "
				";
            $r = $db->query($q);
            $category_lft = '';
            $category_rgt = '';
            if ($r->count() > 0) {
                $row = $r[0];
                $category_lft = $row->lft;
                $category_rgt = $row->rgt;
            }
            $category_filter = $category_menu_container->add_div()->add_class('categories border-1 padding-left-right10 margin-top-30');
            $element_filter = $category_filter->add_element('62hall-filter', 'category-filter')
                    ->set_key($category_id)
                    ->set_list_filter_product($list_filter_product)
                    ->set_list_filter($list_attribute)
                    ->set_product_visibility('catalog')
                    ->set_filter_category_lft($category_lft)
                    ->set_filter_category_rgt($category_rgt)
                    ->set_filter_page($this->page())
                    ->set_min_price($range_price['min'])
                    ->set_max_price($range_price['max']);


            $filter_name = '';

            //check any subcategory available for this category
            $q = "select count(*) as cnt from product_category where status>0 and parent_id=" . $db->escape($category_id);
            $subcategory_count = cdbutils::get_value($q);
            if ($subcategory_count > 0) {
                $product_category->add_listener('ready')
                        ->add_handler('reload')
                        ->set_target('page-product-category')
                        //->add_param_input('arr_product_id')
                        ->set_url(curl::base() . "shinjuku/product/reload_product_category?product_category_id=" . $category_id);
//                $product_category->add_listener('ready')
//                        ->add_handler('reload')
//                        ->set_target('page-product-category')
//                        //->add_param_input('arr_product_id')
//                        ->set_url(curl::base() . "reload/reload_product_filter?source=category&visibility=catalog&sortby=diskon&filter_page=" . $this->page() . '&filter_category_lft=' . $category_lft . '&filter_category_rgt=' . $category_rgt);
            } else {
                $product_category->add_listener('ready')
                        ->add_handler('reload')
                        ->set_target('page-product-category')
                        //->add_param_input('arr_product_id')
                        ->set_url(curl::base() . "reload/reload_product_filter?source=category&visibility=catalog&sortby=diskon&filter_page=" . $this->page() . '&filter_category_lft=' . $category_lft . '&filter_category_rgt=' . $category_rgt);
            }
        } else {
            $category_container->add('Kategori Shinjuku tidak ditemukan');
        }
        echo $app->render();
    }
    
    public function reload_product_category() {
        $request = $_GET;
        $db = CDatabase::instance();
        $app = CApp::instance();
        $product_category_id = carr::get($request,'product_category_id');
        $subcategories = $db->query("select * from product_category where status>0 and parent_id=".$db->escape($product_category_id));
        $div_row = $app->add_div()->add_class('row');
        foreach($subcategories as $subcategory) {
            $container = $div_row->add_div()->add_class('container product-container col-xs-6 col-sm-4 col-md-3');
            $subcategory_image_url = curl::base() . 'cresenity/noimage';
            if (strlen($subcategory->image_name) > 0) {
                $subcategory_image_url = image::get_image_url($subcategory->image_name);
            }
            $html = '';
            $html .= '<a href="'.curl::base().'shinjuku/product/category/'.$subcategory->product_category_id.'">';
            $html .= '  <div class=" product margin-bottom-10">';
            $html .= '      <img src="'.$subcategory_image_url.'" />';
//            $html .= '      <div class=" product-bottom-container">		';
//            $html .= '          <div class=" font-gray small">			';
//            $html .= '                  '.$subcategory->name.'';
//            $html .= '          </div>';
//            $html .= '      </div>';
            $html .= '  </div>';
            $html .= '</a>';
            $container->add($html);
        }
        echo $app->render();
    }

    private function generate_list($type, $data = array()) {
        $list = array();

        foreach ($data as $key => $row_data) {
            if ($type == $this->type_select) {
                $value = carr::get($row_data, 'attribute_key');
            }
            if ($type == $this->type_image) {
                $value['key'] = carr::get($row_data, 'attribute_key');
                $value['value'] = carr::get($row_data, 'file_path');
            }

            $list[$key] = $value;
        }

        return $list;
    }

    public function generate_attribute($code, $product_id) {
        $app = CApp::instance();

        $request = $_GET;

        $param = array();
        foreach ($request as $key => $row_request) {
            if ($key != 'capp_current_container_id') {
                $param[] = $row_request;
            }
        }

        $attribute_category = product::get_attribute_category_all($product_id);
        $attibute_data = $attribute_category[$code];
        $id = carr::get($attibute_data, 'attribute_category_id');
        $type = carr::get($attibute_data, 'attribute_category_type');
        $prev = carr::get($attibute_data, 'prev_attribute_category');
        $next = carr::get($attibute_data, 'next_attribute_category');

        $attribute = product::get_attribute_list($product_id, $id, $param);

        $list = $this->generate_list($type, $attribute['data']);

        foreach ($prev as $row_prev) {
            $arr_param[] = 'att_cat_' . $id;
        }
        if ($type == $this->type_select) {
            $div_container = $app->add_div('container-' . $code);
            $select = $div_container->add_field()
                    ->add_control('att_cat_' . $id, 'select')
                    ->add_class('select-62hallfamily margin-bottom-10')
                    ->custom_css('margin-left', '15px')
                    ->custom_css('width', '130px')
                    ->set_list($list);

            if (!empty($next) and count($list) > 0) {
                $listener = $select->add_listener('ready');

                $handler = $listener->add_handler('reload')
                        ->set_target('container-' . $next)
                        ->add_param_input($arr_param)
                        ->set_url(curl::base() . 'shinjuku/generate_attribute/' . $next . '/' . $product_id);
                $listener = $select->add_listener('change');

                $handler = $listener->add_handler('reload')
                        ->set_target('container-' . $next)
                        ->add_param_input($arr_param)
                        ->set_url(curl::base() . 'shinjuku/generate_attribute/' . $next . '/' . $product_id);
            }
        } else if ($type == $this->type_image) {
            $div_container = $app->add_div('container-' . $code);
            $index = 1;
            foreach ($list as $key => $value) {
                $active = NULL;
                if ($index == 1) {
                    $active = 'active';
                    $div_container->add_control('att_cat_' . $id, 'hidden')
                            ->set_value($key);
                }
                $image = $div_container->add_div($key)
                        ->add_class('btn-colors margin-bottom-10 link ' . $active)
                        ->custom_css('display', 'inline-block')
                        ->custom_css('padding', '2px')
                        ->add('<img src="' . $value['value'] . '" style="width:20px; height:20px"/>');

                if (!empty($next) and count($list) > 0) {
                    $listener = $image->add_listener('ready');

                    $handler = $listener->add_handler('reload')
                            ->set_target('container-' . $next)
                            ->add_param_input($arr_param)
                            ->set_url(curl::base() . 'shinjuku/generate_attribute/' . $next . '/' . $product_id);

                    $listener = $image->add_listener('click');

                    $handler = $listener->add_handler('reload')
                            ->set_target('container-' . $next)
                            ->add_param_input($arr_param)
                            ->set_url(curl::base() . 'shinjuku/generate_attribute/' . $next . '/' . $product_id);

                    $handler = $listener->add_handler('custom')
                            ->set_js("
								var key=$(this).attr('id');
								$('#" . 'att_cat_' . $id . "').val(key);
							");
                }
                $index++;
            }
        }

        echo $app->render();
    }

}
