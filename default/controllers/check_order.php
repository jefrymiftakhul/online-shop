<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of check_order
 *
 * @author JIttron
 */
class Check_order_Controller extends ProductController{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $app = CApp::instance();
        $user = $app->user();
        $org_id = ccfg::get('org_id');
        
        $container = $app->add_div()->add_class('container check-order-container');
        $div_content_order = $app->add_div('content-order');
        
        $row = $container->add_div()->add_class('row');
        $col = $row->add_div()->add_class('col-lg-12');
        
        //$div_content_order = $row->add_div('content-order')->add_class('col-lg-12');
        
        $div_title = $col->add_div()->add_class('div-title');
        $div_input = $col->add_div()->add_class('div-input');
        //$div_content_order = $col->add_div('content-order');
        
        $div_title->add('<h2 class="title-check-order">Status Pengiriman</h2>');
        
        
        $div_input->add_control('booking_code', 'text')->set_placeholder('MASUKAN ORDER ID LALU KLIK TOMBOL CEK STATUS');
        $btn_check_status = $div_input->add_elm('a')->add_class('btn-check-status')->add('Cek Status');
        $btn_check_status
                ->add_listener('click')
                ->add_handler('reload')
                ->set_target('content-order')
                ->add_param_input(array('booking_code'))
                ->set_url(curl::base().'retrieve/invoice');
        
        
        
        
        echo $app->render();
    }
}
