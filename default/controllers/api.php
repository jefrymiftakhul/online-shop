<?php

class Api_Controller extends CController {

    public function __construct() {
        parent::__construct();
        $app = CApp::instance();
    }

    public function session($params) {
        $api = CApiServerSixtyTwoHallFamily::instance('session', array($params));
        echo $api->exec();
    }

    public function transaction($params) {
        $api = CApiServerSixtyTwoHallFamily::instance('transaction', array($params));
        echo $api->exec();
    }

    public function data($params) {
        $api = CApiServerSixtyTwoHallFamily::instance('data', array($params));
        
        $return = $api->exec();
        
        echo $return;
    }
    
    public function trevomember($params) {
        $api = CApiServerSixtyTwoHallFamily::instance('trevomember', array($params));
        echo $api->exec();
    }
    
    public function trevodriver($params) {
        $api = CApiServerSixtyTwoHallFamily::instance('trevodriver', array($params));
        echo $api->exec();
    }

    public function ittronmall($params) {
        
        if(!isset($_GET['noheader'])){
            header('content-type: application/json');
        }
        
        $api = CApiServerSixtyTwoHallFamily::instance('ittronmall', array($params));
        echo $api->exec();
    }
    
//        public function __call($type = null, $param = null) {
//            $params['type'] = $type;
//            $service = '';
//            $error = 0;
//            $error_message = '';
//            if (isset($param[0])) {
//                $service = $param[0];
//                $params['service'] = $service;
//            }
//            else {
//                $error++;
//                $error_message = "Invalid Url";
//            }
//            if ($error == 0) {
//                $api = CApiServerSixtyTwohallFamily::instance($params);
//                echo $api->exec();
//            }
//            if ($error > 0) {
//                echo $error_message;
//            }
//        }
}
