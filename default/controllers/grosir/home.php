<?php

/**
 * Description of home
 *
 * @author Ecko Santoso
 * @since 31 Agu 16
 */
class Home_Controller extends GrosirController
{
    private $product_per_row = 6;
    private $responsive_bootstrap = array(
        'col_lg' => 3,
    );
    private $page = 'grosir';
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $app = CApp::instance();
        $user = $app->user();
        $org_id = $this->org_id();
        $db = CDatabase::instance();
// ===============================================================================================================
        //         menu
        $link_list = $this->list_menu();
        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_link($link_list);
        $grosir_category = $app->add_div()->add_class('green-category-wrapper');
        $q = 'select * from product_category where status > 0 and parent_id is null and product_type_id = 11 and (org_id is null or org_id = '.$db->escape(CF::org_id()).')';

        $data_grosir_category = $db->query($q);

        //    getting full path
        // $current_menu = crouter::routed_uri();
        $current_menu = CFRouter::$controller;
        if ($data_grosir_category != null) {
            $grosir_category->add('<ul class="ul-green-category">');
            foreach ($data_grosir_category as $key => $value) {
                    $grosir_category->add('<li>');
                    $grosir_category->add('<a href="' . curl::base() . $this->page() . '/product/category/' . $value->url_key . '">');
                    $grosir_category->add(strtoupper(clang::__($value->name)));
                    $grosir_category->add('</a>');
                    $grosir_category->add('</li>');
            }
            $grosir_category->add('</ul>');
        }

        $container = $app->add_div()->add_class('container produk-lokal')->add_div()->add_class('row');
        $col_12 = $container->add_div()->add_class('col-xs-12');
        // Produk Grosir Terbaru
        $grosir_product_id = $db->query("select product_id from product where status > 0 and (org_id is null or org_id = ".$db->escape(CF::org_id()).") and status_confirm = 'CONFIRMED' and product_type_id = 11 order by created desc limit 10");
        $results = array();
        if (count($grosir_product_id) > 0) {
            foreach ($grosir_product_id as $key => $value) {
                $results[] = product::get_product($value->product_id);
            }
        }

        $grosir_terbaru = $col_12->add_div()->add_class('home-deal grosir-terbaru');
        $grosir_terbaru->add_div()->add(
            '<h4 class="deal-title">'
            . '<div class="deal-title-wrapper">'
            . clang::__('GROSIR TERBARU')
            . '</div>'
            . '</h4>');
        $grosir_terbaru->add_element('product-slider')->add_class('def-slider')
            ->set_slides($results)
            ->set_slide_to_show(4)
            ->set_slide_to_scroll(4)
            ->set_page($this->page);
// Produk Terlaris

        $grosir_product_id = $db->query("select product_id from product where status > 0 and (org_id is null or org_id = ".$db->escape(CF::org_id()).") and  status_confirm = 'CONFIRMED' and product_type_id = 11 order by created desc limit 10");
        $results = array();
        if (count($grosir_product_id) > 0) {
            foreach ($grosir_product_id as $key => $value) {
                $results[] = product::get_product($value->product_id);
            }
        }
        $grosir_terlaris = $col_12->add_div()->add_class('home-deal grosir-terlaris');
        $grosir_terlaris->add_div()->add(
            '<h4 class="deal-title">'
            . '<div class="deal-title-wrapper">'
            . clang::__('GROSIR TERLARIS')
            . '</div>'
            . '</h4>');
        $grosir_terlaris->add_element('product-slider')->add_class('def-slider')
            ->set_slides($results)
            ->set_slide_to_show(4)
            ->set_slide_to_scroll(4)
            ->set_page($this->page);
// Produk Termurah
        $grosir_product_id = $db->query("select product_id from product where status > 0 and (org_id is null or org_id = ".$db->escape(CF::org_id()).") and status_confirm = 'CONFIRMED' and product_type_id = 11 order by sell_price asc limit 10");
        $results = array();
        if (count($grosir_product_id) > 0) {
            foreach ($grosir_product_id as $key => $value) {
                $results[] = product::get_product($value->product_id);
            }
        }
        $grosir_termurah = $col_12->add_div()->add_class('home-deal grosir-termurah');
        $grosir_termurah->add_div()->add(
            '<h4 class="deal-title">'
            . '<div class="deal-title-wrapper">'
            . clang::__('GROSIR TERMURAH')
            . '</div>'
            . '</h4>');
        $grosir_termurah->add_element('product-slider')->add_class('def-slider')
            ->set_slides($results)
            ->set_slide_to_show(4)
            ->set_slide_to_scroll(4)
            ->set_page($this->page);

        echo $app->render();
    }
    public function category($category)
    {

    }
}
