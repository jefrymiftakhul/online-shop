<?php

/**
 * Description of product
 *
 * @author Ecko Santoso
 * @since 02 Sep 16
 */
class Product_Controller extends LuckyDayController {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function item($product_id) {
        $app = CApp::instance();
        $user = $app->user();
        $db = CDatabase::instance();
        $org_id = $this->org_id();
        $menu_list = $this->menu_list();
        $link_list = $this->list_menu();

        $error = 0;
        $error_message = '';

        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_menu($menu_list);
        $element_menu->set_link($link_list);
        $element_menu->set_url_menu(curl::base() . 'luckyday/product/category/');

        $container = $app->add_div()->add_class('container margin-top-20');
        if(!is_numeric($product_id)) {
            $product_id = cdbutils::get_value("select product_id from product where url_key= ".$db->escape($product_id));
        }
        
        if ($product_id == null) {
            curl::redirect(curl::base());
        }
        $msg = cmsg::flash_all();
        if (strlen($msg) > 0) {
            $container->add_div('div-info')->add($msg);
        }
        $q = "
                    select
                            pt.name as product_type_name,
                            p.name as product,
                            p.*,
                            pt.*
                    from
                            product as p
                            inner join product_type as pt on pt.product_type_id=p.product_type_id
                    where
                            p.status>0
							and is_active>0
                            and p.status_confirm='CONFIRMED'
                            and p.product_id=" . $db->escape($product_id) . "
                            and pt.name=" . $db->escape($this->page()) . "
            ";
        $product = $db->query($q);
        if ($product->count() == 0) {
            $error++;
            $error_message = "Product not found please select other product";
        }

        if ($error == 0) {
            $product_image = $container->add_div()->add_class('col-md-4 padding-0');
            $product_detail = $container->add_div()->add_class('col-md-4');
            $product_info = $container->add_div()->add_class('col-md-4 margin-top-20');

            // GALERY PRODUCT
            $data_galery_product = product::get_product_image($product_id);
            $q = "
                    select
                            pt.name as product_type_name,
                            p.name as product,
                            p.*,
                            pt.*
                    from
                            product as p
                            inner join product_type as pt on pt.product_type_id=p.product_type_id
                    where
                            p.status>0
							and is_active>0
                            and p.status_confirm='CONFIRMED'
                            and p.product_id=" . $db->escape($product[0]->product_id_lucky) . "
            ";
            $product_real=cdbutils::get_row($q);
            //$page_image=$product_real->product_type_name;
            $element_galery_product = $product_image->add_element('62hall-galery-product', 'galery-product-' . $product_id);
            $element_galery_product->set_images($data_galery_product);
            $element_galery_product->set_page('luckyday');


            // DETAIL PRODUCT
            $data_detail_product = product::get_product($product_id);
            $data_attribute_category = product::get_attribute_category_all($product_id);
            $element_detail_product = $product_detail->add_element('62hall-luckyday-detail', 'detail-luckyday-' . $product_id);

            $element_detail_product->set_key($product_id)
                    ->set_detail_list($data_detail_product)
                    ->set_attribute_list($data_attribute_category);

            // PRODUCT INFO
            $element_detail_info = $product_info->add_element('62hall-detail-info', 'detail-info' . $product_id);
            //share fb
            $org = org::get_org($org_id);
            $url = '';
            $http = 'http';
            if(isset($_SERVER['HTTPS'])) {
                $http = 'https';
                
            }
            if ($org) {
                $url = $http.'://' . $org->domain . '/luckyday/product/item/' . $product_id;
            }
            $url_image = '';
            $description = '';
            $q = "SELECT * FROM product WHERE product_id=" . $db->escape($product_id);
            $r = $db->query($q);
            if ($r->count() > 0) {
                $row = $r[0];
                $url_image = $row->file_path;
                $description = $row->description;
            }
            $facebook_app_id = ccfg::get('facebook_app_id');
            $additional_head = '
                                <meta property="fb:app_id" content="' . $facebook_app_id . '">
                                <meta property="og:url" content="' . $url . '">
                                <meta property="og:title" content="' . $data_detail_product['name'] . '-' . $org->name . '">
                                <meta property="og:site_name" content="' . $url . '">
                                <meta property="og:description" content="' . htmlspecialchars(strip_tags(carr::get($data_detail_product, 'overview'))) . '">
                                <meta property="og:type" content="article">
                                <meta property="og:image" content="' . image::get_image_url($data_detail_product['image_path'], 'view_all') . '">

                            ';
            $app->set_additional_head($additional_head);
            $element_detail_info->set_url_shared_facebook($url);
            $element_detail_info->set_url_shared_twitter($url);
            $element_detail_info->set_text_twitter($data_detail_product['name'] . '-' . $org->name);
            // OVERVIEW PRODUCT
//            $overview_product = $app->add_div()->add_class('container');
//            $element_overview = $overview_product->add_element('62hall-overview-product', 'tab-product');
//            $element_overview->set_key($product_id);
//            $app->add_br();


            // PICKED FOR YOU
//            $pickedforyou = $app->add_div()->add_class('container');
//            $pickedforyou->add('<h4 class="border-bottom-gray padding bold font-red">PRODUK TERKAIT</h4>');
//            $app->add_br();

            //$picked_for_you_list = deal::get_deal($org_id, $this->page(), $this->deal_pick_for_you);
            $picked_for_you_list = array();
            
            $q="
                select
                    pr.product_related_by_id as product_id
                from
                    product_related as pr
                    left join product as p on p.product_id=pr.product_id
                where
                    p.status>0
                    and pr.status>0
                    and p.is_active>0
                    and p.status_confirm='CONFIRMED'
                    and pr.product_id=".$db->escape($product_id)."
            ";    
            $r=$db->query($q);
            if($r->count()>0){
                foreach($r as $row){
                    $picked_for_you_list[]=product::get_product($row->product_id);
                }
            }
            $element_picked_for_you = $app->add_element('62hall-slide-product', 'picked-for-you');
            $element_picked_for_you->set_count_item(6)
                    ->set_page($this->page())
                    ->set_list_item($picked_for_you_list);

            $app->add_js("
                $('input').blur();
            ");
        } else {
            $container->add($error_message);
        }

        echo $app->render();
    }
}
