<?php

/**
 * Description of home
 *
 * @author Ecko Santoso
 * @since 31 Agu 16
 */
class Home_Controller extends LuckyDayController {
    
    private $_count_item = 12;

    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $app = CApp::instance();
        $user = $app->user();
        $org_id = $this->org_id();
        
        // menu
        $link_list = $this->list_menu();
        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_link($link_list);
        
        $winners = transaction::get_last_winner_lucky_day();
//        cdbg::var_dump($winners);
        
        // SLIDE
        $slide_list = slide::get_slide($org_id, $this->page());        
//        $slide_list = slide::get_slide($org_id, 'service');
        if (count($slide_list) > 0) {
            $element_slide_show = $app->add_element('62hall-slide-show', 'element_slide_show');
            $element_slide_show->set_type('luckyday');
            $element_slide_show->set_slides($slide_list);
        }
        
        // grid schema
        $main_container = $app->add_div()->add_class('container');
        
        $main_row = $main_container->add_div()->add_class('row luckyday-main-content');
        $left = $main_row->add_div()->add_class('col-md-3');
        $right = $main_row->add_div('page-product-category')->add_class('col-md-9');
        
        $main_winner = $left->add_div()->add_class('sidebar-widget');
        $winner_element = $main_winner->add_element('62hall-luckyday-winner');
        $winner_element->set_list($winners);
        
        // preparing for filter
        $options = array(
            'product_type'        => $this->page(),
            'visibility'          => 'catalog',
        );
        $list_attribute = product::get_attributes_array($options);
//        cdbg::var_dump($list_attribute);
//        die();
//        $list_attribute = $this->raw_list_attr();

        $min_price = product::get_min_price($options);
        $max_price = product::get_max_price($options);
        $min_price = 0;
        $max_price = 10000;
        if ($min_price == null) {
            $min_price = 0;
        }
        if ($max_price == null) {
            $max_price = 0;
        }
        $range_price = array(
            'min' => $min_price,
            'max' => $max_price,
        );

        $list_filter_product = array(
            'countdown'    => clang::__('Sisa Kupon'),
//            'min_price' => clang::__('Termurah'),
//            'max_price' => clang::__('Termahal'),
            'is_new'    => clang::__('Terbaru'),
//            'popular'   => clang::__('Popular'),
            'A-Z'       => clang::__('A-Z'),
            'Z-A'       => clang::__('Z-A'),
        );
        
        // filter Element
        $filter_div = $left->add_div()->add_class('border-1 padding-left-right10');
//        $element_filter  = $filter_div->add_element('62hall-luckyday-filter', 'luckyday-filter')
        $element_filter  = $filter_div->add_element('62hall-filter', 'luckyday-filter')
                                           ->set_list_filter_product($list_filter_product)
                                           ->set_list_filter($list_attribute)
                                           ->set_product_visibility('catalog')
                                           ->set_filter_page($this->page())
                                           ->set_filter_price(false);
//                                           ->set_min_price($range_price['min'])
//                                           ->set_max_price($range_price['max']);

        $filter_name = '';
        $right->add_listener('ready')
                         ->add_handler('reload')
                         ->set_target('page-product-category')
                         ->set_url(curl::base() . "reload/reload_product_filter?visibility=catalog&sortby=diskon&filter_page=" . $this->page() );
        
        echo $app->render();
    }
    
    public function reload_product() {
        
    }
    
    public function raw_data() {
        $app = CApp::instance();
        $http = 'http';
        if(isset($_SERVER['HTTPS'])) {
            $http = 'https';

        }
        $dummy_product = array(
            "product_id" => "192060",
            "product_type_id" => "5",
            "product_category_id" => "183",
            "vendor_id" => "104",
            "country_manufacture_id" => "94",
            "code" => "nmccompany",
            "name" => "Company Card",
            "url_key" => "company-card",
            "new_product_date" => "2016-09-15",
            "currency" => "idr",
            "purchase_price_nta" => null,
            "purchase_price" => null,
            "sell_price_nta" => "2500000",
            "sell_price" => "2600000",
            "sell_price_start" => null,
            "sell_price_end" => null,
            "ho_upselling" => "0",
            "ho_commission" => "0",
            "ho_commission_type" => null,
            "weight" => "1",
            "stock" => "0",
            "description" => "lorem ipsum",
            "overview" => "<p>Lorem ipsum rincian company card<\/p>\n",
            "spesification" => "",
            "faq" => "",
            "is_active" => "1",
            "is_available" => "1",
            "is_stock" => "0",
            "is_hot_item" => "0",
            "show_minimum_stock" => "0",
            "alert_minimum_stock" => "0",
            "parent_id" => null,
            "sku" => "nmccompany1",
            "product_data_type" => "simple",
            "status_product" => "FINISHED",
            "is_selection" => "0",
            "filename" => "company-card.png",
            "image_name" => "default_image_ProductImageParent_20160830_company-card.png",
            "file_path" => $http."://admin.mybigmall.local/res/show/NjI2MlJXUFNDXkJtX19XVVNtZkBZVkNRQntbU1FXZlNEV1hGaQAGAwACDgEGbVVdW0JXXE8fVVNEVhhCWFU=",
            "visibility" => "not_visibility_individually",
            "featured_image" => null,
            "description_voucher" => null,
            "is_voucher" => "0",
            "is_generate_code" => "0",
            "is_email" => "0",
            "image_fixed" => "0",
            "status" => "1",
            "quota" => "2000",
            "slot" => "450",
        );
        
        for($i=0; $i < 12; $i++){
            $element_product = $app->add_element('62hall-luckyday-product', 'product-luckyday-' . $i);
            $element_product
                    ->set_product_id($i)
                    ->set_column(4)
                    ->set_using_class_div_gold(true)
                    ->set_product($dummy_product);
        }
        
        echo $app->render();
    }
    
    public function raw_list_attr() {
        return array(
            "GDCOLOR" => array(
                "attribute_category_id"      => "12",
                "attribute_category_name"    => "Warna",
                "attribute_category_type"    => "colorpicker",
                "attribute_category_url_key" => "color_atk",
                "attribute"                  => array(
                    99  => array(
                            "attribute_key"     => "brown",
                            "attribute_url_key" => "brown",
                            "file_path"         => NULL,
                            "hex_code"         => "#FF530D"
                        ),
                    100 => array(
                            "attribute_key"     => "orange",
                            "attribute_url_key" => "orange",
                            "file_path"         => NULL,
                            "hex_code"          => "#3D0CE8"
                        ),
                    101 => array(
                            "attribute_key"     => "magenta",
                            "attribute_url_key" => "magenta",
                            "file_path"         => NULL,
                            "hex_code"         => "#E89F0C"
                        ),
                    102 => array(
                            "attribute_key"     => "ivory",
                            "attribute_url_key" => "ivory",
                            "file_path"         => NULL,
                            "hex_code"         => "#09FFF0"
                        ),
                ),
            ),
        );
    }
}
