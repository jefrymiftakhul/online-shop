<?php

/**
 * Description of home
 *
 * @author Ecko Santoso
 * @since 31 Agu 16
 */
class Homebak_Controller extends LuckyDayController {
    
    private $_count_item = 12;

    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $app = CApp::instance();
        $user = $app->user();
        $org_id = $this->org_id();
        
        // menu
        $link_list = $this->list_menu();
        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_link($link_list);
        
        
        // SLIDE
//        $slide_list = slide::get_slide($org_id, $this->page());
//        $slide_list = slide::get_slide($org_id, 'service');
//        if (count($slide_list) > 0) {
//            $element_slide_show = $app->add_element('62hall-slide-show', 'element_slide_show');
//            $element_slide_show->set_type('luckyday');
//            $element_slide_show->set_slides($slide_list);
//        }
        
        // grid schema
        $main_container = $app->add_div()->add_class('container');
        $main_row = $main_container->add_div()->add_class('row');
        $left = $main_row->add_div()->add_class('col-md-3');
        $right = $main_row->add_div('luckyday-product-div')->add_class('col-md-9');
        
        // filter element
        $left->add('FILTER HERE');
//        $filter_elm = $left->add_element('62hall-filter-luckyday', 'product-filter');
        
        // product list
        $right->add_listener('ready')
                ->add_handler('reload')
                ->set_target('luckyday-product-div')
                ->set_url(curl::base() . "luckyday/home/raw_data");
        
        echo $app->render();
    }
    
    public function reload_product() {
        
    }
    
    public function raw_data() {
        $app = CApp::instance();
        
        $dummy_product = array(
            "product_id" => "192060",
            "product_type_id" => "5",
            "product_category_id" => "183",
            "vendor_id" => "104",
            "country_manufacture_id" => "94",
            "code" => "nmccompany",
            "name" => "Company Card",
            "url_key" => "company-card",
            "new_product_date" => "2016-09-15",
            "currency" => "idr",
            "purchase_price_nta" => null,
            "purchase_price" => null,
            "sell_price_nta" => "2500000",
            "sell_price" => "2600000",
            "sell_price_start" => null,
            "sell_price_end" => null,
            "ho_upselling" => "0",
            "ho_commission" => "0",
            "ho_commission_type" => null,
            "weight" => "1",
            "stock" => "0",
            "description" => "lorem ipsum",
            "overview" => "<p>Lorem ipsum rincian company card<\/p>\n",
            "spesification" => "",
            "faq" => "",
            "is_active" => "1",
            "is_available" => "1",
            "is_stock" => "0",
            "is_hot_item" => "0",
            "show_minimum_stock" => "0",
            "alert_minimum_stock" => "0",
            "parent_id" => null,
            "sku" => "nmccompany1",
            "product_data_type" => "simple",
            "status_product" => "FINISHED",
            "is_selection" => "0",
            "filename" => "company-card.png",
            "image_name" => "default_image_ProductImageParent_20160830_company-card.png",
            "file_path" => "http://admin.mybigmall.local/res/show/NjI2MlJXUFNDXkJtX19XVVNtZkBZVkNRQntbU1FXZlNEV1hGaQAGAwACDgEGbVVdW0JXXE8fVVNEVhhCWFU=",
            "visibility" => "not_visibility_individually",
            "featured_image" => null,
            "description_voucher" => null,
            "is_voucher" => "0",
            "is_generate_code" => "0",
            "is_email" => "0",
            "image_fixed" => "0",
            "status" => "1",
            "quota" => "2000",
            "slot" => "450",
        );
        
        for($i=0; $i < 12; $i++){
            $element_product = $app->add_element('62hall-luckyday-product', 'product-luckyday-' . $i);
            $element_product
                    ->set_product_id($i)
                    ->set_column(4)
                    ->set_using_class_div_gold(true)
                    ->set_product($dummy_product);
        }
        
        echo $app->render();
    }
}
