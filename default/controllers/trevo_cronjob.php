<?php

    class Trevo_Cronjob_Controller extends CController {

        public function __construct() {
            parent::__construct();
        }        

        public function cancel_order() {
            $db = CDatabase::instance();
            $q = "select tt.transaction_trevo_id, tt.transaction_id
            from transaction t
            left join transaction_trevo tt on t.transaction_id = tt.transaction_id
            where t.status > 0 
            and tt.status > 0                        
            and (tt.order_status = 'PENDING')";
            $r = $db->query($q);
            if ($r != null) {
                foreach ($r as $k => $v) {
                    $transaction_id = cobj::get($v, 'transaction_id');
                    $transaction_trevo_id = cobj::get($v, 'transaction_trevo_id');
                    $where = array(
                        'transaction_trevo_id' => $transaction_trevo_id,
                        'transaction_id' => $transaction_id,
                    );
                    trevo::cancel_order($where);
                }
            }
        }

    }
    