<?php

    /**
     *
     * @author Riza 
     * @since  Jan 8, 2015
     * @license http://ittron-indonesia.com ITTron Indonesia
     */
    class Retrieve_Controller extends ProductController {

        const __CONTROLLER = 'retrieve/';

        public function __construct() {
            parent::__construct();
        }

        public function invoice($booking_code = NULL, $email = null, $member_id = null) {
            $app = CApp::instance();
            $db = CDatabase::instance();
            $org_id = $this->org_id();
            //$menu_list = $this->menu_list();
            //$link_list = $this->list_menu();
            $menu_list = $this->menu_list();
            $link_list = $this->list_menu();


            // SHOPPING CART 
            $app->add_listener('ready')
                    ->add_handler('reload')
                    ->set_target('shopping-cart')
                    ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

//            $product_category = product_category::get_product_category_menu($this->page());
//            // Menu
//            $menu_list = array(
//                'name' => 'KATEGORI',
//                'menu' => $product_category
//            );
//
//            $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
//            $element_menu->set_menu($menu_list)
//                    ->set_link($link_list);

            $request = array_merge($_POST, $_GET);

            if ( empty($booking_code) ) {
                $booking_code = carr::get($request, 'nomor-pesanan');
                $email = carr::get($request, 'email');
            }

//            $title_header = $container->add_div()->add_class('col-md-12 bold font-size24')
//                    ->add(clang::__('Terima Kasih'));
//            
//            $header = $container->add_div()->add_class('col-md-12 border-1 margin-bottom-20');
//            $container_header = $header->add_div()->add_class('col-md-12 padding-0 margin-top-20 margin-bottom-20')
//                    ->add(clang::__('Pesanan anda telah diterima. Pesanan Anda akan segera diproses setelah pembayaran anda telah dikonfirmasi.'));
//          


            $query = "select
                        *
                      from
                        transaction
                      where
                        status > 0
                        and org_id = " . $db->escape($org_id) . "
                        and booking_code = " . $db->escape($booking_code) . "
                        and email = " . $db->escape($email) . "
                    ";
            $data = cdbutils::get_row($query);


            if ( count($data) > 0 ) {
                $element_invoice = $app->add_element('62hall-invoice', 'element_invoice');
                $element_invoice->set_booking_code($booking_code);
            }
            else {

                $container = $app->add_div()->add_class('container margin-top-30 margin-bottom-30');
                $row = $container->add_div()->add_class('row');

                $icon = $row->add_div()->add_class('col-md-3');
                $image_send = curl::base() . 'application/lapakbangunan/default/media/img/imbuilding/send_message.png';
                $icon->add('<img src="' . $image_send . '" width="180px" />');

                $confirm = $row->add_div()->add_class('col-md-9');

                $confirm->add_div()->add_class('bold font-size24')->add('Invoice Anda tidak ditemukan!');


                $backtohome = $container->add_div()->add_class('col-md-2 col-md-offset-5');
                $button = $backtohome->add_action()
                        ->set_label(clang::__('kembali ke home'))
                        ->add_class('btn-62hallfamily bg-red border-3-red upper font-size20')
                        ->set_link(curl::base());
            }

            echo $app->render();
        }

        public function status_order($transaction_id) {
            $app = CApp::instance();
            $db = CDatabase::instance();
            $org_id = $this->org_id();
//            $menu_list = $this->menu_list();
//            $link_list = $this->list_menu();
            $menu_list = $this->menu_list();
            $link_list = $this->list_menu();


            // SHOPPING CART 
            $app->add_listener('ready')
                    ->add_handler('reload')
                    ->set_target('shopping-cart')
                    ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

            $product_category = product_category::get_product_category_menu('product');

            // Menu

            $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
            $element_menu->set_menu($menu_list)
                    ->set_link($link_list);

            $container = $app->add_div()->add_class('container');
            $row_container = $container->add_div()->add_class('row');

            $title = $row_container->add_div()->add_class('col-md-12 bold font-size24 margin-top-30 margin-bottom-10')
                    ->add('Status Pesanan');

            $content = $row_container->add_div()->add_class('col-md-12 margin-bottom-10');

            $content->add('<table class="table-62hallfamily">');
            $content->add('<tr>');
            $content->add('<th width="30px">');
            $content->add('#');
            $content->add('</th>');
            $content->add('<th>');
            $content->add('Keterangan');
            $content->add('</th>');
            $content->add('<th>');
            $content->add('Status');
            $content->add('</th>');

            $data_transaction = transaction::get_transaction('transaction_id', $transaction_id);
            $data_transaction_detail = transaction::get_transaction_detail('transaction_id', $transaction_id);

            $query = "
                    select 
                        *
                    from 
                        transaction_payment
                    where 
                    status > 0
                    and transaction_id = " . $db->escape($transaction_id) . "
                    ";

            $data_payment = array();
            $r = $db->query($query);
            if ( $r->count() > 0 ) {
                foreach ( $r as $row ) {
                    $data_payment[] = $row;
                }
            }

            $no = 0;
            foreach ( $data_payment as $index => $row_payment ) {

                if ( $index == 0 ) {
                    $no++;
                    $content->add('<tr>');
                    $content->add('<td>');
                    $content->add($no);
                    $content->add('</td>');
                    $content->add('<td>');
                    $content->add('Tanggal : <br/>' . ctransform::format_datetime($row_payment->date) . '<br>');
                    $content->add('Kode Transaksi : <br/>' . $data_transaction[0]->booking_code . '<br/>');
                    foreach ( $data_transaction_detail as $transaction_detail ) {
                        $content->add('Nama Barang <br/>' . $transaction_detail->name . '<br>');
                    }
                    $content->add('Total:  <br/>Rp. ' . ctransform::format_currency($row_payment->total_payment));
                    $content->add('</td>');
                    $content->add('<td>');
                    $content->add($row_payment->payment_status . '<br>');
                    $content->add('(' . ctransform::format_datetime($row_payment->date) . ')');
                    $content->add('</td>');
                    $content->add('</tr>');
                }
                else {
                    $no++;
                    $content->add('<tr>');
                    $content->add('<td>');
                    $content->add($no);
                    $content->add('</td>');
                    $content->add('<td>');
                    foreach ( $data_transaction_detail as $transaction_detail ) {
                        $content->add($transaction_detail->name . '<br>');
                    }

                    $content->add('Rp. ' . ctransform::format_currency($row_payment->total_payment));
                    $content->add('</td>');
                    $content->add('<td>');

                    $content->add($row_payment->payment_status . '<br>');
                    $content->add('(' . ctransform::format_datetime($row_payment->date) . ')');

                    $content->add('</td>');
                    $content->add('</tr>');
                }
            }

            $query = "
                    select 
                        *
                    from 
                        transaction_payment
                    where 
                    status > 0
                    and transaction_id = " . $db->escape($transaction_id) . "
                    and date_success is not null
                    ";

            $data_payment_success = cdbutils::get_row($query);

            if ( count($data_payment_success) > 0 ) {
                if ( $data_payment_success->payment_type != 'cod' ) {
                    $no++;

                    $content->add('<tr>');
                    $content->add('<td>');
                    $content->add($no);
                    $content->add('</td>');
                    $content->add('<td>');
                    $content->add('Tanggal : <br/>' . ctransform::format_datetime($row_payment->date_success) . '<br>');
                    $content->add('Kode Pembayaran : ' . $row_payment->payment_code . '<br/>');
                    foreach ( $data_transaction_detail as $transaction_detail ) {
                        $content->add('Nama Barang <br/>' . $transaction_detail->name . '<br>');
                    }
                    $content->add('Total:  <br/>Rp. ' . ctransform::format_currency($row_payment->total_payment));
                    $content->add('</td>');
                    $content->add('<td>');
                    $content->add($row_payment->payment_status . '<br>');
                    $content->add('(' . ctransform::format_datetime($row_payment->date_success) . ')');
                    $content->add('</td>');
                    $content->add('</tr>');
                }
            }

            $data_transaction_shipping = transaction::get_transaction_shipping('transaction_id', $transaction_id);

            if ( count($data_transaction_shipping) > 0 ) {
                foreach ( $data_transaction_shipping as $transaction_shipping ) {
                    $no++;

                    $content->add('<tr>');
                    $content->add('<td>');
                    $content->add($no);
                    $content->add('</td>');
                    $content->add('<td>');
                    $content->add($transaction_shipping->description . '<br/>');
                    foreach ( $data_transaction_detail as $transaction_detail ) {
                        $content->add($transaction_detail->name . '<br>');
                    }
                    $content->add('Total:  <br/>Rp. ' . ctransform::format_currency($row_payment->total_payment));
                    $content->add('</td>');

                    $shipping_status = NULL;
                    if ( $transaction_shipping->shipping_status == 'ON_SHIPPING' ) {
                        $shipping_status = 'DI KIRIM';
                    }

                    if ( $transaction_shipping->shipping_status == 'DELIVERED' ) {
                        $shipping_status = 'SAMPAI';
                    }

                    $content->add('<td>');
                    $content->add($shipping_status . '<br>');
                    $content->add('(' . ctransform::format_datetime($transaction_shipping->created) . ')');
                    $content->add('</td>');
                    $content->add('</tr>');
                }
            }

            $content->add('</table>');


            $footer = $row_container->add_div()->add_class('col-md-12 margin-top-10 margin-bottom-30');

            $action = $footer->add_action()
                    ->set_label(clang::__('LANJUTKAN BELANJA'))
                    ->add_class('btn-imlasvegas-secondary  btn-primary pull-right')
                    ->set_link(curl::base());

            $action = $footer->add_action()
                    ->set_label(clang::__('KEMBALI'))
                    ->add_class('btn-imlasvegas-secondary  btn-primary pull-right margin-right-10')
                    ->set_link(curl::base() . 'retrieve/invoice/' . $data_transaction[0]->booking_code . '/' . $data_transaction[0]->email);

            echo $app->render();
        }

    }
    