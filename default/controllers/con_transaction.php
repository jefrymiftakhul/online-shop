<?php

    /**
     *
     * @author Seians0077
     * @since  Jun 16, 2016
     * @license http://piposystem.com Piposystem
     */
    class Cron_Transaction_controller extends SixtyTwoHallFamilyController{

        public function __construct() {
            parent::__construct();
        }

        public function cron_closing_auction() {
            $app = CApp::instance();
            $transaction= New CTransaction;
            $transaction->cron();
        }

    }