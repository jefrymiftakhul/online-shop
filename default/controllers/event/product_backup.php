<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Jun 13, 2016
     */
    class product_Controller extends LokalController {

        public function __construct() {
            parent::__construct();
        }

        protected $preview = false;

        public function index($product_id=null) {
            $app = CApp::instance();
            $session = Session::instance();
            $request = $_GET;
            $err_code = 0;
            $err_message = '';
            if ($err_code == 0) {
                if (strlen($product_id) == 0) {
                    $err_code++;
                    $err_message = 'ERR[1] ' . clang::__('Product not found');
                }
            }

            if ($err_code == 0) {
                try {
                    $product_detail = product::get_lokal_details($product_id);
                }
                catch (Exception $exc) {
                    $err_code++;
                    $err_message = $exc->getMessage();
                }
            }
			// menu
			$link_list = $this->list_menu();
			$element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
			$element_menu->set_link($link_list);

            $container = $app->add_div()->add_class('container');
            $prod_det_container = $container->add_div()->add_class('lokal-detail-container produk-lokal');
            if ($err_code == 0) {
                $product_name = carr::get($product_detail, 'name');
                $app->title($product_name);
                if ($this->preview) {
                    prelove_product::add_user_view($product_id);
                }  

                // $product_image = $app->add_div()->add_class('container')->add_div()->add_class('row');
                // $data_galery_product = product::get_product_image($product_id);
                // $element_galery_product = $product_image->add_element('62hall-galery-product', 'galery-product-' . $product_id);
                // $element_galery_product->set_images($data_galery_product);

                // add element product details
                $product_detail_element = $prod_det_container->add_element('lokal-product-details')
                    ->set_product($product_detail);
                $product_detail_element->set_preview($this->preview);
				$product_detail_element->set_url_product('lokal/product/index/'.$product_id);
            }
            if ($err_code > 0) {
                // show page product error
                $prod_det_container->add_element('product-not-found');
            }
            echo $app->render();
        }

        public function preview($product_slug, $product_id) {
            $this->preview = true;
            $this->index($product_slug, $product_id);
        }

        protected $state_from = null;
        protected $category_slug = null;
        public function category($category_slug = null) {
            if ($category_slug !== null) {
                $app       = CApp::instance();
                $user      = $app->user();
                $db        = CDatabase::instance();
                $org_id    = $this->org_id();
                $menu_list = $this->menu_list();
                $link_list = $this->list_menu();
                CFBenchmark::start('benchmark_category');
                $product_type_id=cdbutils::get_value('select product_type_id from product_type where name='.$db->escape($this->page()));
                $category=cdbutils::get_row("select * from product_category where status>0 and product_type_id = ".$db->escape($product_type_id)." and url_key=".$db->escape($category_slug));
                
                if($category==null){
                    curl::redirect('prelove/home');
                }
                $category_id=cobj::get($category,'product_category_id');
                // SHOPPING CART
                $app->add_listener('ready')
                    ->add_handler('reload')
                    ->set_target('shopping-cart')
                    ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

                // Menu
                $data_product_category = product_category::get_product_category_menu($this->page());

                $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
                $element_menu->set_menu($menu_list);
                $element_menu->set_link($link_list);

                $container_menu_sm = $app->add_div()->add_class('visible-xs visible-sm container');

                $container               = $app->add_div()->add_class('container');
                $category_menu_container = $container->add_div()->add_class('col-md-4 padding-0 margin-30');
                $category_menu           = $category_menu_container->add_div()->add_class('categories hidden-sm hidden-xs border-1 padding-left-right10');
                $category_container      = $container->add_div()->add_class('col-md-8 margin-30 padding-0');

                $first_parent = product_category::get_first_parent_category($category_id);
                if ($first_parent == null) {
                    curl::redirect(curl::base());
                    return;
                }
                // CATEGORY ADVERTISE
                $list_image_cms_product_category = cms_product_category::get_cms_product_category($org_id, $category_id);
                $advertise_category              = null;
                if (count($list_image_cms_product_category) == 0 && $first_parent != null) {
                    $list_image_cms_product_category = cms_product_category::get_cms_product_category($org_id, $first_parent->product_category_id);
                }

                if (count($list_image_cms_product_category) > 0) {

                    $advertise_category         = $category_container->add_div('advertise-category')->add_class('col-md-12 padding-0');
                    $element_advertise_category = $advertise_category->add_element('62hall-category-advertise', 'category-advertise');
                    $element_advertise_category->set_list_advertise($list_image_cms_product_category);
                }

                $title                     = $category_container->add_div()->add_class('col-md-12 padding-0');
                $data_category_menu_choise = product_category::get_parent_category($category_id);

                $data_category_menu_choise_name = '';
                if ($data_category_menu_choise != null) {
                    $data_category_menu_choise_name = $data_category_menu_choise->name;
                }
                $title->add('<h4 class="border-bottom-gray bold">' . $data_category_menu_choise_name . '</h4>');

                $product_category = $category_container->add_div('page-product-category')->add_class('col-md-12 padding-0');

                // CATEGORY MENU

                $category_menu_list    = product_category::get_product_category_menu($this->page(), $first_parent->product_category_id);
                $child_menu_list       = product_category::get_product_category_menu($this->page(), $category_id);
                $element_category_menu = $category_menu->add_element('62hall-category-menu', 'category-menu-' . $category_id);
                $element_category_menu->set_key($category_id)
                                      ->set_head_key($first_parent->product_category_id)
                                      ->set_head_menu($first_parent->name)
                                      ->set_list_menu($category_menu_list);

                $list = array();
                if (count($child_menu_list) > 0) {
                    $list[''] = "Pilih Subcategory ...";
                    foreach ($child_menu_list as $key => $value) {
                        $sm_category_id = carr::get($value, 'category_id');
                        $sm_name        = carr::get($value, 'name');

                        $list[$sm_category_id] = $sm_name;
                    }
                }

                $container_menu_sm_wrapper = $container_menu_sm->add_div()->add_class('menu-sm-wrapper col-md-12');
                $container_menu_sm_wrapper->add('<h4 class="bold">' . $data_category_menu_choise_name . '</h4>');

                if (count($child_menu_list) > 0) {
                    $control  = $container_menu_sm_wrapper->add_div()->add_class('form-62hallfamily')->add_control('sm_category_select', 'select')->add_class('select-category-menu select-62hallfamily')->set_list($list)->set_value($category_id);
                    $listener = $control->add_listener('change');
                    $listener->add_handler('custom')
                             ->set_js("
                                var sm_category = jQuery('#sm_category_select').val();
                                window.location = '" . curl::base() . "prelove/product/category/'+sm_category;
                            ");
                }

                // CATEGORY FILTER
                //$data_product = cms_product::get_page_category($org_id, $this->page(), $category_id);

                $options = array(
                    'product_type'        => $this->page(),
                    'product_category_id' => $category_id,
                    'visibility'          => 'catalog',
                );
        //        $products = product::get_result($options);
                $list_attribute = product::get_attributes_array($options);

                $benchmark = CFBenchmark::get('benchmark_category');
                if (isset($_GET['benchmark'])) {
                    cdbg::var_dump($benchmark);
                }
        //        $arr_product_id = array();
                //        $p = 0;
                //        foreach ($products as $product) {
                //            $product_id = cobj::get($product, 'product_id');
                //
                //            $arr_product_id[] = $product_id;
                //            if($p>60) {
                //                break;
                //            }
                //            $p++;
                //        }
                //
                //        $implode_product_id = implode(',', $arr_product_id);
                //
                //        $app->add_control('arr_product_id', 'hidden')
                //                ->set_value($implode_product_id);
                //
                //        $list_attribute = product::get_attribute_category_multi_product($arr_product_id, 'catalog');
                //        $range_price = product::get_range_price($arr_product_id);
                $min_price = product::get_min_price($options);
                $max_price = product::get_max_price($options);
                if ($min_price == null) {
                    $min_price = 0;
                }
                if ($max_price == null) {
                    $max_price = 0;
                }
                $range_price = array(
                    'min' => $min_price,
                    'max' => $max_price,
                );

                $list_filter_product = array(
                    'diskon'    => clang::__('Diskon'),
                    'min_price' => clang::__('Termurah'),
                    'max_price' => clang::__('Termahal'),
                    'is_new'    => clang::__('Terbaru'),
                    'popular'   => clang::__('Popular'),
                    'A-Z'       => clang::__('A-Z'),
                    'Z-A'       => clang::__('Z-A'),
                );
                $q = "
                        select
                            *
                        from
                            product_category
                        where
                            product_category_id=" . $db->escape($category_id) . "
                    ";
                $r            = $db->query($q);
                $category_lft = '';
                $category_rgt = '';
                if ($r->count() > 0) {
                    $row          = $r[0];
                    $category_lft = $row->lft;
                    $category_rgt = $row->rgt;
                }
                $category_filter = $category_menu_container->add_div()->add_class('categories border-1 padding-left-right10 margin-top-30');
                $element_filter  = $category_filter->add_element('62hall-filter', 'category-filter')
                                                   ->set_key($category_id)
                                                   ->set_list_filter_product($list_filter_product)
                                                   ->set_list_filter($list_attribute)
                                                   ->set_product_visibility('catalog')
                                                   ->set_filter_category_lft($category_lft)
                                                   ->set_filter_category_rgt($category_rgt)
                                                   ->set_filter_page($this->page())
                                                   ->set_min_price($range_price['min'])
                                                   ->set_max_price($range_price['max']);

                $filter_name = '';
                $product_category->add_listener('ready')
                                 ->add_handler('reload')
                                 ->set_target('page-product-category')
                                 //->add_param_input('arr_product_id')
                                 ->set_url(curl::base() . "reload/reload_product_filter?source=category&visibility=catalog&sortby=diskon&filter_page=" . $this->page() . '&filter_category_lft=' . $category_lft . '&filter_category_rgt=' . $category_rgt);

                echo $app->render();
            }
        }       

        public function search() {
            $app = CApp::instance();
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = "";
            $list_view_per_page = array(
                '20' => '20',
                '40' => '40',
                '60' => '60',
            );

            $category_slug = '';
            $options = $this->options;
            $state_from_category = false;
            if ($this->state_from == 'category') {
                $category_slug = $this->category_slug;
                $state_from_category = true;
            }

            $request = $_GET;
            $sort_by = carr::get($request, 'sord', prelove_product::__ORD_LASTOFFER);
            $q = carr::get($request, 'q', null);
            $page = carr::get($request, 'page', 1);
            $item_per_page = carr::get($request, 'per_page', 20);
            if (!in_array($item_per_page, $list_view_per_page)) {
                if (isset($_GET['debug_page'])) {
                    // do nothing
                }
                else {
                    $item_per_page = 20;
                }
            }

            // paging limitation
            $limit_start = ($page - 1) * $item_per_page;
            $limit_end = $page * $item_per_page;
            $options['limit_start'] = $limit_start;
            $options['limit_end'] = $limit_end;
            $options['sord'] = $sort_by;
            $options['keyword'] = $q;

            $container = $app->add_div()->add_class('result-container');

            // header result
            $title_content = '';
            if ($state_from_category) {
                $product_category = prelove_product_category::get_by_slug($category_slug);
                if ($product_category == null) {
                    $err_code++;
                    $err_message = clang::__('This category doesnt exists');
                }
                if ($err_code == 0) {
                    $product_category_id = cobj::get($product_category, 'product_category_id');					
                    $title_page = cobj::get($product_category, 'name');					

                    // add product_category_id to options
                    $options['product_category_id'] = $product_category_id;
                    $title_content = cobj::get($product_category, 'name');
                    if (strlen($title_page) > 0) {
                        $app->title($title_page);
                    }
                    else {
                        $app->title($title_content);
                    }
                }
            }

            $result_header = $container->add_div()->add_class('result-header');
            $result_header_row = $result_header->add_div()->add_class('row');

            // left side result header, title will be filled below after query
            $title_wrapper = $result_header_row->add_div()->add_class('col-lg-8 col-md-6 col-sm-6 col-xs-12');

            // view per page

            $view_perpage_wrapper = $result_header_row->add_div()->add_class('col-lg-2 col-md-3 col-sm-3 col-xs-6 view-perpage-wrapper');
            $view_perpage_wrapper->add_div()->add_class('view-perpage-label')->add(clang::__('Show'));
            $view_perpage_wrapper->add_div()->add_class('view-perpage-control')->add_control('view_per_page', 'dropdown')
                    ->set_list($list_view_per_page)->set_masking(true)
                    ->set_value($item_per_page)->add_class('dropdown-prelove');

            // sort by container
            $filter_sort_by = prelove_product::get_order_by();
            $list_sort_by = array();
            foreach ($filter_sort_by as $sort_by_key => $sort_by_val) {
                $list_sort_by[$sort_by_key] = array(
                    'value' => clang::__($sort_by_val),
                    'mask' => clang::__($sort_by_val),
                );
            }
            $sort_by_wrapper = $result_header_row->add_div()->add_class('col-lg-2 col-md-3 col-sm-3 col-xs-6 sort-by-wrapper');
            $sort_by_wrapper->add_div()->add_class('sort-by-label')->add(clang::__('Sort By'));
            $sort_by_wrapper->add_div()->add_class('sort-by-control')->add_control('sort-by', 'dropdown')->set_list($list_sort_by)->set_masking(true)
                    ->set_value($sort_by)->add_class('dropdown-prelove');

            $container_row = $container->add_div()->add_class('row result-body');

            // left container, FILTER
            $left_container = $container_row->add_div()->add_class('col-lg-3 col-md-4 left-container');
            $left_container = $left_container->add_form()->add_class('form-prelove');
            $left_container->add_div()->add_class('title')->add(clang::__('SHOP BY'));

//            if ($state_from_category) {
            $left_row_category = $left_container->add_div()->add_class('category filter-box');
            $left_row_category->add_div()->add_class('subtitle-wrapper')
                    ->add_div()->add_class('subtitle')->add(clang::__('Categories'));
            $category_content = $left_row_category->add_div()->add_class('category-content');
            $categories = prelove_product::get_all_category();
            foreach ($categories as $k => $category) {
                $selected_menu = false;
                $url_key = cobj::get($category, 'url_key');
                $name = cobj::get($category, 'name');
                $subnav = cobj::get($category, 'subnav');
                $name = clang::__($name);
                $url = curl::base() . 'product/category/' . $url_key;

                // find selected menu
                if ($url_key == $category_slug) {
                    $selected_menu = true;
                    $title_category = $name;
                }

                $masking = $name;
                if (isset($subnav) && count($subnav) > 0) {
                    if ($selected_menu) {
                        $masking = $name . ' <i class="fa fa-caret-down"></i>';
                    }
                    else {
                        $masking = $name . ' <i class="fa fa-caret-right"></i>';
                    }
                }

                $selected_class = '';
                if ($selected_menu) {
                    $selected_class = ' selected ';
                    $selected_sub_category = $subnav;
                }

                $list_product_category[$url_key] = array(
                    'value' => $name,
                    'mask' => $masking,
                    'link' => $url,
                    'class' => 'submenu ' . $url_key . $selected_class
                );
            }
            $category_content->add_control('filter-prod-category', 'dropdown')
                    ->add_class('dropdown-prelove menu-open hover-no-bg')->set_list($list_product_category)->set_value(0)
                    ->set_placeholder('Categories')->set_masking(true)
                    ->set_opt_on_close(false);
//            }
            // <editor-fold defaultstate="collapsed" desc="FILTER">
            // filter deal type (dt)
            $q_deal_types = array();
            $q_deal_type = carr::get($request, 'dt');
            if (strlen($q_deal_type) > 0) {
                $q_deal_types = explode(',', $q_deal_type);
                $options['deal_types'] = $q_deal_types;
            }

            $left_row_category = $left_container->add_div()->add_class('deal-types filter-box')
                    ->add_attr('_key', 'dt');
            $left_row_category->add_div()->add_class('subtitle-wrapper')
                    ->add_div()->add_class('subtitle')->add(clang::__('Deal Types'));
            $category_content = $left_row_category->add_div()->add_class('deal-types-content');

            $list_deal_types = prelove_product::get_deal_types();
            $attr_dropdown = $left_row_category->add_control('dropdown-deal-types', 'dropdown')
                    ->add_class('dropdown-prelove menu-open hover-no-bg')->set_placeholder(clang::__('Deal Type'))->set_masking(true)
                    ->set_opt_on_close(false);
            foreach ($list_deal_types as $deal_type_key => $deal_type) {
                $checked = false;
                if (in_array($deal_type_key, $q_deal_types)) {
                    $checked = true;
                }
                $attr_dropdown->add_list_control('', 'checkbox')->set_label($deal_type)
                        ->add_class('item filter-item')->set_value($deal_type_key)->set_label_wrap(true)
                        ->set_checked($checked);
            }

            // filter condition
            $q_conditions = array();
            $q_condition = carr::get($request, 'cond');
            if (strlen($q_condition) > 0) {
                $q_conditions = explode(',', $q_condition);
                $options['conditions'] = $q_conditions;
            }

            $left_row_category = $left_container->add_div()->add_class('condition filter-box')
                    ->add_attr('_key', 'cond');
            $left_row_category->add_div()->add_class('subtitle-wrapper')
                    ->add_div()->add_class('subtitle')->add(clang::__('Condition'));
            $category_content = $left_row_category->add_div()->add_class('condition-content');

            $list_conditions = prelove_product::get_conditions();
            $attr_dropdown = $left_row_category->add_control('dropdown-condition', 'dropdown')
                    ->add_class('dropdown-prelove menu-open hover-no-bg')->set_placeholder(clang::__('Condition'))->set_masking(true)
                    ->set_opt_on_close(false);
            foreach ($list_conditions as $condition_key => $condition_v) {
                $checked = false;
                if (in_array($condition_key, $q_conditions)) {
                    $checked = true;
                }
                $attr_dropdown->add_list_control('', 'checkbox')->set_label($condition_v)
                        ->add_class('item filter-item')->set_value($condition_key)->set_label_wrap(true)
                        ->set_checked($checked);
            }
            // </editor-fold>
            // right container, RESULT CONTENT
            $result_content = $container_row->add_div()->add_class('col-lg-9 col-md-8');
            $result = prelove_product::get_results($options);
            $product_results = $result['products'];
            $total_result = $result['total'];

            $element_search_result = $result_content->add_element('product-search-results')->set_product_result($product_results)
                            ->set_item_per_page($item_per_page)->set_total_result($total_result)
                            ->set_current_page($page)->set_query($q);
            $element_search_result->set_url(curl::base() . 'prelove/product/search');
            if ($state_from_category) {
                $element_search_result->set_url(curl::base() . 'prelove/product/category/' . $category_slug);
            }
            if (strlen($this->url_search_result) > 0) {
                $element_search_result->set_url($this->url_search_result);
            }

            // fill title with total record query
            if (!$state_from_category) {
                if ($q !== null) {
                    $title_content = $total_result . ' ' . clang::__('Product Found with Keyword') . ' ' . $q;
                }
                else {
                    $title_content = $total_result . ' ' . clang::__('Product Found');
                }
            }
            if (isset($request) && count($request) == 0 && $this->state_from == 'seller') {
                $title_content = clang::__('Product Seller');
            }
            $title_wrapper->add_div()->add_class('title')->add($title_content);

            echo $app->render();
        }
        
        protected $options = array();
        protected $url_search_result = null;
        public function seller($ref_code){
            $app = CApp::instance();
            $this->state_from = 'seller';
            
            $container = $app->add_div()->add_class('seller-container');
            $data_seller = seller::get_data($ref_code);
            if ($data_seller == null) {
                // show page seller not found
                $container->add_element('seller-not-found');
                echo $app->render();
            }
            else {
                $member_id = cobj::get($data_seller, 'member_id');
                $image_name = cobj::get($data_seller, 'image_name');
                $name = cobj::get($data_seller, 'name');
                $address = cobj::get($data_seller, 'address');
                $phone = cobj::get($data_seller, 'phone');
                $email = cobj::get($data_seller, 'email');
                $last_login = cobj::get($data_seller, 'last_login');
                $last_login = date("d F Y" , strtotime($last_login));
                $date_join = cobj::get($data_seller, 'date_join');
                $date_join = date("d F Y" , strtotime($date_join));
                $province_name = cobj::get($data_seller, 'province_name');
                $city_name = cobj::get($data_seller, 'city_name');
                $data_total = seller::get_total_transaction($member_id);
				$total_product = seller::get_total_product($member_id);
				$total_success = seller::get_total_success_transaction($member_id);
	//            $total_settle = carr::get($data_total, 'SETTLE', 0);
	//            $total_failed = carr::get($data_total, 'FAILED', 0);
				$total_transaction = $total_success . '/' .$total_product;
                
                $wrapper = $container->add_div()->add_class('seller-wrapper');
                $container_row = $wrapper->add_div()->add_class('row');
                
                // left side
                $image_url = image::get_url_member($image_name, 'view_all');
                $left_side = $container_row->add_div()->add_class('col-md-2 txt-center');
                $left_side->add('<img src="' .$image_url .'" class="img-responsive" />');
                
                // right side
                $right_side = $container_row->add_div()->add_class('col-md-9 seller-information');
                $top_info = $right_side->add_div()->add_class('top-info');
                $top_info->add('<span class="seller-name">' .$name .'</span>');
                $top_info->add_br();
                if (strlen($province_name) > 0 && strlen($city_name) > 0) {
                    $top_info->add($province_name .', ' .$city_name);
                }
                else {
                    $top_info->add_br();
                }
                
                $bottom_info = $right_side->add_div()->add_class('bottom-info');
                $bottom_info_row = $bottom_info->add_div()->add_class('row');
//                $bottom_left = $bottom_info_row->add_div()->add_class('col-md-5');
                $bottom_right = $bottom_info_row->add_div()->add_class('col-md-10');
                $bottom_right->add(clang::__('Last Online:') .' ' .$last_login);
                $bottom_right->add_br();
                $bottom_right->add(clang::__('Join:') .' ' .$date_join);
                $bottom_right->add_br();
                $bottom_right->add(clang::__('Product Sold:') .' ' .$total_transaction);
                
                $this->options['member_id'] = $member_id;
                $this->url_search_result = curl::base() .'user/'.$ref_code;
                $this->search();
            }
        }

    }
    