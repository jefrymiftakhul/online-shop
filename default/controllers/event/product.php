<?php

/**
 *
 * @author Riza
 * @since  Nov 19, 2015
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class Product_Controller extends EventController
{

    private $_count_item = 16;
    private $deal_pick_for_you = "pick_for_you";
    private $type_select = 'select';
    private $type_image = 'image';

    public function __construct()
    {
        parent::__construct();
    }

    public function index($product_id)
    {
        echo $product_id;
    }

    public function item($product_id)
    {
        $db = CDatabase::instance();
        $org_id = CF::org_id();
        if (!is_numeric($product_id)) {
            $product_id = cdbutils::get_value("select product_id from product where status > 0 and (org_id is null or org_id = " . $db->escape($org_id) . ") and url_key= " . $db->escape($product_id));
        }
        
        if ($product_id == null) {
            curl::redirect(curl::base());
        }

        $app = CApp::instance();
        $user = $app->user();

        $org_id = $this->org_id();
        $menu_list = $this->menu_list();
        $link_list = $this->list_menu();

        $error = 0;
        $error_message = '';

        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_menu($menu_list);
        $element_menu->set_link($link_list);
        $element_menu->set_url_menu(curl::base() . $this->page().'/product/category/');

        $container = $app->add_div()->add_class('container margin-top-20 produk-event')->add_div()->add_class('row');
        $container->add_div()->add(cmsg::flash_all());

        $q = "
                    select
                            p.name as product,
                            p.*,
                            pt.*
                    from
                            product as p
                            inner join product_type as pt on pt.product_type_id=p.product_type_id
                    where
                            p.status>0
                            and is_active>0
                            and p.status_confirm='CONFIRMED'
                            and p.product_id=" . $db->escape($product_id) . "
                            and pt.name=" . $db->escape($this->page()) . "
            ";
        $product = $db->query($q);
        if ($product->count() == 0) {
            $error++;
            $error_message = "Product not found please select other product";
        }

        if ($error == 0) {
            $product_title = $container->add_div()->add_class('col-xs-12');
            $product_image = $container->add_div()->add_class('col-xs-12 margin-top-10 margin-bottom-10');
            $product_detail = $container->add_div()->add_class('col-xs-12');
            $product_info = $container->add_div()->add_class('col-xs-12 margin-top-20');

            // VARIBALE PRODUCT
            $data_product = product::get_product($product_id);
            $name = carr::get($data_product,'name');
            $start_bid_date = carr::get($data_product,'start_bid_date');
            $end_bid_date = carr::get($data_product,'end_bid_date');
            $detail_price = carr::get($data_product,'detail_price');
            $price = carr::get($detail_price, 'channel_sell_price');
            $promo_price = carr::get($detail_price, 'promo_price');
            $quota = carr::get($data_product, 'quota');
            $stock = carr::get($data_product, 'stock');
            $location_event = carr::get($data_product, 'location_event');
            $address = carr::get($data_product, 'address');
            $spesification = carr::get($data_product, 'spesification');
            $availability = carr::get($data_product, 'is_available');

            // HANDLING TIMES
            $is_started = false;
            if (time() >= strtotime($start_bid_date)) {
                $is_started = true;
            }

            if($is_started){
                $time = prelove::time_diff_day($end_bid_date);
            }else{
                $time = prelove::time_diff_day($start_bid_date);
                $end_time = prelove::time_diff_day($end_bid_date, $start_bid_date);

                $end_day = $end_time['day'];
                $end_hour = $end_time['hour'];
                $end_minutes = $end_time['minutes'];
                $end_second = $end_time['second'];
            }

            $day = $time['day'];
            $hour = $time['hour'];
            $minutes = $time['minutes'];
            $second = $time['second'];

            $times_up = false;
            if ($day == 0 && $hour == 0 && $minutes == 0 && $second == 0) {
                $times_up = true;
            }
            if(($is_started && $times_up)){
                $time_wrapper = $product_title->add_div()->add_class('time_wrapper pull-right');
                $time_wrapper->add('<div class="event-done">'.clang::__('EVENT SUDAH BERAKHIR').'</div>');
            }else{
                if(!$is_started){
                    $time_wrapper = $product_title->add_div()->add_class('time_wrapper pull-right');
                    $time_wrapper->add_div()->add_class('event-info')->add(clang::__('Event akan di mulai dalam :'));
                    $time_wrapper->add('<div class="time-left"><span class="day">' . $end_day . '</span><br/>' . clang::__('DAYS') . '</div>');
                    $time_wrapper->add('<div class="time-left"><span class="hour">' . $end_hour . '</span><br/>' . clang::__('HOURS') . '</div>');
                    $time_wrapper->add('<div class="time-left"><span class="minute">' . $end_minutes . '</span><br/>' . clang::__('MINS') . '</div>');
                    $time_wrapper->add('<div class="time-left"><span class="second">' . $end_second . '</span><br/>' . clang::__('SECS') . '</div>');
                }else{
                    $time_wrapper = $product_title->add_div()->add_class('time_wrapper pull-right');
                    $time_wrapper->add_div()->add_class('event-info')->add(clang::__('Event sedang berlangsung dan akan berakhir setelah :'));
                    $time_wrapper->add('<div class="time-left"><span class="day">' . $day . '</span><br/>' . clang::__('DAYS') . '</div>');
                    $time_wrapper->add('<div class="time-left"><span class="hour">' . $hour . '</span><br/>' . clang::__('HOURS') . '</div>');
                    $time_wrapper->add('<div class="time-left"><span class="minute">' . $minutes . '</span><br/>' . clang::__('MINS') . '</div>');
                    $time_wrapper->add('<div class="time-left"><span class="second">' . $second . '</span><br/>' . clang::__('SECS') . '</div>');
                }
            }

            // PRODUCT TITLE
            $product_title->add('<h4 class="detail-title">'.$name.'</h4>');

            // PRODUCT IMAGE
            $image = carr::get($data_product,'image_url');
            $img_wrapper = $product_image->add_div()->add_class('img-wrapper');
            $img_wrapper->add_img()->set_src($image);

            // HARGA TIKET SEMINAR
            $product_detail->add('<h4 class="detail-title green">'.clang::__('HARGA TIKET SEMINAR').'</h4>');
            $product_detail->add_div()->add_class('price')->add('Rp. '.ctransform::thousand_separator($price).' / '.clang::__('Person'));
            $product_detail->add_div()->add_class('quota')->add('Quota : '.$quota.' / '.clang::__('Sesi'));
            $product_detail->add_div()->add_class('quota')->add('Sisa Stock : '.$stock);

            // JADWAL SEMINAR
            $product_detail->add_br();
            $product_detail->add('<h4 class="detail-title green">'.clang::__('JADWAL SEMINAR').'</h4>');
            $product_detail->add_div()->add(clang::__('Event akan di laksanakan pada :').' '.date('d-m-Y',strtotime($start_bid_date)));

            // LOKASI SEMINAR
            $product_detail->add_br();
            $product_detail->add('<h4 class="detail-title green">'.clang::__('LOKASI SEMINAR').'</h4>');
            $product_detail->add_div()->add(clang::__('Event akan di laksanakan di :').' '.$location_event.', '.$address);

             // MATERI SEMINAR
            $product_detail->add_br();
            $materi_wrapper = $product_detail->add_div()->add_class('materi_wrapper');
            $materi_wrapper->add('<h4 class="detail-title green">'.clang::__('MATERI SEMINAR').'</h4>');
            $materi_wrapper->add('<p>'.$spesification.'</p>');
            $product_detail->add_br();

            // Button Buy Event
            if(!$times_up){
                $form = $product_detail->add_form('form-detail-product');
                $form->add_control('product_id', 'hidden')->set_value($product_id);
                $form->add_control('page', 'hidden')->set_value('event');
                $action = $form->add_action()
                    ->set_label(clang::__('<i class="fa fa-send"></i> Pesan Sekarang'))
                    ->add_class('btn-62hallfamily bg-red btn-add-cart border-3-red')
                    ->custom_css('height', '34px')
                    ->set_link(curl::base() . 'event/updatecontact/update_contact/' . $product_id);
                if($stock == 0){
                    $action->set_disabled(true);
                    $action->set_link('#');
                    $action->add_class('disabled');
                    $form->add_br();
                    $form->add('<span style="font-size:12px;">'.clang::__('Maaf anda tidak dapat membeli produk ini karena stok habis').'</span>');
                }
            }

        } else {
            $container->add($error_message);
        }

        echo $app->render();
    }

    //<editor-fold defaultstate="collapsed" desc="Product Overview">
    public function overview($product_id)
    {
        $app = CApp::instance();

        $data_detail_product = product::get_product($product_id);
        $overview = carr::get($data_detail_product, 'overview', null);

        $app->add($overview);

        echo $app->render();
    }

    public function spesifikasi($product_id)
    {
        $app = CApp::instance();

        $data_detail_product = product::get_product($product_id);
        $spesification = carr::get($data_detail_product, 'spesification', null);

        $app->add($spesification);

        echo $app->render();
    }

    public function reviews($product_id)
    {
        $app = CApp::instance();

        echo $app->render();
    }

    public function faq($product_id)
    {
        $app = CApp::instance();

        $data_detail_product = product::get_product($product_id);
        $faq = carr::get($data_detail_product, 'faq', null);

        $app->add($faq);

        echo $app->render();
    }

    //</editor-fold>

    public function category($category_id = null)
    {
        $app = CApp::instance();
        $user = $app->user();
        $db = CDatabase::instance();
        $org_id = $this->org_id();
        $menu_list = $this->menu_list();
        $link_list = $this->list_menu();

        $request = $_GET;
        $request_category_id = carr::get($request, 'category');
        if ($request_category_id) {
            $category_id = $request_category_id;
        }
        if (!is_numeric($category_id)) {
            $category_id = cdbutils::get_value('select product_category_id from product_category where url_key =' . $db->escape($category_id));
        }
        // Menu
        $data_product_category = product_category::get_product_category_menu($this->page());

        $element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_menu($menu_list);
        $element_menu->set_link($link_list);
        $element_menu->set_url_menu(curl::base() . 'lokal/product/category/');
        $first_parent = product_category::get_first_parent_category($category_id);

        $container = $app->add_div()->add_class('container');
        $category_menu_container = $container->add_div()->add_class('col-md-4 padding-0 margin-30');
        $category_container = $container->add_div()->add_class('col-md-8 margin-30 padding-0');
        $list_image_cms_product_category = cms_product_category::get_cms_product_category($org_id, $category_id);
        if (count($list_image_cms_product_category) == 0 && $first_parent != null) {
            $list_image_cms_product_category = cms_product_category::get_cms_product_category($org_id, $first_parent->product_category_id);
            if (count($list_image_cms_product_category) > 0) {
                $advertise_category = $category_container->add_div('advertise-category')->add_class('col-md-12 padding-0');
                $element_advertise_category = $advertise_category->add_element('62hall-category-advertise', 'category-advertise');
                $element_advertise_category->set_list_advertise($list_image_cms_product_category);
            }
        }

        $data_category_menu_choise = product_category::get_parent_category($category_id);

        if ($data_category_menu_choise) {
            $category_menu = $category_menu_container->add_div()->add_class('categories border-1 padding-left-right10');

            $title = $category_container->add_div()->add_class('col-md-12 padding-0');

            $title->add('<h4 class="border-bottom-gray bold">' . $data_category_menu_choise->name . '</h4>');

            $product_category = $category_container->add_div('page-product-category')->add_class('col-md-12 padding-0 lokal-result');

            // CATEGORY MENU
            $category_menu_list = product_category::get_product_category_menu($this->page(), $first_parent->product_category_id);

            $element_category_menu = $category_menu->add_element('62hall-category-menu', 'category-menu-' . $category_id);
            $element_category_menu->set_key($category_id)
                ->set_head_key($first_parent->product_category_id)
                ->set_head_menu($first_parent->name)
                ->set_list_menu($category_menu_list)
                ->set_url_menu(curl::base() . 'lokal/product/category/');

            // CATEGORY FILTER
            $options = array(
                'product_type' => $this->page(),
                'product_category_id' => $category_id,
                'visibility' => 'catalog',
            );
            $list_attribute = product::get_attributes_array($options);

            $min_price = product::get_min_price($options);
            $max_price = product::get_max_price($options);
            if ($min_price == null) {
                $min_price = 0;
            }
            if ($max_price == null) {
                $max_price = 0;
            }
            $range_price = array(
                'min' => $min_price,
                'max' => $max_price,
            );
            $list_filter_product = array(
                'diskon' => clang::__('Diskon'),
                'min_price' => clang::__('Termurah'),
                'max_price' => clang::__('Termahal'),
                'is_new' => clang::__('Terbaru'),
                'popular' => clang::__('Popular'),
                'A-Z' => clang::__('A-Z'),
                'Z-A' => clang::__('Z-A'),
            );
            $q = "
                    select
                            *
                    from
                            product_category
                    where
                            product_category_id=" . $db->escape($category_id) . "
                ";
            $r = $db->query($q);
            $category_lft = '';
            $category_rgt = '';
            if ($r->count() > 0) {
                $row = $r[0];
                $category_lft = $row->lft;
                $category_rgt = $row->rgt;
            }

            $q = 'select * from product_category where status > 0 and parent_id is null and product_type_id = 10';
            $data_product_category = $db->query($q);
            $category_filter = $category_menu_container->add_div()->add_class('categories border-1 padding-left-right10 margin-top-30');
            $element_filter = $category_filter->add_element('62hall-filter', 'category-filter');
            $element_filter->set_filter_product(false);
            $element_filter->set_filter_price(true);
            $element_filter->set_filter_location(true);
            $element_filter->set_filter_category(true);
            $element_filter->set_filter_page($this->page());
            $element_filter->set_min_price($range_price['min']);
            $element_filter->set_max_price($range_price['max']);
            $element_filter->set_list_filter_category($data_product_category);

            $filter_name = '';
            $product_category->add_listener('ready')
                ->add_handler('reload')
                ->set_target('page-product-category')
                ->add_param_input('arr_product_id')
                ->set_url(curl::base() . "reload/reload_product_filter?visibility=catalog&filter_page=" . $this->page() . '&filter_category_lft=' . $category_lft . '&filter_category_rgt=' . $category_rgt);
        } else {
            $category_container->add('Kategori Lokal tidak ditemukan');
        }
        echo $app->render();
    }

    private function generate_list($type, $data = array())
    {
        $list = array();

        foreach ($data as $key => $row_data) {
            if ($type == $this->type_select) {
                $value = carr::get($row_data, 'attribute_key');
            }
            if ($type == $this->type_image) {
                $value['key'] = carr::get($row_data, 'attribute_key');
                $value['value'] = carr::get($row_data, 'file_path');
            }

            $list[$key] = $value;
        }

        return $list;
    }

    public function generate_attribute($code, $product_id)
    {
        $app = CApp::instance();

        $request = $_GET;

        $param = array();
        foreach ($request as $key => $row_request) {
            if ($key != 'capp_current_container_id') {
                $param[] = $row_request;
            }
        }

        $attribute_category = product::get_attribute_category_all($product_id);
        $attibute_data = $attribute_category[$code];
        $id = carr::get($attibute_data, 'attribute_category_id');
        $type = carr::get($attibute_data, 'attribute_category_type');
        $prev = carr::get($attibute_data, 'prev_attribute_category');
        $next = carr::get($attibute_data, 'next_attribute_category');

        $attribute = product::get_attribute_list($product_id, $id, $param);

        $list = $this->generate_list($type, $attribute['data']);

        foreach ($prev as $row_prev) {
            $arr_param[] = 'att_cat_' . $id;
        }
        if ($type == $this->type_select) {
            $div_container = $app->add_div('container-' . $code);
            $select = $div_container->add_field()
                ->add_control('att_cat_' . $id, 'select')
                ->add_class('select-62hallfamily margin-bottom-10')
                ->custom_css('margin-left', '15px')
                ->custom_css('width', '130px')
                ->set_list($list);

            if (!empty($next) and count($list) > 0) {
                $listener = $select->add_listener('ready');

                $handler = $listener->add_handler('reload')
                    ->set_target('container-' . $next)
                    ->add_param_input($arr_param)
                    ->set_url(curl::base() . 'product/generate_attribute/' . $next . '/' . $product_id);
                $listener = $select->add_listener('change');

                $handler = $listener->add_handler('reload')
                    ->set_target('container-' . $next)
                    ->add_param_input($arr_param)
                    ->set_url(curl::base() . 'product/generate_attribute/' . $next . '/' . $product_id);
            }
        } else if ($type == $this->type_image) {
            $div_container = $app->add_div('container-' . $code);
            $index = 1;
            foreach ($list as $key => $value) {
                $active = null;
                if ($index == 1) {
                    $active = 'active';
                    $div_container->add_control('att_cat_' . $id, 'hidden')
                        ->set_value($key);
                }
                $image = $div_container->add_div($key)
                    ->add_class('btn-colors margin-bottom-10 link ' . $active)
                    ->custom_css('display', 'inline-block')
                    ->custom_css('padding', '2px')
                    ->add('<img src="' . $value['value'] . '" style="width:20px; height:20px"/>');

                if (!empty($next) and count($list) > 0) {
                    $listener = $image->add_listener('ready');

                    $handler = $listener->add_handler('reload')
                        ->set_target('container-' . $next)
                        ->add_param_input($arr_param)
                        ->set_url(curl::base() . 'product/generate_attribute/' . $next . '/' . $product_id);

                    $listener = $image->add_listener('click');

                    $handler = $listener->add_handler('reload')
                        ->set_target('container-' . $next)
                        ->add_param_input($arr_param)
                        ->set_url(curl::base() . 'product/generate_attribute/' . $next . '/' . $product_id);

                    $handler = $listener->add_handler('custom')
                        ->set_js("
                                var key=$(this).attr('id');
                                $('#" . 'att_cat_' . $id . "').val(key);
                            ");
                }
                $index++;
            }
        }

        echo $app->render();
    }

    public static function callback_ajax($args = '')
    {

        $db = CDatabase::instance();

        //$operation = carr::get($args, 'operation');
        $custom_field_data = carr::get($args, 'custom_field_data');
        $product_id = cobj::get($custom_field_data, 'product_id');

        //$product_id = 100;
        if (strlen($product_id) > 0) {
            $product_city_arr = array();
            $result_product_city = $db->query('select * from product_city where status > 0 and product_id = ' . $db->escape($product_id) . ' ');
            if ($result_product_city->count() > 0) {
                foreach ($result_product_city as $product_city_key => $product_city_val) {

                    $province_id = cobj::get($product_city_val, 'province_id');
                    $city_id = cobj::get($product_city_val, 'city_id');
                    $product_city_id = cobj::get($product_city_val, 'product_city_id');

                    if ($province_id != null && $city_id == null) {
                        $custom_product_city_id = 'pr_' . $province_id;
                        //$custom_product_city_id = $province_id;
                        $product_city_arr[$custom_product_city_id] = $product_city_id;
                    }

                    if ($province_id != null && $city_id != null) {
                        //$custom_product_city_id = $province_id.'_'.$city_id;
                        //$custom_product_city_id = $city_id;
                        $custom_product_city_id = 'ct_' . $city_id;
                        $product_city_arr[$custom_product_city_id] = $product_city_id;
                    }
                }
            }
        }

        //$result_province = $db->query('select * from province where status > 0 and country_id = '.$db->escape(94).' and province_id = '.$db->escape(15).' or province_id = '.$db->escape(13).'  ');
        $result_province = $db->query('select * from province where status > 0 and country_id = ' . $db->escape(94) . ' ');
        $province_all = array();
        foreach ($result_province as $province_k => $province_v) {
            $province_arr = array();

            $province_selected = false;
            $province_id = cobj::get($province_v, 'province_id');
            $province_name = cobj::get($province_v, 'name');
            if (isset($product_city_arr['pr_' . $province_id])) {
                $province_selected = true;
            }

            $province_state = array('selected' => $province_selected);
            $result_city = $db->query('select * from city where status > 0 and country_id = ' . $db->escape(94) . ' and province_id = ' . $db->escape($province_id) . '  ');
            $city_all = array();
            $is_city_selected = false;
            foreach ($result_city as $city_k => $city_v) {
                $city_selected = false;
                $city_arr = array();
                $city_id = cobj::get($city_v, 'city_id');
                $city_name = cobj::get($city_v, 'name');

                if (isset($product_city_arr['ct_' . $city_id])) {
                    $city_selected = true;
                    $is_city_selected = true;
                }
                $city_state = array('selected' => $city_selected);

                //$city_arr['id'] = 'ct_'.$province_id.'_'.$city_id;
                $city_arr['id'] = $province_id . '_' . $city_id;
                $city_arr['text'] = $city_name;
                $city_arr['state'] = $city_state;
                //$city_arr['icon'] = 'glyphicon glyphicon-flash';
                $city_arr['icon'] = false;
                $city_all[] = $city_arr;
            }

            if ($is_city_selected) {
                array_push($province_state, array('opened' => true));
            }
            $province_arr['id'] = 'pr_' . $province_id;
            $province_arr['text'] = $province_name;
            $province_arr['icon'] = false;
            $province_arr['state'] = $province_state;
            $province_arr['children'] = $city_all;
            $province_all[] = $province_arr;
        }

//            $array[] = array(
        //                'id'=>1,
        //                'text'=>'joko jainul arif',
        //                'children'=>array(
        //                    array(
        //                        'id'=>2,
        //                        'text'=>'Child 1',
        //                    ),
        //                    array(
        //                        'id'=>3,
        //                        'text'=>'Child 2',
        //                    ),
        //                ),
        //            );
        //cdbg::var_dump($province_all);
        //echo json_encode($array);
        //cdbg::var_dump($province_all);
        echo json_encode($province_all);
    }

}
