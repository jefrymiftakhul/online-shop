/**
 *
 * @author Raymond Sugiarto
 * @since  Mar 29, 2016
 */

(function ($, window, document) {
    
    var locOpts = {
        url: "data/location/",
        provinceId: "#provinceId",
        cityId: "#cityId",
        districtId: "#districtId",
        shippingTypeId: "#shipping-type-select .dropdown-menu-list",
        urlShippingFee: "order/shipping/shippingFee",
    };;
    
    $.location = {
        options: {},
        init: function(options){
            locOpts.callback = null;
            for (var optKey in $.location.options) {
                locOpts[optKey] = $.location.options[optKey];
            }
            if (typeof options !== 'undefined') {
                for (var optKey in options) {
                    locOpts[optKey] = options[optKey];
                }
            }
        },
        reloadCity: function(options){
            this.init(options);
            var postData = {
                'provinceId': jQuery(locOpts.provinceId).val()
            };
            this.request('city', postData, options);
        },
        reloadDistrict: function(options){
            this.init(options);
            var postData = {
                'cityId': jQuery(locOpts.cityId).val()
            };
            this.request('district', postData, options);
        },
        getShippingFee: function(options, base){
            this.init(options);
            var shippingTypeId;
            if (typeof base !== 'undefined') {
                 shippingTypeId = base.attr('val');
            }
            if (typeof shippingTypeId === 'undefined') {
                shippingTypeId = jQuery(locOpts.shippingTypeId).attr('value');
            }
            var postData = {
                shippingTypeId: shippingTypeId,
                shippingDistrictId: jQuery(locOpts.districtId).val(),
            };
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: baseUrl + locOpts.urlShippingFee,
                data: postData,
            }).done(function (res) {
                if (res.status == 'SUCCESS') {
                    var data = res.data;
                    jQuery('#shipping-fee').val(data.fee_formatted);
                }
                else {
                    jQuery('#shipping-fee').val(res.message);
                }
            }).error(function (jqXHR, status, thrown) {
                if (status == 'error') {

                }
            });
        },
        request: function(locationType, postData, options){
            $.ajax({
                type: "post",
                dataType: 'json',
                url: baseUrl + locOpts.url + locationType,
                data: postData,
            }).done(function (res) {
                if (res.status == 'SUCCESS') {
                    var data = res.data;
                    if (typeof data.cities !== 'undefined') {
                        jQuery(locOpts.cityId).html('');
                        for (var cityKey in data.cities) {
                            var selectOpt = jQuery('<option>').val(cityKey).html(data.cities[cityKey]);
                            jQuery(locOpts.cityId).append(selectOpt);
                        }
                        jQuery(locOpts.cityId).select2({
                            dropdownCssClass: 'ost-select2',
                            containerCssClass : 'tpx-select2-container ost-select2',
                        });
                    }
                    if (typeof data.districts !== 'undefined') {
                        jQuery(locOpts.districtId).html('');
                        for (var districtKey in data.districts) {
                            var selectOpt = jQuery('<option>').val(districtKey).html(data.districts[districtKey]);
                            jQuery(locOpts.districtId).append(selectOpt);
                        }
                        jQuery(locOpts.districtId).select2({
                            dropdownCssClass: 'ost-select2',
                            containerCssClass : 'tpx-select2-container ost-select2',
                        });
                    }
                }
                
                if (typeof locOpts.callback === 'function') {
                    locOpts.callback(res);
                }
            }).error(function (jqXHR, status, thrown) {
                if (status == 'error') {

                }
            });
        }
    }
    
    $.ostore = {
        cart: {},
        dialog: {
            error: function (message, title) {
                
            },
            showLoading: function() {
                if (jQuery('#backdropLoading').length > 0) {
                    jQuery('#backdropLoading').remove();
                }
                var loadingScreen = '<div id="backdropLoading" class="modal fade in">' 
                        + '<img src="' + cartOpts['imgLoading'] + '"/>'
                        + '</div>';
                jQuery('body').append(loadingScreen);
            },
            hideLoading: function() {
                if (jQuery('#backdropLoading').length > 0) {
                    jQuery('#backdropLoading').remove();
                }
            }
        },
        showLoading: function (target) {
            jQuery(target).html('<img class="os-loading" src="' + cartOpts['imgLoading'] + '"/>');
        },
        hideLoading: function (target) {
            jQuery(target).html("");
        },
        location: $.location,
    }

    $.ostore.cart.options = {
        url: "products/cart/",
        cartTargetId: "#cart-total-item",
        imgLoading: imageLoading,
    };

    var cart = $.ostore.cart;
    var cartOpts = cart.options;

    $.ostore.cart = {
        add: function (id, options, action) {
            if (typeof options !== 'undefined') {
                for (var optKey in options) {
                    cartOpts[optKey] = options[optKey];
                }
            }
            var actionUrl = 'addItem';
            if (typeof action !== 'undefined') {
                if (action == 'update') {
                    actionUrl = 'updateItem';
                }
                else if (action == 'remove') {
                    actionUrl = 'removeItem';
                }
                else if (action == 'change') {
                    actionUrl = 'changeItem';
                }
                else if (action == 'summary') {
                    actionUrl = 'getSummary';
                }
            }
            
            $.ostore.dialog.showLoading();
            
            var postData = jQuery('#' + id).closest('form').serialize();
            $.ajax({
                type: "post",
                dataType: 'json',
                url: baseUrl + cartOpts.url + actionUrl,
                data: postData,
            }).done(function (res) {
                $.ostore.dialog.hideLoading();
                if (res.status == "SUCCESS") {
                    $(cartOpts.cartTargetId).html(res.total);
                    $(cartOpts.cartTargetId).attr('state', 'new');
                }
                if (typeof cartOpts.callback === 'function') {
                    cartOpts.callback(res);
                }
            }).error(function (jqXHR, status, thrown) {
                if (status == 'error') {

                }
            });
        },
        update: function (id, options){
            this.add(id, options, 'update');
        },
        remove: function (id, options) {
            this.add(id, options, 'remove');
        },
        change: function (id, options){
            this.add(id, options, 'change');
        },
        getSummary: function(id, options) {
            this.add(id, options, 'summary');
        },
        showItems: function (target) {
            $.ostore.showLoading(target);
            $.ajax({
                type: "post",
                dataType: 'json',
                url: baseUrl + cartOpts.url + "showItems",
            }).done(function (res) {
                $( cartOpts.cartTargetId ).removeAttr('state');
                $.cresenity._handle_response(res,function() {
                    $(target).html(res.html);
                    if (res.js && res.js.length > 0) {
                        var script = $.cresenity.base64.decode(res.js);
                        eval(script);
                    }
                    $( target ).data('xhr', false);
                    if ( $( target ).find('.prettyprint').length > 0 ) {
                        window.prettyPrint && prettyPrint();
                    }
                });
            }).error(function (jqXHR, status, thrown) {
                if (status == 'error') {

                }
            });
        }
    }
})(this.jQuery, window, document);