<script>
    if(typeof $.app=='undefined') {
        var window_height = $(window).height();

        (function ($) {
            $.app = {
                show_dialog: function (id_target, title, body, id_custom, close) {
                    var modal_class = '';
                    if (!title) {
                        title = 'Dialog';
                    }
                    if (!id_custom){
                        id_custom = '';
                    }

                    if (title == 'Error') {
                        modal_class = 'modal-error';
                    }

                    var _dialog_html = "<div  id='custom_modal_" + id_custom + "' class='modal " + id_custom + "'>" +
                                    "  <div class='modal-dialog'>" +
                                    "    <div class='modal-content clearfix "+modal_class+"'>" +
                                    "       <div class='modal-header'>";
                    if (!close || close == '1') {
                        _dialog_html += "        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>";
                    }
                    _dialog_html += "        <h4 class='modal-title'>Modal title</h4>" +
                                    "      </div>" +
                                    "      <div class ='modal-body'>" +
                                    "        <p>One fine body…</p>" +
                                    "       </div>" +
        //                "      <div class='modal-footer'>"+
        //                "        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>"+
                                    //                "        <button type='button' class='btn btn-primary'>Save changes</button>"+
                                    //                "      </div>"+
                                    "    </div><!-- /.modal-content -->" +
                                    "  </div><!-- /.modal-dialog - ->" +
                                    "</div><!-- /.modal -->";
                    var selection = jQuery('#' + id_target);
                    var handle;
                    var dialog_is_remove = false;
                    if (selection.length == 0) {
                        selection = jQuery('<div/>').attr('id', id_target);
                        dialog_is_remove = true;
                    }
                    var selection_exist = false;

                    if (!selection.is(".modal-body")) {
                        //var overlay = $('<div class="modal-backdrop fade in"></div>').hide();
                        var parent = $(_dialog_html);
                        handle = $(".modal-body", parent);
                                //console.log(handle.parent());

                        jQuery(".modal-header button.close", parent).text(unescape("%D7")).click(function (event) {
                            event.preventDefault();
                            if (dialog_is_remove) {
                                $('body .modal-backdrop.modal-backdrop-'+id_target+'').remove();
                                //handle.parents('.modal').prev(".modal-backdrop").remove();
                                jQuery(this).parents(".modal").find(".modal-dialog").parent().remove();
                            } 
                            else {
                                $('body .modal-backdrop.modal-backdrop-'+id_target+'').hide();
                                //handle.parents('.modal').prev(".modal-backdrop").hide();
                                jQuery(this).parents(".modal").find(".modal-dialog").parent().hide();
                            }
                        });
                        jQuery("body").append(parent);
                        jQuery(".modal-header h4", parent).html(title);
                        // Create dialog body from current jquery selection
                        // If specified body is a div element and only one element is 
                        // specified, make it the new modal dialog body
                        // Allows us to do something like this 
                        // $('<div id="foo"></div>').dialog2(); $("#foo").dialog2("open");

                        if (selection.is("div") && selection.length == 1) {
                            handle.replaceWith(selection);
                            selection.addClass("modal-body").show();
                            handle = selection;
                        }
                        // If not, append current selection to dialog body
                        else {
                            handle.append(selection);
                        }
                    }
                    else {
                        handle = selection;
                        parent = handle.parents('.modal');
                        if(parent.length>0) parent = jQuery(parent[0]);
                        selection_exist = true;
                    }

                    var overlay = false;
                    if(!selection_exist) {
                        overlay = $('.modal-backdrop.modal-backdrop-'+id_target);
                        if(overlay.length==0) {
                            overlay = $('<div class="modal-backdrop modal-backdrop-'+id_target+' fade in"></div>').hide();
                            $('body').append(overlay);
                        } else {
                            overlay.hide();
                        }
                    }

                    if (!handle.is(".opened")) {
                        if(overlay) {
                           overlay.show();
                        }
                        //console.log(id_target);
                        //console.log(parent);
                        parent.addClass("opened").show();

                    }
                    if(body!=undefined && body.length>0) {
                        if(body.substring(0,4)=='http'||body.substring(0,1)=='/') {
                             jQuery('#' + id_target).append(jQuery('<div>')
                            .attr('id', id_target + '-loading').css('text-align', 'center')
                            .css('margin-top', '100px').css('margin-bottom', '100px')
                            .append(jQuery('<i>').addClass('fa fa-repeat fa-spin fa-4x')));

                            jQuery('#'+id_target).data('xhr',jQuery.ajax({
                                    type: 'post',
                                    url: body,
                                    dataType: 'json',
                                    data: {},

                            }).done(function( data ) {
                                    $.cresenity._handle_response(data,function() {
                                            jQuery('#'+id_target).append(data.html);
                                            jQuery('#'+id_target).find('#'+id_target+'-loading').remove();
                                            var script = $.cresenity.base64.decode(data.js);
                                            //console.log(script);
                                            eval(script);

                                            jQuery('#'+id_target).removeClass('loading');
                                            jQuery('#'+id_target).data('xhr',false);
                                            if(jQuery('#'+id_target).find('.prettyprint').length>0) {
                                                    window.prettyPrint && prettyPrint();

                                            }
                                    });
                            }).error(function(obj,t,msg) {
                                    if(msg!='abort') {
                                            $.cresenity.message('error','Error, please call administrator... (' + msg + ')');
                                    }
                            }));

                        } else {
                            jQuery('#' + id_target).html(body);
                        }
                    }
                    if(selection_exist) {

                        parent.modal({
                           'backdrop':'static'
                        });
                        selection.show();
                    } else {

                        parent.show();
                    }

                    // apply scroll if modal need to scroll
                    if(id_custom!='undefined'&&id_custom.length>0) {
                        var modal_height = $('.'+id_custom).find('.modal-content').height();
                        if (modal_height > window_height) {
                            var modal_height_auto = window_height - 100;
                            $('.'+id_custom).find('.modal-content').css({'max-height': modal_height_auto+'px', 'overflow-y':'scroll'});
                        }
                    }
                },
                show_loading : function(){
                    $('body').append('<div class="loading-container">'
                        +'<div class="loading"></div>'
                        +'<div class="loading-information">Please Wait...</div>'
                    +'</div>');
                },
                hide_loading : function(){
                    $('body').find('.loading-container').remove();
                }
            }
        })(this.jQuery, window, document);

        $('.btn-popup').on('click', function(event){
            event.stopPropagation();
            var popup_target = $(this).attr('data-popup');
            var is_hide = $('.'+popup_target).hasClass('hide');
            if (is_hide == true) {
                $('.'+popup_target).removeClass('hide');
                $(this).addClass('active');
            }
            else {
                $('.'+popup_target).addClass('hide');
                $(this).removeClass('active');
            }
        });
        
        jQuery('.btn-62hallfamily-reg').on('click', function(){
            jQuery('.state-from').val('');
        });
        jQuery('.btn-62hallfamily-login').on('click', function(){
            jQuery('.state-from').val('login');
        });

        $('.btn-modal').on('click', function(event){
            var modal_content   = $(this).attr('data-modal');
            var modal_title     = $(this).attr('data-title');
            var body_target     = $(this).attr('data-target');
            //var body            = $('.'+modal_content).html();
            var body = '';

            if (modal_content && modal_title) {
                if (modal_content != 'logout') {
                    event.preventDefault();
                    if(body_target=='undefined' || body_target.length==0) {
                        body_target = '62hall-dialog';
                    }
                    //jQuery('#'+body_target).parents('.modal').attr('id');
                    //jQuery('#'+body_target).parents('.modal').modal();
                    $.app.show_dialog(body_target, modal_title, body, modal_content);
                }
            }
        });

        var progress_bar = '<div class="progress">'
                            +'<div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'
                              +'<span class="sr-only">45% Complete</span>'
                            +'</div>'
                          +'</div>';



        $('#nav-side-bar').click(function() {

            $('ul.categories').toggleClass('show');
            $('li.categories').toggleClass('show');
        });

        $('ul.categories').click(function() {

            $('ul.categories').toggleClass('show');
            $('li.categories').toggleClass('show');
        });

       $('#nav-side-bar').click(function() {

            $('.menu-mobile').toggleClass('show');

        });

        $('.menu-mobile').click(function() {

            $('.menu-mobile').toggleClass('show');
        });

        $('body').click(function(evt){    
            if($('ul.categories').hasClass('show')) {
                if(evt.target.id == "menu-categories")
                   return;
                //For descendants of menu_content being clicked, remove this check if you do not want to put constraint on descendants.
                if($(evt.target).closest('#menu-categories').length)
                   return;             
                if(evt.target.id == "nav-side-bar")
                   return;
                if($(evt.target).closest('#nav-side-bar').length)
                   return;             
                $('ul.categories').toggleClass('show');
                $('li.categories').toggleClass('show');
            }
        });
        
        // $(window).scroll(function() {
        //     var scrollMenu = $('body').find('.floor-elevator-block');
        //     var firstFloor = $('body').find('.container-floor');
        //     var firstFloorPosition = $(firstFloor).position().top;
        //     if ($(this).scrollTop() >= (firstFloorPosition-50) ) {
        //         $(scrollMenu).fadeIn('slow');
        //     }
        //     else {
        //       $(scrollMenu).fadeOut('slow');
        //      }
        //  });
        
        $('.scrollmenu').on('click', function(){
            var floorpage = $(this).attr('data-floor');
            $('html, body').animate({
                scrollTop: $('#'+floorpage).offset().top
            }, 1000);
        });

        jQuery(document).mouseup(function (e) {
                var option = jQuery(e.target).closest(".62hall-options");
                var dropdown = jQuery(e.target).closest(".62hall-dropdown");
                if (option.length == 0 && dropdown.length == 0) {
                    jQuery(".62hall-options").hide();
                }
            });

        function pad(number) {
            var result = "" + number;
            if (result.length < 2) {
                result = "0" + result;
            }

            return result;
        }
        
        function run_timer_promo(){
            jQuery('.time').each(function(){
                var promo_end_date=jQuery(this).attr('promo_end_date');
                if(promo_end_date.length>0){
                    var reg = /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/;
                    var res_reg = reg.exec(promo_end_date); 
                    var obj_promo_end_date = new Date(
                        (+res_reg[1]),
                        (+res_reg[2])-1,
                        (+res_reg[3]),
                        (+res_reg[4]),
                        (+res_reg[5]),
                        (+res_reg[6])
                    );
                    var date_now=new Date();
                    var diff=obj_promo_end_date-date_now;
                    var seconds = parseInt(diff / 1000);
                    
                    var minutes = parseInt(seconds/60);
                    seconds -= minutes * 60;
                    var hours = parseInt(minutes / 60);
                    minutes -= hours * 60; 
                    var days = parseInt(hours / 24);
                    hours -= days * 24;
                    
                    seconds=pad(seconds);
                    minutes=pad(minutes);
                    hours=pad(hours);
                    days=pad(days);
                    
                    jQuery(this).html(days+'H:'+hours+'J:'+minutes+'M:'+seconds+'D');
//                    jQuery(this).html(seconds+'|'+minutes);
                }


            });
        }
        jQuery(document).ready(function() {
            window.setInterval(run_timer_promo, 1000);

            //can click in ipad mini
            jQuery('body').on('touchstart.dropdown', '.dropdown-menu', function (e) { 
                jQuery(".child-1").hide();
                e.stopPropagation(); 
            });
            

            $('#keyword').keypress(function(e) {
                if(e.which == 13) {
                    $(this).closest('form').submit();
                }
            });

            $('#keyword').closest('form').submit(function() {
                if($('#keyword').val().trim()=='') {
                    alert('Please input keyword !!!');
                    return false;
                }
                return true;
            });
            
            $(".carousel-inner").swipe( {
                    //Generic swipe handler for all directions
                    swipeLeft:function(event, direction, distance, duration, fingerCount) {
                            $(this).parent().carousel('prev'); 
                    },
                    swipeRight: function() {
                            $(this).parent().carousel('next'); 
                    },
                    //Default is 75px, set to 0 for demo so any distance triggers swipe
                    threshold:0
            });
            
            
            $("article .carousel-control").each(function() {
                if($(this).find('.fa').length==0) {
                    if($(this).hasClass('left')) {
                        $(this).append(' <span class="fa fa-chevron-circle-left" aria-hidden="true"></span>');
                    }
                    if($(this).hasClass('right')) {
                        $(this).append(' <span class="fa fa-chevron-circle-right" aria-hidden="true"></span>');
                    }
                }
               
            });
            
            $(".slide-show").each(function() {
                if($(this).find('.item').length==0) {
                    $(".slide-show").css('height','400px');
                }
            });
            
            //check mobile
            var isMobile = 'desktop'; //initiate as desktop
            // device detection
            if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
                || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = 'mobile';
            if (isMobile == 'mobile') {
                if($('.container-floor').length==0) {
                //remove icon caret right
                   $('li.dropdown-submenu.categories-submenu').removeClass('dropdown-submenu');
                }
            }
            
            $('#category-dropdown-select').on('click','li > a', function(){
                var cat_name = $(this).attr('data-name');
                var cat_code = $(this).attr('data-code');
                var wrapper = $(this).closest('label');
                wrapper.find('#category-code-selected').val(cat_code);
                wrapper.find('#category-label-selected').text(cat_name);
            });

            jQuery(".dropdown-transaction-status").click(function(e){
                e.stopPropagation(); 
            });

            jQuery('.ico-show-xs').click(function() {
                if (jQuery('.main-menu').hasClass('hidden-xs')) {
                    jQuery('.main-menu').removeClass('hidden-xs').stop().hide().slideDown();
                }
                else {
                    jQuery('.main-menu').slideToggle();
                }
            });

            jQuery(window).resize(function() {
                if (jQuery(this).width() >= 768) {
                    jQuery('.main-menu').css('display', 'block');
                }
            });

        });
        
        //login FB
        var facebook_login = "<?php echo ccfg::get('login_facebook');?>";
        var facebook_login_app_id = "<?php echo ccfg::get('login_facebook_app_id');?>";

        if(facebook_login == 1){
            function statusChangeCallback(response) { 
                if (response.status === 'connected') {
                    // FB.api('/me?fields=name,email', function(response) {
                    //     ajax_login_fb(response.email, response.name);
                    // });
                } else if (response.status === 'not_authorized') {
                  // The person is logged into Facebook, but not your app.

                } else {
                  // The person is not logged into Facebook, so we're not sure if
                  // they are logged into this app or not.        
                }
            }

            function checkLoginState() {
                FB.getLoginStatus(function(response) {
                    statusChangeCallback(response);
                });
            }

            window.fbAsyncInit = function() {
                FB.init({
                    appId      : facebook_login_app_id,
                    cookie     : true,  // enable cookies to allow the server to access 
                                        // the session
                    xfbml      : true,  // parse social plugins on this page
                    version    : 'v2.2' // use version 2.2
                });

                FB.getLoginStatus(function(response) {
                    statusChangeCallback(response);
                });

                checkLoginState();
            };

            // Load the SDK asynchronously
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));


            function testAPI() {
                console.log('Welcome!  Fetching your information.... ');
                FB.api('/me?fields=name,email', function(response) {
                    console.log(JSON.stringify(response));
                });
            }

            function login_fb() {
                FB.login(function(response) {
                    if (response.authResponse) {
                        // if login success / connected                    
                        //profile    
                        FB.api('/me?fields=name,email', function(p_response){
                            //token auth
                            var fb_access_token = '';
                            var fb_user_id = p_response.id;
                            var fb_profile = JSON.stringify(p_response);
                            var fb_auth = '';

                            FB.getLoginStatus(function(st_response) {
                                fb_access_token = st_response.authResponse.accessToken;
                                fb_auth = JSON.stringify(st_response);
                            }); 

                            ajax_login_fb(fb_access_token, fb_user_id, fb_profile, fb_auth);
                        });
                    } else {
                        // cancelled
                        console.log('cancel login');
                    }
                }, { scope: 'email' });
            }

            // function login_fb() {
            //     var oauth_url = 'https://www.facebook.com/dialog/oauth/';
            //     oauth_url += '?app_id='+facebook_login_app_id;
            //     oauth_url += '&redirect_uri=' + encodeURIComponent('https://apps.facebook.com/APP_NAMESPACE/');
            //     oauth_url += '&scope=email,name'
            //     window.top.location = oauth_url;
            // }

            function ajax_login_fb(fb_access_token, fb_user_id, fb_profile, fb_auth){
                var fb_url = "<?php echo curl::httpbase();?>auth/vToken";
                var redir_url = jQuery(location).attr('href');

                $.ajax({
                    type: "post",
                    dataType: 'json',
                    url: fb_url,
                    data: {
                        access_token: fb_access_token,
                        user_id: fb_user_id,
                        profile: fb_profile,
                        auth: fb_auth
                    },
                    success: function(result) {
                        if(result.error == 0){
                            location.href = redir_url;
                        }
                        else {
                            console.log(result);
                        }
                    },
                    error: function(result) {
                        console.log('error ajax '+result);
                    }
                });
            }

            jQuery("body").on('click', ".login-fb", function(){
                login_fb();
            });

            jQuery("body").on("click", ".btn-logout", function(){
                // FB.logout();
            });
            //end login fb
        }

    }

    jQuery('.search-box-btn').unbind('click').click(function(){
        jQuery(".search-box").slideToggle()
    });

    jQuery(".g-signin2").click(function(){
        g_google_login = true;
    });
    
/*

	$('article #myCarousel item img').bind("load",function(){
   $(this).attr('data-height',$(this).height()); 
});
$('article #myCarousel').bind('slide.bs.carousel', function (e) {
    $("article .slide-show .carousel-indicators").css('top',(parseInt($(e.relatedTarget).find('img').attr('data-height'))+2)+'px');
    
});
$(document).ready(function() {
    $("article .slide-show .carousel-indicators").css(
    $("article .slide-show .carousel-indicators").css('top',($("article .slide-show .active img").height()+2)+'px');
})	
$( window ).resize(function() { 
	
    $("article .slide-show .carousel-indicators").css('top',($("article .slide-show .active img").height()+2)+'px');
        
});
	
	
*/