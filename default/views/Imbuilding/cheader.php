<?php
defined('SYSPATH') OR die('No direct access allowed.');
$url_base = curl::base();
$db = CDatabase::instance();
//~~
$org_id = CF::org_id();
$get_code_org = "default";
$get_code = '';
$fav_icon = '';
$item_image = '';
$org_name = '';
if ($org_id) {
    $get_code = cdbutils::get_row("SELECT * FROM org WHERE org_id = " . $db->escape($org_id));
    $get_code_org = $get_code->code;
    $fav_icon = $get_code->item_favicon;
    $item_image = $get_code->item_image;
    $org_name = $get_code->name;
}

$keyword = carr::get($_GET, 'keyword');


$controller = CF::instance();
$page = 'product';
if (method_exists($controller, 'page')) {
    $page = $controller->page();
}
if (carr::get($_GET, 'page') != null && CFRouter::$controller == 'search') {
    $page = carr::get($_GET, 'page');
}

if ($page != 'product') {
    $url_base = curl::base() . $page . '/home/';
}


$data_org = org::get_org($org_id);
$store = '';
$have_product = 0;
$have_gold = 0;
$have_service = 0;
$have_register = 0;
$have_deposit = 0;

if (!empty($data_org)) {
    $store = $data_org->name;
    $have_product = $data_org->have_product;
    $have_gold = $data_org->have_gold;
    $have_service = $data_org->have_service;
    $have_register = $data_org->have_register;
    $have_deposit = $data_org->have_deposit;
}
$url_site = cms::get_option('site_url', 'single', $org_id);

$page_type = "produk";
if ($page == "service") {
    $page_type = "jasa";
}
$url_help = curl::base() . 'read/page/index/beli-' . $page_type . '-di-' . cstr::sanitize($org_name);

global $additional_footer_js;
?>
<!DOCTYPE html>
<html lang="id">
    <head>
        <?php echo $additional_head; ?>
        <title><?php echo $store; ?></title>
        <meta charset="UTF-8">
        <?php
        $login_from_google = ccfg::get("login_google");
        if ($login_from_google) {
            ?>
            <meta name="google-signin-client_id" content="<?php echo ccfg::get('login_google_app_id'); ?>">
        <?php } ?>
        <link rel="icon" type="image/png" href="<?= curl::base() . 'application/admin62hallfamily/' . $get_code_org . '/upload/logo/item_favicon/' . $fav_icon ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
        <?php echo $head_client_script; ?>
        <script type="text/javascript">
            var g_google_login = false;
        </script>

    </head>
    <?php $theme_color = ''; ?>
    <body class="<?php echo $theme_color; ?>">
        <!-- header -->
        <header>
            <div class="container">
                <div class="im-header">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="im-menu">
                                <ul class="top-menu-left">
                                    <li><a href=""><?php echo clang::__('Help');?></a></li>
                                    <li class="pos-relative">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo clang::__('Status Transaction');?></a>
                                        <div class="panel dropdown-menu dropdown-transaction-status" role="menu">
                                            <div class="panel-heading"><?php echo clang::__('Check Order Status');?></div>
                                            <div class="panel-body">
                                                <form action="<?php echo curl::base();?>retrieve/invoice" id="form-status-pesanan" name="form-status-pesanan">
                                                    <div class="form-group">
                                                        <label for="nomor-pesanan"><?php echo clang::__('Order Number');?></label>
                                                        <input type="text" id="nomor-pesanan" name="nomor-pesanan" class="form-control"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email"><?php echo clang::__('Email');?></label>
                                                        <input type="text" id="email" name="email" class="form-control"/>
                                                    </div>
                                                    <button type="submit" class="btn btn-primary btn-imbuilding"><?php echo clang::__('View');?></button>
                                                </form>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <form action="/search">
                                <div class="im-search">
                                    <div class="ico-search"></div>
                                    <input class="form-control im-input" id="keyword" name="keyword" placeholder="Search">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6 col-xs-6 col-xxs-600">
                                    <div class="im-logo">
                                    <?php 
                                        $logo = curl::base() . 'application/admin62hallfamily/' . $get_code_org . '/upload/logo/item_image/' . $item_image; 
                                    ?>
                                    <a href="<?php echo curl::base();?>" class="brand-logo" title="Home">
                                        <img src="<?php echo $logo;?>" alt="<?php echo $store;?>"/>
                                    </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-6 col-xxs-600">
                                    <div class="im-menu hidden-xs">
                                        <ul class="top-menu-right">
                                            <li><a href=""><?php echo clang::__('Currency');?></a></li>
                                            <li><a href=""><?php echo clang::__('Language');?></a></li>
                                        </ul>
                                    </div>
                                    
                                    <!-- <div class="ico-bookmark">
                                        <div class="ico-bookmark-count">0</div>
                                    </div> -->

                                    <div class="container-login-cart">
                                        <div class="shoping-cart-wrapper">
                                            <div id="shopping-cart" class="shopping-cart">

                                            </div>
                                        </div>
                                        <div class="login-register-wrapper">
                                            <button class="btn btn-primary btn-imbuilding-light">Login</button>
                                            <button class="btn btn-primary btn-imbuilding-light">Register</button>
                                        </div>
                                        <!-- <ul>
                                            <li><a href=""><?php //echo clang::__('Register');?></a></li>
                                            <li><a href=""><?php //echo clang::__('Login');?></a></li>
                                        </ul> -->

                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div> <!--end container-->
            
            <div class="main-menu-wrap">
                <div class="container">
                    <div class="visible-xs">
                        <div class="ico-show-xs fa fa-bars"></div>
                    </div>

                    <?php
                        $product_category = product_category::get_product_category_menu($page, '', CF::org_id());
                        $txt_menu = lapakbangunan::generate_menu($product_category);
                        echo $txt_menu;
                    ?>
                </div>
            </div>
        </header>
        <!-- end header -->

        <!-- main content -->
        <div id="main-content">
