<body style="margin:0;padding:0;font-family: \'Open Sans\', Arial, serif; font-weight: 400; ;">
   <div class="main-wrapper" style="width:100%;margin:0;padding:0;">
       <?php 
	   $header = CView::factory('email/header');
	   $header->org_id = $org_id;
	   echo $header->render();
       ?>
<!-- Content -->
                <div class="content" style="padding:30px;">
                        <div class="title-left" style="font-size:18px;font-weight:bold;font-family:">
                                <?php echo clang::__('Member').' '.$member_name; ?> | <?php echo $member_email; ?>
                        </div>
                        <div class="content-text">
                                <p>
                                    <?php echo clang::__('Has been transfered :'); ?>
                                </p>
                                <table>
                                <tr>
                                    <td width="250">
                                        <?php echo clang::__('Name of account Holder');?>
                                    </td>
                                    <td>
                                        : <?php echo $payment_from; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo clang::__('Amount of Topup');?>
                                    </td>
                                    <td>
                                        : Rp. <?php echo ctransform::thousand_separator($nominal); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo clang::__('Time Transfer');?>
                                    </td>
                                    <td>
                                        : <?php echo $confirmed; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo clang::__('Note');?>
                                    </td>
                                    <td>
                                        : <?php echo $note; ?>
                                    </td>
                                </tr>
                                </table>
                        </div>
                        <br/>
                        <div class="title-left" style="font-size:18px;font-weight:bold;font-family:">
                                <?php echo clang::__('To 62HallFamily Bank Account :');?>
                        </div>
                        <div class="content-text">
                                <table>
                                <tr>
                                    <td width="250">
                                        <?php echo clang::__('Account Name');?>
                                    </td>
                                    <td>
                                        : <?php echo $acc_name; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo clang::__('Bank Name');?>
                                    </td>
                                    <td>
                                        : <?php echo $bank_name; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo clang::__('Account Number');?>
                                    </td>
                                    <td>
                                        : <?php echo $acc_no; ?>
                                    </td>
                                </tr>
                                </table>
                            <p>
                                <?php echo clang::__('Keep up with special offers from us by following our newsletter. Thank you for choosing the 62HallFamily as a place to shop Online.'); ?>
                            </p>
                        </div>
                </div>

<?php 
    
   $footer = CView::factory('email/footer');
   $footer->member_email = $member_email;
   echo $footer->render();
    
   ?>
    </div>
</body>
