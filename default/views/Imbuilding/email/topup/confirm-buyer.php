<body style="margin:0;padding:0;font-family: \'Open Sans\', Arial, serif; font-weight: 400; ;">
  <div class="main-wrapper" style="width:100%;margin:0;padding:0;">
	<?php
	   $header = CView::factory('email/header');
	   $header->org_id = $org_id;
	   echo $header->render();
	?>
	<!-- Content -->
	<div class="content" style="padding:30px;">
	  <div class="title-left" style="font-size:18px;font-weight:bold;font-family:">
		<?php echo clang::__('Dear') . ' ' . $member_name; ?>
	  </div>
	  <div class="content-text">
		<p>
		  <?php echo clang::__('Thanks for filling the form Top Up.'); ?><br/>
		  <?php echo clang::__('Our staff will immediately check your payment. If within 60 minutes of your balance is not added, please contact out customer service.'); ?><br/>
		  <?php echo clang::__('Your balance will automatically added after checked from our staff.'); ?>
		</p>
		<p>
		  <?php echo clang::__('Keep up with special offers from us by following our newsletter. Thank you for choosing the 62HallFamily as a place to shop Online.'); ?>
		</p>
	  </div>
	</div>

	<?php
	$footer = CView::factory('email/footer');
	$footer->member_email = $member_email;
	echo $footer->render();
	?>
  </div>
</body>
