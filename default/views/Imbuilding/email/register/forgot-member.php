 <body style="margin:0;padding:0;font-family: \'Open Sans\', Arial, serif; font-weight: 400; ;">
   <div class="main-wrapper" style="margin:0;padding:0;">
       <?php 
			// routing theme
			$theme = CF::theme();
			$theme_ready = array('62hallmall', 'imbuilding');
			$path = '';
			if (strlen($theme) > 0) {
				if (in_array($theme, $theme_ready)) {
					$path = $theme .'/';
				}
			}
            $header = CView::factory($path.'email/header');
			$header->org_id = $org_id;
			echo $header->render();
			$data_org=org::get_org($org_id);
		?>
<!-- Content -->
        <div class="content" style="padding:30px;font-size:14px;">
            <div class="title-left" style="font-size:18px;">
                <?php echo clang::__('Hi').' '.$member_name; ?>,
            </div>
            <div class="content-text" style="line-height:20px;">
                <p>
                    <?php echo clang::__('Siap mengganti password Anda ?');?>
				</p>
				<p style="text-align:center;">
					<a href="#" style="display:inline-block;background:#24B5E5;text-decoration:none;color:#fff;padding:5px 20px;border-radius:2px;">Topup</a>
				</p>
				<p>
					<?php echo clang::__('Anda mempunyai 24 jam untuk mengganti password Anda.');?><br>
					<?php echo clang::__('Setelah lewat dari jangka waktu, Anda dapat meminta link yang baru.');?>
				</p>
            </div>
        </div>

<?php 
		$footer = CView::factory($path.'email/footer');
		$footer->member_email = $member_email;
		echo $footer->render();    
	 ?>
    </div>
</body>
