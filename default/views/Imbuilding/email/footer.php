<div class="footer-wrapper" style="line-height:0.7em">
	<div style="margin-top:10px;font-size: 14px;color:#D0E8FE;background: #024c93;padding: 20px 30px;padding-top: 30px;line-height: 16px;text-align: center;">
		<a href="#" style="color:#ffffff;text-decoration:none;font-weight:bold;"><?php echo clang::__('Syarat dan Ketentuan'); ?></a> | <a href="#" style="font-weight:bold;color:#ffffff;text-decoration:none;"><?php echo clang::__('Kebijakan Privasi') ?></a>
		<p><?php echo clang::__('Email ini di kirimkan kepada') . ' <a href="mailto:'.$member_email.'" style="font-weight:bold;color:#fff;">' . $member_email; ?>.</a></p>
		<p><?php echo clang::__('Jika anda membutuhkan bantuan silahkan klik') . ' <a style="color:#fff;text-decoration:none;" target="_blank" href="http://' . ccfg::get('domain') . '/read/page/index/hubungi-kami">di sini</a>'; ?>.</p>
		<hr style="margin:30px auto;margin-bottom:20px;border-style: solid;">
		<div style="padding-bottom:20px;">
			<div style="text-align:left;display:inline-block;float:left;color:#ffffff;"><?php
				$footer = cms_options::get('footer');
				$footer = str_replace('<p>', '', $footer);
				$footer = str_replace('</p>', '', $footer);
				echo $footer;
				?>
			</div>
			<div style="display:inline-table;float:right;">
				<img src="<?php echo curl::httpbase() . 'application/62hallfamily/default/media/img/imbuilding/icon-twitter.png'; ?>" style="margin:0px 10px;display:inline-block;vertical-align:middle;">
				<img src="<?php echo curl::httpbase() . 'application/62hallfamily/default/media/img/imbuilding/icon-facebook.png'; ?>" style="margin:0px 10px;display:inline-block;vertical-align:middle;">
				<img src="<?php echo curl::httpbase() . 'application/62hallfamily/default/media/img/imbuilding/icon-instagram.png'; ?>" style="margin:0px 10px;display:inline-block;vertical-align:middle;">
				<img src="<?php echo curl::httpbase() . 'application/62hallfamily/default/media/img/imbuilding/icon-google.png'; ?>" style="margin:0px 10px;display:inline-block;vertical-align:middle;">
			</div>
			<div style="clear:both;"></div>
		</div>
	</div>
</div>