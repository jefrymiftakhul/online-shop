
<!DOCTYPE html>
<html style="background: white;">
    <head>
                <title>Kinerja Mall</title>
        <meta charset="UTF-8">
                <meta name="google-signin-client_id" content="google id">
                <link rel="icon" type="image/png" href="/application/admin62hallfamily/kinerjamall/upload/logo/item_favicon/">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
        <!--<link href="/application/62hallfamily/default/media/css/bootstrap-3.3.5/bootstrap.css" rel="stylesheet" />-->
<link href="/application/62hallfamily/default/media/css/bootstrap-3.3.5/bootstrap.min.css" rel="stylesheet" />
<link href="/media/css/plugins/chosen/chosen.css" rel="stylesheet" />
<link href="/application/62hallfamily/default/media/css/62hallfamily-select2.css" rel="stylesheet" />
<link href="/application/62hallfamily/default/media/css/font-awesome.min.css" rel="stylesheet" />
<link href="/application/62hallfamily/default/media/css/bootstrap-dropdown.css" rel="stylesheet" />
<link href="/application/62hallfamily/default/media/css/jquery.fancybox.css" rel="stylesheet" />
<link href="/application/62hallfamily/default/media/css/flexslider.css" rel="stylesheet" />
<link href="/application/62hallfamily/default/media/css/jquery-touchspin/jquery.bootstrap-touchspin.css" rel="stylesheet" />
<link href="/application/62hallfamily/default/media/css/62hallfamily-bootstrap-slider.css" rel="stylesheet" />
<link href="/media/css/plugins/validation-engine/jquery.validationEngine.css" rel="stylesheet" />
<link href="/application/62hallfamily/default/media/css/owl.carousel.css" rel="stylesheet" />
<link href="/application/62hallfamily/default/media/css/62hallmall/style.css?v=1" rel="stylesheet" />
        
        <script type="text/javascript">
            var g_google_login = false;
        </script>

    </head>
        <body class="">
        <!-- CPAGE START -->    
<!---------------------------------------------------------------------------->
<!-- header -->
<header>
    <div id="headtop">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    News Ticker
                </div>
                <div class="col-md-4">
                    Top Menu
                </div>
            </div>
        </div>
    </div>
    <div id="header">
        <div class="container">
            <div class="row">
                <div class="col-md-3">LOGO</div>
                <div class="col-md-6">SEARCH BOX</div>
                <div class="col-md-3">MEMBER BUTTON</div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    MAIN MENU
                </div>
            </div>
        </div>
    </div>
</header>
<!-- end header -->
<!-- main content -->
<div id="main-content">
    MAIN CONTENT HERE
</div>
<!-- end main content -->
<!-- footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">LEFT</div>
            <div class="col-md-4">MID</div>
            <div class="col-md-4">RIGHT</div>
        </div>
    </div>
</footer>
<!-- end footer -->
<!---------------------------------------------------------------------------->            
        <!-- CPAGE END -->
        
<script src="/media/js/require.js"></script>

<script language="javascript">
document.addEventListener('capp-started', function(customEvent) {
    
                $('input').blur();
                $('.btn-register-subscribe').click(function(){
                   $('#email_subscribe').focus(); 
                });
			jQuery('#147340919257d270a84c32a804449413').click(function() {				
				
				
				var thiselm=jQuery(this);
				var clicked = thiselm.attr('data-clicked');
			
				thiselm.attr('data-clicked','1');
			
				
				
                $.cresenity.reload('message-subscribe','/subscribe/user_subscribe/','get',{'email_subscribe':$.cresenity.value('#email_subscribe'),'type':$.cresenity.value('#type')});
             
				
				
			});
		




            
});	

require(['/application/62hallfamily/default/media/js/jquery-2.1.1.min.js'],function(){require(['/application/62hallfamily/default/media/js/bootstrap-3.3.5/bootstrap.js'],function(){require(['/application/62hallfamily/default/media/js/bootstrap-3.3.5/bootstrap.min.js'],function(){require(['/media/js/plugins/chosen/chosen.jquery.min.js'],function(){require(['/application/62hallfamily/default/media/js/62hallfamily-select2.js'],function(){require(['/application/62hallfamily/default/media/js/bootstrap-dropdown.js'],function(){require(['/application/62hallfamily/default/media/js/zoomsl-3.0.min.js'],function(){require(['/application/62hallfamily/default/media/js/jquery.fancybox.pack.js'],function(){require(['/application/62hallfamily/default/media/js/flexslider.min.js'],function(){require(['/application/62hallfamily/default/media/js/history/jquery.history.js'],function(){require(['/application/62hallfamily/default/media/js/jquery.bootstrap-touchspin.js'],function(){require(['/application/62hallfamily/default/media/js/jquery.touchSwipe.min.js'],function(){require(['/application/62hallfamily/default/media/js/bootstrap-slider.js'],function(){require(['/media/js/plugins/validation-engine/jquery.validationEngine-2.6.2.js'],function(){require(['/media/js/plugins/validation-engine/languages/jquery.validationEngine-en.js'],function(){require(['/application/62hallfamily/default/media/js/owl.carousel.min.js'],function(){require(['https://apis.google.com/js/platform.js'],function(){var capp_started_event_initialized=false;var scrolltotop={setting:{startline:100,scrollto:0,scrollduration:1000,fadeduration:[500,100]},controlHTML:'<img src="/media/img/up.png" style="width:51px; height:42px" />',controlattrs:{offsetx:5,offsety:5},anchorkeyword:'#top',state:{isvisible:false,shouldvisible:false},scrollup:function(){if(!this.cssfixedsupport)
this.$control.css({opacity:0})
var dest=isNaN(this.setting.scrollto)?this.setting.scrollto:parseInt(this.setting.scrollto)
if(typeof dest=="string"&&jQuery('#'+dest).length==1)
dest=jQuery('#'+dest).offset().top
else
dest=0
this.$body.animate({scrollTop:dest},this.setting.scrollduration);},keepfixed:function(){var $window=jQuery(window)
var controlx=$window.scrollLeft()+$window.width()-this.$control.width()-this.controlattrs.offsetx
var controly=$window.scrollTop()+$window.height()-this.$control.height()-this.controlattrs.offsety
this.$control.css({left:controlx+'px',top:controly+'px'})},togglecontrol:function(){var scrolltop=jQuery(window).scrollTop()
if(!this.cssfixedsupport)
this.keepfixed()
this.state.shouldvisible=(scrolltop>=this.setting.startline)?true:false
if(this.state.shouldvisible&&!this.state.isvisible){this.$control.stop().animate({opacity:1},this.setting.fadeduration[0])
this.state.isvisible=true}
else if(this.state.shouldvisible==false&&this.state.isvisible){this.$control.stop().animate({opacity:0},this.setting.fadeduration[1])
this.state.isvisible=false}},init:function(){jQuery(document).ready(function($){var mainobj=scrolltotop
var iebrws=document.all
mainobj.cssfixedsupport=!iebrws||iebrws&&document.compatMode=="CSS1Compat"&&window.XMLHttpRequest
mainobj.$body=(window.opera)?(document.compatMode=="CSS1Compat"?$('html'):$('body')):$('html,body')
mainobj.$control=$('<div id="topcontrol">'+mainobj.controlHTML+'</div>').css({position:mainobj.cssfixedsupport?'fixed':'absolute',bottom:mainobj.controlattrs.offsety,right:mainobj.controlattrs.offsetx,opacity:0,cursor:'pointer'}).attr({title:'Scroll Back to Top'}).click(function(){mainobj.scrollup();return false}).appendTo('body')
if(document.all&&!window.XMLHttpRequest&&mainobj.$control.text()!='')
mainobj.$control.css({width:mainobj.$control.width()})
mainobj.togglecontrol()
$('a[href="'+mainobj.anchorkeyword+'"]').click(function(){mainobj.scrollup()
return false})
$(window).bind('scroll resize',function(e){mainobj.togglecontrol()})})}}
scrolltotop.init();jQuery(document).on('click','a.confirm',function(e){var ahref=$(this).attr('href');var message=$(this).attr('data-confirm-message');var no_double=$(this).attr('data-no-double');var clicked=$(this).attr('data-clicked');var btn=jQuery(this);btn.attr('data-clicked','1');if(no_double){if(clicked==1)return false;}
if(!message){message="Apakah anda yakin ?";}else{message=$.cresenity.base64.decode(message);}
str_confirm="OK";str_cancel="Cancel";e.preventDefault();e.stopPropagation();bootbox.confirm(message,function(confirmed){if(confirmed){window.location.href=ahref;}else{btn.removeAttr('data-clicked');}});return false;});jQuery(document).on('click','input[type=submit].confirm',function(e){var submitted=$(this).attr('data-submitted');var btn=jQuery(this);if(submitted=='1')return false;btn.attr('data-submitted','1');var message=$(this).attr('data-confirm-message');if(!message){message="Apakah anda yakin ?";}else{message=$.cresenity.base64.decode(message);}
str_confirm="OK";str_cancel="Cancel";bootbox.confirm(message,str_cancel,str_confirm,function(confirmed){if(confirmed){jQuery(e.target).closest('form').submit();}else{btn.removeAttr('data-submitted');}});return false;});jQuery(document).ready(function(){jQuery("#toggle-subnavbar").click(function(){var cmd=jQuery("#toggle-subnavbar span").html();if(cmd=='Hide'){jQuery('#subnavbar').slideUp('slow');jQuery("#toggle-subnavbar span").html('Show');}else{jQuery('#subnavbar').slideDown('slow');jQuery("#toggle-subnavbar span").html('Hide');}});jQuery("#toggle-fullscreen").click(function(){$.cresenity.fullscreen(document.documentElement);});});;(function($,window,document,undefined)
{$.cresenity={_filesadded:"",_loadjscss:function(filename,filetype,callback){if(filetype=="js"){var fileref=document.createElement('script')
fileref.setAttribute("type","text/javascript")
fileref.setAttribute("src",filename)}else if(filetype=="css"){var fileref=document.createElement("link")
fileref.setAttribute("rel","stylesheet")
fileref.setAttribute("type","text/css")
fileref.setAttribute("href",filename)}
if(typeof fileref!="undefined"){fileref.onload=$.cresenity._handle_response_callback(callback);if(typeof(callback)==='function'){fileref.onreadystatechange=function(){if(this.readyState=='complete'){$.cresenity._handle_response_callback(callback);}}}
document.getElementsByTagName("head")[0].appendChild(fileref);}},_removejscss:function(filename,filetype){var targetelement=(filetype=="js")?"script":(filetype=="css")?"link":"none";var targetattr=(filetype=="js")?"src":(filetype=="css")?"href":"none";var allsuspects=document.getElementsByTagName(targetelement);for(var i=allsuspects.length;i>=0;i--){if(allsuspects[i]&&allsuspects[i].getAttribute(targetattr)!=null&&allsuspects[i].getAttribute(targetattr).indexOf(filename)!=-1){allsuspects[i].parentNode.removeChild(allsuspects[i])}}},_handle_response:function(data,callback){if(data.css_require&&data.css_require.length>0){for(var i=0;i<data.css_require.length;i++){$.cresenity.require(data.css_require[i],'css');}}
require(data.js_require,callback);return;$.cresenity._filesloaded=0;$.cresenity._filesneeded=0;if(data.css_require&&data.css_require.length>0)$.cresenity._filesneeded+=data.css_require.length;if(data.js_require&&data.js_require.length>0)$.cresenity._filesneeded+=data.js_require.length;if(data.css_require&&data.css_require.length>0){for(var i=0;i<data.css_require.length;i++){$.cresenity.require(data.css_require[i],'css',callback);}}
if(data.js_require&&data.js_require.length>0){for(var i=0;i<data.js_require.length;i++){$.cresenity.require(data.js_require[i],'js',callback);}}
if($.cresenity._filesloaded==$.cresenity._filesneeded){callback();}},_handle_response_callback:function(callback){$.cresenity._filesloaded++;if($.cresenity._filesloaded==$.cresenity._filesneeded){callback();}},require:function(filename,filetype,callback){if($.cresenity._filesadded.indexOf("["+filename+"]")==-1){$.cresenity._loadjscss(filename,filetype,callback);$.cresenity._filesadded+="["+filename+"]"}else{$.cresenity._filesloaded++;if($.cresenity._filesloaded==$.cresenity._filesneeded){callback();}}},days_between:function(date1,date2){var ONE_DAY=1000*60*60*24
var date1_ms=date1.getTime()
var date2_ms=date2.getTime()
var difference_ms=Math.abs(date1_ms-date2_ms)
return Math.round(difference_ms/ONE_DAY)},set_confirm:function(selector){$(selector).click(function(e){var ahref=$(this).attr('href');e.preventDefault();e.stopPropagation();bootbox.confirm("Are you sure?",function(confirmed){if(confirmed){window.location.href=ahref;}});});},is_number:function(n){return!isNaN(parseFloat(n))&&isFinite(n);},get_dialog:function(dlg_id,title){var div_content=$('body #'+dlg_id+' #'+dlg_id+'_content');if(div_content.length){$('body #'+dlg_id+' #'+dlg_id+'_header h3').html(title);return div_content;}
if(title=="undefined")title="";if(!title)title="";var div=$('<div>').attr('id',dlg_id);var btnClose='<a href="'+'javascript:;'+'" class="close" data-dismiss="modal">&times;</a>';btnClose='';div.append('<div class="modal-header" id="'+dlg_id+'_header">'+btnClose+'<h3>'+title+'</h3></div>')
div_content=$('<div class="modal-body" id="'+dlg_id+'_content"></div>');div.append(div_content);var btn_close=$('<a id="'+dlg_id+'_close">').addClass('btn').attr('href','javascript:void(0)');btn_close.append('<i class="icon icon-close"></i> Close');btn_close.click(function(){$('#'+dlg_id+'').modal('hide');$('#'+dlg_id+'').remove();});div_footer=$('<div class="modal-footer" id="suspended_dlg_footer"></div>');div_footer.append(btn_close);div.append(div_footer);div.css('overflow','hidden');div.addClass('modal');$("body").append(div);return div_content;},message:function(type,message,alert_type,callback){alert_type=typeof alert_type!=='undefined'?alert_type:'notify';var container=$('#container');if(alert_type=='bootbox'){if(typeof callback=='undefined'){bootbox.alert(message);}else{bootbox.alert(message,callback);}}
if(alert_type=='notify'){obj=$('<div>');container.prepend(obj);obj.addClass('notifications');obj.addClass('top-right');obj.notify({'message':{text:message},'type':type}).show();}},thousand_separator:function(rp){rp=""+rp;var rupiah="";var vfloat="";var ds=' ';var ts='.';var dd=0;var dd=parseInt(dd);var minus_str="";if(rp.indexOf("-")>=0){minus_str=rp.substring(rp.indexOf("-"),1);rp=rp.substring(rp.indexOf("-")+1);}
if(rp.indexOf(".")>=0){vfloat=rp.substring(rp.indexOf("."));rp=rp.substring(0,rp.indexOf("."));}
p=rp.length;while(p>3){rupiah=ts+rp.substring(p-3)+rupiah;l=rp.length-3;rp=rp.substring(0,l);p=rp.length;}
rupiah=rp+rupiah;vfloat=vfloat.replace('.',ds);if(vfloat.length>dd)vfloat=vfloat.substring(0,dd+1);return minus_str+rupiah+vfloat;},replace_all:function(string,find,replace){escaped_find=find.replace(/([.*+?^=!:${}()|\[\]\/\\])/g,"\\$1");return string.replace(new RegExp(escaped_find,'g'),replace);},format_currency:function(rp){return $.cresenity.thousand_separator(rp);},unformat_currency:function(rp){if(typeof rp=="undefined"){rp='';}
var ds=' ';var ts='.';var last3=rp.substr(rp.length-3);var char_last3=last3.charAt(0);if(char_last3!=ts){rp=this.replace_all(rp,ts,'');}
rp=rp.replace(ds,".");return rp;},base64:{_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=this._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=this._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}},url:{add_query_string:function(url,key,value){key=encodeURI(key);value=encodeURI(value);var url_array=url.split('?');var query_string='';var base_url=url_array[0];if(url_array.length>1)query_string=url_array[1];var kvp=query_string.split('&');var i=kvp.length;var x;while(i--){x=kvp[i].split('=');if(x[0]==key){x[1]=value;kvp[i]=x.join('=');break;}}
if(i<0){kvp[kvp.length]=[key,value].join('=');}
query_string=kvp.join('&');if(query_string.substr(0,1)=='&')query_string=query_string.substr(1);return base_url+'?'+query_string;},replace_param:function(url){var available=true;while(available){matches=url.match(/{([\w]*)}/);if(matches!=null){var key=matches[1];var val=null;if($('#'+key).length>0){var val=$.cresenity.value('#'+key);}
if(val==null){val=key;}
url=url.replace('{'+key+'}',val);}else{available=false;}}
return url;}},reload:function(id_target,url,method,data_addition){if(!method)method="get";var xhr=jQuery('#'+id_target).data('xhr');if(xhr)xhr.abort();url=$.cresenity.url.replace_param(url);if(typeof data_addition=='undefined')data_addition={};url=$.cresenity.url.add_query_string(url,'capp_current_container_id',id_target);jQuery('#'+id_target).addClass('loading');jQuery('#'+id_target).empty();jQuery('#'+id_target).append(jQuery('<div>').attr('id',id_target+'-loading').css('text-align','center').css('margin-top','100px').css('margin-bottom','100px').append(jQuery('<i>').addClass('icon icon-repeat icon-spin icon-4x')))
jQuery('#'+id_target).data('xhr',jQuery.ajax({type:method,url:url,dataType:'json',data:data_addition,}).done(function(data){$.cresenity._handle_response(data,function(){jQuery('#'+id_target).html(data.html);if(data.js&&data.js.length>0){var script=$.cresenity.base64.decode(data.js);eval(script);}
jQuery('#'+id_target).removeClass('loading');jQuery('#'+id_target).data('xhr',false);if(jQuery('#'+id_target).find('.prettyprint').length>0){window.prettyPrint&&prettyPrint();}});}).error(function(obj,t,msg){if(msg!='abort'){$.cresenity.message('error','Error, please call administrator... ('+msg+')');}}));},append:function(id_target,url,method,data_addition){if(!method)method="get";var xhr=jQuery('#'+id_target).data('xhr');url=$.cresenity.url.replace_param(url);if(typeof data_addition=='undefined')data_addition={};url=$.cresenity.url.add_query_string(url,'capp_current_container_id',id_target);jQuery('#'+id_target).addClass('loading');jQuery('#'+id_target).append(jQuery('<div>').attr('id',id_target+'-loading').css('text-align','center').css('margin-top','100px').css('margin-bottom','100px').append(jQuery('<i>').addClass('icon icon-repeat icon-spin icon-4x')))
jQuery('#'+id_target).data('xhr',jQuery.ajax({type:method,url:url,dataType:'json',data:data_addition,}).done(function(data){$.cresenity._handle_response(data,function(){jQuery('#'+id_target).append(data.html);jQuery('#'+id_target).find('#'+id_target+'-loading').remove();var script=$.cresenity.base64.decode(data.js);console.log(script);eval(script);jQuery('#'+id_target).removeClass('loading');jQuery('#'+id_target).data('xhr',false);if(jQuery('#'+id_target).find('.prettyprint').length>0){window.prettyPrint&&prettyPrint();}});}).error(function(obj,t,msg){if(msg!='abort'){$.cresenity.message('error','Error, please call administrator... ('+msg+')');}}));},show_tooltip:function(id_target,url,method,text,toggle,position,title,data_addition){if(typeof title=='undefined')title='';if(typeof position=='undefined')position='auto';if(typeof data_addition=='undefined')data_addition={};if(typeof text=='undefined')text=' ';var _tooltip_html='<div class="popover" id="popover'+id_target+'" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content" id="popover-content'+id_target+'"></div></div>';var selection=jQuery('#'+id_target);var handle;var html_content=text;var parent=$(jQuery('#'+id_target).html());var close_button="<a id='closetooltip"+id_target+"' class='close' style='margin-left:10px;'>X</a>";if(typeof jQuery('#popover'+id_target).html()=='undefined'){if(url.length>0){text=jQuery('<div>').attr('id',id_target+'-loading').css('text-align','center').css('margin-top','100px').css('margin-bottom','100px').append(jQuery('<i>').addClass('icon icon-repeat icon-spin icon-4x'));}
if(position=='auto'||position==''){$('#'+id_target).popover({container:'body',title:close_button+title,html:true,trigger:"manual",content:text,selector:true,template:_tooltip_html,});}else{$('#'+id_target).popover({container:'body',title:close_button+title,html:true,trigger:"manual",content:text,placement:position,selector:true,template:_tooltip_html,});}
$('#'+id_target).popover('show');handle=$('.tooltip-inner',parent);if(url.length>0){var xhr=handle.data('xhr');if(xhr)xhr.abort();url=$.cresenity.url.add_query_string(url,'capp_current_container_id','#tooltip_'+id_target);handle.data('xhr',jQuery.ajax({type:method,url:url,dataType:'json',data:data_addition,}).done(function(data){$.cresenity._handle_response(data,function(){$('#'+id_target).popover('destroy');if(position=='auto'||position==''){$('#'+id_target).popover({animation:false,title:close_button+title,html:true,trigger:"manual",content:html_content+data.html,selector:true,template:_tooltip_html,});}else{$('#'+id_target).popover({animation:false,title:close_button+title,html:true,trigger:"manual",content:html_content+data.html,placement:position,selector:true,template:_tooltip_html,});}
$('#'+id_target).popover('show');if(data.js&&data.js.length>0){var script=$.cresenity.base64.decode(data.js);eval(script);}
$("#closetooltip"+id_target).on("click",function(){$('#'+id_target).popover('destroy');});});}).error(function(obj,t,msg){if(msg!='abort'){$.cresenity.message('error','Error, please call administrator... ('+msg+')');}
$('#'+id_target+'-loading').remove();}));}else{$("#closetooltip"+id_target).on("click",function(){$('#'+id_target).popover('destroy');});}}else{if(toggle=="1"){$('#'+id_target).popover('destroy');}}},show_real_notification:function(id_target,url){var selection=jQuery('#'+id_target);url=$.cresenity.url.replace_param(url);var handle;if(selection.length==0){selection=jQuery('<div/>').attr('id',id_target);}
handle=selection;handle.data('xhr',jQuery.ajax({type:'post',data:{'title':document.title,},url:url,dataType:'json',}).done(function(data){$.cresenity._handle_response(data,function(){jQuery('#'+id_target).html(data.html);if(data.js&&data.js.length>0){var script=$.cresenity.base64.decode(data.js);eval(script);}
jQuery('#'+id_target).removeClass('loading');jQuery('#'+id_target).data('xhr',false);if(jQuery('#'+id_target).find('.prettyprint').length>0){window.prettyPrint&&prettyPrint();}});}).error(function(obj,t,msg){if(msg!='abort'){$.cresenity.message('error','Error, please call administrator... ('+msg+')');}}));},show_dialog:function(id_target,url,method,title,data_addition){if(!title)title='Dialog';if(typeof data_addition=='undefined')data_addition={};var _dialog_html="<div class='modal' style=\"display: none;\" >"+"<div class='modal-header loading'>"+"<a href='#' class='close'></a>"+"<span class='loader'></span><h3></h3>"+"</div>"+"<div class='modal-body'>"+"</div>"+"<div class='modal-footer'>"+"</div>"+"</div>";var selection=jQuery('#'+id_target);var handle;var dialog_is_remove=false;if(selection.length==0){selection=jQuery('<div/>').attr('id',id_target);dialog_is_remove=true;}
url=$.cresenity.url.add_query_string(url,'capp_current_container_id',id_target);if(!selection.is(".modal-body")){var overlay=$('<div class="modal-backdrop"></div>').hide();var parent=$(_dialog_html);jQuery(".modal-header a.close",parent).text(unescape("%D7")).click(function(event){event.preventDefault();if(dialog_is_remove){handle.parent().prev(".modal-backdrop").remove();jQuery(this).parents(".modal").find(".modal-body").parent().remove();}else{handle.parent().prev(".modal-backdrop").hide();jQuery(this).parents(".modal").find(".modal-body").parent().hide();}});jQuery("body").append(overlay).append(parent);jQuery(".modal-header h3",parent).html(title);handle=$(".modal-body",parent);if(selection.is("div")&&selection.length==1){handle.replaceWith(selection);selection.addClass("modal-body").show();handle=selection;}
else{handle.append(selection);}}
else{handle=selection;}
if(!method)method="get";var xhr=handle.data('xhr');if(xhr)xhr.abort();url=$.cresenity.url.replace_param(url);jQuery('#'+id_target).append(jQuery('<div>').attr('id',id_target+'-loading').css('text-align','center').css('margin-top','100px').css('margin-bottom','100px').append(jQuery('<i>').addClass('icon icon-repeat icon-spin icon-4x')))
if(!handle.is(".opened")){overlay.show();handle.addClass("opened").parent().show();}
handle.data('xhr',jQuery.ajax({type:method,url:url,dataType:'json',data:data_addition,}).done(function(data){$.cresenity._handle_response(data,function(){jQuery('#'+id_target).html(data.html);if(data.js&&data.js.length>0){var script=$.cresenity.base64.decode(data.js);eval(script);}
jQuery('#'+id_target).removeClass('loading');jQuery('#'+id_target).data('xhr',false);if(jQuery('#'+id_target).find('.prettyprint').length>0){window.prettyPrint&&prettyPrint();}
if(data.title){jQuery('#'+id_target+'').parent().find('.modal-header h4').html(data.title);}});}).error(function(obj,t,msg){if(msg!='abort'){$.cresenity.message('error','Error, please call administrator... ('+msg+')');}}));},value:function(elm){elm=jQuery(elm);if(elm.length==0)return null;if(elm.attr('type')=='checkbox'){if(!elm.is(':checked')){return null;}}
if(elm.val()!='undefined'){return elm.val();}
if(elm.attr('value')!='undefined'){return elm.attr('value');}
return elm.html();},dialog:{alert:function(message,options){$.fn.dialog2.helpers.alert(message,{});},prompt:function(message,options){$.fn.dialog2.helpers.prompt(message,{});},confirm:function(message,options){$.fn.dialog2.helpers.confirm(message,{});},show:function(selector,options){$(selector).dialog2(options);}},fullscreen:function(element){if(!$('body').hasClass("full-screen")){$('body').addClass("full-screen");if(element.requestFullscreen){element.requestFullscreen();}else if(element.mozRequestFullScreen){element.mozRequestFullScreen();}else if(element.webkitRequestFullscreen){element.webkitRequestFullscreen();}else if(element.msRequestFullscreen){element.msRequestFullscreen();}}else{$('body').removeClass("full-screen");if(document.exitFullscreen){document.exitFullscreen();}else if(document.mozCancelFullScreen){document.mozCancelFullScreen();}else if(document.webkitExitFullscreen){document.webkitExitFullscreen();}}}}
String.prototype.format_currency=function(){return $.cresenity.format_currency(this)};String.prototype.unformat_currency=function(){return $.cresenity.unformat_currency(this);};})(this.jQuery,window,document);if(typeof $.app=='undefined'){var window_height=$(window).height();(function($){$.app={show_dialog:function(id_target,title,body,id_custom,close){var modal_class='';if(!title){title='Dialog';}
if(!id_custom){id_custom='';}
if(title=='Error'){modal_class='modal-error';}
var _dialog_html="<div  id='custom_modal_"+id_custom+"' class='modal "+id_custom+"'>"+"  <div class='modal-dialog'>"+"    <div class='modal-content clearfix "+modal_class+"'>"+"       <div class='modal-header'>";if(!close||close=='1'){_dialog_html+="        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>";}
_dialog_html+="        <h4 class='modal-title'>Modal title</h4>"+"      </div>"+"      <div class ='modal-body'>"+"        <p>One fine body…</p>"+"       </div>"+"    </div><!-- /.modal-content -->"+"  </div><!-- /.modal-dialog - ->"+"</div><!-- /.modal -->";var selection=jQuery('#'+id_target);var handle;var dialog_is_remove=false;if(selection.length==0){selection=jQuery('<div/>').attr('id',id_target);dialog_is_remove=true;}
var selection_exist=false;if(!selection.is(".modal-body")){var parent=$(_dialog_html);handle=$(".modal-body",parent);jQuery(".modal-header button.close",parent).text(unescape("%D7")).click(function(event){event.preventDefault();if(dialog_is_remove){$('body .modal-backdrop.modal-backdrop-'+id_target+'').remove();jQuery(this).parents(".modal").find(".modal-dialog").parent().remove();}
else{$('body .modal-backdrop.modal-backdrop-'+id_target+'').hide();jQuery(this).parents(".modal").find(".modal-dialog").parent().hide();}});jQuery("body").append(parent);jQuery(".modal-header h4",parent).html(title);if(selection.is("div")&&selection.length==1){handle.replaceWith(selection);selection.addClass("modal-body").show();handle=selection;}
else{handle.append(selection);}}
else{handle=selection;parent=handle.parents('.modal');if(parent.length>0)parent=jQuery(parent[0]);selection_exist=true;}
var overlay=false;if(!selection_exist){overlay=$('.modal-backdrop.modal-backdrop-'+id_target);if(overlay.length==0){overlay=$('<div class="modal-backdrop modal-backdrop-'+id_target+' fade in"></div>').hide();$('body').append(overlay);}else{overlay.hide();}}
if(!handle.is(".opened")){if(overlay){overlay.show();}
parent.addClass("opened").show();}
if(body!=undefined&&body.length>0){if(body.substring(0,4)=='http'||body.substring(0,1)=='/'){jQuery('#'+id_target).append(jQuery('<div>').attr('id',id_target+'-loading').css('text-align','center').css('margin-top','100px').css('margin-bottom','100px').append(jQuery('<i>').addClass('fa fa-repeat fa-spin fa-4x')));jQuery('#'+id_target).data('xhr',jQuery.ajax({type:'post',url:body,dataType:'json',data:{},}).done(function(data){$.cresenity._handle_response(data,function(){jQuery('#'+id_target).append(data.html);jQuery('#'+id_target).find('#'+id_target+'-loading').remove();var script=$.cresenity.base64.decode(data.js);eval(script);jQuery('#'+id_target).removeClass('loading');jQuery('#'+id_target).data('xhr',false);if(jQuery('#'+id_target).find('.prettyprint').length>0){window.prettyPrint&&prettyPrint();}});}).error(function(obj,t,msg){if(msg!='abort'){$.cresenity.message('error','Error, please call administrator... ('+msg+')');}}));}else{jQuery('#'+id_target).html(body);}}
if(selection_exist){parent.modal({'backdrop':'static'});selection.show();}else{parent.show();}
if(id_custom!='undefined'&&id_custom.length>0){var modal_height=$('.'+id_custom).find('.modal-content').height();if(modal_height>window_height){var modal_height_auto=window_height-100;$('.'+id_custom).find('.modal-content').css({'max-height':modal_height_auto+'px','overflow-y':'scroll'});}}},show_loading:function(){$('body').append('<div class="loading-container">'
+'<div class="loading"></div>'
+'<div class="loading-information">Please Wait...</div>'
+'</div>');},hide_loading:function(){$('body').find('.loading-container').remove();}}})(this.jQuery,window,document);$('.btn-popup').on('click',function(event){event.stopPropagation();var popup_target=$(this).attr('data-popup');var is_hide=$('.'+popup_target).hasClass('hide');if(is_hide==true){$('.'+popup_target).removeClass('hide');$(this).addClass('active');}
else{$('.'+popup_target).addClass('hide');$(this).removeClass('active');}});jQuery('.btn-62hallfamily-reg').on('click',function(){jQuery('.state-from').val('');});jQuery('.btn-62hallfamily-login').on('click',function(){jQuery('.state-from').val('login');});$('.btn-modal').on('click',function(event){var modal_content=$(this).attr('data-modal');var modal_title=$(this).attr('data-title');var body_target=$(this).attr('data-target');var body='';if(modal_content&&modal_title){if(modal_content!='logout'){event.preventDefault();if(body_target=='undefined'||body_target.length==0){body_target='62hall-dialog';}
$.app.show_dialog(body_target,modal_title,body,modal_content);}}});var progress_bar='<div class="progress">'
+'<div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'
+'<span class="sr-only">45% Complete</span>'
+'</div>'
+'</div>';$('#nav-side-bar').click(function(){$('ul.categories').toggleClass('show');$('li.categories').toggleClass('show');});$('ul.categories').click(function(){$('ul.categories').toggleClass('show');$('li.categories').toggleClass('show');});$('#nav-side-bar').click(function(){$('.menu-mobile').toggleClass('show');});$('.menu-mobile').click(function(){$('.menu-mobile').toggleClass('show');});$('body').click(function(evt){if($('ul.categories').hasClass('show')){if(evt.target.id=="menu-categories")
return;if($(evt.target).closest('#menu-categories').length)
return;if(evt.target.id=="nav-side-bar")
return;if($(evt.target).closest('#nav-side-bar').length)
return;$('ul.categories').toggleClass('show');$('li.categories').toggleClass('show');}});jQuery(document).mouseup(function(e){var option=jQuery(e.target).closest(".62hall-options");var dropdown=jQuery(e.target).closest(".62hall-dropdown");if(option.length==0&&dropdown.length==0){jQuery(".62hall-options").hide();}});jQuery(document).ready(function(){jQuery('body').on('touchstart.dropdown','.dropdown-menu',function(e){jQuery(".child-1").hide();e.stopPropagation();});$('#keyword').keypress(function(e){if(e.which==13){$(this).closest('form').submit();}});$('#keyword').closest('form').submit(function(){if($('#keyword').val().trim()==''){alert('Please input keyword !!!');return false;}
return true;});$(".carousel-inner").swipe({swipeLeft:function(event,direction,distance,duration,fingerCount){$(this).parent().carousel('prev');},swipeRight:function(){$(this).parent().carousel('next');},threshold:0});$("article .carousel-control").each(function(){if($(this).find('.fa').length==0){if($(this).hasClass('left')){$(this).append(' <span class="fa fa-chevron-circle-left" aria-hidden="true"></span>');}
if($(this).hasClass('right')){$(this).append(' <span class="fa fa-chevron-circle-right" aria-hidden="true"></span>');}}});$(".slide-show").each(function(){if($(this).find('.item').length==0){$(".slide-show").css('height','400px');}});var isMobile='desktop';if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4)))isMobile='mobile';if(isMobile=='mobile'){if($('.container-floor').length==0){$('li.dropdown-submenu.categories-submenu').removeClass('dropdown-submenu');}}});var facebook_login="1";var facebook_login_app_id="app id facebook";if(facebook_login==1){function statusChangeCallback(response){if(response.status==='connected'){}else if(response.status==='not_authorized'){}else{}}
function checkLoginState(){FB.getLoginStatus(function(response){statusChangeCallback(response);});}
window.fbAsyncInit=function(){FB.init({appId:facebook_login_app_id,cookie:true,xfbml:true,version:'v2.2'});FB.getLoginStatus(function(response){statusChangeCallback(response);});checkLoginState();};(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="//connect.facebook.net/en_US/sdk.js";fjs.parentNode.insertBefore(js,fjs);}(document,'script','facebook-jssdk'));function testAPI(){console.log('Welcome!  Fetching your information.... ');FB.api('/me?fields=name,email',function(response){console.log(JSON.stringify(response));});}
function login_fb(){FB.login(function(response){if(response.authResponse){FB.api('/me?fields=name,email',function(p_response){var fb_access_token='';var fb_user_id=p_response.id;var fb_profile=JSON.stringify(p_response);var fb_auth='';FB.getLoginStatus(function(st_response){fb_access_token=st_response.authResponse.accessToken;fb_auth=JSON.stringify(st_response);});ajax_login_fb(fb_access_token,fb_user_id,fb_profile,fb_auth);});}else{console.log('cancel login');}},{scope:'email'});}
function ajax_login_fb(fb_access_token,fb_user_id,fb_profile,fb_auth){var fb_url="http://kinerjamall.local/auth/vToken";var redir_url=jQuery(location).attr('href');$.ajax({type:"post",dataType:'json',url:fb_url,data:{access_token:fb_access_token,user_id:fb_user_id,profile:fb_profile,auth:fb_auth},success:function(result){if(result.error==0){location.href=redir_url;}
else{console.log(result);}},error:function(result){console.log('error ajax '+result);}});}
jQuery("body").on('click',".login-fb",function(){login_fb();});jQuery("body").on("click",".btn-logout",function(){});}}
jQuery('.search-box-btn').unbind('click').click(function(){jQuery(".search-box").slideToggle()});jQuery(".g-signin2").click(function(){g_google_login=true;});$('#type').val('product');$.cresenity._filesadded+='['+'/application/62hallfamily/default/media/css/bootstrap-3.3.5/bootstrap.css'+']'
$.cresenity._filesadded+='['+'/application/62hallfamily/default/media/css/bootstrap-3.3.5/bootstrap.min.css'+']'
$.cresenity._filesadded+='['+'/media/css/plugins/chosen/chosen.css'+']'
$.cresenity._filesadded+='['+'/application/62hallfamily/default/media/css/62hallfamily-select2.css'+']'
$.cresenity._filesadded+='['+'/application/62hallfamily/default/media/css/font-awesome.min.css'+']'
$.cresenity._filesadded+='['+'/application/62hallfamily/default/media/css/bootstrap-dropdown.css'+']'
$.cresenity._filesadded+='['+'/application/62hallfamily/default/media/css/jquery.fancybox.css'+']'
$.cresenity._filesadded+='['+'/application/62hallfamily/default/media/css/flexslider.css'+']'
$.cresenity._filesadded+='['+'/application/62hallfamily/default/media/css/jquery-touchspin/jquery.bootstrap-touchspin.css'+']'
$.cresenity._filesadded+='['+'/application/62hallfamily/default/media/css/62hallfamily-bootstrap-slider.css'+']'
$.cresenity._filesadded+='['+'/media/css/plugins/validation-engine/jquery.validationEngine.css'+']'
$.cresenity._filesadded+='['+'/application/62hallfamily/default/media/css/owl.carousel.css'+']'
$.cresenity._filesadded+='['+'/application/62hallfamily/default/media/css/62hallmall/style.css?v=1'+']'
if(typeof capp_started_event_initialized==='undefined'){capp_started_event_initialized=false;}
if(!capp_started_event_initialized){var evt=document.createEvent('Events');evt.initEvent('capp-started',false,true,window,0);capp_started_event_initialized=true;document.dispatchEvent(evt);}})})})})})})})})})})})})})})})})});    
    if (window) {
        window.onload = function() {
        }
    }


function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    var google_id_token = googleUser.getAuthResponse().id_token;
    if(g_google_login == true){
        login_google(google_id_token);
    }
}          

function login_google(google_id_token) {
    var google_url = "http://kinerjamall.local/auth/vTokenGoogle";
    var redir_url = jQuery(location).attr('href');

    $.ajax({
        type: "post",
        dataType: 'json',
        url: google_url,
        data: {
            id_token: google_id_token
        },
        success: function(result) {
            if(result.error == 0){
                location.href = redir_url;
            }
            else {
                console.log(result);
            }
        },
        error: function(result) {
            console.log('error ajax '+result);
        }
    });
}</script>

</body>
</html>
