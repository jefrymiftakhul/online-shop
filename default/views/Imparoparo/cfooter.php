<?php
global $additional_footer_js;

$store = NULL;
$domain = NULL;
$org_id = CF::org_id();
$data_org = org::get_org($org_id);
$db = CDatabase::instance();
if (!empty($data_org)) {
    $store = $data_org->name;
    $domain = $data_org->domain;
}

$cms_options = cms::get_option();

?>
</div>
<!-- end main content -->
<!-- footer -->
<footer>

    <div class=" container-options">
        <div class=" container">
            <div class=" im-nopadding col-xs-12 col-sm-6 col-md-5 col-lg-4 ">
                <p class="offer-text-top">DAPATKAN PROMO MENARIK DARI KAMI</p>
                <p class="offer-text-bottom">NO SPAMMING, BISA UNSUBSCRIBE KAPAN SAJA</p>
            </div>
            <div class=" im-nopadding col-xs-12 col-sm-6 col-md-5 col-lg-6">
                <div class="im-subscribe-group">
                    <div class="input-group">
                        <form action="/subscribe/user_subscribe" method="POST">
                            <input name="email_subscribe" class="form-control" placeholder="someone@gmail.com" type="text">
                            <!--<div class="im-subscribe-icon"></div>-->
                            <button class="btn im-subscribe-icon"></button>
                        </form>
                    </div>
                </div>

            </div>
            <div class=" im-nopadding  sosmed-group col-xs-12 col-sm-2 col-md-2 col-lg-2">
                <div class="sosmed-group-inner">
                    <p>Get Connected</p>
                    <ul class="social-media">
                        <?php if(strlen(carr::get($cms_options, 'media_fb'))>0): ?>
                        
                        <li class="facebook"><a href="<?php echo (carr::get($cms_options,'media_fb'));?>">Facebook</a></li>
                        <?php endif; ?>
                        <?php if(strlen(carr::get($cms_options, 'media_ig'))>0): ?>
                        <li class="instagram"><a href="<?php echo (carr::get($cms_options,'media_ig'));?>">Instagram</a></li>
                        <?php endif; ?>
                        <?php // if(strlen(carr::get($cms_options, 'media_ln'))>0): ?>
<!--                        <li class="line"><a href="#">Line</a></li>-->
                        <?php // endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="im-footer container">

        

        <div class="row">
            <div class="col-md-5 col-lg-5 hidden-xs hidden-sm visible-md visible-lg">
                <h5 class="widget-title"><?php echo clang::__('TENTANG PAROPARO'); ?></h5>
                <?php if(strlen(carr::get($cms_options, 'profil'))>0): ?>
                <p>
                    <?php echo carr::get($cms_options, 'profil');?>
                </p>
                <?php endif; ?>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <div class="row">
                    <div class="col-xs-6 col-sm-3 visible-xs visible-sm hidden-md hidden-lg">
                        <h5 class="widget-title"><?php echo clang::__('TENTANG PAROPARO'); ?></h5>
                        <?php if(strlen(carr::get($cms_options, 'profil'))>0): ?>
                        <p>
                            <?php echo carr::get($cms_options, 'profil');?>
                        </p>
                        <?php endif; ?>
                    </div>
                    <div class="col-xs-6 col-sm-3 col-md-4 col-lg-4">
                        <h5 class="widget-title"><?php echo clang::__('LAYANAN'); ?></h5>
                        <?php $menu_service = cms::nav_menu('menu_footer_service'); ?>
                        <ul class="list-unstyled">
                            <?php
                            if (count($menu_service) > 0) {
                                foreach ($menu_service as $menu_service_k => $menu_service_v) {
                                    echo '<li><a href="' . $menu_service_v['menu_url'] . '">' . $menu_service_v['menu_name'] . '</a></li>';
                                }
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="clearfix visible-xs-block"></div>
                    <div class="col-xs-6 col-sm-3 col-md-4 col-lg-4">
                        <h5 class="widget-title"><?php echo clang::__('PRODUK'); ?></h5>
                        <?php 
                        $footer_category = product::get_product_category_footer(); 
                        ?>
                        <ul class="list-unstyled">
                            <?php
                            if (count($footer_category) > 0) {
                                foreach ($footer_category as $category_key => $category_val) {
                                    echo '<li><a href="' . $category_val['url'] . '">' . strtoupper($category_val['name']) . '</a></li>';
                                }
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="col-xs-6 col-sm-3 col-md-4 col-lg-4">
                        <h5 class="widget-title"><?php echo clang::__('LEGAL'); ?></h5>
                        <?php $menu_about = cms::nav_menu('menu_footer_about'); ?>
                        <ul class="list-unstyled">
                            <?php
                            if (count($menu_about) > 0) {
                                foreach ($menu_about as $menu_about_k => $menu_about_v) {
                                    echo '<li><a href="' . $menu_about_v['menu_url'] . '">' . $menu_about_v['menu_name'] . '</a></li>';
                                }
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- end footer -->
<?php
//cdbg::var_dump($additional_footer_js);
?>
<script src="<?php echo curl::base(); ?>media/js/require.js"></script>

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
    $.src="https://v2.zopim.com/?4ooNKnIvfrpcICRcYqocrPB31wuIZf1x";z.t=+new Date;$.
    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->

<script type="text/javascript">
<?php
$google_analytics_id = cms_options::get('google_analytics');
//cdbg::var_dump($google_analytics_code);
$google_analytics_code = "
             (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                ga('create', '" . $google_analytics_id . "', 'auto');
                ga('send', 'pageview');
            ";
$is_google_analytics_code = preg_match("/^ua-\d{4,9}-\d{1,4}$/i", strval($google_analytics_id)) ? true : false;

if ($is_google_analytics_code) {
    $error = 0;
    try {
        CJSMin::minify($google_analytics_code);
    } catch (Exception $ex) {
        $error++;
    }
    if ($error == 0) {
        echo $google_analytics_code;
    }
}
?>
    window.register_button_executed = 0;
    window.login_button_executed = 0;
    document.addEventListener('capp-started', function (customEvent) {
<?php
echo $additional_footer_js;
?>

    });
<?php
echo $js;
echo $ready_client_script;
?>

    if (window) {
        window.onload = function () {
<?php
echo $load_client_script;
?>
        }
    }
<?php echo $custom_js ?>
</script>
<?php echo $custom_footer; ?>

</body>
</html>
<?php
if (ccfg::get("log_request")) {


    log62::request();
}
?>