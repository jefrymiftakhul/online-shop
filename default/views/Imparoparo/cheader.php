<?php
defined('SYSPATH') OR die('No direct access allowed.');

$url_base = curl::base();
$db = CDatabase::instance();

$org_id = CF::org_id();
$get_code_org = "default";
$get_code = '';
$fav_icon = '';
$item_image = '';
$org_name = '';
if ($org_id) {
    $get_code = cdbutils::get_row("SELECT * FROM org WHERE org_id = " . $db->escape($org_id));
    $get_code_org = $get_code->code;
    $fav_icon = $get_code->item_favicon;
    $item_image = $get_code->item_image;
    $org_name = $get_code->name;
}

$keyword = carr::get($_GET, 'keyword');
$category = carr::get($_GET, 'category');
$total_cart = product::get_total_cart();

$controller = CF::instance();
$page = 'product';
if (method_exists($controller, 'page')) {
    $page = $controller->page();
}
if (carr::get($_GET, 'page') != null && CFRouter::$controller == 'search') {
    $page = carr::get($_GET, 'page');
}

if ($page != 'product') {
    $url_base = curl::base() . $page . '/home/';
}
$body_class='';
if(CFRouter::$controller=='product'&&CFRouter::$method=='item') {
    $body_class=' page-body-single-product';
}
$data_org = org::get_org($org_id);
$store = '';
$have_product = 0;
$have_gold = 0;
$have_service = 0;
$have_register = 0;
$have_deposit = 0;

if (!empty($data_org)) {
    $store = $data_org->name;
    $have_product = $data_org->have_product;
    $have_gold = $data_org->have_gold;
    $have_service = $data_org->have_service;
    $have_register = $data_org->have_register;
    $have_deposit = $data_org->have_deposit;
}
$url_site = cms::get_option('site_url', 'single', $org_id);

$page_type = "produk";
if ($page == "service") {
    $page_type = "jasa";
}
$url_help = curl::base() . 'read/page/index/beli-' . $page_type . '-di-' . cstr::sanitize($org_name);

global $additional_footer_js;
?>

<!DOCTYPE html>
<html lang="id">
    <head>
        <?php echo $additional_head; ?>
        <title><?php echo $store; ?></title>
        <meta charset="UTF-8">
        <?php
        $login_from_google = ccfg::get("login_google");
        if ($login_from_google):
            ?>
            <meta name="google-signin-client_id" content="<?php echo ccfg::get('login_google_app_id'); ?>">
            <?php
        endif;
        ?>

        <link rel="icon" type="image/png" href="<?= curl::base() . 'application/adminittronmall/' . $get_code_org . '/upload/logo/item_favicon/' . $fav_icon ?>">
        <!--<link href="//db.onlinewebfonts.com/c/9b7350af1bd31d8cd491e595ed49aeb2?family=NEOTERIC" rel="stylesheet" type="text/css"/>-->
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
        <?php echo $head_client_script; ?>
        <script type="text/javascript">
            var g_google_login = false;
        </script>
        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
         fbq('init', '1864445583833015'); 
        fbq('track', 'PageView');
        </script>
        <noscript>
         <img height="1" width="1" 
        src="https://www.facebook.com/tr?id=1864445583833015&ev=PageView
        &noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->
        
        <?php
            $css_zen_desk_head = "<!-- Custom CSS Zendesk Livechat -->
        <style>";
            $css_zen_desk_content = "@media (max-width: 480px) {
                .zopim{
                    right:auto !important;
                    left:16px !important;
                    bottom:16px !important;
                }
            }";
            if(CFRouter::$controller=='product'&&CFRouter::$method=='item') {
                $css_zen_desk_content = "@media (max-width: 480px) {
                    .zopim{
                        right:auto !important;
                        left:16px !important;
                        bottom:61px !important;
                    }
                }";
            }
            $css_zen_desk_foot = "</style>
        <!-- End Custom CSS Zendesk Livechat -->";
        echo $css_zen_desk_head;
        echo $css_zen_desk_content;
        echo $css_zen_desk_foot;
        ?>
        
    </head>
    <?php $theme_color = ''; ?>
    <body class="<?php echo $theme_color; ?> <?php echo $body_class; ?>">
        <!-- header -->
        <header>
            <div class="container">
                <div id="head-top" class="row">
                    <div class="col-xs-5 col-sm-3 col-md-2 col-lg-2">
                        <div class="main-logo">
                            <?php $logo = curl::base() . 'application/adminittronmall/' . $get_code_org . '/upload/logo/item_image/' . $item_image; ?>
                            <a href="<?= curl::httpbase() ?>" class="brand-logo" title="Home">    
                                <img  src="<?= $logo ?>" alt="<?= $store ?>" class="img-responsive"/>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-5 hidden-xs visible-sm visible-md visible-lg">
                        <div class="mainbar desktop">
                            <ul class="main-navigation">
                                <li><a target="_blank" href="<?= curl::httpbase() . 'read/page/index/cara-pemesanan' ?>">CARA PEMESANAN</a></li>
                                <li><a target="_blank" href="<?= curl::httpbase() . 'read/page/index/hubungi-kami' ?>">HUBUNGI KAMI</a></li>
                                <li><a target="_blank" href="<?= curl::httpbase() . 'testimonial' ?>">TESTIMONIAL</a></li>
                                <li><a href="<?= curl::httpbase() . 'read/page/category/artikel' ?>">BLOG</a></li>
                            </ul>
                            <form class="main-form" action="/search">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="keyword" id="keyword" value="<?php echo chtml::specialchars($keyword); ?>" placeholder="CARI NAMA PRODUK LALU KETIK ENTER">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn">Cari</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-xs-7 col-sm-3 col-md-6 col-lg-5">
                        <div class="im-navbar-right">
                            <div class="action-list-item">
                                <a href="<?= curl::httpbase() . 'check_order' ?>">
                                    <div class="im-nav-right-group">
                                        <!--<div class="im-nav-right im-notification"></div>-->
                                        <img src="<?= curl::base() . 'application/ittronmall/default/media/img/Imparoparo/sprite-19.png'?>" class="im-nav-right im-notification">
                                        <span class="hidden-xs hidden-sm">CEK ORDER</span>
                                    </div>
                                </a>
                            </div>
                            <div class="action-list-item">
                                <a href="<?= curl::httpbase() . 'products/shoppingcart/' ?>">
                                    <div class="im-nav-right-group">
                                        <!--<div class="im-nav-right im-mycart"></div>-->
                                        <img src="<?= curl::base() . 'application/ittronmall/default/media/img/Imparoparo/sprite-20.png'?>" class="im-nav-right im-mycart">
                                        <span class="hidden-xs hidden-sm">MY CART</span>
                                        <span id="total_cart" class="total_cart"><?php echo $total_cart; ?></span>
                                    </div>
                                </a>
                            </div>
                            <?php
                            $additional_footer_js = "";
                            $element_register = CElement_Imparoparo_Register::factory('register');
                            $element_register->set_trigger_button(true);
                            $element_register->set_icon(FALSE);
                            $element_register->set_store($store);
                            $element_register->set_have_gold($have_gold);
                            $element_register->set_have_service($have_service);

                            $element_login = CElement_Imparoparo_Login::factory('login');
                            $element_login->set_trigger_button(TRUE);
                            $element_login->set_icon(FALSE);

                            if (member::get_data_member() == false) {
                                if ($have_register > 0) {
                                    $element_login->set_register_button(true);
                                    $element_login->set_element_register($element_register);
                                }
                            }
                            
                            if (member::get_data_member() === false) {
                                echo $element_register->html();
                                $additional_footer_js .= $element_register->js();
                            }

                            echo $element_login->html();
                            $additional_footer_js .= $element_login->js();
                            ?>
                        </div>
                    </div>
                </div>
                <div id="head-top-full" class="row visible-xs hidden-sm hidden-md hidden-lg">
                    <div class="col-lg-12">
                        <div class="mainbar">
                            <ul class="main-navigation">
                                <li><a target="_blank" href="<?= curl::httpbase() . 'read/page/index/cara-pemesanan' ?>">CARA PEMESANAN</a></li>
                                <li><a target="_blank" href="<?= curl::httpbase() . 'read/page/index/hubungi-kami' ?>">HUBUNGI KAMI</a></li>
                                <li><a target="_blank" href="<?= curl::httpbase() . 'testimonial' ?>">TESTIMONIAL</a></li>
                                <li><a href="<?= curl::httpbase() . 'read/page/category/artikel' ?>">BLOG</a></li>
                            </ul>
                            <div class="main-form-area">
                                <?php  if(count(crouter::segments()) > 0) { ?>
                                <div class="push-back" onclick="javascript:window.history.back();">Back</div>
                                <?php } ?>
                                <form class="main-form" action="/search">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="keyword" id="keyword" value="<?php echo chtml::specialchars($keyword); ?>" placeholder="CARI NAMA PRODUK LALU KETIK ENTER">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn">Cari</button>
                                        </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- end header -->

        <!-- main content -->
        <div id="main-content">
