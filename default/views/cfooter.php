<?php
global $additional_footer_js;

$store = NULL;
$domain = NULL;
$org_id = CF::org_id();
$data_org = org::get_org($org_id);
if(ccfg::get('have_same_cms_first_parent')>0){
	$org=org::get_first_parent_org($org_id);
	if($org!=null){
		$org_id=$org->org_id;
	}
}

$db=CDatabase::instance();
if(!empty($data_org)){
    $store = $data_org->name;
    $domain = $data_org->domain;
}

$arr_option=array();
$q="
	select
		*
	from
		cms_options
	where
		org_id=".$db->escape($org_id)."
";
$r=$db->query($q);
if($r->count()>0){
	foreach($r as $row){
		$arr_option[$row->option_name]=$row->option_value;
	}
}

?>
<?php
    if(ccfg::get('no_web_front')==0):
?>

<footer>
    <div class="bg-gray font-white">
        <div class="container">
            <div class="row margin-30">
                <div class="col-md-3 font-gray">
                    <h4 class="font-black">NEWSLETTER</h4>
                    <ul>
                        <li class="small col-sm-5 col-md-12 padding-left-right0"><?php echo clang::__("Register and get attractive deals from ").$store;?></li>
                        <li class="col-sm-7 col-md-12 padding-left-right0">
                            <?php
                            $element_subscribe = CElement_Product_Subscribe::factory();
                            echo $element_subscribe->html();
                            $additional_footer_js .= $element_subscribe->js();
                            ?>
                        </li>
                        <li>
                            <div>
                                <div class="icon-payment-type margin-top-10" style="display: inline-block"></div>
                                <!--<div class="icon-mastercard margin-top-10" style="display: inline-block"></div>-->
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-4 font-gray">
                    <h4 class="font-black">LAYANAN PELANGGAN</h4>
                    <!--    Kami -->
                    <?php 
                    
                    $menu_service = cms::nav_menu('menu_footer_service');
                   
                    ?>
                    <ul class="list-unstyled">
                        <?php
                        if (count($menu_service) > 0) {
                            foreach ($menu_service as $menu_service_k => $menu_service_v) {
                                echo '<li><a href="'.$menu_service_v['menu_url'].'">'.$menu_service_v['menu_name'].'</a></li>';
                            }
                        }
                        ?>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-4 font-gray">
                    <h4 class="font-black"><?= $store ?></h4>
                    <?php 
                    
                    $menu_about = cms::nav_menu('menu_footer_about');
                   
                    ?>
                    <ul class="list-unstyled">
                        <?php
                        if (count($menu_about) > 0) {
                            foreach ($menu_about as $menu_about_k => $menu_about_v) {
                                echo '<li><a href="'.$menu_about_v['menu_url'].'">'.$menu_about_v['menu_name'].'</a></li>';
                            }
                        }
                        ?>
                    </ul>
                </div>
                <div class="footer-contact-us-container col-md-3 col-sm-4 font-gray">
                    <h4 class="font-black">HUBUNGI KAMI</h4>
                    <ul class="list-unstyled">
                    <?php
					$contact_us='';
					$phone='';
					$phone_mobile='';
					if(carr::get($arr_option,'contact_us')){
						$contact_us=carr::get($arr_option,'contact_us');
						echo '<li>'.$contact_us.'</li>';
					}
					if(carr::get($arr_option,'contact_us_phone_1')){
						$phone=carr::get($arr_option,'contact_us_phone_1');
						echo '<li>Telp : '.$phone.'</li>';
					}
					if(carr::get($arr_option,'contact_us_mobile_1')){
						$phone_mobile=carr::get($arr_option,'contact_us_mobile_1');
						echo '<li>&nbsp &nbsp &nbsp &nbsp &nbsp '.$phone_mobile.'</li>';
					}
                    ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright bg-red font-white">
        <center>
            <?php
            //~~
			$footer='';
			if(carr::get($arr_option,'footer')){
				$footer=carr::get($arr_option,'footer');
			}
            ?>
            <!-- Copyright -->
           <?php echo $footer; ?>
        </center>
    </div>
</footer>
<?php
    endif;
?>

</div> <!-- .page-container-full -->
<?php
    //cdbg::var_dump($additional_footer_js);
?>
<script src="<?php echo curl::base(); ?>media/js/require.js"></script>

<script language="javascript">
    
    <?php
        $google_analytics_id = cms_options::get('google_analytics');
        //cdbg::var_dump($google_analytics_code);
        $google_analytics_code="
             (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                ga('create', '".$google_analytics_id."', 'auto');
                ga('send', 'pageview');
            ";
        $is_google_analytics_code = preg_match("/^ua-\d{4,9}-\d{1,4}$/i", strval($google_analytics_id)) ? true : false;
       
        if($is_google_analytics_code) {
            $error=0;
            try {
                CJSMin::minify($google_analytics_code);
            } catch (Exception $ex) {
                $error++;
            }
            if($error==0) {
                echo $google_analytics_code;
            }
        }

    ?>
    
document.addEventListener('capp-started', function(customEvent) {
    <?php
    echo $additional_footer_js; 
    ?>
            
});	
<?php
echo $js;
echo $ready_client_script;
?>
    
    if (window) {
        window.onload = function() {
<?php 
    echo $load_client_script;
?>
        }
    }
<?php echo $custom_js ?>
</script>
<?php echo $custom_footer; ?>

</body>
</html>
<?php

if (ccfg::get("log_request")) {


    log62::request();
}
?>