<?php
global $additional_footer_js;

$store = NULL;
$domain = NULL;
$org_id = CF::org_id();
$data_org = org::get_org($org_id);
$db = CDatabase::instance();
if (!empty($data_org)) {
    $store = $data_org->name;
    $domain = $data_org->domain;
}
$arr_option = array();
$q = "
	select
		*
	from
		cms_options
	where
		org_id=" . $db->escape($org_id) . "
";
$r = $db->query($q);
if ($r->count() > 0) {
    foreach ($r as $row) {
        $arr_option[$row->option_name] = $row->option_value;
    }
}
?>
</div>
<!-- end main content -->
<!-- footer -->

<footer>
    <div class="footer-newslatter">
        <div class="icon-subscribe"></div>
        <div class="container">
            <div class="row newsletter">
                <div class="newsletter-wrap">
                    <div class="newsletter-title">
                        STAY IN TOUCH WITH US
                    </div>
                    <div class="newsletter-description">
                        Sign up for our newsletter to stay up to date with the latest news, special offers and other stuff.
                    </div>
                    <form action="/subscribe/user_subscribe" method="POST">
                        <input name="email_subscribe" type="text" class="form-control im-newsletter" placeholder="Enter Your Email">
                        <button class="btn btn-newsletter"><?php echo clang::__('SUBSCRIBE'); ?></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container">
            <div class="menu-footer">
                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                    <h3 class="widget-title"><?php echo strtoupper(clang::__('LAYANAN PELANGGAN')); ?></h3>
                    <!--<h4 class="font-black">LAYANAN PELANGGAN</h4>-->
                    <?php
                    $menu_service = cms::nav_menu('menu_footer_service');
                    ?>
                    <ul class="list-unstyled">
                        <?php
                        if (count($menu_service) > 0) {
                            foreach ($menu_service as $menu_service_k => $menu_service_v) {
                                echo '<li><a href="' . $menu_service_v['menu_url'] . '">' . $menu_service_v['menu_name'] . '</a></li>';
                            }
                        }
                        ?>
                    </ul>
                </div>
                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                    <h3 class="widget-title"><?php echo strtoupper($store); ?></h3>
                    <?php $menu_about = cms::nav_menu('menu_footer_about'); ?>
                    <ul class="list-unstyled">
                        <?php
                        if (count($menu_about) > 0) {
                            foreach ($menu_about as $menu_about_k => $menu_about_v) {
                                echo '<li><a href="' . $menu_about_v['menu_url'] . '">' . $menu_about_v['menu_name'] . '</a></li>';
                            }
                        }
                        ?>
                    </ul>
                </div>            
                <div class="clearfix visible-xs-block"></div>
                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                    <h3 class="widget-title"><?php echo strtoupper(clang::__('Follow Us')); ?></h3>
                    <ul class="list-unstyled">
                        <?php
                        $media = cms_options::get('media_', CF::org_id(), 'all');
                        if ($media != NULL) {
                            foreach ($media as $media_k => $media_v) {
                                $option_name = carr::get($media_v, 'option_name');
                                $option_value = carr::get($media_v, 'option_value');

                                if ($media_k == 0) {
                                    $sosmed = "Facebook";
                                } else if ($media_k == 1) {
                                    $sosmed = "Twitter";
                                } else if ($media_k == 2) {
                                    $sosmed = "Instagram";
                                } else if ($media_k == 3) {
                                    $sosmed = "Google";
                                }

                                if ($option_value != NULL) {
                                    echo '<li class="display-table ' . $option_name . '">
                                            <a href="' . $option_value . '" target="_blank">
                                                <div class="table-cell">
                                                    <div class="ico-' . $option_name . '"></div>
                                                </div>
                                                <div class="table-cell">' . $sosmed . '</div>
                                            </a>
                                        </li>';
                                }
                            }
                        }
                        ?>
                    </ul>
                </div>
                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                    <h3 class="widget-title"><?php echo strtoupper(clang::__('Contact Us')); ?></h3>
                    <ul class="list-unstyled">
                        <?php
                        $contact_us = '';
                        $phone = '';
                        $phone_mobile = '';
                        $bbm = '';
                        $email = '';

                        if (carr::get($arr_option, 'contact_us')) {
                            $contact_us = carr::get($arr_option, 'contact_us');
                            echo '<li class="display-table">
                            <div class="table-cell">
                                <div class="ico-home"></div>
                            </div>
                            <div class="table-cell">' . $contact_us . '</div>
                        </li>';
                        }
                        if (carr::get($arr_option, 'contact_us_phone_1')) {
                            $phone = carr::get($arr_option, 'contact_us_phone_1');
                            $raw_phone = str_replace(array(' ', '-'), '', $phone);
                            echo '<li class="display-table phone-info">
                            <div class="table-cell">
                                <div class="ico-phone"></div>
                            </div>
                            <div class="table-cell"><a href="tel:' . $raw_phone . '">' . $phone . '</a>';
                        }
//                if(carr::get($arr_option,'contact_us_mobile_1')){
//                    $phone_mobile=carr::get($arr_option,'contact_us_mobile_1');
//                    $raw_phone_mobile = str_replace(array(' ','-'), '', $phone_mobile);
//                    
//                    $br='<br>';
//                    $icon_phone = '';
//                    if(strlen(trim(carr::get($arr_option,'contact_us_phone_1'))) == 0){
//                        $icon_phone = '
//                            <li class="display-table">
//                            <div class="table-cell">
//                                <div class="ico-phone"></div>
//                            </div>
//                            <div class="table-cell">
//                            ';
//                        
//                        $br='';
//                    }
//                    echo '
//                        '.$icon_phone.'
//                        '.$br.'
//                        <a href="tel:'.$raw_phone_mobile.'">'.$phone_mobile."</a></div></li>";
//                }
                        if (carr::get($arr_option, 'contact_us_mobile_1')) {
                            $phone_mobile = carr::get($arr_option, 'contact_us_mobile_1');
                            $raw_phone_mobile = str_replace(array(' ', '-'), '', $phone_mobile);
                            echo '<li class="display-table phone-info">
                            <div class="table-cell">
                                <div class="ico-wa"></div>
                            </div>
                            <div class="table-cell">
                                <a href="tel:' . $raw_phone_mobile . '">' . $phone_mobile . '</a>
                            </div>
                        </li>';
                        }
                        if (carr::get($arr_option, 'contact_us_bbm_1')) {
                            $bbm = carr::get($arr_option, 'contact_us_bbm_1');
                            echo '<li class="display-table">
                            <div class="table-cell">
                                <div class="ico-bbm"></div>
                            </div>
                            <div class="table-cell">' . $bbm . '</div>
                        </li>';
                        }
                        if (carr::get($arr_option, 'contact_us_mail_1')) {
                            $email = carr::get($arr_option, 'contact_us_mail_1');
                            echo '<li class="display-table">
                            <div class="table-cell">
                                <div class="ico-email"></div>
                            </div>
                            <div class="table-cell">' . $email . '</div>
                        </li>';
                        }
                        ?>
                    </ul>
                </div>           
            </div>
            
            <div class="copyright">
                <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 40px"><div style="border-top: 1px solid #d0d1d3;">
                        <p style="margin-top: 40px">Copyright © <?php echo $store; ?> 2017 All Right Reserved. Privacy Policy<?php
                            $footer_cp = carr::get($arr_option, 'footer');
                            echo $footer_cp;
                            ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- end footer -->
<?php
//cdbg::var_dump($additional_footer_js);
?>
<script src="<?php echo curl::base(); ?>media/js/require.js"></script>

<script type="text/javascript">
<?php
$google_analytics_id = cms_options::get('google_analytics');
//cdbg::var_dump($google_analytics_code);
$google_analytics_code = "
             (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                ga('create', '" . $google_analytics_id . "', 'auto');
                ga('send', 'pageview');
            ";
$is_google_analytics_code = preg_match("/^ua-\d{4,9}-\d{1,4}$/i", strval($google_analytics_id)) ? true : false;

if ($is_google_analytics_code) {
    $error = 0;
    try {
        CJSMin::minify($google_analytics_code);
    } catch (Exception $ex) {
        $error++;
    }
    if ($error == 0) {
        echo $google_analytics_code;
    }
}
?>

    document.addEventListener('capp-started', function(customEvent) {
<?php
echo $additional_footer_js;
?>

    });
<?php
echo $js;
echo $ready_client_script;
?>

    if (window) {
        window.onload = function() {
<?php
echo $load_client_script;
?>
        }
    }
<?php echo $custom_js ?>
</script>
<?php echo $custom_footer; ?>

</body>
</html>
<?php
if (ccfg::get("log_request")) {


    log62::request();
}