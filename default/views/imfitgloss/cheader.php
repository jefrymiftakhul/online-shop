<?php
defined('SYSPATH') OR die('No direct access allowed.');

/**
 * @author: ITRodex5
 * @since:   2016-11-15 11:59:24
 * @last modified by:   ITRodex5
 * @last modified time: 2016-11-15 11:52:10
 * @filename: cheader.php
 * @filepath: C:\xampp\htdocs_pipo\application\ittronmall\default\views\fitgloss\cheader.php
 */
$url_base = curl::base();
$db = CDatabase::instance();
//~~
$org_id = CF::org_id();
$get_code_org = "default";
$get_code = '';
$fav_icon = '';
$item_image = '';
$org_name = '';
if ($org_id) {
    $get_code = cdbutils::get_row("SELECT * FROM org WHERE org_id = " . $db->escape($org_id));
    $get_code_org = $get_code->code;
    $fav_icon = $get_code->item_favicon;
    $item_image = $get_code->item_image;
    $org_name = $get_code->name;
}

$keyword = carr::get($_GET, 'keyword');
$category = carr::get($_GET, 'category');


$controller = CF::instance();
$page = 'product';
if (method_exists($controller, 'page')) {
    $page = $controller->page();
}
if (carr::get($_GET, 'page') != null && CFRouter::$controller == 'search') {
    $page = carr::get($_GET, 'page');
}

if ($page != 'product') {
    $url_base = curl::base() . $page . '/home/';
}


$data_org = org::get_org($org_id);
$store = '';
$have_product = 0;
$have_gold = 0;
$have_service = 0;
$have_register = 0;
$have_deposit = 0;

if (!empty($data_org)) {
    $store = $data_org->name;
    $have_product = $data_org->have_product;
    $have_gold = $data_org->have_gold;
    $have_service = $data_org->have_service;
    $have_register = $data_org->have_register;
    $have_deposit = $data_org->have_deposit;
}
$url_site = cms::get_option('site_url', 'single', $org_id);

if (CFRouter::$controller == 'home') {
    $change = 'shadow';
} else {
    $change = '';
}

$page_type = "produk";
if ($page == "service") {
    $page_type = "jasa";
}
//$url_help = curl::base() . 'read/page/index/beli-' . $page_type . '-di-' . cstr::sanitize($org_name);
$url_help = curl::base() . 'read/page/index/belanja-di-' . cstr::sanitize($org_name);

global $additional_footer_js;
?>
<!DOCTYPE html>
<html lang="id">
    <head>
        <?php echo $additional_head; ?>
        <title><?php echo $store; ?></title>
        <meta charset="UTF-8">
        <?php
        $login_from_google = ccfg::get("login_google");
        if ($login_from_google) {
            ?>
            <meta name="google-signin-client_id" content="<?php echo ccfg::get('login_google_app_id'); ?>">
        <?php } ?>
        <link rel="icon" type="image/png" href="<?= curl::base() . 'application/adminittronmall/' . $get_code_org . '/upload/logo/item_favicon/' . $fav_icon ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
        <?php echo $head_client_script; ?>
        <script type="text/javascript">
            var g_google_login = false;
        </script>

    </head>
    <?php $theme_color = ''; ?>
    <body class="<?php echo $theme_color; ?>">
        <header>
            <div class="main-menu-wrap  <?php echo $change; ?>">
                <div class="container">
                    <!--<div class="navbar-collapse collapse">-->
                    <!--<div class="navbar-nav">-->
                    <ul class="nav navbar-nav">
                        <li>
                            <div class="im-logo col-sm-2">
                                <?php
                                $logo = curl::base() . 'application/adminittronmall/' . $get_code_org . '/upload/logo/item_image/' . $item_image;
//                                $logo = curl::base() . 'application/ittronmall/default/media/img/fitgloss/logo.jpg';
                                ?>
                                <a href="<?php echo curl::base(); ?>" class="brand-logo " title="Home">
                                    <img src="<?php echo $logo; ?>" alt="<?php echo $store; ?>"/>
                                </a>
                            </div>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-center">
                        <li class="active">
                            <a href="<?php echo curl::base(); ?>">
                                <div class="menu-list-wrap">
                                    <div class="menu-list-text">Home</div>
                                </div>
                            </a>
                        </li>
                        <li class="menu-category">
                            <?php
                            $product_category = product_category::get_product_category_menu('product', '', $org_id);
                            $menu_list = array(
                                'name' => 'Kategori',
                                'menu' => $product_category
                            );

                            $menu = CElement_Menu::factory();
                            $menu->set_menu($menu_list);
                            $menu->set_trigger_type('click');
                            echo $menu->html();
                            $additional_footer_js .= $menu->js();
                            ?>
                        </li>
                        <li>
                            <!--<a href="<?php echo curl::httpbase() ?>read/page/index/belanja-di-lasvegas">-->
                            <a href="<?php echo $url_help; ?>">
                                <div class="menu-list-wrap">
                                    <div class="menu-list-text">Bantuan</div>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <div class="menu-list-wrap">
                                    <div class="menu-list-texts">Cek Pemesanan</div>
                                </div>
                            </a>
                            <div class="panel dropdown-menu dropdown-transaction-status" role="menu">
                                <div class="panel-heading"><?php echo clang::__('Check Order Status'); ?></div>
                                <div class="panel-body">
                                    <form action="<?php echo curl::base(); ?>retrieve/invoice" id="form-status-pesanan" name="form-status-pesanan">
                                        <div class="form-group">
                                            <label for="nomor-pesanan"><?php echo clang::__('Order Number'); ?></label>
                                            <input type="text" id="nomor-pesanan" name="nomor-pesanan" class="form-control im-field"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="email"><?php echo clang::__('Email'); ?></label>
                                            <input type="text" id="email" name="email" class="form-control im-field"/>
                                        </div>
                                        <button type="submit" class="btn btn-imlasvegas btn-view-status-transaction"><?php echo clang::__('View'); ?></button>
                                    </form>
                                </div>
                            </div>
                        </li>   
                        <?php if (member::get_data_member() == false) : ?>
                            <!--                                -->
<!--                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <div class="menu-list-wrap">
                                        <div class="menu-list-text">
                                            <div class="icon-user"></div>
                                        </div>
                                    </div>
                                </a>
                                <div class="dropdown-menu">
                                    <li>
                                        <a id="1482480465585cdb51835eb168102309" href="javascript:;" class="btn   btn-modal-register-register btn-register " data-target="modal-body-register" data-modal="register_form" data-title="REGISTER">
                                            <div class="menu-list-wrap">
                                                <div class="menu-list-text">Daftar</div>
                                            </div>
                                        </a>
                                    </li>                     
                                    <li>
                                        <a href="javascript:;" class="btn btn-modal btn-login" data-target="modal-body-login" data-modal="login_form" data-title="LOGIN">
                                            <div class="menu-list-wrap">
                                                <div class="menu-list-text">Masuk</div>
                                                <div class="login-register-wrapper">
                                                    <div class="ico-user visible-xs"></div>
                                                </div>
                                            </div>                               
                                        </a>
                                    </li>
                                </div>
                            </li>-->
                        <?php else: ?>
<!--
                            <li class="">
                                <a href="<?php echo curl::httpbase() ?>/member/account">
                                    <div class="menu-list-wrap">
                                        <div class="menu-list-text">My Account</div>
                                    </div>
                                </a>
                            </li>   
                            <li>
                                <a href="<?php echo curl::httpbase() ?>/member/account/logout">
                                    <div class="menu-list-wrap">
                                        <div class="menu-list-text">Keluar</div>
                                    </div>
                                </a>
                            </li>   
-->
                        <?php
                        endif;

                        $product_category = product_category::get_product_category_menu('product', '', CF::org_id());

                        $arr_category = array();
                        foreach ($product_category as $key => $value) {
                            //$arr_category[carr::get($value, 'code')] = carr::get($value, 'name');
                            $arr_category[carr::get($value, 'url_key')] = carr::get($value, 'name');
                        }
                        ?>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <form id="demo-2" action="/search" class="search form">
                                <div class="form-group">
                                    <div class="search-container">
                                        <div class="search-container-inner">
                                            <input class="form-control search-form" type="search" id="keyword" name="keyword" placeholder="Search" value="<?php echo chtml::specialchars($keyword); ?>">                                                                                
                                            <a href="javascript:;" class="ico-search btn"></a>
                                            <a href="javascript:;" class="ico-search-close"></a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </li>
                        
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <div class="menu-list-wrap">
                                    <div class="menu-list-text">
                                        <div class="icon-user"></div>
                                    </div>
                                </div>
                            </a>
                            <div class="dropdown-menu categories padding-0">
                                <ul class="navbar-ul">
                                    <li class="logins">
                                        <?php
                                        $element_register = CElement_IttronmallFitgloss_Register::factory('register');
                                        $element_register->set_trigger_button(true);
                                        $element_register->set_icon(FALSE);
                                        $element_register->set_store($store);
                                        $element_register->set_have_gold($have_gold);
                                        $element_register->set_have_service($have_service);

                                        $element_login = CElement_IttronmallFitgloss_Login::factory('login');
                                        $element_login->set_trigger_button(TRUE);
                                        $element_login->set_icon(FALSE);

                                        if (member::get_data_member() == false) {
                                            if ($have_register > 0) {
                                                $element_login->set_register_button(true);
                                                $element_login->set_element_register($element_register);
                                            }
                                        }

                                        echo $element_login->html();
                                        $additional_footer_js .= $element_login->js();
                                        
                                        ?>                                    
                                    </li>
                                     <li>
                            
                            <div class="shoping-cart-wrapper">
                                <div id="shopping-cart" class="shopping-cart 62hall-dropdown dropdown-toggle">

                                </div>
                            </div>
                        </li>
                                </ul>
                            </div>
                        </li>
                       
                    </ul>

                </div>
            </div>
        </header>
        <!-- main content -->
        <div id="main-content">
