<?php
defined('SYSPATH') OR die('No direct access allowed.');
$db = CDatabase::instance();
$cookie_name = "cookie_popup";
$show_popup = 0;
$popup_content = '';
$org_id = CF::org_id();
$url = '';
$merchant_category = '';

if (!isset($_COOKIE[$cookie_name])) {
    setcookie($cookie_name, 'popup_show', time() + (3600 * 4), "/"); // 3600 = 1 hour
    $q = "SELECT * FROM cms_popup where status>0 and is_active = 1 and org_id=" . $db->escape($org_id) . " ORDER BY RAND() LIMIT 1";
    $result = cdbutils::get_row($q);
    if ($result !== null) {
//        $arr=array(
//            'url_image'=>$result->image_url,
//        );
        //$json_additional_param=json_encode($arr);        
        $url = cobj::get($result, 'url');
        $show_popup = 1;
        $image_url = $result->image_url;
        $image_url = str_replace("http://", "//", $image_url);
        $popup_content = '<div id="div-popup" style="top:-20px; background-image: url(\'' . $image_url . '\'); background-repeat:no-repeat;" class="modal-content-background"></div>';
    }
}
//$url_image = $result->image_url;

$url_base = curl::base();
//~~
$get_code_org = "default";
$get_code = '';
$fav_icon = '';
$item_image = '';
$org_name = '';
if ($org_id) {
    $get_code = cdbutils::get_row("SELECT * FROM org WHERE org_id = " . $db->escape($org_id));
    if (ccfg::get('have_same_cms_first_parent') > 0) {
        $get_code = org::get_first_parent_org($org_id);
    }
    $get_code_org = $get_code->code;
    $fav_icon = $get_code->item_favicon;
    $item_image = $get_code->item_image;
    $org_name = $get_code->name;
}

$keyword = carr::get($_GET, 'keyword');

$controller = CF::instance();
$page = 'product';
if (method_exists($controller, 'page')) {
    $page = $controller->page();
}
if (carr::get($_GET, 'page') != null && (CFRouter::$controller == 'search' || CFRouter::$controller == 'request')) {
    $page = carr::get($_GET, 'page');
}

$org_share_code = null;
if (CFRouter::$controller == 'product_share') {
    // get last slashed url
    $category = explode('/', CFRouter::$routed_uri);
    $org_share_code = $category[count($category) - 1];
}
if (CFRouter::$controller == 'search' && isset($_GET['org_share_code'])) {
    $org_share_code = $_GET['org_share_code'];
}

if ($page != 'product') {
    $url_base = curl::base() . $page . '/home/';
}

$data_org = org::get_org($org_id);
$store = '';
$have_product = 0;
$have_gold = 0;
$have_service = 0;
$have_register = 0;
$have_deposit = 0;
$have_mpu = 0;
$name_mpu = 'MPU';

if (!empty($data_org)) {
    $store = $data_org->name;
    $have_product = $data_org->have_product;
    $have_gold = $data_org->have_gold;
    $have_service = $data_org->have_service;
    $have_register = $data_org->have_register;
    $have_deposit = $data_org->have_deposit;
    $have_mpu = $data_org->have_mpu;
    $name_mpu = $data_org->name_mpu;
    $name_mpu = ucwords(strtolower($name_mpu));
}
// prevent column name_mpu empty
if (strlen($name_mpu) == 0 || $name_mpu == null) {
    $name_mpu = 'MPU';
}
//cdbg::var_dump($name_mpu);
$url_site = cms::get_option('site_url', 'single', $org_id);

$page_type = "produk";
if ($page == "service") {
    $page_type = "jasa";
}
$url_help = curl::base() . 'read/page/index/beli-' . $page_type . '-di-' . cstr::sanitize($org_name);

global $additional_footer_js;
?>
<!DOCTYPE html>
<html style="background: white;">
    <head>
        <?php echo $additional_head; ?>
        <title><?php echo $store; ?></title>
        <meta charset="UTF-8">
        <?php
        $login_from_google = ccfg::get("login_google");
        if ($login_from_google) {
            ?>
            <meta name="google-signin-client_id" content="<?php echo ccfg::get('login_google_app_id'); ?>">
        <?php } ?>
        <link rel="icon" type="image/png" href="<?= curl::base() . 'application/adminittronmall/' . $get_code_org . '/upload/logo/item_favicon/' . $fav_icon ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
        <?php echo $head_client_script; ?>
        <script type="text/javascript">
            var g_google_login = false;
<?php if ($show_popup == 1) { ?>
                document.addEventListener('capp-started', function(customEvent) {
                    $.app.show_dialog('promopopup', '  ', '<?php echo addslashes($popup_content); ?>', 'popup-promo');
    <?php if (strlen($url) > 0) { ?>
                        $('#div-popup').click(function() {
                            window.open('<?php echo $url; ?>');
                        });
    <?php } ?>
                });
<?php } ?>
        </script>

        <?php
        $shopping_cart_pro = "style='display:block !important;'";
        ?>

    </head>
    <body>        

        <div class="page-container-full">
            <div class="page-header-full">
                <header >
                    <div class="bg-black">
                        <div class="container font-white">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="inline-block header-social-media">
                                        <?php
                                        $facebook = cms_options::get('media_fb');
                                        $twitter = cms_options::get('media_tw');
                                        $instagram = cms_options::get('media_ig');
                                        $google_plus = cms_options::get('media_gp');
                                        ?>

                                        <?php if ($facebook) { ?>
                                            <a href="<?= $facebook ?>"><i class="fa fa-facebook sosial-media"></i></a>
                                        <?php } ?>
                                        <?php if ($twitter) { ?>    
                                            <a href="<?= $twitter ?>"><i class="fa fa-twitter sosial-media"></i></a>
                                        <?php } ?>
                                        <?php if ($instagram) { ?>
                                            <a href="<?= $instagram ?>"><i class="fa fa-instagram sosial-media"></i></a>
                                        <?php } ?>
                                        <?php if ($google_plus) { ?>
                                            <a href="<?= $google_plus ?>"><i class="fa fa-google-plus sosial-media"></i></a>
                                        <?php } ?>
                                    </div>
                                    <div class="header-menu-mobile visible-sm visible-xs pull-right">
                                        <div class="navbar-collapse navbar-right" style="clear:both">
                                            <ul class="nav navbar-nav navbar-top-menu margin-top-bottom-0-i">
                                                <li class="dropdown inline-block hidden-sm visible-xs-inline-block search-box-btn">
                                                    <a href="javascript:;" class="font-white font-size12"><i class="fa fa-search"></i> </span></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="inline-block header-menu pull-right">
                                        <div class="navbar-collapse navbar-right" style="clear:both">
                                            <ul class="nav navbar-nav navbar-top-menu margin-top-bottom-0-i navbar-top-menu-header-list">
                                                <li class="dropdown inline-block">
                                                    <a href="#" class="dropdown-toggle font-white font-size12" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo clang::__("Status Order"); ?> </span></a>
                                                    <ul class="dropdown-menu status-pesanan font-black" role="menu">
                                                        <li>
                                                            <form action="<?= curl::base() . 'retrieve/invoice' ?>" id="form-status-pesanan" name="form-status-pesanan" class="form-62hallfamily">
                                                                <strong class="font-red font-big"><?php echo clang::__("Check Order Status"); ?></strong><br>
                                                                <strong><?php echo clang::__("Order Number"); ?></strong></br>
                                                                <input style="height: 26px" type="text" id="nomor-pesanan" name="nomor-pesanan" class="font-small"/></br>
                                                                <strong><?php echo clang::__("Email"); ?></strong></br>
                                                                <input style="height: 26px" type="text" id="email" name="email" class="font-small"/></br>
                                                                <button type="submit" class="btn-62hallfamily-small margin-top-10 border-3-red bg-red"><?php echo clang::__("VIEW"); ?></button>
                                                            </form>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="dropdown inline-block">
                                                    <a href="<?php echo $url_help; ?>" class="font-white font-size12"><?php echo clang::__("Help"); ?> </a>
                                                </li> 
                                            </ul>
                                        </div>
                                    </div>
                                </div> <!-- end div -->
                            </div>
                        </div>
                    </div>

                    <?php
                    $search_placeholder = clang::__('Search nama product') . " ...";
                    ?>  

                    <div class="search-box search-box-style">
                        <form name="form-search" id="form-search-sm" class="form-horizontal form-62hallfamily" target="_self" action="/search" method="GET" autocomplete="on" enctype="application/x-www-form-urlencoded">
                            <div class="div-table full-width margin-bottom-20 search-box-style">                                 
                                <div class="div-table-cell">
                                    <input type="hidden" name="page" id="page" class="" value="<?php echo $page; ?>">
                                    <?php
                                    if (strlen($org_share_code) > 0) {
                                        ?>
                                        <input type="hidden" name="org_share_code" id="org_share_code" class="" value="<?php echo $org_share_code; ?>">
                                        <?php
                                    }
                                    ?>
                                    <input type="text" placeholder="<?php echo $search_placeholder; ?>" name="keyword" id="keyword" class="input-unstyled form-control search form-control  validate[]" value="<?php echo chtml::specialchars($keyword); ?>" style="margin-top:2px !important;">
                                </div>                  
                                <div class="div-table-cell search-box-btn-submit">                 
                                    <button type="submit" class="btn-62hallfamily bg-red padding-left-0 border-3-red" style="height:35px;margin-top:27px !important"><i class="fa fa-search"></i></button>
                                </div>
                            </div>      
                        </form>
                    </div>

                    <div class="bg-white padding">
                        <div class="container">
                            <div class="row">
                                <div id="logo" class="col-md-4 col-sm-4 col-xs-6">
                                    <!-- <div id="nav-side-bar" class="link">
                                        <i class="fa fa-navicon font-red"></i>
                                    </div> -->
                                    <?php
//                                    $logo = curl::base() . 'application/admin62hallfamily/' . $get_code_org . '/upload/logo/item_image/' . $item_image;
                                    $logo = curl::base() . 'application/adminittronmall/' . $get_code_org . '/upload/logo/item_image/' . $item_image;
                                    ?>
                                    <a href="<?= curl::base() ?>"><img class="logo brand" src="<?= $logo ?>" alt="<?php echo $store; ?>"/></a>
                                </div>
                                <div id="search" class="col-md-5 col-sm-6" style="padding-left:0px;">
                                     <form name="form-search" id="form-search" class="form-horizontal form-62hallfamily" target="_self" action="/search" method="GET" autocomplete="on" enctype="application/x-www-form-urlencoded">
                                        <div class=" col-md-12 col-xs-12 margin-bottom-20 hidden-xs search-field">
                                            <div class=" col-md-11 col-xs-11">
                                                <?php
                                                $search_placeholder = clang::__('Search product') . " ...";
                                                ?>
                                                <input type="hidden" name="page" id="page" class="" value="<?php echo $page; ?>">
                                                <?php
                                                if (strlen($org_share_code) > 0) {
                                                    ?>
                                                    <input type="hidden" name="org_share_code" id="org_share_code" class="" value="<?php echo $org_share_code; ?>">
                                                    <?php
                                                }
                                                ?>
                                                <div class="form-group ">							
                                                    <label class="  control-label"></label>
                                                    <input type="text" placeholder="<?php echo $search_placeholder; ?>" name="keyword" id="keyword" class="input-unstyled form-control search form-control  validate[] keyword-suggest" value="<?php echo chtml::specialchars($keyword); ?>" style="margin-top:2px !important;" autocomplete="off">
                                                </div>
                                            </div>					
                                            <div  class=" col-md-1 col-xs-1 padding-0">					
                                                <button type="submit" class="btn-62hallfamily bg-red padding-left-0 border-3-red" style="height:35px;margin-top:27px !important"><i class="fa fa-search"></i></button>
                                            </div>
                                        </div>		
                                    </form>
                                </div>
                                <div id="shopping-cart" class="col-md-1 col-sm-2 col-xs-6 shopping-cart hidden-sm shopping-chart-pro" <?php echo $shopping_cart_pro; ?> >

                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-8 auth desktop hidden-sm hidden-xs">
                                    <?php
                                    $additional_footer_js = "";
                                    if (member::get_data_member() == false) {
                                        echo '<ul class="col-lg-12 col-md-12 col-sm-12 bg-red usermenu">';
                                        if ($have_register > 0) {
                                            echo '<li class="non-active">';
                                            $element_register = CElement_Register::factory()
                                                    ->set_trigger_button(true)
                                                    ->set_icon(FALSE)
                                                    ->set_store($store)
                                                    ->set_have_gold($have_gold)
                                                    ->set_have_service($have_service);
                                            echo $element_register->html();
                                            $additional_footer_js .= $element_register->js();
                                            echo '</li>';
                                        }
                                        echo '<li class="active">';
                                        $login_element = CElement_IttronmallLivemall_Login::factory()
                                                ->set_session(false)
                                                ->set_trigger_button(TRUE)
                                                ->set_icon(FALSE);
                                        echo $login_element->html();
                                        echo '</li>';
                                        echo '</ul>';
                                        $additional_footer_js .= $login_element->js();
                                    } else {
                                        echo '<ul class="col-lg-12 col-md-12 col-sm-12">';
                                        $login_element = CElement_IttronmallLivemall_Login::factory()
                                                ->set_session(true)
                                                ->set_trigger_button(TRUE)
                                                ->set_icon(FALSE);
                                        echo $login_element->html();
                                        $additional_footer_js .= $login_element->js();
                                        echo '</ul>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
            </div>

            <div class="container-slide-menu">
                <div class="row">
                    <?php
                    $product_category = product_category::get_product_category_menu('product', '', $org_id);
                    $menu_list = array(
                        'name' => 'Kategori',
                        'menu' => $product_category
                    );
                    $element_menu = CElement_IttronmallLivemall_Menu::factory();
                    $element_menu->set_menu($menu_list);
                    $element_menu->set_trigger_type('click');
                    echo $element_menu->html();
                    $additional_footer_js .=$element_menu->js();
                    ?>
                </div>
            </div>