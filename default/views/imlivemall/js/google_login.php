<script>

function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    var google_id_token = googleUser.getAuthResponse().id_token;
    if(g_google_login == true){
        login_google(google_id_token);
    }
}

function onSignInFailure(googleUser){
    console.log(googleUser.reason);
}

function login_google(google_id_token) {
    var google_url = "<?php echo curl::httpbase();?>auth/vTokenGoogle";
    var redir_url = jQuery(location).attr('href');

    $.ajax({
        type: "post",
        dataType: 'json',
        url: google_url,
        data: {
            id_token: google_id_token
        },
        success: function(result) {
            if(result.error == 0){
                location.href = redir_url+"change_password";
            }
            else {
                console.log(result);
            }
        },
        error: function(result) {
            console.log('error ajax '+result);
        }
    });
}