<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php
ob_start();
$tanda_seru = curl::base() . 'application/62hallfamily/default/media/img/tanda-seru.png';
?>
<style>
    .title-not-found {
        font-size: 20px;
        font-weight: bold;
        margin: 30px;
    }
    .text-not-found {
        font-size: 20px;        
        margin: 15px;
        margin-bottom: 50px;
    }
    .tanda-seru {
        width: 150px;
        margin-top: 50px;        
    }
</style>
<div class="main-inner">

	    <div id="container" class="container login-container">
                <center>                    
                    <img src="<?php echo $tanda_seru; ?>" class="tanda-seru">
                    <div class="title-not-found">
                        <h2><b>404 PAGE NOT FOUND</b></h2>
                    </div>
                    <div class="text-not-found">
                        We are sorry but the page you are looking for doesn't exist<br>
                        You could return to the home page
                    </div>
                </center>
<!--                    <div class="row-fluid">
                            <div class="span12">
                                    <div class="well">
                                            <h1>404 Page Not Found</h1>
                                            testing
                                            <p>Sorry, an error has occured, Requested page not found!</p>
                                            <a href="<?php echo curl::base(); ?>" class="btn btn-primary"><i class="icon icon-home"></i> Take me to home</a>
                                    </div>
                            </div>
                    </div>-->
            </div>
</div>
<?php
$content = ob_get_clean();
$app = CApp::instance();
//include DOCROOT . 'application\62hallfamily\default\libraries\SixtyTwoHallFamilyController.php';
$ctrl = SixtyTwoHallFamilyController::factory();
$menu_list = $ctrl->menu_list();
$link_list = $ctrl->list_menu();
$element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
$element_menu->set_menu($menu_list)->set_link($link_list);
$app->add($content);
echo $app->render();
