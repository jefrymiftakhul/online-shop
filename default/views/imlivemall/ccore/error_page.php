<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php
ob_start();
$sad_face = curl::base() . 'application/62hallfamily/default/media/img/sad-face.png';
?>
<style>
    .title-something-wrong {
        color: red;
        margin-bottom: 75px;
        margin-top: 15px;
    }
    .sad-face {
        margin-bottom: 15px;    
        margin-top: 75px;
        width: 150px;
    }
</style>
<div class="main-inner">
    <div id="container" class="container login-container">
        <center>                    
            <img src="<?= $sad_face ?>" class="sad-face">
            <div class="title-something-wrong">
                <h3><b>OOPS! SOMETHING WENT WRONG</b></h3>
            </div>
        </center>
    </div>
</div>
<?php
$content = ob_get_clean();
$app = CApp::instance();
$ctrl = SixtyTwoHallFamilyController::factory();
$menu_list = $ctrl->menu_list();
$link_list = $ctrl->list_menu();
$element_menu = $app->add_element('62hall-menu', 'menu-62hall-product');
$element_menu->set_menu($menu_list)->set_link($link_list);
$app->add($content);
echo $app->render();
