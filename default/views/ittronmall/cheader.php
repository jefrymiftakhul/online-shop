<?php
    defined('SYSPATH') OR die('No direct access allowed.');
    $url_base = curl::base();
    $db = CDatabase::instance();

    $data_org = org::get_info();
    $theme_color = carr::get($data_org, 'themes_color', "default");
    $org_code = carr::get($data_org, 'code');
    
    $org_name = carr::get($data_org, 'name');
    $url_help='';
?>
<!DOCTYPE html>
<html>
    <head>
        <?php echo $additional_head; ?>
        <title><?php echo carr::get($data_org, 'name'); ?></title>
        <meta charset="UTF-8">
        <meta http-equiv="X-Frame-Options" content="deny">
        <link rel="icon" type="image/png" href="<?php echo carr::get($data_org, 'favicon_full_path'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
        <?php echo $head_client_script; ?>
        <script type="text/javascript">
            var baseUrl = '<?php echo curl::base(); ?>';
            var code = '<?php echo $org_code; ?>';
            <?php 
                $image_loading = curl::base() .'application/ittronmall/' .$org_code .'/media/img/asset/loading.gif';
                if (!file_exists($image_loading)) {
                    $image_loading = curl::base() .'application/ittronmall/default/media/img/asset/loading.gif';
                }
            ?>
            var imageLoading = '<?php echo $image_loading; ?>';
        </script>
    </head>
    <body class="<?php echo $theme_color; ?>">
        <header>
            <div id="top-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-xs-5 socmed-wrapper">
                        </div>
                        <div class="col-md-6 col-xs-7 txt-right head-navbar">
                            <span class="head-navbar-item">
                            </span> 
                            <span class="head-navbar-item">
                                <a href="<?php echo $url_help; ?>" class="ost-navbar-btn-link hidden-xs">
                                    <?php echo clang::__('Help'); ?>
                                </a>
                                <a href="<?php echo $url_help; ?>">
                                    <span class="hidden-sm hidden-md hidden-lg os os-help"></span>
                                </a>
                                
                            </span> 
                            <?php 
                                // Modal dialog outside header
                                echo CIttronMallElement_Auth_ButtonLoginRegister::factory('btn-login-reg')
                                        ->set_login(true)->set_register(true)->html(); 
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="middle-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12 txt-center">
                            <a href="<?= $url_base ?>">
                                <img class="logo brand"
                                     src="<?php echo carr::get($data_org, 'logo_full_path'); ?>"
                                         <?php // echo curl::base() ."application/62hallfamily/default/media/img/logo.png"; ?> 
                                     alt="<?php echo carr::get($data_org, 'name'); ?>"/>
                            </a>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <div class="row">
                                <?php
                                    $search_class = 'col-md-12';
                                    if (general::get_curr_page() == 'product') {
                                        $search_class = 'col-md-10 col-sm-10 col-xs-9';
                                    }
                                ?>
                                <div class="<?php echo $search_class; ?>">
                                    <?php 
                                        $search_form = CIttronMallElement_Search::factory()->set_type(general::get_curr_page());
                                        echo $search_form->html();
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
 		<?php 
			
        ?>
       
            
        