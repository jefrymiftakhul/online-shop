<?php 
    $data_org = org::get_info();
    $store_name = carr::get($data_org, 'name');
                    
    $menu_service = cms::nav_menu('menu_footer_service');
    $menu_about = cms::nav_menu('menu_footer_about');
    $arr_option = cms_options::get();
?>

        <div class="body-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <h4 class="footer-title"><?php echo clang::__('NEWSLETTER'); ?></h4>
                        <div class="footer-content row">
                            <div class="col-md-12 col-sm-6">
                                <?php echo clang::__("Register and get attractive deals from ") .$store_name;?>
                            </div>
                            <div class="col-md-12 col-sm-6">
                                <?php
                                    $subscriber = C62HallElement_Subscriber::factory();
                                    echo $subscriber->html();
                                ?>
                            </div>
                            <div class="col-md-12">
                                <span class="os os-visa-mastercard"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 cust-service-container">
                        <h4 class="footer-title"><?php echo clang::__('LAYANAN PELANGGAN'); ?></h4>
                        <div class="footer-content">
                            <ul class="list-unstyled cust-service">
                            <?php
                                if (count($menu_service) > 0) {
                                    foreach ($menu_service as $menu_service_k => $menu_service_v) {
                                        echo '<li><a href="'.$menu_service_v['menu_url'].'">'.$menu_service_v['menu_name'].'</a></li>';
                                    }
                                }
                            ?>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <h4 class="footer-title"><?php echo $store_name; ?></h4>
                        <div class="footer-content">
                            <ul class="list-unstyled store-footer-list">
                                <?php
                                if (count($menu_about) > 0) {
                                    foreach ($menu_about as $menu_about_k => $menu_about_v) {
                                        echo '<li><a href="'.$menu_about_v['menu_url'].'">'.$menu_about_v['menu_name'].'</a></li>';
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <h4 class="footer-title"><?php echo clang::__('HUBUNGI KAMI'); ?></h4>
                        <div class="footer-content row">
                            <?php
                                $contact_us='';
                                $phone='';
                                $phone_mobile='';
                                if(carr::get($arr_option,'contact_us')){
                                        $contact_us=carr::get($arr_option,'contact_us');
                                        echo '<div class="col-md-12">'.$contact_us.'</div>';
                                }
                                if(carr::get($arr_option,'contact_us_phone_1')){
                                        $phone=carr::get($arr_option,'contact_us_phone_1');
                                        echo '<div class="col-md-12">Telp : '.$phone.'</div>';
                                }
                                if(carr::get($arr_option,'contact_us_mobile_1')){
                                        $phone_mobile=carr::get($arr_option,'contact_us_mobile_1');
                                        echo '<div class="col-md-12">&nbsp &nbsp &nbsp &nbsp &nbsp '.$phone_mobile.'</div>';
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 txt-center">
                        <?php 
                            $footer = '';
                            if (carr::get($arr_option,'footer')) {
                                $footer = carr::get($arr_option,'footer');
                            }
                            echo $footer;
                        ?>
                    </div>
                </div>
            </div>			
        </footer>

        <script src="<?php echo curl::base(); ?>media/js/require.js"></script>
        <script language="javascript">
            requirejs.config({
                waitSeconds: 0,
            });
        <?php 
            echo $js . $ready_client_script; 
        ?>
	
            if (window) {
                window.onload = function() {
        <?php echo $load_client_script; ?>
                }
            }
        <?php  echo $custom_js ?>
        </script>
        <?php echo $custom_footer; ?>
    </body>
</html>