<?php 
    $session = Session::instance();
    
    $data_member = member::get_data_member();
    echo $header_body;
?>

<div class="clear-both"></div>
<?php
    $template = Session::instance()->get('template');
    if ($template == true) {
?>
<div class="content-wrapper">
    <div class="container">
        <?php if (Session::instance()->get('member_area') == 1) { ?>
        <div class="row member-container">
            <div class="col-md-4 col-sm-12">
                <div class="member-navbar-wrapper">
                    <div class="row">
                        <div class="col-md-6 col-xs-6 image">
                            <img src="<?php echo cobj::get($data_member, 'image_url'); ?>"/>
                        </div>
                        <div class="col-md-5 col-xs-5 user-right-info">
                            <div class="user-right-info-content">
                                <?php echo $session->get('name'); ?>
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-member">
                        
                        <li><a <?php if(CFRouter::$controller == 'profile') { echo 'class="active"'; } ?> href="<?php echo curl::base(); ?>member/profile">Halaman Akun</a></li>
                        <li><a <?php if(CFRouter::$controller == 'changeProfile') { echo 'class="active"'; } ?> href="<?php echo curl::base(); ?>member/changeProfile">Ubah Akun</a></li>
                        <li><a <?php if(CFRouter::$controller == 'changePassword') { echo 'class="active"'; } ?> href="<?php echo curl::base(); ?>member/changePassword">Ubah Password</a></li>
                        <li><a <?php if(CFRouter::$controller == 'addressBook') { echo 'class="active"'; } ?> href="<?php echo curl::base(); ?>member/addressBook">Buku Alamat</a></li>
                        <li><a <?php if(CFRouter::$controller == 'myOrder') { echo 'class="active"'; } ?> href="<?php echo curl::base(); ?>member/myOrder">Pesanan Saya</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-8 col-sm-12">
        <?php } ?>
        <?php
            $msg = cmsg::flash_all();
            if ( strlen($msg) > 0 ) {
                echo '<div class="row os-cmsg"><div class="col-md-12">' . $msg . '</div></div>';
            }
        ?>        
        <?php echo $content; ?>
                
        <?php if (Session::instance()->get('member_area') == 1) { ?>
            </div> 
        </div> <!-- END Member Area -->
        <?php } ?>
    </div>
</div>
<?php 
    }
    else {
        echo $content;
    }
?>