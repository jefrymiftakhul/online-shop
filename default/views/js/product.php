
<script>
    jQuery('.fa.link-toggle-filter-header').on('click', function () {
        // action close dialog
        if (jQuery(this).hasClass('fa-caret-down')) {
            jQuery(this).removeClass('fa-caret-down');
            jQuery(this).addClass('fa-caret-right');
            jQuery(this).parent('.title').next('.list-content').hide();
        } else { // action open dialog
            jQuery(this).removeClass('fa-caret-right');
            jQuery(this).addClass('fa-caret-down');
            jQuery(this).parent('.title').next('.list-content').show();
        }
    });

    jQuery('.fa.link-toggle').on('click', function () {
        // action close dialog
        if (jQuery(this).hasClass('fa-caret-down')) {
            jQuery(this).removeClass('fa-caret-down');
            jQuery(this).addClass('fa-caret-right');
            jQuery(this).parents('.item-title').next('.item-list').hide();
        } else { // action open dialog
            jQuery(this).removeClass('fa-caret-right');
            jQuery(this).addClass('fa-caret-down');
            jQuery(this).parents('.item-title').next('.item-list').show();
        }
    });

    // SORT
    var sort_by = jQuery('#sort-by').val();
    jQuery("body").on("click", "#sort-by-select .dropdown-menu-list", function () {
        if (jQuery(this).attr('val') != sort_by) {
            reload_product();
        }
    });

    // FILTER ITEM & ATTRIBUTE
    jQuery(".filter-item").on('click', function (e) {
        reload_product();
    });

    // View per page
    var view_per_page = jQuery('#view_per_page').val();
    jQuery("body").on("click", "#view_per_page-select .dropdown-menu-list", function () {
        if (jQuery(this).attr('val') != view_per_page) {
            reload_product();
        }
    });

    // PAGINATION
    jQuery(".pagination .item").on('click', function () {
        var page = jQuery(this).html();

        if (jQuery(this).hasClass('point')) {
            // do nothing
        }
        else if (jQuery(this).hasClass('active')) {
            // do nothing
        }
        else {
            if (jQuery(this).hasClass('first')) {
                page = jQuery(this).parents('nav').find('.page-item:first').html();
            } else if (jQuery(this).hasClass('last')) {
                page = jQuery(this).parents('nav').find('.page-item:last').html();
            } else if (jQuery(this).hasClass('next')) {
                page = parseInt(jQuery("#current_page").val()) + 1;
            } else if (jQuery(this).hasClass('prev')) {
                page = parseInt(jQuery("#current_page").val()) - 1;
            }
            jQuery('#current_page').val(page);
            reload_product();
        }
    });

    function reload_product() {
        var url = '<?php echo $url; ?>?';
        var filter = [];

        // query
        var query = jQuery("#q").val();
        if (query.length > 0) {
            filter.push('q=' + query);
        }

        // page
        var page = jQuery("#current_page").val();
        filter.push("page=" + page);

        var view_per_page = jQuery("#view_per_page").val();
        var per_page = jQuery("#per_page").val();
        if (jQuery("#view_per_page").length > 0) {
            per_page = view_per_page;
        }
        filter.push("per_page=" + per_page);

        // sort by
        var sort_by = jQuery('#sort-by').val();
        filter.push('sord=' + sort_by);

        // filter attribute
        jQuery('.filter-box').each(function () {
            var key = jQuery(this).attr('_key');
            var items = jQuery(this).find('a.dropdown-menu-list-link .item:checked');
            var filter_item_per_category = [];
            items.each(function () {
                filter_item_per_category.push(jQuery(this).val());
            });
            if (filter_item_per_category.length > 0) {
                var filter_item_str = filter_item_per_category.join(",");
                filter.push(key + "=" + filter_item_str);
            }
        });
        
        var filter_str = filter.join('&');
        url = url + filter_str;
        
    <?php
        if ($reload) {
            ?>
                $.cresenity.reload('<?php echo $target ?>', url, 'get', '');
            <?php
        }
        else {
            ?>
                $.lastm.showLoading();
                window.location.href = url;
            <?php
        }
    ?>
    }