<script>
    var facebook_login = "<?php echo ccfg::get('login_facebook');?>";
    var facebook_login_app_id = "<?php echo ccfg::get('login_facebook_app_id');?>";

    if(facebook_login == 1){
        function statusChangeCallback(response) { 
            if (response.status === 'connected') {
                // FB.api('/me?fields=name,email', function(response) {
                //     ajax_login_fb(response.email, response.name);
                // });
            } else if (response.status === 'not_authorized') {
              // The person is logged into Facebook, but not your app.
            
            } else {
              // The person is not logged into Facebook, so we're not sure if
              // they are logged into this app or not.        
            }
        }

        function checkLoginState() {
            FB.getLoginStatus(function(response) {
                statusChangeCallback(response);
            });
        }

        window.fbAsyncInit = function() {
            FB.init({
                appId      : facebook_login_app_id,
                cookie     : true,  // enable cookies to allow the server to access 
                                    // the session
                xfbml      : true,  // parse social plugins on this page
                version    : 'v2.2' // use version 2.2
            });

            FB.getLoginStatus(function(response) {
                statusChangeCallback(response);
            });

            checkLoginState();
        };

        // Load the SDK asynchronously
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

     
        function testAPI() {
            console.log('Welcome!  Fetching your information.... ');
            FB.api('/me?fields=name,email', function(response) {
                console.log(JSON.stringify(response));
            });
        }

        function login_fb() {
            FB.login(function(response) {
                if (response.authResponse) {
                    // if login success / connected                    
                    //profile    
                    FB.api('/me?fields=name,email', function(p_response){
                        //token auth
                        var fb_access_token = '';
                        var fb_user_id = p_response.id;
                        var fb_profile = JSON.stringify(p_response);
                        var fb_auth = '';
                        
                        FB.getLoginStatus(function(st_response) {
                            fb_access_token = st_response.authResponse.accessToken;
                            fb_auth = JSON.stringify(st_response);
                        }); 

                        ajax_login_fb(fb_access_token, fb_user_id, fb_profile, fb_auth);
                    });
                } else {
                    // cancelled
                    console.log('cancel login');
                }
            }, { scope: 'email' });
        }

        function ajax_login_fb(fb_access_token, fb_user_id, fb_profile, fb_auth){
            var fb_url = "<?php echo curl::httpbase();?>authentication/vToken";
            var redir_url = jQuery(location).attr('href');

            $.ajax({
                type: "post",
                dataType: 'json',
                url: fb_url,
                data: {
                    access_token: fb_access_token,
                    user_id: fb_user_id,
                    profile: fb_profile,
                    auth: fb_auth
                },
                success: function(result) {
                    if(result.error == 0){
                        location.href = redir_url;
                    }
                    else {
                        console.log(result);
                    }
                },
                error: function(result) {
                    console.log('error ajax '+result);
                }
            });
        }

        jQuery("body").on('click', ".btn-login-fb", function(){
            login_fb();
        });
    }

    