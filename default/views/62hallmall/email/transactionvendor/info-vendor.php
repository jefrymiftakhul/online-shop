<body style="margin:0;padding:0;font-family: \'Open Sans\', Arial, serif; font-weight: 400; ;">
   <div class="main-wrapper" style="width:100%;margin:0;padding:0;">
       <?php 
			// routing theme
			$theme = CF::theme();
			$theme_ready = array('62hallmall', 'imbuilding');
			$path = '';
			if (strlen($theme) > 0) {
				if (in_array($theme, $theme_ready)) {
					$path = $theme .'/';
				}
			}
            $header = CView::factory($path.'email/header');
			$header->org_id = $org_id;
			echo $header->render();
       ?>
<!-- Content -->
       <div class="content" style="padding:30px;">
                <div class="title-left" style="font-size:18px;font-weight:bold;font-family:">
                    <?php echo clang::__('Dear').' '.$member_name; ?>
                </div>
                <div class="content-text">
                  <p>
                    Selamat produk anda telah terjual. Silahkan lakukan pengiriman ke :
                  </p>
                  <p>
                    <table border="0">                          
                        <tr>                        
                          <td width="100">Nama</td><td> :<?php echo $member_shipping->shipping_first_name." ".$member_shipping->shipping_last_name; ?></td>
                        </tr>
                        <tr>
                          <td valign="top">Alamat</td><td> :<?php echo $member_shipping->shipping_address; ?>, <?php echo $member_shipping->shipping_district; ?><br>
                            <?php echo ucwords(strtolower($member_shipping->shipping_city)). " - ".$member_shipping->shipping_province; ?>
                          </td>
                        </tr>
                        <tr>
                          <td>Nomor HP</td><td> :<?php echo $member_shipping->shipping_phone; ?></td>
                        </tr>
                    </table>
                  <p>
                  Dengan list barang sebagai berikut :
                    <table border="0" style="width:100%;max-width:800px;border-collapse: collapse;border:solid 1px #132944;">                             
                      <thead>
                        <tr style="background:#132944;">
                          <th style="padding:10px;color:#fff;font-size:13px;">No</th>
                          <th style="padding:10px;color:#fff;font-size:13px;"><?php echo clang::__('Nama Barang'); ?></th>
                          <th style="padding:10px;color:#fff;font-size:13px;"><?php echo clang::__('Jumlah'); ?></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          $num = 1;
                          foreach ($detail_barang as $key => $value) {
                            echo '<tr>';
                              echo '<td align="center">'.$num.'</td>';
                              echo '<td style="padding:10px;">'.$value["product_name"].'</td>';
                              echo '<td align="center">'.$value["jumlah"].'</td>';
                            echo '</tr>';
                            $num++;
                          }
                        ?>
                      </tbody>
                    </table>
                  </p>
                  <p>
                          <?php //echo clang::__('Keep up with special offers from us by following our newsletter. Thank you for choosing the Last Minute as a place to offer your products.'); ?>
                  </p>
                </div>
        </div>

<?php
	$footer_file=CF::get_file('views',$path.'email/footer');
    if(file_exists($footer_file)){
        include $footer_file;
        
    }        
             
    
   ?>
    </div>
</body>
