<?php
global $additional_footer_js;

$store = NULL;
$domain = NULL;
$org_id = CF::org_id();
$data_org = org::get_org($org_id);
$db=CDatabase::instance();
if(!empty($data_org)){
    $store = $data_org->name;
    $domain = $data_org->domain;
}

$arr_option=array();
$q="
	select
		*
	from
		cms_options
	where
		org_id=".$db->escape($org_id)."
";
$r=$db->query($q);
if($r->count()>0){
	foreach($r as $row){
		$arr_option[$row->option_name]=$row->option_value;
	}
}

?>
</div>
<!-- end main content -->
<!-- footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h3 class="widget-title">Newsletter</h3>
                <?php
                $element_subscribe = CElement_62hallmall_Product_Subscribe::factory();
                echo $element_subscribe->html();
                $additional_footer_js .= $element_subscribe->js();
                ?>
            </div>
            <div class="col-md-4">
                <h3 class="widget-title">Ikuti Kami</h3>
                <?php
                $media = cms_options::get('media_', CF::org_id(), 'all');
                if ($media != NULL) {
                    echo '<ul class="list-inline social-media">';
                    foreach ($media as $media_k => $media_v) {
                        $option_name = carr::get($media_v, 'option_name');
                        $option_value = carr::get($media_v, 'option_value');
                        if (strlen($option_value) > 0) {
                            echo '<li class="'.$option_name.'"><a href="'.$option_value.'" target="_blank">'.$option_name.'</a></li>';
                        }
                    }
                    echo '</ul>';
                }
                ?>
            </div>
            <div class="col-md-4">
                <h3 class="widget-title">Download Aplikasi</h3>
                <ul class="list-inline mobile-apps">
                    <li class="playstore"><a href="https://play.google.com/store/apps/details?id=kinerja.kinerjapay&hl=en" target="_blank">Playstore</a></li>
                    <li class="appstore coming-soon"><a href="#">Appstore</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <h3 class="widget-title">Layanan Pelanggan</h3>
                <?php $menu_service = cms::nav_menu('menu_footer_service');?>
                <ul class="list-unstyled">
                    <?php
                    if (count($menu_service) > 0) {
                        foreach ($menu_service as $menu_service_k => $menu_service_v) {
                            echo '<li><a href="'.$menu_service_v['menu_url'].'">'.$menu_service_v['menu_name'].'</a></li>';
                        }
                    }
                    ?>
                </ul>
                <h3 class="widget-title">Hubungi Kami</h3>
                    <div class="widget-content">
                    <?php
                    $contact = cms_options::get('contact', NULL, 'all');
                    $contact_data = array();
                    if (count($contact) > 0) {
                        foreach ($contact as $contact_k => $contact_v) {
                            $option_name = carr::get($contact_v, 'option_name');
                            $option_value = carr::get($contact_v, 'option_value');
                            $contact_data[$option_name] = $option_value;
                        }
                    }
                    echo carr::get($contact_data, 'contact_us');
                    ?>
                    </div>
            </div>
            <div class="col-md-3">
                <h3 class="widget-title"><?php echo $store;?></h3>
                <?php $menu_about = cms::nav_menu('menu_footer_about');?>
                <ul class="list-unstyled">
                    <?php
                    if (count($menu_about) > 0) {
                        foreach ($menu_about as $menu_about_k => $menu_about_v) {
                            echo '<li><a href="'.$menu_about_v['menu_url'].'">'.$menu_about_v['menu_name'].'</a></li>';
                        }
                    }
                    ?>
                </ul>
            </div>
            <div class="col-md-6">
                <h3 class="widget-title">Online Shopping Terlengkap Se-Indonesia</h3>
                <div class="widget-content">
                <?php
                   $footer = cms_options::get('footer_summary');
                   echo $footer;
                ?>  
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- end footer -->
<?php
    //cdbg::var_dump($additional_footer_js);
?>
<script src="<?php echo curl::base(); ?>media/js/require.js"></script>

<?php
        $facebook_pixel_id = cms_options::get('facebook_pixel');
        //cdbg::var_dump($google_analytics_code);
        $facebook_pixel_script="
            !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
         fbq('init', '".$facebook_pixel_id."'); 
        fbq('track', 'PageView');
            
        ";
        if(strlen($facebook_pixel_id)>0) {
            $facebook_pixel_code="
                 <!-- Facebook Pixel Code -->
            <script>
            ".$facebook_pixel_script."
            </script>
            <noscript>
             <img height='1' width='1' 
            src='https://www.facebook.com/tr?id=".$facebook_pixel_id."&ev=PageView
            &noscript=1'/>
            </noscript>
            <!-- End Facebook Pixel Code -->
                ";
           
            $error=0;
            try {
                CJSMin::minify($facebook_pixel_script);
            } catch (Exception $ex) {
                $error++;
            }
            if($error==0) {
                echo $facebook_pixel_code;
            }
            
        }

    ?>

<script type="text/javascript">
    <?php
        $google_analytics_id = cms_options::get('google_analytics');
        //cdbg::var_dump($google_analytics_code);
        $google_analytics_code="
             (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                ga('create', '".$google_analytics_id."', 'auto');
                ga('send', 'pageview');
            ";
        $is_google_analytics_code = preg_match("/^ua-\d{4,9}-\d{1,4}$/i", strval($google_analytics_id)) ? true : false;
       
        if($is_google_analytics_code) {
            $error=0;
            try {
                CJSMin::minify($google_analytics_code);
            } catch (Exception $ex) {
                $error++;
            }
            if($error==0) {
                echo $google_analytics_code;
            }
        }

    ?>
        
        
    
document.addEventListener('capp-started', function(customEvent) {
    <?php
    echo $additional_footer_js; 
    ?>
            
});	
<?php
echo $js;
echo $ready_client_script;
?>
    
    if (window) {
        window.onload = function() {
<?php 
    echo $load_client_script;
?>
        }
    }
<?php echo $custom_js ?>
</script>
<?php echo $custom_footer; ?>

</body>
</html>
<?php

if (ccfg::get("log_request")) {


    log62::request();
}
?>