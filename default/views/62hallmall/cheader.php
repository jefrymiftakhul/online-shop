<?php
defined('SYSPATH') OR die('No direct access allowed.');
$url_base = curl::httpbase();
$db = CDatabase::instance();
$cookie_name = "cookie_popup";
$show_popup=0;
$popup_content='';
//~~
$org_id = CF::org_id();
if(!isset($_COOKIE[$cookie_name])) {
    setcookie($cookie_name, 'test_cookies', time() + (3600 * 4), "/"); // 3600 = 1 hour
    $q = "SELECT * FROM cms_popup where status>0 and is_active = 1 and org_id=".$db->escape($org_id)." ORDER BY RAND() LIMIT 1";
    $result = cdbutils::get_row($q);
    if($result!==null){
//        $arr=array(
//            'url_image'=>$result->image_url,
//        );
        //$json_additional_param=json_encode($arr);        
        $show_popup=1;
        $popup_content='<div style="top:-20px; background-image: url('.$result->image_url.'); background-repeat:no-repeat;" class="modal-content-background"></div>';
    }
}
$get_code_org = "default";
$get_code = '';
$fav_icon = '';
$item_image = '';
$org_name = '';
if ($org_id) {
    $get_code = cdbutils::get_row("SELECT * FROM org WHERE org_id = " . $db->escape($org_id));
    $get_code_org = $get_code->code;
    $fav_icon = $get_code->item_favicon;
    $item_image = $get_code->item_image;
    $org_name = $get_code->name;
}

$keyword = carr::get($_GET, 'keyword');


$controller = CF::instance();
$page = 'product';
if (method_exists($controller, 'page')) {
    $page = $controller->page();
}
if (carr::get($_GET, 'page') != null && CFRouter::$controller == 'search') {
    $page = carr::get($_GET, 'page');
}

if ($page != 'product') {
    $url_base = curl::httpbase() . $page . '/home/';
}

if ($page == 'kinerja') {
    $url_base = curl::httpbase();
}




$data_org = org::get_org($org_id);
$store = '';
$have_product = 0;
$have_gold = 0;
$have_service = 0;
$have_register = 0;
$have_deposit = 0;

if (!empty($data_org)) {
    $store = $data_org->name;
    $have_product = $data_org->have_product;
    $have_gold = $data_org->have_gold;
    $have_service = $data_org->have_service;
    $have_register = $data_org->have_register;
    $have_deposit = $data_org->have_deposit;
}
$url_site = cms::get_option('site_url', 'single', $org_id);

$page_type = "produk";
if ($page == "service") {
    $page_type = "jasa";
}
$url_help = curl::base() . 'read/page/index/beli-' . $page_type . '-di-' . cstr::sanitize($org_name);

global $additional_footer_js;
?>
<!DOCTYPE html>
<html lang="id">
    <head>
        <?php echo $additional_head; ?>
        <title><?php echo $store; ?></title>
        <meta charset="UTF-8">
        <?php
        $login_from_google = ccfg::get("login_google");
        if ($login_from_google) {
            ?>
            <meta name="google-signin-client_id" content="<?php echo ccfg::get('login_google_app_id'); ?>">
        <?php } ?>
        <link rel="icon" type="image/png" href="<?= curl::base() . 'application/admin62hallfamily/' . $get_code_org . '/upload/logo/item_favicon/' . $fav_icon ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
        <?php echo $head_client_script; ?>
        <?php
//        cdbg::var_dump($head_client_script);
//        die();
        ?>

        <script type="text/javascript">
            var g_google_login = false;
            <?php if($show_popup==1){ ?>
            document.addEventListener('capp-started', function(customEvent) {
                        $.app.show_dialog('promopopup','  ','<?php echo addslashes($popup_content); ?>','popup-promo'); 

            });
            <?php } ?> 
        </script>

    </head>
    <?php $theme_color = ''; ?>
    <body class="<?php echo $theme_color; ?>">
        <!-- header -->
        <header>
            <div id="headtop" class="hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 hidden-sm">
                            <!--News Ticker-->
                            <?php
                            $additional_footer_js = "";
                            
                            $news_list = cms::get_post(NULL, 'news');
                            if (count($news_list) > 0) {
                                $element_newsticker = CElement_62hallmall_Newsticker::factory('');
                                $element_newsticker->set_list_data($news_list);
                                echo $element_newsticker->html();
                                $additional_footer_js .= $element_newsticker->js();
                            }
                            else {
                                echo 'No Updated available';
                            }
                            ?>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <ul class="top-navigation list-unstyled">
                                <?php
                                
                                    $menu_header=cms::nav_menu('menu_header');
                                    if (count($menu_header) > 0) {
                                        foreach ($menu_header as $menu_header_k => $menu_header_v) {
                                            echo "  <li class='dropdown inline-block'>
                                                        <a href='".carr::get($menu_header_v, 'menu_url')."'>".carr::get($menu_header_v, 'menu_name')."</a>
                                                    </li>";
                                        }
                                    }
                                    $session = Session::instance();
                                if (strlen($session->get('member_id') > 0)) {
                                ?>
                                
                                <?php
                                    }
                                ?>
                                <li>
                                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true"><?php echo clang::__("Status Order");?></a>
                                    <div class="panel dropdown-menu status-pesanan" role="menu">
                                        <div class="panel-heading"><?php echo clang::__('Check Order Status');?></div>
                                        <div class="panel-body">
                                            <form action="<?php echo curl::httpbase();?>/retrieve/invoice" id="form-status-pesanan" name="form-status-pesanan">
                                                <div class="form-group">
                                                    <label for="nomor-pesanan"><?php echo clang::__('Order Number');?></label>
                                                    <input type="text" id="nomor-pesanan" name="nomor-pesanan" class="form-control"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="email"><?php echo clang::__('Email');?></label>
                                                    <input type="text" id="email" name="email" class="form-control"/>
                                                </div>
                                                <button type="submit" class="btn btn-default"><?php echo clang::__('View');?></button>
                                            </form>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="<?php echo $url_help;?>"><?php echo clang::__("Help");?> </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div id="header" class="hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2">
                            <?php 
                            $logo = curl::base() . 'application/admin62hallfamily/' . $get_code_org . '/upload/logo/item_image/' . $item_image; 
                            // fake logo for test
//                            $logo = 'http://mybigmall.co/application/admin62hallfamily/mybigmall.co/upload/logo/item_image/logo.jpg';
                            ?>                            
                            <a href="<?php echo $url_base;?>" class="brand-logo" title="Home">
                                <img src="<?php echo $logo;?>" alt="<?php echo $store;?>"/>
                            </a>
                        </div>
                        <div class="col-md-7">
                             <?php 
                                $search_placeholder = clang::__('Search nama product')." ...";
                                if($page=="service") {
                                    $search_placeholder = clang::__('Search nama service')." ...";
                                }
                                if($page=="gold") {
                                    $search_placeholder = clang::__('Search nama gold')." ...";
                                }
                            ?>
                            <div class="main-search">
                                <?php 
                                $product_category = product_category::get_product_category_menu($page);
                                $category_selected = '';
                                $category_selected_name = 'All';
                                if (isset($_GET['category']) && strlen($_GET['category']) > 0 ) {
                                    $category_selected = $_GET['category'];
                                }
                                if (count($product_category) > 0) {
                                    foreach ($product_category as $product_category_k => $product_category_v) {
                                        if (carr::get($product_category_v, 'code') == $category_selected) {
                                            $category_selected_name = carr::get($product_category_v, 'name');
                                        }
                                    }
                                }
                                ?>
                                <form name="form-search" id="form-search-sm" target="_self" action="/search" method="GET" autocomplete="on" enctype="application/x-www-form-urlencoded">
                                    <label id="category-select">
                                        <!--<div>-->
                                            <div class="category-selected" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <div id="category-label-selected" class="label-dropdown"><?php echo $category_selected_name;?></div>
                                                <input type="hidden" name="category" id="category-code-selected" value="<?php echo $category_selected;?>">
                                            </div>
                                            <ul class="dropdown-menu dropdown-search" id="category-dropdown-select">
                                                <?php
                                                echo '<li><a data-code="" data-name="All">All</a></li>';
                                                if (count($product_category) > 0) {
                                                    foreach ($product_category as $product_category_k => $product_category_v) {
                                                        $category_name = carr::get($product_category_v, 'name');
                                                        $category_code = carr::get($product_category_v, 'code');
                                                        echo '<li><a data-code="'.$category_code.'" data-name="'.$category_name.'">'.$category_name.'</a></li>';
                                                    }
                                                }
                                                ?>
                                            </ul>
                                        <!--</div>-->
                                    </label>
                                    <label id="keyword-search">
                                        <input type="hidden" name="page" id="page" class="" value="<?php echo $page; ?>">
                                        <input type="text" placeholder="<?php echo $search_placeholder; ?>" name="keyword" id="keyword" class="validate[]" value="<?php echo chtml::specialchars($keyword); ?>">
                                    </label>
                                    <label id="submit-search">
                                        <button type="submit"><?php echo clang::__('Search');?></button>
                                    </label>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-3">
<!--                            <div id="shopping-cart" class="col-md-1 shopping-cart">

                                </div>-->
                            <?php
								
										
								$element_register=CElement_62hallmall_Register::factory('register');
								$element_register->set_trigger_button(true);
								$element_register->set_icon(FALSE);
								$element_register->set_store($store);
								$element_register->set_have_gold($have_gold);
								$element_register->set_have_service($have_service);

								$element_login=CElement_62hallmall_Login::factory('login');
								$element_login->set_trigger_button(TRUE);
								$element_login->set_icon(FALSE);
								$element_login->display_cart_icon(TRUE);

								if (member::get_data_member() == false) {
									if($have_register>0){
										$element_login->set_register_button(true);
										$element_login->set_element_register($element_register);
									}
								}
								echo $element_login->html();
								$additional_footer_js .= $element_login->js();
								?>
                        </div>
                    </div>
<!--                    <div class="row">
                        <div class="col-md-12">
                            MAIN MENU
                            <?php 
//                            $main_menu_data = template::get_main_menu($page);
//                            if ($main_menu_data != NULL) {
//                                $menu_list = carr::get($main_menu_data, 'menu_list');
//                                $list_menu = carr::get($main_menu_data, 'list_menu');
//                                
//                                $element_menu = CElement_Menu::factory('menu-62hall-'.$page.  rand(0, 5));
//                                $element_menu->set_menu($menu_list)
//                                        ->set_link($list_menu);
//                                echo $element_menu->html();
//                            }
                            ?>
                        </div>
                    </div>-->
                </div>
            </div>
            <div id="header-mobile" class="visible-xs hidden-sm hidden-lg">
                <nav class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <a class="navbar-toggle custom-navbar-toggle mobile-menu-trigger">
                                <span class="sr-only">Toggle Navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </a>
                            <a class="brand-logo" href="<?php echo $url_base;?>" title="Home">
                                <img src="<?php echo $logo;?>" alt="<?php echo $store;?>"/>
                            </a>
                            <ul class="header-mobile-nav">
                                <li>
                                    <a class="header-mobile-nav-icon search-mobile-trigger" title="Search"><i class="fa fa-search"></i></a>
                                </li>
                                <li>
                                    <a class="header-mobile-nav-icon" title="Search" href="<?php echo curl::httpbase();?>products/shoppingcart/"><i class="fa fa-shopping-cart"></i></a>
                                </li>
                                <li>
                                    <a class="header-mobile-nav-icon" title="Search" data-target="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                    <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                                        <li><a href="<?php echo curl::httpbase().'member/action/status_order'?>">Status Order</a></li>
                                        <?php if (member::get_data_member() == false) {?>
                                        <li><a href="<?php echo curl::httpbase().'member/action/register'?>">Register</a></li>
                                        <li><a href="<?php echo curl::httpbase().'member/action/login'?>">Login</a></li>
                                        <?php } else { ?>
                                        <li><a href="<?php echo curl::httpbase().'member/account'?>">Member Area</a></li>
                                        <li><a href="<?php echo curl::httpbase().'member/account/logout'?>">Logout</a></li>
                                        <?php } ?>
                                        <li><a href="<?php echo $url_help;?>">Help</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div class="side-search">
                    <div class="container">
                        <div class="col-xs-12">
                            <form name="form-search" id="form-search-xs" target="_self" action="/search" method="GET" autocomplete="on" enctype="application/x-www-form-urlencoded">
                                <div class="input-group">
                                    <input type="text" placeholder="<?php echo $search_placeholder; ?>" name="keyword" id="keyword" class="form-control validate[]" value="<?php echo chtml::specialchars($keyword); ?>">
                                    <span class="input-group-btn">
                                        <button class="btn btn-danger" type="submit"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="mobile-side-menu">
                    <?php
                    $product_category = product_category::get_product_category_menu($page);
                    ?>
                    <ul class="side-menu">
                        <?php
                        foreach ($product_category as $r_k => $r_v) {
                            $url_menu = curl::base() .'product/category/'.carr::get($r_v, 'category_id');
                            $menu_name = carr::get($r_v, 'name');
                        ?>
                        <li><a href="<?php echo $url_menu;?>" class="truncate"><?php echo $menu_name;?></a></li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="visible-xs">
                
            </div>
        </header>
        <!-- end header -->
        <!-- main content -->
        <div id="main-content">
