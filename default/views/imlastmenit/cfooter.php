<?php
    global $additional_footer_js;

    $store = NULL;
    $domain = NULL;
    $org_id = CF::org_id();
    $data_org = org::get_org($org_id);
    $db = CDatabase::instance();
    if ( !empty($data_org) ) {
        $store = $data_org->name;
        $domain = $data_org->domain;
    }

    $arr_option = array();
    $q = "
	select
		*
	from
		cms_options
	where
		org_id=" . $db->escape($org_id) . "
";
    $r = $db->query($q);
    if ( $r->count() > 0 ) {
        foreach ( $r as $row ) {
            $arr_option[$row->option_name] = $row->option_value;
        }
    }
?>
</div>
<!-- end main content -->

<!-- footer -->
<footer>
    <div class="footer-above">
        <div class="container">
            <div class="row">
                <div class="footer-col col-xs-6 col-sm-3">
                    <h3 class="footer-item-title">Customer Service</h3>
                    <?php $menu_service = cms::nav_menu('menu_footer_service'); ?>
                    <ul class="list-unstyled">
                        <?php
                            if ( count($menu_service) > 0 ) {
                                foreach ( $menu_service as $menu_service_k => $menu_service_v ) {
                                    echo '<li><a href="' . $menu_service_v['menu_url'] . '">' . $menu_service_v['menu_name'] . '</a></li>';
                                }
                            }
                        ?>
                    </ul>
                </div>
                
                <div class="footer-col col-xs-6 col-sm-3">
                    <h3 class="footer-item-title"><?php echo $store; ?></h3>
                    
                    
                    
                    
                    
                    
                    
                    <?php $menu_about = cms::nav_menu('menu_footer_about'); ?>
                    <ul class="list-unstyled">
                        <?php
                            if ( count($menu_about) > 0 ) {
                                foreach ( $menu_about as $menu_about_k => $menu_about_v ) {
                                    echo '<li><a href="' . $menu_about_v['menu_url'] . '">' . $menu_about_v['menu_name'] . '</a></li>';
                                }
                            }
                        ?>
                    </ul>
                </div>
                <div class="footer-col col-xs-6 col-sm-3">
                    <h3 class="footer-item-title"><?php echo clang::__('CONTACT US'); ?></h3>
                    <ul class="list-unstyled">
                        <?php
                        $contact_us = '';
                        $phone = '';
                        $phone_mobile = '';
                        $bbm = '';
                        $email = '';

                        if (carr::get($arr_option, 'contact_us')) {
                            $contact_us = carr::get($arr_option, 'contact_us');
                            echo '<li>
                            <div class="table-cell">' . $contact_us . '</div>
                        </li>';
                        }
                        if (carr::get($arr_option, 'contact_us_phone_1')) {
                            $phone = carr::get($arr_option, 'contact_us_phone_1');
                            $raw_phone = str_replace(array(' ', '-'), '', $phone);
                            echo '<li>
                            <div class="table-cell"><a href="tel:' . $raw_phone . '">' . $phone . '</a>';
                        }
                        if (carr::get($arr_option, 'contact_us_mobile_1')) {
                            $phone_mobile = carr::get($arr_option, 'contact_us_mobile_1');
                            $raw_phone_mobile = str_replace(array(' ', '-'), '', $phone_mobile);
                            echo '<li>
                            <div class="table-cell">
                                <a href="tel:' . $raw_phone_mobile . '">' . $phone_mobile . '</a>
                            </div>
                        </li>';
                        }
                        
                        ?>
                    </ul>
                </div>
                <div class="footer-col col-xs-6 col-sm-3">
                    <h3 class="footer-item-title"><?php echo clang::__('SUBSCRIBE TO NEWSLETTER'); ?></h3>
                    <ul class="list-unstyled subscribe">
                        Get the latest tips on makeup, facialcare and bodycare!
                        <form action="<?php echo curl::base(); ?>subscribe/user_subscribe">
                            <div class="input-group">
                                  <input type="text" name="email_subscribe" class="form-control" placeholder="Enter your email...">
                                  <span class="input-group-btn">
                                      <button class="btn btn-default" type="submit"><span class="lbl-subscribe-send">Send </span><i class="fa fa-send-o"></i></button>
                                  </span>
                            </div>
                        </form>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-below text-center">
        <div class="container">
            <div class="row row-footer-about-us">
                <div class="left-footer-bottom">
                    Copyright © <?php echo $store.' '.date('Y'); ?> All Right Reserved. Privacy Policy
                </div>
                <div class="right-footer-bottom">
                    <span>Visit Us at</span>
                    <?php
                        $media = cms_options::get('media_', CF::org_id(), 'all');
                        
                        if ( $media != NULL ) {
                            echo '<ul class="list-inline social-media-bottom">';
                            foreach ( $media as $media_k => $media_v ) {
                                $option_name = carr::get($media_v, 'option_name');
                                $option_value = carr::get($media_v, 'option_value');
                                if($option_value!=NULL){
                                echo '<li class="' . $option_name . '"><a href="' . $option_value . '" target="_blank"><div class="ico-' . $option_name . '"></div></a></li>';
                            }}
                            echo '</ul>';
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- end footer -->
<?php
    //cdbg::var_dump($additional_footer_js);
?>
<script src="<?php echo curl::base(); ?>media/js/require.js"></script>

<script type="text/javascript">
<?php
    $google_analytics_id = cms_options::get('google_analytics');
    //cdbg::var_dump($google_analytics_code);
    $google_analytics_code = "
             (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                ga('create', '" . $google_analytics_id . "', 'auto');
                ga('send', 'pageview');
            ";
    $is_google_analytics_code = preg_match("/^ua-\d{4,9}-\d{1,4}$/i", strval($google_analytics_id)) ? true : false;

    if ( $is_google_analytics_code ) {
        $error = 0;
        try {
            CJSMin::minify($google_analytics_code);
        }
        catch ( Exception $ex ) {
            $error++;
        }
        if ( $error == 0 ) {
            echo $google_analytics_code;
        }
    }
?>

    document.addEventListener('capp-started', function (customEvent) {
<?php
    echo $additional_footer_js;
?>

    });
<?php
    echo $js;
    echo $ready_client_script;
?>

    if (window) {
        window.onload = function () {
<?php
    echo $load_client_script;
?>
        }
    }
<?php echo $custom_js ?>
</script>
<?php echo $custom_footer; ?>

</body>
</html>
<?php
    if ( ccfg::get("log_request") ) {


        log62::request();
    }
?>