<?php
defined('SYSPATH') OR die('No direct access allowed.');
$url_base = curl::base();
$db = CDatabase::instance();
$org_id = CF::org_id();
$get_code_org = "default";
$get_code = '';
$fav_icon = '';
$item_image = '';
$org_name = '';
if ($org_id) {
    $get_code = cdbutils::get_row("SELECT * FROM org WHERE org_id = " . $db->escape($org_id));
    $get_code_org = $get_code->code;
    $fav_icon = $get_code->item_favicon;
    $item_image = $get_code->item_image;
    $org_name = $get_code->name;
}
$class_search = '';
if (CF::domain() == 'tokolasvegas.com') {
    $class_search = 'im-search-lv';
}
$keyword = carr::get($_GET, 'keyword');
$category = carr::get($_GET, 'category');


$controller = CF::instance();
$page = 'product';
if (method_exists($controller, 'page')) {
    $page = $controller->page();
}
$home_active = "";
if (carr::get($_GET, 'page') != null && CFRouter::$controller == 'search') {
    $page = carr::get($_GET, 'page');
}

if(CFRouter::$controller == 'home'){
    $home_active = "active";
}

if ($page != 'product') {
    $url_base = curl::base() . $page . '/home/';
}


$data_org = org::get_org($org_id);
$store = '';
$have_product = 0;
$have_gold = 0;
$have_service = 0;
$have_register = 0;
$have_deposit = 0;

if (!empty($data_org)) {
    $store = $data_org->name;
    $have_product = $data_org->have_product;
    $have_gold = $data_org->have_gold;
    $have_service = $data_org->have_service;
    $have_register = $data_org->have_register;
    $have_deposit = $data_org->have_deposit;
}
$url_site = cms::get_option('site_url', 'single', $org_id);

$page_type = "produk";
if ($page == "service") {
    $page_type = "jasa";
}
$url_help = curl::base() . 'read/page/index/belanja-di-' . cstr::sanitize($org_name);

global $additional_footer_js;
?>
<!DOCTYPE html>
<html lang="id">
    <head>
        <?php echo $additional_head; ?>
        <title><?php echo $store; ?></title>
        <meta charset="UTF-8">
        <?php
        $login_from_google = ccfg::get("login_google");
        if ($login_from_google) {
            ?>
            <meta name="google-signin-client_id" content="<?php echo ccfg::get('login_google_app_id'); ?>">
        <?php } ?>
        <link rel="icon" type="image/png" href="<?= curl::base() . 'application/adminittronmall/' . $get_code_org . '/upload/logo/item_favicon/' . $fav_icon ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
        <?php echo $head_client_script; ?>
        <script type="text/javascript">
            var g_google_login = false;
        </script>
        <!-- Facebook Pixel Code -->
        <script>
            !function (f, b, e, v, n, t, s) {
                if (f.fbq)
                    return;
                n = f.fbq = function () {
                    n.callMethod ?
                            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)
                    f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window,
                    document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1278817668863528'); // Insert your pixel ID here.
            fbq('track', 'PageView');
        </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1278817668863528&ev=PageView&noscript=1"
                   /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
</head>
<?php $theme_color = ''; ?>
<body class="<?php echo $theme_color; ?>">
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span> <i class="fa fa-bars"></i>
                </button>
                <ul class="navbar-menu-center hidden-xs">
                    <div class="bgdrop"></div>
                    <a href="#" id="lastm-search-close" class="search-btn-close"><i class="fa fa-times"></i></a>
                    <form action="<?php echo curl::base() ?>search" method="get" accept-charset="utf-8">
                    <div class="input-group">
                        <span class="input-group-btn group-btn-search">
                            <div class="btn btn-default placeholder-search" type="button"><i class="fa fa-search"></i></div>
                        </span>
                        <input type="text" id="keyword" name="keyword" value="<?php echo chtml::specialchars($keyword); ?>" class="form-control" placeholder="Search Products...">
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-search" type="submit"><i class="fa fa-search visible-xs-inline-block"></i> <span class="hidden-xs">SEARCH</span></button>
                        </span>
                    </div><!-- /input-group -->
                    </form>
                </ul>
                <ul class="navbar-menu-right">
                    <li class="visible-xs-inline-block">
                        <a href="#" id="lastm-search"><div class="icon-search">&nbsp;</div></a>
                    </li>
                    <li class="dropdown page-scroll">
                        <a href="#" id="myaccount-menu" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><div class="icon-account">&nbsp;</div><span class="label-menu visible-md visible-lg">My Account</span></a>
                      <ul class="dropdown-menu">
                            <?php
                            $element_register = CElement_IttronmallLastmenit_Register::factory('register');
                            $element_register->set_trigger_button(true);
                            $element_register->set_icon(FALSE);
                            $element_register->set_store($store);
                            $element_register->set_have_gold($have_gold);
                            $element_register->set_have_service($have_service);

                            $element_login = CElement_IttronmallLastmenit_Login::factory('login');
                            $element_login->set_trigger_button(TRUE);
                            $element_login->set_icon(FALSE);

                            if (member::get_data_member() == false) {
                                if ($have_register > 0) {
                                    $element_login->set_register_button(true);
                                    $element_login->set_element_register($element_register);
                                }
                            }
                            echo $element_login->html();
                            $additional_footer_js .= $element_login->js();
                            ?>
                      <?php
                        // if (member::get_data_member() == false) {
                        //     echo '<li><a href="#">Login</a></li>';
                        //     if ($have_register > 0) {
                        //         echo '<li><a href="#">Register</a></li>';
                        //     }
                        // }
                      ?>
                      </ul>
                    </li>
                    <li class="page-scroll">
                        <a id="shopping-cart-button" class="dropdown-toggle" href="#"><div class="icon-shopping-cart">&nbsp;</div><span class="label-menu visible-md visible-lg">Cart</span></a>
                        <div class="shoping-cart-wrapper">
                            <div id="shopping-cart" class="shopping-cart 62hall-dropdown dropdown-toggle">
                                <div class=" dropdown">         
                                    <div class=" container-shopping-cart-icon">     
                                        <a href="#" class="62hall-dropdown icon-shopping-cart margin-top-3 pull-right bold" role="button" aria-expanded="false"></a>

                                    </div>      
                                    <div class=" count font-white">0</div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar-collapse collapse navbar-menu-left" id="bs-example-navbar-collapse-1" aria-expanded="false" style="height: 1px;">
                <ul class="nav navbar-nav">
                    <li class="<?php echo $home_active; ?>" class="page-scroll">
                        <a href="<?php echo curl::base(); ?>">&nbsp;&nbsp;&nbsp; Home &nbsp;&nbsp;&nbsp;</a>
                    </li>
                    <li class="page-scroll">
                        <a href="<?php echo $url_help; ?>">Bantuan</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    <?php 
        $additional_footer_js .= "
            $('#lastm-search').click(function(){
                var search = $('.navbar-menu-center');
                search.css('cssText', 'display: block !important;');
                search.addClass('showed');
//                console.log('working');
            });
            
            $('#lastm-search-close').click(function(){
                var search = $('.navbar-menu-center');
                search.css('cssText', 'display: inherit;');
                search.removeClass('showed');
            });
        ";
    ?>
    <div id="main-content">