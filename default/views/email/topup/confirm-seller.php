<body style="margin:0;padding:0;font-family: \'Open Sans\', Arial, serif; font-weight: 400; ;">
   <div class="main-wrapper" style="width:100%;margin:0;padding:0;">
       <?php 
	   $header = CView::factory('email/header');
	   $header->org_id = $org_id;
	   echo $header->render();
       ?>
<!-- Content -->
                <div class="content" style="padding:30px;">
                        <div class="title-left" style="font-size:18px;font-weight:bold;font-family:">
                                <?php echo clang::__('Member').' '.$member_name; ?> | <?php echo $member_email; ?>
                        </div>
                        <div class="content-text">
                                <p>
                                    <?php echo clang::__('Telah ditransfer :'); ?>
                                </p>
                                <table>
                                <tr>
                                    <td width="250">
                                        <?php echo clang::__('Rekening dari');?>
                                    </td>
                                    <td>
                                        : <?php echo $payment_from; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo clang::__('Jumlah Top Up');?>
                                    </td>
                                    <td>
                                        : Rp. <?php echo ctransform::thousand_separator($nominal_real); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo clang::__('Waktu Transfer');?>
                                    </td>
                                    <td>
                                        : <?php echo $confirmed; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo clang::__('Catatan');?>
                                    </td>
                                    <td>
                                        : <?php echo $note; ?>
                                    </td>
                                </tr>
                                </table>
                        </div>
                        <br/>
                        <div class="title-left" style="font-size:18px;font-weight:bold;font-family:">
                                <?php echo clang::__('Ke Rekening Bank :');?>
                        </div>
                        <div class="content-text">
                                <table>
                                <tr>
                                    <td width="250">
                                        <?php echo clang::__('Atas Nama');?>
                                    </td>
                                    <td>
                                        : <?php echo $acc_name; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo clang::__('Bank');?>
                                    </td>
                                    <td>
                                        : <?php echo $bank_name; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo clang::__('No Rekening');?>
                                    </td>
                                    <td>
                                        : <?php echo $acc_no; ?>
                                    </td>
                                </tr>
                                </table>
                        </div>
                </div>

<?php 
    
   $footer = CView::factory('email/footer');
   $footer->member_email = $member_email;
   echo $footer->render();
    
   ?>
    </div>
</body>
