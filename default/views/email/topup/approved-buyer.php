<body style="margin:0;padding:0;font-family: \'Open Sans\', Arial, serif; font-weight: 400; ;">
   <div class="main-wrapper" style="width:100%;margin:0;padding:0;">
       <?php 
       $header = CView::factory('email/header');
	   $header->org_id = $org_id;
	   echo $header->render();
       ?>
<!-- Content -->
       <div class="content" style="padding:30px;">
                <div class="title-left" style="font-size:18px;font-weight:bold;font-family:">
                    <?php echo clang::__('Dear').' '.$member_name; ?>
                </div>
                <div class="content-text">
                        <p>
                                <?php echo clang::__('Terimakasih telah melakukan Top Up.'); ?><br/>
                                <?php echo clang::__('Saldo Anda akan bertambah otomatis. Silahkan cek saldo Anda.'); ?>
                        </p>
                </div>
        </div>

<?php 
    
   $footer = CView::factory('email/footer');
   $footer->member_email = $member_email;
   echo $footer->render();
    
   ?>
    </div>
</body>