<body style="margin:0;padding:0;font-family: \'Open Sans\', Arial, serif; font-weight: 400; ;">
   <div class="main-wrapper" style="width:100%;margin:0;padding:0;">
       <?php
          $header = CView::factory('email/header');
          $header->org_id = $org_id;
          $data_org = org::get_org($org_id);
          $logo = curl::httpbase() . 'application/admin62hallfamily/' . $data_org->code . '/upload/logo/item_image/' . $data_org->item_image;
          echo $header->render();
        ?>
<!-- Content -->
        <div class="content" style="padding:30px;">
          <?php
            foreach ($voucher as $key => $value) {
              $logo_vendor = cobj::get($value,'logo_merchant');
              $product_id = cobj::get($value,'product_id');
          ?>
            <table style="border-collapse: collapse;margin-bottom:20px;border:solid 1px rgba(0,0,0,0.8);width:100%;">
              <tr>
                <th style="padding:15px;width:15%;height:90px;text-align:left;"><img src="<?php echo $logo_vendor; ?>" style="width:100%;"></th>
                <th style="padding:15px;text-align: left;">Merchant :<br><?php echo cobj::get($value,'vendor_name',''); ?><br><?php echo $value->vendor_address; ?></th>
                <th style="padding:15px;width:15%;"><img src="<?php echo $logo; ?>" style="width: 80px;height:auto;"></th>
              </tr>
              <tr>
                <td colspan="2" style="padding:15px;"><span style="font-weight: bold;font-style: italic;font-size:18px;margin-left: 20px;"><?php echo cobj::get($value,'product_name');?><br/></span><span style="margin-left: 20px;"><?php echo $value->description ?></span>
                <br/>
                <div style="text-align: center;font-weight: bold;">Terms and Condition</div>
                <br/>
                <div style="width:100%;padding:0px 20px;">
                  <?php
                    $db = CDatabase::instance();
                    $q = 'select * from product_term_voucher where status > 0 and product_id ='.$db->escape('192338');
                    $data_terms = $db->query($q);
                    if(count($data_terms) > 0){
                      echo '<ul>';
                      foreach ($data_terms as $key_terms => $value_terms) {
                        echo '<li style="text-align:justify;line-height:1.8em;">'.$value_terms->term_and_condition.'</li>';
                      }
                      echo '</ul>';
                    }
                  ?>
                </div>
                </td>
                <td style="font-weight: bold;text-align: center;vertical-align:top;">
                  <?php echo $value->voucher_code.'<br/>';
                    $expired = cobj::get($transaction,'expired');
                    if($expired != null){
                      echo '<p style="font-weight:normal;">Tanggal Kadaluarsa : </p>';
                      echo '<p style="font-weight:bold;color:red;">'.date('d-M-Y H:i',strtotime($expired)).'</p>';
                    }
                  ?>
                </td>
              </tr>
            </table>
          <?php
          }
          ?>
        </div>
        <?php
          $footer = CView::factory('email/footer');
          $footer->member_email = $member_email;
          $footer->org_id = $org_id;
          echo $footer->render();
        ?>
    </div>
</body>
