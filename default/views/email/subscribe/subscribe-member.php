<body style="margin:0;padding:0;font-family: \'Open Sans\', Arial, serif; font-weight: 400; ;">
    <div class="main-wrapper" style="margin:0;padding:0;">
        <?php
        // routing theme
        $theme = CF::theme();
        
        $view_header_path = 'email/header';
        if(CView::exists($theme . '/email/header')) {
            $view_header_path = $theme . '/email/header';
        }
        $view_footer_path = 'email/footer';
        if(CView::exists($theme . '/email/footer')) {
            $view_footer_path = $theme . '/email/footer';
        }
        $header = CView::factory($view_header_path);
        $header->org_id = $org_id;
        echo $header->render();

        $data_org = org::get_org($org_id);
        ?>
        <!-- Content -->
        <div class="content" style="padding:30px;font-size:14px;">
            <div class="title-left" style="font-size:18px;">
                <?php echo clang::__('Hi') . ' ' . $member_email; ?>,
            </div>
            <div class="content-text" style="line-height:20px;">
                <p>
                    <?php echo clang::__('Terimakasih telah bergabung dengan Newsletter kami. Dapatkan info dan promo menarik dari website kami.'); ?>
                </p>
                <p>
                    <?php echo clang::__("Ayo, kunjungi") . ' <a href="' . $data_org->domain . '">' . $data_org->name . '</a> ' . clang::__('sekarang juga!'); ?>
                    <br>
                    <?php echo clang::__("Temukan barang yang Anda cari dengan harga yang menarik."); ?>
                </p>
            </div>
        </div>

        <?php
        $footer = CView::factory($view_footer_path);
        $footer->member_email = $member_email;
        $footer->org_id = CF::org_id();
        $footer->sosmed = array();
        echo $footer->render();
        ?>
    </div>
</body>
