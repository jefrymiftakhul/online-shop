<?php
$mobile_phone = cdbutils::get_value('select option_value from cms_options where option_name = "contact_us_mobile_1" and org_id = '.$org_id);
?>
<body style="margin:0;padding:0;font-family: \'Open Sans\', Arial, serif; font-weight: 400; ;">
   <div class="main-wrapper" style="width:100%;margin:0;padding:0;">
       <?php 
            $header = CView::factory('email/header');
			$header->org_id = $org_id;
			echo $header->render();
       ?>
<!-- Content -->
    <div class="content" style="padding:38px 25px; margin:22px 25px; background: #fff">
        <div style="color:#6d6e71; font-size:15px;">
            <p style="margin:0px; font-size:21px;color:#3d3d3d"><b>Selamat Bergabung</b></p>
            <p style="margin-top:38px"><label style="color:#3d3d3d">Hi <?php echo $member_name;?>,</label></p>
            <p>Akun Anda telah aktif. Anda mendaftar menggunakan </p>
            <p style="margin-top:38px"> Terima kasih! Kami informasikan bahwa Anda akan langsung menerima informasi</p>
            <p> produk-produk terbaru serta penawaran eksklusif dan spesial untuk member.</p>
            <p> jika Anda memiliki pertanyaan lebih lanjut, hubungi kami melalui email:</p>
            <p> <label style="color:#27a8df;">info@<?php echo $org_name; ?>.com</label> atau telepon: <?php echo $mobile_phone ;?>(Hanya selama jam kerja)</p>
        </div> 
    </div>

    <?php 
   $footer = CView::factory('email/footer');
   $footer->member_email = $member_email;
   $footer->org_id = $org_id;
   $footer->unsublink = null;
   echo $footer->render(); 
   ?>
    </div>
</body>
