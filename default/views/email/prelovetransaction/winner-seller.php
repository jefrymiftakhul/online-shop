 <body style="margin:0;padding:0;font-family: \'Open Sans\', Arial, serif; font-weight: 400; ;">
   <div class="main-wrapper" style="width:100%;margin:0;padding:0;">
       <?php 
			$header = CView::factory('email/header');
			$header->org_id = $org_id;
			echo $header->render();
		?>
<!-- Content -->
        <div class="content" style="padding:30px;">
            <div class="title-left" style="font-size:18px;font-weight:bold;">
                <?php echo clang::__('Dear').' '.$member_name; ?>,
            </div>
            <div class="content-text">
                <p><?php echo clang::__('Buyer');?> <?php echo $buyer_name.' '.clang::__('has been a winner of the products you offer. please immediately send delivery of products to address the Buyer.');?></p>
                    <div class="title-left" style="font-size:16px;font-weight:bold;">
                        <?php echo clang::__('BUYER SHIPPING ADDRESS');?>
                    </div>
                <div>
                    <table>
                                <tr>
                                    <td width="250">
                                        <?php echo clang::__('Name');?>
                                    </td>
                                    <td>
                                        : <?php echo $winner_name; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo clang::__('Address');?>
                                    </td>
                                    <td>
                                        : <?php echo $winner_address; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo clang::__('Phone Number');?>
                                    </td>
                                    <td>
                                        : <?php echo $winner_phone; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo clang::__('Province');?>
                                    </td>
                                    <td>
                                        : <?php echo $winner_province; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo clang::__('City');?>
                                    </td>
                                    <td>
                                        : <?php echo $winner_city; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo clang::__('District');?>
                                    </td>
                                    <td>
                                        : <?php echo $winner_district; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo clang::__('Post Code');?>
                                    </td>
                                    <td>
                                        : <?php echo $winner_postal; ?>
                                    </td>
                                </tr>
                    </table>
                </div>
                <p>
                    <?php //echo clang::__("If you've done the shipment, please do");?> <a style="color:#F8931F;font-weight:bold;" href="<?php //echo $url_ondelivery; ?>" target="_blank"><?php //echo clang::__('SEND');?> </a> <?php //echo clang::__('on your transaction.');?>
                </p>
                <p>
                    <div class="product_wrapper" style="max-width:262px;margin:0 auto;margin-bottom:15px;border:solid 1px #d1d1d1;">
                        <img src="<?php echo $image_url; ?>" style="width:100%;height:auto;"></img>
                        <div class="detail_product" style="text-align:center;padding:5px;border-top:solid 1px #d1d1d1;color:#666;">
                            <div style="padding:10px 0;font-weight: bold;font-size:14px;letter-spacing:1px;"><?php echo $product_name; ?></div>
                            <div style="font-size:14px;letter-spacing:1px;">Last Price</div>
                            <div style="padding:10px 0;font-size:18px;color:#F8931F;font-weight:bold;"><?php echo $sell_price; ?></div>
                            <div class="button" style="background:#F8931F;color:#fff; text-align:center;max-width:350px; margin: 10px;">
                                <a href="#" style="text-decoration:none;"><div style="font-size:18px;letter-spacing:1px;color:#fff;padding:5px 0;">WINNER<br/><span style="font-size:14px;"><?php echo $member_name; ?></span></div></a>
                            </div>
                        </div>
                    </div>
                </p>
                <p>Keep up with special offers from us by following our newsletter. Thank you</p>
            </div>
        </div>

<?php 
		$footer = CView::factory('email/footer');
		$footer->member_email = $member_email;
		echo $footer->render();    
	 ?>
    </div>
</body>
