 <body style="margin:0;padding:0;font-family: \'Open Sans\', Arial, serif; font-weight: 400; ;">
   <div class="main-wrapper" style="width:100%;margin:0;padding:0;">
        <?php 
			$header = CView::factory('email/header');
			$header->org_id = $org_id;
			echo $header->render();
		?>

<!-- Content -->
        <div class="content" style="padding:30px;">
            <div class="title-left" style="font-size:18px;font-weight:bold;">
                <?php echo clang::__('Dear').' '.$member_name; ?>,
            </div>
            <div class="content-text">
                <p>
                    <?php echo clang::__('Thanks for doing offer on this product. You offered with this price :');?> Rp. <?php echo $sell_price; ?>
                </p>
                <p>
                    <div class="product_wrapper" style="max-width:250px;margin:0 auto;margin-bottom:15px;border:solid 1px #d1d1d1;">
                        <img src="<?php echo $image_url; ?>" style="width:100%;height:auto;"></img>
                        <div class="detail_product" style="text-align:center;padding:5px;border-top:solid 1px #d1d1d1;color:#666;">
                            <div style="padding:10px 0;font-size:14px;letter-spacing:1px;"><?php echo $product_name; ?></div>
                        </div>
                    </div>
                </p>
                <p>
                    <?php echo clang::__('Keep up with special offers from us by following our newsletter. Thank you');?>
                </p>
            </div>
        </div>

	 <?php 
		$footer = CView::factory('email/footer');
		$footer->member_email = $member_email;
		echo $footer->render();    
	 ?>

    </div>
</body>
