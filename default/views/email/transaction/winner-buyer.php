<body style="margin:0;padding:0;font-family: \'Open Sans\', Arial, serif; font-weight: 400; ;">
   <div class="main-wrapper" style="width:100%;margin:0;padding:0;">
       <?php 
            $header = CView::factory('email/header');
			$header->org_id = $org_id;
			echo $header->render();
       ?>
<!-- Content -->
       <div class="content" style="padding:30px;">
                <div class="title-left" style="font-size:18px;font-weight:bold;font-family:">
                    <?php echo clang::__('Dear').' '.$member_email; ?>
                </div>
                <div class="content-text">
                        <p>
                                <?php echo clang::__("Selamat anda adalah pemenang untuk product lucky day").' '.substr($product_name,16).'.' ?><br/>
                        </p>
                        <p>
                            <?php echo clang::__('kami akan mengirimkan ke alamat anda'); ?> 
                        </p>
                        <p>
                            <?php echo $member_address; ?><br>
                            <?php echo $member_districts; ?><br>
                            <?php echo $member_city.','.$member_province; ?><br>
                            <?php echo 'Kode Pos '.$member_postal_code; ?><br>
                        </p>
                        <p>
                            <?php echo clang::__('barang di terima paling lambat dalam 15 hari'); ?> <br>                                       
                        </p>
                        
                        
                        <p>
                                <?php echo clang::__('Keep up with special products from us by following our newsletter. Thank you for following us'); ?>
                        </p>
                </div>
        </div>

<?php 
   $footer = CView::factory('email/footer');
   $footer->member_email = $member_email;
   $footer->org_id = $org_id;
   echo $footer->render(); 
   ?>
    </div>
</body>
