<?php
    $db = CDatabase::instance();
?>
<body style="margin:0;padding:0;font-family: \'Open Sans\', Arial, serif; font-weight: 400; ;">
   <div class="main-wrapper" style="width:100%;margin:0;padding:0;">
       <?php
            $total=0;
            $header = CView::factory('email/header');
			$header->org_id = $org_id;
			echo $header->render();
       ?>
<!-- Content -->
    <div class="content" style="margin:40px 30px;">
        <div style="color:#6d6e71; font-size:11px;">
            <p><label style="color:#3d3d3d; font-size:14px"><b>Hi <?php echo $billing_name;?>,</b></label></p>
            <div style="font-size:14px; color: #6d6e71">
                <?php echo $message; ?>
            </div>
            <table style="margin-top:35px; font-size: 14px; color: #6d6371">
                <tr style="font-color:">
                    <td style="width:30%">
                        Kode Pesanan
                    </td>
                    <td style="width:3%">
                        :
                    </td>
                    <td style="width:67%">
                        <?php echo $booking_code; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Tanggal Pesanan
                    </td>
                    <td>
                        :
                    </td>
                    <td>
                        <?php
                            $tanggal = date('d',strtotime($date_transaction));
                            $month = date('m',strtotime($date_transaction));
                            $year = date('Y',strtotime($date_transaction));
                            $jam = date('H:i:s',strtotime($date_transaction));
                            switch ($month) {
                                case 1:
                                     $month = 'Januari';
                                    break;
                                
                                case 2:
                                     $month = 'Februari';
                                    break;
                                
                                case 3:
                                     $month = 'Maret';
                                    break;
                                
                                case 4:
                                     $month = 'April';
                                    break;
                                
                                case 5:
                                     $month = 'Mei';
                                    break;
                                
                                case 6:
                                     $month = 'Juni';
                                    break;
                                
                                case 7:
                                     $month = 'Juli';
                                    break;
                                
                                case 8:
                                     $month = 'Agustus';
                                    break;
                                
                                case 9:
                                     $month = 'September';
                                    break;
                                
                                case 10:
                                     $month = 'Oktober';
                                    break;
                                
                                case 11:
                                     $month = 'November';
                                    break;
                                
                                case 12:
                                     $month = 'Desember';
                                    break;

                                default:
                                    break;
                            }
                            $full_date = $tanggal.' '.$month.' '.$year;
                            echo $full_date;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Metode Pembayaran
                    </td>
                    <td>
                        :
                    </td>
                    <td>
                        <?php 
                            $str_payment_type = str_replace('_', ' ', $payment_type);
                            echo ucwords($str_payment_type); 
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Status Pesanan
                    </td>
                    <td>
                        :
                    </td>
                    <td>
                        <?php echo $status_transaction; ?>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" style="margin-top:35px; border: #eceff1 1px solid; width:100%;">
                <tr style="background-color: #eceff1; height:30px;" >
                    <td colspan="2" style="padding-left:10px; font-size:12px; color:#3d3d3d">
                        <b>Detail Pesanan</b>
                    </td>
                </tr>
                <tr style="background-color: #fff;" >
                    <td width="50%" style="padding:15px 10px; font-size:11px; color:#6d6e71; ">
                        <p>Kode Pesanan : <?php echo $booking_code; ?></p>
                        <p>Tanggal : <?php echo $full_date; ?></p>
                    </td>
                    <td style="padding:15px 10px; font-size:11px; color:#6d6e71; border-left:#eceff1 1px solid;">
                        <p>Email : <?php echo $billing_email; ?></p>
                        <p>Telephone : <?php echo $billing_phone; ?></p>
                    </td>
                </tr>
            </table>
            
            <table cellspacing="0" cellpadding="0" style="margin-top:15px; border: #eceff1 1px solid; width:100%;">
                <tr style="background-color: #eceff1; height:30px;" >
                    <td width="50%" style="padding-left:10px; font-size:12px; color:#3d3d3d">
                        <b>Alamat Pembayaran</b>
                    </td>
                    <td style="padding-left:10px; font-size:12px; color:#3d3d3d">
                        <b>Alamat Pengiriman</b>
                    </td>
                </tr>
                <tr style="background-color: #fff;" >
                    <td style="padding:15px 10px; font-size:11px; color:#6d6e71; ">
                        <p><?php echo $billing_name; ?></p>
                        <p><?php echo $billing_address; ?></p>
                        <p><?php echo $billing_province .' - '.$billing_city; ?></p>
                        <p><?php echo $billing_phone; ?></p>
                    </td>
                    <td style="padding:15px 10px; font-size:11px; color:#6d6e71; border-left:#eceff1 1px solid;">
                        <p><?php echo $shipping_name; ?></p>
                        <p><?php echo $shipping_address; ?></p>
                        <p><?php echo $shipping_province .' - '.$shipping_city; ?></p>
                        <p><?php echo $shipping_phone; ?></p>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" padding:15px 10px; style="margin-top:15px; border: #eceff1 1px solid; width:100%;">
                <tr style="background-color: #eceff1; height:30px;" >
                    <td width="50%" style="padding-left:10px; font-size:12px; color:#3d3d3d">
                        <b>Produk</b>
                    </td>
                    <td align="center" width="20%" style="padding-left:10px; font-size:12px; color:#3d3d3d">
                        <b>Harga</b>
                    </td>
                    <td align="center" width="10%" style="padding-left:10px; font-size:12px; color:#3d3d3d">
                        <b>Jumlah</b>
                    </td>
                    <td align="center" width="20%" style="padding-left:10px; font-size:12px; color:#3d3d3d">
                        <b>Subtotal</b>
                    </td>
                </tr>
                <?php foreach($detail as $row_td): ?>
                
                <tr style="background-color: #fff;" >
                    <td style="padding:15px 10px; font-size:11px; color:#6d6e71; ">
                        <p><label style="font-size:13px"><b><?php echo cobj::get($row_td,'product_name'); ?><b></label></p>
                        <?php
                        $is_package_product=cobj::get($row_td, 'is_package_product');
                        
                        if($is_package_product){
                            $html_attribute = '';
                            $html_attribute .= '<table class="package-attribute-table">';
                            $q_simple ="select p.name,p.product_id from transaction_detail_package as tdp inner join product as p on p.product_id=tdp.product_id where tdp.transaction_detail_id=".$db->escape($row_td->transaction_detail_id);
                            $r_simple = $db->query($q_simple);
                            foreach ($r_simple as $product_simple) {

                                $name = cobj::get($product_simple, 'name');
                                $html_attribute .= '<tr><td colspan="3"><b>'.$name.'</b></td></tr>';
                                $q2 = "select * from product_group_attribute where status>0 and product_id=".$db->escape($product_simple->product_id);
                                $r2 = $db->query($q2);
                                $attr=array();
                                foreach($r2 as $row_attr) {
                                    $cat_id = $row_attr->attribute_category_id;
                                    $attr_id = $row_attr->attribute_id;

                                    $att_cat_name = cdbutils::get_value('select name from attribute_category where attribute_category_id=' . $db->escape($cat_id));
                                    $attribute_key = attribute::get_attribute_key($attr_id);

                                    $html_attribute .= '<tr>';
                                    $html_attribute .= '    <td>' . $att_cat_name . '</td>';
                                    $html_attribute .= '    <td>&nbsp;:&nbsp;</td>';
                                    $html_attribute .= '    <td>' . $attribute_key . '</td>';
                                    $html_attribute .= '</tr>';
                                }

                            }
                            $html_attribute .= '</table>';
                            echo $html_attribute;
                        } 
                        if((!$is_package_product) && $row_td->parent_id!=null){
                            $html_attribute = '';
                            $html_attribute .= '<table class="configurable-attribute-table attribute-table">';
                            $q2 = "select * from product_group_attribute where status>0 and product_id=".$db->escape($row_td->product_id);
                            $r2 = $db->query($q2);
                            $attr=array();
                            foreach($r2 as $row_attr) {
                                $cat_id = $row_attr->attribute_category_id;
                                $attr_id = $row_attr->attribute_id;

                                $att_cat_name = cdbutils::get_value('select name from attribute_category where attribute_category_id=' . $db->escape($cat_id));
                                $attribute_key = attribute::get_attribute_key($attr_id);

                                $html_attribute .= '<tr>';
                                $html_attribute .= '    <td>' . $att_cat_name . '</td>';
                                $html_attribute .= '    <td>&nbsp;:&nbsp;</td>';
                                $html_attribute .= '    <td>' . $attribute_key . '</td>';
                                $html_attribute .= '</tr>';
                            }


                            $html_attribute .= '</table>';
                            echo $html_attribute;
                        }
                        ?>   
                    </td>
                    
                    <td align="right" style="padding:15px 10px; font-size:11px; color:#6d6e71; border-left:#eceff1 1px solid;">
                        <p><label style="font-size:11px"><?php 
                            $sell_price=cobj::get($row_td,'sell_price');
                            $sell_price_promo=cobj::get($row_td,'sell_price_promo');
                            echo '<b>Rp '.ctransform::format_currency($sell_price_promo).'</b>';
                            ?></label></p>
                        <?php if($sell_price!=$sell_price_promo): ?>
                        <p><label style="font-size:9px; text-decoration: line-through;">
                            <b>Rp. <?php echo ctransform::format_currency($sell_price)?></b>
                        </label></p>
                            
                        <?php endif; ?>
                    </td>
                    <td align="center" style="padding:15px 10px; font-size:11px; color:#6d6e71; border-left:#eceff1 1px solid;">
                        <p><?php 
                            $qty=cobj::get($row_td,'qty');
                            echo $qty;
                            ?>
                        </p>
                    </td>
                    <td align="right" style="padding:15px 10px; font-size:11px; color:#6d6e71; border-left:#eceff1 1px solid;">
                        <p><?php 
                            $subtotal=$qty*$sell_price_promo;
                            $total+=$subtotal;
                            echo 'Rp. '.ctransform::format_currency($subtotal);
                        ?></p>
                    </td>
                </tr>
                <?php endforeach;?>
                <tr style="font-size:11px; font-weight: bold; ">
                    <td style="border-top:#eceff1 1px solid">
                    </td>
                    <td style="border-top:#eceff1 1px solid;  padding:15px 10px 5px 0px">
                        <p>Subtotal</p>
                    </td>
                    <td colspan="2" align="right" style="border-top:#eceff1 1px solid; padding:15px 10px 5px 0px">
                        <p><?php echo 'Rp '.ctransform::format_currency($total); ?></p>
                    </td>
                </tr>
                <tr style="font-size:11px; font-weight: bold; ">
                    <td>
                    </td>
                    <td style="padding:5px 10px 5px 0px">
                        <p>Pengiriman</p>
                    </td>
                    <td colspan="2" align="right" style="padding:5px 10px 5px 0px">
                        <p><?php 
                            $str_total_shipping='-';
                            if($total_shipping>0){
                                $str_total_shipping='Rp '.ctransform::format_currency($total_shipping);
                            }
                            echo $str_total_shipping; 
                        ?></p>
                    </td>
                </tr>
                <tr style="font-size:11px; font-weight: bold">
                    <td>
                    </td>
                    <td colspan="3" style="padding:5px 0px">
                        <p><hr></p>
                    </td>
                </tr>
                <tr style="font-size:11px; font-weight: bold">
                    <td>
                    </td>
                    <td style="padding:5px 10px 15px 0px">
                        <p>Total Belanja Anda</p>
                    </td>
                    <td colspan="2" align="right" style="padding:5px 10px 15px 0px">
                        <p><label style="font-size:16px; color:#ed7217"><?php 
                            $grand_total=$total+$total_shipping-$voucher_amount;
                            echo 'Rp '.ctransform::format_currency($grand_total);
                            ?></label></p>
                    </td>
                </tr>
            </table>
            <?php if(!isset($request)): ?>
            <table cellspacing="0" cellpadding="0" style="margin-top:15px; border: #eceff1 1px solid; width:100%;">
                <tr style="background-color: #eceff1; height:30px;" >
                    <td width="50%" style="padding-left:10px; font-size:12px; color:#3d3d3d">
                        <b>Metode Pembayaran</b>
                    </td>
                    <td style="padding-left:10px; font-size:12px; color:#3d3d3d">
                        <b>Status Pembayaran</b>
                    </td>
                </tr>
                <tr style="background-color: #fff;" >
                    <td width="50%" style="padding:15px 10px; font-size:11px; color:#6d6e71; ">
                        <p><?php 
                            echo ucwords($str_payment_type).' '.$bank_detail;
                        ?></p>
                    </td>
                    <td style="padding:15px 10px; font-size:11px; color:#6d6e71; border-left:#eceff1 1px solid;">
                        <p><?php echo $status_payment; ?></p>
                    </td>
                </tr>
            </table>
            <?php endif;?>
        </div> 
        <?php if(isset($link_payment)): ?>
        <div style="margin-top:25px">
            <p>
                Product tersebut tersedia sesuai dengan request pesanan Anda.<br>
                Silahkan klik <a style="color:#27c1fe; text-decoration:none" href="<?php echo $link_payment?>">disini</a> untuk menyelesaikan pembayaran Anda
            </p>
        </div>
        <?php endif; ?>
        <?php if(isset($url_payment_confirmation)):?>
        <div align="right" style="margin-top:30px">
            <a style="color:#fff; background:#27c1fe; text-decoration:none; padding:8px 10px" href="<?php echo $url_payment_confirmation; ?>">Konfirmasi</a>
        </div>
        <?php endif; ?>
        <?php if($status_transaction!='PENDING') :?>
        <div style="margin-top:25px">
            Terima kasih telah berbelanja di <?php 
                $org=org::get_org($org_id);
                echo cobj::get($org,'name');
            ?>
        </div>
        <?php //if(isset($show_belanja_lagi)):?>
<!--        <div style="margin-top:25px">
            <?php //$url=cobj::get($org,'domain')?>
            <?php //if(!ccfg::get('no_web_front')): ?>
            <a style="color:#fff; background:#27c1fe; text-decoration:none; padding:8px 10px" href="<?php //echo $url; ?>">Belanja Lagi</a>
            <?php //endif; ?>
        </div>-->
        <?php //endif;?>
        <?php endif;?>
    </div>

    <?php 
   $footer = CView::factory('email/footer');
   $footer->member_email = $billing_email;
   $footer->org_id = $org_id;
   echo $footer->render(); 
   ?>
    </div>
</body>
