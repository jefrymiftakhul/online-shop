<body style="margin:0;padding:0;font-family: \'Open Sans\', Arial, serif; font-weight: 400; ;">
   <div class="main-wrapper" style="width:100%;margin:0;padding:0;">
       <?php 
            $header = CView::factory('email/header');
			$header->org_id = $org_id;
			echo $header->render();
       ?>
<!-- Content -->
    <div class="content" style="padding:38px 25px; margin:22px 25px; background: #fff">
        <div style="color:#6d6e71; font-size:15px;">
            <p style="margin:0px; font-size:18px;color:#3d3d3d"><b>Hi <?php  echo $member_name?> </b></p>
            <p style="margin-top:38px"><label style="color:#3d3d3d">Siap mengganti password Anda?</label></p>
            <p style="margin-top:38px; text-align:left"> <a style="color:#fff; background:#fe0100; text-decoration:none; padding:5px 10px" href="<?php echo $url; ?>">Ganti Sekarang</a></p>
            
            <p style="margin-top:38px"> Anda mempunyai waktu 24 jam untuk mengganti password Anda.</p>
            <p> Setelah lewat dari jangka waktu, Anda dapat meminta link yang baru.</p>
        </div> 
    </div>

    <?php 
   $footer = CView::factory('email/footer');
   $footer->member_email = $member_email;
   $footer->unsublink = null;
   $footer->org_id = $org_id;
   echo $footer->render(); 
   ?>
    </div>
</body>
