
<?php
$footer = cms_options::get_key('footer', $org_id);
$footer = str_replace('<p>', '', $footer);
$footer = str_replace('</p>', '', $footer);

$app = CApp::instance();
$domain = $app->domain();
$org=org::get_org($org_id);
$theme=cobj::get($org,'theme');
?>
<div class="footer-wrapper" style="line-height:0.7em;width: 98%">
    <div style="margin-top:10px;font-size: 14px;    color: #ffffff;
         background: #DF3F49; padding-top: 8px;line-height: 16px;text-align: center;border-top: solid 7px #b90303;">
        <div style="padding: 20px;">
            <?php 
            $sosial_media = NULL;
            $facebook = cms_options::get('media_fb');
            $twitter = cms_options::get('media_tw');
            $instagram = cms_options::get('media_ig');
            $google_plus = cms_options::get('media_gp');


            $link_facebook = NULL;
            if ($facebook) {
                $link_facebook = '<a target="_blank" href="' . $facebook . '"><img style="margin:-10px;padding: 15px"src="http://' . $domain . '/application/ittronmall/default/media/img/imlasvegas/white-fb.png' . '"/></a>';
            }
            $link_twitter = NULL;
            if ($twitter) {
                $link_twitter = '<a target="_blank" href="' . $twitter . '"><img style="margin:-10px;padding: 15px"src="http://' . $domain . '/application/ittronmall/default/media/img/imlasvegas/white-tw.png' . '"/></a>';
            }
            $link_instagram = NULL;
            if ($instagram) {
                $link_instagram = '<a target="_blank" href="' . $instagram . '"><img style="margin:-10px;padding: 15px" src="http://' . $domain . '/application/ittronmall/default/media/img/imlasvegas/white-ig.png' . '"/></a>';
            }
            $link_google_plus = NULL;
            if ($google_plus) {
                $link_google_plus = '<a target="_blank" href="' . $google_plus . '"><img style="margin:-10px;padding: 15px" src="http://' . $domain . '/application/ittronmall/default/media/img/imlasvegas/white-gp.png' . '"/></a>';
            }
            $sosmed = array($link_facebook, $link_twitter, $link_instagram, $link_google_plus);
            foreach ($sosmed as $key => $value) {
                if ($value != NULL)
                    echo $value;
            }
            ?>
        </div>

        <?php
            if(ccfg::get('no_web_front')===null ||ccfg::get('no_web_front')==0):
        ?>
        <a target="_blank" href="<?php echo 'http://' . $domain . '/read/page/index/syarat-dan-ketentuan'; ?>" style="color:#ffffff;text-decoration:none;font-weight:bold;"><?php echo clang::__('Syarat dan Ketentuan'); ?></a> | <a target="_blank" href="<?php echo 'http://' . $domain . '/read/page/index/kebijakan-privasi'; ?>" style="font-weight:bold;color:#ffffff;text-decoration:none;"><?php echo clang::__('Kebijakan Privasi') ?></a>
        <?php
            endif;
        ?>
        <p><?php echo clang::__('Email ini di kirimkan kepada') . ' <a href="mailto:' . $member_email . '" style="font-weight:bold;color:#fff;">' . $member_email; ?>.</a></p>
        <?php
            if(ccfg::get('no_web_front')===null ||ccfg::get('no_web_front')==0):
        ?>
        <p><?php echo clang::__('Jika anda membutuhkan bantuan silahkan klik') . ' <a style="color:#fff;text-decoration:none;" target="_blank" href="http://' . $domain . '/read/page/index/hubungi-kami">di sini</a>'; ?>.</p>
        <?php
            endif;
        ?>
        <hr style="margin:30px auto;margin-bottom:20px;border-style: solid;">
        <div style="padding-bottom:20px;">
            <div style="text-align: center;color: #ffffff;">
                <?php
                 $footer = cms_options::get_key('footer', $org_id);
                 $footer = str_replace('<p>', '', $footer);
                 $footer = str_replace('</p>', '', $footer);
                 echo $footer;
                 ?>
            </div>

            <div style="clear:both;"></div>
        </div>
    </div>
</div>