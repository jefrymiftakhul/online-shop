<?php
defined('SYSPATH') OR die('No direct access allowed.');

/**
 * @author: ITRodex5
 * @since:   2016-11-15 11:59:24
 * @last modified by:   ITRodex5
 * @last modified time: 2016-11-15 11:52:10
 * @filename: cheader.php
 * @filepath: C:\xampp\htdocs_pipo\application\62hallfamily\default\views\imlasvegas\cheader.php
 */
$url_base = curl::base();
$db = CDatabase::instance();
//~~
$org_id = CF::org_id();
$get_code_org = "default";
$get_code = '';
$fav_icon = '';
$item_image = '';
$org_name = '';
if ($org_id) {
    $get_code = cdbutils::get_row("SELECT * FROM org WHERE org_id = " . $db->escape($org_id));
    $get_code_org = $get_code->code;
    $fav_icon = $get_code->item_favicon;
    $item_image = $get_code->item_image;
    $org_name = $get_code->name;
}

$keyword = carr::get($_GET, 'keyword');
$category = carr::get($_GET, 'category');


$controller = CF::instance();
$page = 'product';
if (method_exists($controller, 'page')) {
    $page = $controller->page();
}
if (carr::get($_GET, 'page') != null && CFRouter::$controller == 'search') {
    $page = carr::get($_GET, 'page');
}

if ($page != 'product') {
    $url_base = curl::base() . $page . '/home/';
}


$data_org = org::get_org($org_id);
$store = '';
$have_product = 0;
$have_gold = 0;
$have_service = 0;
$have_register = 0;
$have_deposit = 0;

if (!empty($data_org)) {
    $store = $data_org->name;
    $have_product = $data_org->have_product;
    $have_gold = $data_org->have_gold;
    $have_service = $data_org->have_service;
    $have_register = $data_org->have_register;
    $have_deposit = $data_org->have_deposit;
}
$url_site = cms::get_option('site_url', 'single', $org_id);

$page_type = "produk";
if ($page == "service") {
    $page_type = "jasa";
}
$url_help = curl::base() . 'read/page/index/beli-' . $page_type . '-di-' . cstr::sanitize($org_name);


global $additional_footer_js;
$additional_footer_js .="
    $('.dropdown-transaction-status').click(function(e){
        e.stopPropagation();
    });
";
?>
<!DOCTYPE html>
<html lang="id">
    <head>
        <?php echo $additional_head; ?>
        <title><?php echo $store; ?></title>
        <meta charset="UTF-8">
        <?php
        $login_from_google = ccfg::get("login_google");
        if ($login_from_google) {
            ?>
            <meta name="google-signin-client_id" content="<?php echo ccfg::get('login_google_app_id'); ?>">
        <?php } ?>
        <link rel="icon" type="image/png" href="<?= curl::base() . 'application/adminittronmall/' . $get_code_org . '/upload/logo/item_favicon/' . $fav_icon ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
        <?php echo $head_client_script; ?>
        <script type="text/javascript">
            var g_google_login = false;
        </script>

    </head>
    <?php $theme_color = ''; ?>
    <body class="<?php echo $theme_color; ?>">
        <nav class="navbar navbar-golden">
            <div class="row row-navbar">
                <div class="col-sm-2 col-xs-12">
                    <a href="#" id="btn" class="btn ico-search-goldenrama"><i class="fa fa-search"></i></a>
                    <?php
                        $item_image = cdbutils::get_value('select item_image from org where status > 0 and org_id = '.$db->escape(CF::org_id()));
                        $logo = curl::base() . 'application/adminittronmall/'.$get_code_org.'/upload/logo/item_image/'.$item_image;
                    ?>
                    <a class="navbar-brand" href="<?php echo curl::base(); ?>">
                              <img src="<?php echo $logo; ?>" alt="<?php echo $store; ?>"/>
                    </a>
                    <ul class="nav navbar-nav shopping-cart-small">
                        <div class="shopping-cart-wrapper">
                            <div id="shopping-cart" class="shopping-cart 62hall-dropdown dropdown-toggle">

                            </div>
                        </div>
                    </ul>
                </div>
                <div class="col-xs-12 col-md-8">
                    <div class="col-xs-12 col-menu">
                        <ul class="nav navbar-nav navbar-menu-container hidden-xs">
                            <li class="active"><a href="<?php echo curl::base(); ?>">Beranda</a></li>
                            <li><a href="<?php echo curl::base().'search' ?>">Produk Kami</a></li>
                            <li><a href="<?php echo $url_help; ?>">Bantuan</a></li>
                            <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <div class="menu-list-wrap">
                                        <div class="menu-list-text">Transaksi</div>
                                        <div class="icon-trx visible-xs"></div>
                                    </div>
                                </a>
                                <div class="panel dropdown-menu dropdown-transaction-status" role="menu">
                                    <div class="panel-heading"><?php echo clang::__('Check Order Status'); ?></div>
                                    <div class="panel-body">
                                        <form action="<?php echo curl::base(); ?>retrieve/invoice" id="form-status-pesanan" name="form-status-pesanan">
                                            <div class="form-group">
                                                <label for="nomor-pesanan"><?php echo clang::__('Order Number'); ?></label>
                                                <input type="text" id="nomor-pesanan" name="nomor-pesanan" class="form-control im-field"/>
                                            </div>
                                            <div class="form-group">
                                                <label for="email"><?php echo clang::__('Email'); ?></label>
                                                <input type="text" id="email" name="email" class="form-control im-field"/>
                                            </div>
                                            <button type="submit" class="btn btn-imlasvegas btn-view-status-transaction"><?php echo clang::__('View'); ?></button>
                                        </form>
                                    </div>
                                </div>
                            </li>
                            <?php
                                $url = '';
                                $menu_service = cms::nav_menu('menu_footer_service');
                                if(count($menu_service) > 0){
                                    foreach ($menu_service as $key => $value) {
                                        if(carr::get($value,'menu_name') == 'Hubungi Kami'){
                                            $url = carr::get($value,'menu_url');
                                        }
                                    }
                                }
                            ?>
                            <li><a href="<?php echo $url; ?>">Hubungi Kami</a></li>
                        </ul>
                        <?php
                            $element_register = CElement_Register::factory('register');
                            $element_register->set_trigger_button(true);
                            $element_register->set_icon(FALSE);
                            $element_register->set_store($store);
                            $element_register->set_have_gold($have_gold);
                            $element_register->set_have_service($have_service);

                            $element_login = CElement_Login::factory('login');
                            $element_login->set_trigger_button(TRUE);
                            $element_login->set_icon(FALSE);

                            if (member::get_data_member() == false) {
                                if ($have_register > 0) {
                                    $element_login->set_register_button(true);
                                    $element_login->set_element_register($element_register);
                                }
                            }
                            echo $element_login->html();
                            $additional_footer_js .= $element_login->js();
                        ?>
                        <ul class="nav navbar-nav">
                            <div class="shopping-cart-wrapper">
                                <div id="shopping-cart-secondary" class="shopping-cart shopping-cart-secondary 62hall-dropdown dropdown-toggle">

                                </div>
                            </div>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-md-10 hidden-xs">
                        <form action="/search" class="search form">
                            <div class="input-group navbar-search-container">
                                <input type="text" class="form-control" placeholder="Search for..." value="<?php echo chtml::specialchars($keyword); ?>">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">CARI</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </nav>
        <form id="goldenrama-search-form" action="<?php echo curl::base(); ?>search" class="goldenrama-form-search">
            <div class="bg-drop"></div>
            <a id="close-search" href="#" class="fa fa-times"></a>
            <div class="input-group">
                <input class="form-control search-form" type="search" id="keyword" name="keyword" placeholder="Search" value="<?php echo chtml::specialchars($keyword); ?>">                                                                                
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                </span>
              </div><!-- /input-group -->
        </form>
        <div id="main-content">
