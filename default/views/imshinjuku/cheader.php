<?php
defined('SYSPATH') OR die('No direct access allowed.');

/**
 * @author: ITRodex5
 * @since:   2016-11-15 11:59:24
 * @last modified by:   ITRodex5
 * @last modified time: 2016-11-15 11:52:10
 * @filename: cheader.php
 * @filepath: C:\xampp\htdocs_pipo\application\62hallfamily\default\views\imlasvegas\cheader.php
 */
$url_base = curl::base();
$db = CDatabase::instance();
//~~
$org_id = CF::org_id();
$get_code_org = "default";
$get_code = '';
$fav_icon = '';
$item_image = '';
$org_name = '';
if ($org_id) {
    $get_code = cdbutils::get_row("SELECT * FROM org WHERE org_id = " . $db->escape($org_id));
    $get_code_org = $get_code->code;
    $fav_icon = $get_code->item_favicon;
    $item_image = $get_code->item_image;
    $org_name = $get_code->name;
}
$bantuan_post_key=cdbutils::get_value("
    select 
        post_name 
    from 
        cms_post 
    where 
        status>0 
        and post_name like '%belanja-di-%'
        and org_id=".$db->escape($org_id)."
");
$url_bantuan=curl::base().'read/page/index/'.$bantuan_post_key;
$keyword = carr::get($_GET, 'keyword');
$category = carr::get($_GET, 'category');


$controller = CF::instance();
$page = 'product';
if (method_exists($controller, 'page')) {
    $page = $controller->page();
}
if (carr::get($_GET, 'page') != null && CFRouter::$controller == 'search') {
    $page = carr::get($_GET, 'page');
}

if ($page != 'product') {
    $url_base = curl::base() . $page . '/home/';
}


$data_org = org::get_org($org_id);
$store = '';
$have_product = 0;
$have_gold = 0;
$have_service = 0;
$have_register = 0;
$have_deposit = 0;

if (!empty($data_org)) {
    $store = $data_org->name;
    $have_product = $data_org->have_product;
    $have_gold = $data_org->have_gold;
    $have_service = $data_org->have_service;
    $have_register = $data_org->have_register;
    $have_deposit = $data_org->have_deposit;
}
$url_site = cms::get_option('site_url', 'single', $org_id);

$page_type = "produk";
if ($page == "service") {
    $page_type = "jasa";
}
$url_help = curl::base() . 'read/page/index/beli-' . $page_type . '-di-' . cstr::sanitize($org_name);

if (CFRouter::$controller == 'home') {
    $change = 'transparent';
} else {
    $change = '';
}
global $additional_footer_js;

$additional_footer_js.="
    $('.dropdown-transaction-status').on('click',function(e){
        e.stopPropagation();
    });
    $('.main-menu-mobile .dropdown-submenu-category').on('click',function(e){
        e.stopPropagation();
        if($(this).hasClass('open')) {
            $(this).removeClass('open');
        } else {
            if ($(window).width > 767) {
                $(this).parent().find('.dropdown-submenu.dropdown-submenu-click').removeClass('open');
            }
            $(this).addClass('open');
        }

    });
";
?>
<!DOCTYPE html>
<html lang="id">
    <head>
        <?php echo $additional_head; ?>
        <title><?php echo $store; ?></title>
        <meta charset="UTF-8">
        <?php
        $login_from_google = ccfg::get("login_google");
        if ($login_from_google) {
            ?>
            <meta name="google-signin-client_id" content="<?php echo ccfg::get('login_google_app_id'); ?>">
        <?php } ?>
        <link rel="icon" type="image/png" href="<?= curl::base() . 'application/adminittronmall/' . $get_code_org . '/upload/logo/item_favicon/' . $fav_icon ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
        <?php echo $head_client_script; ?>
        <script type="text/javascript">
            var g_google_login = false;
        </script>
    </head>
    <?php $theme_color = ''; ?>
    <body class="<?php echo $theme_color; ?>">
        <header>
            <div class="main-menu-wrap navbar-wrapper shinjuku-navbar <?php echo $change; ?>">
                <div class="container">
                    <div class="menu-list-container">
                        <ul class="main-menu">
                            <li>                                
                                <div class="im-logo">
                                    <?php
                                $logo = curl::base() . 'application/adminittronmall/' . $get_code_org . '/upload/logo/item_image/' . $item_image;
                                    //$logo = curl::base() . 'application/ittronmall/default/media/img/ittronmall-shinjuku/logo_putih.png';
//                                 $logo = curl::base().'application/62hallfamily/default/media/img/imlasvegas/logo.jpg';
//                                $logo = 'https://pbs.twimg.com/profile_images/378800000738050034/ec303c3042bad062b879614225f8cb9a_400x400.jpeg';
                                    ?>
                                    <a href="<?php echo curl::base(); ?>" class="brand-logo " title="Home">
                                        <img src="<?php echo $logo; ?>" alt="<?php echo $store; ?>"/>
                                    </a>
                                </div>
                            </li>
                            <!--<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">-->
                                
                                <li class="active hidden-xs">
                                    <a href="<?php echo curl::base(); ?>">
                                        <div class="menu-list-wrap">
                                            <div class="menu-list-text">Home</div>
                                        </div>
                                    </a>
                                </li>

                                <li class="hidden-xs">
                                    <?php
                                    $product_category = product_category::get_product_category_menu('product', '', $org_id);
                                    $menu_list = array(
                                        'name' => 'Kategori',
                                        'menu' => $product_category
                                    );

                                    $menu = CElement_Menu::factory();
                                    $menu->set_menu($menu_list);
                                    $menu->set_trigger_type('click');
                                    echo $menu->html();
                                    $additional_footer_js .= $menu->js();
                                    ?>
                                </li>
                                <li class="hidden-xs">
                                    <a href="<?php echo $url_bantuan ?>">
                                        <div class="menu-list-wrap">
                                            <div class="menu-list-text">Bantuan</div>
                                        </div>
                                    </a>
                                </li>
                            <?php    

                            

                            $arr_category = array();
                            foreach ($product_category as $key => $value) {
                                //$arr_category[carr::get($value, 'code')] = carr::get($value, 'name');
                                $arr_category[carr::get($value, 'url_key')] = carr::get($value, 'name');
                            }
                            ?>
                                <!--</div>-->
                        </ul>
                        <div class="collapse navbar-collapse main-menu-mobile" id="bs-example-navbar-collapse-1">
                            <div class="container">
                                <ul class="nav navbar-nav visible-xs">
                                    <li>
                                        <form action="/search" class="search form visible-xs">
                                            <div class="form-group">
                                                <button type="submit" class="ico-search btn"></button>
                                                <input class="form-control im-input" id="keyword" name="keyword" placeholder="Search" value="<?php echo chtml::specialchars($keyword); ?>">                                                                                
                                            </div>
                                        </form>
                                    </li>
                                    <li class="active">
                                        <a href="<?php echo curl::base(); ?>">Home</a>
                                    </li>
                          
                                </li>
                                    <li class="dropdown">
                                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kategori <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <?php
                                            if (count($product_category) > 0) {
                                                $html_menu=  product_category::generate_html_menu_mobile($product_category);
                                                echo $html_menu;
                                            }
                                            ?>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="<?php echo $url_bantuan ?>">Bantuan</a>
                                    </li>
                                    <?php if (member::get_data_member() == false) : ?>
                                    <li>
                                        <a href="javascript:;" data-target="modal-body-login" class="btn-modal " data-modal="login_form" data-title="LOGIN">Masuk</a>
                                    </li>                     
                                    <li>
                                        <a href="javascript:;" class=" btn-modal-register-register " data-target="modal-body-register" data-modal="register_form" data-title="REGISTER">Daftar</a>
                                    </li>
                                <?php else: ?>
                                    <li>
                                        <a href="<?php echo curl::httpbase() ?>/member/account">My Account</a>
                                    </li>   
                                    <li>
                                        <a href="<?php echo curl::httpbase() ?>/member/account/logout">Keluar</a>
                                    </li>   

                                <?php
                                endif;
                                ?>    

                                </ul>
                            </div>
                        </div>
                        <ul class="mobile-button pull-right visible-xs">
                            <li>
                                <a class="collapsed collapsed-icon" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">Collapse</a>
                            </li>
                            <li>
                                <a href="<?php echo curl::httpbase();?>products/shoppingcart" class="cart-icon">Cart</a>
                            </li>
                            <li>
                                <a href="<?php echo curl::httpbase();?>member/action/status_order" class="check-transaction-icon">Transaction</a>
                            </li>
                        </ul>

                        <ul class="main-menu main-menu-right hidden-xs">
                            <li>
                                <form action="/search" class="search form">
                                    <div class="form-group">
                                        <button type="submit" class="ico-search btn"></button>
                                        <input class="form-control im-input" id="keyword" name="keyword" placeholder="Search" value="<?php echo chtml::specialchars($keyword); ?>">                                                                                
                                    </div>
                                </form>
                            </li>
                            <li>
                                <!--                                <div id="shopping-cart" class="shopping-cart">
                                                                </div>-->
                                <div class="shoping-cart-wrapper">
                                    <div id="shopping-cart" class="shopping-cart 62hall-dropdown dropdown-toggle">

                                    </div>
                                </div>
                            </li>
                            <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <div class="menu-list-wrap">
                                        <div class="menu-list-text hidden-xs hidden-sm">Status Transaksi</div>
                                        <div class="icon-trx visible-xs visible-sm"></div>
                                    </div>
                                </a>
                                <div class="panel dropdown-menu dropdown-transaction-status" role="menu">
                                    <div class="panel-heading"><?php echo clang::__('Check Order Status'); ?></div>
                                    <div class="panel-body">
                                        <form action="<?php echo curl::base(); ?>retrieve/invoice" id="form-status-pesanan" name="form-status-pesanan">
                                            <div class="form-group">
                                                <label for="nomor-pesanan"><?php echo clang::__('Order Number'); ?></label>
                                                <input type="text" id="nomor-pesanan" name="nomor-pesanan" class="form-control im-field"/>
                                            </div>
                                            <div class="form-group">
                                                <label for="email"><?php echo clang::__('Email'); ?></label>
                                                <input type="text" id="email" name="email" class="form-control im-field"/>
                                            </div>
                                            <button type="submit" class="btn btn-imlasvegas btn-view-status-transaction"><?php echo clang::__('View'); ?></button>
                                        </form>
                                    </div>
                                </div>
                            </li>   
                            <li>
                                <?php
                                $element_register = CElement_Register::factory('register');
                                $element_register->set_trigger_button(true);
                                $element_register->set_icon(FALSE);
                                $element_register->set_store($store);
                                $element_register->set_have_gold($have_gold);
                                $element_register->set_have_service($have_service);

                                $element_login = CElement_Login::factory('login');
                                $element_login->set_trigger_button(TRUE);
                                $element_login->set_icon(FALSE);
                                
                                if (member::get_data_member() == false) {
                                    if ($have_register > 0) {
                                        $element_login->set_register_button(true);
                                        $element_login->set_element_register($element_register);
                                    }
                                }
                                echo $element_login->html();
                                $additional_footer_js .= $element_login->js();
                                $additional_footer_js .= "$('.modal').appendTo('body')";
                                ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
        <!-- main content -->
        <div id="main-content">
