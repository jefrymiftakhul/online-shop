<?php
global $additional_footer_js;

$store = NULL;
$domain = NULL;
$org_id = CF::org_id();
$data_org = org::get_org($org_id);

$db = CDatabase::instance();
if (!empty($data_org)) {
    $store = $data_org->name;
    $domain = $data_org->domain;
    $item_image = $data_org->item_image;
    $get_code_org = $data_org->code;
}
$arr_option = array();
$q = "
	select
		*
	from
		cms_options
	where
		org_id=" . $db->escape($org_id) . "
";
$r = $db->query($q);
if ($r->count() > 0) {
    foreach ($r as $row) {
        $arr_option[$row->option_name] = $row->option_value;
    }
}
?>
</div>
<!-- end main content -->
<!-- footer -->

<footer>
    <div class="footer-newslatter">
        <div class="container">
            <div class="row newsletter">
                <div class="newsletter-wrap">
                    <div class="newsletter-title">
                        NEWSLETTER
                    </div>
                    <div class="newsletter-description">
                        Daftar dan dapatkan penawaran menarik dari <?php echo $domain; ?>
                    </div>
                    <form action="/subscribe/user_subscribe" method="POST">
                        <input name="email_subscribe" type="text" class="form-control im-newsletter" placeholder="Enter Your Email">
                        <button class="btn btn-newsletter"><?php echo clang::__('REGISTER'); ?></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container">
            <div class="row menu-footer">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <h3 class="widget-title"><?php echo clang::__('LAYANAN PELANGGAN'); ?></h3>
                    <?php
                    $menu_service = cms::nav_menu('menu_footer_service');
                    ?>
                    <ul class="list-unstyled">
                        <?php
                        if (count($menu_service) > 0) {
                            foreach ($menu_service as $menu_service_k => $menu_service_v) {
                                echo '<li><a href="' . $menu_service_v['menu_url'] . '">' . $menu_service_v['menu_name'] . '</a></li>';
                            }
                        }
                        ?>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <h3 class="widget-title"><?php echo clang::__('Our Service'); ?></h3>
                    <!--<h3 class="widget-title">Customer Service</h3>-->
                    <?php
                    // $menu_service = cms::nav_menu('menu_footer_service');
                    $product_category = product_category::get_product_category_menu('product', '', $org_id);
                    $url_menu = curl::base() . 'search?keyword=&category=';
                    ?>
                    <ul class="list-unstyled">
                        <?php
                        if (count($product_category) > 0) {
                            foreach ($product_category as $menu_service_k => $menu_service_v) {
                                $url = $url_menu . $menu_service_v['url_key'];
                                echo '<li><a href="' . $url . '">' . $menu_service_v['name'] . '</a></li>';
                            }
                        }
                        ?>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <h3 class="widget-title"><?php echo $store; ?></h3>
                    <?php $menu_about = cms::nav_menu('menu_footer_about'); ?>
                    <ul class="list-unstyled">
                        <?php
                        if (count($menu_about) > 0) {
                            foreach ($menu_about as $menu_about_k => $menu_about_v) {
                                echo '<li><a href="' . $menu_about_v['menu_url'] . '">' . $menu_about_v['menu_name'] . '</a></li>';
                            }
                        }
                        ?>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <h3 class="widget-title"><?php echo clang::__('Contact Us'); ?></h3>
                    <ul class="list-unstyled">
                        <?php
                        $contact_us = '';
                        $phone = '';
                        $phone_mobile = '';
                        $bbm = '';
                        $email = '';

                        if (carr::get($arr_option, 'contact_us')) {
                            $contact_us = carr::get($arr_option, 'contact_us');
                            echo '<li class="display-table">
                           
                            <div class="table-cell">' . $contact_us . '</div>
                        </li>';
                        }
                        if (carr::get($arr_option, 'contact_us_phone_1')) {
                            $phone = carr::get($arr_option, 'contact_us_phone_1');
                            $raw_phone = str_replace(array(' ', '-'), '', $phone);
                            echo '<li class="display-table" style="margin-bottom:0px;">
                            <div class="table-cell" style="width:35px;">
                                Telp. 
                            </div>
                            :<div class="table-cell"><a href="tel:' . $raw_phone . '">' . $phone . '</a>';
                        }
//                if(carr::get($arr_option,'contact_us_mobile_1')){
//                    $phone_mobile=carr::get($arr_option,'contact_us_mobile_1');
//                    $raw_phone_mobile = str_replace(array(' ','-'), '', $phone_mobile);
//                    
//                    $br='<br>';
//                    $icon_phone = '';
//                    if(strlen(trim(carr::get($arr_option,'contact_us_phone_1'))) == 0){
//                        $icon_phone = '
//                            <li class="display-table">
//                            <div class="table-cell">
//                                <div class="ico-phone"></div>
//                            </div>
//                            <div class="table-cell">
//                            ';
//                        
//                        $br='';
//                    }
//                    echo '
//                        '.$icon_phone.'
//                        '.$br.'
//                        <a href="tel:'.$raw_phone_mobile.'">'.$phone_mobile."</a></div></li>";
//                }
                        if (carr::get($arr_option, 'contact_us_mobile_1')) {
                            $phone_mobile = carr::get($arr_option, 'contact_us_mobile_1');
                            $raw_phone_mobile = str_replace(array(' ', '-'), '', $phone_mobile);
                            echo '<li class="display-table">
                            <div class="table-cell" style="width:35px;">
                               WA  
                            </div>
                            :<div class="table-cell">
                                <a href="tel:' . $raw_phone_mobile . '">' . $phone_mobile . '</a>
                            </div>
                        </li>';
                        }
                        if (carr::get($arr_option, 'contact_us_bbm_1')) {
                            $bbm = carr::get($arr_option, 'contact_us_bbm_1');
                            echo '<li class="display-table">
                            <div class="table-cell">
                                <div class="ico-bbm"></div>
                            </div>
                            <div class="table-cell">' . $bbm . '</div>
                        </li>';
                        }
                        if (carr::get($arr_option, 'contact_us_mail_1')) {
                            $email = carr::get($arr_option, 'contact_us_mail_1');
                            echo '<li class="display-table">
                            <div class="table-cell">
                                <div class="ico-email"></div>
                            </div>
                            <div class="table-cell">' . $email . '</div>
                        </li>';
                        }
                        ?>
                    </ul>
                </div>           
            </div>


            <div class="row">
                <div class="col-md-12 menu-sosmed">
                    <div class="col-lg-7 col-md-7 col-sm-8 col-xs-12 pull-left">
                        <?php
                       $logo = curl::base() . 'application/adminittronmall/' . $get_code_org . '/upload/logo/item_image/' . $item_image;
                        // $logo = curl::base() . 'application/ittronmall/default/media/img/ittronmall-shinjuku/logo.jpg';
//                                 $logo = curl::base().'application/62hallfamily/default/media/img/imlasvegas/logo.jpg';
//                                $logo = 'https://pbs.twimg.com/profile_images/378800000738050034/ec303c3042bad062b879614225f8cb9a_400x400.jpeg';
                        ?>
                        <a href="<?php echo curl::base(); ?>" class="brand-logo-footer " title="Home">
                            <img src="<?php echo $logo; ?>" alt="<?php echo $store; ?>"/>
                        </a>
                        <div class="copyright">
                            <?php
                            $footer_cp = carr::get($arr_option, 'footer');
                            echo $footer_cp;
                            ?>
                            <!--Copyright &copy; 2017. <?php //echo $domain; ?>. All Rights Reserved. Privacy Policy-->
                        </div>
                    </div>

                    <div class="col-lg-5 col-md-5 col-xs-12 col-sm-4 pull-right">
                        <?php
                        $media = cms_options::get('media_', CF::org_id(), 'all');

                        if ($media != NULL) {
                            echo '<ul class="list-inline social-media-bottom">';
                            foreach ($media as $media_k => $media_v) {
                                $option_name = carr::get($media_v, 'option_name');
                                $option_value = carr::get($media_v, 'option_value');
                                if ($option_value != NULL) {
                                    echo '<li class="' . $option_name . '"><a href="' . $option_value . '" target="_blank"><div class="ico-' . $option_name . '"></div></a></li>';
                                }
                            }
                            echo '</ul>';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- end footer -->
<?php
//cdbg::var_dump($additional_footer_js);
?>
<script src="<?php echo curl::base(); ?>media/js/require.js"></script>

<script type="text/javascript">
<?php
$google_analytics_id = cms_options::get('google_analytics');
//cdbg::var_dump($google_analytics_code);
$google_analytics_code = "
             (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                ga('create', '" . $google_analytics_id . "', 'auto');
                ga('send', 'pageview');
            ";
$is_google_analytics_code = preg_match("/^ua-\d{4,9}-\d{1,4}$/i", strval($google_analytics_id)) ? true : false;

if ($is_google_analytics_code) {
    $error = 0;
    try {
        CJSMin::minify($google_analytics_code);
    } catch (Exception $ex) {
        $error++;
    }
    if ($error == 0) {
        echo $google_analytics_code;
    }
}
?>

    document.addEventListener('capp-started', function(customEvent) {
<?php
echo $additional_footer_js;
?>

    });
<?php
echo $js;
echo $ready_client_script;
?>

    if (window) {
        window.onload = function() {
<?php
echo $load_client_script;
?>
        }
    }
<?php echo $custom_js ?>
</script>
<?php echo $custom_footer; ?>

</body>
</html>
<?php
if (ccfg::get("log_request")) {


    log62::request();
}
?>