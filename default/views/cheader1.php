<?php
defined('SYSPATH') OR die('No direct access allowed.');
$url_base = curl::base();
$db = CDatabase::instance();
//~~
$org_id = CF::org_id();
$get_code_org = "default";
$get_code = '';
$fav_icon = '';
$item_image = '';
if ($org_id) {
    $get_code = cdbutils::get_row("SELECT * FROM org WHERE org_id = " . $db->escape($org_id));
    $get_code_org = $get_code->code;
    $fav_icon = $get_code->item_favicon;
    $item_image = $get_code->item_image;
}

$keyword = carr::get($_GET,'keyword');


$controller = CF::instance();
$page = 'product';
if(method_exists($controller,'page')) {
    $page = $controller->page();
}
if(carr::get($_GET,'page')!=null && CFRouter::$controller =='search') {
    $page = carr::get($_GET,'page');
}

if($page!='product'){
	$url_base = curl::base().$page.'/home/';
}




$data_org = org::get_org($org_id);
$store = '';
$have_product = 0;
$have_gold = 0;
$have_service = 0;
$have_register = 0;
$have_deposit = 0;

if (!empty($data_org)) {
    $store = $data_org->name;
    $have_product = $data_org->have_product;
    $have_gold = $data_org->have_gold;
    $have_service = $data_org->have_service;
    $have_register = $data_org->have_register;
    $have_deposit = $data_org->have_deposit;
}
$url_site = cms::get_option('site_url', 'single', $org_id);
global $additional_footer_js;
?>
<!DOCTYPE html>
<html style="background: white;">
    <head>
        <?php echo $additional_head; ?>
        <title><?php echo $store; ?></title>
        <meta charset="UTF-8">
        <link rel="icon" type="image/png" href="<?= curl::base() . 'application/admin62hallfamily/' . $get_code_org . '/upload/logo/item_favicon/' . $fav_icon ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
        <?php echo $head_client_script; ?>

    </head>
    <body>
        <div class="page-container-full">
            <div class="page-header-full">
                <header >
                    <div class="bg-black">
                        <div class="container font-white">


                            <div class="row">
                                <div class="col-md-4">

                                    <?php
                                    $facebook = cms_options::get_facebook();
                                    $twitter = cms_options::get_twitter();
                                    ?>

                                    <?php if ($facebook) { ?>
                                        <a href="<?= $facebook ?>"><i class="fa fa-facebook sosial-media"></i></a>
                                    <?php } ?>
                                    <?php if ($twitter) { ?>    
                                        <a href="<?= $twitter ?>"><i class="fa fa-twitter sosial-media"></i></a>
                                    <?php } ?>
                                    <?php if (!$twitter && !$facebook) { ?>    
                                        &nbsp;
                                    <?php } ?>
                                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" style="float:right">
                                        <span class="sr-only"></span>
                                        <i class="fa fa-navicon"></i>
                                    </button>
                                </div>
                                <div class="col-md-8">
                                    <div class="collapse navbar-collapse navbar-right" style="clear:both">
                                        <ul class="nav navbar-nav">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle font-white font-size12" data-toggle="dropdown" role="button" aria-expanded="false">Status Order </span></a>
                                                <ul class="dropdown-menu status-pesanan font-black" role="menu">
                                                    <li>
                                                        <form action="<?= curl::base() . 'retrieve/invoice' ?>" id="form-status-pesanan" name="form-status-pesanan" class="form-62hallfamily">
                                                            <strong class="font-red font-big">Cek Status Pesanan</strong><br>
                                                            <strong>Nomor Pesanan</strong></br>
                                                            <input style="height: 26px" type="text" id="nomor-pesanan" name="nomor-pesanan" class="font-small"/></br>
                                                            <strong>Email</strong></br>
                                                            <input style="height: 26px" type="text" id="email" name="email" class="font-small"/></br>
                                                            <button type="submit" class="btn-62hallfamily-small margin-top-10 border-3-red bg-red">LIHAT</button>
                                                        </form>
                                                    </li>
                                                </ul>
                                            </li>
                                            <!--                                    Help belum berfungsi
                                                                                <li class="dropdown">
                                                                                    <a href="<?php //echo $url_base     ?>" class="dropdown-toggle font-white font-size12" data-toggle="dropdown" role="button" aria-expanded="false">Help </a>
                                                                                </li>-->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bg-white padding">
                        <div class="container">
                            <div class="row">
                                <div id="logo" class="col-md-4">
                                    <div id="nav-side-bar" class="link">
                                        <i class="fa fa-navicon font-red"></i>
                                    </div>
                                    <?php
                                    $logo = curl::base() . 'application/admin62hallfamily/' . $get_code_org . '/upload/logo/item_image/' . $item_image;
                                    ?>
                                    <a href="<?= $url_base ?>"><img class="logo brand" src="<?= $logo ?>" alt="<?php echo $store; ?>"/></a>
                                </div>
                                <div id="search" class="col-md-5" style="padding-left:0px;">
                                    <form name="form-search" id="form-search" class="form-horizontal form-62hallfamily" target="_self" action="/search" method="GET" autocomplete="on" enctype="application/x-www-form-urlencoded">
                                        <div class=" col-md-12 col-xs-12 margin-bottom-20">									
                                            <div class=" col-md-11 col-xs-11">											
                                                    <?php 
                                                        $search_placeholder = "Cari produk ...";
                                                        if($page=="service") {
                                                            $search_placeholder = "Cari jasa ...";
                                                        }
                                                        if($page=="gold") {
                                                            $search_placeholder = "Cari gold ...";
                                                        }
                                                    ?>
                                                    <input type="hidden" name="page" id="page" class="" value="<?php echo $page; ?>">
                                                    <div class="form-group ">							
                                                    <label class="  control-label"></label>
                                                    <input type="text" placeholder="<?php echo $search_placeholder; ?>" name="keyword" id="keyword" class="input-unstyled form-control search form-control  validate[]" value="<?php echo chtml::specialchars($keyword); ?>" style="margin-top:2px !important;">
                                                </div>
                                            </div>					
                                            <div  class=" col-md-1 col-xs-1 padding-0">					
                                                <button type="submit" class="btn-62hallfamily bg-red padding-left-0 border-3-red" style="height:35px;margin-top:27px !important"><i class="fa fa-search"></i></button>
                                            </div>
                                        </div>		
                                    </form>
                                </div>
                                <div id="shopping-cart" class="col-md-1 shopping-cart">

                                </div>
                                <div class="col-md-2 col-xs-8 auth desktop">
                                    <?php
                                    $additional_footer_js = "";
									if (member::get_data_member() == false) {
										echo '<ul class="col-md-12 bg-red usermenu">';
										if($have_register>0){
											echo '<li class="non-active">';
											$element_register = CElement_Register::factory()
													->set_trigger_button(true)
													->set_icon(FALSE)
													->set_store($store)
													->set_have_gold($have_gold)
													->set_have_service($have_service);
											echo $element_register->html();
											$additional_footer_js .= $element_register->js();
											echo '</li>';
										}
										echo '<li class="active">';
										$login_element = CElement_Login::factory()
												->set_session(false)
												->set_trigger_button(TRUE)
												->set_icon(FALSE);
										echo $login_element->html();
										echo '</li>';
										echo '</ul>';
										$additional_footer_js .= $login_element->js();
									} else {
										echo '<ul class="col-md-12">';
										$login_element = CElement_Login::factory()
												->set_session(true)
												->set_trigger_button(TRUE)
												->set_icon(FALSE);
										echo $login_element->html();
										$additional_footer_js .= $login_element->js();
										echo '</ul>';
									}
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
            </div>