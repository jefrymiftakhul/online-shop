<?php

    return array(
        array(
            "name" => "dashboard",
            "label" => "Dashboard",
            "controller" => "home",
            "method" => "index",
            "icon" => "home",
        ),
        array(
            "name" => "setting_user_list",
            "label" => "User",
            "icon" => "gear",
            "subnav" => include dirname(__FILE__) . "/nav/setting_users" . EXT,
        ),
    );
    