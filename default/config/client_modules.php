<?php

return array(
    "62hallfamily" => array(
        "css" => array(
            "62hallfamily/62hallfamily.css?v=16",
            "62hallfamily/62hallfamily_2.css?v=9",
            "62hallfamily/62hallfamily-Product.css?v=9",
            "62hallfamily/62hallfamily-Gold.css?v=9",
            "62hallfamily/62hallfamily-Service.css?v=9",
            "62hallfamily/62hallfamily-Pulsa.css?v=9",
            "62hallfamily/62hallfamily-Page.css?v=9",
            "62hallfamily/prelove.css",
        ),
        "js" => array(
            "https://apis.google.com/js/platform.js"
        )
    ),
    "62hallfloor" => array(
        "css" => array(
            "62hallfloor/style.css?v=2",
        ),
    ),
    "62hallmall" => array(
        "css" => array(
            "62hallmall/style.css?v=1",
//                "62hallfamily/62hallfamily.css?v=1",
        ),
        "js" => array(
            "https://apis.google.com/js/platform.js",
        )
    ),
    "imbuilding" => array(
        "css" => array(
            "imbuilding/style.css?v=1",
        ),
        "js" => array(
            "https://apis.google.com/js/platform.js",
        )
    ),
    "Imlasvegas" => array(
        "css" => array(
            "Imlasvegas/style.css?v=3",
            "Imlasvegas/style2.css?v=2"
        ),
        "js" => array(
            "https://apis.google.com/js/platform.js",
        )
    ),
    "Imparoparo" => array(
        "css" => array(
            "Imparoparo/style.css?v=7",
            "Imparoparo/style2.css?v=7",
            "Imparoparo/paroparo.css?v=8",
        ),
        "js" => array(
            "https://apis.google.com/js/platform.js",
        )
    ),
    "imshinjuku" => array(
        "css" => array(
            "default-style.css",
            "imshinjuku/shinjuku.css?v=7",
//            "IttronmallShinjuku/style.css",
        ),
        "js" => array(
//            "https://apis.google.com/js/platform.js",
        )
    ),
    "imfitgloss" => array(
        "css" => array(
            "default-style.css",
//            "ittronmall-fitgloss/style.css?v=1",
            "imfitgloss/fitgloss.css?v=3",
        ),
        "js" => array(
            "https://apis.google.com/js/platform.js",
        ),
    ),
    "imrantaumall" => array(
        "css" => array(
            "default-style.css",
            "imrantaumall/rantaumall.css?v=3",
        ),
        "js" => array(
            "https://apis.google.com/js/platform.js",
        ),
    ),
    "imyandb" => array(
        "css" => array(
//            "default-style.css",
//            "ittronmall-fitgloss/style.css?v=1",
            "imyandb/imyandb.css?v=1",
        ),
        "js" => array(
            "https://apis.google.com/js/platform.js",
        ),
    ),
    "imgoldenrama" => array(
        "css" => array(
//            "default-style.css",
//            "Imlasvegas/style.css?v=2",
//            "Imlasvegas/style2.css?v=2",
//            "imgoldenrama/style.css",
//            "imgoldenrama/style2.css",
            "imgoldenrama/goldenrama.css",
        ),
        "js" => array(
//                "https://apis.google.com/js/platform.js",
        )
    ),
    "imlastmenit" => array(
        "css" => array(
            "imlastmenit/imlastmenit.css",
        ),
        "js" => array(
//                "https://apis.google.com/js/platform.js",
        )
    ),
    "imurbanliving" => array(
        "css" => array(
            "imurbanliving/imurbanliving.css",
        ),
    ),
    "imultimatebussiness" => array(
        "css" => array(
            "imultimatebussiness/imultimatebussiness.css",
        ),
        "js" => array(
                "https://apis.google.com/js/platform.js",
        )
    ),
    "imlivemall" => array(
        "css" => array(
            "default-style.css",
            "imlivemall/livemall.css",
            "imlivemall/style.css",
        ),
        "js" => array(
                "https://apis.google.com/js/platform.js",
        )
    ),
    "select2" => array(
        "css" => array(
            "62hallfamily-select2.css"
        ),
        "js" => array(
            "62hallfamily-select2.js"
        ),
    ),
    "jquery" => array(
        "js" => array(
            "jquery-2.1.1.min.js"
        ),
    ),
    "owl_carousel" => array(
        "css" => array("owl.carousel.css"),
        "js" => array("owl.carousel.min.js")
    ),
    "owl_carousel132" => array(
        "css" => array("owl-carousel/owl.carousel.css"),
        "js" => array("owl-carousel/owl.carousel.min.js")
    ),
    "bootstrap-3.3.5" => array(
        "css" => array(
            "bootstrap-3.3.5/bootstrap.min.css",
        ),
        "js" => array(
            "bootstrap-3.3.5/bootstrap.min.js",
        ),
        "requirements" => array("jquery"),
    ),
    "bootstrap-select" => array(
        "css" => array(
            "bootstrap-select/bootstrap-select.css",
        ),
        "js" => array(
            "bootstrap-select/bootstrap-select.js",
        ),
        "requirements" => array("bootstrap-3.3.5"),
    ),
    "font-awesome" => array(
        "css" => array(
            "font-awesome.min.css",
        ),
    ),
    "bootstrap-dropdown" => array(
        "css" => array(
            "bootstrap-dropdown.css",
        ),
        "js" => array(
            "bootstrap-dropdown.js"
        ),
        "requirements" => array("jquery", "bootstrap-3.3.5"),
    ),
    "zoomsl-3.0" => array(
        "js" => array(
            "zoomsl-3.0.min.js",
        ),
        "requirements" => array("jquery"),
    ),
    "flexslider" => array(
        "css" => array(
            "flexslider.css",
        ),
        "js" => array(
            "flexslider.min.js",
        ),
        "requirements" => array("jquery"),
    ),
    "rating" => array(
        "css" => array(
            "rating/rating.min.css",
        ),
        "js" => array(
            "rating/rating.min.js",
        ),
    ),
    "fancybox" => array(
        "css" => array(
            "jquery.fancybox.css",
        ),
        "js" => array(
            "jquery.fancybox.pack.js",
        ),
        "requirements" => array("jquery"),
    ),
    "touchspin" => array(
        "css" => array(
            "jquery-touchspin/jquery.bootstrap-touchspin.css",
        ),
        "js" => array(
            "jquery.bootstrap-touchspin.js",
        ),
        "requirements" => array("jquery", "bootstrap-3.3.5"),
    ),
    "jquery.history" => array(
        "js" => array(
            "history/jquery.history.js",
        ),
        "requirements" => array("jquery"),
    ),
    "touchswipe" => array(
        "js" => array(
            "jquery.touchSwipe.min.js",
        ),
        "requirements" => array("jquery"),
    ),
    "bootstrap-slider" => array(
        "css" => array(
            "62hallfamily-bootstrap-slider.css",
        ),
        "js" => array(
            "bootstrap-slider.js",
        ),
        "requirements" => array("jquery", "bootstrap-3.3.5"),
    ),
    "datepicker-dropdown" => array(
        //            "js" => array("datepicker-dropdown" . DS . "jquery.date-dropdowns.min.js"),
        "js" => array("datepicker-dropdown" . DS . "jquery.date-dropdowns.js?v=2"),
    ),
    "bootstrap-datepicker" => array(
        "css" => array(
            "bootstrap-3.3.5/datepicker3.css",
        ),
        "js" => array(
            "bootstrap-3.3.5/bootstrap-datepicker.js",
        //            "bootstrap-3.3.5/locales/bootstrap-datepicker.id.js?v=1",
        ),
        "requirements" => array("jquery", "bootstrap-3.3.5"),
    ),
);
