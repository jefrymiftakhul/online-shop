<?php

class email{
    public static function get_email_transaction($type_recipient = NULL, $type_transaction = NULL) {
        $org_id = CF::org_id();
        $db = CDatabase::instance();
        
        $query="
            select
                    *
            from
                    email_transaction 
            where
                    status>0 
                    and (org_id=".$db->escape($org_id)."
                    or org_id is null or org_id = 0)
        ";
        
        if($type_recipient){
            $query .= " and type_recipient =".$db->escape($type_recipient);
        }
        
        if($type_transaction){
            $query .= " and type_transaction =".$db->escape($type_transaction);
        }
        
        $data_email = array();
        $result = $db->query($query);
        if($result->count() > 0){
            foreach($result as $row){
                $data_email[] = $row->email;
            }
        }
        
        return $data_email;
    }
	private static function email_format($base_class) {
            $classname = ucfirst($base_class) . "Email";
            $file = CF::get_files('libraries', "FormatEmail/" . $classname);
            include_once $file[0];
            $tbclass = new $classname();
            return $tbclass;
        }

        public static function send_email($recipient, $data, $type = 'Forgetpassword') {
            $tbclass = self::email_format($type);
            $format = $tbclass::format($recipient, $data);
            $arr_options = array();
            $data_org = carr::get($data, 'data_org');
            $domain = cobj::get($data_org, 'domain');
            $arr_options['smtp_from'] = ccfg::get('smtp_from');
            cmail::send_smtp($recipient, $format['subject'], $format['message'], array(), array(), array(), $arr_options);
        }

        public static function send($type, $id, $options = array(), $debug = false) {
 
            $email = EmailRouter::factory($type, $id, $options);
            $email->set_debug($debug);
            return $email->send();
        }

        const __APPEND = 'append';
        const __OVERWRITE = 'overwrite';
        public static function resend($type, $email_tracker_id, $options = array(), $debug = false) {
            $email = EmailRouter::factory($type, $email_tracker_id, $options);
            $email->set_debug($debug);
            return $email->resend(carr::get($options, 'emails', array()));
        }
    
}