<?php

    class prelove {

        public static function render_breadcrumb(&$container, $arr_breadcrumb) {
            $breadcrumb_container = $container->add_div()->add_class('breadcrumb-container');
            foreach ($arr_breadcrumb as $breadcrumb_k => $breadcrumb_v) {
                $label = carr::get($breadcrumb_v, 'label');
                $current = carr::get($breadcrumb_v, 'current');
                $url = carr::get($breadcrumb_v, 'url');

                $breadcrumb = '';
                if ($breadcrumb_k > 0) {
                    $breadcrumb .= ' > ';
                }

                $class = "";
                if ($current == true) {
                    $class .= " active ";
                    $url = 'javascript:;';
                    $breadcrumb .= '<span class="' . $class . '">' . $label . '<span>';
                }
                else {
                    $class .= " os-link ";
                    $breadcrumb .= '<a href="' . $url . '" class=" ' . $class . '">' . $label . '</a>';
                }
                $breadcrumb_container->add($breadcrumb);
            }
        }

        public static function time_diff($time1, $time2 = null) {
            if ($time2 == null) {
                $time2 = time();
            }
            else {
                $time2 = strtotime($time2);
            }

            $hour = '00';
            $minute = '00';
            $second = '00';
            $time_diff = strtotime($time1) - $time2;

            $time_left = $time_diff;
            $hour = $time_diff / (60 * 60);
            $hour = floor($hour);

            $minutes = $time_diff - ($hour * 60 * 60);
            $time_left = $minutes;
            $minutes = $minutes / 60;
            $minutes = floor($minutes);

            $second = $time_left - ($minutes * 60);
            $time_left = $second;

            if ($hour < 00) {
                $hour = '00';
                $minutes = '00';
                $second = '00';
            }
            if ($minutes < 00) {
                $minutes = '00';
                $second = '00';
            }
            if ($second < 00) {
                $second = '00';
            }

            return array(
                'hour' => $hour,
                'minutes' => $minutes,
                'second' => $second
            );
        }
        public static function time_diff_day($time1, $time2 = null) {
            if ($time2 == null) {
                $time2 = time();
            }
            else {
                $time2 = strtotime($time2);
            }
            $day = '00';
            $hour = '00';
            $minute = '00';
            $second = '00';
            $time_diff = strtotime($time1) - $time2;

            $time_left = $time_diff;
            $day = $time_diff / ((60*60)*24);
            $day = sprintf('%02d',floor($day));


            $hour = $time_diff - ($day*(24*(60 * 60)));
            $time_left = $hour;
            $hour = $time_left / (60 * 60);
            $hour = floor($hour);

            $minutes = $time_left - ($hour * 60 * 60);
            $time_left = $minutes;
            $minutes = $minutes / 60;
            $minutes = floor($minutes);

            $second = $time_left - ($minutes * 60);
            $time_left = $second;

            if ($day < 00) {
                $day = '00';
                $hour = '00';
                $minutes = '00';
                $second = '00';
            }
            if ($hour < 00) {
                $hour = '00';
                $minutes = '00';
                $second = '00';
            }
            if ($minutes < 00) {
                $minutes = '00';
                $second = '00';
            }
            if ($second < 00) {
                $second = '00';
            }

            return array(
                'day' => $day,
                'hour' => $hour,
                'minutes' => $minutes,
                'second' => $second
            );
        }

        public static function country_select($options) {
            $country_id = carr::get($options, 'country_id');
            $all = carr::get($options, 'all');
            $ctrlid = carr::get($options, 'ctrlid');
            $name = carr::get($options, 'name');


            if ($name == null) $name = $ctrlid;
            $db = CDatabase::instance();
            $app = CApp::instance();
            $list = array();

            $table_name = 'country';

            $q = "select country_id,name from `" . $table_name . "` where status>0 ";

            $q.=" order by name asc";

            $list = cdbutils::get_list($q);
            if ($all) {
                $list = array("ALL" => clang::__("ALL")) + $list;
            }

            $inp = CFormInputSelect::factory($ctrlid);

            $inp->set_value($country_id)->set_list($list)->set_name($name);
            return $inp->html();
        }

        public static function province_select($options) {
            $country_id = carr::get($options, 'country_id');
            $province_id = carr::get($options, 'province_id');
            $country_required = carr::get($options, 'country_required');

            $all = carr::get($options, 'all');
            $ctrlid = carr::get($options, 'ctrlid');
            $db = CDatabase::instance();
            $app = CApp::instance();
            $list = array();
            $table_name = 'province';

            if (!($country_required && (strlen($country_id) == 0 || $country_id == "ALL"))) {

                $q = "select province_id,name from `" . $table_name . "` where status>0 ";
                if (strlen($country_id) > 0 && $country_id != "ALL") {
                    $q.=" and country_id=" . $db->escape($country_id);
                }

                $q.=" order by name asc";
                $list = cdbutils::get_list($q);
                if ($list == NULL) {
                    $list = array
                        ('NULL' => "");
                }
            }
            if ($all) {
                $list = array("ALL" => clang::__("ALL")) + $list;
            }
            $inp = CFormInputSelect::factory($ctrlid);
            $inp->set_value($province_id)->set_list($list);
            return $inp->html();
        }

        public static function city_select($options) {
            $province_id = carr::get($options, 'province_id');
            $city_id = carr::get($options, 'city_id');
            $province_required = carr::get($options, 'province_required');

            $all = carr::get($options, 'all');
            $ctrlid = carr::get($options, 'ctrlid');
            $db = CDatabase::instance();
            $app = CApp::instance();
            $list = array();
            $table_name = 'city';

            if (!($province_required && (strlen($province_id) == 0 || $province_id == "ALL"))) {

                $q = "select city_id,name from `" . $table_name . "` where status>0 ";
                if (strlen($province_id) > 0 && $province_id != "ALL") {
                    $q.=" and province_id=" . $db->escape($province_id);
                }

                $q.=" order by name asc";

                $list = cdbutils::get_list($q);
            }
            if ($all) {
                $list = array("ALL" => clang::__("ALL")) + $list;
            }
            $inp = CFormInputSelect::factory($ctrlid);
            $inp->set_value($city_id)->set_list($list);
            return $inp->html();
        }
        public static function district_select($options) {
            $city_id = carr::get($options, 'city_id');
            $district_id = carr::get($options, 'district_id');
            $city_required = carr::get($options, 'city_required');

            $all = carr::get($options, 'all');
            $ctrlid = carr::get($options, 'ctrlid');
            $db = CDatabase::instance();
            $app = CApp::instance();
            $list = array();
            $table_name = 'districts';

            if (!($city_required && (strlen($city_id) == 0 || $city_id == "ALL"))) {

                $q = "select districts_id,name from `" . $table_name . "` where status>0 ";
                if (strlen($city_id) > 0 && $city_id != "ALL") {
                    $q.=" and city_id=" . $db->escape($city_id);
                }

                $q.=" order by name asc";

                $list = cdbutils::get_list($q);
            }
            if ($all) {
                $list = array("ALL" => clang::__("ALL")) + $list;
            }
            $inp = CFormInputSelect::factory($ctrlid);
            $inp->set_value($district_id)->set_list($list);
            return $inp->html();
        }

        public static function full_date($data_date, $noday = false) {
            $day = Date("l", strtotime($data_date));
            $day = clang::__($day);
            $date = Date("d", strtotime($data_date));
            $month = Date("F", strtotime($data_date));
            $month = clang::__($month);
            $year = Date("Y", strtotime($data_date));

            if ($noday == true) {
                return $date . " " . $month . " " . $year;
            }
            else {
                return $day . ", " . $date . " " . $month . " " . $year;
            }
        }
    }
    