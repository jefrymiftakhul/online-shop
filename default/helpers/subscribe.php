<?php

class subscribe {

    public static function save($type, $request) {
        $org_id = ccfg::get('org_id');
        $db = CDatabase::instance();
        $email = carr::get($request, 'email_subscribe');
        $name = carr::get($request, 'name_subscribe');
        $member_id = carr::get($request, 'member_id', NULL);
        $is_subscribe = carr::get($request, 'is_subscribe', 0);

        $action = carr::get($request, 'action_subscribe');

        if ($action == 'page-gold') {
            $email = carr::get($request, 'email_subscribe_gold');
        }

        $ip = crequest::remote_address();
        $date = date("Y-m-d H:i:s");

        $err_code = 0;
        $message = NULL;
        $result = array();

        // check email
        $query = '
                    SELECT
                        *
                    FROM
                        subscribe
                    WHERE
                        status > 0 AND
                        org_id = ' . $db->escape($org_id) . ' AND
                        subscribe_type = ' . $db->escape($type) . ' AND
                        email = ' . $db->escape($email) . '
                    ';

        $ex = $db->query($query);
        if ($ex->count() > 0) {
            $data = array(
                'org_id' => $org_id,
                'subscribe_type' => $type,
                'email' => $email,
                'subscribe_name' => $name,
                'updated' => $date,
                'updatedby' => $email,
                'updatedip' => $ip
            );
            $update = subscribe::update($data, $email, $type);
            $err_code += $update['err_code'];
            $err_code++;
            $message = "Email sudah terdaftar";
        } else {
            $data = array(
                'org_id' => $org_id,
                'subscribe_type' => $type,
                'email' => $email,
                'subscribe_name' => $name,
                'created' => $date,
                'createdby' => $email,
                'createdip' => $ip
            );
            $insert = subscribe::insert($data);
            $err_code += $insert['err_code'];
            $message = $insert['message'];
        }

        if ($member_id) {
            $data = array(
                'org_id' => $org_id,
                'member_id' => $member_id,
                'subscribe_type' => $type,
                'is_active' => $is_subscribe,
                'email' => $email,
                'updated' => $date,
                'updatedby' => $email,
                'updatedip' => $ip,
            );
            $update = subscribe::update($data, $email, $type);
            $err_code += $update['err_code'];
            $message = $update['message'];
        }

        $result = array(
            'err_code' => $err_code,
            'message' => $message,
        );

        return $result;
    }

    public function insert($data) {
        $db = CDatabase::instance();

        $err_code = 0;
        $message = NULL;
        $db->begin();
        try {
            if ($err_code == 0) {
                $r = $db->insert('subscribe', $data);
            }
        } catch (Exception $ex) {
            $err_code++;
            $message = clang::__('Error, Please call administrator!');
            clog::write('DB Error page subscribe/insert. ' . $ex->getMessage());
            $db->rollback();
        }
        if ($err_code == 0) {
            $db->commit();
            $message = "Pendaftaran Newsletter Berhasil";
        }

        $result = array(
            'err_code' => $err_code,
            'message' => $message,
        );

        return $result;
    }

    public function update($data, $email, $type) {
        $db = CDatabase::instance();

        $err_code = 0;
        $message = NULL;
        $org_id=carr::get($data,'org_id');
        $db->begin();
        try {
            if ($err_code == 0) {
                $r = $db->update('subscribe', $data, array('org_id'=>$org_id,'email' => $email, 'subscribe_type' => $type));
            }
        } catch (Exception $ex) {
            $err_code++;
            $message = clang::__('Error, Please call administrator!');
            clog::write('DB Error page subscribe/update. ' . $ex->getMessage());
            $db->rollback();
        }
        if ($err_code == 0) {
            $db->commit();
        }

        $result = array(
            'err_code' => $err_code,
            'message' => $message,
        );

        return $result;
    }

}
