<?php


class product_category {
    
	public static function get_product_category_menu($page,$parent_id='', $org_id = '') {
		$db=CDatabase::instance();
                $org_id = CF::org_id();
		$arr_product_category=array();
		$q="
			select
				*
			from
				product_type
			where
				name=".$db->escape($page)."
		";
		$r=$db->query($q);
		if($r->count()>0){
			$q="
				select
					*
				from
					product_category				
				where
					status>0
					and product_type_id=".$db->escape($r[0]->product_type_id)."
			";
                        org::concat_query('', $q, $org_id);
			if(strlen($parent_id)>0){
				$q.="
					and parent_id=".$db->escape($parent_id)."
				";
			}
			else{
				$q.="
					and (parent_id is null or parent_id = 0)
				";
			}
            
			if (strlen($org_id) > 0) {
				$q .= ' and org_id = '.$db->escape($org_id);
			}

                        $q .= " order by lft asc";
                        
			$res=$db->query($q);
			if($res->count()>0){
				foreach($res as $row){
					$arr_product_category_sub=array();
					$arr_product_category_sub['category_id'] = $row->product_category_id;
					$arr_product_category_sub['url_key'] = $row->url_key;
					$arr_product_category_sub['code'] = $row->code;
					$arr_product_category_sub['name'] = $row->name;
					$arr_product_category_sub['image_name'] = $row->image_name;                                        
					$arr_product_category_sub['image_url'] = $row->image_url; //image::get_image_url($row->image_name);
					$arr_product_category_sub['icon'] = $row->icon;
					$arr_product_category_sub['color'] = $row->color;
					$arr_product_category_sub['subnav']=product_category::get_product_category_menu($page,$row->product_category_id);
					$arr_product_category[]=$arr_product_category_sub;
				}
			}
		}
//                die($page);
                if ($page == 'digital') {
                    $web_session = Session::instance();
                    $arr_product_category = $web_session->get('product_category_kp');
                }
		return $arr_product_category;
	}

	public static function get_all_product_category() {
		$db = CDatabase::instance();
		$q = "
			SELECT
				*
			FROM
				product_category
			WHERE
				status > 0
					AND
				org_id = " . CF::org_id();
		return $db->query($q);
	}
        
    public static function get_parent_category( $category_id){
       $db=CDatabase::instance();
        $q = "
            select
                *
            from     
                product_category
            where
                status > 0
                and product_category_id = " . $db->escape($category_id) . "
            ";
        
        return cdbutils::get_row($q);
    }
    
    public static function get_first_parent_category($category_id){
        $db=CDatabase::instance();
        $row=product_category::get_parent_category($category_id);
        $res=$row;
        $parent_id = null;
        if($row){
            $parent_id=$row->parent_id;
        }
        
        while($parent_id!=null and $parent_id!=0){
            $row=product_category::get_parent_category($parent_id);
            $res=$row;
            $parent_id=$row->parent_id;
        }
        return $res;

    }
    
    
    public static function generate_html_menu_mobile($product_category){
        $html='';
        foreach ($product_category as $pc_k => $pc_v) {
            $name = carr::get($pc_v, 'name');
            $url_key = carr::get($pc_v, 'url_key');
            $subnav=carr::get($pc_v, 'subnav');
            if(count($subnav)>0){
                $html.='<li class="dropdown-submenu dropdown-submenu-category"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">'.$name.' <span class="caret"></span></a>';
                $html.='<ul class="dropdown-menu ">';
                $html.=self::generate_html_menu_mobile($subnav);
                $html.='</ul>';
                $html.='</li>';
                
            }else{
                $html.='<li><a href="'.curl::httpbase().'search?keyword=&category='.$url_key.'">'.$name.'</a></li>';
            }
        
        }
        return $html;
    }
}
