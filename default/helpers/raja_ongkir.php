<?php

    /**
     * Description of verifyEmail
     *
     * @author Delvo
     */
    class raja_ongkir {

        public static function calculate_cost($api_key, $origin = '', $destination = '', $weight = '', $courier = 'jne', $service_type = null) {
            $curl = curl_init();
            if(strlen($weight)==0||$weight<=0){
                $weight=1;
            }
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://api.rajaongkir.com/starter/cost",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "origin=$origin&destination=$destination&weight=$weight&courier=$courier",
                CURLOPT_HTTPHEADER => array(
                    "content-type: application/x-www-form-urlencoded",
                    "key:" . $api_key,
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            $response = json_decode($response, true);
            $data = carr::get($response, 'rajaongkir');
            $data_response = carr::get($data, 'results');
            if ($service_type != null && $data_response != null) {
                $shipping_service_use = ccfg::get('shipping_service_use');
                foreach ($data_response as $data_response_k => $data_response_v) {
                    $code = carr::get($data_response_v, 'code');
                    $costs = carr::get($data_response_v, 'costs');
                    $i = 0;
                    $data_response[$data_response_k]['costs'] = array();
                    foreach ($costs as $k => $v) {
                        if (empty($shipping_service_use[$code][$v['service']]) == false) {
                            if ($service_type == $v['service']) {
                                $data_response[$data_response_k] = $v;
                            }
                        }
                    }
                }
            }
            else if ($data_response == null) {
                $status = carr::get($data, 'status');
                $code = carr::get($status, 'code');
                $desc = carr::get($status, 'description');
                return array(
                    'err_code' => $code,
                    'err_message' => $desc,
                );
            }
            $origin_details = carr::get($data, 'origin_details');
            $destination_details = carr::get($data, 'destination_details');
            $data_shipping = carr::get($data_response, 0);
//        cdbg::var_dump($response);
//        die();
//        foreach ($jne_cost as $key => $value) {
//            $data .= $value['service'].'<br>';
//        }
            $data_response[0]['origin_details'] = $origin_details;
            $data_response[0]['destination_details'] = $destination_details;
            return $data_response;
        }

        public static function update_db($api_key) {
            $db = CDatabase::instance();
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://api.rajaongkir.com/starter/city",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "content-type: application/x-www-form-urlencoded",
                    "key:" . $api_key,
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            $response = json_decode($response, true);
            $data = carr::get($response, 'rajaongkir');
            $data_response = carr::get($data, 'results');
            $origin_details = carr::get($data, 'origin_details');
            $destination_details = carr::get($data, 'destination_details');
            cdbg::var_dump($data_response);
//        die();
            foreach ($data_response as $key => $value) {
                $db->update('city', array('raja_ongkir_province_name' => $value['province']), array('raja_ongkir_city_id' => $value['city_id']));
            }
//            die('sukses');
            $data_response[0]['origin_details'] = $origin_details;
            $data_response[0]['destination_details'] = $destination_details;
            return $data_response;
        }

    }
    