<?php

class deal {

    public static function get_deal($org_id, $page, $deal_type, $options = array(), $product_category = "", $product_name = "") {
        $db = CDatabase::instance();
        $arr_deal = array();

        $limit_start = carr::get($options, 'limit_start');
        $limit_end = carr::get($options, 'limit_end');
        $sortby = carr::get($options, 'sortby');

        $options = array_merge($options, ['deal_type' => $deal_type]);

        $r = product::get_result($options);
        if ($r->count() > 0) {
            foreach ($r as $row) {
                $arr_product = product::get_product($row->product_id);
                $arr_deal[] = $arr_product;
            }
        }

        if (isset($_GET['is_debug'])) {
            cdbg::var_dump($arr_deal);
            die();
        }

        return $arr_deal;
    }

    public static function get_count($org_id, $page, $deal_type) {
        $db = CDatabase::instance();

        $options = [
            'deal_type' => $deal_type,
        ];
        $count = product::get_count($options);
        return $count;
    }

    public static function get_deal_name($org_id, $page, $deal_type, $merchant_category) {
        $db = CDatabase::instance();
        if ($org_id == null) {
            $org_id = CF::org_id();
        }
        $q = "
                select
                    cd.name
                from
                    cms_deal as cd
                    inner join cms_deal_setting as cds on cds.cms_deal_setting_id=cd.cms_deal_setting_id
                where
                    cd.status>0
                    and cd.is_active=1
                    and cds.code=" . $db->escape($deal_type) . "
                    and cds.merchant_category=" . $db->escape($merchant_category) . "
                    and cd.org_id=" . $db->escape($org_id) . "
            ";
        $res = cdbutils::get_value($q);



        return $res;
    }

}
