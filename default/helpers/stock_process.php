<?php

class stock_process {

    public static function execute($type, $id) {
        $db = CDatabase::instance();
        $class = 'stock_process_' . $type;
        $dir = dirname(__FILE__);
        $class_dir = $dir . DS . 'stock_process' . DS;
        $file = $class_dir . $class . EXT;
        if (!file_exists($file)) {
            throw new Exception('File not found:' . $file);
        }
        require_once $file;
        $data = call_user_func(array($class, 'run'), $id);

        $products = carr::get($data, 'products');
        $transaction_id = carr::get($data, 'transaction_id');
        $transaction_code = carr::get($data, 'transaction_code');
        $description = carr::get($data, 'description');
        $module_name = carr::get($data, 'module_name');
        $ref_table = carr::get($data, 'ref_table');
        $ref_id = carr::get($data, 'ref_id');
        foreach ($products as $k => $product) {
            $recalculate_data = array();
            if ($product['is_package_product'] > 0) {
      
                $q = '
                    select 
                        tdp.product_id as product_detail_id
                        ,td.product_id 
                        ,pp.qty
                        ,pp.related_package_id
                    from transaction_detail_package tdp
                        inner join transaction_detail td on tdp.transaction_detail_id = td.transaction_detail_id
                        inner join product p on tdp.product_id = p.product_id
                        inner join product_package pp on pp.related_package_id= p.parent_id and pp.product_id=td.product_id
                    where tdp.transaction_detail_id='.$db->escape(carr::get($product, 'transaction_detail_id')).'
                    ';

                $r = $db->query($q);
                foreach ($r as $row) {
                    //$recalculate_data['product_id'] = cobj::get($row, 'related_package_id');
                    //$recalculate_data['product_id'] = carr::get($product, 'product_id');
                    $recalculate_data['product_id'] = cobj::get($row, 'product_detail_id');
                    $recalculate_data['qty_in'] = carr::get($product, 'qty_in') * cobj::get($row,'qty');
                    $recalculate_data['qty_out'] = carr::get($product, 'qty_out') * cobj::get($row,'qty');
                    $recalculate_data['transaction_id'] = $transaction_id;
                    $recalculate_data['transaction_code'] = $transaction_code;
                    $recalculate_data['description'] = $description;
                    $recalculate_data['module_name'] = $module_name;
                    $recalculate_data['ref_table'] = $ref_table;
                    $recalculate_data['ref_id'] = $ref_id;
                    $result = recalculate_stock::stock($recalculate_data);
                }
              
            } 
            else {
                $recalculate_data['product_id'] = carr::get($product, 'product_id');
                $recalculate_data['qty_in'] = carr::get($product, 'qty_in');
                $recalculate_data['qty_out'] = carr::get($product, 'qty_out');
                $recalculate_data['transaction_id'] = $transaction_id;
                $recalculate_data['transaction_code'] = $transaction_code;
                $recalculate_data['description'] = $description;
                $recalculate_data['module_name'] = $module_name;
                $recalculate_data['ref_table'] = $ref_table;
                $recalculate_data['ref_id'] = $ref_id;
                $result = recalculate_stock::stock($recalculate_data);
            }
        }
    }

}
