<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Jun 10, 2016
     */
    class prelove_product {
        
        public static function get_all_category(){
            $db = CDatabase::instance();
            $q = "
                SELECT * 
                FROM product_category
                WHERE status > 0
                ";
            $r = $db->query($q);
            return $r;
        }
        
        public static function get_images($product_id){
            $db = CDatabase::instance();
            $q = '
                SELECT * 
                FROM product_image
                WHERE product_id = ' .$db->escape($product_id) .'
                    AND status > 0
                ';
            $r = $db->query($q);
            return $r;
        }
        
        public static function get_winner($product_id){
            $db = CDatabase::instance();
            $q = 'SELECT t.*, m.name as member_name, m.email as member_email
                FROM transaction t 
                INNER JOIN member m ON m.member_id = t.member_id
                WHERE t.product_id = ' .$db->escape($product_id) .' AND t.status > 0';
            $r = cdbutils::get_row($q);
            
            $return = null;
            if ($r != null) {
                $return = array();
                $return['transaction_id'] = $r->transaction_id;
                $return['member_id'] = $r->member_id;
                $return['member_name'] = $r->member_name;
                $return['member_email'] = $r->member_email;
                $return['vendor_sell_price'] = $r->vendor_sell_price;
                $return['status_transaction'] = $r->status_transaction;
            }
            
            return $return;
        }
        
        public static function get($product_id){
            $db = CDatabase::instance();
            $q = '
                SELECT p.*
                FROM product p
                
                WHERE p.product_id = ' .$db->escape($product_id) .'
                    AND p.status > 0
                ';
            $r = cdbutils::get_row($q);
            $data = array();
            if ($r != null) {
                $data['product_category_id'] = $r->product_category_id;
                $data['product_id'] = $r->product_id;
                $data['member_id'] = $r->member_id;
                $data['name'] = $r->name;
                $data['condition'] = $r->condition;
                $data['url_key'] = $r->url_key;
                $data['start_price'] = $r->start_price;
                $data['using_increment'] = $r->using_increment;
                $data['price_increment'] = $r->price_increment;
                $data['start_bid_date'] = $r->start_bid_date;
                $data['start_bid_mode'] = $r->start_bid_mode;
                $data['end_bid_date'] = $r->end_bid_date;
                $data['price_buy_it_now'] = $r->price_buy_it_now;
                $data['already_buy'] = $r->already_buy;
                $data['description'] = $r->description;
                $data['is_publish'] = $r->is_publish;
                $data['status_product'] = $r->status_product;
                $data['image_path'] = $r->file_path;
                $data['image_name'] = $r->image_name;
                $data['filename'] = $r->filename;
                $data['user_last_bid_price'] = $r->user_last_bid_price;
                $data['total_bid'] = $r->total_bid;
                $data['status_transaction'] = $r->status_transaction;
                //$data['product_image_id'] = $r->product_image_id;
                // tambahan shipping info
                $data['weight'] = $r->weight;
                $data['city_id'] = $r->city_id;
                $data['province_id'] = $r->province_id;
                $data['shipping'] = $r->shipping;
            }
            else {
                throw new Exception(clang::__('Product not found'));
            }
            return $data; 
        }
        
        public static function get_product($product_id){
            $db = CDatabase::instance();
            $q = '
                SELECT p.*, pi.image_path, pi.image_name, pi.filename
                FROM product p
                INNER JOIN product_image pi ON pi.product_image_id = p.product_image_id
                WHERE p.product_id = ' .$db->escape($product_id) .'
                    AND p.status_product = "ACTIVE" 
                    AND p.is_publish = 1 AND p.status > 0
                ';
            $r = cdbutils::get_row($q);
            $data = array();
            if ($r != null) {
                $data['product_id'] = $r->product_id;
                $data['member_id'] = $r->member_id;
                $data['name'] = $r->name;
                $data['condition'] = $r->condition;
                $data['url_key'] = $r->url_key;
                $data['start_price'] = $r->start_price;
                $data['using_increment'] = $r->using_increment;
                $data['price_increment'] = $r->price_increment;
                $data['end_bid_date'] = $r->end_bid_date;
                $data['price_buy_it_now'] = $r->price_buy_it_now;
                $data['already_buy'] = $r->already_buy;
                $data['description'] = $r->description;
                $data['is_publish'] = $r->is_publish;
                $data['status_product'] = $r->status_product;
                $data['image_path'] = $r->image_path;
                $data['image_name'] = $r->image_name;
                $data['filename'] = $r->filename;
                $data['user_last_bid_price'] = $r->user_last_bid_price;
                $data['total_bid'] = $r->total_bid;
                $data['status_transaction'] = $r->status_transaction;
            }
            else {
                throw new Exception(clang::__('Product not found'));
            }
            return $data; 
        }
        
        public function get_details($product_id, $set_as_preview = false){
            $db = CDatabase::instance();
            $q = '
                SELECT p.*, p.file_path as image_path, 
                    pr.name as province_name, c.name as city_name,
                    pc.url_key as product_category_url_key, pc.name as product_category_name
                FROM product p
                INNER JOIN product_category pc ON pc.product_category_id = p.product_category_id
                LEFT JOIN province pr ON pr.province_id = p.province_id
                LEFT JOIN city c ON c.city_id = p.city_id
                WHERE p.product_id = ' .$db->escape($product_id) .'
                    AND p.status_product = "FINISHED" AND p.status > 0
                ';
            if (!$set_as_preview) {
                $q .= " AND p.is_publish = 1 ";
            }
            $r = cdbutils::get_row($q);
            $data = array();
            if ($r != null) {
                $data['product_id'] = $r->product_id;
                $data['member_id'] = $r->member_id;
                $data['name'] = $r->name;
                $data['condition'] = $r->condition;
                $data['url_key'] = $r->url_key;
                $data['start_price'] = $r->start_price;
                $data['using_increment'] = $r->using_increment;
                $data['price_increment'] = $r->price_increment;
                $data['start_bid_date'] = $r->start_bid_date;
                $data['end_bid_date'] = $r->end_bid_date;
                $data['price_buy_it_now'] = $r->price_buy_it_now;
                $data['already_buy'] = $r->already_buy;
                $data['description'] = $r->description;
                $data['is_publish'] = $r->is_publish;
                $data['status_product'] = $r->status_product;
                $data['image_path'] = $r->image_path;
                $data['image_name'] = $r->image_name;
                $data['filename'] = $r->filename;
                $data['user_last_bid_price'] = $r->user_last_bid_price;
                $data['total_bid'] = $r->total_bid;
                $data['status_transaction'] = $r->status_transaction;
                
                $data['description'] = $r->description;
                $data['product_category_url_key'] = $r->product_category_url_key;
                $data['product_category_name'] = $r->product_category_name;
                $data['user_id_last_bid'] = $r->user_id_last_bid;
                
                $data['province_name'] = $r->province_name;
                $data['city_name'] = $r->city_name;
                // $data['member_ref_code'] = $r->member_ref_code;
                // $data['member_name'] = $r->member_name;
                // $data['member_join_date'] = $r->date_join;
                // $data['member_last_login'] = $r->last_login;
                // $data['member_image'] = $r->member_image;
                // $data['member_email'] = $r->member_email;
                $data['deposit_hold'] = $r->deposit_hold;
            }
            else {
                throw new Exception(clang::__('Product not found'));
            }
            return $data; 
        }
        
        
        const __ORD_NEWOFFER = 'new_offer';
        const __ORD_LASTOFFER = 'last_offer';
        const __ORD_AZ = 'az';
        const __ORD_ZA = 'za';
        const __ORD_PRICE_LOW_HIGH = 'low_price';
        const __ORD_PRICE_HIGH_LOW = 'high_price';
        const __ORD_OFFER_LOW_HIGH = 'less_offer';
        const __ORD_OFFER_HIGH_LOW = 'more_offer';
        const __ORD_ENDOFFER = 'end_offer'; // this order just for order END OFFER
        public static function get_order_by() {
            return array(
                self::__ORD_AZ => clang::__('A-Z'),
                self::__ORD_ZA => clang::__('Z-A'),
                self::__ORD_NEWOFFER => clang::__('New Offer'),
                self::__ORD_LASTOFFER => clang::__('Last Offer'),
                self::__ORD_PRICE_LOW_HIGH => clang::__('Lower Price'),
                self::__ORD_PRICE_HIGH_LOW => clang::__('Higher Price'),
                self::__ORD_OFFER_LOW_HIGH => clang::__('Less Offer'),
                self::__ORD_OFFER_HIGH_LOW => clang::__('More Offer'),
            );
        }
        
        const __UPCOMING_OFFER = 'upcoming';
        const __AVAILABLE_OFFER = 'avail';
        const __END_OFFER = 'end';
        public static function get_deal_types(){
            return array(
                self::__UPCOMING_OFFER => clang::__('Upcoming Offer'),
                self::__AVAILABLE_OFFER => clang::__('Available Offer'),
                self::__END_OFFER => clang::__('End Offer'),
            );
        }
        
        const __COND_NEW = 'new';
        const __COND_USED = 'used';
        public static function get_conditions(){
            return array(
                self::__COND_NEW => clang::__('New'),
                self::__COND_USED => clang::__('Used'),
            );
        }


        /**
         * 
         * @param type $options
         * <table border=1>
         *      <tr>
         *          <td>Key</td>
         *          <td>Description</td>
         *      </tr>
         *      <tr>
         *          <td>sord</td>
         *          <td>
         *              <b>ORDER BY</b>
         *              <ul>
         *                  <li>self::__ORD_AZ</li>
         *                  <li>self::__ORD_ZA</li>
         *              </ul>
         *          </td>
         *      </tr>
         * </table>
         * @return array
         */
        public static function get_results($options){
            $db = CDatabase::instance();
            
            $q_conditions = array();
            if (($product_category_id = carr::get($options, 'product_category_id')) > 0) {
                $q_conditions[] = ' p.product_category_id = ' .$db->escape($product_category_id);
            }
            if (($seller_member_id = carr::get($options, 'member_id')) > 0) {
                $q_product_seller = ' p.member_id = ' .$db->escape($seller_member_id);
                if (($except_product_id = carr::get($options, 'except_product_id')) > 0) {
                    $q_product_seller .= ' AND p.product_id <> ' .$db->escape($except_product_id);
                }
                $q_conditions[] = $q_product_seller;
            }
             
            // <editor-fold defaultstate="collapsed" desc="Query Deal Types">
            $deal_types = carr::get($options, 'deal_types', array());
            if (is_string($deal_types)) {
                $deal_types = array($deal_types);
            }
            $q_deals = array();
            foreach ($deal_types as $deal_type_key => $deal_type) {
                switch ($deal_type) {
                    case self::__AVAILABLE_OFFER:
                        $q_deals[] = " p.end_bid_date > " .$db->escape(date("Y-m-d H:i:s")) ." AND "
                                    ." p.start_bid_date <= " .$db->escape(date("Y-m-d H:i:s")) ." AND p.already_buy = '0' AND p.product_type_id = 9"; 
                        break;
                    case self::__UPCOMING_OFFER:
                        $q_deals[] = " p.start_bid_date > " .$db->escape(date("Y-m-d H:i:s")) ." AND p.product_type_id = 9 "; 
                        break;
                    case self::__END_OFFER:
                        $q_deals[] = " (p.end_bid_date <= " .$db->escape(date("Y-m-d H:i:s")) ." OR p.already_buy = '1' ) AND p.product_type_id = 9";
                        break;
                    default:
                        break;
                }
            }

            if (count($q_deals) > 0) {
                $q_conditions[] = ' (' .implode(' OR ', $q_deals) .' ) ';
            }
            // </editor-fold>
            

            // <editor-fold defaultstate="collapsed" desc="Query Product Conditions">
            $product_conditions = carr::get($options, 'conditions', array());
            $q_prod_conditions = array();
            foreach ($product_conditions as $product_cond_key => $product_condition) {
                $q_prod_conditions[] = 'p.condition = ' .$db->escape($product_condition);
            }
            if (count($q_prod_conditions) > 0) {
                $q_conditions[] = ' (' .implode(' OR ', $q_prod_conditions) .' ) ';
            }
            // </editor-fold>
             
            // Query User Search (Keyword)
            $keyword = carr::get($options, 'keyword');
            if (strlen($keyword) > 0) {
                $q_conditions[] = ' (p.name LIKE "%' .$db->escape_like($keyword) .'%" OR 
                    pc.name LIKE "%' .$db->escape_like($keyword) .'%" )
                    ';
            }
            
            $where = implode(' AND ', $q_conditions);
            if (strlen($where) > 0) {
                $where .= ' AND ';
            }
            $select = 'p.*, p.file_path as image_path,
                (CASE WHEN p.user_last_bid_price IS NULL THEN p.start_price ELSE p.user_last_bid_price END) ord_last_bid_price
                ';
            $q = "
                SELECT {select} 
                FROM product p
                INNER JOIN product_category pc ON pc.product_category_id = p.product_category_id
                WHERE " .$where ." 
                    p.status_product = 'FINISHED' 
                    AND p.is_publish = 1 AND p.status > 0
                ";
            
            // count all products before limit it
            $q_count = str_replace('{select}', 'count(*)', $q);
            $total = cdbutils::get_value($q_count);
            // add order by to query
            // <editor-fold defaultstate="collapsed" desc="ORDER BY">
            $sord = carr::get($options, 'sord');
            switch ($sord) {
                case self::__ORD_AZ:
                    $order_by = 'p.name ASC';
                    break;
                case self::__ORD_ZA:
                    $order_by = 'p.name DESC';
                    break;
                case self::__ORD_PRICE_LOW_HIGH:
                    $order_by = 'ord_last_bid_price ASC';
                    break;
                case self::__ORD_PRICE_HIGH_LOW:
                    $order_by = 'ord_last_bid_price DESC';
                    break;
                case self::__ORD_OFFER_LOW_HIGH:
                    $order_by = 'p.total_bid ASC';
                    break;
                case self::__ORD_OFFER_HIGH_LOW:
                    $order_by = 'p.total_bid DESC';
                    break;
                case self::__ORD_NEWOFFER:
//                    $order_by = 'p.start_bid_date ASC';
//                    break;
                case self::__ORD_ENDOFFER:
                    $order_by = 'p.end_bid_date DESC';
                    break;
                case self::__ORD_LASTOFFER:
                default:
                    $order_by = 'p.end_bid_date ASC';
                    break;
            }
            $q .= " ORDER BY " .$order_by;
            // </editor-fold>
            
            // add limit to query
            $limit_start = carr::get($options, 'limit_start', 0);
            $limit_end = carr::get($options, 'limit_end', 10);
            if (!is_numeric($limit_start)) {
                $limit_start = 0;
            }
            if (!is_numeric($limit_end)) {
                $limit_end = 10;
            }
            $q .= " LIMIT " .$limit_start .', ' .$limit_end;
            
            // replace query select with expected select
            $q_result = str_replace('{select}', $select, $q);
            
            $data_products = array();
            $r = $db->query($q_result);
            foreach ($r as $k => $v) {
                $data = array();
                $data['product_id'] = $v->product_id;
                $data['member_id'] = $v->member_id;
                $data['name'] = $v->name;
                $data['condition'] = $v->condition;
                $data['url_key'] = $v->url_key;
                $data['start_price'] = $v->start_price;
                $data['using_increment'] = $v->using_increment;
                $data['price_increment'] = $v->price_increment;
                $data['end_bid_date'] = $v->end_bid_date;
                $data['start_bid_date'] = $v->start_bid_date;
                $data['price_buy_it_now'] = $v->price_buy_it_now;
                $data['already_buy'] = $v->already_buy;
                $data['description'] = $v->description;
                $data['is_publish'] = $v->is_publish;
                $data['status_product'] = $v->status_product;
                $data['image_path'] = $v->image_path;
                $data['image_name'] = $v->image_name;
                $data['filename'] = $v->filename;
                $data['user_last_bid_price'] = $v->user_last_bid_price;
                $data['total_bid'] = $v->total_bid;
                $data['status_transaction'] = $v->status_transaction;
                $data_products[] = $data;
            }
            $results = array(
                'total' => $total,
                'q'  => $q_result,
                'q_count'  => $q_count,
                'options'  => $options,
                'products'  => $data_products,
            );
            if (isset($_GET['debug_q'])) {
                cdbg::var_dump($results);
            }
            return $results;
        }
        
        public static function get_suggestions($options){
            $db = CDatabase::instance();
            
            $q_conditions = array();
            
            // Query User Search (Keyword)
            $keyword = carr::get($options, 'keyword');
            if (strlen($keyword) > 0) {
                $q_conditions[] = 'p.name LIKE "%' .$db->escape_like($keyword) .'%"';
//                $q_conditions[] = ' (p.name LIKE "%' .$db->escape_like($keyword) .'%" OR 
//                    pc.name LIKE "%' .$db->escape_like($keyword) .'%" )
//                    ';
            }
            
            $where = implode(' AND ', $q_conditions);
            if (strlen($where) > 0) {
                $where .= ' AND ';
            }
            
            $select = 'p.*, pi.image_path, pi.image_name, pi.filename, 
                pc.name as product_category_name,
                (CASE WHEN p.user_last_bid_price IS NULL THEN p.start_price ELSE p.user_last_bid_price END) ord_last_bid_price
                ';
            $q_result = "
                SELECT " .$select ." 
                FROM product p
                INNER JOIN product_category pc ON pc.product_category_id = p.product_category_id
                INNER JOIN product_image pi ON pi.product_image_id = p.product_image_id
                WHERE " .$where ." 
                    p.status_product = 'ACTIVE' 
                    AND p.is_publish = 1 AND p.status > 0
                ";
            
            // add limit to query
            $limit_start = carr::get($options, 'limit_start', 0);
            $limit_end = carr::get($options, 'limit_end', 10);
            if (!is_numeric($limit_start)) {
                $limit_start = 0;
            }
            if (!is_numeric($limit_end)) {
                $limit_end = 10;
            }
            $q_result .= " LIMIT " .$limit_start .', ' .$limit_end;
            
            $data_products = array();
            $r = $db->query($q_result);
            foreach ($r as $k => $v) {
                $data = array();
                $data['value'] = $v->name;
                $data['data'] = $v->name;
                $data_products[] = $data;
            }
            
            $results = array(
                'q'  => $q_result,
                'options'  => $options,
                'products'  => $data_products,
            );
            return $results;
        }
        
        
        public static function add_user_view($product_id){
            $db = CDatabase::instance();
            $session = Session::instance();
            $member_id = $session->get('member_id');
            $ip_address = crequest::remote_address();
            $session_id = csess::session_id();
            $browser = crequest::browser();
            $browser_version = crequest::browser_version();
            $platform = crequest::platform();
            $platform_version = crequest::platform_version();
            try {
                $db->begin();
                
                $q = "SELECT product_view_id FROM product_view WHERE product_id = " .$db->escape($product_id) ." AND status > 0";
                $product_view_id = cdbutils::get_value($q);
                if ($product_view_id != null) {
                    $q_upd = "UPDATE product_view 
                        SET total_view = total_view + 1, last_view_member_id = " .$db->escape($member_id) .",
                            last_view_date = " .$db->escape(date("Y-m-d H:i:s")) .", updated = " .$db->escape(date("Y-m-d H:i:s")) ."
                        WHERE product_view_id = " .$db->escape($product_view_id) ."";
                    $db->query($q_upd);
                }
                else {
                    $product_view = array();
                    $product_view['product_id'] = $product_id;
                    $product_view['last_view_member_id'] = $member_id;
                    $product_view['last_view_date'] = date("Y-m-d H:i:s");
                    $product_view['total_view'] = '1';
                    $product_view['created'] = date("Y-m-d H:i:s");
                    $product_view['createdby'] = $member_id;
                    $product_view['updated'] = date("Y-m-d H:i:s");
                    $product_view['updatedby'] = $member_id;
                    $product_view['status'] = '1';
                    $r = $db->insert('product_view', $product_view);
                    $product_view_id = $r->insert_id();
                }
                
                $product_view_log = array();
                $product_view_log['product_view_id'] = $product_view_id;
                $product_view_log['member_id'] = $member_id;
                $product_view_log['session_id'] = $session_id;
                $product_view_log['ip_address'] = $ip_address;
                $product_view_log['user_agent'] = CF::user_agent();
                $product_view_log['browser'] = $browser;
                $product_view_log['browser_version'] = $browser_version;
                $product_view_log['platform'] = $platform;
                $product_view_log['platform_version'] = $platform_version;
                $product_view_log['view_date'] = date("Y-m-d H:i:s");
                $product_view_log['created'] = date("Y-m-d H:i:s");
                $product_view_log['createdby'] = $member_id;
                $product_view_log['updated'] = date("Y-m-d H:i:s");
                $product_view_log['updatedby'] = $member_id;
                $product_view_log['status'] = '1';
                $db->insert('product_view_log', $product_view_log);
                $db->commit();
                return true;
            }
            catch (Exception $exc) {
                $db->rollback();
                return false;
            }
            return false;
        }
    }
    