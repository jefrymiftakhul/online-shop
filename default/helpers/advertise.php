<?php


class advertise {
    
	public static function get_advertise($org_id,$page) {
		$db=CDatabase::instance();
		$arr_advertise=array();
		$org_data_found=false;
		$q_base="
			select
				ca.cms_advertise_id as cms_advertise_id,
				cad.cms_advertise_row_id as cms_advertise_row_id
			from
				cms_advertise as ca
				left join cms_advertise_setting as cas on cas.cms_advertise_setting_id=ca.cms_advertise_setting_id
				left join cms_advertise_detail as cad on cad.cms_advertise_id=ca.cms_advertise_id
			where
				ca.status>0
				and ca.is_active>0
				
				and cas.product_type=".$db->escape($page)."
		";
                if(CF::theme() =='Imlasvegas'){
                    $q_base .= ' and cas.name ="Imlasvegas" and cad.status>0 ';
                }
                else{
                    $q_base.= 'and case when cad.status is null then 1=1 else cad.status>0 end ';
                }
		$q_org=$q_base;
		if($org_id){
			$q_org.="
				and ca.org_id=".$org_id."
				order by 
					cad.priority asc
			";
			$r=$db->query($q_org);
			if($r->count()>0){
				$org_data_found=true;
			}
		}
		if(!$org_data_found){
			$q_no_org=$q_base."
				and ca.org_id is null
				order by 
					cad.priority asc
			";
			$r=$db->query($q_no_org);
		}
		if (isset($_GET['debug'])) {
			cdbg::var_dump($q_org);
			if (isset($q_no_org)) {
				cdbg::var_dump($q_no_org);
			}
		}
//                cdbg::var_dump($r);
//                die;
		if($r->count()>0){
			foreach($r as $row){
				$arr_row=array();
				$q_row="
					select
						card.cms_advertise_col_id
					from
						cms_advertise_row as car
						inner join cms_advertise_row_detail as card on card.cms_advertise_row_id=car.cms_advertise_row_id
					where
						card.status>0
						and car.status>0
						and card.cms_advertise_row_id=".$db->escape($row->cms_advertise_row_id)."
					order by
						card.priority asc
				";
				$r_row=$db->query($q_row);
				if($r_row->count()>0){
					foreach($r_row as $row_row){
						$arr_col=array();
						$q_col="
							select
								image_url,
								col_span,
                                                                url_link
							from
								cms_advertise_col
							where
								cms_advertise_col_id=".$db->escape($row_row->cms_advertise_col_id)."
						";
						$r_col=$db->query($q_col);
						if($r_col->count()>0){
							$arr_col['image_url']=$r_col[0]->image_url;
							$arr_col['col_span']=$r_col[0]->col_span;
							$arr_col['url_link']=$r_col[0]->url_link;
						}
						$arr_row[]=$arr_col;
					}
				}
				$arr_advertise[]=$arr_row;
			}
		}
                
		return $arr_advertise;
	}
}
