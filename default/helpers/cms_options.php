<?php

class cms_options {

    public static function get_key($key, $org_id = null) {
        $db = CDatabase::instance();
        if ($org_id == null) {
            $org_id = CF::org_id();
        }
        $query = "
                    select 
                        option_value
                    from 
                        cms_options
                    where
                        org_id = " . $db->escape($org_id) . "
                        and option_name = " . $db->escape($key) . "
                    ";
        $val = cdbutils::get_value($query);

        return $val;
    }

    public static function get($key, $org_id = null, $view = 'single') {
        $db = CDatabase::instance();
        if ($org_id == null) {
            $org_id = CF::org_id();
        }
        $query = "
                select 
                    option_value
                from 
                    cms_options
                where
                    org_id = " . $db->escape($org_id) . "
                    and option_name = " . $db->escape($key) . "
                ";
        $val = cdbutils::get_value($query);
        if ($view == 'all') {
            if (strpos($key, '_') == FALSE) {
                $key = $key . '_';
            }
            $query = "SELECT option_name, option_value FROM cms_options WHERE org_id = " . $db->escape($org_id) . " AND option_name LIKE " . $db->escape($key . '%');
            $val = array();
            $r = $db->query($query);
            if ($r != NULL) {
                foreach ($r as $key => $value) {
                    $arr_val['option_name'] = cobj::get($value, 'option_name');
                    $arr_val['option_value'] = cobj::get($value, 'option_value');
                    $val[] = $arr_val;
                }
            }
        }


        return $val;
    }

    public static function get_facebook($org_id = null) {
        $db = CDatabase::instance();
        if ($org_id == null) {
            $org_id = CF::org_id();
        }
        $query = "
                select 
                    option_value
                from 
                    cms_options
                where
                    org_id = " . $db->escape($org_id) . "
                    and option_name = 'media_fb'
                ";
        $facebook = cdbutils::get_value($query);

        return $facebook;
    }

    public static function get_twitter() {
        $db = CDatabase::instance();
        $org_id = CF::org_id();
        $query = "
                select 
                    option_value
                from 
                    cms_options
                where
                    org_id = " . $db->escape($org_id) . "
                    and option_name = 'media_tw'
                ";
        $twitter = cdbutils::get_value($query);

        return $twitter;
    }

    public static function get_image_list($type = '', $org_id = NULL) {
        $result = array();
        $db = CDatabase::instance();

        if ($org_id == NULL) {
            $org_id = CF::org_id();
        }

        if (strlen($type) > 0) {
            $q = $db->query("SELECT * FROM cms_setting WHERE type = " . $db->escape($type) . " AND status > 0 AND org_id=" . $db->escape($org_id));
            if ($q->count() == 0) {
                $q = $db->query("SELECT * FROM cms_setting WHERE type = " . $db->escape($type) . " AND status > 0 AND org_id is null");
            }
            if (count($q) > 0) {
                foreach ($q as $q_k => $q_v) {
                    $arr_result['name'] = $q_v->name;
                    $arr_result['optional'] = $q_v->optional;
                    $arr_result['image_name'] = $q_v->image_name;
                    $arr_result['image_url'] = $q_v->image_url;
                    $result[] = $arr_result;
                }
            }
        }

        return $result;
    }

}
