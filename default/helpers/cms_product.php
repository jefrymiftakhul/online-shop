<?php

class cms_product {

    public static function get_cms_product($org_id, $page, $line) {
        $db = CDatabase::instance();
        $arr_cms_product = array();
        $org_data_found = false;
        $q_base = "
			select
				cp.cms_product_id as cms_advertise_id,
				cpd.cms_product_product_category_id as cms_product_product_category_id,
				cppc.product_category_id as product_category_id,
				cppc.name as product_category,
                                cppc.image_url as image_url,
                cp.name as name_category
			from
				cms_product as cp
				inner join cms_product_detail as cpd on cpd.cms_product_id=cp.cms_product_id
				inner join cms_product_product_category as cppc on cppc.cms_product_product_category_id=cpd.cms_product_product_category_id
				inner join product_category as pc on pc.product_category_id=cppc.product_category_id
			where
				cp.status>0
                and cpd.status > 0
				and cp.is_active>0
				and cp.product_type=" . $db->escape($page) . "
				and cp.position=" . $db->escape($line) . "
		";
        $q_org = $q_base;
        if ($org_id) {
            $q_org.="
				and cp.org_id=" . $org_id . "
				order by cpd.priority asc
			";
            $r = $db->query($q_org);
            if ($r->count() > 0) {
                $org_data_found = true;
            }
			
        }
        if (!$org_data_found) {
            $q_no_org = $q_base . "
				and cp.org_id is null
				order by cpd.priority asc
			";
            $r = $db->query($q_no_org);
        }
        if ($r->count() > 0) {
            foreach ($r as $row) {
                $arr_product_product_category = array();

                $arr_product_product_category['cms_product_product_category_id'] = $row->cms_product_product_category_id;
                $arr_product_product_category['product_category_id'] = $row->product_category_id;
                $arr_product_product_category['name_category'] = $row->name_category;
                $arr_product_product_category['product_category'] = $row->product_category;
                $arr_product_product_category['product_category_image_url'] = $row->image_url;
                $q_row = "
					select
						cppcd.product_id as product_id,
						cppc.cms_product_product_category_id as cms_product_product_category_id,
						pc.name as product_category
					from
						cms_product_product_category as cppc
						inner join cms_product_product_category_detail as cppcd on cppcd.cms_product_product_category_id=cppc.cms_product_product_category_id
						inner join product_category as pc on pc.product_category_id=cppc.product_category_id
					where
						cppc.status>0
						and cppcd.status>0
						and cppc.cms_product_product_category_id=" . $db->escape($row->cms_product_product_category_id) . "
					order by
						cppcd.priority asc
				";
                $r_row = $db->query($q_row);
                if ($r_row->count() > 0) {
                    foreach ($r_row as $row_row) {
                        $arr_product = array();
                        $q_row = "
							select
								p.*
							from
								product p
                                                        where
								p.status>0 
								and p.status_confirm = 'CONFIRMED'
								and p.is_active = 1
								and p.product_id=" . $db->escape($row_row->product_id) . "
						";
                        $r2_row = $db->query($q_row);
                        if ($r2_row->count() > 0) {
                            $arr_product_product_category['data'][] = product::get_product($row_row->product_id);
                        }
                    }
                }
                $arr_cms_product[] = $arr_product_product_category;
            }
        }
        return $arr_cms_product;
    }

    public static function get_cms_product_product_category($cms_product_product_category_id) {
        $db = CDatabase::instance();
        $org_id = ccfg::get('org_id');

        $arr_product_product_category = array();

        $q = "
			select
				*
			from
				cms_product_product_category as cppc
			where
				cppc.cms_product_product_category_id=" . $db->escape($cms_product_product_category_id) . "			
		";
        $r = $db->query($q);
        if ($r->count() > 0) {
            $arr_product_product_category['image_url'] = $r[0]->image_url;
        }

        $q_base = "
			select
				cppcd.product_id as product_id,
                                cppc.image_url as image_url
			from
				cms_product_product_category as cppc
				inner join cms_product_product_category_detail as cppcd on cppcd.cms_product_product_category_id=cppc.cms_product_product_category_id
			where
				cppc.status>0
				and cppcd.status>0
				and cppc.cms_product_product_category_id=" . $db->escape($cms_product_product_category_id) . "
			order by
				cppcd.priority asc
		";
        $r = $db->query($q_base);
        if ($r->count() > 0) {
            foreach ($r as $row) {
                $arr_product = array();
                $q_row = "
					select
						p.*
					from
						product p
						left join product_price pp on pp.product_id = p.product_id and pp.org_id = " . $db->escape($org_id) . "
					where
						p.status>0 
						and p.status_confirm = 'CONFIRMED'
                                                and p.is_active = 1
						and p.product_id=" . $db->escape($row->product_id) . "
				";
                $r_row = $db->query($q_row);
                if ($r_row->count() > 0) {
                    $arr_product_product_category['data'][] = product::get_product($row->product_id);
                    ;
                }
            }
        }
        return $arr_product_product_category;
    }

    public static function get_page_category($org_id, $page, $category_id, $start = 0, $count = NULL, $filter = array()) {
        $db = CDatabase::instance();

        $arr_product = array();
        $q = "
			select
				*
			from
				product_type
			where
				name='" . $page . "'
		";
        $r = $db->query($q);
        if ($r->count() > 0) {
            $r_pt = $r[0];
            $q_product_category = "
                            select 
                                * 
                            from 
                                product_category 
                            where 
                                product_category_id = " . $db->escape($category_id);

            
            $r_pc = cdbutils::get_row($q_product_category);
            $q_base = "
                            select
                                    p.*
                            from
                                    product p 
                                    inner join product_category pc on pc.product_category_id=p.product_category_id
                            where
                                    p.status>0
                                    and p.status_confirm = 'CONFIRMED'
                                    and p.is_active = 1
                                    and p.product_type_id=" . $db->escape($r_pt->product_type_id) . "
									and ((p.visibility='catalog_search')or(p.visibility='catalog'))
                                    and pc.status > 0 and pc.lft >= " . $r_pc->lft . " and pc.rgt <= " . $r_pc->rgt . "


                    ";

            if ($start > 0 && !empty($count)) {
                $q_base .= "limit " . $start . "," . $count;
            }


            $r = $db->query($q_base);
            if ($r->count() > 0) {
                foreach ($r as $row) {
                    $arr_product[] = product::get_product($row->product_id);
                }
            }
        }

        return $arr_product;
    }

}
