<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Jun 14, 2016
     */
    class prelove_product_category {
        
        public static function get_by_slug($category_slug){
            $db = CDatabase::instance();
            $q = 'SELECT * 
                FROM product_category 
                WHERE url_key = ' .$db->escape($category_slug) .' AND status > 0';
            $r = cdbutils::get_row($q);
            return $r;
        }
    }
    