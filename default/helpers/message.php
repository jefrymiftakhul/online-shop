<?php

/**
 *
 * @author M. Isman Subakti
 * @since  April 25, 2016
 * @license http://ittron.co.id ITtron
 */
class message {

    public static function send_email($from, $to, $subject, $message) {
        $db = CDatabase::instance();
        $q = "SELECT name FROM member WHERE member_id = ".$db->escape($from);
        $data = cdbutils::get_row($q);
        
        $q = "SELECT email
                FROM department_msg 
                WHERE status > 0 
                    AND department_msg_id = ".$db->escape($to);
        $recipient = cdbutils::get_value($q);
        
        if ($data != NULL && strlen($recipient) > 0) {
            $name = cobj::get($data, 'name');            
            $date = Date("d-m-Y H:i:s");

            $subject_msg = $subject." (".$name.") ".$date;

            $text = "
                <html>
                    <body style='font-family:\"arial\"'>
                        ".$name." ".clang::__("have sent message")."<br><br>
                        <b><u>
                            ".$subject."
                        </u></b>
                        <br>
                        ".$message."
                    </body>
                </html>
            ";

            try{
                cmail::send_smtp($recipient, $subject_msg, $text);
            }
            catch (Exception $ex) {
                throw new Exception($ex->getMessage());
            }
        }
    }

}
