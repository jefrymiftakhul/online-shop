<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Jun 27, 2016
     */
    class prelove_seller {
        public static function get_total_transaction($member_id){
            $db = CDatabase::instance();
            $q = "
                SELECT t.status_transaction, count(t.transaction_id)
                FROM transaction t
                LEFT JOIN product p ON p.product_id = t.product_id
                LEFT JOIN member m ON m.member_id = p.member_id
                WHERE m.member_id = " .$db->escape($member_id) ." 
                    AND t.status > 0
                GROUP BY t.status_transaction
                ";
            $r = $db->query($q);
            $data = array();
            foreach ($r as $k => $v) {
                $status_transaction = cobj::get($v, 'status_transaction');
                $total_transaction = cobj::get($v, 'total_transaction');
                $data[$status_transaction] = $total_transaction;			
            }
            return $data;
        }
		public static function get_total_product($member_id){
		  $db = CDatabase::instance();
		  $q = '
			SELECT count(product_id) as total_product FROM product WHERE is_publish > 0 AND status_product = '.$db->escape('FINISHED').' AND status > 0 AND member_id = '.$db->escape($member_id).'
		  ';
		  $total = cdbutils::get_value($q);
		  return $total;
		}
		
		public static function get_total_success_transaction($member_id) {
		  $db = CDatabase::instance();
		  $q = '
			SELECT count(*) as total_success
			FROM product p
			INNER JOIN transaction t
			ON t.product_id = p.product_id
			WHERE p.member_id = '.$db->escape($member_id).'
		  ';
		  $total = cdbutils::get_value($q);
		  return $total;
		}
        
        public static function get_data($ref_code) {
            $db = CDatabase::instance();
            
            $q = "SELECT m.*, pr.name as province_name, c.name as city_name
                FROM member m
                LEFT JOIN province pr ON pr.province_id = m.province_id
                LEFT JOIN city c ON c.city_id = m.city_id
                WHERE m.ref_code = " .$db->escape($ref_code) .' AND m.status > 0';
            $r = cdbutils::get_row($q);
            return $r;
        }
    }
    