<?php

class recalculate_stock {

    public static function get_stock_before($data) {
        $db = CDatabase::instance();
        $stock = 0;
        try {
            //get stock before
            $q = "
				select
					*
				from
					product
				where
					status>0
					and product_id=" . $db->escape(carr::get($data, 'product_id')) . "
				
			";
            $r = $db->query($q);
            if ($r->count() > 0) {
                $row = $r[0];
                $stock = $row->stock;
            }

            $res = array();
            $res['stock'] = $stock;
            return $res;
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage() . '[001] before');
        }
    }

    public static function get_stock_after($stock_before, $data) {
        $db = CDatabase::instance();
        try {
            $stock = $stock_before['stock'] + $data['qty_in'] - $data['qty_out'];
            if ($stock < 0) {
                throw new Exception(clang::__('Stock Not Enough'));
            }
            //stock
            $data_update = array(
                "stock" => $stock,
            );
            $db->update("product", $data_update, array("status" => 1, "product_id" => $data['product_id']));
            
            $product = cdbutils::get_row('select * from product where product_id='.$db->escape($data['product_id']));
            if($product!=null && $product->parent_id!=null) {
                recalculate::product_stock_configurable($product->parent_id);
            }
            
            $res = array();
            $res['stock'] = $stock;
            return $res;
        } catch (Exception $ex) {
            throw new Exception('' . $ex->getMessage() . '[0002] after');
        }
    }

    public static function stock($data) {
        $db = CDatabase::instance();
        //get data stock before
        $stock_before = self::get_stock_before($data);
        // set stock after
        $stock_after = self::get_stock_after($stock_before, $data);
        //insert product_stock
        try {
            $data_insert_history = array(
                "product_id" => $data['product_id'],
                "qty_before" => $stock_before['stock'],
                "qty_after" => $stock_after['stock'],
                "qty_in" => $data['qty_in'],
                "qty_out" => $data['qty_out'],
                "history_date" => date('Y-m-d H:i:s'),
                "module_name" => $data['module_name'],
                "description" => $data['description'],
                "ref_table" => $data['ref_table'],
                "ref_id" => $data['ref_id'],
                "transaction_code" => $data['transaction_code'],
                "created" => date('Y-m-d H:i:s'),
                "createdby" => 'SYSTEM',
            );
            $r = $db->insert("product_stock", $data_insert_history);
            $stock_history_id = $r->insert_id();
        } catch (Exception $ex) {
            throw new Exception('' . $ex->getMessage() . '[0003] history');
        }
    }

}
