<?php

class ittronmall {

    function get_breadcrumb($category_id, $is_txt = false) {
        $data_category_menu_choise = product_category::get_parent_category($category_id);

        $depth = cobj::get($data_category_menu_choise, 'depth');
        $parent_id = cobj::get($data_category_menu_choise, 'parent_id');
        $txt_breadcrumb = array();
        $txt_breadcrumb[] = array(
            'name' => cobj::get($data_category_menu_choise, 'name'),
            'category_id' => cobj::get($data_category_menu_choise, 'product_category_id'),
            'url_key' => cobj::get($data_category_menu_choise, 'url_key'),
        );

        if (strlen($depth) > 0) {
            for ($i = 0; $i < $depth; $i++) {
                $txt = product_category::get_parent_category($parent_id);
                $arr_bread = array(
                    'name' => cobj::get($txt, 'name'),
                    'category_id' => cobj::get($txt, 'product_category_id'),
                    'url_key' => cobj::get($txt, 'url_key'),
                );
                $txt_breadcrumb[] = $arr_bread;
                $parent_id = cobj::get($txt, 'parent_id');
            }
        }

        krsort($txt_breadcrumb);

        $return = "<div class='container-breadcrumb'>";

        //$return .= "<div><a href='" . curl::base() . "'><div class='ico-home-dark'></div></a></div>";

        foreach ($txt_breadcrumb as $key => $value) {
            $class='';
            $separate = '';
            if ($key < count($txt_breadcrumb) - 1) {
                $separate = "<div class='separate-category'>></div>";
            }
            
            if($key==key($txt_breadcrumb)){
                $class .= 'breadcrumb-active';
            }
            
            $bcategory_id = carr::get($value, 'category_id');
            $url_key = carr::get($value, 'url_key');
            $bname = carr::get($value, 'name');
            $return .= $separate;
            $return .= "<div><a class='".$class."' href='" . curl::base() . "product/category/" . $url_key . "'>" . $bname . "</a></div>";
        }

        $return .= "</div>";

        if ($is_txt) {
            return $txt_breadcrumb;
        }

        return $return;
    }

}
