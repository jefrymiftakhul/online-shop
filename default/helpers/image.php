<?php

    class image {

        public static function get_image_url($image_name, $size = 'original') {
            return self::get_raw_image_url($image_name, $size);
            //$image_name = cdbutils::get_value('select image_name from product_image order by product_image_id desc limit 1');
            //$image_name = 'default_image_ProductImageParent_20160115_TOTE ROYAL BROWN.jpg';
            $admin_domain_name = ccfg::get('domain_admin');
            //$size='original';
            $image_name_encode = CResources::encode($image_name);

            $http = 'http';
            if (isset($_SERVER['HTTPS'])) {
                $http = 'https';
            }
            if ($size == 'original') {
                $url = $http . '://' . $admin_domain_name . '/res/show/' . $image_name_encode;
            }
            else {
                $url = $http . '://' . $admin_domain_name . '/res/show/' . $size . '/' . $image_name_encode;
            }

            return $url;
        }

        public static function get_raw_image_url($image_name, $size = 'original') {
            //$image_name = cdbutils::get_value('select image_name from product_image order by product_image_id desc limit 1');
            //$image_name = 'default_image_ProductImageParent_20160115_TOTE ROYAL BROWN.jpg';
            $admin_domain_name = ccfg::get('domain_admin');

///home/hallfamily/public_html/application/ittronmall/default/resources/default/image/ProductImageParent/20161018/view_all/default_image_ProductImageParent_20161018_NightViewGlassesKacamataMalamAntiSilau_5_scaled.jpg

            $temp = '';
            $arr_name = explode("_", $image_name);
            //org_code
            if (isset($arr_name[0])) {
                $temp.=$arr_name[0] . DS;
            }
            //resource_type
            if (isset($arr_name[1])) {
                $temp.=$arr_name[1] . DS;
            }
            //name
            if (isset($arr_name[2])) {
                $temp.=$arr_name[2] . DS;
            }
            //date
            if (isset($arr_name[3])) {
                $temp.=$arr_name[3] . DS;
            }

            $before_size = $temp;
            if ($size != 'original') {
                $temp.=$size . DS;
            }
            $after_size = $temp;

            $temp.=rawurlencode($image_name);
            $http = 'http';
            if (isset($_SERVER['HTTPS'])) {
                $http = 'https';
            }

            if (CF::domain() == 'paroparoshop.62hallfamily.com') {
                $admin_domain_name = "admin.paroparoshop.62hallfamily.com";
            }

            //Check Folder
            $app_code = 'adminittronmall';
            $check_image = 'application' . DS . $app_code . DS . 'default/resources/' . $after_size . $image_name;

            if (!file_exists($check_image)) {
                $original_image = 'application' . DS . $app_code . DS . 'default/resources/' . $before_size . $image_name;

                if (file_exists($original_image)) {
                    $optional_param = array(
                        'app_code' => 'adminittronmall',
                        'app_code_replace' => 'ittronmall',
                        'optional_date' => $arr_name[3],
                    );
                    self::resize_image($original_image, $image_name, $size, $optional_param);
                }
            }

            $url = $http . '://' . $admin_domain_name . '/application/adminittronmall/default/resources/' . $temp;

            return $url;
        }

        public function resize_image($original_image, $image_name, $size = 'original', $optional_param = array()) {
            $file = explode('_', $image_name);
            $arr_size = explode('x', $size);
            if (isset($arr_size[0]) && isset($arr_size[1])) {
                if (is_numeric($arr_size[0]) && is_numeric($arr_size[1])) {
                    $resource = CResources::factory('image', $file[2], $file[0]);

                    $resource->add_size($size, array(
                        'width' => $arr_size[0],
                        'height' => $arr_size[1],
                        'proportional' => false,
                        'whitespace' => true,
                    ));

                    $count = count($file);
                    for ($i = 4; $i < $count; $i++) {
                        $filename = $file[$i] . '_';
                    }
                    $filename = substr($filename, 0, -1);

                    $path_file = $original_image;

                    $path = file_get_contents($path_file);

                    $file_name_generated = $resource->save($filename, $path, $optional_param);
                    $image_url = $resource->get_url($file_name_generated);
                }
            }
        }

        public static function get_image_url_front($image_name, $size = 'original') {
            //$image_name = cdbutils::get_value('select image_name from product_image order by product_image_id desc limit 1');
            //$image_name = 'default_image_ProductImageParent_20160115_TOTE ROYAL BROWN.jpg';
//            $admin_domain_name = ccfg::get('domain_admin');

            $org_id = CF::org_id();
            //echo $org_id;
            $org = org::get_org($org_id);
            $domain_name = $org->domain;

            //$size='original';
            $image_name_encode = CResources::encode($image_name);

            $http = 'http';
            if (isset($_SERVER['HTTPS'])) {
                $http = 'https';
            }
            $url = $http . '://' . $domain_name . '/res/show/' . $size . '/' . $image_name_encode;

            return $url;
        }

        public static function get_raw_image_url_front($image_name, $size = 'original') {
            //$image_name = cdbutils::get_value('select image_name from product_image order by product_image_id desc limit 1');
            //$image_name = 'default_image_ProductImageParent_20160115_TOTE ROYAL BROWN.jpg';
//            $admin_domain_name = ccfg::get('domain_admin');

            $org_id = CF::org_id();
            //echo $org_id;
            $org = org::get_org($org_id);
            $domain_name = $org->domain;

            //$size='original';
            $image_name_encode = CResources::encode($image_name);



            ///home/hallfamily/public_html/application/ittronmall/default/resources/default/image/ProductImageParent/20161018/view_all/default_image_ProductImageParent_20161018_NightViewGlassesKacamataMalamAntiSilau_5_scaled.jpg

            $temp = '';
            $arr_name = explode("_", $image_name);
            //org_code
            if (isset($arr_name[0])) {
                $temp.=$arr_name[0] . DS;
            }
            //resource_type
            if (isset($arr_name[1])) {
                $temp.=$arr_name[1] . DS;
            }
            //name
            if (isset($arr_name[2])) {
                $temp.=$arr_name[2] . DS;
            }
            //date
            if (isset($arr_name[3])) {
                $temp.=$arr_name[3] . DS;
            }
            if ($size != 'original') {
                $temp.=$size . DS;
            }
            $temp.=$image_name;
            $http = 'http';
            if (isset($_SERVER['HTTPS'])) {
                $http = 'https';
            }
            $url = $http . '://' . $domain_name . '/application/ittronmall/default/resources/' . $temp;

            return $url;
        }

        public static function is_exists($image_name) {
            if ($image_name) {
                $image_path = CResources::get_path($image_name);

                if ($image_path != null) {
                    if (file_exists($image_path)) {
                        return true;
                    }
                }
            }
            return false;
        }

        public static function get_image($image_url, $size = 'view_all') {
//            $ch = curl_init(image::get_image_url($image_url, $size));
//            curl_setopt($ch, CURLOPT_NOBODY, true);
//            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
//            curl_exec($ch);
//            $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $retcode = 200;
            if ($retcode == 500 or $image_url == NULL) {
                $image_url = curl::base() . 'application/ittronmall/default/media/img/product/no-image.png';
            }
            else {
                $image_url = image::get_image_url($image_url, $size);
            }
//            curl_close($ch);
            return $image_url;
        }

    }
    