<?php

/**
 * Description of template
 *
 * @author Ecko Santoso
 * @since 14 Sep 16
 */
class template {
    
    public static function get_main_menu($page) {
        if (strlen($page) > 0) {
            $page_identity = strtolower($page);
        
            switch ($page_identity) {
                case 'product':
                case 'gold':
                case 'service':
                    $class_prefix = ucfirst($page_identity);
                    break;
                case 'namecard':
                    $class_prefix = 'NameCard';
                    break;
                case 'luckyday':
                    $class_prefix = 'LuckyDay';
                    break;
                default :
                    $class_prefix = 'Product';
                    break;
            }
            
            $class_name = $class_prefix.'Controller';
            $controller = $class_name::factory();
            $menu_list = $controller->menu_list();
            $list_menu = $controller->list_menu();
            
            $return = array(
                'menu_list' => $menu_list,
                'list_menu' => $list_menu
            );
            return $return;
        }
        
        return NULL;
    }
    
    public static function show_404($title = '', $message= '', $return_url = '') {
        if (strlen($title) == 0) {
            $title = 'Pencarian tidak tersedia';
        }
        if (strlen($return_url) == 0) {
            $return_url = curl::httpbase();
        }
        $html = '<div class="row">'; 
            $html .= '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
                $html .= '<div class="page-404">';
                    $html .= '<div class="icon-404">Ops</div>';
                    $html .= '<div class="content-404">';
                        $html .= '<h3 class="text-center">'.$title.'</h3>';
                        $html .= '<p class="text-center">'.$message.'</p>';
                    $html .= '</div>';
                    $html .= '<a href="'.$return_url.'" class="btn-404" target="_self">HOME</a>';
                $html .= '</div>';
            $html .= '</div>';
        $html .= '</div>';
        return $html;
    }
}
