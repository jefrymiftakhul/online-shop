<?php

class org {

    public static function full_date($data_date, $noday = false) {
        $day = Date("l", strtotime($data_date));
        $day = clang::__($day);
        $date = Date("d", strtotime($data_date));
        $month = Date("F", strtotime($data_date));
        $month = clang::__($month);
        $year = Date("Y", strtotime($data_date));

        if ($noday == true) {
            return $date . " " . $month . " " . $year;
        } else {
            return $day . ", " . $date . " " . $month . " " . $year;
        }
    }

    public static function get_org($org_id) {
        $db = CDatabase::instance();
        $query = "SELECT
                            *
                        FROM
                            org
                        WHERE org_id = " . $db->escape($org_id);
        $row_org = cdbutils::get_row($query);

        return $row_org;
    }

    public static function get_first_parent_org($org_id) {
        $db = CDatabase::instance();
        $org = cdbutils::get_row('select * from org where org_id=' . $db->escape($org_id));
        $parent_id = cobj::get($org, 'parent_id');
        if ($parent_id !== null && $parent_id > 0) {
            $org = org::get_first_parent_org($parent_id);
        }
        return $org;
    }

    public static function get_org_parent_tree($org_id) {
        $db = CDatabase::instance();
        $res = array();
        $org = org::get_org($org_id);
        if ($org) {
            $lft = cobj::get($org, 'lft');
            $rgt = cobj::get($org, 'rgt');

            $q_parent = "
                    select
                        *
                    from
                        org
                    where
                        status>0
                        and lft<=" . $db->escape($lft) . "
                        and rgt>=" . $db->escape($rgt) . "
                    order by 
                        lft desc    
                ";
            $r_parent = $db->query($q_parent);
            if ($r_parent->count() > 0) {
                foreach ($r_parent as $row_parent) {
                    $arr_org = array();
                    $arr_org['org_id'] = $row_parent->org_id;
                    $arr_org['merchant_type'] = $row_parent->merchant_type;
                    $res[] = $arr_org;
                }
            }
        }
        return $res;
    }

    public static function calculate_org_merchant_commission($org_id) {
        $db = CDatabase::instance();
        $res = array();
        $file = CF::get_file('data', 'setting');
        $mallcity_config_commission = array();
        if (file_exists($file)) {
            $setting = include $file;
            $mallcity_config_commission = carr::get($setting, 'mallcity_config_commission');
        }
        $org_parent_tree = org::get_org_parent_tree($org_id);
        $i = 0;
        $total_percent_share = 0;
        $merchant_commission = array();
        $have_admin = 0;
        foreach ($org_parent_tree as $key => $val) {
            $last_merchant = $val;
            $merchant_type = carr::get($val, 'merchant_type');
            $merchant_rules = carr::get($mallcity_config_commission, $merchant_type);
            $rules = carr::get($merchant_rules, 'rules');
            $rules_level = carr::get($rules, $i);
            if ($rules_level == null) {
                $rules_level = carr::get($rules, 'default');
            }
            if ($merchant_type == 'ADMIN') {
                $commission_percent = 100 - ($total_percent_share + 10);
                $merchant_org_id = carr::get($val, 'org_id');
                $res[] = array(
                    "org_id" => $merchant_org_id,
                    "commission_percent" => $commission_percent,
                );
                $have_admin = 1;
            }
            if ($rules_level != null) {
                $merchant_org_id = carr::get($val, 'org_id');
                $commission_percent = carr::get($rules_level, 'value');
                if ($merchant_type == 'HO') {
                    if ($have_admin == 0) {
                        $commission_percent = 100 - $total_percent_share;
                    }
                }
                $res[] = array(
                    "org_id" => $merchant_org_id,
                    "commission_percent" => $commission_percent,
                );
                $total_percent_share+=$commission_percent;
            }
            $merchant_commission[$merchant_type] = $merchant_type;
            $i++;
        }
        return $res;
    }

    public static function type($org_id = null) {
        $db = CDatabase::instance();

        if (strlen($org_id) == 0)
            return null;

        return cdbutils::get_value('select type from org where org_id = ' . $db->escape($org_id) . ' and status > 0 and is_active > 0 ');
    }

    public static function concat_query($prefix = '', &$q = '', $org_id = null) {
        $db = CDatabase::instance();
        $org_type = org::type($org_id);
        $and = '';
        $search = false;
        if (strpos($q, 'WHERE') != false || strpos($q, 'where') != false) {
            $search = true;
        }
        if ($search != false) {
            $and = 'and';
        } else {
            $and = 'where';
        }

        if (strlen($prefix) > 0)
            $prefix.='.';

        if ($org_type == 'ittronmall') {
            if (strlen($org_id) > 0) {
                $q .= ' ' . $and . ' ' . $prefix . 'org_id = ' . $db->escape($org_id);
            }
        } else if ($org_type == '62hallfamily') {
            $q .= ' and (' . $prefix . 'org_id is null || ' . $prefix . 'org_id = ' . $db->escape($org_id) . ') ';
        }
    }

}
