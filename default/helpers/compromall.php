<?php

    /**
     *
     * @author Khumbaka
     * @since  Feb 8, 2017
     * @license http://ittron.co.id ITtron Indonesia
     */
    class compromall {

        public static function valid_domain($domain) {
             //validate domain
            $valid_domain = (preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $domain) //valid chars check
                    && preg_match("/^.{1,253}$/", $domain) //overall length check
                    && preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $domain) );
            if($valid_domain) {
                $valid_domain = strpos($domain,'.')!==false; 
            }
            if($valid_domain) {
                if($domain=='admin.compromall.id') {
                    $valid_domain = false;
                }
                if($domain=='api.compromall.id') {
                    $valid_domain = false;
                }
            }
            return $valid_domain;
        }
        
        public static function email_format($base_class) {
            $classname = ucfirst($base_class) . "Email";
            $file = CF::get_files('libraries', "FormatEmail/" . $classname);
            include_once $file[0];
            $tbclass = new $classname();
            return $tbclass;
        }

        private static function send_email($recipient, $data) {
            $tbclass = compromall::email_format("forgetpassword");
            $format = $tbclass::format($recipient, $data);
            return cmail::send_smtp($recipient, $format['subject'], $format['message']);
        }

        private static function __generate_token($email = NULL) {
            $data = $email . date('Y-m-d H:i:s');

            return md5($data);
        }

        public static function forgot_password($email) {
            $err_code = 0;
            $err_message = "";
            $org = org::get_org(CF::org_id());
            $db = CDatabase::instance();
            if ($email != NULL) {
                $data_member = cdbutils::get_row("SELECT * FROM member WHERE email = " . $db->escape($email) . " AND status > 0 AND is_verified = 1 AND org_id = " . $db->escape(CF::org_id()));
                if ($data_member != NULL) {

                    $member_id = $data_member->member_id;
                    $name = $data_member->name;
                    $token = compromall::__generate_token($data_member->email);
                    $tommorow_time = strtotime(date('Y-m-d H:i:s') . "+1 days");
                    $expired_date = date('Y-m-d H:i:s', $tommorow_time);
                    $param_get = '';
                    if (ccfg::get('no_web_front') > 0) {
                        $param_get = '?api=1';
                    }
                    $content = '
                                <b style="font-size:22px">Hi ' . $data_member->name . ',</b>
                                <div style="margin-top:30px;font-size:18px;">
                                Siap mengganti password anda?
                                <br><br>
                                <a href="' . curl::httpbase() . 'member/forgotpassword/change/' . $token . $param_get . '" style="font-size:18px;margin-top:20px;background:#fe0100;padding:10px 20px;color:white;text-decoration:none;">
                                    Ganti Sekarang
                                </a>
                                </div>
                                <div style="margin-top:20px;font-size:18px">
                                    Anda mempunyai 24 jam untuk mengganti password Anda.<br>
                                    Setelah lewat dari jangka waktu, Anda dapat meminta link yang baru.
                                </div>
                                ';

                    try {
                        $data_email = array(
                            'logo' => curl::httpbase() . 'application/adminittronmall/' . $org->code . '/upload/logo/item_image/' . $org->item_image,
                            'subject' => $org->name . ' - Forgot Password',
                            'content' => $content,
                        );
                        
//                        compromall::send_email($email, $data_email);
                    }
                    catch (Exception $e) {
                        $err_code++;
                        $err_message = $e->getMessage();
                    }

                    if ($err_code == 0) {
                        $data_request_forgot_password = array(
                            'member_id' => $member_id,
                            'token' => $token,
                            'expired_date' => $expired_date,
                            'created' => date('Y-m-d H:i:s'),
                            'createdby' => 'system',
                            'updated' => date('Y-m-d H:i:s'),
                            'updateby' => 'system',
                            'status' => 1,
                        );

                        $insert = $db->insert('request_forgot_password', $data_request_forgot_password);
                        $request_id=$insert->insert_id();
                        email::send('IMForgotPassword',$request_id);
                    }
                    
                }
                else {
                    $data_member = cdbutils::get_row("SELECT * FROM member WHERE email = " . $db->escape($email) . " AND status > 0 AND org_id = " . $db->escape(CF::org_id()));
                    if ($data_member == null) {
                        $err_code++;
                        $err_message = "Email tidak ditemukan!";
                    }
                    else {
                        $err_code++;
                        $err_message = "Mohon maaf, email " . $email . " belum terverifikasi. Silahkan verifikasi lewat email terlebih dahulu.";
                    }
                }
            }
            else {
                $err_code++;
                $err_message = "Email tidak boleh kosong";
            }

            return array(
                'err_code' => $err_code,
                'err_message' => $err_message,
            );
        }

    }
    