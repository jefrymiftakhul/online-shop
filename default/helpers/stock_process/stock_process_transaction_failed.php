<?php

class stock_process_transaction_failed {

    public static function run($id) {
        $db = CDatabase::instance();
        $q = "
			select
				t.transaction_id as transaction_id,
				t.code as code,
				td.product_id as product_id,
				td.qty as qty,
                                td.transaction_detail_id,
                                p.is_package_product
			from
				transaction as t
				inner join transaction_detail as td on td.transaction_id=t.transaction_id
                                left join product p on p.product_id = td.product_id
			where
				td.status>0
				and t.transaction_id=" . $db->escape($id) . "
		";
        $r = $db->query($q);
        $data = array();
        if ($r->count() > 0) {
            $data['transaction_id'] = $r[0]->transaction_id;
            $data['transaction_code'] = $r[0]->code;
            $data['description'] = '';
            $data['module_name'] = 'transaction_failed';
            $data['ref_table'] = 'transaction';
            $data['ref_id'] = $r[0]->transaction_id;
            foreach ($r as $row) {
                $data['products'][] = array(
                    "transaction_detail_id" => $row->transaction_detail_id,
                    "product_id" => $row->product_id,
                    "qty_in" => $row->qty,
                    "qty_out" => 0,
                    "is_package_product" => $row->is_package_product,
                );
            }
        }
        return $data;
    }

}
