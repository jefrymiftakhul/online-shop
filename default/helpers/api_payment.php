<?php

    class Api_Payment {

        public static function insert_bank_transfer($options) {
            $err_code = 0;
            $err_message = "";
            $session_pg_id = carr::get($options, 'session_pg_id');
            $payment_type = carr::get($options, 'payment_type');
            $payment_code = carr::get($options, 'payment_type_code');
            $session = CApiClientSession::instance('PG', $session_pg_id);
            $session_transaction = $session->get('transaction');
            $api_client = CApiClient::instance('PG', 'BT');
            if ($err_code == 0) {
                if ($session->get('request') != '1') {
                    $items = carr::get($session_transaction, 'item');
                    $check_price = $api_client->engine()->check_price($items, $session_transaction['type']);
                    $session->set('check_price_before_payment', $check_price);
                    $err_code = $check_price['err_code'];
                    $err_message = $check_price['err_message'];
                }
            }
            if ($err_code == 0) {
                if ($session->get('request') != '1') {
                    $type = carr::get($session_transaction, 'type');
                    if ($type != 'sell') {
                        $items = carr::get($session_transaction, 'item');
                        $check_stock = $api_client->engine()->check_stock($items, $type);
                        $session->set('check_stock_before_payment', $check_stock);
                        $err_code = $check_stock['err_code'];
                        $err_message = $check_stock['err_message'];
                    }
                }
            }
            if ($err_code == 0) {
                $session_contact = carr::get($session_transaction, 'contact_detail');
                $session_contact_shipping = carr::get($session_contact, 'shipping_address');
                $city_id = carr::get($session_contact_shipping, 'city_id');
                $items = carr::get($session_transaction, 'item', array());
                $arr_item = array();
                foreach ($items as $key => $val) {
                    $arr_item[] = $val['item_id'];
                }

                $check_shipping_city = shipping::check_product_available_shipping_city($arr_item, $city_id);
                $err_code = $check_shipping_city['err_code'];
                $err_message = $check_shipping_city['err_message'];
            }
            if ($err_code == 0) {
                $before_payment = $api_client->engine()->before_payment();
                $err_code = $before_payment['err_code'];
                $err_message = $before_payment['err_message'];
            }
            if ($err_code == 0) {
                $update_payment = $api_client->engine()->update_payment_code();
                $err_code = $update_payment['err_code'];
                $err_message = $update_payment['err_message'];
            }
            if ($err_code == 0) {
                $booking_code = $session->get('booking_code');
                $email = $session->get('transaction_email');
                try {
                    email::send('IMTransaction', $session->get('transaction_id'));
                } catch (Exception $e) {
                    
                }
                //SixtyTwoHallFamilyController::factory('api')->send_transaction('order', $booking_code);
            }
            return array(
                'err_code' => $err_code,
                'err_message' => $err_message,
            );
        }

        public static function midtrans_success($options) {
            $err_code = 0;
            $err_message = "";
            $booking_code = carr::get($options, 'booking_code');
            SixtyTwoHallFamilyController::factory('api')->send_transaction('order', $booking_code);
            return array(
                'err_code' => $err_code,
                'err_message' => $err_message,
            );
        }
        public static function insert_midtrans($options) {
            $err_code = 0;
            $err_message = "";
            $session_pg_id = carr::get($options, 'session_pg_id');
            $payment_type = carr::get($options, 'payment_type');
            $payment_code = carr::get($options, 'payment_type_code');
            $payment_status = carr::get($options, 'payment_status');
            $session = CApiClientSession::instance('PG', $session_pg_id);
            $session_transaction = $session->get('transaction');
            $api_client = CApiClient::instance('PG', 'BT');
            if ($err_code == 0) {
                if ($session->get('request') != '1') {
                    $items = carr::get($session_transaction, 'item');
                    $check_price = $api_client->engine()->check_price($items, $session_transaction['type']);
                    $session->set('check_price_before_payment', $check_price);
                    $err_code = $check_price['err_code'];
                    $err_message = $check_price['err_message'];
                }
            }
            if ($err_code == 0) {
                if ($session->get('request') != '1') {
                    $type = carr::get($session_transaction, 'type');
                    if ($type != 'sell') {
                        $items = carr::get($session_transaction, 'item');
                        $check_stock = $api_client->engine()->check_stock($items, $type);
                        $session->set('check_stock_before_payment', $check_stock);
                        $err_code = $check_stock['err_code'];
                        $err_message = $check_stock['err_message'];
                    }
                }
            }
            if ($err_code == 0) {
                $session_contact = carr::get($session_transaction, 'contact_detail');
                $session_contact_shipping = carr::get($session_contact, 'shipping_address');
                $city_id = carr::get($session_contact_shipping, 'city_id');
                $items = carr::get($session_transaction, 'item', array());
                $arr_item = array();
                foreach ($items as $key => $val) {
                    $arr_item[] = $val['item_id'];
                }

                $check_shipping_city = shipping::check_product_available_shipping_city($arr_item, $city_id);
                $err_code = $check_shipping_city['err_code'];
                $err_message = $check_shipping_city['err_message'];
            }
            if ($err_code == 0) {
                $before_payment = $api_client->engine()->before_payment();
                $err_code = $before_payment['err_code'];
                $err_message = $before_payment['err_message'];
            }
            if ($err_code == 0) {
                $update_payment = $api_client->engine()->update_payment_code();
                $err_code = $update_payment['err_code'];
                $err_message = $update_payment['err_message'];
            }
            if ($err_code == 0) {
                $booking_code = $session->get('booking_code');
                $email = $session->get('transaction_email');
                //SixtyTwoHallFamilyController::factory('api')->send_transaction('order', $booking_code, $payment_status);
                
                try {
                    email::send('IMTransaction', $session->get('transaction_id'));
                } catch (Exception $e) {
                    
                }
            }
            return array(
                'err_code' => $err_code,
                'err_message' => $err_message,
            );
        }

        public function format_request($options) {
            $app = CApp::instance();
            $method = carr::get($options, 'method');
            $org_id = null;
            $org = $app->org();
            if ($org) {
                $org_id = $org->org_id;
            }

            $db = CDatabase::instance();
            $session = CApiClientSession::instance('PG');
            $request = $getpost;
            switch ($method) {
                case 'login':
                    return $request;
                case 'get_payment_charge':
                    $session_transaction = $session->get('transaction');
                    $request['currency_code'] = 'IDR';
                    $request['amount'] = $session_transaction['amount_payment'];
                    return $request;
                case 'payment' :
                    $org_id = CF::org_id();
                    $domain = '';
                    $q = "
                        select
                            *
                        from
                            org
                        where
                            org_id=" . $db->escape($org_id) . "
                    ";
                    $r = $db->query($q);
                    if ($r->count() > 0) {
                        $row = $r[0];
                        $domain = $row->domain;
                    }
                    if ($session->get('payment_type') == 'bank_transfer') {
                        $session_bank_transfer = $session->get('bank_transfer');
                        $request['payment_info']['bank'] = $session->get('bank');
                        $request['payment_info']['bank_transfer'] = $session_bank_transfer;
                    }
                    $session_transaction = $session->get('transaction');
                    $request['back_url'] = 'http://' . $domain . '/payment/redirect/';
                    return $request;
                    break;
            }
        }

        public function api($options) {
            $getpost = array_merge($_GET, $_POST);
            $method = carr::get($options, 'method');
            $payment_type = carr::get($options, 'payment_type');
            $product_code = carr::get($options, 'product_code');
            $getpost['product_code'] = $product_code;
            $getpost['payment_type'] = $payment_type;

            $request = self::format_request($method, $product_code, $getpost);
            $session = CApiClientSession::instance('PG');
            $session->set('last_method', $method);
            $session->set('request_' . $method, $request);
            $api_data = CApiClient::instance('PG', strtoupper($product_code))->exec($method, $request);
            $session->set('api_data_' . $method, $api_data);
            $session->set('have_' . $method, '1');
            if (isset($api_data['err_code']) && $api_data['err_code'] > 0) {
                $app = CApp::instance();
                cmsg::add('error', '[' . $method . '] ' . $api_data['err_message'] . " (" . $api_data['err_code'] . ')');
                echo $app->render();
            }
            else {
                $session->set('have_success_' . $method, '1');
                $this->$method($product_code, $request, $api_data);
            }
        }

        public function login($product_code, $request, $api_data) {
            $app = CApp::instance();
            $err_code = carr::get($api_data, "err_code");
            $err_msg = carr::get($api_data, "err_message");
            if ($err_code == 0) {
                $this->api('get_payment_charge', $product_code, 'credit_card');
            }
        }

        public function get_payment_charge($product_code, $request, $api_data) {
            $app = CApp::instance();
            $session = CApiClientSession::instance('PG');
            $session_transaction = $session->get('transaction');
            $total = carr::get($session_transaction, 'amount_payment');
            $payment_type = $session->get('payment_type');
            $err_code = carr::get($api_data, "err_code");
            $err_msg = carr::get($api_data, "err_message");
            $data = carr::get($api_data, "data");
            $div_payment_info = $app->add_div('div_payment_info')->add_class('col-md-12 border-1 padding-0 margin-top-20');
            $add_div_submit = 1;
            switch ($payment_type) {
                case 'bank_transfer' : {
                        $this->api('payment', $product_code, $payment_type);
                        return;
                    }
                case 'paypal' :
                case 'credit_card' : {
                        $div_payment_info_text = $div_payment_info->add_div('div_payment_info_text')->add_class('col-md-6 padding-left-10 padding-20');
                        $div_payment_info_value = $div_payment_info->add_div('div_payment_info_value')->add_class('col-md-6 padding-left-10 padding-20 text-right');
                        //$div_payment_info_space=$div_payment_info->add_div('div_payment_info_space')->add_class('col-md-4 padding-0');
                        $div_payment_info_text->add("
						<p class='margin-0'><label class='font-size14 '><b>Total </b></label></p>
						<p class='margin-0'><label class='font-size14 '><b>Charge </b></label></p>
						<p class='margin-0'><label class='font-size18 '><b>Grand Total </b></label></p>
						
					");
                        $div_payment_info_value->add("
						<p class='margin-0  '><label class='font-size14 font-red'><b>Rp. " . ctransform::format_currency($total) . "</b></label></p>
						<p class='margin-0  '><label class='font-size14 font-red'><b>Rp. " . ctransform::format_currency($data['total']) . "</b></label></p>
						<p class='margin-0  '><label class='font-size18 font-red'><b>Rp. " . ctransform::format_currency($total + $data['total']) . "</b></label></p>
					");
                        break;
                    }
                case 'mandiri_ib' : {
                        $div_payment_info_lft = $div_payment_info->add_div()->add_class('col-md-10');
                        $div_payment_info_rgt = $div_payment_info->add_div()->add_class('col-md-2 padding-top-20 padding-left-right');

                        $div_payment_info_lft->add('
						<p class="padding-left-right padding-top-20" ><label>Pembayaran menggunakan mandiri ini akan dikenakan charge yang di tanggung pengguna sebesar Rp.5500,00. (Rincian biaya di kanan belum termasuk charge)</label></p>
					');
                        $div_payment_info_lft->add('
						<p class="padding-left-right" ><label>Silahkan klik tombol lanjutkan, untuk melanjutkan pembayaran...</label></p>
						<p class="padding-left-right"><label>Anda akan segera diarahkan menuju situs pembayaran online terpercaya 62pay...</label></p>
					');
                        break;
                    }
                default : {
                        $div_payment_info_lft = $div_payment_info->add_div()->add_class('col-md-10');
                        $div_payment_info_rgt = $div_payment_info->add_div()->add_class('col-md-2 padding-top-20 padding-left-right');

                        $div_payment_info_lft->add('
						<p class="padding-left-right padding-top-20" ><label>Silahkan klik tombol lanjutkan, untuk melanjutkan pembayaran...</label></p>
						<p class="padding-left-right"><label>Anda akan segera diarahkan menuju situs pembayaran online terpercaya 62pay...</label></p>
					');

                        $button_submit = $div_payment_info_rgt->add_field()->add_action('submit_button')->set_label(clang::__('Lanjutkan'))->add_class('btn-62hallfamily bg-red');
                        $add_div_submit = 0;
                        break;
                    }
            }
            if ($add_div_submit > 0) {
                $div_submit = $app->add_div('div_submit')->add_class('col-md-12 border-1 padding');
                $div_submit_left = $div_submit->add_div('div_submit_left')->add_class('col-md-6 padding-left-10 padding-20');
                $div_submit_right = $div_submit->add_div('div_submit_right')->add_class('col-md-6 padding-left-10 padding-20');
                $button_submit = $div_submit_right->add_field()->add_action('submit_button')->set_label(clang::__('Lanjutkan'))->add_class('btn-62hallfamily bg-red pull-right');
            }
            $listener_button_submit = $button_submit->add_listener('click');
            $listener_button_submit
                    ->add_handler('reload')
                    ->set_target('div_payment_info')
                    ->set_url(curl::base() . self::__CONTROLLER . 'api/payment/' . $product_code . '/' . $payment_type);

            echo $app->render();
        }

        public function payment($product_code, $request, $api_data) {
            $app = CApp::instance();
            $data = carr::get($api_data, 'data');
            if (isset($data['redirect_url'])) {

                // get page
                $session = CApiClientSession::instance('PG');
                $transaction = $session->get('transaction');
                $page = carr::get($transaction, 'page');
                if ($page == 'product') {
                    $cart = Cart62hall_Product::factory();
                    $cart->regenerate();
                }

                $app->add_js("
					window.location.href='" . $data['redirect_url'] . "';
				");
                echo $app->render();
            }
        }

    }
    