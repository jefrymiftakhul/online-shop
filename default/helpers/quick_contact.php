<?php


class quick_contact {
    
	public static function get_quick_contact($org_id) {
		$db=CDatabase::instance();
		$arr_quick_contact=array();
		$org_data_found=false;
		$q_base="
			select
				*
			from
				cms_quick_contact 
			where
				status>0
		";
		$q_org=$q_base;
		if($org_id){
			$q_org.="
				and org_id=".$org_id."
			";
			$r=$db->query($q_org);
			if($r->count()>0){
				$org_data_found=true;
			}
		}
		if(!$org_data_found){
			$q_no_org=$q_base."
				and org_id is null
			";
			$r=$db->query($q_no_org);
		}
		if($r->count()>0){
			$row=$r[0];
			$arr_quick_contact['name']=$row->name;
			$arr_quick_contact['address']=$row->address;
			$arr_quick_contact['phone']=$row->phone;
			$arr_quick_contact['pin_bb']=$row->pin_bb;
			$arr_quick_contact['whatsapp']=$row->whatsapp;
			$arr_quick_contact['email']=$row->email;
			$arr_quick_contact['image_url']=$row->image_url;
		}
		return $arr_quick_contact;
	}
}
