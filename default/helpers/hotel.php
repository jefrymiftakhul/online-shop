<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Nov 26, 2015
     * @license http://piposystem.com Piposystem
     */
    class hotel {

        //total room = 5
        //adult = 10
        //child < adult

        public static $total_room = 5;
        public static $total_adult = 10;
        
        public static function set_hidden_value($value){
            $value = cjson::encode($value);
            $value = security::encrypt($value);
            $value = urlencode($value);
            return $value;
        }
        public static function get_hidden_value($value){
            $value = urldecode($value);
            $value = security::decrypt($value);
            $value = cjson::decode($value, true);
            return $value;
        }
        
        public static function get_transaction($booking_code) {
            $db = CDatabase::instance();
            $q = 'SELECT *
                FROM transaction_group tg
                WHERE tg.status > 0 and tg.booking_code = ' . $db->escape($booking_code) . '
                ORDER BY transaction_date desc
                ';
            $r = cdbutils::get_row($q);
            $data = array();
            if ($r != null) {
                $transaction_group_id = cobj::get($r, 'transaction_group_id');
                $data = array(
                    'transaction_group_id' => $transaction_group_id,
                    'product_category_code' => cobj::get($r, 'product_category_code'),
                    'transaction_date' => cobj::get($r, 'transaction_date'),
                    'booking_date' => cobj::get($r, 'booking_date'),
                    'payment_hold_date_time' => cobj::get($r, 'payment_hold_date_time'),
                    'issued_date' => cobj::get($r, 'issued_date'),
                    'hold_date' => cobj::get($r, 'hold_date'),
                    'hold_date_time' => cobj::get($r, 'hold_date'),
                    'transaction_code' => cobj::get($r, 'code'),
                    'reference_number' => cobj::get($r, 'reference_number'),
                    'booking_code' => cobj::get($r, 'booking_code'),
                    'currency_code' => cobj::get($r, 'currency_code'),
                    'currency_rate' => cobj::get($r, 'currency_rate'),
                    'basic_fare' => cobj::get($r, 'basic_fare'),
                    'tax_total' => cobj::get($r, 'tax_total'),
                    'discount_total' => cobj::get($r, 'discount_total'),
                    'fee_total' => cobj::get($r, 'fee_total'),
                    'ssr_total' => cobj::get($r, 'ssr_total'),
                    'insurance_total' => cobj::get($r, 'insurance_total'),
                    'admin_agent_total' => cobj::get($r, 'admin_agent_total'),
                    'promo_total' => cobj::get($r, 'promo_total'),
                    'vendor_sell_price' => cobj::get($r, 'vendor_sell_price'),
                    'channel_sell_price' => cobj::get($r, 'channel_sell_price'),
                    'channel_transfer' => cobj::get($r, 'channel_transfer'),
                    'status_transaction' => cobj::get($r, 'status_transaction'),
                    'status_payment' => cobj::get($r, 'status_payment'),
                    'payment_type' => cobj::get($r, 'payment_type'),
                    'confirmed_by' => cobj::get($r, 'confirmed_by'),
                    'api_session_id' => cobj::get($r, 'api_session_id'),
                );
                
                $q = "SELECT *, c_phone as c_phone_1 FROM transaction_group_hotel WHERE transaction_group_id=" . $db->escape($transaction_group_id);
                $r_hotel = cdbutils::get_row($q);
                
                $contact = array(
                    "title" => cobj::get($r_hotel, 'c_title'),
                    "full_name" => cobj::get($r_hotel, 'c_first_name').' '.cobj::get($r_hotel, 'c_last_name'),
                    "address" => cobj::get($r_hotel, 'c_address'),
                    "phone" => cobj::get($r_hotel, 'c_phone'),
                    "email" => cobj::get($r_hotel, 'c_email')                   
                );
                $data["contact"] = $contact;
                
                $data["c_phone_1"] = cobj::get($r_hotel, 'c_phone_1');
                
                $q = "Select * from transaction where transaction_group_id = ".$db->escape($transaction_group_id);
                $r_transaction = cdbutils::get_row($q);
                $transaction_id = cobj::get($r_transaction, 'transaction_id');
                
                $q = "Select * from transaction_hotel where transaction_id = ".$db->escape($transaction_id);
                $r_transaction_hotel = cdbutils::get_row($q);
                $transaction_hotel = array(
                    "hotel_name" => cobj::get($r_transaction_hotel, 'hotel_name'),
                    "hotel_address" => cobj::get($r_transaction_hotel, 'hotel_address'),
                    "hotel_phone" => cobj::get($r_transaction_hotel, 'hotel_phone'),
                    "check_in" => cobj::get($r_transaction_hotel, 'check_in'),
                    "check_out" => cobj::get($r_transaction_hotel, 'check_out'),
                    "total_room" => cobj::get($r_transaction_hotel, 'total_room'),
                    "hotel_rating" => cobj::get($r_transaction_hotel, 'hotel_rating'),
                    "total_guest" => cobj::get($r_transaction_hotel, 'total_guest'),
                    "comments" => cobj::get($r_transaction_hotel, 'comments'),
                    "hotel_image_url" => cobj::get($r_transaction_hotel, 'hotel_image_url'),
                    "vendor_reference_code" => cobj::get($r_transaction_hotel, 'vendor_reference_code'), 
                    "vendor_vat" => cobj::get($r_transaction_hotel, 'vendor_vat'),
                    "vendor_supplier_name" => cobj::get($r_transaction_hotel, 'vendor_supplier_name'),
                    "cp_time" => cobj::get($r_transaction_hotel, 'cp_time'),
                    "cp_date_from" => cobj::get($r_transaction_hotel, 'cp_date_from'),
                    "cp_amount" => cobj::get($r_transaction_hotel, 'cp_amount'),
                    "voucher_number" => cobj::get($r_transaction_hotel, 'voucher_number'),
                );
                
                $data["transaction_hotel"] = $transaction_hotel;
                $transaction_hotel_id = cobj::get($r_transaction_hotel, 'transaction_hotel_id');

                $q = "SELECT * FROM transaction_room WHERE transaction_hotel_id = ".$db->escape($transaction_hotel_id);
                $r_room = cdbutils::get_row($q);
                $room = array(
                    "room_category_code" => cobj::get($r_room, 'room_category_code'),
                    "room_category_name" => cobj::get($r_room, 'room_category_name'),
                    "room_type_name" => cobj::get($r_room, 'room_type_name'),
                    "bed_child" => cobj::get($r_room, 'bed_child'),
                    "breakfast" => cobj::get($r_room, 'breakfast'),
                    "price" => cobj::get($r_room, 'price')                    
                );
                $data["rooms"] = $room;
                
                $q = 'SELECT tgp.*
                    FROM transaction_group tg, transaction_group_pax tgp
                    WHERE tg.status > 0 AND tgp.status > 0 AND tg.transaction_group_id = tgp.transaction_group_id
                        AND tg.transaction_group_id = ' . $db->escape($transaction_group_id) . '
                    ';
                $r_pax = $db->query($q);
                $passengers = array();
                if ($r_pax->count() > 0) {
                    foreach ($r_pax as $r_pax_k => $r_pax_v) {
                        $passengers[] = array(
                            'pax_number' => cobj::get($r_pax_v, 'pax_number'),
                            'type' => cobj::get($r_pax_v, 'type'),
                            'title' => cobj::get($r_pax_v, 'title'),
                            'full_name' => cobj::get($r_pax_v, 'full_name'),
                            'birthdate' => date("d M Y", strtotime(cobj::get($r_pax_v, 'birthdate'))),
                        );
                    }
                }
                $data['passengers'] = $passengers;

                $q = 'SELECT * 
                    FROM transaction_group tg, transaction_group_charge tgc
                    WHERE tg.status > 0 AND tgc.status > 0 AND tg.transaction_group_id = tgc.transaction_group_id
                        AND tg.transaction_group_id = ' . $db->escape($transaction_group_id) . '
                    ';
                $r_charges = $db->query($q);
                $charges = array();
                if ($r_charges->count() > 0) {
                    foreach ($r_charges as $r_charges_k => $r_charges_v) {
                        $charges[] = array(
                            'pax_type' => cobj::get($r_charges_v, 'pax_type'),
                            'trip_index' => cobj::get($r_charges_v, 'trip_index'),
                            'flight_index' => cobj::get($r_charges_v, 'flight_index'),
                            'collect_type' => cobj::get($r_charges_v, 'collect_type'),
                            'type' => cobj::get($r_charges_v, 'type'),
                            'ssr_type' => cobj::get($r_charges_v, 'ssr_type'),
                            'code' => cobj::get($r_charges_v, 'code'),
                            'name' => cobj::get($r_charges_v, 'name'),
                            'currency_code' => cobj::get($r_charges_v, 'currency_code'),
                            'amount_value' => cobj::get($r_charges_v, 'amount_value'),
                        );
                    }
                }
                $data['charges'] = $charges;
                
                //PAYMENT
                
                $data_payment = array();
                $r = self::get_payment($booking_code);
                if ($r != null) {
                    $data_payment = array(
                        'account_number' => cobj::get($r, 'account_number'),
                        'bill_key' => cobj::get($r, 'bill_key'),
                        'indomaret_payment_code' => cobj::get($r, 'indomaret_payment_code'),
                        'payment_code' => cobj::get($r, 'payment_code'),
                        "payment_product" => cobj::get($r, "payment_product"),
                        "status_payment" => cobj::get($r, "status_payment"),
                        "created" => cobj::get($r, "created"),
                    );
                }
                $data['payment'] = $data_payment;
                
                $data_member = array();
                $q = 'SELECT m.* 
                        FROM member m
                        JOIN transaction_group tg 
                            ON tg.member_id = m.member_id
                        WHERE m.status > 0 
                            AND transaction_group_id ='.$db->escape($transaction_group_id);
                $db_member = cdbutils::get_row($q);
                if ($db_member != null) {
                    $data_member['name'] = cobj::get($db_member, 'name');
                    $data_member['email'] = cobj::get($db_member, 'email');
                    $data_member['member_code'] = cobj::get($db_member, 'member_code'); 
                    $data_member['partner_name'] = cobj::get($db_member, 'partner_name');
                }
                $data['member'] = $data_member;

                //expense
                $q = 'SELECT point 
                        FROM transaction_point_expense
                        WHERE status > 0 
                            AND transaction_group_id = '.$db->escape($transaction_group_id);
                $point = cdbutils::get_value($q);
                $data['point'] = $point;
                
                return $data;
            }
            else {
                throw new Exception('Booking Code ' . $booking_code . ' is not found');
            }
        }

        const __StepData = '1';
        const __StepPayment = '2';
        const __StepEnd = '3';

        public static function step_html($step) {
            $active1 = '';
            $active2 = '';
            $active3 = '';
            if ($step == self::__StepData) {
                $img_src = 'step1-hotel.png';
                $active1 = 'active';
            }
            else if ($step == self::__StepPayment) {
                $img_src = 'step2-hotel.png';
                $active1 = 'active';
                $active2 = 'active';
            }
            else if ($step == self::__StepEnd) {
                $img_src = 'step3-hotel.png';
                $active1 = 'active';
                $active2 = 'active';
                $active3 = 'active';
            }
            else {
                throw new Exception('Step Error');
            }

            $table = "
                <table class='col-center airlines-step'>
                    <tr style='text-align: center;'>
                        <td colspan='3'><img src='" . curl::base() . "application/sunholiday/default/media/img/" . $img_src . "' class='img_step'/></td>
                    </tr>
                    <tr class='air-step-label'>
                        <td class='" . $active1 . " step1'>" . clang::__('Input Data') . "</td>
                        <td class='" . $active2 . " step2'>" . clang::__('Payment') . "</td>
                        <td class='" . $active3 . " step3'>" . clang::__('End') . "</td>
                    </tr>
                </table>
                ";
            return $table;
        }

        public static function total_promo($promo){
            $total_promo = 0;
            foreach($promo as $pl_k => $pl_v){
                $fare_type = carr::get($pl_v, 'fare_type');
                $fare_amount = carr::get($pl_v, 'fare_amount');
                if($fare_type == 'amount'){
                    $total_promo += $fare_amount;
                }
                if($fare_type == 'percent'){
                    $total_promo += $price_ori * $fare_amount;
                }
            }
            return $total_promo;
        }

        public static function check_date($date){
            $ret = 'false';
            //format date yyyy-mm-dd
            $xdate = explode('-', $date);
            if(count($xdate) != 3){
                $ret = 'false';
            }
            else {
                $day = $xdate[2];
                $month = $xdate[1];
                $year = $xdate[0];

                $number = 'true';
                //check number
                if(!is_numeric($day) || !is_numeric($month) || !is_numeric($year)){
                    $number = 'false';
                }

                //check digit
                $digit = 'true';
                if(strlen($day) != 2 || strlen($month) != 2 || strlen($year) != 4){
                    $digit = 'false';
                }

                $data = false;
                if($number == 'true' && $digit == 'true'){
                    $data = checkdate($month, $day, $year);
                }
                
                if($data == true){
                    $ret = 'true';
                }                
            }
            return $ret;
        }

        public static function get_payment($booking_code) {
            $db = CDatabase::instance();
            $q = "SELECT tp.*
                FROM transaction_group tg, transaction_group_payment tp
                WHERE tg.status > 0 AND tp.status > 0 AND tg.transaction_group_id = tp.transaction_group_id
                    AND (tp.status_payment is NULL OR tp.status_payment = 'PENDING' OR tp.status_payment = 'SUCCESS' OR tp.status_payment = 'DEFAULT')
                    AND tg.booking_code = " . $db->escape($booking_code) . "
                ORDER BY tg.transaction_date desc
                ";
            $r = cdbutils::get_row($q);
            return $r;
        }
        

        public static function status_transaction($booking_code) {
            $db = CDatabase::instance();
            $q = 'SELECT *
                FROM transaction_group tg
                WHERE tg.status > 0 and tg.booking_code = ' . $db->escape($booking_code) . '
                ORDER BY transaction_date desc
                ';
            $r = cdbutils::get_row($q);
            if ($r != null) {
                $status = cobj::get($r, 'status_transaction');
                return $status;
            }
            return false;
        }
        
    }
    