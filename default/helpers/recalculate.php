<?php

class recalculate {

    public function product_stock_configurable($parent_id) {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $max_stock = cdbutils::get_value('select MAX(stock) as cnt from product where status > 0 and is_active = 1 and parent_id = ' . $db->escape($parent_id));

        if ($max_stock !== NULL) {
            $db->query('UPDATE product set stock=' . $max_stock . ' where product_id =' . $db->escape($parent_id));
        }
    }
    
    public function product_weight_configurable($parent_id) {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $max_weight = cdbutils::get_value('select MAX(weight) as cnt from product where status > 0 and is_active = 1 and parent_id = ' . $db->escape($parent_id));

        if ($max_weight !== NULL) {
            $db->query('UPDATE product set weight=' . $max_weight . ' where product_id =' . $db->escape($parent_id));
        }
    }
    
}