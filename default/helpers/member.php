<?php

    class member {
        public static function get_data_member(){
            $db         = CDatabase::instance();
            $session    = CSession::instance();
            if(count($session) > 0) {
                $SQL        = 'SELECT * FROM member WHERE member_id = ' . $db->escape($session->get('member_id'));
                $return     = '';
                $results    = $db->query($SQL);
                if($results->count() > 0){
                    foreach ($results as $result) {
                        $return = $result;
                    }
                    
                    $expl_last_name = explode(' ',$return->name);
                    $first_name     = $expl_last_name[0];
                    $last_name      = '';
                    if (count($expl_last_name) > 1) {
                        $last_name = $expl_last_name[(count($expl_last_name)-1)-1] .' '. $expl_last_name[(count($expl_last_name)-1)];
                    }
                    
                    // Jika data tersedia maka session bersifat true dan bernilai data member
                    return (object) array_merge((array) $return, array("last_name" => $last_name,"first_name" => $first_name));
                } else {
                    // Jika data tidak tersedia maka session bersifat false
                    return false;
                }
            }
        }
        
        public static function is_login(){
            if (strlen(CSession::instance()->get('member_id')) == 0) {
                return false;
            }
            return true;
        }
        
        public static function get_email_member($member_id){
            $db = CDatabase::instance();
            
            $SQL = 'SELECT email, member_id,name FROM member WHERE member_id = ' . $db->escape($member_id);
            $row = cdbutils::get_row($SQL);
            
            return $row;
        }
        
        public static function get_member_by_id($member_id){
            $db = CDatabase::instance();
            $q = "SELECT * FROM member WHERE member_id = " .$db->escape($member_id) ." AND status > 0";
            $r = cdbutils::get_row($q);
            return $r;
        }
        
        public static function get_parents($member_id,$include_self=false){
            $db=CDatabase::instance();
            $res=array();
            $notation_lft='<';
            $notation_rgt='>';
            if($include_self){
                $notation_lft='<=';
                $notation_rgt='>=';
            }
            $member=cdbutils::get_row("select * from member where member_id=".$db->escape($member_id));
            if($member){
                $lft=cobj::get($member,'lft');
                $rgt=cobj::get($member,'rgt');
                $org_id=cobj::get($member,'org_id');
                $q_parent="
                    select
                        m.member_id as member_id,
                        m.name as member_name,
                        m.email as member_email,
                        m.downline_quota as member_downline_quota,
                        m.downline_remain as member_downline_remain,
                        mc.member_category_id as member_category_id,
                        mc.name as member_category,
                        mc.lft as member_category_lft,
                        mc.rgt as member_category_rgt
                    from
                        member as m
                        inner join member_category as mc on mc.member_category_id=m.member_category_id
                    where
                        m.status>0
                        and m.org_id=".$db->escape($org_id)."
                        and m.lft".$notation_lft.$db->escape($lft)."
                        and m.rgt".$notation_rgt.$db->escape($rgt)."
                    order by 
                        m.lft desc    
                ";
                $r_parent=$db->query($q_parent);
                if($r_parent->count()>0){
                    foreach($r_parent as $row_parent){
                       $arr_member=array();
                       $arr_member['member_id']=$row_parent->member_id;
                       $arr_member['member_name']=$row_parent->member_name;
                       $arr_member['member_email']=$row_parent->member_email;
                       $arr_member['member_downline_quota']=$row_parent->member_downline_quota;
                       $arr_member['member_downline_remain']=$row_parent->member_downline_remain;
                       $arr_member['member_category_id']=$row_parent->member_category_id;
                       $arr_member['member_category_lft']=$row_parent->member_category_lft;
                       $arr_member['member_category_rgt']=$row_parent->member_category_rgt;
                       $res[]=$arr_member;
                    }
                }
            }
            return $res;
        }
        
        public static function get_commission_member_category($member_id){
            $db=CDatabase::instance();
            $res=array();
            $member_parent=self::get_parents($member_id,true);
            $mc_lft_before=null;
            $mc_rgt_before=null;
            $org_id=CF::org_id();
            foreach($member_parent as $member){
                $q_commission="
                    select
                        sum(commission) as total_commission
                    from
                        member_category
                    where
                        lft>=".$db->escape($member['member_category_lft'])."
                        and rgt<=".$db->escape($member['member_category_rgt'])."
                ";
                if($mc_lft_before!=null){
                    $q_commission.="
                        and lft<".$db->escape($mc_lft_before)."
                        and rgt>".$db->escape($mc_rgt_before)."
                    ";    
                }
                $q_commission.="
                    group by 
                        org_id
                ";    
                $total_commission=cdbutils::get_value($q_commission);
                $arr_member['member_category_id']=$member['member_category_id'];
                $arr_member['member_id']=$member['member_id'];
                $arr_member['commission_percent']=$total_commission;
                $res[]=$arr_member;
                $mc_lft_before=$member['member_category_lft'];
                $mc_rgt_before=$member['member_category_rgt'];
                
            }
            
            
            
            return $res;
        }
        
        public static function get_member_balance($member_id){
            $db=CDatabase::instance();
            
            $q="
                select
                    balance_idr
                from
                    member_balance
                where
                    member_id=".$db->escape($member_id)."
            ";
            $balance_idr=cdbutils::get_value($q);
            
            return $balance_idr;
            
        }
        public static function get_member_balance_lucky($member_id){
            $db=CDatabase::instance();
            
            $q="
                select
                    balance_idr
                from
                    member_balance_lucky
                where
                    member_id=".$db->escape($member_id)."
            ";
            $balance_idr=cdbutils::get_value($q);
            
            return $balance_idr;
            
        }
        public static function get_member_balance_point($member_id){
            $db=CDatabase::instance();
            
            $q="
                select
                    mbp.balance_idr
                from member m
                left join member_balance_poin mbp on m.member_id = mbp.member_id
                where
                    m.member_id=".$db->escape($member_id)."
            ";
            $balance_idr=cdbutils::get_value($q);
            
            return $balance_idr;
            
        }
    }