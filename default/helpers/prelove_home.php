<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class prelove_home{
    public static function deals_responsive_size(){
            $responsive = array();
            $responsive[] = array(
                'breakpoint' => '1200',
                'slide_to_show' => '4',
                'slide_to_scroll' => '4',
                'dots' => 'false',
            );
            $responsive[] = array(
                'breakpoint' => '1100',
                'slide_to_show' => '3',
                'slide_to_scroll' => '3',
                'dots' => 'false',
            );
            $responsive[] = array(
                'breakpoint' => '992',
                'slide_to_show' => '2',
                'slide_to_scroll' => '2',
                'dots' => 'false',
            );
            $responsive[] = array(
                'breakpoint' => '768',
                'slide_to_show' => '1',
                'slide_to_scroll' => '1',
                'dots' => 'false',
            );
//            $responsive[] = array(
//                'breakpoint' => '370',
//                'slide_to_show' => '1',
//                'slide_to_scroll' => '1',
//            );
            return $responsive;
        }
		public static function lokal_responsive_size(){
            $responsive = array();
            $responsive[] = array(
                'breakpoint' => '1200',
                'slide_to_show' => '3',
                'slide_to_scroll' => '3',
                'dots' => 'false',
            );
            $responsive[] = array(
                'breakpoint' => '1100',
                'slide_to_show' => '3',
                'slide_to_scroll' => '3',
                'dots' => 'false',
            );
            $responsive[] = array(
                'breakpoint' => '992',
                'slide_to_show' => '2',
                'slide_to_scroll' => '2',
                'dots' => 'false',
            );
            $responsive[] = array(
                'breakpoint' => '768',
                'slide_to_show' => '1',
                'slide_to_scroll' => '1',
                'dots' => 'false',
            );
//            $responsive[] = array(
//                'breakpoint' => '370',
//                'slide_to_show' => '1',
//                'slide_to_scroll' => '1',
//            );
            return $responsive;
        }
		public static function lokal_responsive_size_related(){
            $responsive = array();
            $responsive[] = array(
                'breakpoint' => '1200',
                'slide_to_show' => '4',
                'slide_to_scroll' => '1',
                'dots' => 'false',
            );
            $responsive[] = array(
                'breakpoint' => '1100',
                'slide_to_show' => '3',
                'slide_to_scroll' => '1',
                'dots' => 'false',
            );
            $responsive[] = array(
                'breakpoint' => '992',
                'slide_to_show' => '2',
                'slide_to_scroll' => '1',
                'dots' => 'false',
            );
            $responsive[] = array(
                'breakpoint' => '768',
                'slide_to_show' => '1',
                'slide_to_scroll' => '1',
                'dots' => 'false',
            );
//            $responsive[] = array(
//                'breakpoint' => '370',
//                'slide_to_show' => '1',
//                'slide_to_scroll' => '1',
//            );
            return $responsive;
        }
}

