<?php

    /**
     *
     * @author Seians0077
     * @since  Jun 9, 2016
     * @license http://piposystem.com Piposystem
     */
    class member_balance {

        public static function history($type, $id, $nominal = null) {
            $app = CApp::instance();
            $db = CDatabase::instance();
            $org_id = CF::org_id();
            $error_code = 0;
            $error_message = '';
            $result = array();
            $classname = ucfirst($type);
            $classname .= "MemberBalance";

            /*
             * Cek file ada atau tidak
             */
            $file = CF::get_files('libraries', 'member_balance/' . $classname);

            //Variable untuk class library OrgBalance
            $tbclass = null;

            //File library OrgBalance harus ada
            if (count($file) > 0 && file_exists($file[0])) {
                include_once $file[0];
                $tbclass = new $classname();
                if ($nominal != null) {
                    if (method_exists($tbclass, 'set_nominal')) {
                        $tbclass->set_nominal($nominal);
                    }
                }
                $data_balance = $tbclass->balance($id);
            }
            else {
                $error_code = 1;
                $error_message = clang::__("File not found");
            }

            //ID tidak boleh null
            if ($error_code == 0 && $id == NULL) {
                $error_code = 2;
                $error_message = clang::__("ID cannot be NULL");
            }

            //variable public ref_table harus ada di library
            if ($error_code == 0 && !isset($tbclass->ref_table)) {
                $error_code = 3;
                $error_message = clang::__("ref_table not found");
            }

            //variable public ref_pk harus ada di library
            if ($error_code == 0 && !isset($tbclass->ref_pk)) {
                $error_code = 4;
                $error_message = clang::__("ref_pk not found");
            }

            //field is_posted harus ada di ref_table yang diberikan
            if ($error_code == 0) {
                $q = "Show columns from {$tbclass->ref_table} where field = 'is_posted'";
                $query = $db->query($q);
                if ($query->count() <= 0) {
                    $error_code = 5;
                    $error_message = clang::__("is_posted not found in ref_table");
                }
            }

            $user = $app->user();
            if ($user == NULL) {
                $user = new Stdclass();
                $user->username = 'system';
            }
            //currency yang diterima
            $available_currency = array('IDR', 'USD', 'SGD');
            //key yang harus ada di return class library
            $required_keys = array('org_id', 'member_id', 'currency', 'balance_in', 'balance_out', 'saldo', 'module', 'description', 'history_date', 'createdby');

            //ambil return dari class
            //cek jika is_posted pada data / id sudah 1
//            if ($error_code == 0) {
//                $q = "SELECT is_posted FROM {$tbclass->ref_table} WHERE {$tbclass->ref_pk} = '{$id}' AND is_posted = '1'";
//                $query = $db->query($q);
//                if ($query->count() > 0) {
//                    $error_code = 6;
//                    $error_message = clang::__("Already posted");
//                }
//            }
            //index [data] harus ada
            if ($error_code == 0) {
                if (!isset($data_balance['data'])) {
                    $error_code = 11;
                    $error_message = clang::__("Data balance not set");
                }
            }
            $arr_balance_summary = array();
            if ($error_code == 0) {
                foreach ($data_balance['data'] as $data) {
                    //key harus lengkap
                    foreach ($required_keys as $key) {
                        if (!isset($data[$key])) {
                            if (!$key == 'org_id') {
                                $error_code = 7;
                                $error_message = clang::__("Balance return array format is not valid") . ' ("' . ($key) . '")';
                                $result = $data;
                                break;
                            }
                        }
                    }
                    //currency harus terdaftar di variable $available_currency
                    $data['currency'] = strtoupper($data['currency']);
                    if (!in_array($data['currency'], $available_currency)) {
                        $error_code = 8;
                        $error_message = clang::__("Currency not available");
                        $result = $data;
                        break;
                    }

                    //tampung total balance_in dan balance_out
                    if (!isset($arr_balance_summary[$data['member_id']][$data['currency']])) {
                        $arr_balance_summary[$data['member_id']][$data['currency']]['saldo_before'] = 0;
                        $arr_balance_summary[$data['member_id']][$data['currency']]['balance_in'] = 0;
                        $arr_balance_summary[$data['member_id']][$data['currency']]['balance_out'] = 0;
                        $arr_balance_summary[$data['member_id']][$data['currency']]['saldo'] = 0;
                    }

                    //tambah total balance dan plafon
                    $arr_balance_summary[$data['member_id']][$data['currency']]['member_id'] = $data['member_id'];
                    $arr_balance_summary[$data['member_id']][$data['currency']]['saldo_before'] += $data['saldo_before'];
                    $arr_balance_summary[$data['member_id']][$data['currency']]['balance_in'] += $data['balance_in'];
                    $arr_balance_summary[$data['member_id']][$data['currency']]['balance_out'] += $data['balance_out'];
                    $arr_balance_summary[$data['member_id']][$data['currency']]['saldo'] += $data['saldo'];
                }
            }
            //cek saldo harus cukup
            if ($error_code == 0) {
                foreach ($arr_balance_summary as $member_id => $balance_sum_per_currency) {
                    if ($error_code == 0) {
                        foreach ($balance_sum_per_currency as $currency_id => $total_balance) {
                            if ($total_balance['balance_out'] > 0) {
                                $total_update_balance = doubleval($total_balance['balance_in'] - $total_balance['balance_out']);
                                $q = "SELECT * FROM member_balance 
                                WHERE 
                                    member_id = " . $db->escape($total_balance['member_id']) . "
                                    AND balance_" . strtolower($currency_id) . " >= " . abs($total_update_balance);
                                $query = $db->query($q);
                                if ($query->count() <= 0) {
                                    $error_code = 9;
                                    $error_message = clang::__("Insufficient balance");
                                    $result['member_id'] = $member_id;
                                    break;
                                }
                            }
                        }
                    }
                    else {
                        break;
                    }
                }
            }


            if ($error_code == 0) {
                $result = array();
                try {
                    //insert member_balance_history
                    foreach ($data_balance['data'] as $data) {
                        $data_insert = $data;
                        $data_insert['ref_table'] = $tbclass->ref_table;
                        $data_insert['ref_id'] = $id;
                        $data_insert['created'] = date('Y-m-d H:i:s');
                        $r = $db->insert('member_balance_history', $data_insert);
                        if ($r->insert_id() !== null) {
                            $data_insert['member_balance_history_id'] = $r->insert_id();
                            $result[] = $data_insert;
                        }
                        else {
                            $error_code = 12;
                            $result = $data_insert;
                            break;
                        }
                    }

                    //update balance di tabel member_balance
                    if ($error_code == 0) {
                        foreach ($arr_balance_summary as $member_id => $balance_sum_per_currency) {
                            foreach ($balance_sum_per_currency as $currency_id => $total_balance) {
                                $total_update_balance = doubleval($total_balance['balance_in'] - $total_balance['balance_out']);
                                $q = "UPDATE member_balance 
                            SET balance_" . strtolower($currency_id) . " = balance_" . strtolower($currency_id) . " + " . $total_update_balance . "
                            WHERE member_id = " . $db->escape($total_balance['member_id']);
                                $r = $db->query($q);

                                if ($r->count() == 0) {
                                    $data_member_balance = array(
                                        'member_id' => $total_balance['member_id'],
                                        'org_id' => $org_id,
                                        'balance_' . strtolower($currency_id) => $total_update_balance,
                                    );
                                    $db->insert('member_balance', $data_member_balance);
                                }
                            }
                        }
                    }

                    //update is posted
                    $db->update($tbclass->ref_table, array('is_posted' => 1), array($tbclass->ref_pk => $id));
                }
                catch (Exception $e) {
                    $error_code = 404;
                    $error_message = "Error, call administrator..." . $e->getMessage();
                }
            }

            if ($error_code > 0) {
                $log_path = CF::get_dir('logs');
                $arr = array(
                    "error_code" => $error_code,
                    "error_message" => $error_message,
                    "type" => $type,
                    "id" => $id,
                    "detail" => $data_balance
                );
                if (!is_dir($log_path)) mkdir($log_path);
                $log_path .= "Balance" . DS;
                if (!is_dir($log_path)) mkdir($log_path);
                $log_path .= date("Y-m-d") . DS;
                if (!is_dir($log_path)) mkdir($log_path);

                $filename = "time_" . date("His") . ".log.php";
                @file_put_contents($log_path . $filename, json_encode($arr));
                throw new Exception($error_message);
            }

            return array(
                'err_code' => $error_code,
                'err_message' => $error_message,
                'result' => $result
            );
        }

    }
    