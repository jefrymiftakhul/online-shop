<?php


class cms_product_category {
    
	public static function get_cms_product_category($org_id,$product_category_id) {
		$db=CDatabase::instance();
		$arr_cms_product_category=array();
		$org_data_found=false;
		$q_base="
			select
				cpcd.cms_product_category_row_col_id as cms_product_category_row_col_id
			from
				cms_product_category as cpc
				inner join cms_product_category_detail as cpcd on cpcd.cms_product_category_id=cpc.cms_product_category_id
			where
				cpc.status>0
                and cpcd.status > 0
				and cpc.is_active>0
				and cpc.product_category_id=".$db->escape($product_category_id)."
		";
		$q_org=$q_base;
		if($org_id){
			$q_org.="
				and cpc.org_id=".$org_id."
				order by
					cpcd.priority asc
			";
			$r=$db->query($q_org);
			if($r->count()>0){
				$org_data_found=true;
			}
		}
		if(!$org_data_found){
			$q_no_org=$q_base."
				and cpc.org_id is null
				order by
					cpcd.priority asc
			";
			$r=$db->query($q_no_org);
		}
		if($r->count()>0){
			foreach($r as $row){
				$arr_product_category_row_col=array();
				$q_row="
					select
						image_url,
						row_span,
						col_span
					from
						cms_product_category_row_col
					where
						cms_product_category_row_col_id=".$db->escape($row->cms_product_category_row_col_id)."
				";
				$r_row=$db->query($q_row);
				if($r_row->count()>0){
					$arr_product_category_row_col['image_url']=$r_row[0]->image_url;
					$arr_product_category_row_col['row_span']=$r_row[0]->row_span;
					$arr_product_category_row_col['col_span']=$r_row[0]->col_span;
					$arr_cms_product_category[]=$arr_product_category_row_col;
				}
			}
		}
		return $arr_cms_product_category;
	}
	
	public static function get_brand_by_category($category_id, $org_id, $max = 4, $random = false) {
            $db = CDatabase::instance();
            $result = array();
//            $q_brand = "SELECT * FROM cms_brand WHERE status > 0 AND product_category_id = ".$db->escape($category_id)." AND org_id=".$db->escape($org_id);
//            $r_brand = $db->query($q_brand);
//            if ($r_brand != NULL) {
//                foreach ($r_brand as $brand_k => $brand_v) {
//                    $cms_brand_id = cobj::get($brand_v, 'cms_brand_id');
//                    $q_brand_detail = "SELECT * FROM cms_brand_detail WHERE status > 0 AND cms_brand_id = ".$db->escape($cms_brand_id) ." LIMIT ".$max;
//                    
//                    $r_brand_detail = $db->query($q_brand_detail);
//                    if ($r_brand_detail != NULL) {
//                        foreach ($r_brand_detail as $brand_detail_k => $brand_detail_v) {
//                            $arr_result['cms_brand_detail_id'] = cobj::get($brand_detail_v, 'cms_brand_detail_id');
//                            $arr_result['product_id'] = cobj::get($brand_detail_v, 'product_id');
//                            $arr_result['product_name'] = cobj::get($brand_detail_v, 'product_name');
//                            $arr_result['sku'] = cobj::get($brand_detail_v, 'sku');
//                            $arr_result['category'] = cobj::get($brand_detail_v, 'category');
//                            $result[] = $arr_result;
//                        }
//                    }
//                }
//            }
            $q_brand = "SELECT * FROM brand_image WHERE status > 0 AND is_active = 1 AND org_id = ".$db->escape($org_id)." AND product_category_id = ".$db->escape($category_id);
            if ($random == false) {
                $q_brand .= " ORDER BY created DESC";
            }
            else{
                $q_brand .= " ORDER BY RAND()";
            }
            $q_brand .= " LIMIT ".$max;
            
            $r_brand = $db->query($q_brand);
            if ($r_brand != NULL) {
                foreach ($r_brand as $brand_k => $brand_v) {
                    $arr['brand_image_id'] = cobj::get($brand_v, 'brand_image_id');
                    $arr['org_id'] = cobj::get($brand_v, 'org_id');
                    $arr['product_category_id'] = cobj::get($brand_v, 'product_category_id');
                    $arr['name'] = cobj::get($brand_v, 'name');
                    $arr['image_name'] = cobj::get($brand_v, 'image_name');
                    $arr['image_url'] = cobj::get($brand_v, 'image_url');
                    $arr['is_active'] = cobj::get($brand_v, 'is_active');
                    $result[] = $arr;
                }
            }
            return $result;
        }
}
