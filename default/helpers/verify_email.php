<?php

    /**
     * Description of verifyEmail
     *
     * @author Delvo
     */
    class verify_email {

        public static function generate_link($member_id) {
            $db = CDatabase::instance();
            $data_member = cdbutils::get_row("select * from member where member_id=" . $db->escape($member_id));
            $date_now = date('Y-m-d H:i:s');
            $str_code = date('YmdHis');
            $code = md5($str_code . $data_member->email);

            $param_get = '';
            if (ccfg::get('no_web_front') > 0) {
                $param_get = '?api=1';
            }
            $member_email_expired = ccfg::get('member_email_expired');
            if ($member_email_expired != NULL) {
                $expired_date = date('Y-m-d H:i:s', strtotime('+' . $member_email_expired . ' hours'));
            }
            $expired_date = date('Y-m-d H:i:s', strtotime('+3 day'));
            $insert = array(
                "member_id" => $member_id,
                "verify_code" => $code,
                "email" => $data_member->email,
                "actual_sent_date" => $date_now,
                "expired_date" => $expired_date,
                "created" => $date_now,
                "createdby" => 'system',
                "createdip" => crequest::remote_address()
            );

            $db->begin();
            try {
                $do = $db->insert('email_verification', $insert);
                if ($do) {
                    $db->commit();
                    $org = org::get_org(CF::org_id());
                    $domain = cobj::get($org,'domain');
                    $url = 'http://'.$domain . "/verifyEmail/confirm/" . $code . $param_get;
//                curl::base();
                    return $url;
                }
                else {
                    $db->rollback();
                    return FALSE;
                }
            }
            catch (Exception $exc) {
                throw new Exception($exc->getMessage());
            }
            return FALSE;
        }

    }
    