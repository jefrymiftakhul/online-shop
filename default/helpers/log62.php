<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class log62 {

    public static function search($keyword,$member_id = null, $count = null) {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $app_id = CF::app_id();
        $org_id = CF::org_id();
        $org = $app->org();
        if ($org != null) {
            $org_id = $org->org_id;
        }

        if ($count == null) {
            $count = 0;
        }

        //no need to log ajax request
        if (crequest::is_ajax())
            return false;
        //no need to log on administrator page
        $router_uri = CFRouter::routed_uri(CFRouter::$current_uri);
        $rsegment = explode('/', $router_uri);
        if (count($rsegment) > 0) {
            if ($rsegment[0] == "admin") {
                return false;
            }
        }
        
        if($member_id==0) {
            $member_id=null;
        }

        
        $controller = crouter::controller();
        if ($controller == "cresenity")
            return false;
   
        $ip_address = crequest::remote_address();
        $session_id = csess::session_id();
        $browser = crequest::browser();
        $browser_version = crequest::browser_version();
        $platform = crequest::platform();
        $platform_version = crequest::platform_version();
        $description = CF::domain();

        // Check keyword is exist or not, if not exist then insert
        $r = cdbutils::get_row("SELECT * FROM log_search WHERE LOWER(keyword) = ".$db->escape(strtolower($keyword))." ORDER BY request_date DESC LIMIT 1");
        if ($r != NULL) {
            $log_seach_id = $r->log_search_id;
            $hit_count = $r->hit_count;
            $log_update = array(
                'hit_count' => ($hit_count*1) + 1,
                'count_result' => $count,
                'request_date' => date('Y-m-d H:i:s')
            );
            $db->update("log_search", $log_update, array('log_search_id'=>$log_seach_id));
        }
        else {
            $data = array(
                "request_date" => date("Y-m-d H:i:s"),
                "org_id" => $org_id,
                "session_id" => csess::session_id(),
                "keyword" => $keyword,
                "count_result" => $count,
                "user_agent" => CF::user_agent(),
                "browser" => $browser,
                "browser_version" => $browser_version,
                "platform" => $platform,
                "platform_version" => $platform_version,
                "remote_addr" => $ip_address,
                "member_id" => $member_id,
                "uri" => crouter::complete_uri(),
                "routed_uri" => crouter::routed_uri(),
                "controller" => crouter::controller(),
                "method" => crouter::method(),
                "query_string" => crouter::query_string(),
                "description" => $description,
                "app_id" => $app_id,
            );
            $db->insert("log_search", $data);
        }

    }
    
    public static function request($user_id=null) {
            $app = CApp::instance();
            $db = CDatabase::instance('','log');
            
            $app_id = CF::app_id();
            $org_id = CF::org_id();
            $org = $app->org();
            if ($org != null) {
                $org_id = $org->org_id;
            }

            //no need to log ajax request
            if (crequest::is_ajax()) return false;
            //no need to log on administrator page
            $router_uri = CFRouter::routed_uri(CFRouter::$current_uri);
            $rsegment = explode('/', $router_uri);
            if (count($rsegment) > 0) {
                if ($rsegment[0] == "admin") {
                    return false;
                }
            }


            $nav = cnav::nav();

            $nav_name = "";
            $nav_label = "";
            $action_label = "";
            $action_name = "";
            $controller = crouter::controller();
            if ($controller == "cresenity") return false;
            $method = crouter::method();
            if ($nav != null) {
                $nav_name = $nav["name"];
                $nav_label = $nav["label"];

                if (isset($nav["action"])) {
                    foreach ($nav["action"] as $act) {
                        if (isset($act["controller"]) && isset($act["method"]) && $act["controller"] == $controller && $act["method"] == $method) {
                            $action_name = $act["name"];
                            $action_label = $act["label"];
                        }
                    }
                }
            }

            $ip_address = crequest::remote_address();
            $session_id = csess::session_id();
            $browser = crequest::browser();
            $browser_version = crequest::browser_version();
            $platform = crequest::platform();
            $platform_version = crequest::platform_version();
            $description = CF::domain();

            $data = array(
                "request_date" => date("Y-m-d H:i:s"),
                "org_id" => $org_id,
                "session_id" => csess::session_id(),
                "user_agent" => CF::user_agent(),
                "browser" => $browser,
                "browser_version" => $browser_version,
                "platform" => $platform,
                "platform_version" => $platform_version,
                "remote_addr" => $ip_address,
                "user_id" => $user_id,
                "uri" => crouter::complete_uri(),
                "routed_uri" => crouter::routed_uri(),
                "controller" => crouter::controller(),
                "method" => crouter::method(),
                "query_string" => crouter::query_string(),
                "nav" => $nav_name,
                "nav_label" => $nav_label,
                "action" => $action_name,
                "action_label" => $action_label,
                "description" => $description,
                "app_id" => $app_id,
            );
            $db->insert("log_request", $data);
        }

}
