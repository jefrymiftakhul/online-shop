<?php

    /**
     *
     * @author Seians0077
     * @since  Dec 7, 2015
     * @license http://piposystem.com Piposystem
     */
    class product_voucher {

        public static function quotahistory($id, $quota_in_out) {
            $app = CApp::instance();
            $db = CDatabase::instance();
            $user = $app->user();

            $err_code = 0;
            $err_message = '';

            if ($user == null) {
                $user = new stdClass();
                $user->username = 'system';
            }

            if ($err_code == 0) {
                $q_quota = " SELECT * FROM product_voucher WHERE  
                             product_voucher_id = " . $db->escape($id);
                $get_quota = cdbutils::get_row($q_quota);
            }

            if ($err_code == 0) {
                $quota_before = 0;
                $quota_after = 0;
                if ($get_quota == null) {
                    //$err_code++;
                    //$err_message = clang::__("Product doesn't exist [re[3]]");
                }
                else {
                    $quota_before = $get_quota->quota;
                    $quota_after = $quota_before + $quota_in_out;
                }
            }

            if ($err_code == 0) {
                if ($quota_in_out > 0) {
                    $quota_in = $quota_in_out;
                    $quota_out = 0;
                }
                else if ($quota_in_out < 0) {
                    $quota_in = 0;
                    $quota_out = -1 * $quota_in_out;
                }
                else {
                    $quota_in = $quota_in_out;
                    $quota_out = $quota_in_out;
                }
               $db->query("update product_voucher set quota=".$quota_after." where product_voucher_id =  ".$db->escape($id));
            }

            if ($err_code == 0) {
//                $db->begin();

                $q = " SELECT * FROM product_voucher WHERE status>0 AND product_voucher_id = " . $db->escape($id) . '
                       ORDER BY created DESC ';
                $get_voucher = cdbutils::get_row($q);
                $data_voucher = array(
                    'product_voucher_id' => $get_voucher->product_voucher_id,
                    'quota_before' => $quota_before,
                    'quota_after' => $quota_after,
                    'quota_in' => $quota_in,
                    'quota_out' => $quota_out,
                    'history_date' => date('Y-m-d H:i:s'),
                    'ref_table' => 'voucher(opname)',
                    'ref_id' => $get_voucher->product_voucher_id,
                    'created' => date('Y-m-d H:i:s'),
                    'createdby' => $user->username,
                    'updated' => date('Y-m-d H:i:s'),
                    'updatedby' => $user->username,
                );
                try {
                    $db->insert('product_voucher_history', $data_voucher);
                }
                catch (Exception $e) {
                    $err_code++;
                    $err_message = $e->getMessage();
                }
            }

//            if ($err_code == 0) {
//                $db->commit();
//            }
//            else {
//                $db->rollback();
//            }

            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
            );

            return $return;
        }

       
        
    
    }
    