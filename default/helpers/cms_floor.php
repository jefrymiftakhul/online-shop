<?php

    class cms_floor {

        public static function get_cms_floor($org_id, $page) {
            $db = CDatabase::instance();
            $arr_floor = array();
            $org_data_found = false;
            $q_base = "
			select
				cf.cms_floor_id as cms_floor_id,
				cfd.cms_floor_detail_id as cms_floor_detail_id,
				cfd.product_category_id as product_category_id,
				cfd.name as name,
                                cfd.image_path as image_path,
                                cfd.image_name as image_name,
                                pc.name as product_category
                                
			from
				cms_floor as cf
				inner join cms_floor_detail as cfd on cfd.cms_floor_id=cf.cms_floor_id
				inner join product_category as pc on pc.product_category_id=cfd.product_category_id
			where
				cf.status>0
				and cfd.status>0
				and cf.is_active>0
				
		";
            if (strlen($page) > 0 && strtolower($page) != "all") {
                $q_base .= " and cf.product_type = " . $db->escape($page);
            }
            $q_org = $q_base;
            if ($org_id) {
                $q_org.="
				and cf.org_id=" . $org_id . "
			";
                $q_org.="
				order by
					cfd.priority asc
			";
                $r = $db->query($q_org);
                if ($r->count() > 0) {
                    $org_data_found = true;
                }
            }
            if (!$org_data_found) {
                $q_no_org = $q_base . "
				and cf.org_id is null
			";
                $q_no_org.="
				order by
					cfd.priority asc
			";
                $r = $db->query($q_no_org);
            }
            if ($r->count() > 0) {
                foreach ($r as $row) {
                    $floor=array();
                    $floor['cms_floor_detail_id']=$row->cms_floor_detail_id;
                    $floor['product_category_id']=$row->product_category_id;
                    $floor['product_category_name']=$row->product_category_name;
                    $floor['name']=$row->name;
                    $floor['image_path']=$row->image_path;
                    $floor['image_name']=$row->image_name;
                    $arr_floor[] = $floor;
                }
            }
            return $arr_floor;
        }

    }
    