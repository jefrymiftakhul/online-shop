<?php

class lapakbangunan {
    function generate_menu($arr_menu = array(), $type='', $psubmenu = false, $is_active = array()) {
        if ($psubmenu) {
            $txt_hd = '';
            $txt_vs = '';
            if (strlen($type) == 0) {
                $txt_hd = 'hidden-xs';
                $txt_vs = 'visible-xs';
            }

            $return = '
            <div class="ico-submenu">
                    <i class="fa fa-caret-right '.$txt_hd.'"></i>
                    <i class="fa fa-caret-down '.$txt_vs.'"></i>
                </div>
                <div class="submenu-wrap">
                    <ul class="submenu">
            ';
        }
        else {
            if (strlen($type) == 0) {
                $menu_type = 'main-menu hidden-xs';
            }
            else {
                $menu_type = $type;
            }

            $return = '<ul class="'.$menu_type.'">';

            if (strlen($type) == 0) {
                $return .= ' <li class="active">
                                <a href="'.curl::base().'home">Home</a>
                            </li>';
            }
        }

        foreach ($arr_menu as $key => $value) {
            $submenu = carr::get($value, 'subnav', array());
            $category_id = carr::get($value, 'category_id');
            $name = carr::get($value, 'name');

            $txt_active = '';
            if (in_array($category_id, $is_active)) {
                $txt_active = 'is-active active';
            }
            $loop_submenu = '';
            $is_submenu = '';
            if (count($submenu) > 0) {
                if (strlen($type) == 0) {
                    $is_submenu = 'btn-submenu';
                }

                $loop_submenu = self::generate_menu($submenu, $type, true, $is_active);
            }

            $return .= '
                <li class="'.$txt_active.'">
                    <a href="'.curl::base().'product/category/'.$category_id.'" class="noselect '.$is_submenu.'">'.$name.'</a>
                    '.$loop_submenu.'
                </li>
            ';
        }

        $return .= '</ul>';        
        if ($psubmenu) {
            $return .= '
            </div>';
        }

        return $return;
        $tes = '
            <ul class="main-menu">
                <li class="active">
                    <a href="">Home</a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="btn-submenu">Pria</a>
                    <div class="ico-submenu">
                        <i class="fa fa-caret-down"></i>
                    </div>
                    <div class="submenu-wrap">
                        <ul class="submenu">
                            <li>
                                <a href="javascript:void(0)">Pakaian</a>
                                <div class="ico-submenu">
                                    <i class="fa fa-caret-right hidden-xs"></i>
                                    <i class="fa fa-caret-down visible-xs"></i>
                                </div>
                                <div class="submenu-wrap">
                                    <ul class="submenu">
                                        <li>
                                            <a href="javascript:void(0)">Kaos bagus</a>
                                            <div class="ico-submenu">
                                                <i class="fa fa-caret-right hidden-xs"></i>
                                                <i class="fa fa-caret-down visible-xs"></i>
                                            </div>
                                            <div class="submenu-wrap">
                                                <ul class="submenu">
                                                    <li>
                                                        <a href="">Kaos bagus</a>
                                                    </li>
                                                    <li>
                                                        <a href="">Celana</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">Celana</a>
                                            <div class="ico-submenu">
                                                <i class="fa fa-caret-right hidden-xs"></i>
                                                <i class="fa fa-caret-down visible-xs"></i>
                                            </div>
                                            <div class="submenu-wrap">
                                                <ul class="submenu">
                                                    <li>
                                                        <a href="">Kaos bagus</a>
                                                    </li>
                                                    <li>
                                                        <a href="">Celana</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a href="">Kesehatan</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="">Wanita</a>
                </li>
            </ul>
        ';
    }

    function get_breadcrumb($category_id, $is_txt = false) {
        $data_category_menu_choise = product_category::get_parent_category($category_id);

        $depth = cobj::get($data_category_menu_choise, 'depth');
        $parent_id = cobj::get($data_category_menu_choise, 'parent_id');
        $txt_breadcrumb = array();
        $txt_breadcrumb[] = array(
            'name' => cobj::get($data_category_menu_choise, 'name'),
            'category_id' => cobj::get($data_category_menu_choise, 'product_category_id')
        );

        if (strlen($depth) > 0) {
            for ($i=0; $i < $depth; $i++) { 
                $txt = product_category::get_parent_category($parent_id);                
                $arr_bread = array(
                    'name' => cobj::get($txt, 'name'),
                    'category_id' => cobj::get($txt, 'product_category_id')
                );
                $txt_breadcrumb[] = $arr_bread;
                $parent_id = cobj::get($txt, 'parent_id');
            }
        }

        krsort($txt_breadcrumb);

        $return = "<div class='container-breadcrumb'>";
        
        $return .= "<div><a href='".curl::base()."'><div class='ico-home-dark'></div></a></div>";

        foreach ($txt_breadcrumb as $key => $value) {
            $separate = '';
            if ($key < count($txt_breadcrumb) - 1) {
                $separate = "<div class='separate-category'>></div>";
            }
            $bcategory_id = carr::get($value, 'category_id');
            $bname = carr::get($value, 'name');
            $return .= $separate;
            $return .= "<div><a href='".curl::base()."product/category/".$bcategory_id."'>".$bname."</a></div>";
        }

        $return .= "</div>";

        if ($is_txt) {
            return $txt_breadcrumb;
        }

        return $return;
    }

}