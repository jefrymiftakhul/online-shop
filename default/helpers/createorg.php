<?php

/**
 *
 * @author Seians0077
 * @since  Dec 3, 2015
 * @license http://piposystem.com Piposystem
 */
class createorg {

    public function create_org($new_arr = array()) {
        if (isset($new_arr)) {
            $file_org = DOCROOT . 'data' . DS . 'org' . DS . $new_arr["code"] . '.php';

            $data_org = array(
                "org_id" => $new_arr["org_id"],
                "org_code" => $new_arr["code"],
                "org_name" => $new_arr["name"],
            );

            cphp::save_value($data_org, $file_org);
        }
    }

    public function create_domain($new_arr = array(), $type = '') {
        $app_id = '892';
        $app_code = "ittronmall";
        $domain = $new_arr["domain"];
        if ($type == 'admin') {
            $app_id = '891';
            $app_code = $type . "ittronmall";
            $domain = $type . '.' . $new_arr["domain"];
        }
        $file_domain = DOCROOT . 'data' . DS . 'domain' . DS . $domain . '.php';

        $data_domain = array(
            "app_id" => $app_id,
            "app_code" => $app_code,
            "org_id" => $new_arr["org_id"],
            "org_code" => $new_arr["code"],
            "store_id" => null,
            "store_code" => null,
            "domain" => $domain,
        );

        cphp::save_value($data_domain, $file_domain);
    }

    public function create_app($new_arr = array(), $login = 1, $type = '') {
        $path = (DOCROOT . 'application' . DS . $type . 'ittronmall' . DS . $new_arr["code"]);
        if (!is_dir($path)) {
            mkdir($path);
        }
        $path = (DOCROOT . 'application' . DS . $type . 'ittronmall' . DS . $new_arr["code"] . DS . 'config');
        if (!is_dir($path)) {
            mkdir($path);
        }
        $file_app = $path . DS . 'app.php';

        $have_user_login = false;
        if ($login == 1)
            $have_user_login = true;

        $data_app = array(
            //smtp
//                "smtp_host" => "smtp.mandrillapp.com",
//                "smtp_port" => "587",
//                "smtp_secure" => false,
//                "smtp_username" => "system@piposystem.com",
//                "smtp_password" => "PiI5yyr4FJOoJ_YOZclAPA",
            "smtp_from" => "noreply@" . $new_arr['domain'],
            //org
            "org_id" => $new_arr['org_id'],
            "have_clock" => false,
            "have_user_login" => $have_user_login,
            "have_user_access" => true,
            "have_user_permission" => true,
            "title" => $new_arr['name'],
            "domain_admin" => "admin." . $new_arr['domain'],
            "default_timezone" => "Asia/Jakarta",
            "set_timezone" => true,
            "multilang" => false,
            "decimal_separator" => " ",
            "thousand_separator" => ".",
            "decimal_digit" => "0",
            "date_formatted" => "d-m-Y",
            "long_date_formatted" => "d-m-Y H:i:s",
            "require_js" => false,
            //themes	
            // "theme" => "ittronmall",
            // "bootstrap" => "3",
            //api prod
            'api_url' => 'http://62pay.co.id/api/',
            'api_auth' => 'b27d10bffb120d6796d1fc4fe0447c40',
            'api_domain_server' => 'http://62pay.co.id/',
                //api local
//                'api_url' => 'http://torsapi.local/api/',
//                'api_auth' => '75461bd40e0c8d8555ba737dd7711f7a',
//                'api_domain_server' => 'http://torsapi.local/',
        );
        if(ccfg::get('compromall_system')){
           $data_app = array_merge($data_app,array('compromall_system'=>true));
        }
        else if(carr::get($new_arr, 'merchant_type')=='compromall'){
           $data_app = array_merge($data_app,array('compromall_system'=>true));
        }

        cphp::save_value($data_app, $file_app);
    }

    public function delete_org($new_arr = array()) {
        if (isset($new_arr)) {
            $file_org = DOCROOT . 'data' . DS . 'org' . DS . $new_arr["code"] . '.php';
            if (file_exists($file_org)) {
                unlink($file_org);
            }
        }
    }

    public function delete_domain($new_arr = array(), $type = '') {
        $domain = $new_arr["domain"];
        if ($type == 'admin') {
            $domain = $type . '.' . $new_arr["domain"];
        }
        $file_domain = DOCROOT . 'data' . DS . 'domain' . DS . $domain . '.php';
        if(is_dir($file_domain))
        {
             unlink($file_domain);
        }
       
    }

    public function delete_app($new_arr = array(), $type = '') {
        $path = (DOCROOT . 'application' . DS . $type . 'ittronmall' . DS . $new_arr["code"] . DS . 'config' . DS . 'app.php');
        if (file_exists($path)) {
            unlink($path);
        }
    }

    public function create_cms_advertise_slide($data_org = array()) {
        $db = CDatabase::instance();
        if (count($data_org)) {
            $org_id = carr::get($data_org, 'org_id');
            $org_name = carr::get($data_org, 'name');
            $have_product = carr::get($data_org, 'have_product');
            $have_service = carr::get($data_org, 'have_service');
            $have_gold = carr::get($data_org, 'have_gold');

            $data_cms_advertise = array();
            $data_cms_advertise['org_id'] = $org_id;
            $data_cms_advertise['is_active'] = 1;

            $data_cms_slide = array();
            $data_cms_slide['org_id'] = $org_id;
            $data_cms_slide['is_active'] = 1;
            if ($have_product > 0) {
                $advertise_setting_id = cdbutils::get_value("
						select
							cms_advertise_setting_id
						from
							cms_advertise_setting
						where
							status>0
							and product_type='product'
					");
                $data_cms_advertise['cms_advertise_setting_id'] = $advertise_setting_id;
                $data_cms_advertise['name'] = 'DEFAULT ADVERTISE PRODUCT ' . $org_name;


                $data_cms_slide['product_type'] = 'product';
                $data_cms_slide['name'] = 'DEFAULT SLIDE PRODUCT ' . $org_name;

                $db->insert('cms_advertise', $data_cms_advertise);
                $db->insert('cms_slide', $data_cms_slide);
            }
            if ($have_service > 0) {
                $advertise_setting_id = cdbutils::get_value("
						select
							cms_advertise_setting_id
						from
							cms_advertise_setting
						where
							status>0
							and product_type='service'
					");
                $data_cms_advertise['cms_advertise_setting_id'] = $advertise_setting_id;
                $data_cms_advertise['name'] = 'DEFAULT ADVERTISE SERVICE ' . $org_name;


                $data_cms_slide['product_type'] = 'service';
                $data_cms_slide['name'] = 'DEFAULT SLIDE SERVICE ' . $org_name;

                $db->insert('cms_advertise', $data_cms_advertise);
                $db->insert('cms_slide', $data_cms_slide);
            }
        }
    }

    public function create_cms_child_of_parent($new_arr) {
        $app = CApp::instance();
        $db = CDatabase::instance();

        $parent_id = carr::get($new_arr, 'parent_id');
        $parent_name = cdbutils::get_value("SELECT name FROM org WHERE org_id=" . $db->escape($parent_id));
        $org_id = carr::get($new_arr, 'org_id');
        $org_name = carr::get($new_arr, 'name');

        // CLONE TERMS FROM PARENT
        $get_terms = $db->query("SELECT * FROM cms_terms WHERE org_id=" . $db->escape($parent_id) . " AND status > 0 ORDER BY cms_terms_id ASC");
        $cms_terms_id_arr = array();
        if ($get_terms != NULL) {
            foreach ($get_terms as $get_terms_k => $get_terms_v) {
                $get_terms_v_arr = (array) $get_terms_v;
                $data_terms = array(
                    "org_id" => $org_id,
                    "name" => carr::get($get_terms_v_arr, 'name'),
                    "slug" => carr::get($get_terms_v_arr, 'slug'),
                    'created' => date('Y-m-d H:i:s'),
                    'createdby' => cobj::get($app->user(), 'username', 'api'),
                    'updated' => date('Y-m-d H:i:s'),
                    'updatedby' => cobj::get($app->user(), 'username', 'api'),
                );
                $insert_cms_terms = $db->insert('cms_terms', $data_terms);
                $cms_terms_id_arr[] = $insert_cms_terms->insert_id();
            }
        }

        // CLONE TERMS TAXONOMY FROM PARENT
        $get_term_taxonomy = $db->query("SELECT * FROM cms_term_taxonomy WHERE org_id = " . $db->escape($parent_id) . " AND status > 0 ORDER BY cms_terms_id ASC");
        $cms_term_taxonomy_id_arr = array();
        if ($get_term_taxonomy != NULL) {
            foreach ($get_term_taxonomy as $get_term_taxonomy_k => $get_term_taxonomy_v) {
                $get_term_taxonomy_v_arr = (array) $get_term_taxonomy_v;
                $data_term_taxonomy = array(
                    "org_id" => $org_id,
                    "cms_terms_id" => $cms_terms_id_arr[$get_term_taxonomy_k],
                    "taxonomy" => carr::get($get_term_taxonomy_v_arr, 'taxonomy'),
                    'created' => date('Y-m-d H:i:s'),
                    'createdby' => cobj::get($app->user(), 'username', 'api'),
                    'updated' => date('Y-m-d H:i:s'),
                    'updatedby' => cobj::get($app->user(), 'username', 'api'),
                );
                $insert_cms_term_taxonomy = $db->insert('cms_term_taxonomy', $data_term_taxonomy);
                $cms_term_taxonomy_id_arr[] = $insert_cms_term_taxonomy->insert_id();
            }
        }

        // CLONE POST with type 'page' FROM PARENT
        $get_post = $db->query("SELECT * FROM cms_post WHERE org_id = " . $db->escape($parent_id) . " AND post_type = 'page' AND status > 0 ORDER BY cms_post_id ASC");
        $cms_post_id_arr = array();
        if ($get_post != NULL) {
            foreach ($get_post as $get_post_k => $get_post_v) {
                $get_post_v_arr = (array) $get_post_v;

                $post_title = str_replace($parent_name, $org_name, carr::get($get_post_v_arr, 'post_title'));
                $post_name = str_replace(strtolower($parent_name), strtolower($org_name), carr::get($get_post_v_arr, 'post_name'));
                $data_post = array(
                    "org_id" => $org_id,
                    "post_content" => carr::get($get_post_v_arr, 'post_content'),
                    "post_title" => $post_title,
                    "post_name" => $post_name,
                    "post_status" => carr::get($get_post_v_arr, 'post_status'),
                    "post_type" => carr::get($get_post_v_arr, 'post_type'),
                    "cannot_delete" => carr::get($get_post_v_arr, 'cannot_delete'),
                    'created' => date('Y-m-d H:i:s'),
                    'createdby' => cobj::get($app->user(), 'username', 'api'),
                    'updated' => date('Y-m-d H:i:s'),
                    'updatedby' => cobj::get($app->user(), 'username', 'api'),
                );

                $insert_cms_post = $db->insert('cms_post', $data_post);
                $cms_post_id_arr[] = array(
                    'cms_post_id' => $insert_cms_post->insert_id(),
                    'post_title' => $post_title,
                    'post_name' => $post_name
                );
            }
        }

        // NOTE : DINAMIC CLONE LANJUT KAPAN2, SKG URGENT
    }

    public function create_cms($new_arr = array()) {
        $app = CApp::instance();
        $db = CDatabase::instance();

//            $parent_id = carr::get($new_arr, 'parent_id');
//            if (strlen($parent_id) > 0) {
//                $check_is_parent_exist = cdbutils::get_row("SELECT * FROM org WHERE org_id = ".$db->escape($parent_id));
//                if ($check_is_parent_exist != NULL) {
//                    $this->create_cms_child_of_parent($new_arr);
//                    return true;
//                }
//            }

        $type = carr::get($new_arr, 'type');
        if ($type != null && $type == 'ittronmall') {
            $category_arr = array(
                'promo' => 'Promo',
                'newsticker' => 'Newsticker',
                'news_promo_member' => 'News Promo Member',
            );

            foreach ($category_arr as $key => $val) {
                $check = cdbutils::get_row("
                        SELECT * FROM cms_terms 
                        WHERE status>0 AND slug = " . $db->escape($key) . "  
                        AND org_id = " . $db->escape($new_arr['org_id']));
                if ($check == null) {
                    $data_terms = array(
                        "org_id" => $new_arr['org_id'],
                        "name" => $val,
                        "slug" => $key,
                        'created' => date('Y-m-d H:i:s'),
                        'createdby' => cobj::get($app->user(), 'username', 'api'),
                        'updated' => date('Y-m-d H:i:s'),
                        'updatedby' => cobj::get($app->user(), 'username', 'api'),
                    );
                    $terms = $db->insert("cms_terms", $data_terms);
                    $terms_id = $terms->insert_id();

                    $data_term_taxonomy = array(
                        "org_id" => $new_arr['org_id'],
                        "cms_terms_id" => $terms_id,
                        "taxonomy" => 'category',
                        'created' => date('Y-m-d H:i:s'),
                        'createdby' => cobj::get($app->user(), 'username', 'api'),
                        'updated' => date('Y-m-d H:i:s'),
                        'updatedby' => cobj::get($app->user(), 'username', 'api'),
                    );
                    $term_taxonomy = $db->insert("cms_term_taxonomy", $data_term_taxonomy);
                    $term_taxonomy_id = $term_taxonomy->insert_id();
                }
            }
        }

        $arr_terms = array(
            'menu_footer_service' => 'Menu Footer Service',
            'menu_footer_about' => 'Menu Footer About',
        );
        $data_terms = array(
            "org_id" => $new_arr['org_id'],
            "name" => 'Artikel',
            "slug" => 'Artikel',
            'created' => date('Y-m-d H:i:s'),
            'createdby' => cobj::get($app->user(), 'username', 'api'),
            'updated' => date('Y-m-d H:i:s'),
            'updatedby' => cobj::get($app->user(), 'username', 'api'),
        );
        $insert_terms = $db->insert("cms_terms", $data_terms);
        $cms_terms_artikel_id = $insert_terms->insert_id();

        $data_term_taxonomy = array(
            "org_id" => $new_arr['org_id'],
            "cms_terms_id" => $cms_terms_artikel_id,
            "taxonomy" => 'category',
            'created' => date('Y-m-d H:i:s'),
            'createdby' => cobj::get($app->user(), 'username', 'api'),
            'updated' => date('Y-m-d H:i:s'),
            'updatedby' => cobj::get($app->user(), 'username', 'api'),
        );
        $insert_term_taxonomy = $db->insert("cms_term_taxonomy", $data_term_taxonomy);
        $cms_term_taxonomy_artikel_id = $insert_term_taxonomy->insert_id();

        foreach ($arr_terms as $keys => $values) {
            $check = cdbutils::get_row("SELECT * FROM cms_terms
                            WHERE status>0 AND slug = " . $db->escape($keys) . " 
                            AND org_id = " . $db->escape($new_arr['org_id']));

            if (count($check) == 0) {
                $data_terms = array(
                    "org_id" => $new_arr['org_id'],
                    "name" => $values,
                    "slug" => $keys,
                    'created' => date('Y-m-d H:i:s'),
                    'createdby' => cobj::get($app->user(), 'username', 'api'),
                    'updated' => date('Y-m-d H:i:s'),
                    'updatedby' => cobj::get($app->user(), 'username', 'api'),
                );
                $insert_terms = $db->insert("cms_terms", $data_terms);
                $cms_terms_id = $insert_terms->insert_id();

                $data_term_taxonomy = array(
                    "org_id" => $new_arr['org_id'],
                    "cms_terms_id" => $cms_terms_id,
                    "taxonomy" => 'menu_footer',
                    'created' => date('Y-m-d H:i:s'),
                    'createdby' => cobj::get($app->user(), 'username', 'api'),
                    'updated' => date('Y-m-d H:i:s'),
                    'updatedby' => cobj::get($app->user(), 'username', 'api'),
                );
                $insert_term_taxonomy = $db->insert("cms_term_taxonomy", $data_term_taxonomy);
                $cms_term_taxonomy_id = $insert_term_taxonomy->insert_id();

                if ($values == 'Menu Footer Service') {
                    // <editor-fold defaultstate="collapsed" desc="Service">
                    $arr_footer = array(
                        'belanja-di' => 'Belanja di ',
                        'beli-produk-di' => 'Beli Produk di ',
//                        'beli-jasa-di' => 'Beli Jasa di ',
                    );
                    $cnt = 1;
                    foreach ($arr_footer as $key => $value) {
                        $org_name_for_url = strtolower($new_arr['name']);
                        $org_name_for_url = str_replace(' ', '-', $org_name_for_url);
                        $post_content = self::format_post_content($key, $new_arr);
                        $data_post = array(
                            "post_content" => $post_content,
                            "org_id" => $new_arr['org_id'],
                            "post_title" => $value . $new_arr['name'],
                            "post_name" => $key . '-' . $org_name_for_url,
                            "post_type" => "page",
                            "post_status" => 'publish',
                            'created' => date('Y-m-d H:i:s'),
                            'createdby' => cobj::get($app->user(), 'username', 'api'),
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => cobj::get($app->user(), 'username', 'api'),
                        );
                        $insert_post = $db->insert("cms_post", $data_post);
                        $cms_post_id = $insert_post->insert_id();

                        $data_term_relationships = array(
                            "org_id" => $new_arr['org_id'],
                            "cms_term_taxonomy_id" => $cms_term_taxonomy_id,
                            "cms_post_id" => $cms_post_id,
                            "term_order" => $cnt,
                            'created' => date('Y-m-d H:i:s'),
                            'createdby' => cobj::get($app->user(), 'username', 'api'),
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => cobj::get($app->user(), 'username', 'api'),
                        );
                        $insert_term_relationships = $db->insert("cms_term_relationships", $data_term_relationships);
                        $cms_term_relationships_id = $insert_term_relationships->insert_id();

                        $data_menu = array(
                            "org_id" => $new_arr['org_id'],
                            "cms_terms_id" => $cms_terms_id,
                            "depth" => 0,
                            "lft" => 1,
                            "rgt" => 2,
                            "name" => $value . $new_arr['name'],
                            "menu_item_type" => 'post_type',
                            "menu_item_object_id" => $cms_post_id,
                            "menu_item_object" => 'page',
                            'created' => date('Y-m-d H:i:s'),
                            'createdby' => cobj::get($app->user(), 'username', 'api'),
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => cobj::get($app->user(), 'username', 'api'),
                        );
                        $insert_menu = $db->insert("cms_menu", $data_menu);
                        $cms_menu_id = $insert_menu->insert_id();
                        $cnt++;
                    }

                    $arr_footer = array(
                        'informasi-pembayaran' => 'Informasi Pembayaran',
                        'hubungi-kami' => 'Hubungi Kami',
                    );
                    $cnt = 4;
                    foreach ($arr_footer as $key => $value) {
                        $post_content = self::format_post_content($key, $new_arr);
                        $data_post = array(
                            "post_content" => $post_content,
                            "org_id" => $new_arr['org_id'],
                            "post_title" => $value,
                            "post_name" => $key,
                            "post_type" => "page",
                            "post_status" => 'publish',
                            'created' => date('Y-m-d H:i:s'),
                            'createdby' => cobj::get($app->user(), 'username', 'api'),
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => cobj::get($app->user(), 'username', 'api'),
                        );
                        $insert_post = $db->insert("cms_post", $data_post);
                        $cms_post_id = $insert_post->insert_id();

                        $data_term_relationships = array(
                            "org_id" => $new_arr['org_id'],
                            "cms_term_taxonomy_id" => $cms_term_taxonomy_id,
                            "cms_post_id" => $cms_post_id,
                            "term_order" => $cnt,
                            'created' => date('Y-m-d H:i:s'),
                            'createdby' => cobj::get($app->user(), 'username', 'api'),
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => cobj::get($app->user(), 'username', 'api'),
                        );
                        $insert_term_relationships = $db->insert("cms_term_relationships", $data_term_relationships);
                        $cms_term_relationships_id = $insert_term_relationships->insert_id();

                        $data_menu = array(
                            "org_id" => $new_arr['org_id'],
                            "cms_terms_id" => $cms_terms_id,
                            "depth" => 0,
                            "lft" => 1,
                            "rgt" => 2,
                            "name" => $value,
                            "menu_item_object_id" => $cms_post_id,
                            "menu_item_object" => 'page',
                            "menu_item_type" => 'post_type',
                            'created' => date('Y-m-d H:i:s'),
                            'createdby' => cobj::get($app->user(), 'username', 'api'),
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => cobj::get($app->user(), 'username', 'api'),
                        );
                        $insert_menu = $db->insert("cms_menu", $data_menu);
                        $cms_menu_id = $insert_menu->insert_id();
                        $cnt++;
                    }
                    // </editor-fold>
                } else if ($values == 'Menu Footer About') {
                    // <editor-fold defaultstate="collapsed" desc="About">
                    $arr_footer = array(
                        'tentang' => 'Tentang ',
                        'artikel' => 'Artikel ',
                    );
                    $cnt = 1;
                    foreach ($arr_footer as $key => $value) {
                        $org_name_for_url = strtolower($new_arr['name']);
                        $org_name_for_url = str_replace(' ', '-', $org_name_for_url);
                        $post_content = self::format_post_content($key, $new_arr);
                        $post_type = "page";
                        $cms_term_taxonomi_id_insert = $cms_term_taxonomy_id;
                        if ($key == 'artikel') {
                            $post_type = "post";
                            $cms_term_taxonomi_id_insert = $cms_term_taxonomy_artikel_id;
                        }
                        $data_post = array(
                            "post_content" => $post_content,
                            "org_id" => $new_arr['org_id'],
                            "post_title" => $value . $new_arr['name'],
                            "post_name" => $key . '-' . $org_name_for_url,
                            "post_type" => $post_type,
                            "post_status" => 'publish',
                            'created' => date('Y-m-d H:i:s'),
                            'createdby' => cobj::get($app->user(), 'username', 'api'),
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => cobj::get($app->user(), 'username', 'api'),
                        );
                        $insert_post = $db->insert("cms_post", $data_post);
                        $cms_post_id = $insert_post->insert_id();

                        $data_term_relationships = array(
                            "org_id" => $new_arr['org_id'],
                            "cms_term_taxonomy_id" => $cms_term_taxonomi_id_insert,
                            "cms_post_id" => $cms_post_id,
                            "term_order" => $cnt,
                            'created' => date('Y-m-d H:i:s'),
                            'createdby' => cobj::get($app->user(), 'username', 'api'),
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => cobj::get($app->user(), 'username', 'api'),
                        );
                        $insert_term_relationships = $db->insert("cms_term_relationships", $data_term_relationships);
                        $cms_term_relationships_id = $insert_term_relationships->insert_id();
                        $cms_terms_id_insert = $cms_terms_id;
                        if ($key == 'artikel') {
                            $post_type = "category";
                            $cms_post_id = $cms_terms_artikel_id;
                            $cms_terms_id_insert = $cms_terms_id;
                        }

                        $data_menu = array(
                            "org_id" => $new_arr['org_id'],
                            "cms_terms_id" => $cms_terms_id_insert,
                            "depth" => 0,
                            "lft" => 1,
                            "rgt" => 2,
                            "menu_item_object_id" => $cms_post_id,
                            "menu_item_object" => $post_type,
                            "name" => $value . $new_arr['name'],
                            "menu_item_type" => 'post_type',
                            'created' => date('Y-m-d H:i:s'),
                            'createdby' => cobj::get($app->user(), 'username', 'api'),
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => cobj::get($app->user(), 'username', 'api'),
                        );
                        $insert_menu = $db->insert("cms_menu", $data_menu);
                        $cms_menu_id = $insert_menu->insert_id();
                        $cnt++;
                    }

                    $arr_footer = array(
                        'keuntungan-anggota' => 'Keuntungan Anggota',
                        'syarat-dan-ketentuan' => 'Syarat dan Ketentuan',
                        'kebijakan-privasi' => 'Kebijakan Privasi',
                    );
                    $cnt = 3;
                    foreach ($arr_footer as $key => $value) {
                        $post_content = self::format_post_content($key, $new_arr);
                        $data_post = array(
                            "post_content" => $post_content,
                            "org_id" => $new_arr['org_id'],
                            "post_title" => $value,
                            "post_name" => $key,
                            "post_type" => "page",
                            "post_status" => 'publish',
                            'created' => date('Y-m-d H:i:s'),
                            'createdby' => cobj::get($app->user(), 'username', 'api'),
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => cobj::get($app->user(), 'username', 'api'),
                        );
                        $insert_post = $db->insert("cms_post", $data_post);
                        $cms_post_id = $insert_post->insert_id();

                        $data_term_relationships = array(
                            "org_id" => $new_arr['org_id'],
                            "cms_term_taxonomy_id" => $cms_term_taxonomy_id,
                            "cms_post_id" => $cms_post_id,
                            "term_order" => $cnt,
                            'created' => date('Y-m-d H:i:s'),
                            'createdby' => cobj::get($app->user(), 'username', 'api'),
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => cobj::get($app->user(), 'username', 'api'),
                        );
                        $insert_term_relationships = $db->insert("cms_term_relationships", $data_term_relationships);
                        $cms_term_relationships_id = $insert_term_relationships->insert_id();

                        $data_menu = array(
                            "org_id" => $new_arr['org_id'],
                            "cms_terms_id" => $cms_terms_id,
                            "depth" => 0,
                            "lft" => 1,
                            "rgt" => 2,
                            "name" => $value,
                            "menu_item_object_id" => $cms_post_id,
                            "menu_item_object" => 'page',
                            "menu_item_type" => 'post_type',
                            'created' => date('Y-m-d H:i:s'),
                            'createdby' => cobj::get($app->user(), 'username', 'api'),
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => cobj::get($app->user(), 'username', 'api'),
                        );
                        $insert_menu = $db->insert("cms_menu", $data_menu);
                        $cms_menu_id = $insert_menu->insert_id();
                        $cnt++;
                    }
                    // </editor-fold>
                } else {
                    //No Action
                }
            }
        }
    }

    public function format_post_content($key, $arr = array()) {
        $post_context = NULL;

        if ($key == 'belanja-di') {
            $post_context = "No Result";
            $post_context = '<div id="14527716675697895383322372775719" class=" font-size24 font-red bold col-md-5 padding-0">					Belanja di ' . $arr['name'] . '
</div>					
<div id="14527716675697895383af2155920531" class=" col-md-7 padding-0 margin-top-20" style="border-bottom:3px dashed #ccc;">					
</div>			
<div id="14527716675697895383eda267919051" class=" col-md-12 padding-0 margin-top-30 margin-bottom-20">									<div id="14527716675697895383eda941309875" class=" col-md-8">											<div id="14527716675697895383eda178700008" class=" font-size24 font-red text-right">						HARGA MURAH<br>DAN BERSAING
        </div>						<div id="145277166756978953842c2981603663" class=" text-right">						Harga yang tertera di website ' . $arr['name'] . ' merupakan harga "up to date"<br>dan murah. Kami senatiasa menawarkan fitur - fitur yang<br>memanjakan Anda untuk belanja dengan harga - harga yang murah.
        </div>
    </div>					<div id="145277166756978953842c2906044759" class=" col-md-4" style="margin-top:-10px;">					<center><img style="width:150px" src="/application/62hallfamily/default/media/img/62hallfamily/footer/i-03.png" /></center>
    </div>
</div>				<div id="145277166756978953846aa017684694" class=" col-md-12 padding-0 margin-top-30 margin-bottom-20">									<div id="145277166756978953846aa296173863" class=" col-md-4" style="margin-top:-10px;">					<center><img style="width:200px" src="/application/62hallfamily/default/media/img/62hallfamily/footer/i-02.png" /></center>
    </div>					<div id="14527716675697895384a92446086002" class=" col-md-8">											<div id="14527716675697895384a92240146227" class=" font-size24 font-red">						PRODUK LENGKAP<br>DAN BERKUALITAS
        </div>						<div id="14527716675697895384a92061031483" class="">						Harga yang tertera di website ' . $arr['name'] . ' merupakan harga "up to date"<br>dan murah. Kami senatiasa menawarkan fitur - fitur yang<br>memanjakan Anda untuk belanja dengan harga - harga yang murah.
        </div>
    </div>
</div>				<div id="14527716675697895384e7a268420223" class=" col-md-12 padding-0 margin-top-30 margin-bottom-20">									<div id="14527716675697895384e7a259441519" class=" col-md-8">											<div id="14527716675697895385262664665207" class=" font-size24 font-red text-right">						MUDAH DAN<br>HANDAL
        </div>						<div id="14527716675697895385262012545543" class=" text-right">						' . $arr['name'] . ' menyediakan kemudahan belanja melalui shopping online<br>klik buy dan menyediakan berbagai metode pembayaran, melalui<br>Online Payment, Kartu Kredit, Bank Transfer, Bayar Tunai ditempat<br>(COD), dan Cicilan 0%.
        </div>
    </div>					<div id="1452771667569789538564a677865380" class=" col-md-4" style="margin-top:-10px;">					<center><img style="width:180px" src="/application/62hallfamily/default/media/img/62hallfamily/footer/i-04.png" /></center>
    </div>
</div>				<div id="1452771667569789538564a045592230" class=" col-md-12 padding-0 margin-top-30 margin-bottom-40">									<div id="14527716675697895385a32524866950" class=" col-md-4" style="margin-top:-10px;">					<center><img style="width:200px" src="/application/62hallfamily/default/media/img/62hallfamily/footer/i-01.png" /></center>
    </div>					<div id="14527716675697895385a32269703361" class=" col-md-8">											<div id="14527716675697895385a32410681914" class=" font-size24 font-red">						ASURANSI<br>PENGIRIMAN
        </div>						<div id="14527716675697895385e1a700141700" class="">						' . $arr['name'] . ' menjaga keamanan produk dalam proses"<br>pengiriman dengan standard packing yang aman. Dipastikan<br>produk yang Anda beli sampai dalam kondisi yang baik.
        </div>
    </div>
</div>';
        } else if ($key == 'beli-jasa-di') {
            $post_context = '<div id="145276224156976481d2834537921971" class=" col-md-12 padding-0">
        <div id="145276224156976481d2834731077232" class=" font-size24 font-red bold col-md-6 padding-0">Beli Jasa di ' . $arr['name'] . '
        </div>					
        <div id="145276224156976481d2c1c835685255" class=" col-md-6 padding-0 margin-top-30 margin-bottom-30" style="border-bottom:3px dashed #ccc;">
            
        </div>
    </div>				
    <div id="145276224156976481d2c1c217668249" class=" col-md-12 padding-0">				
        <div id="myCarousel" class="carousel slide slide-show" data-ride="carousel">
            <ol class="carousel-indicators"><li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1" class="active"></li><li data-target="#myCarousel" data-slide-to="2" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="3" class="active"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <center><img src="' . curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/footer/sc-buyservice/j-1.jpg" alt="" width="80%"></center>
                    <div class="col-md-12 border-1 margin-top-20"><h3 class="font-red">1. Cari Jasa yang Anda inginkan</h3>Anda dapat menemukan produk yang Anda cari dengan cara antara lain:<br><br>a. Pilih kategori yang Anda cari di kolom pencarian<br>b. Anda bisa juga memilih produk yang direkomendasikan di halaman utama</div>
                </div>
                <div class="item ">
                    <center><img src="' . curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/footer/sc-buyservice/j-2.jpg" alt="" width="80%"></center>
                    <div class="col-md-12 border-1 margin-top-20"><h3 class="font-red">2. Periksa Informasi Jasa & Harga</h3>Pada halaman ini Anda dapat melihat informasi lengkap tentang produk yang Anda pilih.<br>Pastikan Produk yang Anda pilih sesuai dengan yang diingikan. dan klik <b>"Beli"</b>
                    </div>
                </div>
                <div class="item ">
                    <center><img src="' . curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/footer/sc-buyservice/j-3.jpg" alt="" width="80%"></center>
                    <div class="col-md-12 border-1 margin-top-20"><h3 class="font-red">3. Masukkan Alamat Pemesanan & Alamat Tujuan</h3>Isi informasi alamat pembayaran dan alamat tujuan dengan benar dan lengkap.<br>Periksa produk Anda dan klik <b>"Lanjutkan Pembayaran"</b></div>
                </div>
                <div class="item ">
                    <center><img src="' . curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/footer/sc-buyservice/j-4.jpg" alt="" width="80%"></center>
                    <div class="col-md-12 border-1 margin-top-20"><h3 class="font-red">4. Periksa Email dan Akun Anda</h3>Transaksi Anda selesai. Anda dapat mencetak pesanan Anda dengan klik <b>"Cetak Pesanan"</b><br>Silahkan cek email Anda untuk detail transaksi atau kembali ke halaman utama.</div>
                </div>
            </div>
            <a class="left carousel-control font-red font-size24" style="top:190px" href="#myCarousel" role="button" data-slide="prev">
                <span class="fa fa-chevron-circle-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control font-red font-size24" style="top:190px" href="#myCarousel" role="button" data-slide="next">
                <span class="fa fa-chevron-circle-right font-red" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>';
        } else if ($key == 'beli-produk-di') {
            $post_context = '<div id="1452771295569787dfa29fc304224607" class=" font-size24 font-red bold col-md-6 padding-0">
    Beli Produk di ' . $arr['name'] . '
    </div>					
<div id="1452771295569787dfa2de4473935431" class=" col-md-6 padding-0 margin-top-30 margin-bottom-30" style="border-bottom:3px dashed #ccc;">					
    </div>
<div id="1452771295569787dfa31cc029204747" class=" col-md-12 padding-0">				
    <div id="myCarousel" class="carousel slide slide-show" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="2" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="3" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="4" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="5" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="6" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="7" class="active"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="item active"><center><img src="' . curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/footer/sc-buyproduct/p-1.jpg" alt="" width="80%"></center><div class="col-md-12 border-1 margin-top-20"><h3 class="font-red">1. Cari Produk yang Anda inginkan</h3>Anda dapat menemukan produk yang Anda cari dengan cara antara lain:<br><br>a. Ketik nama produk yang Anda cari pada kolom pencarian<br>b. Pilih kategori yang ada di halaman kami<br>c. Anda bisa juga memilih produk yang direkomendasikan di halaman utama</div></div><div class="item "><center><img src="' . curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/footer/sc-buyproduct/p-2.jpg" alt="" width="80%"></center><div class="col-md-12 border-1 margin-top-20"><h3 class="font-red">2. Periksa Informasi Produk, Pengiriman & Kuantitas</h3>Pada halaman ini Anda dapat melihat informasi lengkap tentang produk yang Anda pilih.<br>Pastikan Produk yang Anda pilih sesuai dengan warna, ukuran dan jumlah yang diingikan.<br>lalu klik <b>"Add to Cart"</b></div></div><div class="item "><center><img src="' . curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/footer/sc-buyproduct/p-3.jpg" alt="" width="80%"></center><div class="col-md-12 border-1 margin-top-20"><h3 class="font-red">3. Lihat Keranjang Belanja atau Lanjut Belanja</h3>Setelah klik Add to Cart akan muncul pop up dimana Anda dapat untuk melihat<br>keranjang belanja Anda atau Lanjut Belanja</div></div><div class="item "><center><img src="' . curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/footer/sc-buyproduct/p-4.jpg" alt="" width="80%"></center><div class="col-md-12 border-1 margin-top-20"><h3 class="font-red">4. Memesan Tanpa Memiliki Akun</h3>a. Jika Anda belum menjadi member kami, Anda dapat tetap dapat melakukan pemesanan sebagai tamu atau<br>Anda juga dapat mendaftarka diri sebagai member dengan klik <span class="font-red underline">Daftar Sekarang</span><br>b. Jika Anda sudah menjadi member. Silahkan melakukan proses login.</div></div><div class="item "><center><img src="' . curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/footer/sc-buyproduct/p-5.jpg" alt="" width="80%"></center><div class="col-md-12 border-1 margin-top-20"><h3 class="font-red">5. Lihat Keranja belanja Anda</h3>Pada halaman ini Anda masih dapat menambahkan atau menghapus belajaan Anda.<br>Setelah Anda yakin dengan produk yang dipilih, langsung klik <b>"Proses Pembayaran"</b></div></div><div class="item "><center><img src="' . curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/footer/sc-buyproduct/p-6.jpg" alt="" width="80%"></center><div class="col-md-12 border-1 margin-top-20"><h3 class="font-red">6. Masukkan Alamat & Metode Pengiriman</h3>Isi informasi alamat pembayaran dan pengiriman dengan benar dan lengkap.<br>Periksa produk Anda dan klik <b>"Lanjutkan Pembayaran"</b></div></div><div class="item "><center><img src="' . curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/footer/sc-buyproduct/p-7.jpg" alt="" width="80%"></center><div class="col-md-12 border-1 margin-top-20"><h3 class="font-red">7. Pilih Metode Pembayaran</h3>Kami menyediakan pilihan metode pembayaran yang dapat Anda gunakan.<br>Silahkan mengisi data dengan benar dan lengkap sesuai metode pembayaran yang Anda pilih.</div></div><div class="item "><center><img src="' . curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/footer/sc-buyproduct/p-8.jpg" alt="" width="80%"></center><div class="col-md-12 border-1 margin-top-20"><h3 class="font-red">8. Periksa Email & Akun Anda</h3></div></div></div><a class="left carousel-control font-red font-size24" style="top:190px" href="#myCarousel" role="button" data-slide="prev">
            <span class="fa fa-chevron-circle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control font-red font-size24" style="top:190px" href="#myCarousel" role="button" data-slide="next">
            <span class="fa fa-chevron-circle-right font-red" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>';
        } else if ($key == 'hubungi-kami') {
            $post_context = '<div id="14527716675697895383322372775719" class=" font-size24 font-red bold col-md-5 padding-0">					Hubungi Kami
</div>					
<div id="14527716675697895383af2155920531" class=" col-md-7 padding-0 margin-top-20" style="border-bottom:3px dashed #ccc;">					
</div>			
<div id="14527716675697895383eda267919051" class=" col-md-12 padding-0 margin-top-30 margin-bottom-20">	
    <div class="font-red font-size24">
                CONTACT INFORMATION
            </div>
            <p>
				Ruko Mangga Dua Blok B10 No. 08<br>
				Jl. Jagir Wonokromo no 98 - EG<br>
				Surabaya - Jawa Timur<br>
				<br>
				Phone   : 031-8410062<br>
				Mobile   : 0815-5130-481<br>
				Email    : cs@tokoonlineindonesia.co.id<br>
				Skype   : cstokoonlineindonesia<br>
				ID Line  : cstokoonlineindo<br>
				Line      : 0815-5130-481<br>
				WA       : 0815-5130-481<br>
				BBM      : 5CC6A2B<br>
				<br>
				Operational Hours<br>
				Senin - Sabtu : 09.00 - 17.00<br>
            </p>
</div>';
        } else if ($key == 'kebijakan-privasi') {
            $post_context = '<div id="14527716675697895383322372775719" class=" font-size24 font-red bold col-md-5 padding-0">						
Kebijakan & Privacy
</div>					
<div id="14527716675697895383af2155920531" class=" col-md-7 padding-0 margin-top-20" style="border-bottom:3px dashed #ccc;">					
</div>	
<div id="14527716675697895383eda267919051" class=" col-md-12 padding-0 margin-top-20 margin-bottom-20">
    Yang dimaksud dengan kebijakan privasi ini adalah acuan atau aturan yang melindungi dan mengatur penggunaan data dan informasi penting lainnya untuk semua pihak yang terlibat dalam www.' . $arr['domain'] . ' ini. Semua pihak yang melakukan akses pada website ini, secara otomatis dianggap telah membaca, memahami dan memberikan persetujuannya terhadap pengumpulan dan penggunaan data pribadi pengguna seperti yang akan diuraikan di bawah ini.<br><br>
    <div style="margin-left-30">
1.www.' . $arr['domain'] . ' melindungi segala informasi yang diberikan oleh customer pada saat melakukan transaksi pembelian baik berupa biodata customer maupun data pengiriman.<br>
2.www.' . $arr['domain'] . ' melindungi segala informasi yang diberikan oleh customer pada saat ingin menjadi member atau anggota berupa informasi secara lengkap tentang keanggotaan<br>
3.www.' . $arr['domain'] . ' berhak menggunakan data dari customer dan member untuk peningkatan kualitas pelayanan dan mutu dari website ini<br>
4.www.' . $arr['domain'] . ' tidak bertanggung jawab jika salah satu customer ataupun member melakukan penukaran data sendiri tanpa sepengetahuan www.' . $arr['domain'] . ' dan terjadi penyimpangan didalamnya<br>
5.Setiap customer ataupun member dapat melakukan penghentian langganan dengan melakukan unsubscribe <br>
6.www.' . $arr['domain'] . ' dapat memberitahukan data dan informasi yang dimilikinya bila diwajibkan oleh institusi yang berwenang berdasarkan hukum dan ketentuan yang berlaku di Republik Indonesia.<br>
7.Customer ataupun pengguna website www.' . $arr['domain'] . ' membebaskan manajemen ataupun pihak terkait di dalamnya, jika ditemukan adanya pelanggaran kebijakan hak cipta atau hak kekayaaan intelektual dikarenakan ketidakmampuan www.' . $arr['domain'] . ' dalam memahaminya. Customer ataupun pengguna website dapat menyampaikan keberatan atas produk dan jasa tersebut yang diindikasikan melakukan pelanggaran.<br>
8.Customer dapat mengirimkan saran dan kritik serta keperluan lainnya melalui kontak di Hubungi Kami.<br><br>
    </div>
Kebijakan Privasi ini dapat diubah atau dilakukan pembaharuan untuk dapat meningkatkan kualitas kenyamanan dan keamanan untuk semua pihak yang terlibat dalam penggunaan website ini. www.' . $arr['domain'] . ' sangat menyarankan setiap pihak yang terlibat dalam penggunaan website ini untuk membaca dan memahami setiap halaman dari Kebijakan Privasi ini dari waktu ke waktu untuk dapat mengerti setiap perubahan atau pembaharuan yang dilakukan. Customer yang melakukan pengaksesan website ini dianggap telah melakukan persetujuan untuk seluruh isi kebijakan privasi ini.
</div>';

            //OLD Content
//                $post_context = '<div id="14527716675697895383322372775719" class=" font-size24 font-red bold col-md-5 padding-0">						
//Kebijakan & Privacy
//</div>					
//<div id="14527716675697895383af2155920531" class=" col-md-7 padding-0 margin-top-20" style="border-bottom:3px dashed #ccc;">					
//</div>	
//<div id="14527716675697895383eda267919051" class=" col-md-12 padding-0 margin-top-20 margin-bottom-20">
//    Yang dimaksud dengan kebijakan privasi ini adalah acuan atau aturan yang melindungi dan mengatur penggunaan data dan informasi penting lainnya untuk semua pihak yang terlibat dalam www.' . $arr['domain'] . ' ini. Semua pihak yang melakukan akses pada website ini, secara otomatis dianggap telah membaca, memahami dan memberikan persetujuannya terhadap pengumpulan dan penggunaan data pribadi pengguna seperti yang akan diuraikan di bawah ini.<br><br>
//    <div style="margin-left-30">
//1.www.' . $arr['domain'] . ' melindungi segala informasi yang diberikan oleh customer pada saat melakukan transaksi pembelian baik berupa biodata customer maupun data pengiriman.<br>
//2.www.' . $arr['domain'] . ' melindungi segala informasi yang diberikan oleh customer pada saat ingin menjadi member atau anggota berupa informasi secara lengkap tentang keanggotaan<br>
//3.www.' . $arr['domain'] . ' berhak menggunakan data dari customer dan member untuk peningkatan kualitas pelayanan dan mutu dari website ini<br>
//4.www.' . $arr['domain'] . ' tidak bertanggung jawab jika salah satu customer ataupun member melakukan penukaran data sendiri tanpa sepengetahuan www.' . $arr['domain'] . ' dan terjadi penyimpangan didalamnya<br>
//5.Setiap customer ataupun member dapat melakukan penghentian langganan dengan melakukan unsubscribe <br>
//6.www.' . $arr['domain'] . ' dapat memberitahukan data dan informasi yang dimilikinya bila diwajibkan oleh institusi yang berwenang berdasarkan hukum dan ketentuan yang berlaku di Republik Indonesia.<br>
//7.Customer ataupun pengguna website www.' . $arr['domain'] . ' membebaskan manajemen ataupun pihak terkait di dalamnya, jika ditemukan adanya pelanggaran kebijakan hak cipta atau hak kekayaaan intelektual dikarenakan ketidakmampuan www.' . $arr['domain'] . ' dalam memahaminya. Customer ataupun pengguna website dapat menyampaikan keberatan atas produk dan jasa tersebut yang diindikasikan melakukan pelanggaran.<br>
//8.Customer dapat mengirimkan saran dan kritik serta keperluan lainnya melalui kontak di Hubungi Kami.<br><br>
//    </div>
//Kebijakan Privasi ini dapat diubah atau dilakukan pembaharuan untuk dapat meningkatkan kualitas kenyamanan dan keamanan untuk semua pihak yang terlibat dalam penggunaan website ini. www.' . $arr['domain'] . ' sangat menyarankan setiap pihak yang terlibat dalam penggunaan website ini untuk membaca dan memahami setiap halaman dari Kebijakan Privasi ini dari waktu ke waktu untuk dapat mengerti setiap perubahan atau pembaharuan yang dilakukan. Customer yang melakukan pengaksesan website ini dianggap telah melakukan persetujuan untuk seluruh isi kebijakan privasi ini.
//</div>';
        } else if ($key == 'keuntungan-anggota') {
            $post_context = '<div id="14527716675697895383322372775719" class=" font-size24 font-red bold col-md-5 padding-0">      
Keuntungan Anggota
</div>     
<div id="14527716675697895383af2155920531" class=" col-md-7 padding-0 margin-top-20" style="border-bottom:3px dashed #ccc;">     
</div> 
<div id="14527716675697895383eda267919051" class=" col-md-12 padding-0 margin-bottom-20">
    <div class="row">
        <div class="col-md-5">
            <img width="100%" src="/application/62hallfamily/default/media/img/62hallfamily/footer/keuntungan/anggota.jpg"/>
        </div>
        <div class="col-md-7 bold margin-top-40">
            Segera pastikan anda menjadi bagian dari<br>
            keluarga besar www.' . $arr['domain'] . ' Suatu kebanggaan bagi kami,<br>
            jika Anda menjadi bagian dari keluarga<br>
            besar kami. 
        </div>
    </div>
    <center>
        <div class="font-size24 font-red">
            5 Keuntungan Langsung
        </div>
        Segera pastikan anda melakukan pendaftaraan keanggotaan secara gratis selama <br>
        1 tahun untuk mendapatkan keuntungan langsung (ya benar , keuntungan langsung) sebagai berikut :
    </center>
    <div class="row margin-top-20">
        <div class="col-md-4 col-md-offset-2">
            <center>
                <img class="margin-bottom-20" width="100px" src="/application/62hallfamily/default/media/img/62hallfamily/footer/keuntungan/k-01.png"/><br><br>
                Kartu anggota Anda yang<br>
                telah jadi dikirimkan <br>
                ke alamat rumah Anda. 
            </center>
        </div>
        <div class="col-md-4">
            <center>
                <img class="margin-bottom-20" width="100px" src="/application/62hallfamily/default/media/img/62hallfamily/footer/keuntungan/k-02.png"/><br><br>
                Diskon langsung untuk produk <br>
                handphone , mainan & perlengkapan bayi <br>
                dan kamera mulai dari Rp 50.000,-
            </center>
        </div>
    </div>
    <div class="row margin-top-20">
        <div class="col-md-4">
            <center>
                <img class="margin-bottom-20" width="100px" src="/application/62hallfamily/default/media/img/62hallfamily/footer/keuntungan/k-03.png"/><br><br>
                Poin keanggotaan yang anda <br>
                dapatkan dapat anda tukarkan sesuai <br>
                dengan ketentuan yang berlaku.
            </center>
        </div>
        <div class="col-md-4 margin-top-10">
            <center>
                <img class="margin-bottom-20" width="100px" src="/application/62hallfamily/default/media/img/62hallfamily/footer/keuntungan/k-04.png"/><br><br>
                Informasi secara berkala <br>
                via email anda sesuai dengan <br>
                produk - produk favorit anda.
            </center>
        </div>
        <div class="col-md-4">
            <center>
                <img class="margin-bottom-20" width="100px" src="/application/62hallfamily/default/media/img/62hallfamily/footer/keuntungan/k-05.png"/><br><br>
                Notifikasi produk - produk promo <br>
                dengan "harga keterlaluan" hanya <br>
                untuk keluarga besar www.' . $arr['domain'] . '
            </center>
        </div>
    </div>
</div>';
        } else if ($key == 'syarat-dan-ketentuan') {
            $post_context = '<div class="font-size24 font-red bold col-md-5 padding-0" id="14527716675697895383322372775719">Syarat &amp; Ketentuan</div>

<div class="col-md-7 padding-0 margin-top-20" id="14527716675697895383af2155920531" style="border-bottom:3px dashed #ccc;"> </div>

<div class="col-md-12 padding-0 margin-top-20 margin-bottom-20" id="14527716675697895383eda267919051" style="text-align: justify;">Selamat datang di www.' . $arr['domain'] . '.<br />
www.' . $arr['domain'] . ' adalah website e-commerce yang menyediakan produk dan jasa sesuai dengan kebutuhan Anda.<br />
<br />
Syarat dan ketentuan yang dijabarkan pada poin-poin dibawah ini adalah yang dibuat oleh www.' . $arr['domain'] . ' dengan memperhatikan Anda sebagai customer dari www.' . $arr['domain'] . ' . Customer diwajibkan untuk membaca dengan teliti dan seksama karena customer memiliki kewajiban untuk tunduk pada seluruh syarat dan ketentuan dari website ini. <u><strong>Customer yang melakukan transaksi pembelian produk di website ini, dianggap telah membaca, menyimak, memahami, dan mengerti serta menyetujui semua isi dari Syarat dan Ketentuan ini.</strong></u> Jika customer tidak menyetujui satu atau lebih dari syarat dan ketentuan ini, maka customer tidak diperkenankan menggunakan layanan website www.' . $arr['domain'] . '<br />
<br />
<b>A. DEFINISI</b><br />
1.www.' . $arr['domain'] . ' adalah suatu website e-commerce yang menyediakan produk dan jasa sesuai dengan kebutuhan anda.<br />
2.Syarat &amp; ketentuan yang tercantum dalam halaman ini adalah persetujuan dari pembeli www.' . $arr['domain'] . ' tentang hak , kewajiban dan tanggung jawab customer dalam penggunaan website www.' . $arr['domain'] . '<br />
3.Customer adalah pihak - pihak yang melakukan transaksi pembelian produk atau jasa di www.' . $arr['domain'] . '<br />
4.Member / anggota adalah pihak - pihak yang terdaftar dalam website www.' . $arr['domain'] . ' secara khusus yang mendapatkan keuntungan lebih dari www.' . $arr['domain'] . '<br />
5.Rekening 62Pay adalah rekening bersama e-commerce milik PT INDONESIA ENAM DUA yang memberikan kenyamanan dan keamanan bagi customer dalam melakukan transaksi produk dan jasa.<br />
<br />
<b>B. TRANSAKSI PEMBELIAN</b><br />
1.Customer yang melakukan transaksi pembelian di www.' . $arr['domain'] . ' bisa langsung melakukan pembelian dan pembayaran tanpa harus terlebih dahulu terdaftar di www.' . $arr['domain'] . '<br />
2.Customer sangat disarankan untuk melakukan pendaftaran keanggotaan untuk mendapatkan berbagai keuntungan dan kemudahan dalam bertransaksi di www.' . $arr['domain'] . ' . Informasi lebih lanjut tentang keanggotaan dapat dibaca dan dipelajari di Keuntungan Anggota.<br />
3.Customer yang melakukan pembelian barang dengan sadar melakukan persetujuan bahwa :<br />
a) Customer telah membaca, memahami, menyetujui seluruh informasi / deskripsi barang maupun jasa (jenis, fungsi, tipe, warna, kualitas dan lainnya) sebelum melakukan transaksi pembelian barang.<br />
b) Perihal warna yang tampil di layar monitor, customer membebaskan www.' . $arr['domain'] . ' dalam keakuratan dikarenakan kesadaran bahwa warna produk bergantung dari resolusi monitor yang dilihat oleh masing-masing customer.<br />
c) Customer juga mengetahui dan sadar bahwa dalam melakukan transaksi pembelian, customer diwajibkan melakukan pembayaran terlebih dahulu baik secara parsial (sebagian) atau full payment (penuh), dimana hal ini akan tergantung dari masing - masing produk dan jasa.<br />
d) Customer juga bersedia berupa ganti rugi uang kembali (uang yang dibayarkan oleh customer dikembalikan 100%) jika terjadi wanprestasi terhadap produk yang dibeli berupa stok habis. Jika terjadi wanprestasi dalam bentuk lainnya berupa kondisi barang tidak sesuai dalam bentuk apapun, customer bersedia mendapatkan ganti rugi 5% dari harga barang atau menunggu pergantian barang. <strong>Perihal ganti rugi ini hanya berlaku jika barang belum dikirim oleh www.' . $arr['domain'] . ' , atau pihak www.' . $arr['domain'] . ' yang melakukan penawaran kepada customer.</strong><br />
4. www.' . $arr['domain'] . ' melalui 62Pay berhak untuk membatalkan sepihak jika pembayaran dari customer dengan menggunakan jasa payment gateway (kartu kredit, internet banking, mobile banking) terindikasi gagal. Jika terjadi hal seperti ini, maka customer wajib melakukan pembayaran ulang sesuai dengan arahan yang disampaikan oleh 62Pay.<br />
5. Customer memahami dan menyetujui bahwa setiap masalah keterlambatan dalam pengiriman barang diluar tanggung jawab dari www.' . $arr['domain'] . ', namun dalam hal ini www.' . $arr['domain'] . ' akan berupaya dengan maksimal dalam pengurusan pengiriman ini kepada pihak terkait. <u><strong>Pihak www.' . $arr['domain'] . ' tidak dapat dituntut dalam bentuk apapun jika durasi produk tiba melebihi estimasi waktu di sistem.</strong></u><br />
6. Customer tidak dapat membatalkan secara sepihak pembelian yang telah dilakukannya kecuali mendapatkan persetujuan tertulis dari www.' . $arr['domain'] . ' <strong>dengan alasan apapun.</strong><br />
7. Customer juga tidak dapat menarik uang atau dana yang telah disetor untuk pembelanjaan toko online www.' . $arr['domain'] . ' <strong>dengan alasan apapun.</strong></div>

<div class="col-md-12 padding-0 margin-top-20 margin-bottom-20" style="text-align: justify;"> </div>

<div class="col-md-12 padding-0 margin-top-20 margin-bottom-20" style="text-align: justify;"><strong>C. PENGIRIMAN</strong></div>

<div class="col-md-12 padding-0 margin-top-20 margin-bottom-20" style="text-align: justify;">1. Pelayanan pengiriman dari www.' . $arr['domain'] . ' tidak dapat menjadwalkan pengiriman sesuai yang diinginkan oleh customer. Kami memiliki jadwal pengiriman yang telah diatur sedemikian rupa pada hari kerja Senin - Sabtu pada pukul 08.00 - 17.00.. </div>

<div class="col-md-12 padding-0 margin-top-20 margin-bottom-20" style="text-align: justify;">2. Jika pada saat pengiriman produk kami , customer tidak berada di tempat , maka kami akan menghubungi customer. Customer dapat memberikan informasi kepada pengiriman / kurir kami , untuk dapat dititipkan di pihak terpercaya. Jika terjadi kerusakan atau kehilangan atau hal - hal lainnya bukan merupakan tanggung jawab dari www.' . $arr['domain'] . '</div>

<div class="col-md-12 padding-0 margin-top-20 margin-bottom-20" style="text-align: justify;">3. Jika customer tidak menerima panggilan telepon dari pengiriman kami , maka secara otomatis kami akan membatalkan pengiriman tersebut. Customer akan mendapatkan sms untuk dapat menghubungi customer service kami : 031-8410062 untuk komunikasi pengiriman lebih lanjut. Proses pengiriman kedua ini akan dikenakan biaya pengiriman yang ditetapkan oleh www.' . $arr['domain'] . '. Jika customer tidak ingin dikenakan biaya pengiriman, customer dapat mengambil produk di depo yang kami siapkan.</div>

<div class="col-md-12 padding-0 margin-top-20 margin-bottom-20" style="text-align: justify;">4. www.' . $arr['domain'] . ' saat ini hanya melayani pengiriman untuk wilayah Indonesia yang dicover oleh perusahaan-perusahaan logistic rekanan kami antara lain JNE, Lion Express, Tiki dll.. Jika kami mendapatkan pengiriman di luar partner rekanan kami, maka www.' . $arr['domain'] . ' akan melakukan refund sesuai dengan nominal pembelian kepada customer.</div>

<div class="col-md-12 padding-0 margin-top-20 margin-bottom-20" style="text-align: justify;">5. www.' . $arr['domain'] . ' untuk saat ini tidak melayani pengiriman internasional.</div>

<div class="col-md-12 padding-0 margin-top-20 margin-bottom-20" style="text-align: justify;">6. Customer tidak dapat membatalkan pesanan, jika status barang tersebut sudah dalam pengiriman ( on shipping ) dengan alasan apapun.</div>

<div class="col-md-12 padding-0 margin-top-20 margin-bottom-20" style="text-align: justify;"> </div>
';

            //OLD Content
//                $post_context = '<div id="14527716675697895383322372775719" class=" font-size24 font-red bold col-md-5 padding-0">						
//Syarat & Ketentuan
//</div>					
//<div id="14527716675697895383af2155920531" class=" col-md-7 padding-0 margin-top-20" style="border-bottom:3px dashed #ccc;">					
//</div>	
//<div id="14527716675697895383eda267919051" class=" col-md-12 padding-0 margin-top-20 margin-bottom-20">
//    Selamat datang di www.' . $arr['domain'] . '.<br> 
//    62hallfamily.com adalah website e-commerce yang menyediakan produk dan jasa sesuai dengan kebutuhan Anda.<br><br>
//    
//Syarat dan ketentuan yang dijabarkan pada poin-poin dibawah ini adalah yang dibuat oleh www.' . $arr['domain'] . ' dengan memperhatikan Anda sebagai customer dari www.' . $arr['domain'] . ' . Customer diwajibkan untuk membaca dengan teliti dan seksama karena customer memiliki kewajiban untuk tunduk pada seluruh syarat dan ketentuan dari website ini. Customer yang melakukan transaksi pembelian produk di website ini, dianggap telah membaca, menyimak, memahami, dan mengerti serta menyetujui semua isi dari Syarat dan Ketentuan ini. Jika customer tidak menyetujui satu atau lebih dari syarat dan ketentuan ini, maka customer tidak diperkenankan menggunakan layanan website www.' . $arr['domain'] . '<br><br> 
//
//<b>A. DEFINISI</b><br>
//1.www.' . $arr['domain'] . ' adalah suatu website e-commerce yang menyediakan produk dan jasa sesuai dengan kebutuhan anda.<br>
//2.Syarat & ketentuan yang tercantum dalam halaman ini adalah persetujuan dari pembeli www.' . $arr['domain'] . ' tentang hak , kewajiban dan tanggung jawab customer dalam penggunaan website www.' . $arr['domain'] . '<br>
//3.Customer adalah pihak - pihak yang melakukan transaksi pembelian produk atau jasa di www.' . $arr['domain'] . '<br>
//4.Member / anggota adalah pihak - pihak yang terdaftar dalam website www.' . $arr['domain'] . ' secara khusus yang mendapatkan keuntungan lebih dari www.' . $arr['domain'] . '<br> 
//5.Rekening 62Pay adalah rekening bersama e-commerce milik PT INDONESIA ENAM DUA yang memberikan kenyamanan dan keamanan bagi customer dalam melakukan transaksi produk dan jasa.<br><br>
//
//<b>B. TRANSAKSI PEMBELIAN</b><br>
//1.Customer yang melakukan transaksi pembelian di www.' . $arr['domain'] . ' bisa langsung melakukan pembelian dan pembayaran tanpa harus terlebih dahulu terdaftar di www.' . $arr['domain'] . '<br> 
//2.Customer sangat disarankan untuk melakukan pendaftaran keanggotaan untuk mendapatkan berbagai keuntungan dan kemudahan dalam bertransaksi di www.' . $arr['domain'] . ' . Informasi lebih lanjut tentang keanggotaan dapat dibaca dan dipelajari di Keuntungan Anggota.<br>
//3.Customer yang melakukan pembelian barang dengan sadar melakukan persetujuan bahwa :<br>
//a)Customer telah membaca, memahami, menyetujui seluruh informasi / deskripsi barang maupun jasa (jenis, fungsi, tipe, warna, kualitas dan lainnya) sebelum melakukan transaksi pembelian barang.<br>
//b)Perihal warna yang tampil di layar monitor, customer membebaskan www.' . $arr['domain'] . ' dalam keakuratan dikarenakan kesadaran bahwa warna produk bergantung dari resolusi monitor yang dilihat oleh masing-masing customer. <br>
//c)Customer juga mengetahui dan sadar bahwa dalam melakukan transaksi pembelian, customer diwajibkan melakukan pembayaran terlebih dahulu baik secara parsial (sebagian) atau full payment (penuh), dimana hal ini akan tergantung dari masing - masing produk dan jasa.<br>
//d)Customer juga bersedia berupa ganti rugi uang kembali (uang yang dibayarkan oleh customer dikembalikan 100%) jika terjadi wanprestasi terhadap produk yang dibeli berupa stok habis. Jika terjadi wanprestasi dalam bentuk lainnya berupa kondisi barang tidak sesuai dalam bentuk apapun, customer bersedia mendapatkan ganti rugi > 50% atau menunggu pergantian barang.<br>
//4.www.' . $arr['domain'] . ' melalui 62Pay berhak untuk membatalkan sepihak jika pembayaran dari customer dengan menggunakan jasa payment gateway (kartu kredit, internet banking, mobile banking) terindikasi gagal. Jika terjadi hal seperti ini, maka customer wajib melakukan pembayaran ulang sesuai dengan arahan yang disampaikan oleh 62Pay.<br>
//5.Customer memahami dan menyetujui bahwa setiap masalah keterlambatan dalam pengiriman barang diluar tanggung jawab dari www.' . $arr['domain'] . ', namun dalam hal ini www.' . $arr['domain'] . ' akan berupaya dengan maksimal dalam pengurusan pengiriman ini kepada pihak terkait.<br>
//6.Customer tidak dapat membatalkan secara sepihak pembelian yang telah dilakukannya kecuali mendapatkan persetujuan tertulis dari www.' . $arr['domain'] . '<br><br>
//</div>';
        } else if ($key == 'informasi-pembayaran') {
            $post_context = '<div id="14527716675697895383322372775719" class=" font-size24 font-red bold col-md-5 padding-0">						
Informasi Pembayaran
</div>					
<div id="14527716675697895383af2155920531" class=" col-md-7 padding-0 margin-top-20" style="border-bottom:3px dashed #ccc;">					
</div>	
<div id="14527716675697895383eda267919051" class=" col-md-12 padding-0 margin-top-20 margin-bottom-20">
Pembayaran Customer akan aman bersama dengan www.' . $arr['domain'] . ' dikarenakan website ini menggunakan layanan 62Pay dari PT INDONESIA ENAM DUA .<br>
Mengapa layanan 62Pay sangat aman ?<br>
- 62Pay adalah perusahaan yang berbadan hukum dengan nama PT INDONESIA ENAM DUA dan bersifat independen.<br>
- Semua dana dari customer akan tertampung di 62Pay dan baru akan dibayarkan kepada merchant setelah barang diterima oleh customer.<br>
- 62Pay melakukan verifikasi dengan baik untuk setiap merchant yang melakukan kerjasama.<br>
Rincian jenis pembayaran 62Pay sebagai berikut :<br><br>
<b>1.Via Transfer ATM</b> <br>
a)Anda bisa melakukan transfer ke rekening BCA , Mandiri , OCBC NISP , CIMB Niaga , BNI , BRI , Permata , Mega dengan atas nama PT INDONESIA ENAM DUA dimana no rekening akan otomatis muncul saat customer melakukan transaksi via www.' . $arr['domain'] . '<br> 
b)Customer melakukan konfirmasi pembayaran via menu konfirmasi pada halaman transaksi.<br>
c)Lengkapi data konfirmasi sesuai dengan jenis pembayaran Customer lalu tekan submit.<br>
<b>2.Via Kartu Kredit / Visa - Master</b><br>
a)Pembayaran ini dilakukan otorisasi otomatis dari perusahaan teknologi di bidang payment gateway yang bekerjasama resmi dengan 62Pay<br>
b)Transaksi minimal dengan menggunakan kartu kredit adalah sebesar Rp 25.000,- dengan biaya transaksi sesuai dengan pihak bank yang mengeluarkan visa dan mastercard.<br> 
<b>3.Via Mandiri Clickpay</b> <br>
a)Customer harus terdaftar dalam layanan mandiri clickpay <br><br>

62Pay memiliki hak untuk melakukan perubahan kebijakan , minimal nominal transaksi serta biaya yang mungkin timbul sesuai dengan regulasi dengan partner pembayaran 
</div>';
        } else if ($key == 'about-us') {
            $post_context = '' . $arr['domain'] . ' -

            Pusat Grosir Busana Muslim Murah Berkualitas menghadirkan koleksi busana muslim dengan

            harga murah dan terjangkau, cocok untuk pilihan busana muslim keluarga dan juga bagi Reseller

            maupun Dropshipper yang ingin memiliki usaha busana muslim online.



            ' . $arr['domain'] . ' memulai usaha sejak awal November 2011 di Fanpage Facebook Muslimah fashion

            kemudian  12 Februari 2012 launcing website www.' . $arr['domain'] . '.

            Alhamdulillah dari tahun ketahun kami terus berkembang sehingga sampai saat ini kami sudah

            memiliki ribuan pelanggan Reseller dan Dropshiper yang tersebar seluruh Indonesia

            dan sebagian dari Malaysia, Hongkong, Taiwan dll.



            Kami mengahdirkan pilihan koleksi Busana Muslim yang sangat lengkap dengan harga murah

            dan terjangkau dengan produk unggulan adalah Gamis Muslimah , Rok dan Celana Muslimah , 

            Jilbab Muslimah Cantik dan Murah , Baju Muslim Anak , Baju Koko dan

            juga Blus dan atasan wanita cantik dan murah . 



            Untuk lebih mengenali kami dan koleksi kami lebih kanjut silahkan bisa pelajari dan

            klik semua link menu kami.



            Terimakasih banyak terlah berkunjung ke Website kami, besar harapan kami anda telah bergabung menjadi bagian dari ribuan pelanggan setia kami.

            Terimakasih';
        } else if ($key == 'privacy-policy') {
            $post_context = '<p style="text-align: center;"><strong>Privacy Policy</strong></p>

            <p><strong>1. INTRODUCTION</strong></p>

            <p>1.1 Welcome to the shopee platform run by Shopee Limited and its affiliates and affiliates (individually and collectively, &quot;Shopee&quot;, &quot;we&quot;, &quot;us&quot; or &quot;our&quot;). Shopee takes its responsibilities under applicable privacy laws and regulations (&quot;Privacy Laws&quot;) seriously and is committed to respecting the privacy rights and concerns of all Users of our Shopee website (the &quot;Site&quot;) (we refer to the Site and the services we provide as described in our Site collectively as the &quot;Services&quot;). We recognize the importance of the personal data you have entrusted to us and believe that it is our responsibility to properly manage, protect and process your personal data. This Privacy Policy (&ldquo;Privacy Policy&rdquo; or &ldquo;Policy&rdquo;) is designed to assist you in understanding how we collect, use, disclose and/or process the personal data you have provided to us and/or we possess about you, whether now or in the future, as well as to assist you in making an informed decision before providing us with any of your personal data. Please read this Privacy Policy carefully. If you have any questions regarding this information or our privacy practices, please see the section entitled &quot;Questions, Concerns or Complaints? Contact Us&quot; at the end of this Privacy Policy.</p>

            <p>1.2 &quot;Personal Data&quot; or &quot;personal data&quot; means data, whether true or not, about an individual who can be identified from that data, or from that data and other information to which an organisation has or is likely to have access. Common examples of personal data could include name, identification number and contact information.</p>

            <p>1.3 By using the Services, registering for an account with us, visiting our website, or accessing the Services, you acknowledge and agree that you accept the practices, requirements, and/or policies outlined in this Privacy Policy, and you hereby consent to us collecting, using, disclosing and/or processing your personal data as described herein. IF YOU DO NOT CONSENT TO THE PROCESSING OF YOUR PERSONAL DATA AS DESCRIBED IN THIS PRIVACY POLICY, PLEASE DO NOT USE OUR SERVICES OR ACCESS OUR WEBSITE. If we change our Privacy Policy, we will post those changes or the amended Privacy Policy on our website. We reserve the right to amend this Privacy Policy at any time.</p>

            <p>&nbsp;</p>

            <p><strong>2.</strong>&nbsp;<strong>WHEN WILL SHOPEE COLLECT PERSONAL DATA?</strong></p>

            <p>2.1 We will/may collect personal data about you:</p>

            <p>(a) when you register and/or use our Services or Site, or open an account with us;</p>

            <p>(b) when you submit any form, including, but not limited to, application forms or other forms relating to any of our products and services, whether online or by way of a physical form;</p>

            <p>(c) when you enter into any agreement or provide other documentation or information in respect of your interactions with us, or when you use our products and services;</p>

            <p>(d) when you interact with us, such as via telephone calls (which may be recorded), letters, fax, face-to-face meetings, social media platforms and emails;</p>

            <p>(e) when you use our electronic services, or interact with us via our application or use services on our website. This includes, without limitation, through cookies which we may deploy when you interact with our application or website;</p>

            <p>(f) when you carry out transactions through our Services;</p>

            <p>(g) when you provide us with feedback or complaints;</p>

            <p>(h) when you register for a contest; or</p>

            <p>(i) when you submit your personal data to us for any reason.</p>

            <p>The above does not purport to be exhaustive and sets out some common instances of when personal data about you may be collected.</p>

            <p>2.2 When you visit, use or interact with our mobile application or the Site, we may collect certain information by automated or passive means using a variety of technologies, which technologies may be downloaded to your device and may set or modify settings on your device. The information we collect may include, without limitation, your Internet Protocol (IP) address, computer/mobile device operating system and browser type, type of mobile device, the characteristics of the mobile device, the unique device identifier (UDID) or mobile equipment identifier (MEID) for your mobile device, the address of a referring web site (if any), and the pages you visit on our website and mobile applications and the times of visit. We may collect, use disclose and/or process this information only for the Purposes (defined below).</p>

            <p>2.3 Our mobile applications may collect precise information about the location of your mobile device using technologies such as GPS, Wi-Fi, etc.. We collect, use, disclose and/or process this information for one or more Purposes including, without limitation, location-based services that you request or to deliver relevant content to you based on your location or to allow you to share your location to other Users as part of the services under our mobile applications. For most mobile devices, you are able to withdraw your permission for us to acquire this information on your location through your device settings. If you have questions about how to disable your mobile device&#39;s location services, please contact your mobile device service provider or the device manufacturer.</p>

            <p><strong>3. WHAT PERSONAL DATA WILL SHOPEE COLLECT?</strong></p>

            <p>3.1 The personal data that Shopee may collect includes but is not limited to:</p>

            <p>&bull; name;</p>

            <p>&bull; email address;</p>

            <p>&bull; date of birth;</p>

            <p>&bull; billing address;</p>

            <p>&bull; bank account and payment information;</p>

            <p>&bull; telephone number;</p>

            <p>&bull; gender;</p>

            <p>&bull; any other information about the User when the User signs up to use our Services or website, and when the User uses the Services or website, as well as information related to how the User uses our Services or website; and</p>

            <p>&bull; aggregate data on content the User engages with.</p>

            <p>3.2 If you do not want us to collect the aforementioned information/personal data, you may opt out at any time by notifying our Data Protection Officer in writing about it. Further information on opting out can be found in the section below entitled &quot;How can you opt-out, remove, request access to or modify information you have provided to us?&quot; . Note, however, that opting out of us collecting your personal data or withdrawing your consent for us to collect, use or process your personal data may affect your use of the Services. For example, opting out of the collection of location information will cause its location-based features to be disabled.</p>

            <p>&nbsp;</p>

            <p><strong>4. SETTING UP AN ACCOUNT</strong></p>

            <p>&nbsp;</p>

            <p>In order to use certain functionalities of the Services, you will have to create a user account which requires you to submit certain personal data. When you register and create an account, we require you to provide us with your name and email address as well as a user name that you select. We also ask for certain information about yourself such as your telephone number, email address, shipping address, photo identification, bank account details, age, date of birth, gender and interests. Upon activating an account, you will select a user name and password. Your user name and password will be used so you can securely access and maintain your account.</p>

            <p>&nbsp;</p>

            <p><strong>5. VIEWING WEB PAGES</strong></p>

            <p>As with most websites, your computer sends information which may include personal data about you that gets logged by a web server when you browse our Site. This typically includes without limitation your computer&#39;s IP address, operating system, browser name/version, the referring web page, requested page, date/time, and sometimes a &quot;cookie&quot; (which can be disabled using your browser preferences) to help the site remember your last visit. If you are logged in, this information is associated with your personal account. The information is also included in anonymous statistics to allow us to understand how visitors use our site.</p>

            <p>&nbsp;</p>

            <p><strong>6. COOKIES</strong></p>

            <p>6.1 We may from time to time implement &quot;cookies&quot; or other features to allow us or third parties to collect or share information that will help us improve our Site and the Services we offer, or help us offer new services and features. &ldquo;Cookies&rdquo; are identifiers we transfer to your computer or mobile device that allow us to recognize your computer or device and tell us how and when the Services or website are used or visited, by how many people and to track movements within our website. We may link cookie information to personal data. Cookies also link to information regarding what items you have selected for purchase and pages you have viewed. This information is used to keep track of your shopping cart, for example. Cookies are also used to deliver content specific to your interest and to monitor website usage.</p>

            <p>6.2 You may refuse the use of cookies by selecting the appropriate settings on your browser. However, please note that if you do this you may not be able to use the full functionality of our Site or the Services.</p>

            <p>&nbsp;</p>

            <p><strong>7. VIEWING AND DOWNLOADING CONTENT AND ADVERTISING</strong></p>

            <p>As with browsing web pages, when you watch content and advertising and access other software on our Site or through the Services, most of the same information is sent to us (including, without limitation, IP Address, operating system, etc.); but, instead of page views, your computer sends us information on the content, advertisement viewed and/or software installed by the Services and the website and time.</p>

            <p>&nbsp;</p>

            <p><strong>8. COMMUNITY &amp; SUPPORT</strong></p>

            <p>8.1 We provide customer service support through email, SMS and feedback forms. In order to provide customer support, we will ask for your email address and mobile phone number. We only use information received from customer support requests, including, without limitation, email addresses, for customer support services and we do not transfer to or share this information with any third parties.</p>

            <p>8.2 You can also post questions and answer other User questions in our community forums. Our forum and messaging services allow you to participate in our community; to do so, we maintain information, such as your user ID, contact list and status messages. In addition, these and similar services in the future may require us to maintain your user ID and password.</p>

            <p>&nbsp;</p>

            <p><strong>9. SURVEYS</strong></p>

            <p>From time-to-time, we may request information from Users via surveys. Participation in these surveys is completely voluntary and you therefore have a choice whether or not to disclose your information to us. Information requested may include, without limitation, contact information (such as your email address), and demographic information (such as interests or age level). Survey information will be used for the purposes of monitoring or improving the use and satisfaction of the Services and will not be transferred to third parties, other than our contractors who help us to administer or act upon the survey.</p>

            <p>&nbsp;</p>

            <p><strong>10. HOW DO WE USE THE INFORMATION YOU PROVIDE US?</strong></p>

            <p>10.1 We may collect, use, disclose and/or process your personal data for one or more of the following purposes:</p>

            <p>(a) to consider and/or process your application/transaction with us or your transactions or communications with third parties via the Services;</p>

            <p>(b) to manage, operate, provide and/or administer your use of and/or access to our Services and our website, as well as your relationship and user account with us;</p>

            <p>(c) to manage, operate, administer and provide you with as well as to facilitate the provision of our Services, including, without limitation, remembering your preferences;</p>

            <p>(d) to tailor your experience through the Services by displaying content according to your interests and preferences, providing a faster method for you to access your account and submit information to us and allowing us to contact you, if necessary;</p>

            <p>(e) to respond to, process, deal with or complete a transaction and/or to fulfil your requests for certain products and services and notify you of service issues and unusual account actions;</p>

            <p>(f) to enforce our Terms of Service or any applicable end user license agreements;</p>

            <p>(g) to protect personal safety and the rights, property or safety of others;</p>

            <p>(h) for identification and/or verification;</p>

            <p>(i) to maintain and administer any software updates and/or other updates and support that may be required from time to time to ensure the smooth running of our Services;</p>

            <p>(j) to deal with or facilitate customer service, carry out your instructions, deal with or respond to any enquiries given by (or purported to be given by) you or on your behalf;</p>

            <p>(k) to contact you or communicate with you via voice call, text message and/or fax message, email and/or postal mail or otherwise for the purposes of administering and/or managing your relationship with us or your use of our Services, such as but not limited to communicating administrative information to you relating to our Services. You acknowledge and agree that such communication by us could be by way of the mailing of correspondence, documents or notices to you, which could involve disclosure of certain personal data about you to bring about delivery of the same as well as on the external cover of envelopes/mail packages;</p>

            <p>(l) to inform you when another User has sent you a private message or posted a comment for you on the Site;</p>

            <p>(m) to conduct research, analysis and development activities (including, but not limited to, data analytics, surveys, product and service development and/or profiling), to analyse how you use our Services, to improve our Services or products and/or to enhance your customer experience;</p>

            <p>(n) to allow for audits and surveys to, among other things, validate the size and composition of our target audience, and understand their experience with Shopee&rsquo;s Services;</p>

            <p>(o) where you give us your prior consent, for marketing and in this regard, to send you by various modes of communication such as postal mail, email, location-based services or otherwise, marketing and promotional information and materials relating to products and/or services (including, without limitation, products and/or services of third parties whom Shopee may collaborate or tie up with) that Shopee (and/or its affiliates or related corporations) may be selling, marketing or promoting, whether such products or services exist now or are created in the future. If you are in Singapore, In the case of the sending of marketing or promotional information to you by voice call, SMS/MMS or fax to your Singapore facsimile number, we will not do so unless we have complied with the requirements of Singapore&rsquo;s Privacy Laws in relation to use of such latter modes of communication in sending you marketing information or you have expressly consented to the same;</p>

            <p>(p) to respond to legal processes or to comply with or as required by any applicable law, governmental or regulatory requirements of any relevant jurisdiction, including, without limitation, meeting the requirements to make disclosure under the requirements of any law binding on Shopee or on its related corporations or affiliates;</p>

            <p>(q) to produce statistics and research for internal and statutory reporting and/or record-keeping requirements;</p>

            <p>(r) to carry out due diligence or other screening activities (including, without limitation, background checks) in accordance with legal or regulatory obligations or our risk management procedures that may be required by law or that may have been put in place by us;</p>

            <p>(s) to audit our Services or Shopee&#39;s business;</p>

            <p>(t) to prevent or investigate any fraud, unlawful activity, omission or misconduct, whether relating to your use of our Services or any other matter arising from your relationship with us, and whether or not there is any suspicion of the aforementioned;</p>

            <p>(u) to store, host, back up (whether for disaster recovery or otherwise) of your personal data, whether within or outside of your jurisdiction;</p>

            <p>(v) to deal with and/or facilitate a business asset transaction or a potential business asset transaction, where such transaction involves Shopee as a participant or involves only a related corporation or affiliate of Shopee as a participant or involves Shopee and/or any one or more of Shopee&#39;s related corporations or affiliates as participant(s), and there may be other third party organisations who are participants in such transaction. A &ldquo;business asset transaction&rdquo; refers to the purchase, sale, lease, merger, amalgamation or any other acquisition, disposal or financing of an organisation or a portion of an organisation or of any of the business or assets of an organisation; and/or</p>

            <p>(w) any other purposes which we notify you of at the time of obtaining your consent.</p>

            <p>(collectively, the &ldquo;<strong>Purposes</strong>&rdquo;).</p>

            <p>10.2 As the purposes for which we will/may collect, use, disclose or process your personal data depend on the circumstances at hand, such purpose may not appear above. However, we will notify you of such other purpose at the time of obtaining your consent, unless processing of the applicable data without your consent is permitted by the Privacy Laws.</p>

            <p>&nbsp;</p>

            <p>&nbsp;</p>

            <p><strong>11. SHARING OF INFORMATION FROM THE SERVICES</strong></p>

            <p>Our Services enable Users to share personal information with each other, in almost all occasions without Shopee&rsquo;s involvement, to complete transactions. In a typical transaction, Users may have access to each other&rsquo;s name, user ID, email address and other contact and postage information. Our Terms of Service require that Users in possession of another User&rsquo;s personal data (the &ldquo;Receiving Party&rdquo;) must (i) comply with all applicable Privacy Laws; (ii) allow the other User (the &ldquo;Disclosing Party&rdquo;) to remove him/herself from the Receiving Party&rsquo;s database; and (iii) allow the Disclosing Party to review what information have been collected about them by the Receiving Party.</p>

            <p>&nbsp;</p>

            <p><strong>12. HOW DOES SHOPEE PROTECT CUSTOMER INFORMATION?</strong></p>

            <p>We implement a variety of security measures to ensure the security of your personal data on our systems. User personal data is contained behind secured networks and is only accessible by a limited number of employees who have special access rights to such systems. We will retain personal data in accordance with the Privacy Laws and/or other applicable laws. That is, we will destroy or anonymize your personal data as soon as it is reasonable to assume that (i) the purpose for which that personal data was collected is no longer being served by the retention of such personal data; and (ii) retention is no longer necessary for any legal or business purposes. If you cease using the Site, or your permission to use the Site and/or the Services is terminated, we may continue storing, using and/or disclosing your personal data in accordance with this Privacy Policy and our obligations under the Privacy Laws. Subject to applicable law, we may securely dispose of your personal data without prior notice to you.</p>

            <p>&nbsp;</p>

            <p><strong>13. DOES SHOPEE DISCLOSE THE INFORMATION IT COLLECTS FROM ITS VISITORS TO OUTSIDE PARTIES?</strong></p>

            <p>13.1 In conducting our business, we will/may need to disclose your personal data to our third party service providers, agents and/or our affiliates or related corporations, and/or other third parties, whether sited in Singapore or outside of Singapore, for one or more of the above-stated Purposes. Such third party service providers, agents and/or affiliates or related corporations and/or other third parties would be processing your personal data either on our behalf or otherwise, for one or more of the above-stated Purposes. Such third parties include, without limitation:</p>

            <p>(a) our subsidiaries, affiliates and related corporations;</p>

            <p>(b) contractors, agents, service providers and other third parties we use to support our business. These include but are not limited to those which provide administrative or other services to us such as mailing houses, telecommunication companies, information technology companies and data centres;</p>

            <p>(c) a buyer or other successor in the event of a merger, divestiture, restructuring, reorganization, dissolution or other sale or transfer of some or all of Shopee&rsquo;s assets, whether as a going concern or as part of bankruptcy, liquidation or similar proceeding, in which personal data held by Shopee about our Service Users is among the assets transferred; or to a counterparty in a business asset transaction that Shopee or any of its affiliates or related corporations is involved in; and</p>

            <p>(d) third parties to whom disclosure by us is for one or more of the Purposes and such third parties would in turn be collecting and processing your personal data for one or more of the Purposes</p>

            <p>13.2 This may require, among other things, share statistical and demographic information about our Users and their use of the Services with suppliers of advertisements and programming. This would not include anything that could be used to identify you specifically or to discover individual information about you.</p>

            <p>13.3 For the avoidance of doubt, in the event that Privacy Laws or other applicable laws permit an organisation such as us to collect, use or disclose your personal data without your consent, such permission granted by the laws shall continue to apply.</p>

            <p>13.4 Third parties may unlawfully intercept or access personal data transmitted to or contained on the site, technologies may malfunction or not work as anticipated, or someone might access, abuse or misuse information through no fault of ours. We will nevertheless deploy reasonable security arrangements to protect your personal data as required by the Privacy Laws; however there can inevitably be no guarantee of absolute security such as but not limited to when unauthorised disclosure arises from malicious and sophisticated hacking by malcontents through no fault of ours.</p>

            <p>&nbsp;</p>

            <p><strong>14. INFORMATION ON CHILDREN</strong></p>

            <p>The Services are not intended for children under the age of 10. We do not knowingly collect or maintain any personal data or non-personally-identifiable information from anyone under the age of 10 nor is any part of our Site or other Services directed to children under the age of 10. We will close any accounts used exclusively by such children and will remove and/or delete any personal data we believe was submitted by any child under the age of 10.</p>

            <p>&nbsp;</p>

            <p><strong>15. INFORMATION COLLECTED BY THIRD PARTIES</strong></p>

            <p>15.1 Our Site uses Google Analytics, a web analytics service provided by Google, Inc. (&quot;Google&quot;). Google Analytics uses cookies, which are text files placed on your computer, to help the website analyse how Users use the Site. The information generated by the cookie about your use of the website (including your IP address) will be transmitted to and stored by Google on servers in the United States. Google will use this information for the purpose of evaluating your use of the website, compiling reports on website activity for website operators and providing other services relating to website activity and Internet usage. Google may also transfer this information to third parties where required to do so by law, or where such third parties process the information on Google&#39;s behalf. Google will not associate your IP address with any other data held by Google.</p>

            <p>15.2 We, and third parties, may from time to time make software applications downloads available for your use on or through the Services. These applications may separately access, and allow a third party to view, your identifiable information, such as your name, your user ID, your computer&#39;s IP Address or other information such as any cookies that you may previously have installed or that were installed for you by a third party software application or website. Additionally, these applications may ask you to provide additional information directly to third parties. Third party products or services provided through these applications are not owned or controlled by Shopee. You are encouraged to read the terms and other policies published by such third parties on their websites or otherwise.</p>

            <p><strong>16. DISCLAIMER REGARDING SECURITY AND THIRD PARTY SITES</strong></p>

            <p>16.1 WE DO NOT GUARANTEE THE SECURITY OF PERSONAL DATA AND/OR OTHER INFORMATION THAT YOU PROVIDE ON THIRD PARTY SITES. We do implement a variety of security measures to maintain the safety of your personal data that is in our possession or under our control. Your personal data is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the personal data confidential. When you place orders or access your personal data, we offer the use of a secure server. All personal data or sensitive information you supply is encrypted into our databases to be only accessed as stated above.</p>

            <p>16.2 In an attempt to provide you with increased value, we may choose various third party websites to link to, and frame within, the Site. We may also participate in co-branding and other relationships to offer e-commerce and other services and features to our visitors. These linked sites have separate and independent privacy policies as well as security arrangements. Even if the third party is affiliated with us, we have no control over these linked sites, each of which has separate privacy and data collection practices independent of us. Data collected by our co-brand partners or third party web sites (even if offered on or through our Site) may not be received by us.</p>

            <p>16.3 We therefore have no responsibility or liability for the content, security arrangements (or lack thereof) and activities of these linked sites. These linked sites are only for your convenience and you therefore access them at your own risk. Nonetheless, we seek to protect the integrity of our Site and the links placed upon each of them and therefore welcome any feedback about these linked sites (including, without limitation, if a specific link does not work).</p>

            <p>&nbsp;</p>

            <p><strong>17. WILL SHOPEE TRANSFER YOUR INFORMATION OVERSEAS?</strong></p>

            <p>Your personal data and/or information may be transferred to, stored or processed outside of your country. In most cases, your personal data will be processed in Singapore, where our servers are located and our central database is operated. Shopee will only transfer your information overseas in accordance with Privacy Laws.</p>

            <p><strong>18. HOW CAN YOU OPT-OUT, REMOVE, REQUEST ACCESS TO OR MODIFY INFORMATION YOU HAVE PROVIDED TO US?</strong></p>

            <p>18.1<em>&nbsp;<u>Opting Out and Withdrawing Consent</u></em></p>

            <p>18.1.1 To modify your email subscriptions, please let us know by sending an email to our Personal Data Protection Officer at the address listed below. Please note that due to email production schedules, you may still receive emails that are already in production.</p>

            <p>18.1.2 You may withdraw your consent for the collection, use and/or disclosure of your personal data in our possession or under our control by sending an email to our Personal Data Protection Officer at the email address listed below in Section 19.2.</p>

            <p>18.1.3 Once we have your clear withdrawal instructions and verified your identity, we will process your request for withdrawal of consent, and will thereafter not collect, use and/or disclose your personal data in the manner stated in your request. If we are unable to verify your identity or understand your instructions, we will liaise with you to understand your request.</p>

            <p>18.1.4 However, your withdrawal of consent could result in certain legal consequences arising from such withdrawal. In this regard, depending on the extent of your withdrawal of consent for us to process your personal data, it may mean that we will not be able to continue providing the Services to you, we may need to terminate your existing relationship and/or the contract you have with us, etc., as the case may be, which we will inform you of.</p>

            <p>18.2&nbsp;<em><u>Requesting Access and/or Correction of Personal Data</u></em></p>

            <p>18.2.1 If you have an account with us, you may personally access and/or correct your personal data currently in our possession or control through the Account Settings page on the Site. If you do not have an account with us, you may request to access and/or correct your personal data currently in our possession or control by submitting a written request to us. We will need enough information from you in order to ascertain your identity as well as the nature of your request so as to be able to deal with your request. Hence, please submit your written request by sending an email to our Personal Data Protection Officer at the email address listed below in Section 19.2.</p>

            <p>18.2.2 For a request to access personal data, once we have sufficient information from you to deal with the request, we will seek to provide you with the relevant personal data within 30 days (or, if you are resident in Malaysia, 21 days). Where we are unable to respond to you within the said 30 days (or, if you are resident in Malaysia, 21 days), we will notify you of the soonest possible time within which we can provide you with the information requested. Note that Privacy Laws may exempt certain types of personal data from being subject to your access request.</p>

            <p>18.2.3 For a request to correct personal data, once we have sufficient information from you to deal with the request, we will:</p>

            <p>(a) correct your personal data within 30 days (or, if you are resident in Malaysia, 21 days). Where we are unable to do so within the said period, we will notify you of the soonest practicable time within which we can make the correction. Note that Privacy Laws may exempt certain types of personal data from being subject to your correction request as well as provides for situation(s) when correction need not be made by us despite your request; and</p>

            <p>(b) we will send the corrected personal data to every other organisation to which the personal data was disclosed by us within a year before the date the correction was made, unless that other organisation does not need the corrected personal data for any legal or business purpose.</p>

            <p>18.2.4 Notwithstanding sub-paragraph (b) immediately above, we may, if you so request, send the corrected personal data only to specific organisations to which the personal data was disclosed by us within a year before the date the correction was made.</p>

            <p>18.2.5 We will/may also be charging you a reasonable fee for the handling and processing of your requests to access your personal data. If we so choose to charge, we will provide you with a written estimate of the fee we will be charging. Please note that we are not required to respond to or deal with your access request unless you have agreed to pay the fee.</p>

            <p>18.2.6 We reserve the right to refuse to correct your personal data in accordance with the provisions as set out in Privacy Laws, where they require and/or entitle an organisation to refuse to correct personal data in stated circumstances.</p>

            <p>&nbsp;</p>

            <p><strong>19. QUESTIONS, CONCERNS OR COMPLAINTS? CONTACT US</strong></p>

            <p>19.1 If you have any questions or concerns about our privacy practices or your dealings with the Services, please do not hesitate to contact:<a href="mailto:support@shopee.sg">support@shopee.sg</a>.</p>

            <p>19.2 If you have any complaint or grievance regarding how we are handling your personal data or about how we are complying with Privacy Laws, we welcome you to contact us with your complaint or grievance.</p>

            <p>19.3 Where it is an email or a letter through which you are submitting a complaint, your indication at the subject header that it is a Privacy Law complaint would assist us in attending to your complaint speedily by passing it on to the relevant staff in our organisation to handle. For example, you could insert the subject header as &ldquo;Privacy Complaint&rdquo;.</p>

            <p>We will certainly strive to deal with any complaint or grievance that you may have fairly and as soon as possible.</p>

            <p>&nbsp;</p>

            <p><strong>20. TERMS AND CONDITIONS</strong></p>

            <p>Please also read the Terms of Service establishing the use, disclaimers, and limitations of liability governing the use of the Site and the Services and other related policies.</p>
            ';
        } else if ($key == 'term_and_condition') {
            $post_context = '<p style="text-align: center;"><span style="font-size:18px"><strong>KEBIJAKAN PRIVASI</strong></span></p>

                <p style="text-align: center;">&nbsp;</p>

                <p style="text-align: center;">&nbsp;</p>

                <p>KEBIJAKAN PRIVASI<br />
                <br />
                <strong>1. PENDAHULUAN</strong><br />
                <br />
                1.1 Selamat datang di platform ' . $arr['name'] . ' yang dioperasikan oleh ' . $arr['name'] . ' Limited dan afiliasinya (secara sendiri-sendiri dan bersama-sama juga disebut &quot;' . $arr['name'] . '&quot; atau &quot;kami&quot;). ' . $arr['name'] . ' dengan serius bertanggung jawab berdasarkan peraturan perundang-undangan tentang privasi yang berlaku (&quot;Undang-Undang Privasi&quot;) dan berkomitmen untuk menghormati hak dan masalah privasi semua Pengguna situs web ' . $arr['name'] . ' kami (&quot;Situs&quot;) (secara bersama-sama kami menyebut Situs dan layanan yang kami sediakan sebagaimana yang dijelaskan dalam Situs kami sebagai &quot;Layanan&quot;). Kami mengakui pentingnya data pribadi yang telah Anda percayakan kepada kami dan percaya bahwa merupakan tanggung jawab kami untuk mengelola, melindungi dan mengolah data pribadi Anda dengan baik. Kebijakan Privasi ini (&quot;Kebijakan Privasi&quot; atau &quot;Kebijakan&quot;) dirancang untuk membantu Anda memahami bagaimana kami mengumpulkan, menggunakan, mengungkapkan dan/atau mengolah data pribadi yang telah Anda percayakan kepada kami dan/atau kami miliki tentang Anda, baik di masa sekarang maupun di masa mendatang, serta untuk membantu Anda membuat keputusan yang tepat sebelum memberikan data pribadi Anda kepada kami. Silakan baca Kebijakan Privasi ini dengan cermat. Apabila Anda memiliki pertanyaan tentang informasi ini atau praktik privasi kami, silakan lihat bagian yang berjudul &quot;Pertanyaan, Masalah atau Keluhan? Hubungi Kami&quot; di akhir Kebijakan Privasi ini.<br />
                <br />
                1.2 &quot;Data Pribadi&quot; atau &quot;data pribadi&quot; berarti data, baik benar maupun tidak, tentang individu yang dapat diidentifikasi dari data tersebut, atau dari data dan informasi lainnya yang dapat atau kemungkinan dapat diakses oleh suatu organisasi. Contoh umum data pribadi dapat mencakup nama, nomor identifikasi dan informasi kontak.<br />
                <br />
                1.3 Dengan menggunakan Layanan, mendaftarkan akun pada kami, mengunjungi situs web kami, atau mengakses Layanan, Anda mengakui dan setuju bahwa Anda menerima praktik, persyaratan, dan/atau kebijakan yang diuraikan dalam Kebijakan Privasi ini, dan Anda dengan ini mengizinkan kami untuk mengumpulkan, menggunakan, mengungkapkan dan/atau mengolah data pribadi Anda seperti yang dijelaskan di sini. APABILA ANDA TIDAK MENGIZINKAN PENGOLAHAN DATA PRIBADI ANDA SEPERTI YANG DIJELASKAN DALAM KEBIJAKAN PRIVASI INI, MOHON JANGAN MENGGUNAKAN LAYANAN KAMI ATAU MENGAKSES SITUS WEB KAMI. Apabila kami mengubah Kebijakan Privasi kami, kami akan memposting perubahan tersebut atau Kebijakan Privasi yang telah diubah pada situs web kami. Kami berhak untuk mengubah Kebijakan Privasi ini setiap saat.<br />
                <br />
                <br />
                <strong>2. KAPAN ' . $arr['name'] . ' AKAN MENGUMPULKAN DATA PRIBADI?</strong><br />
                <br />
                2.1 Kami akan/mungkin mengumpulkan data pribadi Anda:<br />
                <br />
                (a) saat Anda mendaftar dan/atau menggunakan Layanan atau Situs kami, atau membuka sebuah akun dengan kami.<br />
                <br />
                (b) saat Anda mengirimkan formulir apapun, termasuk, tetapi tidak terbatas pada, formulir permohonan atau formulir lainnya yang berkaitan dengan produk dan layanan kami, baik secara online maupun dalam bentuk fisik;<br />
                <br />
                (c) saat anda membuat perjanjian atau memberikan dokumen atau informasi lainnya sehubungan dengan interaksi Anda dengan kami, atau saat Anda menggunakan produk dan layanan kami;<br />
                <br />
                (d) saat Anda berinteraksi dengan kami, seperti melalui sambungan telepon (yang mungkin direkam), surat, faks, pertemuan tatap muka, platform media sosial dan email;<br />
                <br />
                (e) saat Anda menggunakan layanan elektronik kami, atau berinteraksi dengan kami melalui aplikasi kami atau menggunakan layanan di situs web kami. Ini termasuk, dengan tidak terbatas pada, melalui cookies yang mungkin kami gunakan saat Anda berinteraksi dengan aplikasi atau situs web kami;<br />
                <br />
                (f) saat Anda melakukan transaksi melalui Layanan kami;<br />
                <br />
                (g) saat Anda menyampaikan kritik dan saran atau keluhan kepada kami;<br />
                <br />
                (h) saat Anda mendaftar untuk suatu kontes; atau<br />
                <br />
                (i) saat Anda mengirimkan data pribadi Anda kepada kami dengan alasan apapun.<br />
                <br />
                Daftar di atas tidak dimaksudkan sebagai suatu daftar yang lengkap dan hanya menetapkan beberapa contoh umum tentang kapan data pribadi Anda mungkin diambil.<br />
                <br />
                2.3 Saat Anda mengunjungi, menggunakan atau berinteraksi dengan aplikasi mobile atau Situs kami, kami mungkin mengumpulkan informasi tertentu secara otomatis atau pasif dengan menggunakan berbagai teknologi, di mana teknologi tersebut mungkin diunduh ke perangkat Anda dan mungkin menetapkan atau mengubah pengaturan pada perangkat Anda. Informasi yang kami kumpulkan dapat termasuk, dengan tidak terbatas pada, alamat Internet Protocol (IP), sistem operasi dan jenis peramban komputer/perangkat mobile, jenis perangkat mobile, karakteristik perangkat mobile, unique device identifier (UDID) atau mobile equipment identifier (MEID) untuk perangkat mobile Anda, alamat situs perujuk (jika ada), dan halaman yang Anda kunjungi pada situs web dan aplikasi mobile kami serta jumlah kunjungan. Kami hanya dapat mengumpulkan, menggunakan, mengungkapkan dan/atau mengolah informasi ini untuk Tujuan (didefinisikan di bawah).<br />
                <br />
                2.4 Aplikasi mobile kami mungkin mengumpulkan informasi yang tepat tentang lokasi perangkat mobile Anda dengan menggunakan teknologi seperti GPS, Wi-Fi, dsb. Kami mengumpulkan, menggunakan, mengungkapkan dan/atau mengolah informasi ini untuk satu Tujuan atau lebih termasuk, dengan tidak terbatas pada, layanan berbasis lokasi yang Anda minta atau untuk mengirimkan konten yang relevan dengan Anda berdasarkan lokasi Anda atau untuk memungkinkan Anda berbagi lokasi Anda dengan Pengguna lainnya sebagai bagian dari layanan aplikasi mobile kami. Untuk kebanyakan perangkat mobile, Anda dapat menarik izin Anda untuk kami memperoleh informasi tentang lokasi Anda ini melalui pengaturan perangkat Anda. Apabila Anda memiliki pertanyaan tentang bagaimana cara menonaktifkan layanan lokasi perangkat mobile Anda, silakan hubungi penyedia layanan perangkat mobile atau produsen perangkat Anda.<br />
                <br />
                <strong>3. DATA PRIBADI APA YANG AKAN DIKUMPULKAN OLEH ' . $arr['name'] . '?</strong><br />
                <br />
                3.1 Data pribadi yang mungkin dikumpulkan ' . $arr['name'] . ' termasuk tetapi tidak terbatas pada:<br />
                <br />
                &bull; nama;<br />
                <br />
                &bull; alamat email;<br />
                <br />
                &bull; tanggal lahir;<br />
                <br />
                &bull; alamat tagihan;<br />
                <br />
                &bull; rekening bank dan informasi pembayaran;<br />
                <br />
                &bull; nomor telepon;<br />
                <br />
                &bull; jenis kelamin;<br />
                <br />
                &bull; informasi lain apapun tentang Pengguna saat Pengguna mendaftarkan diri untuk menggunakan Layanan atau situs web kami, dan saat Pengguna menggunakan Layanan atau situs web, serta informasi yang berkaitan dengan bagaimana Pengguna menggunakan Layanan atau situs web kami; dan<br />
                <br />
                &bull; seluruh data tentang konten yang digunakan Pengguna.<br />
                <br />
                3.2 Apabila Anda tidak ingin kami mengumpulkan informasi/data pribadi yang disebutkan di atas, Anda dapat memilih keluar setiap saat dengan memberitahu Petugas Perlindungan Data kami secara tertulis tentang hal tersebut. Informasi lebih lanjut tentang memilih keluar dapat ditemukan di bagian di bawah ini yang berjudul &quot; Bagaimana Anda dapat memilih keluar, menghapus, meminta akses atau mengubah informasi yang telah Anda berikan kepada kami?&quot; . Akan tetapi harap diperhatikan bahwa memilih keluar dari kami mengumpulkan data pribadi Anda atau menarik izin Anda untuk kami mengumpulkan, menggunakan atau mengolah data pribadi Anda dapat memengaruhi penggunaan Layanan oleh Anda. Sebagai contoh, memilih keluar dari pengumpulan informasi lokasi akan menyebabkan dinonaktifkannya fitur berbasis lokasi.<br />
                <br />
                <br />
                <strong>4. MEMBUAT AKUN</strong><br />
                <br />
                <br />
                Untuk menggunakan fungsi tertentu dari Layanan, Anda harus membuat akun pengguna yang mengharuskan Anda untuk menyerahkan data pribadi tertentu. Saat Anda mendaftar dan membuat akun, kami mewajibkan Anda untuk memberikan nama dan alamat email Anda serta nama pengguna yang Anda pilih kepada kami. Kami juga menanyakan informasi tertentu tentang diri Anda seperti nomor telepon, alamat email, alamat pengiriman, identifikasi foto, rincian rekening bank, umur, tanggal lahir, jenis kelamin dan minat Anda. Setelah mengaktifkan akun, Anda akan memilih nama pengguna dan kata sandi. Nama pengguna dan kata sandi Anda akan digunakan agar Anda dapat mengakses dan mengelola akun Anda dengan aman.<br />
                <br />
                <br />
                <strong>5. MELIHAT HALAMAN WEB</strong><br />
                <br />
                Sama seperti kebanyakan situs web, komputer Anda mengirimkan informasi yang mungkin termasuk data pribadi tentang Anda yang dicatat oleh server web saat Anda menjelajahi Situs kami. Ini biasanya termasuk dengan tidak terbatas pada alamat IP komputer, sistem operasi, nama/versi peramban Anda, halaman web perujuk, halaman yang diminta, tanggal/waktu, dan terkadang &quot;cookie&quot; (yang dapat dinonaktifkan dengan menggunakan preferensi peramban Anda) guna membantu situs mengingat kunjungan terakhir Anda. Apabila Anda login, informasi ini dikaitkan dengan akun pribadi Anda. Informasi juga disertakan dalam statistik anonim untuk memungkinkan kami memahami bagaimana pengunjung menggunakan situs kami.<br />
                <br />
                <br />
                <strong>6. COOKIES</strong><br />
                <br />
                6.1 Dari waktu ke waktu kami mungkin mengimplementasikan &quot;cookies&quot; atau fitur lainnya guna memungkinkan kami atau pihak ketiga untuk mengumpulkan atau berbagi informasi yang akan membantu kami meningkatkan Situs kami dan Layanan yang kami tawarkan, atau membantu kami menawarkan layanan dan fitur baru. &ldquo;Cookies&rdquo; adalah pengidentifikasi yang kami transfer ke komputer atau perangkat lunak Anda yang memungkinkan kami mengenali komputer atau perangkat Anda dan memberi tahu kami bagaimana dan kapan Layanan atau situs web digunakan atau dikunjungi, oleh berapa banyak orang serta melacak pergerakan dalam situs web kami. Kami dapat menautkan informasi cookie ke data pribadi. Cookies juga tertaut pada informasi tentang barang yang telah Anda pilih untuk dibeli dan halaman yang telah Anda lihat. Informasi ini misalnya digunakan untuk memantau keranjang belanja Anda. Cookies juga digunakan untuk mengirimkan konten yang sesuai dengan minat Anda dan memantau penggunaan situs web.<br />
                <br />
                6.2 Anda dapat menolak penggunaan cookies dengan memilih pengaturan yang sesuai pada peramban Anda. Akan tetapi, harap diperhatikan bahwa apabila Anda melakukan hal ini, Anda mungkin tidak dapat menggunakan fungsi penuh Situs atau Layanan kami.<br />
                <br />
                <br />
                <strong>7. MELIHAT DAN MENGUNDUH KONTEN DAN IKLAN</strong><br />
                <br />
                Sama seperti ketika menjelajahi halaman web, saat Anda melihat konten dan iklan serta mengakses perangkat lunak lainnya di Situs kami atau melalui Layanan, sebagian dari informasi yang sama dikirimkan kepada kami (termasuk, dengan tidak terbatas pada, Alamat IP, sistem operasi, dsb.); tetapi bukannya tentang berapa kali halaman dilihat, komputer Anda mengirimkan informasi kepada kami tentang konten, iklan yang dilihat dan/atau perangkat lunak yang dipasang oleh Layanan dan situs web dan waktu.<br />
                <br />
                <br />
                <strong>8. KOMUNITAS &amp; LAYANAN</strong><br />
                <br />
                8.1 Kami menyediakan dukungan layanan pelanggan melalui email, SMS dan formulir umpan balik. Dalam rangka menyediakan dukungan pelanggan, kami akan meminta alamat email Anda dan nomor ponsel Anda. Kami hanya akan menggunakan informasi yang diterima dari permintaan dukungan pelanggan, termasuk, dengan tidak terbatas pada, alamat email, untuk layanan dukungan pelanggan dan kami tidak mengalihkan atau membagikan informasi ini dengan pihak ketiga mana pun.<br />
                <br />
                8.2 Anda juga dapat memposting pertanyaan dan menjawab pertanyaan Pengguna lain di forum komunitas kami. Forum dan layanan pesan kami memungkinkan Anda berpartisipasi dalam komunitas kami; untuk itu, kami menyimpan informasi, seperti ID pengguna, daftar kontak dan pesan status Anda. Selain itu, layanan ini dan layanan serupa di masa mendatang mungkin mengharuskan kami untuk menyimpan ID pengguna dan kata sandi Anda.<br />
                <br />
                <br />
                <strong>9. SURVEI</strong><br />
                <br />
                Dari waktu ke waktu, kami mungkin meminta informasi dari Pengguna melalui survei. Partisipasi dalam survei ini sepenuhnya bersifat sukarela dan karenanya Anda memiliki pilihan untuk mengungkapkan informasi Anda kepada kami atau tidak. Informasi yang diminta dapat mencakup, dengan tidak terbatas pada, informasi kontak (seperti alamat email Anda), dan informasi demografis (seperti minat atau tingkat usia). Informasi survei akan digunakan untuk tujuan memantau atau meningkatkan penggunaan dan kepuasan Layanan serta tidak akan dialihkan ke pihak ketiga, selain kontraktor kami yang membantu kami mengelola atau menindaklanjuti survei.<br />
                <br />
                <br />
                <strong>10. BAGAIMANA KAMI MENGGUNAKAN INFORMASI YANG ANDA BERIKAN KEPADA KAMI?</strong><br />
                <br />
                10.1 Kami dapat mengumpulkan, menggunakan, mengungkapkan dan/atau mengolah data pribadi Anda untuk satu atau lebih dari tujuan-tujuan berikut ini:<br />
                <br />
                (a) mempertimbangkan dan/atau mengolah aplikasi/transaksi Anda dengan kami atau transaksi maupun komunikasi Anda dengan pihak ketiga melalui Layanan;<br />
                <br />
                (b) mengelola, mengoperasikan, menyediakan dan/atau mengurus penggunaan dan/atau akses Anda ke Layanan kami dan situs web kami, serta hubungan Anda dan akun pengguna Anda dengan kami.<br />
                <br />
                (c) mengelola, mengoperasikan, mengurus, menyediakan kepada Anda dan memfasilitasi pengadaan Layanan kami, termasuk, dengan tidak terbatas pada, mengingat preferensi Anda;<br />
                <br />
                (d) menyesuaikan pengalaman Anda melalui Layanan dengan menampilkan konten sesuai dengan minat dan preferensi Anda, menyediakan metode yang lebih cepat untuk Anda mengakses akun Anda dan mengirimkan informasi kepada kami serta mengizinkan kami untuk menghubungi Anda, jika perlu;<br />
                <br />
                (e) menanggapi, mengolah, berurusan dengan atau menyelesaikan transaksi dan/atau memenuhi permintaan Anda untuk produk dan layanan tertentu serta memberi tahu Anda tentang masalah layanan dan tindakan akun yang tidak lazim;<br />
                <br />
                (f) memberlakukan Persyaratan Layanan atau perjanjian lisensi pengguna akhir apapun yang berlaku;<br />
                <br />
                (g) melindungi keselamatan pribadi dan hak, milik atau keselamatan pihak lainnya;<br />
                <br />
                (h) untuk identifikasi dan/atau verifikasi;<br />
                <br />
                (i) mempertahankan dan memberikan setiap pembaruan perangkat lunak dan/atau pembaruan lainnya serta dukungan yang mungkin diperlukan dari waktu ke waktu untuk memastikan kelancaran Layanan kami;<br />
                <br />
                (j) berurusan dengan atau memfasilitasi layanan pelanggan, melaksanakan instruksi Anda, berurusan dengan atau menanggapi setiap pertanyaan yang diajukan oleh (atau konon diajukan oleh) Anda atau atas nama Anda;<br />
                <br />
                (k) menghubungi Anda atau berkomunikasi dengan Anda melalui panggilan telepon, pesan teks dan/atau pesan faks, email dan/atau surat pos atau cara lainnya untuk tujuan mengurus dan/atau mengelola hubungan Anda dengan kami atau penggunaan Layanan kami oleh Anda, seperti tetapi tidak terbatas pada mengomunikasikan informasi administratif kepada Anda yang berkaitan dengan Layanan kami. Anda mengakui dan setuju bahwa komunikasi semacam itu oleh kami dapat dilakukan dengan mengirimkan surat, dokumen atau pemberitahuan kepada Anda, yang dapat melibatkan pengungkapan data pribadi tertentu tentang Anda untuk melakukan pengiriman tersebut serta tercantum juga pada sampul luar amplop/paket pos;<br />
                <br />
                (l) memberi tahu Anda saat Pengguna lain telah mengirimkan pesan pribadi kepada Anda atau memposting komentar untuk Anda di Situs;<br />
                <br />
                (m) mengadakan kegiatan penelitian, analisis dan pengembangan (termasuk, tetapi tidak terbatas pada, analisis data, survei, pengembangan produk dan layanan dan/atau pembuatan profil), untuk menganalisis bagaimana Anda menggunakan Layanan kami, meningkatkan Layanan atau produk kami dan/atau meningkatkan pengalaman pelanggan Anda;<br />
                <br />
                (n) memungkinkan audit dan survei untuk, antara lain, memvalidasi ukuran dan komposisi audiens sasaran kami, serta memahami pengalaman mereka dengan Layanan ' . $arr['name'] . ';<br />
                <br />
                (o) apabila Anda memberikan persetujuan Anda sebelumnya kepada kami untuk pemasaran dan dalam hal ini, untuk mengirimkan kepada Anda surat pos, email, layanan berbasis lokasi atau kalau tidak, informasi dan materi pemasaran dan promosi yang berkaitan dengan produk dan/atau layanan (termasuk, dengan tidak terbatas pada, produk dan/atau layanan pihak ketiga yang mungkin bekerja sama atau terikat dengan ' . $arr['name'] . ') yang mungkin dijual, dipasarkan atau dipromosikan oleh ' . $arr['name'] . ' (dan/atau afiliasi atau perusahaan terkaitnya), baik apakah produk atau layanan tersebut sudah ada sekarang atau diciptakan di masa mendatang. Apabila Anda berada di Singapura, dalam hal pengiriman informasi pemasaran atau promosi kepada Anda melalui panggilan telepon, SMS/MMS atau faks ke nomor faksimile Singapura Anda, kami tidak akan melakukannya kecuali kami telah mematuhi persyaratan Undang-Undang Privasi Singapura untuk menggunakan modus komunikasi tersebut dalam mengirimkan informasi pemasaran kepada Anda atau Anda telah secara tegas menyetujui hal tersebut;<br />
                <br />
                (p) menanggapi proses hukum atau mematuhi atau sebagaimana diwajibkan oleh setiap hukum, persyaratan pemerintah atau peraturan yang berlaku dengan yurisdiksi yang relevan, termasuk, dengan tidak terbatas pada, memenuhi persyaratan untuk melakukan pengungkapan berdasarkan persyaratan hukum yang mengikat ' . $arr['name'] . ' atau perusahaan terkait atau afiliasinya;<br />
                <br />
                (q) menghasilkan statistik dan penelitian untuk pelaporan internal dan yang diwajibkan oleh hukum dan/atau persyaratan penyimpanan arsip;<br />
                <br />
                (r) melaksanakan uji tuntas atau kegiatan penyaringan lainnya (termasuk, dengan tidak terbatas pada, pemeriksaan latar belakang) sesuai dengan kewajiban hukum atau peraturan atau prosedur manajemen risiko kami yang mungkin diwajibkan oleh hukum atau yang telah diberlakukan oleh kami;<br />
                <br />
                (s) mengaudit Layanan kami atau bisnis ' . $arr['name'] . ';<br />
                <br />
                (t) mencegah atau menyelidiki setiap penipuan, kegiatan yang melanggar hukum, pembiaran atau kesalahan, baik yang berhubungan dengan penggunaan Layanan kami oleh Anda atau setiap hal lain apapun yang timbul dari hubungan Anda dengan kami, dan baik apakah ada kecurigaan tentang hal tersebut di atas atau tidak;<br />
                <br />
                (u) menyimpan, menyelenggarakan, membuat cadangan (baik untuk pemulihan setelah bencana atau hal lainnya) data pribadi Anda, baik di dalam atau di luar yurisdiksi Anda;<br />
                <br />
                (v) berurusan dengan/dan atau memfasilitasi transaksi aset bisnis atau kemungkinan transaksi aset bisnis, di mana transaksi tersebut melibatkan ' . $arr['name'] . ' sebagai peserta atau hanya melibatkan perusahaan terkait atau afiliasi ' . $arr['name'] . ' sebagai peserta atau melibatkan ' . $arr['name'] . ' dan/atau satu perusahaan terkait atau afiliasi ' . $arr['name'] . ' atau lebih sebagai peserta, dan mungkin juga ada organisasi pihak ketiga lainnya yang menjadi peserta dalam transaksi tersebut. &ldquo;Transaksi aset bisnis&rdquo; mengacu pada pembelian, penjualan, penyewaan, penggabungan, peleburan atau bentuk akuisisi, pelepasan atau pembiayaan suatu organisasi atau bagian dari suatu organisasi atau bisnis maupun aset apapun dari suatu organisasi; dan/atau<br />
                <br />
                (w) setiap tujuan lain yang akan kami beritahukan kepada Anda pada saat memperoleh persetujuan Anda.<br />
                <br />
                (secara bersama-sama disebut sebagai &ldquo;Tujuan&rdquo;).<br />
                <br />
                10.2 Karena tujuan kami akan/mungkin mengumpulkan, menggunakan, mengungkapkan atau mengolah data pribadi Anda tergantung pada situasi yang dihadapi, tujuan tersebut mungkin tidak tertera di atas. Akan tetapi, kami akan memberi tahu Anda tentang tujuan lain semacam itu pada saat memperoleh persetujuan Anda, kecuali pengolahan data yang berlaku tanpa persetujuan Anda diizinkan oleh Undang-Undang Privasi.<br />
                <br />
                <br />
                <br />
                <strong>11. BERBAGI INFORMASI DARI LAYANAN</strong><br />
                <br />
                Layanan kami memungkinkan Pengguna untuk berbagi informasi pribadi mereka kepada satu sama lain, dalam hampir semua kesempatan tanpa keterlibatan ' . $arr['name'] . ', untuk menyelesaikan transaksi. Dalam transaksi biasa, Pengguna dapat memiliki akses ke nama, ID pengguna, alamat email serta informasi kontak dan pos lainnya dari pengguna lainnya. Persyaratan Layanan kami mewajibkan agar Pengguna yang memiliki data pribadi Pengguna lainnya (&quot;Pihak Penerima&quot;) harus (i) mematuhi semua Undang-Undang Privasi yang berlaku; (ii) mengizinkan Pengguna lain (&quot;Pihak yang Mengungkapkan&quot;) untuk menghapus dirinya dari database Pihak Penerima; dan (iii) mengizinkan Pihak yang Mengungkapkan untuk meninjau informasi apa yang telah dikumpulkan tentang mereka oleh Pihak Penerima.<br />
                <br />
                <br />
                <strong>12. BAGAIMANA ' . $arr['name'] . ' MELINDUNGI INFORMASI PELANGGAN?</strong><br />
                <br />
                Kami menerapkan berbagai langkah pengamanan untuk memastikan keamanan data pribadi Anda di sistem kami. Data pribadi pengguna berada di belakang jaringan yang aman dan hanya dapat diakses oleh sejumlah kecil karyawan yang memiliki hak akses khusus ke sistem tersebut. Kami akan menyimpan data pribadi sesuai dengan Undang-Undang Privasi dan/atau hukum lain yang berlaku. Karenanya, kami akan memusnahkan atau membuat anonim data pribadi Anda segera setelah merupakan hal yang wajar untuk mengasumsikan bahwa (i) tujuan data pribadi tersebut dikumpulkan tidak lagi tercapai dengan penyimpanan data pribadi tersebut; dan (ii) penyimpanan tidak lagi diperlukan untuk tujuan hukum atau bisnis apapun. Apabila Anda berhenti menggunakan Situs, atau izin Anda untuk menggunakan Situs dan/atau Layanan diakhiri, kami dapat terus menyimpan, menggunakan dan/atau mengungkapkan data pribadi Anda sesuai dengan Kebijakan Privasi ini dan kewajiban kami berdasarkan Undang-Undang Privasi. Dengan tunduk pada hukum yang berlaku, kami dapat membuang data pribadi Anda secara aman tanpa pemberitahuan sebelumnya kepada Anda.<br />
                <br />
                <br />
                <strong>13. APAKAH ' . $arr['name'] . ' MENGUNGKAPKAN INFORMASI YANG DIKUMPULKANNYA DARI PENGUNJUNG KEPADA PIHAK LUAR?</strong><br />
                <br />
                13.1 Dalam menjalankan bisnis kami, kami akan/mungkin perlu mengungkapkan data pribadi Anda kepada penyedia layanan pihak ketiga, agen dan/atau afiliasi atau perusahaan terkait kami, dan/atau pihak ketiga lainnya, entah berlokasi di Singapura atau di luar Singapura, untuk satu atau lebih Tujuan yang disebutkan di atas. Penyedia layanan pihak ketiga, agen dan/atau afiliasi atau perusahaan terkait dan/atau pihak ketiga lainnya tersebut akan mengolah data pribadi Anda atas nama kami atau pihak lainnya, untuk satu atau lebih Tujuan yang disebutkan di atas. Pihak ketiga tersebut termasuk, dengan tidak terbatas pada:<br />
                <br />
                (a) anak perusahaan, afiliasi dan perusahaan terkait kami;<br />
                <br />
                (b) kontraktor, agen, penyedia layanan dan pihak ketiga lainnya yang kami gunakan untuk mendukung bisnis kami. Ini termasuk tetapi tidak terbatas pada mereka yang menyediakan layanan administratif atau lainnya kepada kami seperti, jasa pos, perusahaan telekomunikasi, perusahaan teknologi informasi dan pusat data;<br />
                <br />
                (c) pembeli atau penerus lainnya dalam hal terjadi penggabungan, divestasi, restrukturisasi, reorganisasi, pembubaran atau penjualan atau pengalihan lainnya atas beberapa atau semua aset ' . $arr['name'] . ', baik secara berkelanjutan atau sebagai bagian dari kepailitan, likuidasi atau proses serupa, di mana data pribadi yang dimiliki oleh ' . $arr['name'] . ' tentang Pengguna Layanan kami adalah salah satu aset yang dialihkan; atau kepada rekanan dalam suatu transaksi aset bisnis yang melibatkan ' . $arr['name'] . ' atau salah satu afiliasi atau perusahaan terkaitnya; dan<br />
                <br />
                (d) pihak ketiga kepada siapa kami mengungkapkan data tersebut untuk satu Tujuan atau lebih dan pihak ketiga tersebut pada gilirannya akan mengumpulkan dan mengolah data pribadi Anda untuk satu Tujuan atau lebih<br />
                <br />
                13.2 Ini mungkin memerlukan, antara lain, berbagi informasi statistik dan demografis tentang Pengguna kami dan penggunaan Layanan oleh mereka kepada pemasok iklan dan pemrograman. Ini tidak akan termasuk hal apapun yang dapat digunakan untuk mengidentifikasi Anda secara khusus atau menemukan informasi pribadi tentang diri Anda.<br />
                <br />
                13.3 Untuk menghindari keraguan, dalam hal Undang-Undang Privasi atau hukum yang berlaku lainnya mengizinkan suatu organisasi seperti kami untuk mengumpulkan, menggunakan atau mengungkapkan data pribadi Anda tanpa persetujuan Anda, izin yang diberikan oleh hukum tersebut akan terus berlaku.<br />
                <br />
                13.4 Pihak ketiga mungkin menyadap atau mengakses data pribadi yang dikirimkan ke atau tersimpan di situs tanpa izin, teknologi mungkin mengalami kerusakan fungsi atau tidak bekerja seperti yang diharapkan, atau seseorang mungkin mengakses, memanfaatkan atau menyalahgunakan informasi tanpa ada kesalahan dari pihak kami. Meski demikian kami akan menggunakan pengaturan keamanan yang wajar untuk melindungi data pribadi Anda seperti yang disyaratkan oleh Undang-Undang Privasi; akan tetapi tidak dapat dihindari, tidak ada jaminan keamanan mutlak seperti tetapi tidak terbatas pada pengungkapan tanpa izin yang timbul dari peretasan yang berbahaya dan canggih oleh pihak yang tidak senang atau memusuhi tanpa adanya kesalahan dari pihak kami.<br />
                <br />
                <br />
                <strong>14. INFORMASI TENTANG ANAK-ANAK</strong><br />
                <br />
                Layanan tidak ditujukan untuk anak-anak di bawah usia 10 tahun. Kami tidak secara sengaja mengumpulkan atau menyimpan data pribadi atau informasi non-pribadi apapun dari siapa pun di bawah usia 10 tahun dan bagian apapun dari Situs atau Layanan kami lainnya tidak ditujukan untuk anak-anak di bawah usia 10 tahun. Kami akan menutup setiap akun yang digunakan secara eksklusif oleh anak-anak tersebut dan akan mengeluarkan dan/atau menghapus setiap data pribadi yang kami yakini dikirimkan oleh anak di bawah usia 10 tahun.<br />
                <br />
                <br />
                <strong>15. INFORMASI YANG DIKUMPULKAN OLEH PIHAK KETIGA</strong><br />
                <br />
                15.1 Situs kami menggunakan Google Analytics, sebuah layanan analisis web yang disediakan oleh Google, Inc. (&quot;Google&quot;). Google Analytics menggunakan cookies, yaitu file teks yang ditaruh di komputer Anda, untuk membantu situs web menganalisis bagaimana Pengguna menggunakan Situs. Informasi yang hasilkan oleh cookie tentang penggunaan situs web oleh Anda (termasuk alamat IP Anda) akan dikirimkan ke dan disimpan oleh Google di server di Amerika Serikat. Google akan menggunakan informasi ini untuk tujuan mengevaluasi penggunaan situs web oleh Anda, menyusun laporan tentang kegiatan situs web untuk operator situs web serta menyediakan layanan lainnya yang berkaitan dengan kegiatan situs web dan penggunaan Internet. Google juga dapat mengalihkan informasi ini kepada pihak ketiga bila diwajibkan untuk melakukannya oleh hukum, atau bila pihak ketiga tersebut mengolah informasi tersebut atas nama Google. Google tidak akan mengaitkan alamat IP Anda dengan data lain apapun yang dimiliki oleh Google.<br />
                <br />
                15.2 Kami, dan pihak ketiga, dari waktu ke waktu dapat menyediakan unduhan aplikasi perangkat lunak untuk Anda gunakan di atau melalui Layanan. Aplikasi ini dapat mengakses secara terpisah, dan memungkinkan pihak ketiga untuk melihat informasi identifikasi Anda, seperti nama Anda, ID pengguna Anda, Alamat IP komputer Anda atau informasi lainnya seperti cookies yang mungkin sebelumnya telah Anda pasang atau dipasang untuk Anda oleh aplikasi perangkat lunak atau situs web pihak ketiga. Selain itu, aplikasi ini dapat meminta Anda untuk memberikan informasi tambahan secara langsung kepada pihak ketiga. Produk atau layanan pihak ketiga yang disediakan melalui aplikasi ini tidak dimiliki atau dikendalikan oleh ' . $arr['name'] . '. Anda disarankan untuk membaca syarat dan ketentuan yang diterbitkan oleh pihak ketiga tersebut di situs web mereka atau di tempat lainnya.<br />
                <br />
                <strong>16. PENAFIAN TENTANG KEAMANAN DAN SITUS PIHAK KETIGA</strong><br />
                <br />
                16.1 KAMI TIDAK MENJAMIN KEAMANAN DATA PRIBADI DAN/ATAU INFORMASI LAIN YANG ANDA BERIKAN DI SITUS PIHAK KETIGA. Kami menerapkan berbagai langkah pengamanan untuk menjaga keamanan data pribadi Anda yang kami miliki atau berada di bawah kendali kami. Data pribadi Anda berada di belakang jaringan yang aman dan hanya dapat diakses oleh sejumlah kecil orang yang memiliki hak akses khusus ke sistem tersebut, dan diwajibkan untuk menjaga kerahasiaan data pribadi tersebut. Saat Anda melakukan pesanan atau mengakses data pribadi Anda, kami menawarkan penggunaan server yang aman. Semua data pribadi atau informasi sensitif yang anda berikan dienkripsi ke dalam database kami hanya untuk diakses seperti yang disebutkan di atas.<br />
                <br />
                16.2 Dalam upaya untuk memberikan nilai yang lebih baik kepada Anda, kami mungkin memilih berbagai situs web pihak ketiga untuk ditautkan dan dibingkai dalam Situs. Kami juga dapat berpartisipasi dalam pencitraan merek bersama dan hubungan lainnya untuk menawarkan e-commerce serta layanan dan fitur lainnya kepada pengunjung kami. Situs tautan ini memiliki kebijakan privasi serta pengaturan keamanan yang terpisah dan berdiri sendiri. Bahkan jika pihak ketiga tersebut terafiliasi dengan kami, kami tidak memiliki kendali atas situs tautan tersebut, dan masing-masing situs tersebut memiliki praktik privasi dan pengumpulan data yang terpisah dari kami dan berdiri sendiri. Data yang dikumpulkan oleh mitra kami dalam pencitraan merek bersama atau situs web pihak ketiga (bahkan jika ditawarkan di atau melalui Situs kami) mungkin tidak diterima oleh kami.<br />
                <br />
                16.3 Karenanya kami tidak bertanggung jawab atau memiliki kewajiban apapun atas konten, pengaturan keamanan (atau tidak adanya hal tersebut) dan kegiatan situs tertaut ini. Situs tertaut ini hanya untuk kenyamanan Anda dan karenanya Anda mengaksesnya dengan risiko Anda sendiri. Meski demikian, kami berupaya untuk melindungi integritas Situs kami dan masing-masing tautan yang ditempatkan dan karenanya kami menyambut setiap umpan balik tentang situs tertaut tersebut (termasuk, dengan tidak terbatas pada, bila tautan tertentu tidak berfungsi).<br />
                <br />
                <br />
                <strong>17. APAKAH ' . $arr['name'] . ' AKAN MENGALIHKAN INFORMASI ANDA KE LUAR NEGERI?</strong><br />
                <br />
                Data dan/atau informasi pribadi Anda dapat dialihkan, disimpan atau diolah di luar negara Anda. Dalam kebanyakan kasus, data pribadi Anda akan diolah di Singapura, tempat server kami berada dan database pusat kami beroperasi. ' . $arr['name'] . ' hanya akan mengalihkan informasi Anda ke luar negeri sesuai dengan Undang-Undang Privasi.<br />
                <br />
                <strong>18. BAGAIMANA ANDA DAPAT MEMILIH KELUAR, MENGHAPUS, MEMINTA AKSES ATAU MENGUBAH INFORMASI YANG TELAH ANDA BERIKAN KEPADA KAMI?</strong><br />
                <br />
                18.1 Memilih Keluar dan Mencabut Persetujuan<br />
                <br />
                18.1.1 Untuk mengubah langganan email Anda, silakan beri tahu kami dengan mengirimkan email kepada Petugas Perlindungan Data Pribadi kami di alamat yang tercantum di bawah. Harap diperhatikan bahwa karena adanya jadwal produksi email, Anda mungkin masih menerima email yang sudah diproduksi.<br />
                <br />
                18.1.2 Anda dapat mencabut persetujuan Anda untuk pengumpulan, penggunaan dan/atau pengungkapan data pribadi Anda yang kami miliki atau berada di bawah kendali kami dengan mengirimkan email kepada Petugas Perlindungan Data Pribadi kami di alamat email yang tercantum di bawah di Bagian 18.2.<br />
                <br />
                18.1.3 Setelah kami menerima instruksi pencabutan yang jelas dari Anda dan memverifikasi identitas Anda, kami akan memproses permintaan Anda untuk pencabutan persetujuan, dan setelahnya kami tidak akan mengumpulkan, menggunakan dan/atau mengungkapkan data pribadi Anda dengan cara yang tercantum dalam permintaan Anda. Apabila kami tidak dapat memverifikasi identitas Anda atau memahami instruksi Anda, kami akan menghubungi Anda untuk memahami permintaan Anda.<br />
                <br />
                18.1.4 Akan tetapi, pencabutan persetujuan Anda dapat menyebabkan akibat hukum tertentu yang timbul dari pencabutan tersebut. Dalam hal ini, dengan tergantung pada sejauh mana pencabutan persetujuan untuk kami mengolah data pribadi Anda, ini dapat berarti bahwa kami tidak akan dapat terus menyediakan Layanan kepada Anda, kami mungkin perlu mengakhiri hubungan dan/atau kontrak yang Anda miliki dengan kami, dsb., sesuai keadaannya, yang akan kami beritahukan kepada Anda.<br />
                <br />
                18.2 Meminta Akses dan/atau Pembetulan Data Pribadi<br />
                <br />
                18.2.1 Apabila Anda memiliki akun dengan kami, Anda dapat mengakses dan/atau membetulkan sendiri data pribadi Anda yang saat ini kami miliki atau kendalikan melalui halaman Pengaturan Akun di Situs. Apabila Anda tidak memiliki akun dengan kami, Anda dapat meminta untuk mengakses dan/atau membetulkan data pribadi Anda yang saat ini kami miliki atau kendalikan dengan mengirimkan permintaan tertulis kepada kami. Kami akan membutuhkan cukup informasi dari Anda untuk memastikan identitas Anda serta sifat permintaan Anda agar kami dapat menangani permintaan Anda. Karenanya, silakan kirim permintaan tertulis Anda dengan mengirimkan email kepada Petugas Perlindungan Data Pribadi kami di alamat email yang tercantum di bawah di Bagian 18.2.<br />
                <br />
                18.2.2 Untuk permintaan mengakses data pribadi, setelah kami memiliki cukup informasi dari Anda untuk menangani permintaan, kami akan berusaha untuk memberikan data pribadi yang bersangkutan kepada Anda dalam 30 hari (atau, jika Anda adalah penduduk di Malaysia, dalam 21 hari). Apabila kami tidak dapat menanggapi Anda dalam 30 hari (atau, jika Anda adalah penduduk di Malaysia, dalam 21 hari), kami akan memberi tahu Anda kapan paling cepat kami dapat memberikan informasi yang diminta kepada Anda. Harap diperhatikan bahwa permintaan akses untuk jenis data pribadi tertentu mungkin tidak diperbolehkan oleh Undang-Undang Privasi.<br />
                <br />
                18.2.3 Untuk permintaan membetulkan data pribadi, setelah kami memiliki cukup informasi dari Anda untuk menangani permintaan, kami akan:<br />
                <br />
                (a) membetulkan data pribadi Anda dalam waktu 30 hari (atau, jika Anda adalah penduduk di Malaysia, dalam 21 hari). Apabila kami tidak dapat melakukannya dalam jangka waktu tersebut, kami akan memberi tahu Anda kapan paling cepat kami dapat melakukan pembetulan. Harap diperhatikan bahwa permintaan pembetulan untuk jenis data pribadi tertentu mungkin tidak diperbolehkan oleh Undang-Undang Privasi dan ada situasi tertentu di mana pembetulan tidak perlu dilakukan oleh kami meski ada permintaan oleh Anda; dan<br />
                <br />
                (b) kami akan mengirimkan data pribadi yang sudah dibetulkan ke setiap organisasi lain ke mana data pribadi tersebut telah diungkapkan oleh kami dalam satu tahun sebelum tanggal pembetulan dilakukan, kecuali bila organisasi lain tersebut tidak membutuhkan data pribadi yang sudah dibetulkan untuk tujuan hukum atau bisnis apapun.<br />
                <br />
                18.2.4 Menyimpang dari sub ayat (b) di atas, kami hanya dapat, jika Anda memintanya, mengirimkan data pribadi yang sudah dibetulkan kepada organisasi tertentu ke mana data pribadi tersebut telah diungkapkan oleh kami dalam satu tahun sebelum tanggal pembetulan dilakukan.<br />
                <br />
                18.2.5 Kami juga akan/dapat mengenakan biaya yang wajar kepada Anda untuk penanganan dan pemrosesan permintaan Anda untuk mengakses data pribadi Anda. Apabila kami memilih untuk mengenakan biaya, kami akan memberikan perkiraan biaya yang akan kami kenakan kepada Anda secara tertulis. Harap diperhatikan bahwa kami tidak diwajibkan untuk menanggapi atau menangani permintaan akses Anda kecuali Anda telah setuju untuk membayar biaya tersebut.<br />
                <br />
                18.2.6 Kami berhak untuk menolak membetulkan data pribadi Anda sesuai dengan ketentuan yang tercantum dalam Undang-undang Privasi, bila ketentuan tersebut mewajibkan dan/atau memberikan hak kepada suatu organisasi untuk menolak membetulkan data pribadi dalam situasi yang dinyatakan.<br />
                <br />
                <br />
                <strong>19. PERTANYAAN, MASALAH ATAU KELUHAN? HUBUNGI KAMI</strong><br />
                <br />
                19.1 Apabila Anda memiliki pertanyaan atau masalah tentang praktik privasi kami atau hubungan Anda dengan Layanan, silakan jangan ragu-ragu untuk menghubungi: support@' . $arr['domain'] . '.com<br />
                <br />
                19.2 Apabila Anda memiliki keluhan atau pengaduan apapun tentang bagaimana kami menangani data pribadi Anda atau tentang bagaimana kami mematuhi Undang-Undang Privasi, kami mengundang Anda menghubungi kami dengan keluhan atau pengaduan Anda.<br />
                Silakan kirimkan semua pemberitahuan hukum ke legal@' . $arr['name'] . 'mobile.com dan tuliskan Untuk Perhatian &ldquo;Penasihat Umum&rdquo;.<br />
                <br />
                19.3 Apabila Anda mengirimkan keluhan melalui email atau surat, bila Anda mengindikasikan di bagian perihal bahwa itu merupakan keluhan Undang-Undang Privasi, itu akan membantu kami menanggapi keluhan Anda dengan lebih cepat dengan memberikannya kepada staf yang relevan di dalam organisasi kami untuk menanganinya. Misalnya, Anda dapat menuliskan perihal &quot;Keluhan Privasi&quot;.<br />
                <br />
                Kami akan berusaha untuk menangani setiap keluhan atau pengaduan yang mungkin Anda miliki secara adil dan sesegera mungkin.<br />
                <br />
                <br />
                <strong>20. SYARAT DAN KETENTUAN</strong><br />
                <br />
                Silakan juga baca&nbsp;<a href="http://www.' . $arr['domain'] . '.com//read/page/index/term_and_condition" target="_blank">Syarat Layanan</a>&nbsp;yang menetapkan penggunaan, penafian, dan batasan tanggung jawab yang mengatur penggunaan Situs dan Layanan serta kebijakan terkait lainnya.</p>
                ';
        } else if ($key == 'delivery ') {
            $post_context = '<h1>Pengiriman</h1>

                <p><strong>Ketentuan pengiriman paket :</strong></p>

                <p>&nbsp;</p>

                <p><strong>1. WAHANA, JNE, TIKI :</strong><br />
                Konfirmasi transfer sebelum pukul 16.00 wib paket di kirim di hari yang sama<br />
                Konfirmasi transfer diatas pukul 16.00 wib paket di kirim hari berikutnya.<br />
                *Khusus hari sabtu, konfirmasi transfer di atas jam 14.00 wib, dikirim hari selanjutnya.</p>

                <p>&nbsp;</p>

                <p><strong>2. POS :</strong><br />
                Konfirmasi transfer sebelum pukul 13.00 wib paket di kirim di hari yang sama<br />
                Konfirmasi transfer diatas pukul 12.00 wib paket di kirim hari berikutnya.</p>

                <p>&nbsp;</p>

                <p>3. Khusus ke Luar negeri bisa memakai jasa paket&nbsp;<a href="http://ems.posindonesia.co.id/tarifems.html" target="_blank" title="EMS">EMS</a>/dpex/apx atau jasa paket yg biasa Anda gunakan.</p>

                <p>&nbsp;</p>

                <p><strong>4. KARGO</strong></p>

                <p>Untuk pengiriman dalam jumlah besar, bisa memakai jasa kargo dakota/maxindo/mex (atau jasa paket yang biasa Anda gunakan)</p>

                <p>&nbsp;</p>

                <p>NB : Kami akan menginformasikan jasa paket terbaik (Dari sisi Harga Maupun waktu pengiriman).</p>

                <p>Jika Anda memilih jasa pengiriman lainnya dengan syarat ekspedisi tersebut dekat dengan kantor<br />
                kami dan baru dikirim hari berikutnya setelah konfirmasi transfer.</p>
                 ';
        }
        
        $parent_id = carr::get($arr, 'parent_id');
        if (strlen($parent_id) > 0 && $parent_id!='NONE') {
            $db = CDatabase::instance();
            $parent_data = cdbutils::get_row("SELECT * FROM org WHERE org_id = " . $db->escape($parent_id));
            $parent_name = $parent_data->name;
            $parent_domain = $parent_data->domain;

            $self_name = carr::get($arr, 'name');
            $self_domain = carr::get($arr, 'domain');

            $post_context = cdbutils::get_value("SELECT post_content FROM cms_post WHERE org_id = " . $db->escape($parent_id) . " AND post_name LIKE " . $db->escape($key . '%') . " ORDER BY cms_post_id DESC LIMIT 1");
            $post_context = preg_replace('#' . $parent_domain . '#', $self_domain, $post_context);
            $post_context = preg_replace('#' . $parent_name . '#', $self_name, $post_context);
        }

        return $post_context;
    }

    public function delete_cms($org_id = "") {
        $app = CApp::instance();
        $db = CDatabase::instance();

        $data = array(
            'updated' => date('Y-m-d H:i:s'),
            'updatedby' => cobj::get($app->user(), 'username', 'api'),
            'status' => 0,
        );
        $db->update("cms_post", $data, array('org_id' => $org_id));
        $db->update("cms_terms", $data, array('org_id' => $org_id));
        $db->update("cms_term_taxonomy", $data, array('org_id' => $org_id));
        $db->update("cms_term_relationships", $data, array('org_id' => $org_id));
        $db->update("cms_menu", $data, array('org_id' => $org_id));
    }

    public function default_cms($org_id = null) {
        $app = CApp::instance();
        $db = CDatabase::instance();

        $error = 0;
        $error_message = "";
        $org_list = array();


        if ($error == 0) {
            if (strlen($org_id) == 0) {
                $error++;
                $error_message = "Merchant is not available";
            }
        }
        if ($error == 0) {
            $q = "SELECT * FROM org WHERE status>0 AND is_active >0 AND org_id = " . $db->escape($org_id);
            $results = $db->query($q);
            if ($results->count() > 0) {
                foreach ($results as $key => $result) {
                    array_push($org_list, (array) $result);
                }
            } else {
                $error++;
                $error_message = "Merchant is not available";
            }
        }

        if ($error == 0) {
            foreach ($org_list as $keys => $values) {
                $err = 0;
                $err_msg = "";
                if ($err == 0) {
                    $arr_footer = array(
                        'belanja-di' => 'Belanja di ',
                        'beli-produk-di' => 'Beli Produk di ',
                        'beli-jasa-di' => 'Beli Jasa di ',
                    );
                    foreach ($arr_footer as $key => $value) {
                        $org_name_for_url = strtolower($values['name']);
                        $org_name_for_url = str_replace(' ', '-', $org_name_for_url);
                        $post_content = self::format_post_content($key, $values);
                        $data_post = array(
                            "post_content" => $post_content,
                            "org_id" => $values['org_id'],
                            "post_title" => $value . $values ['name'],
                            "post_name" => $key . '-' . $org_name_for_url,
                            "post_type" => "page",
                            "post_status" => 'publish',
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => cobj::get($app->user(), 'username', 'api'),
                        );
                        try {
                            $db->update("cms_post", $data_post, array('org_id' => $values['org_id'],
                                "post_name" => $key . '-' . $org_name_for_url));
                        } catch (Exception $e) {
                            $err++;
                            $err_msg = clang::__("system_delete_fail") . $e->getMessage();
                        }
                    }
                }

                if ($err == 0) {
                    $arr_footer = array(
                        'informasi-pembayaran' => 'Informasi Pembayaran',
                        'hubungi-kami' => 'Hubungi Kami',
                    );
                    foreach ($arr_footer as $key => $value) {
                        $post_content = self::format_post_content($key, $values);
                        $data_post = array(
                            "post_content" => $post_content,
                            "org_id" => $values['org_id'],
                            "post_title" => $value,
                            "post_name" => $key,
                            "post_type" => "page",
                            "post_status" => 'publish',
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => cobj::get($app->user(), 'username', 'api'),
                        );
                        try {
                            $db->update("cms_post", $data_post, array('org_id' => $values['org_id'],
                                "post_name" => $key));
                        } catch (Exception $e) {
                            $err++;
                            $err_msg = clang::__("system_delete_fail") . $e->getMessage();
                        }
                    }
                }

                if ($err == 0) {
                    $arr_footer = array(
                        'tentang' => 'Tentang ',
                        'artikel' => 'Artikel ',
                    );
                    foreach ($arr_footer as $key => $value) {
                        $org_name_for_url = strtolower($values['name']);
                        $org_name_for_url = str_replace(' ', '-', $org_name_for_url);
                        $post_content = self::format_post_content($key, $values);
                        $post_type = "page";
                        if ($key == 'artikel')
                            $post_type = "post";
                        $data_post = array(
                            "post_content" => $post_content,
                            "org_id" => $values['org_id'],
                            "post_title" => $value . $values['name'],
                            "post_name" => $key . '-' . $org_name_for_url,
                            "post_type" => $post_type,
                            "post_status" => 'publish',
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => cobj::get($app->user(), 'username', 'api'),
                        );
                        try {
                            $db->update("cms_post", $data_post, array('org_id' => $values['org_id'],
                                "post_name" => $key . '-' . $org_name_for_url));
                        } catch (Exception $e) {
                            $err++;
                            $err_msg = clang::__("system_delete_fail") . $e->getMessage();
                        }
                    }
                }

                if ($err == 0) {
                    $arr_footer = array(
                        'keuntungan-anggota' => 'Keuntungan Anggota',
                        'syarat-dan-ketentuan' => 'Syarat dan Ketentuan',
                        'kebijakan-privasi' => 'Kebijakan Privasi',
                    );
                    foreach ($arr_footer as $key => $value) {
                        $post_content = self::format_post_content($key, $values);
                        $data_post = array(
                            "post_content" => $post_content,
                            "org_id" => $values['org_id'],
                            "post_title" => $value,
                            "post_name" => $key,
                            "post_type" => "page",
                            "post_status" => 'publish',
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => cobj::get($app->user(), 'username', 'api'),
                        );
                        try {
                            $db->update("cms_post", $data_post, array('org_id' => $values['org_id'],
                                "post_name" => $key));
                        } catch (Exception $e) {
                            $err++;
                            $err_msg = clang::__("system_delete_fail") . $e->getMessage();
                        }
                    }
                }

                if ($err == 0) {
                    if (ccfg::get('compromall_system')) {
                        $arr_footer = array(
                            'about-us' => 'About Us',
                            'privacy-policy' => 'Privacy policy',
                            'term_and_condition' => 'Term and condition',
                            'delivery ' => 'Delivery '
                        );
                        foreach ($arr_footer as $key => $value) {
                            $post_content = self::format_post_content($key, $values);
                            $data_post = array(
                                "post_content" => $post_content,
                                "org_id" => $values['org_id'],
                                "post_title" => $value,
                                "post_name" => $key,
                                "post_type" => "page",
                                "post_status" => 'publish',
                                'updated' => date('Y-m-d H:i:s'),
                                'updatedby' => cobj::get($app->user(), 'username', 'api'),
                            );
                            try {
                                $db->update("cms_post", $data_post, array('org_id' => $values['org_id'],
                                    "post_name" => $key));
                            } catch (Exception $e) {
                                $err++;
                                $err_msg = clang::__("system_delete_fail") . $e->getMessage();
                            }
                        }
                    }

                    if ($err > 0) {
                        $error++;
                        $error_message = $err_msg;
                    }
                }

                if ($err > 0) {
                    $error++;
                    $error_message = $err_msg;
                }
            }
        }

        $return = array(
            'error' => $error,
            'error_message' => $error_message,
        );

        return $return;
    }

}
