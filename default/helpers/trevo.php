<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 7, 2016
     * @license http://ittron.co.id ITtron Indonesia
     */
    class trevo {

        public static function count_price($distance, $vehicle_type) {
            $data = array();
            if ($vehicle_type == 'motor') {
                $price = 2000;
            }
            if ($vehicle_type == 'mobil') {
                $price = 5000;
            }
            $total_price = $price;
            if ($distance > 1) {
                $total_price = $distance * $price;
            }
            return $total_price;
        }

        public static function push_notif($transaction_id) {
            //jika transaction_trevo where status = pending and transaction_id = input
            //select driver nearme, ambil token dari drivernya
            //send gcm notif
            //type = notification, location
            $db = CDatabase::instance();
            $q = "select * 
            from transaction_trevo 
            where status > 0 
            and order_status = 'PENDING' 
            and transaction_id = " . $transaction_id;
            $r = cdbutils::get_row($q);
            if ($r != null) {
                $latitude = cobj::get($r, 'pickup_location_latitude');
                $longitude = cobj::get($r, 'pickup_location_longitude');
                $t_vehicle_type = cobj::get($r, 'vehicle_type');
                $data = trevo::driver_near_me($latitude, $longitude);
                if (count($data) > 0) {
                    $list_driver = $data['list_driver'];
                    foreach ($list_driver as $list_driver_k => $list_driver_v) {
                        $driver_id = $list_driver_v['driver_id'];
                        $vehicle_type = $list_driver_v['vehicle_type'];
                        if ($t_vehicle_type == $vehicle_type) {
                            $q = "select * 
                            from trevo_device 
                            where status > 0
                            and trevo_driver_id = " . $db->escape($driver_id);
                            $r = cdbutils::get_row($q);
                            if ($r != null) {
                                $data = array(
                                    'transaction_id' => $transaction_id,
                                    'trevo_driver_id' => $driver_id,
                                    'token' => cobj::get($r, 'token'),
                                    'push_type' => 'order',
                                    'service_type' => 'driver',
                                );
                                $db->insert('trevo_push_notif', $data);

                                $token = array();
                                $token[] = cobj::get($r, 'token');
                                $message_notif = "You have new order.";
                                $result_json = trevo::send_gcm_notify($token, 'New Order', $message_notif, 'notification', '', 'driver');
                                $result = json_decode($result_json, true);

//                                $token = array();
//                                $token[] = cobj::get($r, 'token');
//                                $message_notif = "";
//                                $result_json = trevo::send_gcm_notify($token, 'New Order', $message_notif, 'notification', '', 'driver');
//                                $result = json_decode($result_json, true);

                                $token = array();
                                $token[] = cobj::get($r, 'token');
                                $message_notif = "You have new order.";
                                $result_json = trevo::send_gcm_notify($token, 'New Order', $message_notif, 'order', '', 'driver');
                                $result = json_decode($result_json, true);
                                return $result;
                            }
                        }
                    }
                }
            }
            return true;
        }

        public static function send_gcm_notify($reg_id, $title = null, $detail, $type, $data = '', $service_type = 'member') {

            if ($title == null) {
                $title = "Trevo";
            }

            $fields = array(
                'registration_ids' => $reg_id,
                'data' => array("message" => $detail, "data" => $data, "title" => $title, "type" => $type),
            );

//            $path_member = "/api/trevomember/GetOrderDetail?session_id=session20161111164556582593541f8e7&transaction_id=357";
//            $path_driver = "/api/trevodriver/GetOrderDetail?session_id=session201611101643295824414175864&transaction_id=351";
//            $request_uri = $_SERVER['REQUEST_URI'];
//            $path = explode("/", $request_uri);
//            if ($path[2] == 'trevomember') {
//                $trevo_driver = 'AAAAFo9ndSE:APA91bGd7io59FAWIVI2T9nrdsBL_JJCir4BfwWhIzcd9f-WtksDSm0lXtoI0ppPrmwPIV4EsAXe0gFcvPSZf1VzExKezRkQwfYuL69GnTJHl79PxHTAnVZSjOEtxYJFeTkwBzsF5annQdDEFRcuyOlW56XPI82xYg';
//                $key = $trevo_member;
//            }
//            if ($path[2] == 'trevodriver') {
//                $trevo_member = 'AAAAZ-IBFKA:APA91bEua70wjLGlasKrbQu1_Gd-jMV_9tqVGSL3HR86CPC-_c9Wiksj8CSennIHLFK9DsKuiDQfKS9EvJN3MsoAokgCDL0AFKL7TXaIpVwdjMuogsYe4F8tj6r8J4r-wRjHcx6U_st9j2pryfs55S2rXOBK8Nd_2w';
//                $key = $trevo_driver;
//            }

            if ($service_type == 'member') {
                $trevo_member = 'AAAAZ-IBFKA:APA91bEua70wjLGlasKrbQu1_Gd-jMV_9tqVGSL3HR86CPC-_c9Wiksj8CSennIHLFK9DsKuiDQfKS9EvJN3MsoAokgCDL0AFKL7TXaIpVwdjMuogsYe4F8tj6r8J4r-wRjHcx6U_st9j2pryfs55S2rXOBK8Nd_2w';
                $key = $trevo_member;
            }
            if ($service_type == 'driver') {
                $trevo_driver = 'AAAAFo9ndSE:APA91bGd7io59FAWIVI2T9nrdsBL_JJCir4BfwWhIzcd9f-WtksDSm0lXtoI0ppPrmwPIV4EsAXe0gFcvPSZf1VzExKezRkQwfYuL69GnTJHl79PxHTAnVZSjOEtxYJFeTkwBzsF5annQdDEFRcuyOlW56XPI82xYg';
                $key = $trevo_driver;
            }
            $headers = array(
                'Authorization: key=' . $key,
                'Content-Type: application/json'
            );

            try {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

                $result = curl_exec($ch);
                if ($result === FALSE) {
                    die('Problem occurred: ' . curl_error($ch));
                }
                curl_close($ch);
            }
            catch (Exception $ex) {
                $this->error()->add_default(2016);
            }

            return $result;
        }

        public static function calculate_distance($pick_lat, $pick_long, $dest_lat, $dest_long) {
            $data = array();
            //                $pickup_location_latitude = -7.339110;
//                $pickup_location_longitude = 112.712948;
//                $destination_location_latitude = -7.311198;
//                $destination_location_longitude = 112.734943;           
            $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=' . $pick_lat . ',' . $pick_long . '&destinations=' . $dest_lat . ',' . $dest_long . '&key=AIzaSyBj66aP56WKQ_E1GIlJxoSHjc6fekGM0f0';
            $curl = CCurl::factory($url);
            $curl->set_ssl();
            $curl->set_timeout(40000);
            $curl->exec();
            $has_error = $curl->has_error();
            $curl_response = $curl->response();

            $json = json_decode($curl_response, true);
//                cdbg::var_dump($json);
            $row = carr::get($json, 'rows');
            $row_0 = carr::get($row, 0);
            $elements = carr::get($row_0, 'elements');
            $elements_0 = carr::get($elements, 0);

            $distance = carr::get($elements_0, 'distance');
            $distance_text = carr::get($distance, 'text');
            $distance_value = carr::get($distance, 'value');
            $distance_num = number_format($distance_value / 1000, 1);
            $data['distance'] = $distance_num;

            $duration = carr::get($elements_0, 'duration');
            $duration_text = carr::get($duration, 'text');
            $duration_value = carr::get($duration, 'value');
            $duration_num = round($duration_value / 60);
            $data['duration'] = $duration_num;

            return $data;
        }

        public static function driver_near_me($latitude, $longitude, $radius_in_km = 3) {
            $data = array();
            $db = CDatabase::instance();
            $q = " SELECT z.trevo_driver_id, z.name, z.vehicle_type
            ,z.latitude, z.longitude,
            p.distance_unit
                     * DEGREES(ACOS(COS(RADIANS(p.latpoint))
                     * COS(RADIANS(z.latitude))
                     * COS(RADIANS(p.longpoint) - RADIANS(z.longitude))
                     + SIN(RADIANS(p.latpoint))
                     * SIN(RADIANS(z.latitude)))) AS distance_in_km
             FROM trevo_driver AS z
             JOIN (
                   SELECT  " . $db->escape($latitude) . "  AS latpoint,  " . $db->escape($longitude) . " AS longpoint,
                           " . $db->escape($radius_in_km) . " AS radius,      111.045 AS distance_unit
               ) AS p ON 1=1
             WHERE z.status_approval = 'APPROVED' 
             AND z.latitude
                BETWEEN p.latpoint  - (p.radius / p.distance_unit)
                    AND p.latpoint  + (p.radius / p.distance_unit)
               AND z.longitude
                BETWEEN p.longpoint - (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
                    AND p.longpoint + (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
             ORDER BY distance_in_km";
            $r = $db->query($q);
            if ($r != null) {
                foreach ($r as $k => $v) {
                    $r_trevo_driver_id = cobj::get($v, 'trevo_driver_id');
                    $r_name = cobj::get($v, 'name');
                    $r_vehicle_type = cobj::get($v, 'vehicle_type');
                    $r_latitude = cobj::get($v, 'latitude');
                    $r_longitude = cobj::get($v, 'longitude');
                    $r_distance_in_km = cobj::get($v, 'distance_in_km');
                    $list_driver = array(
                        'driver_id' => $r_trevo_driver_id,
                        'name' => $r_name,
                        'vehicle_type' => $r_vehicle_type,
                        'latitude' => $r_latitude,
                        'longitude' => $r_longitude,
                        'distance' => $r_distance_in_km,
                    );
                    $data['list_driver'][] = $list_driver;
                }
            }
            else {
                $this->error()->add_default(2002);
            }
            return $data;
        }

        public static function cancel_order($where) {
            $db = CDatabase::instance();

            $transaction_id = carr::get($where, 'transaction_id');
            $transaction_trevo_id = carr::get($where, 'transaction_trevo_id');

            $org_id = ccfg::get('org_id');
            if ($org_id) {
                $org = org::get_org($org_id);
            }
            else {
                $err_message = 'Data Merchant not Valid';
                $this->error->add($err_message, 2999);
            }

            $data = array(
                'order_status' => 'CANCELED',
                'updated' => date('Y-m-d H:i:s'),
                'updatedby' => $org->code,
            );
            $db->update('transaction_trevo', $data, array('transaction_trevo_id' => $transaction_trevo_id));

            $data = array(
                'transaction_status' => 'CANCELED',
                'order_status' => 'CANCELED',
                'payment_status' => 'CANCELED',
                'shipping_status' => 'CANCELED',
                'updated' => date('Y-m-d H:i:s'),
                'updatedby' => $org->code,
            );
            $db->update('transaction', $data, array('transaction_id' => $transaction_id));

            $q = 'select * from transaction t left join trevo_device td on td.member_id = t.member_id where t.transaction_id = ' . $db->escape($transaction_id);
            $r = cdbutils::get_row($q);
            if ($r != null) {
                $data_json = array(
                    'duration' => 0,
                    'latitude' => 0,
                    'longitude' => 0,
                );
                $json = json_encode($data_json);
                $token = array();
                $token[] = cobj::get($r, 'token');
                $message_notif = '';
                $result_json = trevo::send_gcm_notify($token, 'Trevo', $message_notif, 'location', $json, 'member');
                $result = json_decode($result_json, true);

                $token = array();
                $token[] = cobj::get($r, 'token');
                $message_notif = 'Order anda otomatis dibatalkan, karena sudah melebihi batas waktu yang ditentukan';
                $result_json = trevo::send_gcm_notify($token, 'Trevo', $message_notif, 'notification', '', 'member');
                $result = json_decode($result_json, true);
            }
        }

    }
    