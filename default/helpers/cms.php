<?php

/**
 *
 * @author Seians0077
 * @since  Nov 30, 2015
 * @license http://piposystem.com Piposystem
 */
class cms {

    public static function post_status() {
        return array(
            "publish" => "Publish",
            "draft" => "Draft",
        );
    }

    public static function category_list() {
        return self::__get_taxonomy('category');
    }

    public static function get_image_list($default = 'slide', $size = 'original') {
        $result = array();
        $db = CDatabase::instance();
        
        $type_allowed = array(
            'slide',
            'partner',
            'payment',
            'warranty',
            'greeting',
            'affiliate_hotel_partner',
            'hotel_partner',
            );
        $size_allowed = array(
            'large',
            'medium',
            'original',
            'small',
            'thumbnail',
            'wide'
        );
        
        if (in_array($size, $size_allowed) == FALSE) {
            $size = 'original';
        }
        
        if (in_array($default, $type_allowed)) {
            $q = $db->query("SELECT * FROM cms_setting WHERE type = " . $db->escape($default) . " AND status > 0");
            if(CF::org_id() != null) {
                $q = $db->query("SELECT * FROM cms_setting WHERE type = " . $db->escape($default) . " AND status > 0" . " and org_id = ".$db->escape(CF::org_id()));                
            }
            if (count($q) > 0) {
                foreach ($q as $q_k => $q_v) {
                    $arr_result['name'] = $q_v->name;
                    $arr_result['desc'] = $q_v->desc;
                    $arr_result['optional'] = $q_v->optional;
//                    $arr_result['image_url'] = self::convert_to_url($q_v->file_location, $q_v->filename, $size);
                    $arr_result['image_url'] = curl::httpbase(). substr($q_v->url_location, 1, strlen($q_v->url_location) - 1);
//                    $arr_result['file_location'] = $q_v->file_location;
                    $arr_result['filename'] = $q_v->filename;
                    $arr_result['desc'] = $q_v->desc;
                    $result[] = $arr_result;
                }
            }
        }
        
        return $result;
    }
    public static function category() {
        $db = CDatabase::instance();

        $result = array();
        $cat = self::__get_taxonomy('category');
        if (count($cat) > 0) {
            foreach ($cat as $cat_k => $cat_v) {
                $result[$cat_v['cms_terms_id']] = ucfirst($cat_v['name']);
            }
        }
        return $result;
    }

    public static function menu_group_list() {
        return self::__get_taxonomy('nav_menu');
    }

    public static function menu_group() {
        $result = array();
        $cat = self::__get_taxonomy('nav_menu');
        if (count($cat) > 0) {
            foreach ($cat as $cat_k => $cat_v) {
                $result[$cat_v['cms_terms_id']] = ucfirst($cat_v['name']);
            }
        }
        return $result;
    }

    private static function __get_taxonomy($type = 'category') {
        $db = CDatabase::instance();
        $return = array();
        $q = $db->query("select
                            tr.*, tx.taxonomy
                            from cms_terms tr
                            join cms_term_taxonomy tx on tx.cms_terms_id = tr.cms_terms_id
                            where tr.status > 0
                            and tx.taxonomy = " . $db->escape($type) . "
                            and tx.`status` > 0");
        if (count($q) > 0) {
            foreach ($q as $key => $value) {
                $arr_return['cms_terms_id'] = $value->cms_terms_id;
                $arr_return['name'] = $value->name;
                $arr_return['slug'] = $value->slug;
                $arr_return['taxonomy'] = $value->taxonomy;
                $arr_return['created'] = $value->created;
                $arr_return['createdby'] = $value->createdby;
                $return[] = $arr_return;
            }
        }
        return $return;
    }

    public static function post($id = '', $org_id = '') {
        if (strlen($id) > 0 && is_numeric($id)) {
            return self::__get_post_single("post", $id, $org_id);
        }
        $result = self::__get_post('post', $org_id);
        return $result;
    }

    public static function page($id = '', $org_id = '') {
        if (strlen($id) > 0 && is_numeric($id)) {
            return self::__get_post_single("page", $id, $org_id);
        }
        $result = self::__get_post('page', $org_id);
        return $result;
    }

    public static function page_select() {
        $app = CApp::instance();
        $user = $app->user();
        $org_id = $user->org_id;

        $result = array();
        $cat = self::__get_post('page', $org_id);
        if (count($cat) > 0) {
            foreach ($cat as $cat_k => $cat_v) {
                $result[$cat_v['cms_post_id']] = ucfirst($cat_v['post_title']);
            }
        }
        return $result;
    }

    private static function __get_post($post_type = 'post', $category_name, $page = '', $tag='') {
        $db = CDatabase::instance();
        $result = array();
        $org_id = CF::org_id();

        $offset = 0;
        //$limit = self::get_option('posts_per_page');
        $limit = 6;
        $date_format = self::get_option('date_format');
        $time_format = self::get_option('time_format');
        if ($page != NULL && is_numeric($page)) {
            if ($page != 0) {
                $offset = ($page - 1) * $limit;
            }
        }
        
        $q_join_tag = '';
        if(strlen($tag)>0) {
            $q_join_tag = ' inner join cms_post_tag as pt on pt.cms_post_id=p.cms_post_id and pt.tag='.$db->escape($tag);
        }
        
        $q = "SELECT
                tr.cms_term_taxonomy_id, tt.cms_terms_id, t.name term_name, t.slug term_slug,
                p.*
               FROM cms_post p
               LEFT JOIN cms_term_relationships tr ON tr.cms_post_id=p.cms_post_id
               LEFT JOIN cms_term_taxonomy tt ON tt.cms_term_taxonomy_id = tr.cms_term_taxonomy_id
               LEFT JOIN cms_terms t ON t.cms_terms_id=tt.cms_terms_id
               ".$q_join_tag."
               WHERE p.status > 0
               AND p.post_type = " . $db->escape($post_type) . " AND p.post_status ='publish'";
        if (strlen($category_name) > 0) {
            $q .= " AND t.slug=" . $db->escape($category_name);
        }
        if (strlen($org_id) > 0) {
            $q .= " AND p.org_id=" . $db->escape($org_id);
        }
        $q.= " ORDER BY p.created desc";
        if (strlen($limit) > 0 && is_numeric($limit)) {
            $q .= " LIMIT " .$offset.','. $limit;
        }
        
        //echo $q;

        $r = $db->query($q);
        if ($r->count() > 0) {
            foreach ($r as $r_k => $r_v) {
                $arr_result = array();
                $post_thumbnail = curl::httpbase() . 'cresenity/noimage/240/240/';
                if ($r_v->url_location != NULL && strlen($r_v->url_location) > 1) {
                    $post_thumbnail = $r_v->url_location;
//                    $post_thumbnail = str_replace('/original/', '/small/', $post_thumbnail);
                }
                $arr_result['cms_term_taxonomy_id'] = $r_v->cms_term_taxonomy_id;
                $arr_result['cms_terms_id'] = $r_v->cms_terms_id;
                $arr_result['term_name'] = $r_v->term_name;
                $arr_result['term_slug'] = $r_v->term_slug;
                $arr_result['cms_post_id'] = $r_v->cms_post_id;
                $arr_result['post_content'] = $r_v->post_content;
                $arr_result['post_title'] = $r_v->post_title;
                $arr_result['post_thumbnail'] = $post_thumbnail;
                $arr_result['post_excerpt'] = $r_v->post_excerpt;
                $arr_result['post_status'] = $r_v->post_status;
                $arr_result['post_name'] = $r_v->post_name;
                $arr_result['post_parent'] = $r_v->post_parent;
                $arr_result['guid'] = $r_v->guid;
                $arr_result['menu_order'] = $r_v->menu_order;
                $arr_result['post_type'] = $r_v->post_type;
                $arr_result['post_permalink'] = curl::base() . 'read/page/category/' . $r_v->term_slug . '/' . $r_v->cms_post_id;
                $arr_result['post_author'] = $r_v->createdby;
                $arr_result['post_date'] = date('d-m-Y', strtotime($r_v->created));
                $arr_result['post_time'] = date('H:i:s', strtotime($r_v->created));
//                $arr_result['post_mime_type'] = $r_v->post_mime_type;
                $result[] = $arr_result;
            }
        }
        
        return $result;
    }

    private static function __get_post_single($post_type = "post", $id, $org_id = NULL) {
        $db = CDatabase::instance();
        $result = array();
        $date_format = self::get_option('date_format');

        $q = "SELECT
                tr.cms_term_taxonomy_id, tt.cms_terms_id, t.name term_name, t.slug term_slug,
                p.*, o.name as org
               FROM cms_post p
               LEFT JOIN cms_term_relationships tr ON tr.cms_post_id=p.cms_post_id
               LEFT JOIN cms_term_taxonomy tt ON tt.cms_term_taxonomy_id = tr.cms_term_taxonomy_id
               LEFT JOIN cms_terms t ON t.cms_terms_id=tt.cms_terms_id
               LEFT JOIN org o ON o.org_id = p.org_id AND o.status>0
               WHERE p.status > 0
               AND p.post_type = " . $db->escape($post_type) . " AND p.cms_post_id=" . $db->escape($id);
        if (strlen($org_id) > 0) {
            $q .= " AND p.org_id = " . $db->escape($org_id);
        }
        $r = cdbutils::get_row($q);
        if ($r != NULL) {
            $post_thumbnail = curl::httpbase() . 'cresenity/noimage/240/240/';
            if ($r->url_location != NULL && strlen($r->url_location) > 1) {
                $post_thumbnail = $r->url_location;
//                    $post_thumbnail = str_replace('/original/', '/small/', $post_thumbnail);
            }
            $result['org_id'] = $r->org_id;
            $result['org'] = $r->org;
            $result['cms_term_taxonomy_id'] = $r->cms_term_taxonomy_id;
            $result['cms_terms_id'] = $r->cms_terms_id;
            $result['term_name'] = $r->term_name;
            $result['term_slug'] = $r->term_slug;
            $result['cms_post_id'] = $r->cms_post_id;
            $result['post_content'] = $r->post_content;
            $result['post_title'] = $r->post_title;
            $result['post_excerpt'] = $r->post_excerpt;
            $result['post_status'] = $r->post_status;
            $result['post_name'] = $r->post_name;
            $result['post_parent'] = $r->post_parent;
            $result['guid'] = $r->guid;
            $result['menu_order'] = $r->menu_order;
            $result['post_type'] = $r->post_type;
            $result['post_mime_type'] = $r->post_mime_type;
            $result['cannot_delete'] = $r->cannot_delete;
            $result['post_date'] = date('d-m-Y', strtotime($r->created));
            $result['post_time'] = date('H:i:s', strtotime($r->created));
            $result['post_author'] = $r->createdby;
            $result['post_thumbnail'] = $post_thumbnail;
        }

        return $result;
    }

    public static function add_slash_domain($domain) {
        $last_domain = substr($domain, -1, 1);
        if ($last_domain != '/') {
            $domain .= '/';
        }
        return $domain;
    }

    /**
     * @param string/int $menu_name can be menu_name or menu_id
     */
//    public static function nav_menu($menu_name = '') {
//        $db = CDatabase::instance();
//        
//        $return = array();
//        if (strlen($menu_name) > 0) {
//            if (is_numeric($menu_name)) {
//                $get_cms_term_id = cdbutils::get_row("SELECT * FROM cms_terms WHERE cms_terms_id = ".$db->escape($menu_name)." AND STATUS > 0");
//            }
//            else {
//                $get_cms_term_id = cdbutils::get_row("SELECT * FROM cms_terms WHERE slug = ".$db->escape($menu_name)." AND STATUS > 0");
//            }
//            if ($get_cms_term_id != NULL) {
//                $cms_terms_id = $get_cms_term_id->cms_terms_id;
//                
//                $q_menu = "SELECT * FROM cms_term_relationships WHERE cms_term_taxonomy_id = ".$db->escape($cms_terms_id) ." AND status > 0 ORDER BY term_order ASC";
//                $menu_item = $db->query($q_menu);
//                if (count($menu_item) > 0) {
//                    foreach ($menu_item as $menu_item_k => $menu_item_v) {
//                        $cms_post_id = $menu_item_v->cms_post_id;
//                        $arr_return['cms_term_relationships_id'] = $menu_item_v->cms_term_relationships_id;
//                        $arr_return['cms_term_taxonomy_id'] = $menu_item_v->cms_term_taxonomy_id;
//                        $arr_return['cms_post_id'] = $cms_post_id;
//                        $arr_return['term_order'] = $menu_item_v->term_order;
//                        
//                        // GET TITLE MENU from cms_post
//                        $q_post = "SELECT * FROM cms_post WHERE cms_post_id = ".$db->escape($cms_post_id)." AND post_type = 'nav_menu_item'";
//                        $post = cdbutils::get_row($q_post);
//                        $menu_name = '';
//                        if ($post != NULL) {
//                            $menu_name = $post->post_title;
//                        }
//                        
//                        // GET DETAIL MENU FROM postmeta
//                        $q_meta = "SELECT * FROM cms_postmeta WHERE cms_post_id =".$db->escape($cms_post_id);
//                        $postmeta = $db->query($q_meta);
//                        $menu_type = '';
//                        $menu_url = '';
//                        $menu_icon = '';
//                        $taxonomy_id = '';
//                        if (count($postmeta) > 0) {
//                            foreach ($postmeta as $postmeta_k => $postmeta_v) {
////                                cdbg::var_dump($postmeta_v);
//                                $meta_key = $postmeta_v->meta_key;
//                                $meta_value = $postmeta_v->meta_value;
//                                if ($meta_key == '_menu_item_object') {
//                                    $menu_type = $meta_value;
//                                }
//                                if ($meta_key == '_menu_item_url' && strlen($meta_value) > 0) {
//                                    $menu_url = $meta_value;
//                                }
//                                if ($meta_key == '_menu_item_object_id') {
//                                    $taxonomy_id = $meta_value;
//                                }
//                                if ($meta_key == '_menu_item_classes') {
//                                    $menu_icon = $meta_value;
//                                }
//                            }
//                        }
//                        
//                        if ($menu_type == 'category') {
//                            // GET TERMS
//                            if (strlen($taxonomy_id) > 0) {
//                                $get_term = cdbutils::get_row("SELECT * FROM cms_terms WHERE cms_terms_id = ".$db->escape($taxonomy_id));
//                                if ($get_term != NULL) {
//                                    $menu_url = curl::base(). 'read/'.$menu_type. '/'.$get_term->slug;
//                                }
//                            }
//                        }
//                        
//                        if ($menu_type == 'page') {
//                            if (strlen($taxonomy_id) > 0) {
//                                $get_page = cdbutils::get_row("SELECT * FROM cms_post WHERE cms_post_id = ".$db->escape($taxonomy_id));
//                                if ($get_page != NULL) {
//                                    $menu_url = curl::base() .'read/'.$menu_type. '/'. $get_page->post_name;
//                                }
//                            }
//                        }
//                        
//                        $arr_return['menu_type'] = $menu_type;
//                        $arr_return['menu_name'] = $menu_name;
//                        $arr_return['menu_url'] = $menu_url;
//                        $arr_return['menu_icon'] = $menu_icon;
//                        
//                        $return[] = $arr_return;
//                    }
//                }
//            }
//            
//        }
//        else {
//            // DO VIEW ALL MENU
//        }
//        
//        return $return;
//    }

    public static function menu_attr() {
        $return = array(
            "_menu_item_type" => "",
            "_menu_item_menu_item_parent" => "",
            "_menu_item_object_id" => "",
            "_menu_item_object" => "",
            "_menu_item_target" => "",
            "_menu_item_classes" => "",
            "_menu_item_url" => "",
        );
        return $return;
    }

    public static function excerpt_paragraph($html, $max_char = 100, $trail = '...') {
        // temp var to capture the p tag(s)
        $matches = array();
        if (preg_match('/<p>[^>]+<\/p>/', $html, $matches)) {
            // found <p></p>
            $p = strip_tags($matches[0]);
        } else {
            $p = strip_tags($html);
        }
        //shorten without cutting words
        $p = self::__short_str($p, $max_char);

        // remove trailing comma, full stop, colon, semicolon, 'a', 'A', space
        $p = rtrim($p, ',.;: aA');

        // return nothing if just spaces or too short
        if (ctype_space($p) || $p == '' || strlen($p) < 10) {
            return '';
        }

        return '<p>' . $p . $trail . '</p>';
    }

    private function __short_str($str, $len, $cut = false) {
        if (strlen($str) <= $len) {
            return $str;
        }
        $string = ( $cut ? substr($str, 0, $len) : substr($str, 0, strrpos(substr($str, 0, $len), ' ')) );
        return $string;
    }

    // NEW MENU CONCEPT

    /**
     * @param string/int $menu_name can be menu_name or menu_id
     */
    public static function nav_menu($menu_name = '') {
        $db = CDatabase::instance();
        $org_id = CF::org_id();
        $all_menu = array();
		if(ccfg::get('have_same_cms_first_parent')>0){
			$org=org::get_first_parent_org($org_id);
			if($org!=null){
				$org_id=$org->org_id;
			}
		}
        if (strlen($menu_name) > 0) {
            $cms_terms_id = NULL;
            if (is_numeric($menu_name)) {
                $cms_terms_id = $menu_name;
            } else {
                // Get cms_terms_id from cms_terms (slug)
                $get_cms_term_id = cdbutils::get_row("SELECT * FROM cms_terms WHERE slug = " . $db->escape($menu_name) . " and org_id=" . $db->escape($org_id) . " AND STATUS > 0");
                if ($get_cms_term_id != NULL) {
                    $cms_terms_id = $get_cms_term_id->cms_terms_id;
                }
            }

            if ($cms_terms_id != NULL) {
                $q_menu = "SELECT * FROM `cms_menu` WHERE STATUS>0 AND org_id=" . $db->escape($org_id) . " and `cms_terms_id` = " . $db->escape($cms_terms_id) . " ORDER BY lft ASC";
                $menu_res = $db->query($q_menu);
                if (count($menu_res) > 0) {
                    foreach ($menu_res as $menu_res_k => $menu_res_v) {
                        $menu_type = $menu_res_v->menu_item_type;
                        $menu_object_id = $menu_res_v->menu_item_object_id;
                        $menu_object = $menu_res_v->menu_item_object;
                        $menu_target = $menu_res_v->menu_item_target;
//                        cdbg::var_dump($menu_type);
                        $menu_url = $menu_res_v->menu_item_url;
                        $menu_tail = '';
                        if ($menu_object == 'category') {
                            // GET TERMS
                            if (strlen($menu_object_id) > 0) {
                                $get_term = cdbutils::get_row("SELECT * FROM cms_terms WHERE cms_terms_id = " . $db->escape($menu_object_id));
                                if ($get_term != NULL) {
                                    $menu_tail = $get_term->slug;
                                    $menu_object = 'page/' . $menu_object;
                                }
                            }
                        }

                        if ($menu_object == 'page') {
                            if (strlen($menu_object_id) > 0) {
                                $get_page = cdbutils::get_row("SELECT * FROM cms_post WHERE cms_post_id = " . $db->escape($menu_object_id));
                                if ($get_page != NULL) {
                                    $menu_tail = 'index/' . $get_page->post_name;
                                }
                            }
                        }

                        if ($menu_object != 'custom') {
                            $menu_url = curl::base() . 'read/' . $menu_object . '/' . $menu_tail;
                        }

                        $arr_menu['cms_menu_id'] = $menu_res_v->cms_menu_id;
                        $arr_menu['parent_id'] = $menu_res_v->parent_id;
                        $arr_menu['depth'] = $menu_res_v->depth;
                        $arr_menu['menu_name'] = $menu_res_v->name;
                        $arr_menu['menu_type'] = $menu_object;
                        $arr_menu['menu_object_id'] = $menu_object_id;
                        $arr_menu['menu_url'] = $menu_url;
                        $arr_menu['menu_icon'] = $menu_res_v->menu_item_classes;

                        $all_menu[] = $arr_menu;
                    }
                }
            }
        }

        $return = self::__parseTree($all_menu);
        return $return;
    }

    private static function __parseTree($tree, $root = null) {
        $return = array();
        # Traverse the tree and search for direct children of the root
        foreach ($tree as $tree_k => $tree_v) {
            # A direct child is found
            $cms_menu_id = $tree_v['cms_menu_id'];
            $parent = $tree_v['parent_id'];
            $menu_name = $tree_v['menu_name'];
            $menu_type = $tree_v['menu_type'];
            $menu_object_id = $tree_v['menu_object_id'];
            $menu_url = $tree_v['menu_url'];
            $menu_icon = $tree_v['menu_icon'];

            if ($parent == $root) {
                # Remove item from tree (we don't need to traverse this again)
                //unset($tree[$cms_menu_id]);
                # Append the child into result array and parse its children
                $return[] = array(
                    'menu_id' => $cms_menu_id,
                    'menu_name' => $menu_name,
                    'menu_type' => $menu_type,
                    'menu_object_id' => $menu_object_id,
                    'menu_url' => $menu_url,
                    'menu_icon' => $menu_icon,
                    'sub_menu' => self::__parseTree($tree, $cms_menu_id)
                );
            }
        }
        return empty($return) ? null : $return;
    }

    public static function get_option($option_name='') {
        $db = CDatabase::instance();
        $arr_option = array();
        $org_id = CF::org_id();
        
        $q = "
            select *
            from cms_options
            where org_id=" . $db->escape($org_id) . " ";
        if(strlen($option_name)>0) $q .= 'and option_name='.$db->escape($option_name).' ';
        
        $r = $db->query($q);
        if ( $r->count() > 0 ) {
            foreach ( $r as $row ) {
                $arr_option[$row->option_name] = $row->option_value;
            }
        }
        
        return $arr_option;
    }

    public static function get_post($id = '', $category_name = '', $page = '', $tag = '') {
        if (strlen($id) > 0) {
            return self::__get_post_single("post", $id);
        }
        $result = self::__get_post('post', $category_name, $page, $tag);
        return $result;
    }

}
