<?php

    class generate_code {

        
        public static function _get_next_counter($key_counter) {
            $db = CDatabase::instance();
            $next_counter = 1;
            $is_insert = 1;
            $app = CApp::instance();
            $q = "select case when counter is null then 1 else counter+1 end as next_counter from sys_counter where `key`=" . $db->escape($key_counter) . "";

            $r = $db->query($q);

            if ($r->count() > 0) {
                $next_counter = $r[0]->next_counter;
                $is_insert = 0;
            }
            $cmd = "";
            if ($is_insert == 1) {
                $cmd = "insert into sys_counter(`key`,counter,created) values (" . $db->escape($key_counter) . ",1,now());";
            }
            else {
                $cmd = "update sys_counter set counter = counter+1, updated = now() where `key` = " . $db->escape($key_counter) . "";
            }
            $db->query($cmd);

            return $next_counter;
        }

        public static function _get_key_counter($key_format) {

            $result = $key_format;
            preg_match_all("/{([\w]*)}/", $key_format, $matches, PREG_SET_ORDER);

            foreach ($matches as $val) {
                $str = $val[1]; //matches str without bracket {}
                $b_str = $val[0]; //matches str with bracket {}
                switch ($str) {
                    case "yyyy":
                        $result = str_replace("{yyyy}", date('Y'), $result);
                        break;
                    case "yy":
                        $result = str_replace("{yy}", date('y'), $result);
                        break;
                    case "mm":
                        $result = str_replace("{mm}", date('m'), $result);
                        break;
                    case "dd":
                        $result = str_replace("{dd}", date('d'), $result);
                        break;
                    case "MM":
                        $result = str_replace("{MM}", cutils::month_romawi(date('m')), $result);
                        break;
                }
            }
            return $result;
        }

        public static function _get_next_code($key) {
            $key_format = sys_format_auto_code::get_format($key);

            return generate_code::_get_next_code_from_format($key_format);
        }

        public static function _get_next_code_from_format($key_format) {
            $db = CDatabase::instance();
            $key_counter = generate_code::_get_key_counter($key_format);
            $result = $key_counter;
            $counter = generate_code::_get_next_counter($key_counter);
            preg_match_all("/{([\w]*)}/", $key_format, $matches, PREG_SET_ORDER);
            foreach ($matches as $val) {
                $str = $val[1]; //matches str without bracket {}
                $b_str = $val[0]; //matches str with bracket {}
                $len_counter = strlen($str);
                $pad_counter = str_pad($counter, $len_counter, "0", STR_PAD_LEFT);
                $result = str_replace($b_str, $pad_counter, $result);
            }
            return $result;
        }
		public static function generate_booking_code($id){
			$res=bcadd(bcpow(36, 7), bcmod(bcmul($id, 2521109907419), bcsub(bcpow(36, 8), bcpow(36, 7))));
			$res = strtoupper(base_convert($res, 10, 36));
			return $res;
		}
		public static function generate_voucher_code($id){
			$res=bcadd(bcpow(36, 11), bcmod(bcmul($id, 2521109907419), bcsub(bcpow(36, 8), bcpow(36, 11))));
			$res = strtoupper(base_convert($res, 10, 36));
			return $res;
		}
        public static function get_next_transaction_code() {

            $prefix = "TR" . date('ym') . "-{nnnnn}";
            return generate_code::_get_next_code_from_format($prefix);
        }

        public static function get_next_transaction_bid() {
            $prefix = "TRBID" . date('ym') . "-{nnnnn}";
            return generate_code::_get_next_code_from_format($prefix);
        }
		
        public static function get_next_transaction_payment_code() {

            $prefix = "TRP" . date('ym') . "-{nnnnn}";
            return generate_code::_get_next_code_from_format($prefix);
        }

        public static function get_next_topup_code() {
            $prefix = "TP" . date('ym') . "-{nnnnn}";
            return generate_code::_get_next_code_from_format($prefix);
        }
        public static function get_next_lucky_coupon_code($id){
            $res=bcadd(bcpow(36, 9), bcmod(bcmul($id, 2521109907419), bcsub(bcpow(36, 8), bcpow(36, 9))));
            $res = strtoupper(base_convert($res, 10, 36));
            return $res;    
        }
         public static function get_next_payment_code() {
            $rand = rand(10, 99);
            $prefix = $rand . date('ym') . "{nnnnnnn}";
            $payment_code = generate_code::_get_next_code_from_format($prefix);
            $payment_code = base_convert($payment_code, 16, 36);
            $payment_code = strtoupper($payment_code);
            return $payment_code;
        }
    }
    