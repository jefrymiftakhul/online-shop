<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Feb 26, 2016
     */
    class msg_ticket {

        public static function get_department() {
            $db = CDatabase::instance();
            $q = "SELECT * FROM department_msg WHERE is_active = 1 AND status = 1 and org_id = " . CF::org_id();
            $r = $db->query($q);

            $departments = array();
            foreach ($r as $k => $v) {
                $departments[cobj::get($v, 'department_msg_id')] = array(
                    'name' => cobj::get($v, 'name'),
                    'description' => cobj::get($v, 'notes'),
                );
            }
            return $departments;
        }
        
        public static function get_history($message_id){
            $db = CDatabase::instance();
            $history = array();
            $q = "SELECT md.*, d.name as department_name, mm.name as member_name
                FROM message m
                INNER JOIN message_detail md ON m.message_id = md.message_id AND md.status > 0
                INNER JOIN department_msg d ON m.department_msg_id = d.department_msg_id
                INNER JOIN member mm ON m.member_id = mm.member_id
                WHERE m.message_id = " .$db->escape($message_id) ." 
                    AND m.status > 0
                ORDER BY md.message_detail_id ASC
                ";
            $r = $db->query($q);
            foreach ($r as $k => $v) {
                $history[] = array(
                    'message_type' => cobj::get($v, 'message_type'),
                    'member_id' => cobj::get($v, 'member_id'),
                    'member_name' => cobj::get($v, 'member_name'),
                    'department_msg_id' => cobj::get($v, 'department_msg_id'),
                    'department_name' => cobj::get($v, 'department_name'),
                    'user_id' => cobj::get($v, 'user_id'),
                    'subject' => cobj::get($v, 'subject'),
                    'body' => cobj::get($v, 'body'),
                    'notes' => cobj::get($v, 'notes'),
                    'created' => cobj::get($v, 'created'),
                );
            }
            return $history;
        }
        
        
        // <editor-fold defaultstate="collapsed" desc="Send & Reply Message">
        public static function send_to_department($from, $to, $subject, $body,$org_id=null) {
            try {
                self::send_message($from, $to, $subject, $body, 'department',$org_id);
            }
            catch (Exception $exc) {
                throw $exc;
            }
        }

        public static function send_to_member($from, $to, $subject, $body,$org_id=null) {
            try {
                self::send_message($from, $to, $subject, $body, 'member',$org_id);
            }
            catch (Exception $exc) {
                throw $exc;
            }
        }

        public static function send_message($from, $to, $subject, $body, $action_to, $org_id=null) {
            $db = CDatabase::instance();
            $member_id = null;
            $department_id = null;
            $message_type = null;
            $createdby = null;
            $updatedby = null;
            $ip = crequest::remote_address();
            $last_email_from = null;
            
            if($org_id==null) {
                $org_id = CF::org_id();
            }
            
            if ($action_to == 'member') {
                $member_id = $to;
                $department_id = $from;
                $message_type = 'TO_MEMBER';
                $createdby = $department_id;
                $updatedby = $department_id;
                $last_email_from = 'DEPARTMENT';
            }
            else if ($action_to == 'department') {
                $member_id = $from;
                $department_id = $to;
                $message_type = 'TO_DEPARTMENT';
                $createdby = $member_id;
                $updatedby = $member_id;
                $last_email_from = 'MEMBER';
            }

            $data_message = array(
                'org_id' => $org_id,
                'message_type' => $message_type,
                'member_id' => $member_id,
                'department_msg_id' => $department_id,
                'subject' => $subject,
                'body' => htmlspecialchars($body),
                'last_email_from' => $last_email_from,
                'created' => date("Y-m-d H:i:s"),
                'updated' => date("Y-m-d H:i:s"),
                'createdby' => $createdby,
                'updatedby' => $updatedby,
                'createdip' => $ip,
                'updatedip' => $ip,
                'status' => '1',
            );
            $r = $db->insert('message', $data_message);
            $message_id = $r->insert_id();
            try {
                self::reply_message($message_id, $subject, $body, $action_to,$org_id);
            }
            catch (Exception $exc) {
                throw $exc;
            }
        }

        public static function reply_to_department($message_id, $subject, $body,$org_id=null) {
            try {
                self::reply_message($message_id, $subject, $body, 'department',$org_id);
            }
            catch (Exception $exc) {
                throw $exc;
            }
        }

        public static function reply_to_member($message_id, $subject, $body,$org_id=null) {
            try {
                self::reply_message($message_id, $subject, $body, 'member',$org_id);
            }
            catch (Exception $exc) {
                throw $exc;
            }
        }

        public static function reply_message($message_id, $subject, $body, $action_to,$org_id=null) {
            $db = CDatabase::instance();
            $member_id = null;
            $department_id = null;
            $message_type = null;
            $createdby = null;
            $updatedby = null;
            $ip = crequest::remote_address();
            
            $q = "SELECT * 
                FROM message 
                WHERE message_id = " . $db->escape($message_id) . " AND subject = " . $db->escape($subject)
                    . " AND status > 0";
            $r = cdbutils::get_row($q);
            if ($r != null) {
                $member_id = cobj::get($r, 'member_id');
                $department_id = cobj::get($r, 'department_msg_id');
                if ($action_to == 'member') {
                    $message_type = 'TO_MEMBER';
                    $last_email_from = 'DEPARTMENT';
                }
                else if ($action_to == 'department') {
                    $message_type = 'TO_DEPARTMENT';
                    $last_email_from = 'MEMBER';
                }
                if($org_id==null) {
                    $org_id = CF::org_id();
                }
                $data_message = array(
                    'message_type' => $message_type,
                    'message_id' => $message_id,
                    'member_id' => $member_id,
                    'department_msg_id' => $department_id,
                    'subject' => $subject,
                    'body' => htmlspecialchars($body),
                    'created' => date("Y-m-d H:i:s"),
                    'updated' => date("Y-m-d H:i:s"),
                    'createdby' => $createdby,
                    'updatedby' => $updatedby,
                    'createdip' => $ip,
                    'updatedip' => $ip,
                    'status' => '1',
                );
                $db->insert('message_detail', $data_message);
                $db->update('message', array('last_email_from' => $last_email_from), array('message_id' => $message_id));
            }
            else {
                throw new Exception("RM[0]. Message is not found");
            }
        }
        // </editor-fold>
    }
    