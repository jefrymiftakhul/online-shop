<?php

/**
 * Description of namecard
 *
 * @author Ecko Santoso
 * @since 30 Agu 16
 */
class namecard {
    
    public static function greeting() {
        $return = array();
        $file = CF::get_file('data', 'namecard/greeting');
        if ($file != NULL) {
            $return = include $file;
        }
        
        return $return;
    }
}
