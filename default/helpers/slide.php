<?php


class slide {
        public static function get_slide_detail($org_id,$page,$w_menu=0, $type){
		$db=CDatabase::instance();
		$arr_slide=array();
		$org_data_found=false;
                $q_menu = '';
                if($w_menu > 0){
                    $q_menu = '_with_menu';
                }
		$q_base="
			select
				cs.cms_slide".$q_menu."_id as cms_slide".$q_menu."_id,
				csd.cms_slide_image".$q_menu."_id as cms_slide_image".$q_menu."_id
			from
				cms_slide".$q_menu." as cs
				left join cms_slide_detail".$q_menu." as csd on csd.cms_slide".$q_menu."_id=cs.cms_slide".$q_menu."_id
			where
				cs.status>0
				and cs.is_active>0
                                and csd.`status` > 0
				#and cs.type=".$db->escape($type)."
				and cs.product_type=".$db->escape($page)."
		";
                
                if(strlen($type)>0){
                    $q_base .= " and cs.type=".$db->escape($type)." " ;
                }

		//and case when csd.status is null then 1=1 else csd.status>0 end
		$q_org=$q_base;
		if($org_id){
			$q_org.="
				and cs.org_id=".$org_id."
			";
			$q_org.="
				order by
					csd.priority asc
			";
			$r=$db->query($q_org);
			if($r->count()>0){
				$org_data_found=true;
			}
		}
                
		$org_data_found = true;
                
		if(!$org_data_found){
			$q_no_org=$q_base."
				and cs.org_id is null
			";
			$q_no_org.="
				order by
					csd.priority asc
			";
			$r=$db->query($q_no_org);
		}
		if($r->count()>0){
                        $id_image = 'cms_slide_image'.$q_menu.'_id';
			foreach($r as $row){
				$arr_image=array();
				$q_row="
					select 
                                                name,
						image_url,
                                                url_link
					from
						cms_slide_image".$q_menu." 
					where
						cms_slide_image".$q_menu."_id=".$db->escape($row->$id_image)."
                                                 and org_id = ".$org_id." 
				";
				$r_row=$db->query($q_row);
				if($r_row->count()>0){
					$arr_image['name']=$r_row[0]->name;
					$arr_image['image_url']=$r_row[0]->image_url;
					$arr_image['url_link']=$r_row[0]->url_link;
					$arr_slide[]=$arr_image;
				}
			}
		}
		
		return $arr_slide;
        }

        
        
        public static function get_slide($org_id,$page,$w_menu=0, $type = '') {
            $db=CDatabase::instance();
            $arr_res=array();
            $theme = cdbutils::get_value('select theme from org where org_id = '.$db->escape($org_id));
            $with_menu=0;
            if($w_menu > 0 && $theme=='62hallfloor'){
                $with_menu=1;
            }
            $arr_res=self::get_slide_detail($org_id,$page,$with_menu, $type);
            
            if(count($arr_res)==0){
                if($with_menu==1){
                    $with_menu=0;
                    $arr_res=self::get_slide_detail($org_id,$page,$with_menu);
                }
            }
            
            return $arr_res;
	}
}
