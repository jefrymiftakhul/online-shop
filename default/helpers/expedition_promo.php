<?php



class expedition_promo {
    
    public function calculate_promo($districts_id) {
        $db = CDatabase::instance();
        $cart = Cart62hall_Product::factory();
        $cart_list = $cart->get_cart();
        $org_id = CF::org_id();
        $q = "select * from setting_expedition_promo where status>0 and org_id=".$db->escape($org_id);
        $r = $db->query($q);
        
        foreach($r as $row) {
            $total = 0;
            foreach ($cart_list as $product_id => $data_cart) {
                $subtotal = carr::get($data_cart,'sub_total');
                $total+=$subtotal;
            }
            //check total apakah memenuhi syarat kalau tidak continue
            if($total<$row->min_total) {
                continue;
            }
            //check apakah tujuan memenuhi persyaratan kota
            $row_sepc=cdbutils::get_row("select * from setting_expedition_promo_city where status=1 and setting_expedition_promo_id=".$row->setting_expedition_promo_id);
            if($row_sepc){
                $row_location = cdbutils::get_row("select p.province_id,c.city_id,d.districts_id from province as p inner join city as c on c.province_id=p.province_id inner join districts as d on d.city_id=c.city_id where d.status>0 and p.status>0 and c.status>0 and d.districts_id = ".$db->escape($districts_id));
               
                $q_check_location = cdbutils::get_row("select "
                        . "* "
                        . "from "
                        . "setting_expedition_promo_city"
                        . " where status=1 "
                        . "and setting_expedition_promo_id=".$db->escape($row->setting_expedition_promo_id)." "
                        . "and province_id=".$db->escape(cobj::get($row_location, 'province_id'))." "
                        . "and (city_id=".$db->escape(cobj::get($row_location, 'city_id'))." or (city_id is null))");
                if($q_check_location==null) {
                    continue;
                }
            }
            //check apakah kategori memenuhi persyaratan dari barang yang ada
            $match=1;
            $row_sepca=cdbutils::get_row("select * from setting_expedition_promo_category where setting_expedition_promo_id=".$row->setting_expedition_promo_id);
            if($row_sepca){
                foreach ($cart_list as $product_id => $data_cart) {
                    $product_category_row = cdbutils::get_row("select pc.* from product as p inner join product_category as pc on pc.product_category_id = p.product_category_id where pc.status>0 and p.product_id=".$db->escape($product_id));

                    $q_check_category = "select * from setting_expedition_promo_category as epc inner join product_category as pc on epc.product_category_id=pc.product_category_id where pc.status>0 and setting_expedition_promo_id=".$row->setting_expedition_promo_id." and pc.lft<=".$db->escape($product_category_row->lft)." and pc.rgt>=".$db->escape($product_category_row->rgt);
                    $check_category_row = cdbutils::get_row($q_check_category);
                    if($check_category_row==null) {
                        $match=0;
                        break;   
                    }
                }
            }
            //jika ada salah satu barang tidak memenuhi
            if($match==0) {
                continue;
            }
            
            //jika sampai sini maka semua syarat terpenuhi, hitung discount yang ada
            $discount = $row->discount_nominal;
            $is_free=0;
            if($row->is_free) {
                $is_free=1;
            } else if($row->is_min_total_multiple) {
                $total_multiple = floor($total/$row->min_total);
                $discount*=$total_multiple;
            }
            return array(
                'setting_expedition_promo_id'=>$row->setting_expedition_promo_id,
                'name'=>$row->name,
                'discount'=>$discount,
                'is_free'=>$is_free,
            );
            
            
        }
        return null;
    }
}
