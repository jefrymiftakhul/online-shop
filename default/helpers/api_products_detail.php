<?php

    class Api_Products_Detail {
        //put your code here
        public function convert_products_api($val) {
            $product_data_type='0';
            if($val['product_data_type']=='configurable'){
                $product_data_type='1';
            }
            $arr=array();
            $arr['product_id']=$val['product_id'];
            $arr['url_key']=$val['url_key'];
            $arr['product_code']=$val['code'];
            $arr['product_name']=$val['name'];
            $arr['product_sku']=$val['sku'];
            $arr['weight']=$val['weight'];
            $arr['image_url']=$val['image_url'];//image::get_image_url($val['image_path'],'view_all');
            $detail_flag = array();
            $detail_flag["hot"] = $val['is_hot_item'];
            $new = '0';
            if (strtotime($val['new_product_date']) >= strtotime(date('Y-m-d'))) {
                $new = '1';
            }
            $detail_flag["new"] = $new;
            $arr['flag'] = $detail_flag;
            $arr['is_available']=$val['is_available'];
            $arr['quick_overview']=$val['quick_overview'];
            $arr['stock']=$val['stock'];
            $arr['tax']= $val['tax'];
            $arr['show_minimum_stock']=$val['show_minimum_stock'];
            $arr['overview']=$val['overview'];
            $arr['spesification']=$val['spesification'];
            $arr['faq']=$val['faq'];
            $arr['is_configurable']=$product_data_type;
            $detail_price = array();
            $detail_price["sell_price_start"] = $val["detail_price"]['sell_price_start'];
            $detail_price["sell_price_end"] = $val["detail_price"]['sell_price_end'];
            $detail_price["sell_price"] = $val["detail_price"]['ho_sell_price'];
            $detail_price["sell_price_promo"] = $val["detail_price"]['channel_sell_price'];
            $detail_price["promo_text"] = $val["detail_price"]['promo_text'];
            $arr['detail_price'] = $detail_price;
           
            return $arr;
            
        }
    }
    