<?php

class pulsa_api {
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    public static function api($method = null, $product_code=null, $request=array()) {
//        cdbg::var_dump($method);
//        cdbg::var_dump($product_code);
//        cdbg::var_dump($request);
//        die();
        if (strtolower(carr::get($request, 'product_category_code')) == 'pulsa' ||strtolower(carr::get($request, 'product_category_code')) == 'game_online') {
            $product_pulsa_ready = array('RB', 'KY');
            if (in_array($product_code, $product_pulsa_ready)) {
                $api_method = 'api_pulsa';
            }
            else {
                die('Product code "' . $product_code . '" not registered.');
            }
        } else {
            if ($product_code != 'OB') {
                die('Product code "' . $product_code . '" not registered.');
            }
            $api_method = 'api_ppob';
        }
//        cdbg::var_dump($api_method);die;
        //$this->api_pulsa($method, $product_code, $request);
        return self::api_pulsa($method, $product_code, $request);
    }

    public static function api_pulsa($method = null, $product_code, $request=array()) {
//        die();
        if(count($request) == 0){
            $request = array_merge($_GET, $_POST);
        }
        if ($method == 'get_price') {
            $session = Session::instance();
            $session_key = 'ittron_api_' . 'PU' . "_" . $product_code;
            $session->delete($session_key);
        }

        $session = CApiClientSession::instance('PU', strtoupper($product_code));
        $session->set('last_method', $method);

        $session->set('request_' . $method, $request);

//        if ($method == "commit") {
//            try {
//                pulsa_utils::process_db_presubmit($product_code);
//            } catch (Exception $ex) {
//                $this->show_error($ex->getMessage());
//            }
//        }
        if ($method == "commit"){
           $arr_product_code = array("product_code"=>$product_code);
            $request = array_merge($request, $arr_product_code);

        }
//        cdbg::var_dump($method);
//            cdbg::var_dump($request);die;
        $api_data = CApiClient::instance('PU', strtoupper($product_code))->exec($method, $request);
        $session->set('api_data_' . $method, $api_data);
        $session->set('have_' . $method, '1');

//        if ($method == "commit") {
//            try {
//                pulsa_utils::process_db_postsubmit($product_code);
//            } catch (Exception $ex) {
//                $this->show_error($ex->getMessage());
//            }
//        } elseif ($method == "get_transaction") {
//            pulsa_utils::process_db_updatestatus($product_code);
//        }

        if (isset($api_data['err_code']) && $api_data['err_code'] > 0) {
            $app = CApp::instance();
            //cmsg::add('error', '[' . $method . '] ' . $api_data['err_message'] . " (" . $api_data['err_code'] . ')');
            //echo $app->render();
        } else {
            $session->set('have_success_' . $method, '1');
        }
        return $api_data;
    }
    public function issued_pulsa($transaction_id=null){
        $data_transaction = cdbutils::get_row("select * from transaction_detail where transaction_id=".$transaction_id);
        $product_id = cobj::get($data_transaction, "product_id");
        $phone_number = cobj::get($data_transaction, "pulsa_phone_number");
        
        $data_product = cdbutils::get_row("select * from product where product_id=".$product_id);
        $item_code = cobj::get($data_product, "sku");
        $provider_id = cobj::get($data_product, "provider_id");
        
        $provider = cdbutils::get_row("select * from provider where provider_id=".$provider_id);
        $provider_var_name = cobj::get($provider,'var_name');
        $provider_type = cobj::get($provider,'provider_type');
   
        $product_code = "RB";
        if (ccfg::get('pulsa_product_code') != NULL) {
            $product_code = ccfg::get('pulsa_product_code');
        }
        if ($product_code == 'KY') {
            $item_code = cobj::get($data_product, 'sku_ky');
        }
        
        $get_price_request["operator_name"]=$provider_var_name;
        $get_price_request["product_category_code"]=$provider_type;
        
        self::api_pulsa("get_price", $product_code, $get_price_request);
        
        $commit_request["item_code"]=$item_code;
        $commit_request["msisdn"]=$phone_number;
        $respon = self::api_pulsa("commit", $product_code, $commit_request);
//        $respon =   array( 
//                    "err_code"    => "0",
//                    "err_message" => "",
//                    "data"        => array(
//                                        "item_code"          => $item_code,
//                                        "msisdn"             => $phone_number,
//                                        "description"        => "Pengisian pulsa $item_code Anda ke nomor $phone_number sedang diproses",
//                                        "transaction_code"   => "TRPU1609-00011",
//                                        "commission"         => 0,
//                                        "total"              => 98600,
//                                        "status_transaction" => "ISSUED",
//                                        "status_pulsa"       => "PENDING",
//                                        "reference_number"   => "582374541",
//                        ), 
//                    "next_method" => NULL,
//            );
        return $respon;    
        
    }
 }