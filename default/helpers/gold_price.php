<?php


class gold_price {
    
	public static function get_list_gold_price() {
		$db=CDatabase::instance();
		$arr_gold=array();
		$org_data_found=false;
		$product_type_id='';
		$q="
			select
				*
			from
				product_type
			where
				name='gold'
		";
		$r=$db->query($q);
		if($r->count()>0){
			$q_base="
				select
					distinct(weight) as weight
					,max(sell_price) as sell_price
					,min(purchase_price) as purchase_price
					,sum(stock) as stock
				from
					product 
				where
					status>0
					and product_type_id=".$db->escape($r[0]->product_type_id)."
				group by 
					weight
			";
			$r=$db->query($q_base);
			if($r->count()>0){
				foreach($r as $row){
					$sell_price='Sold Out';
					if($row->stock>0){
						$sell_price=$row->sell_price;
					}
					$arr_gold[]=array(
						"name"=>$row->weight.' gr',
						"buy"=>$row->purchase_price,
						"sell"=>$sell_price,
					);
				}
			}
		}
		
		return $arr_gold;
	}
}
