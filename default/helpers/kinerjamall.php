<?php

class kinerjamall {
    
    private function merchantData($development = FALSE){
        
        $return = array(
            'url' => 'https://www.kinerjapay.com/services/kinerjapay/json/',
            'merchantAppCode' => '4a74dd7d8a0255e89d7f6fa18aa57f07cf2134ec8f582528b0dc69ecee666275',
            'merchantAppPassword' => 'K1nerjaM4ll'
        );
        
        if ($development == TRUE) {
            $return = array(
                'url' => 'https://www.kinerjapay.com/sandbox/services/kinerjapay/json/',
                'merchantAppCode' => '3886c3fcd719b272e165d665fd90c274b2005ab24cffb4bf2050b314a97ba651',
                'merchantAppPassword' => 'K1nerjaM4llD3v'
            );
        }
        
        $return['merchantIpAddress'] = '43.252.138.136';
        
        return $return;
    }

    public static function user_inquiry($user_session, $development = FALSE) {
        $result = array();
        $post_data=array();
        
        $merchant_data = kinerjamall::merchantData($development);
        
//        $url = 'https://www.kinerjapay.com/services/kinerjapay/json/user-detail.php';
//        $post_data['merchantAppCode'] = '4a74dd7d8a0255e89d7f6fa18aa57f07cf2134ec8f582528b0dc69ecee666275';
//        $post_data['merchantAppPassword'] = 'K1nerjaM4ll';
//        
//        if($development == TRUE) {
//            $url = 'https://www.kinerjapay.com/sandbox/services/kinerjapay/json/user-detail.php';
//            $post_data['merchantAppCode'] = '3886c3fcd719b272e165d665fd90c274b2005ab24cffb4bf2050b314a97ba651';
//            $post_data['merchantAppPassword'] = 'K1nerjaM4llD3v';
//        }
//            
//        $post_data['merchantIpAddress'] = '43.252.138.136';
//        $post_data['merchantIpAddress'] = crequest::remote_address();
        $post_data = $merchant_data;
        $url = $merchant_data['url']. 'user-detail.php';
        $post_data['userRef'] = $user_session;
        unset($post_data['url']);
        
        $curl = CCurl::factory(NULL);
        $curl->set_opt(CURLOPT_SSL_VERIFYPEER, FALSE);
        $curl->set_opt(CURLOPT_SSL_VERIFYHOST, 2);        
        $curl->set_url($url);
        $json = cjson::encode($post_data);
        $curl->set_raw_post($json);
        $curl->exec();
        $response = $curl->response();
        clog::write($response);
        
        $response=json_decode($response,true);
        
        // dummy
//        $response = array(
//            'method' => 'user_get',
//            'code' => '100',
//            'success' => '1',
//            'message' => array(
//                'message' => 'Success'
//            ),
//            'result' => array(
//                'user' => array(
//                    'ref' => $user_session,
//                    'name' => 'Dummy Name',
//                    'email' => 'erik@email.com',
//                    'verified' => '1',
//                    'balances' => array()
//                )
//            ),
//        );
        
        $code = carr::get($response, 'code');
        $success = carr::get($response, 'success');
        $message = carr::get($response, 'message');
        $result_api = carr::get($response, 'result');
        
        if ($success == '1') {
            $user = carr::get($result_api, 'user');
            $ref = carr::get($user, 'ref');
            $name = carr::get($user, 'name');
            $email = carr::get($user, 'email');
            $verified = carr::get($user, 'verified');
            $balances = carr::get($user, 'balances');
            
            if ($verified == '1') {
                $result['name'] = $name;
                $result['email'] = $email;
                
                self::do_login($result);
            }
        }
        
        return $result;
    }
    
    public static function do_login($data) {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $org_id = CF::org_id();
        $session = Session::instance();
        $email = carr::get($data, 'email');
        
        $err_code = 0;
        $err_message = "";

        try {
            if ($err_code == 0) {

                $q = '
                      SELECT m.*, mc.name category_name FROM member m 
                        LEFT JOIN member_category mc
                            ON m.member_category_id = mc.member_category_id
                        WHERE m.status > 0 
                        AND m.is_active=1 
                        AND m.email = ' . $db->escape($email) . '
                        AND m.org_id = ' . $db->escape($org_id) . '
                    ';
                $row = cdbutils::get_row($q);

                if ($row != null) {
                    $session->set('member_id', cobj::get($row, 'member_id'));
                    $session->set('email', cobj::get($row, 'email'));
                    $session->set('name', cobj::get($row, 'name'));
                    $session->set('member_category', cobj::get($row, 'category_name'));
                } 
//                else {
//                    $q = '
//                            SELECT * FROM member WHERE status > 0
//                            AND email = ' . $db->escape($email) . '
//                            AND org_id = ' . $db->escape($org_id) . '
//                          ';
//                    $row = cdbutils::get_row($q);
//
//                    if ($row != NULL) {
//                        $link_resend_confirm = curl::base() . 'member/confirm/recend_confirm?email=' . $username;
//                        if($method=='jsonp') {
//                            $link_resend_confirm = curl::base() . 'auth/resend_confirm/' . cobj::get($row, 'member_id');
//                        }
//                        $err_code++;
//                        $err_message = 'Email anda belum di verifikasi, <a href="' . $link_resend_confirm . '"><u>Kirim ulang verifikasi email</u></a>';
//                    } else {
//                        $err_code++;
//                        $err_message = 'Email atau Password salah';
//                    }
//                }
            }
        } catch (Exception $ex) {
            $err_code++;
            $err_message = clang::__('Error, Please try again.');
            clog::write('DB Error page auth/login. ' . $ex->getMessage());
        }
        
        return true;
    }
    
    public static function user_register($data_member, $development = FALSE) {
        $data = array();
        $err_code = 0;
        $err_message = "";
        
        $merchant_data = kinerjamall::merchantData($development);
        $post_data = $merchant_data;
        $url = $merchant_data['url']. 'user-register.php';
        $post_data['userEmail'] = carr::get($data_member, 'email');
        $post_data['userName'] = carr::get($data_member, 'name');
        unset($post_data['url']);
        
        $curl = CCurl::factory(NULL);
        $curl->set_opt(CURLOPT_SSL_VERIFYPEER, FALSE);
        $curl->set_opt(CURLOPT_SSL_VERIFYHOST, 2);        
        $curl->set_url($url);
        $json = cjson::encode($post_data);
        $curl->set_raw_post($json);
        $curl->exec();
        $response = $curl->response();
        
        $response=json_decode($response,true);
        
        $code = carr::get($response, 'code');
        $success = carr::get($response, 'success');
        $message = carr::get($response, 'message');
        $result_api = carr::get($response, 'result');
        
        if ($success == '1') {
            $id = carr::get($result_api, 'id');
            $name = carr::get($result_api, 'name');
            $email = carr::get($result_api, 'email');

            $data['name'] = $name;
            $data['email'] = $email;
        }
        else {
            $err_code++;
            $err_message = carr::get($message, 'message');
        }
        
        $result = array(
            'err_code' => $err_code,
            'err_message' => $err_message,
            'data' => $data,
            'api_data' => json_encode($response),
        );
        
        return $result;
        
    }
    
    public static function login() {
        $err_code = 0;
        $err_message = "";
        $web_session = Session::instance();
        $product_code = 'KP';
        $api_session_id = $web_session->get('api_session_id');
        
        if ($api_session_id == FALSE) {
            $api = CApiServerSixtyTwoHallFamily::instance('session', array('Login'));
            $api->set_post_data(array('auth_id' => ccfg::get('api_auth_client')));
            $api->set_has_return(true);
            $api->exec();
            $api_data = $api->get_response();
            if (isset($_GET['debug'])) {
                cdbg::var_dump(ccfg::get('api_auth_client'));
                echo 'LOGIN-API<br>';
                cdbg::var_dump($api_data);
            }
            
            $api_err_code = carr::get($api_data, 'err_code');
            $api_err_message = carr::get($api_data, 'err_message');
            $data = carr::get($api_data, 'data');
            
            if ($api_err_code == 0) {
                $api_session_id = carr::get($data, 'session_id');
                $web_session->set('api_session_id', $api_session_id);
            }
        }
        
        if (isset($_GET['debug'])) {
            echo 'HP-<br>';
            cdbg::var_dump($api_session_id);
            echo 'HPE-<br>';
        }
        
        
    }
    
    
    public static function kp_api_init() {
        $err_code = 0;
        $err_message = "";
        $web_session = Session::instance();
        $product_code = 'KP';
        $api_session_id = $web_session->get('api_session_id');
        
        if ($api_session_id === FALSE) {
            $api = CApiServerSixtyTwoHallFamily::instance('session', array('Login'));
            $api->set_post_data(array('auth_id' => ccfg::get('api_auth_client')));
            $api->set_has_return(true);
            $api->exec();
            $api_data = $api->get_response();
            
            $api_err_code = carr::get($api_data, 'err_code');
            $api_err_message = carr::get($api_data, 'err_message');
            $data = carr::get($api_data, 'data');
            
            if ($api_err_code == 0) {
                $api_session_id = carr::get($data, 'session_id');
                $web_session->set('api_session_id', $api_session_id);
            }
        }
        
        try {
            $session = CApiServer_Session::instance('session', $api_session_id);
        }
        catch (Exception $e) {
            $err_code++;
            $err_message = $e->getMessage();
        }
        
        if ($err_code == 0) {            
            $args = array(
                    'method' => 'GetProductCategory',
                    'mode' => 'transaction',
                );
            
            kinerjamall::get_product_category_kp($session, $args, $product_code);
            $default_init = array(
                array(
                    'method' => 'GetProducts',
                    'mode' => 'transaction',
                    'product_type' => 'voucher',
                ),
                array(
                    'method' => 'GetProducts',
                    'mode' => 'transaction',
                    'product_type' => 'product',
                ),
            );
            
            foreach ($default_init as $key => $value) {
                kinerjamall::get_products($session, $value, $product_code);
            }
        }
        
    }
    
    public static function get_product_category_kp($session, $args = array(), $product_code = '') {
        
        $web_session = Session::instance();
        $mode = carr::get($args, 'mode');
        $method = carr::get($args, 'method');
        $post_data = array(
            'session_id' => $session->get('session_id'),
            'product_code' => $product_code
        );
        $return = FALSE;
        $product_category = $session->get('product_category_kp');
        if ($product_category == NULL) {
            $api = CApiServerSixtyTwoHallFamily::instance($mode, array($method));
            $api->set_post_data($post_data);
            $api->set_has_return(TRUE);
            $api->exec();
            $api_data = $api->get_response();
            
            $web_session->set('product_category_kp', $session->get('product_category_kp'));
        }
        else {
            $web_session->set('product_category_kp', $product_category);
            $return = true;
        }
        
        return $return;
    }
    
    public static function get_products($session, $args = array(), $product_code = '') {
        $web_session = Session::instance();
        $mode = carr::get($args, 'mode');
        $method = carr::get($args, 'method');
        $product_type = carr::get($args, 'product_type');
        
        $post_data = array(
            'session_id' => $session->get('session_id'),
            'product_code' => $product_code,
            'product_type' => $product_type
        );
        if($product_type=='voucher') {
            $post_data['category_id']=79;
        }
            
        $return = FALSE;
        $products = $session->get('kinerja_product_'.$product_type);
        if ($products == NULL) {
           
            $api = CApiServerSixtyTwoHallFamily::instance($mode, array($method));
            $api->set_post_data($post_data);
            $api->set_has_return(TRUE);
            $api->exec();
            $api_data = $api->get_response();
           
            $web_session->set('kinerja_product_'.$product_type, $session->get('kinerja_product_'.$product_type));
        }
        else {
            $web_session->set('kinerja_product_'.$product_type, $products);
            $return = true;
        }
        
        return $return;
    }
    
    public static function get_product_detail($session, $args = array(), $product_code = '') {
//        cdbg::var_dump($session);
//        die(__METHOD__);
       
        $web_session = Session::instance();
        $mode = carr::get($args, 'mode');
        $method = carr::get($args, 'method');
        $product_id = carr::get($args, 'product_id');
        
        $post_data = array(
            'session_id' => $session->get('session_id'),
            'product_code' => $product_code,
            'product_id' => $product_id,
        );
        
        $return = FALSE;
        
      
           
        $api = CApiServerSixtyTwoHallFamily::instance($mode, array($method));
        $api->set_post_data($post_data);
        $api->set_has_return(TRUE);
        $api->exec();
        $api_data = $api->get_response();

        $web_session->set('last_kinerja_product_detail', $session->get('last_kinerja_product_detail'));

        
        return $api_data;
    }
}