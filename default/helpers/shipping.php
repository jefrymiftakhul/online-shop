<?php

    class shipping {

        public static function get_shipping_type() {
            $db = CDatabase::instance();
            $org_id = CF::org_id();
            $query = "SELECT
                            *
                        FROM
                            shipping_type
                        WHERE status > 0 
                        AND org_id = ".$db->escape($org_id);
            $result = $db->query($query);
            
            return $result;
        }
        
        public static function get_shipping_price($shipping_type_id, $districts_id){
            $db = CDatabase::instance();
            
            $query = "SELECT
                        sp.*
                        ,st.code as shipping_type_code 
                        ,st.name as shipping_type_name 
                        ,c.name as city_name
                        ,d.name as district_name
                    FROM shipping_price sp 
                    left join shipping_type st on sp.shipping_type_id = st.shipping_type_id
                    left join city c on sp.city_id = c.city_id 
                    left join districts d on sp.districts_id = d.districts_id 
                    WHERE
                        sp.status > 0
                        AND sp.shipping_type_id = " . $db->escape($shipping_type_id) . " 
                        and st.org_id=".$db->escape(CF::org_id())." 
                        AND sp.districts_id = " . $db->escape($districts_id);
            $shipping_price = cdbutils::get_row($query);
            
            return $shipping_price;
        }
		
		public static function get_shipping_city_info($product_id){
            $db = CDatabase::instance();
			$arr_product_city=array();
			$q="
				select
					p.province_id as province_id,
					p.name as province,
					c.city_id as city_id,
					c.name as city
				from
					product_city as pc
					left join province as p on p.province_id=pc.province_id
					left join city as c on c.city_id=pc.city_id
				where
					pc.status>0
					and pc.product_id=".$db->escape($product_id)."
			";
			$r=$db->query($q);
			if($r->count()>0){
				foreach($r as $row){
					$arr_city=array();
					if(!isset($arr_product_city[$row->province])){
						$arr_product_city[$row->province]=array();
						$arr_product_city[$row->province]['city']=array();
					}
					if($row->province){
						if($row->city){
							$arr_city['city_id']=$row->city_id;
							$arr_city['city']=$row->city;
							$arr_product_city[$row->province]['city'][$row->city]=$arr_city;
						}else{
							$arr_product_city[$row->province]['city']['all']=true;
						}
					}
				}
				return $arr_product_city;
			}
		}
		
		public static function check_product_available_shipping_city($arr_product_id,$city_id){
			if(!is_array($arr_product_id)){
				$arr_product_id=array(
					'0'=>$arr_product_id,
				);
			}
			$db=CDatabase::instance();
			$res=array();
			$error=0;
			$error_message='';
			$product_error=array();
			$q="
				select
					p.name as province,
					c.name as city
				from
					city as c
					inner join province as p on p.province_id=c.province_id
				where
					c.status>0
					and c.city_id=".$db->escape($city_id)."
			";
			$row=cdbutils::get_row($q);
			if($row){
				foreach($arr_product_id as $product_id){
					$arr_product_city=self::get_shipping_city_info($product_id);
					if(count($arr_product_city)>0){
						$arr_province=carr::get($arr_product_city,$row->province);
						if($arr_province){
							$arr_city=carr::get($arr_province,'city');
							if(carr::get($arr_city,'all')==null){
								if(carr::get($arr_city,$row->city)==null){
									$product_name=cdbutils::get_value('select name from product where product_id='.$db->escape($product_id));
									$error=2;
									$product_error[]=$product_name;
								}
							}
						}else{
							$product_name=cdbutils::get_value('select name from product where product_id='.$db->escape($product_id));
							$error=1;
							$product_error[]=$product_name;
						}
					}
				}
			}
			if($error>0){	
				$product_error=implode(',',$product_error);
				$error_message='Maaf Pengiriman Product['.$product_error.'] Ke Wilayah Anda Tidak Tersedia';
			}
			$res['err_code']=$error;
			$res['err_message']=$error_message;
			
			return $res;
		}

    }
    