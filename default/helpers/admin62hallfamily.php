<?php

    /*
     * Description of admin62hallfamily
     * @author Joko Jainul A
     * @since Nov 17, 2015 4:36:39 PM
     */

    class admin62hallfamily {

        public static function city_select($options) {
            $province_id = carr::get($options, 'province_id');
            $city_id = carr::get($options, 'city_id');
            $province_required = carr::get($options, 'province_required');

            $all = carr::get($options, 'all');
            $ctrlid = carr::get($options, 'ctrlid');
            $db = CDatabase::instance();
            $app = CApp::instance();
            $list = array();
            $table_name = 'city';

            if (!($province_required && (strlen($province_id) == 0 || $province_id == "ALL"))) {

                $q = "select city_id,name from `" . $table_name . "` where status>0 ";
                if (strlen($province_id) > 0 && $province_id != "ALL") {
                    $q.=" and province_id=" . $db->escape($province_id);
                }

                $q.=" order by name asc";

                $list = cdbutils::get_list($q);
            }
            if ($all) {
                $list = array("ALL" => clang::__("ALL")) + $list;
            }
            $inp = CFormInputSelect::factory($ctrlid);
            $inp->set_value($city_id)->set_list($list);
            return $inp->html();
        }
        
        public static function country_select($options) {
            $country_id = carr::get($options, 'country_id');
            $all = carr::get($options, 'all');
            $ctrlid = carr::get($options, 'ctrlid');

            $db = CDatabase::instance();
            $app = CApp::instance();
            $list = array();

            $table_name = 'country';

            $q = "select country_id,name from `" . $table_name . "` where status>0 ";

            $q.=" order by name asc";

            $list = cdbutils::get_list($q);
            if ($all) {
                $list = array("ALL" => clang::__("ALL")) + $list;
            }

            $inp = CFormInputSelect::factory($ctrlid);

            $inp->set_value($country_id)->set_list($list);
            return $inp->html();
        }
        
        public static function province_select($options) {
            $country_id = carr::get($options, 'country_id');
            $province_id = carr::get($options, 'province_id');
            $country_required = carr::get($options, 'country_required');

            $all = carr::get($options, 'all');
            $ctrlid = carr::get($options, 'ctrlid');
            $db = CDatabase::instance();
            $app = CApp::instance();
            $list = array();
            $table_name = 'province';

            if (!($country_required && (strlen($country_id) == 0 || $country_id == "ALL"))) {

                $q = "select province_id,name from `" . $table_name . "` where status>0 ";
                if (strlen($country_id) > 0 && $country_id != "ALL") {
                    $q.=" and country_id=" . $db->escape($country_id);
                }

                $q.=" order by name asc";
                $list = cdbutils::get_list($q);
                if ($list == NULL) {
                    $list = array
                        ('NULL' => "");
                }
            }
            if ($all) {
                $list = array("ALL" => clang::__("ALL")) + $list;
            }
            $inp = CFormInputSelect::factory($ctrlid);
            $inp->set_value($province_id)->set_list($list);
            return $inp->html();
        }

    }
    