<?php

    class transaction {
        public static function get_transaction_id_from_booking($booking_code, $email = null, $org_id = null) {
            $db = CDatabase::instance();
            if ($org_id == null) {
                $org = org::get_info();
                $org_id = carr::get($org, 'org_id');
            }
            $q = "
                select 
                    transaction_id 
                from 
                    transaction 
                where 
                    booking_code = " . $db->escape($booking_code) . " AND status > 0 
                    AND org_id = " . $db->escape($org_id) . "
            ";
            if ($email != null) {
                $q .= "AND email=" . $db->escape($email);
            }
            $q .= " ORDER BY date DESC LIMIT 1 ";
            $transaction_id = cdbutils::get_value($q);

            return $transaction_id;
        }
        public static function get_transaction_contact($key = NULL, $value = NULL){
            $db=CDatabase::instance();
			$query = "
                    SELECT
                        *
                    FROM
                        transaction t
                    LEFT JOIN 
                        transaction_contact tc on tc.transaction_id = t.transaction_id
                    WHERE
                        t.status > 0
                    ";
            
            if($key){
                $query .= "AND " . $key . "=" . $db->escape($value);
            }
            
            $result = array();
            $result = cdbutils::get_row($query);
            
            return $result;
        }
		
		public static function get_prelove_transaction($transaction_id){
			$db = CDatabase::instance();
			$q = 'SELECT
					t.vendor_sell_price,t.product_id, p.name
				FROM transaction t
				LEFT JOIN product p ON t.product_id = p.product_id
				WHERE t.status > 0
				AND t.transaction_id = 
			'.$db->escape($transaction_id);
			$row = cdbutils::get_row($q);
			return $row;
		}
        
        public static function get_transaction_detail($key = NULL, $value = NULL){
            $db = CDatabase::instance();
            
            $query = "
                    SELECT 
                        *, tr.channel_sell_price as transaction_price
                    FROM 
                        transaction_detail tr
                    LEFT JOIN 
                        product as p on p.product_id = tr.product_id
                    WHERE
                        tr.status > 0
                    ";
            
            if($key){
                $query .= "AND " . $key . "=" . $db->escape($value);
            }
            
            $data_product = array();
            $r = $db->query($query);
            if($r->count()>0){
                foreach($r as $row){
                    $data_product[] = $row;
                }
            }
            return $data_product;
        }
        
        
        public static function get_transaction_detail_transaction($key = NULL, $value = NULL){
            $db = CDatabase::instance();
            $query = "
                    SELECT 
                        tr.*, tr.qty, tr.channel_sell_price as transaction_price,
                        t.booking_code, t.date, t.date_success,
                        p.*
                    FROM 
                        transaction_detail tr
                    LEFT JOIN 
                        transaction as t on t.transaction_id = tr.transaction_id
                    INNER JOIN 
                        product as p on p.product_id = tr.product_id
                    WHERE
                        tr.status > 0
                    ";
            
            if($key){
                $query .= "AND " . $key . "=" . $db->escape($value);
            }
            
            $data_product = array();
            $r = $db->query($query);
            if($r->count()>0){
                foreach($r as $row){
                    $data_product[] = $row;
                }
            }
            return $data_product;
        }

        public static function get_id_prev_bid($product_id) {
            $db = CDatabase::instance();
            $q = "SELECT transaction_bid_id
                FROM transaction_bid t
                WHERE t.product_id = " .$db->escape($product_id) ." AND 
                    t.status > 0
                ORDER BY t.transaction_date DESC
                LIMIT 1
                ";
            $r = cdbutils::get_value($q);
            return $r;
        }
        
        public static function get_bid($transaction_bid_id) {
            $db = CDatabase::instance();
            $q = "SELECT tb.member_id, tb.transaction_date, tb.code, tb.vendor_sell_price, tb.vendor_sell_price_before,
                    tb.status_transaction, tb.transaction_bid_ref_id,tb.org_id as bid_org,
                    m.name as member_name, m.email as member_email, m.date_join, m.image_name as member_image,
                    p.*
                    -- ,pi.image_path, pi.image_name, pi.filename, p.name
                FROM transaction_bid tb
                INNER JOIN product p ON p.product_id = tb.product_id
                INNER JOIN member m ON tb.member_id = m.member_id
                WHERE tb.transaction_bid_id = " .$db->escape($transaction_bid_id) ." AND 
                    tb.status > 0
                ";
            $r = cdbutils::get_row($q);
            return $r;
        }

        public static function get($transaction_id) {
            $db = CDatabase::instance();
            $q = "SELECT 
					m.name AS member_name, m.email AS member_email, m.date_join,
					m.image_name AS member_image,
					p.*, 
					s.name as seller_name,
					s.email as seller_email,
					p.member_id as product_member_id,
					t.member_id as transaction_member_id, 
					t.code, t.vendor_sell_price,
					t.transaction_id,
					t.transaction_status,
					t.org_id
					FROM transaction t
					INNER JOIN product p ON p.product_id = t.product_id
					INNER JOIN member m ON t.member_id = m.member_id
					INNER JOIN member s ON p.member_id = s.member_id
					WHERE t.transaction_id = ".$db->escape($transaction_id)." AND 
					 t.status > 0
                ";
            $r = cdbutils::get_row($q);
            return $r;
        }
        
        public static function get_transaction_payment($key = NULL, $value = NULL){
            $db = CDatabase::instance();
            $query = "
                    select 
                        *
                    from 
                        transaction_payment
                    where 
                    status > 0
                    ";
            
            if($key){
                $query .= "AND " . $key . "=" . $db->escape($value);
            }
            
            $result = array();
            $result = cdbutils::get_row($query);
            
            return $result;
        }
        
        public static function get_transaction($key = NULL, $value = NULL){
            $db = CDatabase::instance();
            $query = "
                    SELECT
                        *
                    FROM
                        transaction
                    WHERE
                        status > 0
                    ";
            
            if($key){
                $query .= "AND " . $key .  " = " . $db->escape($value);
            }
            
            $data_transaction = array();
            $r = $db->query($query);
            if($r->count()>0){
                foreach($r as $row){
                    $data_transaction[] = $row;
                }
            }
            return $data_transaction;
        }
        
        public function get_transaction_shipping($key = NULL, $value = NULL){
            $db = CDatabase::instance();
            $query = "
                    SELECT
                        *
                    FROM
                        transaction_shipping
                    WHERE
                        status > 0
                    ";
            
            if($key){
                $query .= "AND " . $key .  " = " . $db->escape($value);
            }
            
            $data_transaction = array();
            $r = $db->query($query);
            if($r->count()>0){
                foreach($r as $row){
                    $data_transaction[] = $row;
                }
            }
            return $data_transaction;
        }
        public static function get_transaction_all($transaction_id) {
            $db = CDatabase::instance();
            $res = array();
            $transaction = array();
            $contact = array();
            $contact_billing = array();
            $contact_shipping = array();
            $contact_detail = array();

            $q = "
                select
                    t.channel_sell_price as total_channel_sell_price,
                    t.*,
                    tc.*,
                    td.*,
                    tp.*
                from
                    transaction as t
                    inner join transaction_contact as tc on tc.transaction_id=t.transaction_id
                    left join transaction_payment as tp on (tp.transaction_id=t.transaction_id and tp.status>0)
                    left join transaction_detail as td on (td.transaction_id=t.transaction_id and td.status>0)
                where
                    t.transaction_id=" . $db->escape($transaction_id) . "
            ";
            $r = $db->query($q);
            if ($r->count() > 0) {
                $first_row = $r[0];
                $transaction['page'] = $first_row->product_type;
                $transaction['type'] = $first_row->type;
                $contact_billing['first_name'] = $first_row->billing_first_name;
                $contact_billing['last_name'] = $first_row->billing_last_name;
                $contact_billing['address'] = $first_row->billing_address;
                $contact_billing['email'] = $first_row->billing_email;
                $contact_billing['province_id'] = $first_row->billing_province_id;
                $contact_billing['province'] = $first_row->billing_province;
                $contact_billing['city_id'] = $first_row->billing_city_id;
                $contact_billing['city'] = $first_row->billing_city;
                $contact_billing['district_id'] = $first_row->billing_districts_id;
                $contact_billing['district'] = $first_row->billing_district;
                $contact_billing['postal_code'] = $first_row->billing_postal_code;
                $contact_billing['phone'] = $first_row->billing_phone;
                $contact_billing['country_code'] = 'IDN';


                $contact_shipping['first_name'] = $first_row->shipping_first_name;
                $contact_shipping['last_name'] = $first_row->shipping_last_name;
                $contact_shipping['address'] = $first_row->shipping_address;
                $contact_shipping['email'] = $first_row->shipping_email;
                $contact_shipping['province_id'] = $first_row->shipping_province_id;
                $contact_shipping['province'] = $first_row->shipping_province;
                $contact_shipping['city_id'] = $first_row->shipping_city_id;
                $contact_shipping['city'] = $first_row->shipping_city;
                $contact_shipping['district_id'] = $first_row->shipping_districts_id;
                $contact_shipping['district'] = $first_row->shipping_district;
                $contact_shipping['postal_code'] = $first_row->shipping_postal_code;
                $contact_shipping['phone'] = $first_row->shipping_phone;
                $contact_shipping['country_code'] = 'IDN';

                foreach ($r as $row) {
                    $item = array();
                    $product = product::get_product($row->product_id);
                    $product_name = $product['name'];
                    $product_code = $product['code'];
                    $product_price = $row->channel_sell_price;
                    $qty = $row->qty;
                    $item['item_id'] = $row->product_id;
                    $item['item_code'] = $product_code;
                    $item['item_name'] = $product_name;
                    $item['qty'] = $qty;
                    $item['price'] = $product_price;
                    $transaction['item'][] = $item;
                    if ($first_row->product_type == 'gold') {
                        if ($first_row->type == 'sell') {
                            $product_id = NULL;
                            $product_code = 'DSG';
                            $product_name = 'Deal Sell Gold';
                            $product_price = 250000;
                            $qty = 1;
                        }
                    }
                    $item['item_id'] = $row->product_id;
                    $item['item_code'] = $product_code;
                    $item['item_name'] = $product_name;
                    $item['qty'] = $qty;
                    $item['price'] = $product_price;
                    $transaction['item_payment'][] = $item;
                }
                $shipping_type_id = $first_row->shipping_type_id;
                $total_shipping = $first_row->total_shipping;
                if ($total_shipping > 0) {
                    $item['item_id'] = NULL;
                    $item['item_code'] = 'SHP';
                    $item['item_name'] = 'Shipping to ' . $first_row->shipping_city;
                    $item['qty'] = 1;
                    $item['price'] = $total_shipping;
                    $transaction['item_payment'][] = $item;
                }
                $total_item = ($first_row->total_channel_sell_price);
                $grand_total = $total_item + $total_shipping;
                $amount_payment = $grand_total;
                if ($first_row->product_type == 'gold') {
                    if ($first_row->type == 'sell') {
                        $amount_payment = 250000;
                    }
                }
                $transaction['total_item'] = $total_item;
                $transaction['shipping_type_id'] = $shipping_type_id;
                $transaction['total_shipping'] = $total_shipping;
                $transaction['amount_payment'] = $amount_payment;



                $contact['first_name'] = $contact_billing['first_name'];
                $contact['last_name'] = $contact_billing['last_name'];
                $contact['email'] = $contact_billing['email'];
                $contact['phone'] = $contact_billing['phone'];
                $contact['billing_address'] = $contact_billing;
                $contact['shipping_address'] = $contact_shipping;
                $transaction['contact_detail'] = $contact;

                $transaction_payment_id = cobj::get($first_row, 'transaction_payment_id');
                $arr_charge = array();
                $total_charge = 0;
                $total_amount_payment = $amount_payment;
                // get payment charge
                if ($transaction_payment_id) {
                    $q = "
                        select
                                *
                        from
                                transaction_payment_charge
                        where
                                status>0
                                and transaction_payment_id=" . $db->escape($transaction_payment_id) . "
                    ";
                    $r = $db->query($q);
                    if ($r->count() > 0) {
                        foreach ($r as $row_charge) {
                            $charge = array();
                            $charge['code'] = $row_charge->code;
                            $charge['name'] = $row_charge->name;
                            $charge['amount'] = $row_charge->amount;
                            $arr_charge[$row_charge->code] = $charge;
                            $total_charge+=$row_charge->amount;
                        }
                        $total_amount_payment = $amount_payment + $total_charge;
                    }
                }
                $res['transaction'] = $transaction;
                $res['product_code'] = cobj::get($first_row, 'payment_vendor');
                $res['transaction_id'] = $transaction_id;
                $res['transaction_code'] = $first_row->code;
                $res['booking_code'] = $first_row->booking_code;
                $res['transaction_email'] = $first_row->email;
                $res['currency_code'] = 'IDR';
                $res['payment_type'] = cobj::get($first_row, 'payment_type');
                $res['payment_code'] = cobj::get($first_row, 'payment_code');
                $res['transaction_payment_id'] = cobj::get($first_row, 'transaction_payment_id');
                $res['detail_charge'] = $arr_charge;
                $res['total_amount_charge'] = $total_charge;
                $res['total_amount_payment'] = $amount_payment;
                $res['bank'] = cobj::get($first_row, 'bank');
                $res['account_number'] = cobj::get($first_row, 'account_number');
                $res['request'] = $first_row->is_request;
            }
            return $res;
        }
        
        public static function winner_lucky_day($product_id){
            $db=CDatabase::instance();
            $err_code=0;
            $transaction_id="";
            try{
                
                $q="
                    select
                        *
                    from
                        lucky_coupon_quota
                    where
                        product_id=".$product_id."
                    order by
                        rand()
                    limit 1
                ";        
                $r=$db->query($q);
                if($r->count()>0){
                    $row=$r[0];
                    $transaction_id=$row->transaction_id;
//                    $q_transaction="
//                        select
//                            *
//                        from
//                            transaction
//                        where
//                            transaction_id=".$db->escape($row->transaction_id)."
//                    ";
//                    $transaction=cdbutils::get_row($q_transaction);
//                    $transaction_code_generate = generate_code::get_next_transaction_code();
//                    $data_transaction=array(
//                        "org_id"=>$transaction->org_id,
//                        "org_parent_id"=>$transaction->org_parent_id,
//                        "member_id"=>$transaction->member_id,
//                        "session_id"=>null,
//                        "api_session_id"=>null,
//                        "code"=>$transaction_code_generate,
//                        "request_code"=>null,
//                        "email"=>$transaction->email,
//                        "date"=>date('Y-m-d H:i:s'),
//                        "date_approval"=>date('Y-m-d H:i:s'),
//                        "date_hold"=>date('Y-m-d H:i:s'),
//                        "date_success"=>date('Y-m-d H:i:s'),
//                        "date_shipping"=>null,
//                        "date_arrived"=>null,
//                        "approvalby"=>'SYSTEM',
//                        "type"=>'buy',
//                        "product_type"=>'luckydeal',
//                        "transaction_status"=>'SUCCESS',
//                        "order_status"=>'SUCCESS',
//                        "payment_status"=>'SUCCESS',
//                        "shipping_status"=>'PENDING',
//                        "vendor_nta"=>0,
//                        "vendor_commission_value"=>0,
//                        "vendor_sell_price"=>0,
//                        "total_promo"=>0,
//                        "ho_upselling"=>0,
//                        "ho_sell_price"=>0,
//                        "channel_commission_full"=>0,
//                        "channel_commission_ho"=>0,
//                        "channel_commission_share"=>0,
//                        "channel_commission_share_ho"=>0,
//                        "channel_commission_value"=>0,
//                        "channel_updown"=>0,
//                        "channel_sell_price"=>0,
//                        "channel_profit"=>0,
//                        "total_shipping"=>0,
//                        "shipping_type_id"=>NULL,
//                        "total_deal"=>0,
//                        "request_description"=>'',
//                        "approval_description"=>'',
//                        "is_request"=>0,
//                        "is_paid"=>1,
//                        "is_posted"=>0,
//                        "transaction_from"=>$transaction->transaction_from,
//                        "created"=>date('Y-m-d H:i:s'),
//                        "createdby"=>'SYSTEM',
//                        "updated"=>date('Y-m-d H:i:s'),
//                        "updatedby"=>'SYSTEM',
//                    );
//                    $r_transaction=$db->insert('transaction',$data_transaction);
//                    $transaction_id=$r_transaction->insert_id();
//                    $booking_code = generate_code::generate_booking_code($transaction_id);
//                    $db->update('transaction',array("booking_code"=>$booking_code),array("transaction_id"=>$transaction_id));
//                    //insert transaction contact
//                    $q_transaction_contact="
//                        select
//                            *
//                        from
//                            transaction_contact
//                        where
//                            transaction_id=".$db->escape($transaction->transaction_id)."
//                    ";
//                    $transaction_contact=cdbutils::get_row($q_transaction_contact);
//                    $data_transaction_contact = array(
//                        'transaction_id' => $transaction_id,
//                        'billing_first_name' => $transaction_contact->billing_first_name,
//                        'billing_last_name' => $transaction_contact->billing_last_name,
//                        'billing_email' => $transaction_contact->billing_email,
//                        'billing_phone' => $transaction_contact->billing_phone,
//                        'billing_address' => $transaction_contact->billing_address,
//                        'billing_province_id' => $transaction_contact->billing_province_id,
//                        'billing_province' => $transaction_contact->billing_province,
//                        'billing_city_id' => $transaction_contact->billing_city_id,
//                        'billing_city' => $transaction_contact->billing_city,
//                        'billing_districts_id' => $transaction_contact->billing_districts_id,
//                        'billing_district' => $transaction_contact->billing_district,
//                        'billing_postal_code' => $transaction_contact->billing_postal_code,
//                        'shipping_first_name' => $transaction_contact->shipping_first_name,
//                        'shipping_last_name' => $transaction_contact->shipping_last_name,
//                        'shipping_email' => $transaction_contact->shipping_email,
//                        'shipping_phone' => $transaction_contact->shipping_phone,
//                        'shipping_address' => $transaction_contact->shipping_address,
//                        'shipping_province_id' => $transaction_contact->shipping_province_id,
//                        'shipping_province' => $transaction_contact->shipping_province,
//                        'shipping_city_id' => $transaction_contact->shipping_city_id,
//                        'shipping_city' => $transaction_contact->shipping_city,
//                        'shipping_districts_id' => $transaction_contact->shipping_districts_id,
//                        'shipping_district' => $transaction_contact->shipping_district,
//                        'shipping_postal_code' => $transaction_contact->shipping_postal_code,
//                        'created' => date('Y-m-d H:i:s'),
//                        'createdby' => "SYSTEM",
//                        'updated' => date('Y-m-d H:i:s'),
//                        'updatedby' => "SYSTEM",
//                    );
//                    $r_transaction_contact = $db->insert('transaction_contact', $data_transaction_contact);                
//                    
//                    $product=cdbutils::get_row("select * from product where product_id=".$db->escape($row->product_id));
//                    $data_transaction_detail = array(
//                        'transaction_id' => $transaction_id,
//                        'vendor_id' => $product->vendor_id,
//                        'product_id' => $product->product_id_lucky,
//                        'qty' => 1,
//                        'vendor_nta' => 0,
//                        'vendor_commission_value' =>0,
//                        'vendor_sell_price' => 0,
//                        'total_promo' => 0,
//                        'ho_upselling' => 0,
//                        'ho_sell_price' => 0,
//                        'channel_commission_full' => 0,
//                        'channel_commission_ho' => 0,
//                        'channel_commission_share' => 0,
//                        'channel_commission_share_ho' => 0,
//                        'channel_commission_value' => 0,
//                        'channel_updown' => 0,
//                        'channel_sell_price' => 0,
//                        'channel_profit' => 0,
//                        'created' => date('Y-m-d H:i:s'),
//                        'createdby' => "SYSTEM",
//                        'updated' => date('Y-m-d H:i:s'),
//                        'updatedby' => "SYSTEM",
//                    );
//                    $db->insert('transaction_detail', $data_transaction_detail);
//                    
//                    $transaction_payment_code_generate = generate_code::get_next_transaction_code();
//                    $data_payment = array(
//                        "transaction_id" => $transaction_id,
//                        "code" => $transaction_payment_code_generate,
//                        "date" => date('Y-m-d H:i:s'),
//                        "payment_status" => 'SUCCESS',
//                        "payment_type" => "winner_lucky_day",
//                        "total_item" => 0,
//                        "total_charge" => 0,
//                        "total_payment" => 0,
//                        "bank" => "",
//                        "account_number" => "",
//                        "created" => date('Y-m-d H:i:s'),
//                        "createdby" => "SYSTEM",
//                        "updated" => date('Y-m-d H:i:s'),
//                        "updatedby" => "SYSTEM",
//                    );
//                    $db->insert('transaction_payment', $data_payment);
//
//                    $data_transaction_history=array(
//                        'org_id' => $transaction->org_id,
//                        'org_parent_id' => $transaction->org_parent_id,
//                        'transaction_id' => $transaction_id,
//                        'date' => date('Y-m-d H:i:s'),
//                        'transaction_status_before'=>'PENDING',
//                        'transaction_status'=>'SUCCESS',
//                        'ref_table'=>'transaction',
//                        'ref_id'=>$transaction_id,
//                    );
//                    $db->insert('transaction_history',$data_transaction_history);
                    
                    // update status coupon winner
                    $db->update('lucky_coupon_quota',array('status_winner'=>'NOT WIN'),array('product_id'=>$product_id));
                    $db->update('lucky_coupon_quota',array('status_winner'=>'WINNER'),array('lucky_coupon_quota_id'=>$row->lucky_coupon_quota_id));
                }
            } catch (Exception $ex) {
                throw new Exception($ex);
            }
            return $transaction_id;
        }
        
        public static function check_proses_winner_lucky_day($product_id){
            $db=CDatabase::instance();
            $res=false;
            $q="
                select
                    *
                from
                    product
                where 
                    product_id=".$db->escape($product_id)."
            ";
            $product=cdbutils::get_row($q);
            $quota=cobj::get($product,'quota');
            $q="
                select
                    *
                from
                    lucky_coupon_quota
                where
                    product_id=".$product_id."
            ";
            $r=$db->query($q);
            $coupon_count=$r->count();
            if($quota==$coupon_count){
                $res=true;
            }
            return $res;
        }
        
        public static function insert_lucky_coupon_quota($transaction_id){
            $db=CDatabase::instance();
            $winner_transaction_id='';
            try{
                $q="
                    select
                        t.transaction_id as transaction_id,
                        t.org_id as org_id,
                        td.product_id as product_id,
                        td.qty as qty
                    from
                        transaction as t
                        inner join transaction_detail as td on td.transaction_id=t.transaction_id
                    where
                        t.transaction_id=".$db->escape($transaction_id)."
                ";
                $r=$db->query($q);
                if($r->count()>0){
                    foreach($r as $row){
                        $qty=$row->qty;
                        for($i = 1; $i <= $qty; $i++) {
                            
                            $data=array(
                                "org_id"=>$row->org_id,
                                "transaction_id"=>$row->transaction_id,
                                "product_id"=>$row->product_id,
                                "created"=>date('Y-m-d H:i:s'),
                                "createdby"=>'SYSTEM',
                                "updated"=>date('Y-m-d H:i:s'),
                                "updatedby"=>'SYSTEM',
                            );
                            $r_insert=$db->insert('lucky_coupon_quota',$data);
                            $id=$r_insert->insert_id();
                            $code=generate_code::get_next_lucky_coupon_code($id);
                            $db->update('lucky_coupon_quota',array('code'=>$code),array("lucky_coupon_quota_id"=>$id));
                        }
                    }
                    $choose_winner=self::check_proses_winner_lucky_day($r[0]->product_id);
                    if($choose_winner){
                        $winner_transaction_id=self::winner_lucky_day($r[0]->product_id);
                    }
                }
            } catch (Exception $ex) {
                throw new Exception($ex);
            }
            return $winner_transaction_id;
        }
        
        public static function get_last_winner_lucky_day($count = 10) {
            $db = CDatabase::instance();
            
            $return = array();
            
            $q = "SELECT 
                    p.name, p.url_key, p.sku, p.image_name,
                    t.email,
                    lcq.*,
                    m.name member_name
                    FROM lucky_coupon_quota lcq
                    INNER JOIN product p ON p.product_id = lcq.product_id
                    INNER JOIN `transaction` t ON t.transaction_id = lcq.transaction_id
                    LEFT JOIN member m ON m.member_id= t.member_id
                    WHERE lcq.status_winner = 'WINNER'
                    AND lcq.status > 0
                    GROUP BY lcq.product_id
                    ORDER BY lcq.updated DESC
                    LIMIT ".$count;
            
            $result = $db->query($q);
            if ($result != NULL) {
                foreach ($result as $result_k => $result_v) {
                    $arr['name'] = cobj::get($result_v, 'name');
                    $arr['url_key'] = cobj::get($result_v, 'url_key');
                    $arr['sku'] = cobj::get($result_v, 'sku');
                    $arr['image_name'] = cobj::get($result_v, 'image_name');
                    $arr['email'] = cobj::get($result_v, 'email');
                    $arr['member_name'] = cobj::get($result_v, 'member_name');
                    $arr['lucky_coupon_quota_id'] = cobj::get($result_v, 'lucky_coupon_quota_id');
                    $arr['org_id'] = cobj::get($result_v, 'org_id');
                    $arr['product_id'] = cobj::get($result_v, 'product_id');
                    $arr['transaction_id'] = cobj::get($result_v, 'transaction_id');
                    $arr['code'] = cobj::get($result_v, 'code');
                    $arr['updated'] = cobj::get($result_v, 'updated');
                    $return[] = $arr;
                }
            }
            return $return;
        }
    }