<?php

    class date {
        public static function array_date(){
            $array_data = array();
            for($date = 1; $date <= 31; $date++){
                $array_data[$date] = $date;
            }
            
            return $array_data;
        }
        
        public static function array_month(){
            $array_data = array();
            $array_data[1] = 'Januari';
            $array_data[2] = 'Februari';
            $array_data[3] = 'Maret';
            $array_data[4] = 'April';
            $array_data[5] = 'Mei';
            $array_data[6] = 'Juni';
            $array_data[7] = 'Juli';
            $array_data[8] = 'Agustus';
            $array_data[9] = 'September';
            $array_data[10] = 'Oktober';
            $array_data[11] = 'November';
            $array_data[12] = 'Desember';
            
            return $array_data;
        }
        
        public static function array_years($count_years = 20){
            $year_now = date('Y');
            $end_year = $year_now - $count_years;
            
            $array_data = array();
            for($year = $year_now; $year >= $end_year; $year--){
                $array_data[$year] = $year;
            }
            
            return $array_data;
        }
        
        public function format_date($date, $separator = '/'){
            $years = substr($date, 0, 4);
            $month = substr($date, 5, 2);
            $day = substr($date, 8, 2);
            
            return $day . $separator . $month . $separator . $years;
        }
    }