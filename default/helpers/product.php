<?php

class product {

    public static function get_package_product($product_id = null) {
        $db = CDatabase::instance();
        $data = array();

        if (empty($product_id))
            return $data;

        $q = ' 
            select pp.product_id as pp, p.* 
            from product_package pp 
            inner join product p on pp.related_package_id = p.product_id
            inner join product p2 on pp.product_id = p2.product_id 
            where pp.product_id=' . $db->escape($product_id) . '
                and pp.status > 0 
                and p.status > 0 
                and p.is_active =1 
                and p.is_available =1 
                and p.is_stock=1 
                and p.stock >0 
                and p.status_confirm="CONFIRMED"
                and p2.status > 0 
                and p2.is_active =1 
                and p2.is_available =1 
                and p2.is_stock=1 
                and p2.stock >0 
                and p2.status_confirm="CONFIRMED"
            ';

        $result = $db->query($q);
        if ($result->count() > 0) {
            foreach ($result as $key => $val) {
                $arr_product = array();

                //$product_price = product::get_product_price($val->product_id);
                $vendor = product::get_vendor(cobj::get($val, 'vendor_id'));

                $arr_product['product_type_id'] = $val->product_type_id;
                $arr_product['product_id'] = $val->product_id;
                $arr_product['vendor_id'] = $val->vendor_id;
                $arr_product['url_key'] = $val->url_key;
                $arr_product['code'] = $val->code;
                $arr_product['name'] = $val->name;
                $arr_product['sku'] = $val->sku;
                $arr_product['sku_ky'] = cobj::get($val, 'sku_ky');
                //$arr_product['image_path']= $val->image_path;
                $arr_product['weight'] = $val->weight;
                $arr_product['image_path'] = $val->image_name;
                $arr_product['image_url'] = $val->file_path;
                $arr_product['is_hot_item'] = $val->is_hot_item;
                $arr_product['new_product_date'] = $val->new_product_date;
                $arr_product['is_available'] = $val->is_available;
                $arr_product['quick_overview'] = $val->description;
                $arr_product['quota'] = $val->quota;
                $arr_product['stock'] = $val->stock;
                $arr_product['show_minimum_stock'] = $val->show_minimum_stock;
                $arr_product['overview'] = $val->overview;
                $arr_product['spesification'] = $val->spesification;
                $arr_product['faq'] = $val->faq;
                $arr_product['product_data_type'] = $val->product_data_type;
                //$arr_product['detail_price'] = $product_price;
                $arr_product['vendor_detail'] = $vendor;
                $arr_product['description'] = $val->description;
                $arr_product['start_bid_date'] = $val->start_bid_date;
                $arr_product['file_path_voucher'] = $val->file_path_voucher;
                $arr_product['image_name_voucher'] = $val->image_name_voucher;
                $arr_product['filename_voucher'] = $val->filename_voucher;
                $arr_product['end_bid_date'] = $val->end_bid_date;
                $arr_product['viewed'] = $val->viewed;
                $arr_product['vidio_link'] = $val->vidio_link;
                $arr_product['is_package_product'] = cobj::get($val, 'is_package_product');
                $arr_product['product_tag'] = cobj::get($val, 'product_tag');

                $data[] = $arr_product;
            }

            return $data;
        }
    }

    /**
     * 
     * @param mixed $data_product <b>product_id or data array</b>
     * @param int $org_id <b>default null</b>
     * @return boolean
     */
    public static function is_soldout($data_product = array(), $org_id = null) {
        $db = CDatabase::instance();
        if (empty($org_id))
            $org_id = CF::org_id();

        if (!is_array($data_product)) {
            $data_product = product::get_product($data_product);
        }

        if (count($data_product) == 0)
            return false;

        if (is_array($data_product)) {
            $product_id = carr::get($data_product, 'product_id');
            $stock = carr::get($data_product, 'stock');
            $is_available = carr::get($data_product, 'is_available');
            $product_data_type = carr::get($data_product, 'product_data_type');
            if ($is_available == 0) {
                return false;
            }
            if ($product_data_type == 'simple') {
                if ($stock == 0 && $is_available == 1)
                    return true;
            }
            else if ($product_data_type == 'configurable') {
                $q = '
                    select sum(p.stock) as total_stock 
                    from product p
                    where p.parent_id=' . $db->escape($product_id) . ' 
                        and p.status=1 
                        and p.is_active=1 
                        and p.is_stock=1
                        and p.is_available=1
                        and p.status_confirm="CONFIRMED"
                    ';
                if (!empty($org_id))
                    $q .= ' and org_id = ' . $db->escape($org_id) . ' ';

                $total_stock = cdbutils::get_value($q);
                if ($total_stock == null || $total_stock == 0) {
                    return true;
                }
            }
        }

        return false;
    }

    public static function voucher_product($voucher_code = null) {
        $db = CDatabase::instance();
        $session = Session::instance();

        $cart = Cart62hall_Product::factory();
        $cart_list = $cart->get_cart();
        $cart_list_image = $cart->get_cart();
        $data_cart = array();
        $total_harga = 0;
        foreach ($cart_list as $key => $value) {
            $data_cart[$key] = $value;
            foreach ($cart_list_image as $key_img => $value_img) {
                if ($key == $key_img) {
                    $data_cart[$key] = $value_img;
                }
            }
        }

        foreach ($data_cart as $item) {
            $product_id = $item['product_id'];
            if (strlen($product_id) > 0) {
                $total_harga += $item['sub_total'];
            }
        }

        $data = array();
        $err_code = 0;
        $err_msg = '';
        $date_now = date('Y-m-d');
        if ($voucher_code != null) {
            $q = 'select * from product_voucher where status > 0 and promo_code =' . $db->escape($voucher_code);
            $res = cdbutils::get_row($q);
            $period = cobj::get($res, 'period');
            $quota = cobj::get($res, 'quota');
            $type = cobj::get($res, 'type');
            $nominal_diskon = cobj::get($res, 'nominal');
            $min_price = cobj::get($res, 'min_price', 0);
            $product_voucher_id = cobj::get($res, 'product_voucher_id');
            if ($res != null) {
                if ($period < $date_now) {
                    $err_code++;
                    $err_msg = 'Periode Kode Voucher Telah Habis';
                    $session->delete('voucher_data');
                }
                if ($quota < 1) {
                    $err_code++;
                    $err_msg = 'Quota Voucher Telah Habis';
                    $session->delete('voucher_data');
                }
                if ($min_price > $total_harga) {
                    $err_code++;
                    $err_msg = 'Total pembelian tidak mencapai minimum untuk penggunaan Voucher ini';
                    $session->delete('voucher_data');
                }
                if ($err_code == 0) {
                    $voucher_data = array(
                        'voucher_code' => $voucher_code,
                        'type' => $type,
                        'nominal' => $nominal_diskon,
                        'product_voucher_id' => $product_voucher_id,
                    );
                    $session->set('voucher_data', $voucher_data);
                }
            } else {
                $err_code++;
                $err_msg = 'Kode Voucher Tidak Tersedia';
            }
        }
        $data = array(
            'err_code' => $err_code,
            'err_msg' => $err_msg,
        );
        return $data;
    }

    public static function get_liker($product_id, $member_id) {
        $db = CDatabase::instance();
        $org_id = CF::org_id();
        $is_like = false;
        $res_count = cdbutils::get_value("
            select
                count(member_wishlist_id) as total_liker
            from
                member_wishlist 
            where
               status > 0
                and product_id=" . $db->escape($product_id) . "
        ");
        if ($member_id != false) {
            $res = cdbutils::get_value("
            select
                member_id
            from
                member_wishlist 
            where
             member_id = " . $member_id . " and 
               status > 0
                and product_id=" . $db->escape($product_id) . " ");
            if ($res != null)
                $is_like = true;
        }
        $result = array(
            'like_count' => $res_count,
            'is_like' => $is_like
        );
        return $result;
    }

    public static function get_total_cart() {
        $cart = Cart62hall_Product::factory();
        $cart_list = $cart->get_cart();
        $count_item = count($cart_list);

        $total_barang = 0;
        if ($count_item > 0) {
            foreach ($cart_list as $data_cart) {
                $total_barang += $data_cart['qty'];
            }
        }

        return $total_barang;
    }

    public static function get_total_sales($product_id) {
        $db = CDatabase::instance();
        $org_id = CF::org_id();
        $res = cdbutils::get_value("
            select
                IFNULL(sum(td.qty),0) as total_sales
            from
                transaction_detail as td
                inner join transaction as t on t.transaction_id=td.transaction_id
            where
                t.order_status='SUCCESS'
                and td.product_id=" . $db->escape($product_id) . "
        ");
        return $res;
    }

    public static function get_product_type($type) {
        $db = CDatabase::instance();
        $q = "
            select
                product_type_id
            from
                product_type
            where
                name=" . $db->escape($type) . "
        ";
        $res = cdbutils::get_value($q);
        return $res;
    }

    public static function get_product($product_id) {
        $db = CDatabase::instance();
        $org_id = ccfg::get('org_id');
        $arr_product = null;
        $q = "
            select
                *
            from
                product
            where
                status>0
                and status_confirm = 'CONFIRMED'
                and product_id=" . $db->escape($product_id) . "
        ";
        
        if(!ccfg::get('compromall_system')){
            org::concat_query('', $q, $org_id);
        }
        
        $r = $db->query($q);
        if ($r->count() > 0) {
            $row = $r[0];
            $flag = $hot = ($row->is_hot_item > 0) ? 'Hot' : NULL;

            if (!empty($row->new_product_date)) {
                if (strtotime($row->new_product_date) >= strtotime(date('Y-m-d'))) {
                    $flag = $new = 'New';
                }
            }
            $product_price = product::get_product_price($row->product_id);
            $vendor = product::get_vendor(cobj::get($row, 'vendor_id'));
            $tax = $row->tax;
            if ($tax == NULL)
                $tax = 0;
            $arr_product['product_type_id'] = $row->product_type_id;
            $arr_product['product_id'] = $row->product_id;
            $arr_product['vendor_id'] = $row->vendor_id;
            $arr_product['url_key'] = $row->url_key;
            $arr_product['tax'] = $tax;
            $arr_product['code'] = $row->code;
            $arr_product['name'] = $row->name;
            $arr_product['sku'] = $row->sku;
            $arr_product['sku_ky'] = cobj::get($row, 'sku_ky');
            //$arr_product['image_path']= $row->image_path;
            $arr_product['weight'] = $row->weight;
            $arr_product['image_path'] = $row->image_name;
            $arr_product['image_url'] = $row->file_path;
            $arr_product['file_path'] = $row->file_path;
            $arr_product['is_hot_item'] = $row->is_hot_item;
            $arr_product['hot_until'] = $row->hot_until;
            $arr_product['new_product_date'] = $row->new_product_date;
            $arr_product['is_available'] = $row->is_available;
            $arr_product['quick_overview'] = $row->description;
            $arr_product['quota'] = $row->quota;
            $arr_product['stock'] = $row->stock;
            $arr_product['show_minimum_stock'] = $row->show_minimum_stock;
            $arr_product['overview'] = $row->overview;
            $arr_product['spesification'] = $row->spesification;
            $arr_product['faq'] = $row->faq;
            $arr_product['product_data_type'] = $row->product_data_type;
            $arr_product['detail_price'] = $product_price;
            $arr_product['vendor_detail'] = $vendor;
            $arr_product['description'] = $row->description;
            $arr_product['start_bid_date'] = $row->start_bid_date;
            $arr_product['file_path_voucher'] = $row->file_path_voucher;
            $arr_product['image_name_voucher'] = $row->image_name_voucher;
            $arr_product['filename_voucher'] = $row->filename_voucher;
            $arr_product['end_bid_date'] = $row->end_bid_date;
            $arr_product['viewed'] = $row->viewed;
            $arr_product['vidio_link'] = $row->vidio_link;
            $arr_product['is_package_product'] = cobj::get($row, 'is_package_product');
            $arr_product['product_tag'] = cobj::get($row, 'product_tag');
//            $arr_product['location_event'] = $row->location_event;
//            $arr_product['address'] = $row->address;
//            $arr_product['spesification'] = $row->spesification;
        }

        return $arr_product;
    }

    public static function get_product_lv($product_id) {
        $db = CDatabase::instance();
        $org_id = ccfg::get('org_id');
        $arr_product = null;
        $q = "
            select
                *
            from
                product
            where
                status>0
                and status_confirm = 'CONFIRMED'
                and product_id=" . $db->escape($product_id) . "
        ";
        $r = $db->query($q);
        if ($r->count() > 0) {
            $row = $r[0];
            $flag = $hot = ($row->is_hot_item > 0) ? 'Hot' : NULL;

            if (!empty($row->new_product_date)) {
                if (strtotime($row->new_product_date) >= strtotime(date('Y-m-d'))) {
                    $flag = $new = 'New';
                }
            }
            $product_price = product::get_product_price($row->product_id);
            $vendor = product::get_vendor(cobj::get($row, 'vendor_id'));

            $arr_product['product_type_id'] = $row->product_type_id;
            $arr_product['product_id'] = $row->product_id;
            $arr_product['vendor_id'] = $row->vendor_id;
            $arr_product['url_key'] = $row->url_key;
            $arr_product['code'] = $row->code;
            $arr_product['name'] = $row->name;
            $arr_product['sku'] = $row->sku;
            $arr_product['sku_ky'] = cobj::get($row, 'sku_ky');
            //$arr_product['image_path']= $row->image_path;
            $arr_product['weight'] = $row->weight;
            $arr_product['image_path'] = $row->image_name;
            $arr_product['file_path'] = $row->file_path;
            $arr_product['is_hot_item'] = $row->is_hot_item;
            $arr_product['hot_until'] = $row->hot_until;
            $arr_product['new_product_date'] = $row->new_product_date;
            $arr_product['is_available'] = $row->is_available;
            $arr_product['quick_overview'] = $row->description;
            $arr_product['quota'] = $row->quota;
            $arr_product['stock'] = $row->stock;
            $arr_product['show_minimum_stock'] = $row->show_minimum_stock;
            $arr_product['overview'] = $row->overview;
            $arr_product['spesification'] = $row->spesification;
            $arr_product['faq'] = $row->faq;
            $arr_product['product_data_type'] = $row->product_data_type;
            $arr_product['detail_price'] = $product_price;
            $arr_product['vendor_detail'] = $vendor;
            $arr_product['description'] = $row->description;
            $arr_product['start_bid_date'] = $row->start_bid_date;
            $arr_product['file_path_voucher'] = $row->file_path_voucher;
            $arr_product['image_name_voucher'] = $row->image_name_voucher;
            $arr_product['filename_voucher'] = $row->filename_voucher;
            $arr_product['end_bid_date'] = $row->end_bid_date;
            // $arr_product['address'] = $row->address;
            // $arr_product['spesification'] = $row->spesification;
        }

        return $arr_product;
    }

    public static function get_product_raja_ongkir($product_id) {
        $db = CDatabase::instance();
        $q = 'SELECT
            c.raja_ongkir_city_id, p.weight,p.shipping
        FROM
            product p
        LEFT JOIN city c ON c.city_id = p.city_id
        WHERE p.product_id = ' . $db->escape($product_id) . '
        AND p.status > 0
        ';
        $data_product = cdbutils::get_row($q);
        return $data_product;
    }

    public function get_lokal_details($product_id, $set_as_preview = false) {
        $db = CDatabase::instance();
        $q = '
            SELECT p.*, p.file_path as image_path,
                pr.name as province_name, c.name as city_name,
                pc.url_key as product_category_url_key, pc.name as product_category_name
            FROM product p
            INNER JOIN product_category pc ON pc.product_category_id = p.product_category_id
            LEFT JOIN province pr ON pr.province_id = p.province_id
            LEFT JOIN city c ON c.city_id = p.city_id
            WHERE p.product_id = ' . $db->escape($product_id) . '
                AND p.status_product = "FINISHED" AND p.status > 0
            ';
        if (!$set_as_preview) {
            $q .= " AND p.is_publish = 1 ";
        }
        $r = cdbutils::get_row($q);
        $data = array();
        if ($r != null) {
            foreach ($r as $key => $value) {
                $data[$key] = $value;
            }
        } else {
            throw new Exception(clang::__('Product not found'));
        }
        return $data;
    }

    public static function get_prelove_product($product_id) {
        $db = CDatabase::instance();
        $q = '
            SELECT p.*
            FROM product p
            WHERE p.product_id = ' . $db->escape($product_id) . '
                AND p.status_product = "FINISHED"
                AND p.is_publish = 1 AND p.status > 0
            ';
        $r = cdbutils::get_row($q);
        $data = array();
        if ($r != null) {
            $data['product_id'] = $r->product_id;
            $data['member_id'] = $r->member_id;
            $data['name'] = $r->name;
            $data['condition'] = $r->condition;
            $data['url_key'] = $r->url_key;
            $data['start_price'] = $r->start_price;
            $data['using_increment'] = $r->using_increment;
            $data['price_increment'] = $r->price_increment;
            $data['end_bid_date'] = $r->end_bid_date;
            $data['price_buy_it_now'] = $r->price_buy_it_now;
            $data['already_buy'] = $r->already_buy;
            $data['description'] = $r->description;
            $data['is_publish'] = $r->is_publish;
            $data['status_product'] = $r->status_product;
            $data['image_path'] = $r->file_path;
            $data['image_name'] = $r->image_name;
            $data['filename'] = $r->filename;
            $data['user_last_bid_price'] = $r->user_last_bid_price;
            $data['total_bid'] = $r->total_bid;
            $data['status_transaction'] = $r->status_transaction;
            $data['weight'] = $r->weight;
            $data['city_id'] = $r->city_id;
            $data['province_id'] = $r->province_id;
            $data['shipping'] = $r->shipping;
        } else {
            throw new Exception(clang::__('Product not found'));
        }
        return $data;
    }

    public function get_lokal_by_id($product_id) {
        $db = CDatabase::instance();
        $org_id = CF::org_id();
        $q = 'SELECT * FROM product WHERE status > 0 AND (org_id is null or org_id = ' . $db->escape($org_id) . ') AND product_id =' . $db->escape($product_id);
        $v = cdbutils::get_row($q);
        $data = null;
        if ($v != null) {
            $data = array();
            $data['product_id'] = $v->product_id;
            $data['member_id'] = $v->member_id;
            $data['name'] = $v->name;
            $data['condition'] = $v->condition;
            $data['url_key'] = $v->url_key;
            $data['description'] = $v->description;
            $data['is_publish'] = $v->is_publish;
            $data['status_product'] = $v->status_product;
            $data['image_path'] = $v->file_path;
            $data['image_name'] = $v->image_name;
            $data['filename'] = $v->filename;
            $data['sell_price'] = $v->sell_price;
        }
        return $data;
    }

    public function get_product_by_type($options = array()) {
        $db = CDatabase::instance();
        $limit = carr::get($options, 'limit');
        $desc = carr::get($options, 'order_desc');
        $asc = carr::get($options, 'order_asc');
        $product_type_id = carr::get($options, 'product_type_id', 1);
        $org_id = CF::org_id();

        $q = 'SELECT * FROM product WHERE status > 0 AND (org_id is null or org_id = ' . $db->escape($org_id) . ') AND status_confirm = "CONFIRMED" AND product_type_id =' . $db->escape($product_type_id);

        if ($desc != null && $asc == null) {
            $q .= ' ORDER BY ' . $desc . ' DESC';
        }

        if ($asc != null && $desc == null) {
            $q .= ' ORDER BY ' . $asc . ' ASC';
        }

        if ($limit != null) {
            $q .= ' LIMIT ' . $limit;
        }
        $row = $db->query($q);
        $data = array();
        $data_product = array();
        if ($row != null) {
            foreach ($row as $k => $v) {
                $data['product_id'] = $v->product_id;
                $data['member_id'] = $v->member_id;
                $data['name'] = $v->name;
                $data['condition'] = $v->condition;
                $data['url_key'] = $v->url_key;
                $data['description'] = $v->description;
                $data['is_publish'] = $v->is_publish;
                $data['status_product'] = $v->status_product;
                $data['image_path'] = $v->file_path;
                $data['image_name'] = $v->image_name;
                $data['filename'] = $v->filename;
                $data['sell_price'] = $v->sell_price;
                $data_product[] = $data;
            }
        }
        return $data_product;
    }

    public static function get_product_image($product_id) {
        $db = CDatabase::instance();
        $arr_product_image = array();

        // get image utama
        $q = "
                select
                    file_path
                from
                    product
                where
                    status > 0
                    and product_id=" . $db->escape($product_id) . "
                ";
        $image_utama = cdbutils::get_value($q);

        if (!empty($image_utama)) {
            $arr_product_image[]['image_path'] = $image_utama;
        }

        $q = "
                select
                    *
                from
                    product_image
                where
                    status > 0
                    and product_id=" . $db->escape($product_id) . "
                ";
        $r = $db->query($q);
        if ($r->count() > 0) {
            foreach ($r as $row) {
                $arr_product_image[]['image_path'] = $row->image_path;
            }
        }

        return $arr_product_image;
    }

    public static function get_product_image_lv($product_id) {
        $db = CDatabase::instance();
        $arr_product_image = array();

        // get image utama
        $q = "
                select
                    image_name
                from
                    product
                where
                    status > 0
                    and product_id=" . $db->escape($product_id) . "
                ";
        $image_utama = cdbutils::get_value($q);
//        cdbg::var_dump($image_utama);
//        die;
        if (!empty($image_utama)) {
            $image_utama = image::get_image_url($image_utama);
            $arr_product_image[]['image_path'] = $image_utama;
        }

        $q = "
                select
                    *
                from
                    product_image
                where
                    status > 0
                    and product_id=" . $db->escape($product_id) . "
                ";
        $r = $db->query($q);
        if ($r->count() > 0) {
            foreach ($r as $row) {
                $image_url = image::get_image_url($row->image_name);
                $arr_product_image[]['image_path'] = $image_url;
            }
        }
        return $arr_product_image;
    }

    public static function get_image_detail_product($product_id) {
        $db = CDatabase::instance();
        $arr_product_image = array();

        // get image utama
        $q = "
                select
                    file_path,
                    image_name
                from
                    product
                where
                    status > 0
                    and product_id=" . $db->escape($product_id) . "
                ";
        $image_utama = $db->query($q);
        if ($image_utama->count() > 0) {
            foreach ($image_utama as $qrow) {
                $arr_product_image[] = array(
                    'image_path' => $qrow->file_path,
//                    'is_package_product' => $qrow->is_package_product,
                );
            }
        }

        $q = "
                select
                    *
                from
                    product_image
                where
                    status > 0
                    and product_id=" . $db->escape($product_id) . "
                ";
        $r = $db->query($q);
        if ($r->count() > 0) {
            foreach ($r as $row) {
                if (strlen($row->image_path) != 0 || $row->image_path != null) {
                    $arr_product_image[] = array(
                        'image_path' => $row->image_path,
                        'image_name' => $row->image_name,
//                        'is_package_product' => $row->is_package_product,
                    );
                }
            }
        }
        return $arr_product_image;
    }

    public static function get_attribute_category() {
        $db = CDatabase::instance();
        $data_category_attribute = array();

        $query = '
                    SELECT
                        *
                    FROM
                        attribute_category
                    WHERE
                        status > 0
                        AND is_active = 1
                    ';

        $result = $db->query($query);
        if ($result->count() > 0) {
            foreach ($result as $key => $value) {
                $code = cobj::get($value, 'code');
                $name = cobj::get($value, 'name');
                $type = cobj::get($value, 'type');

                $attr_category_arr['code'] = $code;
                $attr_category_arr['name'] = $name;
                $attr_category_arr['type'] = $type;

                $data_category_attribute[] = $attr_category_arr;
            }
        }

        return $data_category_attribute;
    }

    /**
     *
     * @param Int $product_id
     * @return array('attribute_category_name','attribute_category_type','attribute_code','attribute_key')
     */
    public static function get_simple_product_from_attribute($product_id, $arr_attribute) {
        $db = CDatabase::instance();
        if (!self::have_child($product_id)) {
            $q = "
                    select
                        *
                    from
                        product
                    where
                        product_id=" . $db->escape($product_id) . "
                ";
            return cdbutils::get_row($q);
        }
        $q = 'select p.* from product as p ';
        $c = 0;
        $add_where = '';
        foreach ($arr_attribute as $cat_id => $att_id) {
            $c++;
            $q.=' inner join product_group_attribute as pga' . $c . ' on pga' . $c . '.product_id=p.product_id and pga' . $c . '.attribute_id=' . $db->escape($att_id) . ' and pga' . $c . '.attribute_category_id=' . $db->escape($cat_id);
            $add_where .=' and pga' . $c . '.status>0 ';
        }
        $q.=' where p.status>0 and p.parent_id=' . $db->escape($product_id) . $add_where;
        
        return cdbutils::get_row($q);
    }

    public static function have_child($product_id) {
        $db = CDatabase::instance();
        return cdbutils::get_value('select count(*) from product where parent_id=' . $db->escape($product_id) . ' and status>0') > 0;
    }

    public static function have_parent($product_id) {
        $db = CDatabase::instance();
        return cdbutils::get_value('select parent_id from product where product_id=' . $db->escape($product_id) . ' and status>0') != null;
    }

    public static function get_product_list_from_attribute($list_product_id, $arr_attribute, $range_filter_price = array(), $arr_filter_product = array(), $visibility = 'search_catalog', $filter_page = '', $filter_name = '', $filter_category_lft = '', $filter_category_rgt = '') {
        $db = CDatabase::instance();
        $q_pricing = "select
                        p.product_id
                        ,p.sell_price as vendor_sell_price
                        ,p.sell_price_nta as vendor_nta
                        ,p.sell_price-p.sell_price_nta as vendor_commission_value
                        ,p.ho_upselling as upselling
                        ,p.sell_price+p.ho_upselling as ho_sell_price
                        ,(p.sell_price-p.sell_price_nta)+p.ho_upselling as channel_commission_full
                        , p.ho_upselling as channel_commision_ho
                        ,case when value is null then 0 else case when pr.type = 'percent' then ((p.sell_price+p.ho_upselling)*pr.value/100) else pr.value end end as promo_total
                        , ((p.sell_price-p.sell_price_nta)+p.ho_upselling) - ((p.ho_upselling)+(case when value is null then 0 else case when pr.type = 'percent' then ((p.sell_price+p.ho_upselling)*pr.value/100) else pr.value end end))  as channel_commission_share
                        , (1-p.percent_share) * ((p.sell_price-p.sell_price_nta)+p.ho_upselling) - ((p.ho_upselling)+(case when value is null then 0 else case when pr.type = 'percent' then ((p.sell_price+p.ho_upselling)*pr.value/100) else pr.value end end)) as channel_commission_share_ho
                        , (p.percent_share) * ((p.sell_price-p.sell_price_nta)+p.ho_upselling) - ((p.ho_upselling)+(case when value is null then 0 else case when pr.type = 'percent' then ((p.sell_price+p.ho_upselling)*pr.value/100) else pr.value end end)) as channel_commission_value
                        ,case when pp.up-pp.down is null then 0 else pp.up-pp.down end as channel_updown
                        ,(p.sell_price+p.ho_upselling) - (case when value is null then 0 else case when pr.type = 'percent' then ((p.sell_price+p.ho_upselling)*pr.value/100) else pr.value end end) +(case when pp.up-pp.down is null then 0 else pp.up-pp.down end) as channel_sell_price
                        , ((p.percent_share) * ((p.sell_price-p.sell_price_nta)+p.ho_upselling) - ((p.ho_upselling)+(case when value is null then 0 else case when pr.type = 'percent' then ((p.sell_price+p.ho_upselling)*pr.value/100) else pr.value end end))) + (case when pp.up-pp.down is null then 0 else pp.up-pp.down end) as channel_profit


                    from (select 0.9 as percent_share,ppp.* from product as ppp where ppp.status>0)as p
                    left join (
                        select pg.priority,pgd.product_id,pgd.type,pgd.value
                        from
                            promo_group_detail as pgd
                            left join promo_group as pg on pgd.promo_group_id=pg.promo_group_id
                        where
                                pg.status>0
                                and pgd.status>0
                                and pg.is_active>0
                                and pg.status_confirm='confirmed'
                                and date(pg.date_start)>=" . $db->escape(date('Y-m-d')) . "
                                and date(pg.date_end)<=" . $db->escape(date('Y-m-d')) . "
                        group by
                            pgd.product_id
                        having pg.priority=min(pg.priority)

                    ) as pr on pr.product_id=p.product_id
                    left join product_price as pp on pp.product_id=p.product_id
                    ";

        $q = 'select distinct case when p.parent_id is null then p.product_id else case when ((p.visibility="catalog_search") or(p.visibility="' . $visibility . '")) then p.product_id else p.parent_id end end as parent_product_id from product as p ';
        $c = 0;
        $db = CDatabase::instance();
        $add_where = '';
        $q.=' inner join (' . $q_pricing . ') as pr on pr.product_id=p.product_id';
        if (isset($range_filter_price) && is_array($range_filter_price) && count($range_filter_price) == 2) {
            $add_where .= ' and pr.channel_sell_price>=' . $db->escape($range_filter_price[0]) . ' and pr.channel_sell_price<=' . $db->escape($range_filter_price[1]) . ' ';
        }
        $q.=' left join product as pp on pp.product_id=p.parent_id';
        $q.=' left join product as ps on ps.parent_id=p.product_id';
        $q.=' left join product_category as pc on pc.product_category_id=p.product_category_id';
        $q.=' left join product_type as pt on pt.product_type_id=p.product_type_id';
        foreach ($arr_attribute as $cat_id => $att_ids) {
            $c++;
            $q.=' left join product_group_attribute as pga' . $c . ' on pga' . $c . '.product_id=(case when ps.product_id is null then p.product_id else ps.product_id end) and pga' . $c . '.attribute_category_id=' . $db->escape($cat_id);
            if (count($att_ids) > 0) {

                $att_id_in = implode(',', $att_ids);
                $add_where .='and pga' . $c . '.attribute_id in (' . ($att_id_in) . ')';
            }

            //$add_where .='and pga'.$c.'.attribute_id='.$db->escape($att_id).'';
            $add_where .=' and (case when pga' . $c . '.status is null then 1 else pga' . $c . '.status end) >0 ';
        }
        $q.=' where p.status>0 and p.is_active>0 and p.status_confirm="confirmed" ' . $add_where;
        $q.=" and (case when p.parent_id is null then 1=1 else case when ((p.visibility='catalog_search') or(p.visibility='" . $visibility . "')) then 1=1 else pp.status_confirm='CONFIRMED' and pp.status>0 end end)";
        $q.=" and (case when (ps.product_id is null and p.product_data_type='configurable') then 0=1 else case when (p.parent_id is null and p.product_data_type='simple') then 1=1 else ps.status>0 end end)";

        if (strlen($visibility) > 0) {
            $q.=" and ((p.visibility='catalog_search') or(p.visibility='" . $visibility . "'))";
        }
        if (strlen($filter_page) > 0) {
            $q.=" and pt.name=" . $db->escape($filter_page);
        }
        if (strlen($filter_name) > 0) {
            $arr_filter_name = explode(' ', $filter_name);
            if (count($arr_filter_name) > 0) {
                $ind = 0;
                $q.="and (";
                foreach ($arr_filter_name as $val) {
                    if ($ind > 0) {
                        $q.="or";
                    }
                    $q.="
                            (p.name like '%" . $val . "%')
                        ";
                    $ind++;
                }
                $q.=")";
            }
        }
        if (strlen($filter_category_lft) > 0) {
            $q.="and pc.lft>=" . $filter_category_lft . "";
            $q.=" and pc.rgt<=" . $filter_category_rgt . "";
        }
        $q.="
            order by
                p.updated desc
        ";
        $r = $db->query($q);
        $result = array();
        foreach ($r as $row) {
            $product = product::get_product($row->parent_product_id);
            if ($product != null) {
                $result[] = $product;
            }
        }
        return $result;





        $arr_product_id = array();
        $res_arr_product = array();
        if (count($arr_attribute_id) > 0) {
            $q = "
                                    select
                                        p.product_id
                                    from
                                        product as p
                                        inner join product_group_attribute as pga on pga.product_id=p.product_id or pga.product_id=cp.product_id
                                        left join attribute_category as atc on atc.attribute_category_id=pga.attribute_category_id
                                    where
                                        pga.status>0
                            ";
            foreach ($arr_attribute_id as $key => $val) {
                $q.="
                                            and atc.code=" . $db->escape($key) . "
                                            and pga.attribute_id in (" . $val . ")
                                    ";
            }
        } else {
            $q = "
                                select
                                    p.*
                                from product p
                                where p.product_id in (" . $list_product_id . ")
                                    ";
        }

        // FILTER NEW ITEM
        if (!is_array($arr_filter_product)) {
            $arr_filter_product = explode(',', $arr_filter_product);
        }
        if (count($arr_filter_product) > 0) {
            if (carr::get($arr_filter_product, 'is_new')) {
                $q.="
                                    and p.new_product_date >= " . $db->escape(date('Y-m-d')) . "
                                    ";
            }
        }

        $r = $db->query($q);

        if ($r->count() > 0) {
            foreach ($r as $row) {
                $arr_product_id[] = $row->parent_product_id;
            }
        }

        // FILTER DISKON or FILTER PRICE
        if (in_array('diskon', $arr_filter_product) || count($range_filter_price) > 0) {
            $set_product_id = array();
            foreach ($arr_product_id as $product_id) {
                $product_diskon = FALSE;
                $data_product_price[$product_id] = product::get_product_price($product_id);
                $data_price[$product_id] = product::get_price($data_product_price[$product_id]);

                if ($data_price[$product_id]['promo_price'] > 0) {
                    $product_diskon = TRUE;
                    $price = $data_price[$product_id]['promo_price'];
                } else {
                    $price = $data_price[$product_id]['price'];
                }

                // $range_filter_price[0] = paramater price minimum
                // $range_filter_price[1] = paramater price maximul
                if (in_array('diskon', $arr_filter_product)) {
                    if ($product_diskon and $price >= $range_filter_price[0] and $price <= $range_filter_price[1]) {
                        $set_product_id[$product_id] = $product_id;
                    }
                } else {
                    if ($price >= $range_filter_price[0] and $price <= $range_filter_price[1]) {
                        $set_product_id[$product_id] = $product_id;
                    }
                }
            }

            $arr_product_id = $set_product_id;
        }

        foreach ($arr_product_id as $product_id) {
            $res_arr_product[] = product::get_product($product_id);
        }

        return $res_arr_product;
    }

    public static function get_attribute_category_multi_product($arr_product, $visibility = 'catalog_search') {
        $db = CDatabase::instance();
        $attribute_category = array();
        foreach ($arr_product as $key => $val) {
            $attribute_product = product::get_attribute_category_all($val, $visibility);
            if (count($attribute_product) > 0) {
                foreach ($attribute_product as $key_ap => $val_ap) {
                    $exist = carr::get($attribute_category, $key_ap);
                    if ($exist == null) {
                        $attribute_category[$key_ap]['attribute_category_id'] = $val_ap['attribute_category_id'];
                        $attribute_category[$key_ap]['attribute_category_name'] = $val_ap['attribute_category_name'];
                        $attribute_category[$key_ap]['attribute_category_type'] = $val_ap['attribute_category_type'];
                        $attribute_category[$key_ap]['attribute'] = $val_ap['attribute'];
                    }
                    foreach ($val_ap['attribute'] as $key_a => $val_a) {
                        $exist = carr::get($attribute_category[$key_ap]['attribute'], $key_a);
                        if ($exist == null) {
                            $attribute_category[$key_ap]['attribute'][$key_a] = $val_a;
                        }
                    }
                }
            }
        }
        return $attribute_category;
    }

    public static function get_attribute_category_all($product_id, $visibility = 'catalog_search') {
        $db = CDatabase::instance();
        $attribute_category = array();
        $q = "
                select
                    p.product_id as product_id,
                    p.attribute_set_id as attribute_set_id
                from
                    product as p
                    left join product as pp on pp.product_id=p.parent_id
                    left join product as ps on ps.parent_id=p.product_id
                where
                    p.status>0
                    and (case when p.parent_id is null then 1=1 else case when ((p.visibility='catalog_search') or(p.visibility='" . $visibility . "')) then 1=1 else pp.status_confirm='CONFIRMED' and pp.status>0 end end)
                    and (case when (ps.product_id is null and p.product_data_type='configurable') then 0=1 else case when (p.parent_id is null and p.product_data_type='simple') then 1=1 else ps.status>0 end end)
                    and p.status_confirm='CONFIRMED'
                    and p.product_id=" . $db->escape($product_id) . "
            ";
        $product = cdbutils::get_row($q);
        if ($product) {
            $q = "
                    select
                        atc.attribute_category_id as attribute_category_id,
                        atc.code as attribute_category_code,
                        atc.name as attribute_category_name,
                        atc.type as attribute_category_type,
                        atc.url_key as attribute_category_url_key
                    from
                        attribute_set as ats
                        inner join product_attribute_category as pac on pac.attribute_set_id=ats.attribute_set_id
                        inner join attribute_category as atc on atc.attribute_category_id=pac.attribute_category_id
                    where
                        pac.status>0
                        and ats.attribute_set_id=" . $db->escape($product->attribute_set_id) . "
                        and pac.product_id=" . $db->escape($product_id) . "
                ";
            $r = $db->query($q);
           
            if ($r->count() > 0) {
                $prev_attribute_category_key = '';
                $prev_attribute_category = array();
                foreach ($r as $row) {
                    $prev_attribute_category[] = $row->attribute_category_code;
                    $attribute_category[$row->attribute_category_code] = array(
                        "attribute_category_id" => $row->attribute_category_id,
                        "attribute_category_type" => $row->attribute_category_type,
                        "attribute_category_code" => $row->attribute_category_code,
                        "attribute_category_url_key" => $row->attribute_category_url_key,
                        "attribute_category_name" => $row->attribute_category_name,
                        "attribute" => product::get_attribute_list($product_id, $row->attribute_category_id),
                    );
                    if (strlen($prev_attribute_category_key) > 0) {
                        $attribute_category[$prev_attribute_category_key]['next_attribute_category'] = $row->attribute_category_code;
                    }
                    if (count($prev_attribute_category) > 0) {
                        $attribute_category[$row->attribute_category_code]['prev_attribute_category'] = $prev_attribute_category;
                    }
                    $prev_attribute_category_key = $row->attribute_category_code;
                }
            }
        }
        return $attribute_category;
    }

    public static function get_product_list_with_attribute($arr_product_id, $attribute_id) {
        $db = CDatabase::instance();
        $res_product_id = array();
        $product_id = '';
        if (count($arr_product_id) > 0) {
            foreach ($arr_product_id as $key => $val) {
                $product_id.=$val . ',';
            }
            $product_id = substr($product_id, 0, -1);
            $q = "
                    select
                        p.product_id as product_id
                    from
                        product_group_attribute as pga
                        inner join product as p on p.product_id=pga.product_id
                    where
                        pga.product_id in (" . $product_id . ")
                        and p.status_confirm='CONFIRMED'
                        and pga.attribute_id=" . $db->escape($attribute_id) . "
                ";
            $r = $db->query($q);
            if ($r->count() > 0) {
                foreach ($r as $row) {
                    $res_product_id[] = $row->product_id;
                }
            }
        }
        return $res_product_id;
    }

    public static function get_attribute_list($product_id, $attribute_category_id, $arr_attribute_id = array()) {
        $db = CDatabase::instance();
        $attribute = array();
        $data_attribute = array();
        $arr_product[] = $product_id;
        $q = "
                select
                    product_id
                from
                    product
                where
                    status>0
                    and status_confirm='CONFIRMED'
                    and parent_id=" . $db->escape($product_id) . "
            ";
        $r = $db->query($q);
        if ($r->count() > 0) {
            foreach ($r as $row) {
                $arr_product[] = $row->product_id;
            }
        }
        foreach ($arr_attribute_id as $key => $val) {
            $arr_product = product::get_product_list_with_attribute($arr_product, $val);
        }
        foreach ($arr_product as $key => $val) {
            $q = "
                    select
                        at.attribute_id as attribute_id,
                        at.key as attribute_key,
                        at.code as attribute_code,
                        at.url_key as attribute_url_key,
                        at.file_path as file_path
                    from
                        product_group_attribute pga
                        inner join product as p on p.product_id=pga.product_id
                        left join attribute at on pga.attribute_id = at.attribute_id

                    where
                        pga.product_id=" . $db->escape($val) . "
                        and p.status_confirm='CONFIRMED'
                        and pga.attribute_category_id=" . $db->escape($attribute_category_id) . "
                ";
            $r = $db->query($q);
            if ($r->count() > 0) {
                foreach ($r as $row) {
                    $exist = carr::get($attribute, $row->attribute_id);
                    if ($exist == null) {
                        $data_attribute[$row->attribute_id] = array(
                            'attribute_id' => $row->attribute_id,
                            'attribute_key' => $row->attribute_key,
                            'attribute_code' => $row->attribute_code,
                            'attribute_url_key' => $row->attribute_url_key,
                            'file_path' => $row->file_path,
                        );
                    }
                }
            }
        }
        if (count($arr_attribute_id) > 0) {
            $q = "
                    select
                        *
                    from
                        attribute_category
                    where
                        attribute_category_id=" . $db->escape($attribute_category_id) . "
                ";
            $r = $db->query($q);
            if ($r->count() > 0) {
                $row = $r[0];
                $attribute['attribute_category'] = $row->code;
                $attribute['attribute_category_name'] = $row->name;
                $attribute['attribute_category_id'] = $row->attribute_category_id;
                $attribute['attribute_category_type'] = $row->type;
            }
            $attribute['data'] = $data_attribute;
        } else {
            $attribute = $data_attribute;
        }
        return $attribute;
    }

    public static function get_attribute($product_id) {

        $db = CDatabase::instance();
        $data_attribute = array();

        $q_get_attribute = '
                select pga.*
                ,ac.name as attr_category_name
                ,ac.type as attr_category_type
                ,at.code as attr_code
                ,at.key as attr_key
                ,at.file_path as attr_file_path
                from product_group_attribute pga
                left join attribute_category ac on pga.attribute_category_id = ac.attribute_category_id
                left join attribute at on pga.attribute_id = at.attribute_id
                where pga.status > 0
                    and ac.status > 0
                    and at.status > 0
                    and pga.product_id = ' . $product_id . '
                ';

        $result_get_attribute = $db->query($q_get_attribute);
        if ($result_get_attribute->count() > 0) {
            foreach ($result_get_attribute as $key => $value) {
                $attr_category_name = cobj::get($value, 'attr_category_name');
                $attr_category_type = cobj::get($value, 'attr_category_type');
                $attr_code = cobj::get($value, 'attr_code');
                $attr_key = cobj::get($value, 'attr_key');
                $attr_file_path = cobj::get($value, 'attr_file_path');

                $attr_arr['attribute_category_name'] = $attr_category_name;
                $attr_arr['attribute_category_type'] = $attr_category_type;
                $attr_arr['attribute_code'] = $attr_code;
                $attr_arr['attribute_key'] = $attr_key;
                $attr_arr['attribute_file_path'] = $attr_file_path;

                $data_attribute[] = $attr_arr;
            }
        }

        return $data_attribute;
    }

    public function get_product_price($product_id) {
        $db = CDatabase::instance();
        $org_id = CF::org_id();
        $org_parent_id = $org_id;
        $org = org::get_org($org_id);
        $tax = 0;
        if ($org->parent_id != null && $org->parent_id > 0) {
            $org_parent_id = $org->parent_id;
            if (ccfg::get('have_parent_setting_price')) {
                $org_parent_id = $org->parent_id;
            } elseif (ccfg::get('have_org_first_parent') > 0) {
                $org_first_parent = org::get_first_parent_org($org_id);
                $org_parent_id = cobj::get($org_first_parent, 'org_id');
                //cdbg::var_dump($org_first_parent);
            }
        }
        $product_price = array();
        $q = "
                    select
                            *
                    from
                            org
                    where
                            org_id=" . $db->escape($org_id) . "
            ";
        $org = $db->query($q);
        $org_category = 'express';
        if ($org->count() > 0) {
            $row = $org[0];
            $org_category = $row->org_category;
        }
        $percent_share = 0.6;
        if (strtolower($org_category) == 'premium') {
            $percent_share = 0.9;
        }
        
        $q = "
                    select
                        *
                    from
                        product as p
                        
                    where
                        p.product_id=" . $db->escape($product_id) . "
                    lock in share mode
            ";
       
        $r = $db->query($q);
        if ($r->count() > 0) {
            $row = $r[0];
            $attribute_price = 0;;
            $vendor_purchase_nta = $row->purchase_price_nta;
            $vendor_purchase_price = $row->purchase_price;
            $vendor_nta = $row->sell_price_nta;
            $vendor_sell_price = $row->sell_price;
            $vendor_purchase_commission_value = $vendor_purchase_price - $vendor_purchase_nta;
            $vendor_commission_value = $vendor_sell_price - $vendor_nta;
            $ho_upselling = $row->ho_upselling;
            $ho_commission = $row->ho_commission;
            $ho_purchase_price = $vendor_purchase_price;
            $ho_sell_price = $vendor_sell_price + $ho_upselling + $attribute_price;
            $channel_purchase_commission_full = $vendor_purchase_commission_value;
            $channel_commission_full = $vendor_commission_value + $ho_upselling;
            $channel_commision_ho = $ho_commission;
            $tax = $row->tax;
            $sell_price_start = $row->sell_price_start;
            $sell_price_end = $row->sell_price_end;


            $parent_product_id = $row->product_id;
            if($row->parent_id!=null&&$row->parent_id!=0) {
                $parent_product_id = $row->parent_id;
            }
            
            $q_promo = "
                            select
                                    pg.*,
                                    pgd.*
                            from
                                    promo_group as pg
                                    inner join promo_group_detail as pgd on pgd.promo_group_id=pg.promo_group_id
                            where
                                    pg.status>0
                                    and pgd.status>0
                                    and pg.status_confirm='confirmed'
                                    and pg.is_active>0
                                    and date(pg.date_start)<=" . $db->escape(date('Y-m-d')) . "
                                    and date(pg.date_end)>=" . $db->escape(date('Y-m-d')) . "
                                    and pgd.product_id=" . $db->escape($parent_product_id) . "
                            order by
                                    pg.priority asc
                    ";
            $r_promo = $db->query($q_promo);
            $promo_type = 'amount';
            $promo_start_date = '';
            $promo_end_date = '';
            $promo_value = 0;
            $promo_amount = 0;
            if ($r_promo->count() > 0) {
                $row_promo = $r_promo[0];
                $promo_start_date = $row_promo->date_start;
                $promo_end_date = $row_promo->date_end;
                $promo_type = $row_promo->type;
                $promo_value = $row_promo->value;
                $promo_amount = $row_promo->value;
                if ($promo_type == 'percent') {
                    $promo_amount = ($ho_sell_price * $promo_value) / 100;
                }
            }

            $channel_purchase_commission_share = $channel_purchase_commission_full;
            $channel_commission_share = $channel_commission_full - ($channel_commision_ho + $promo_amount);
            $channel_purchase_commission_share_ho = (1 - $percent_share) * $channel_purchase_commission_share;
            $channel_commission_share_ho = (1 - $percent_share) * $channel_commission_share;
            $channel_purchase_commission_value = $percent_share * $channel_purchase_commission_share;
            $channel_commission_value = $percent_share * $channel_commission_share;
            $q_org_price = "
                            select
                                    *
                            from
                                    product_price
                            where
                                    status>0
                                    and product_id=" . $db->escape($parent_product_id) . "
                                    and org_id=" . $db->escape($org_parent_id) . "
                    ";
            $channel_updown = 0;
            $pp_end_date = '';
            $r_org_price = $db->query($q_org_price);
            if ($r_org_price->count() > 0) {
                $row_org_price = $r_org_price[0];
                $channel_updown = $row_org_price->up - $row_org_price->down;
                $pp_end_date = date('Y-m-d', strtotime($row_org_price->end_date));
                $member = member::get_data_member();
                if ($member) {
                    $channel_updown_member = $row_org_price->up_member - $row_org_price->down_member;
                    if ($channel_updown_member > 0) {
                        $channel_updown = $channel_updown_member;
                    }
                }
            }

            if (strlen($pp_end_date) > 0) {
                $promo_end_date = $pp_end_date;
            }

            $channel_purchase_price = $ho_purchase_price;
            $channel_sell_price = $ho_sell_price - $promo_amount + $channel_updown;
            $channel_purchase_profit = $channel_purchase_commission_value;
            $channel_profit = $channel_commission_value + $channel_updown;

            $product_price['vendor_purchase_nta'] = $vendor_purchase_nta;
            $product_price['vendor_purchase_price'] = $vendor_purchase_price;
            $product_price['vendor_purchase_commission_value'] = $vendor_purchase_commission_value;
            $product_price['ho_purchase_price'] = $ho_purchase_price;
            $product_price['channel_purchase_commission_full'] = $channel_purchase_commission_full;
            $product_price['channel_purchase_commission_share'] = $channel_purchase_commission_share;
            $product_price['channel_purchase_commission_share_ho'] = $channel_purchase_commission_share_ho;
            $product_price['channel_purchase_commission_value'] = $channel_purchase_commission_value;
            $product_price['channel_purchase_price'] = $channel_purchase_price;
            $product_price['channel_purchase_profit'] = $channel_purchase_profit;


            $product_price['vendor_nta'] = $vendor_nta;
            $product_price['vendor_sell_price'] = $vendor_sell_price;
            $product_price['vendor_commission_value'] = $vendor_commission_value;
            $product_price['ho_upselling'] = $ho_upselling;
            $product_price['ho_sell_price'] = $ho_sell_price + $tax;
            $product_price['channel_commission_full'] = $channel_commission_full;
            $product_price['channel_commission_ho'] = $channel_commision_ho;
            $product_price['promo_start_date'] = $promo_start_date;
            $product_price['promo_end_date'] = $promo_end_date;
            $product_price['promo_type'] = $promo_type;
            $product_price['promo_value'] = $promo_value;
            $product_price['promo_amount'] = $promo_amount;
            $product_price['channel_commission_share'] = $channel_commission_share;
            $product_price['channel_commission_share_ho'] = $channel_commission_share_ho;
            $product_price['channel_commission_value'] = $channel_commission_value;
            $product_price['channel_updown'] = $channel_updown;
            $product_price['channel_sell_price'] = $channel_sell_price + $tax;
            $product_price['channel_profit'] = $channel_profit;
            $product_price['promo_text'] = 0;
            if ($ho_sell_price != 0) {
                $promo_text = round((($ho_sell_price - $channel_sell_price) / $ho_sell_price) * 100);
                if ($promo_text == 0) {
                    $promo_text = round((($ho_sell_price - $channel_sell_price) / $ho_sell_price) * 100, 2);
                }
                if ($promo_text == 0.00) {
                    $promo_text = 0;
                }
                $product_price['promo_text'] = $promo_text;
            }
            $product_price['sell_price_start'] = $sell_price_start;
            $product_price['sell_price_end'] = $sell_price_end;
        }
        return $product_price;
    }

    public static function get_price($data_price) {
        $price = 0;
        $promo_price = 0;
        $promo_value = $data_price['promo_value'];
        
        if ($data_price['ho_sell_price'] > $data_price['channel_sell_price']) {
            $price = $data_price['ho_sell_price'];
            $promo_price = $data_price['channel_sell_price'];
        }else {
            $price = $data_price['channel_sell_price'];
        }
        // JIKA PROMO DIBUAT  100%  
        if($promo_value == 100){
            return array('price' => $price, 'promo_price' => $promo_price,'promo_value' =>$promo_value);
        }else{
            return array('price' => $price, 'promo_price' => $promo_price);
        }
    }

    public static function get_range_price($arr_product = array()) {
        $data_produck_price = array();
        $data_price = array();
        $price = array();
        $min = 0;
        $max = 0;
        foreach ($arr_product as $product_id) {
            $data_produck_price[$product_id] = product::get_product_price($product_id);
            $data_price[$product_id] = product::get_price($data_produck_price[$product_id]);

            if ($data_price[$product_id]['promo_price'] != 0) {
                $price[] = $data_price[$product_id]['promo_price'];
            } else {
                $price[] = $data_price[$product_id]['price'];
            }
        }

        if ($price) {
            $min = min($price);
            $max = max($price);
        }
        return array('min' => $min, 'max' => $max);
    }

    public static function format_k($price) {
        $result = 0;
        if ($price > 1000) {
            $result = substr($price, 0, -3) . 'K';
        } else {
            $result = $price;
        }

        return $result;
    }

    public function get_wholesales($product_id) {
        $err_code = 0;
        $err_message = '';
        $db = CDatabase::instance();
        $org_id = CF::org_id();
        $wholesales = array();
        $q = "
                SELECT qty, price, description from product_price_wholesaler where status > 0 and product_id = " . $db->escape($product_id) . " ";
//                AND org_id = " . $db->escape($org_id) . " ";
        $r = $db->query($q);
        if ($r->count() > 0) {
            foreach ($r as $v) {
                $wholesale = array();
                $wholesale['qty'] = $v->qty;
                $wholesale['price'] = $v->price;
                $wholesale['description'] = $v->description;
                $wholesales[] = $wholesale;
            }
        }

        return $wholesales;
    }

    public function get_wholesales_price($arr_whole = array(), $qty = null) {
        if (count($arr_whole) > 0 && $qty != null) {
            $sortarray = array();
            foreach ($arr_whole as $key => $row) {
                $sortarray[$key] = $row['qty'];
            }
            array_multisort($sortarray, SORT_DESC, $arr_whole);
            foreach ($arr_whole as $key => $value) {
                $w_qty = carr::get($value, 'qty');
                if ($qty > $w_qty) {
                    $w_price = carr::get($value, 'price');
                    return $w_price;
                }
            }
        }
    }

    public static function get_weight_gold($product_category_id = '') {
        $db = CDatabase::instance();
        $product_type_id = self::get_product_type('gold');
        $query = "
                select
                        distinct weight
                from
                        product
                where
                        status>0
                        and status_confirm = 'CONFIRMED'
                        and product_type_id=" . $db->escape($product_type_id) . "
        ";
        if (strlen($product_category_id) > 0) {
            $query.="
                and product_category_id=" . $db->escape($product_category_id) . "
            ";
        }
        $query.="
                order by weight
        ";

        $data = array();
        $result = $db->query($query);
        if ($result->count() > 0) {
            foreach ($result as $row) {
                $data[$row->weight] = $row->weight;
            }
        }

        return $data;
    }

    public static function get_product_related($product_id) {
        $db = CDatabase::instance();
        $arr = array();

        $q_row = "
                        select
                                p.*
                        from
                                product p
                                inner join product_related as pr on p.product_id=pr.product_related_by_id
                        where
                                p.status>0
                                and p.status_confirm = 'CONFIRMED'
                                and p.is_active = '1'
                                and pr.product_id=" . $db->escape($product_id) . "
                ";

        if (ccfg::get('hide_nostock')) {
            $q_row.=" and (p.stock>0 or p.is_package_product=1)";
        }

        $r_row = $db->query($q_row);
        if ($r_row->count() > 0) {
            foreach ($r_row as $row) {
                $arr_product = product::get_product($row->product_id);
                $arr[] = $arr_product;
            }
        }

        return $arr;
    }
    
    public static function get_product_related_by_category($product_id, $org_id=null) {
        $db = CDatabase::instance();
        $arr = array();
        if($org_id==null) $org_id=CF::org_id();

        $q_cat = "select pc.* from product as p inner join product_category pc on p.product_category_id=pc.product_category_id where p.product_id=".$db->escape($product_id);
        $row_cat = cdbutils::get_row($q_cat);
        
        $q_where_org = "";
        
        $q_row = "
            select
                    p.*
            from
                    product p
                    inner join product_category as pc on p.product_category_id=pc.product_category_id
            where
                    p.status>0
                    and p.status_confirm = 'CONFIRMED'
                    and p.is_active = '1'
                    and p.product_id <> ".$db->escape($product_id)."
                    and p.product_category_id=".$db->escape($row_cat->product_category_id)."
                    and p.product_type_id=1
                    and p.org_id= ".$db->escape($org_id)."
            order by p.updated desc
            limit 12;
                            
        ";
        $r_row = $db->query($q_row);
        if ($r_row->count() > 0) {
            foreach ($r_row as $row) {
                $arr_product = product::get_product($row->product_id);
                $arr[] = $arr_product;
            }
        }

        return $arr;
    }

    public static function get_count($options) {
        $options['limit_start'] = null;
        $options['limit_end'] = null;

        $q = self::get_query($options);
        $q = "select count(*) as cnt from (" . $q . ") as t";
        $count = cdbutils::get_value($q);
        if (strlen(carr::get($options, 'search')) > 0) {
            $session = Session::instance();
            log62::search(carr::get($options, 'search'), $session->get('member_id'), $count);
        }
        return $count;
    }

    public static function get_attributes_array($options) {

        $result = self::get_attributes($options);
        $arr = array();

        foreach ($result as $row) {
            if (!isset($arr[$row->code])) {
                $arr[$row->code] = array(
                    "attribute_category_id" => $row->attribute_category_id,
                    "attribute_category_name" => $row->name,
                    "attribute_category_type" => $row->type,
                    "attribute_category_url_key" => $row->attribute_category_url_key,
                    "attribute" => array()
                );
            }

            $attribute = &$arr[$row->code]["attribute"];
            $attribute[$row->attribute_id] = array(
                "attribute_key" => $row->key,
                "attribute_url_key" => $row->attribute_url_key,
                "file_path" => $row->file_path,
            );
        }
        return $arr;
    }

    public static function get_attributes($options) {
        $options['limit_start'] = null;
        $options['limit_end'] = null;
        $options['visibility'] = 'none';
        $q = self::get_query($options);
        $q = '
                select atc.attribute_category_id,atc.code,atc.name, atc.type,att.attribute_id,
                    att.key, att.file_path, atc.url_key as attribute_category_url_key,
                    att.url_key as attribute_url_key
        from (
        select pgaa.attribute_category_id,pgaa.attribute_id from
        product_group_attribute as pgaa
        inner join (' . $q . ') as p on p.product_id=pgaa.product_id
        where pgaa.status>0

        group by attribute_category_id,attribute_id) as pga
        left join attribute as att on pga.attribute_id=att.attribute_id
        left join attribute_category as atc on atc.attribute_category_id=att.attribute_category_id
        where not(att.attribute_id is null)

        ';

        $db = CDatabase::instance();
        return $db->query($q);
    }

    public static function get_min_price($options) {
        $options['limit_start'] = null;
        $options['limit_end'] = null;
        $options['visibility'] = 'none';
        $q = self::get_query($options);
        $q = "select min(channel_sell_Price) as min_price from (" . $q . ") as t";
        $min_price = cdbutils::get_value($q);
        return $min_price;
    }

    public static function get_max_price($options) {
        $options['limit_start'] = null;
        $options['limit_end'] = null;
        $options['visibility'] = 'none';
        $q = self::get_query($options);
        $q = "select max(channel_sell_Price) as max_price from (" . $q . ") as t";
        $max_price = cdbutils::get_value($q);
        return $max_price;
    }

    public static function get_result($options) {
        $q = self::get_query($options);
        $db = CDatabase::instance();
        return $db->query($q);
    }

    public static function __get_query($options) {
        //$list_product_id = carr::get($options,'product_id');
        //
        $member = member::get_data_member();
        $attributes = carr::get($options, 'attributes');
        $min_price = carr::get($options, 'min_price');
        $max_price = carr::get($options, 'max_price');
        $arr_price = carr::get($options, 'arr_price', array());
        $visibility = carr::get($options, 'visibility');
        $product_type = carr::get($options, 'product_type');
        $search = carr::get($options, 'search');
        $deal_type = carr::get($options, 'deal_type');
        $product_category_lft = carr::get($options, 'product_category_lft');
        $product_category_rgt = carr::get($options, 'product_category_rgt');
        $limit_start = carr::get($options, 'limit_start');
        $limit_end = carr::get($options, 'limit_end');
        $product_category_id = carr::get($options, 'product_category_id');
        $available = carr::get($options, 'available');
        $sortby = carr::get($options, 'sortby');
        $non_configurable = carr::get($options, 'non_configurable');
        $stock = carr::get($options, 'stock');
        //prelove
        $prelove_last_deal = carr::get($options, 'prelove_last_deal');
        $prelove_upcoming_deal = carr::get($options, 'prelove_upcoming_deal');
        $prelove_end_deal = carr::get($options, 'prelove_end_deal');
        $prelove_new = carr::get($options, 'prelove_new');
        $prelove_used = carr::get($options, 'prelove_used');
        $db = CDatabase::instance();
        if ($product_category_id != null) {
            $row = cdbutils::get_row('select lft,rgt from product_category where status>0 and product_category_id=' . $db->escape($product_category_id));
            if ($row != null) {
                $product_category_lft = $row->lft;
                $product_category_rgt = $row->rgt;
            }
        }

        if (strlen($visibility) == 0) {
            $visibility = 'catalog';
            if (strlen($search) > 0) {
                $visibility = 'search';
            }
        }

        if ($visibility == 'none') {
            $visibility = null;
        }





        //$list_product_id, $arr_attribute, $range_filter_price = array(), $arr_filter_product = array(),
        //$visibility = 'search_catalog', $filter_page = '',
        //$filter_name = '', $filter_category_lft = '', $filter_category_rgt = ''

        $channel_sell_price_col = 'channel_sell_price';
        $channel_updown_col = 'channel_updown';
        $channel_profit_col = 'channel_profit';

        if ($member) {
            $channel_sell_price_col = 'channel_sell_price_member';
            $channel_updown_col = 'channel_updown_member';
            $channel_profit_col = 'channel_profit_member';
        }

        $percent_org = "0.9";
        $org_id = CF::org_id();
        $org = org::get_org($org_id);
        $org_parent_id = $org_id;
        $org_product_parent_id = $org_id;
        if ($org->parent_id != null && $org->parent_id > 0) {
            $org_parent_id = $org->parent_id;
            if (ccfg::get('have_parent_setting_price')) {
                $org_parent_id = $org->parent_id;
            } elseif (ccfg::get('have_org_first_parent') > 0) {
                $org_first_parent = org::get_first_parent_org($org_id);
                $org_parent_id = cobj::get($org_first_parent, 'org_id');
                $org_product_parent_id = cobj::get($org_first_parent, 'org_id');
                //cdbg::var_dump($org_first_parent);
            }
        }
        if (strtolower($org->org_category) == 'express') {
            $percent_org = "0.6";
        }


        $q_pricing = "select
                        p.product_id
                        ,p.name as product_name
                        ,p.purchase_price_nta as purchase_price_nta
                        ,p.purchase_price as purchase_price
                        ,p.sell_price as vendor_sell_price
                        ,p.sell_price_nta as vendor_nta
                        ,p.sell_price-p.sell_price_nta as vendor_commission_value
                        ,p.ho_upselling as upselling
                        ,p.sell_price+p.ho_upselling as ho_sell_price
                        ,(p.sell_price-p.sell_price_nta)+p.ho_upselling as channel_commission_full
                        , p.ho_upselling as channel_commision_ho
                        ,case when value is null then 0 else case when pr.type = 'percent' then ((p.sell_price+p.ho_upselling)*pr.value/100) else pr.value end end as promo_total
                        , ((p.sell_price-p.sell_price_nta)+p.ho_upselling) - ((p.ho_upselling)+(case when value is null then 0 else case when pr.type = 'percent' then ((p.sell_price+p.ho_upselling)*pr.value/100) else pr.value end end))  as channel_commission_share
                        , (1-p.percent_share) * ((p.sell_price-p.sell_price_nta)+p.ho_upselling) - ((p.ho_upselling)+(case when value is null then 0 else case when pr.type = 'percent' then ((p.sell_price+p.ho_upselling)*pr.value/100) else pr.value end end)) as channel_commission_share_ho
                        , (p.percent_share) * ((p.sell_price-p.sell_price_nta)+p.ho_upselling) - ((p.ho_upselling)+(case when value is null then 0 else case when pr.type = 'percent' then ((p.sell_price+p.ho_upselling)*pr.value/100) else pr.value end end)) as channel_commission_value
                        ,case when pp.up-pp.down is null then 0 else pp.up-pp.down end as channel_updown
                        ,(p.sell_price+p.ho_upselling) - (case when value is null then 0 else case when pr.type = 'percent' then ((p.sell_price+p.ho_upselling)*pr.value/100) else pr.value end end) +(case when pp.up-pp.down is null then 0 else pp.up-pp.down end) as channel_sell_price
                        , ((p.percent_share) * ((p.sell_price-p.sell_price_nta)+p.ho_upselling) - ((p.ho_upselling)+(case when value is null then 0 else case when pr.type = 'percent' then ((p.sell_price+p.ho_upselling)*pr.value/100) else pr.value end end))) + (case when pp.up-pp.down is null then 0 else pp.up-pp.down end) as channel_profit
                        ,case when ((pp.up_member-pp.down_member is null)or(pp.up_member-pp.down_member = 0)) then case when pp.up-pp.down is null then 0 else pp.up-pp.down end else pp.up_member-pp.down_member end as channel_updown_member
                        ,(p.sell_price+p.ho_upselling) - (case when value is null then 0 else case when pr.type = 'percent' then ((p.sell_price+p.ho_upselling)*pr.value/100) else pr.value end end) +(case when ((pp.up_member-pp.down_member is null)or(pp.up_member-pp.down_member = 0)) then case when pp.up-pp.down is null then 0 else pp.up-pp.down end else pp.up_member-pp.down_member end) as channel_sell_price_member
                        , ((p.percent_share) * ((p.sell_price-p.sell_price_nta)+p.ho_upselling) - ((p.ho_upselling)+(case when value is null then 0 else case when pr.type = 'percent' then ((p.sell_price+p.ho_upselling)*pr.value/100) else pr.value end end))) + (case when ((pp.up_member-pp.down_member is null)or(pp.up_member-pp.down_member = 0)) then case when pp.up-pp.down is null then 0 else pp.up-pp.down end else pp.up_member-pp.down_member end) as channel_profit_member

                    from (select " . $percent_org . " as percent_share,ppp.* from product as ppp where ppp.status>0)as p
                    left join (
                        select pg.priority,pgd.product_id,pgd.type,pgd.value
                        from
                            promo_group_detail as pgd
                            left join promo_group as pg on pgd.promo_group_id=pg.promo_group_id
                        where
                                pg.status>0
                                and pgd.status>0
                                and pg.is_active>0
                                and pg.status_confirm='confirmed'
                                and date(pg.date_start)<=" . $db->escape(date('Y-m-d')) . "
                                and date(pg.date_end)>=" . $db->escape(date('Y-m-d')) . "
                        group by
                            pgd.product_id
                        having pg.priority=min(pg.priority)

                    ) as pr on pr.product_id=p.product_id
                    left join (select * from product_price as pp where  ( (not(pp.org_id is null) and pp.org_id=" . $db->escape($org_parent_id) . ") or pp.org_id is null )) as pp on pp.product_id=p.product_id

                    ";

        $q = 'select
                distinct case when p.parent_id is null then p.product_id else case when ((p.visibility="catalog_search") or(p.visibility="' . $visibility . '")) then p.product_id else p.parent_id end end as parent_product_id
                ,p.product_id
                ,p.code as product_code
                ,p.image_name
                ,p.product_data_type
                ,p.name
                ,p.sku
                ,p.url_key
                ,p.is_hot_item
                ,p.new_product_date
                ,p.weight
                ,p.stock
                ,p.quota
                ,p.tax
                ,p.show_minimum_stock
                ,p.is_available
                ,p.purchase_price_nta
                ,p.purchase_price
                ,p.sell_price_start
                ,p.sell_price_end
                ,pr.vendor_sell_price
                ,pr.vendor_nta
                ,pr.vendor_commission_value
                ,pr.upselling
                ,pr.ho_sell_price
                ,pr.channel_commission_full
                ,pr.channel_commision_ho
                ,pr.promo_total
                ,pr.channel_commission_share
                ,pr.channel_commission_share_ho
                ,pr.channel_commission_value
                ,pr.' . $channel_updown_col . ' as channel_updown
                ,pr.' . $channel_sell_price_col . ' as channel_sell_price
                ,pr.' . $channel_profit_col . ' as channel_profit
                ,case when round(((pr.ho_sell_price - pr.' . $channel_sell_price_col . ') / pr.ho_sell_price ) * 100) = 0 then round(((pr.ho_sell_price - pr.' . $channel_sell_price_col . ') / pr.ho_sell_price ) * 100,2) else round(((pr.ho_sell_price - pr.' . $channel_sell_price_col . ') / pr.ho_sell_price ) * 100) end as promo_text
                ,p.file_path
                from product as p

        ';
        $c = 0;
        $db = CDatabase::instance();
        $add_where = '';
        $q.=' inner join (' . $q_pricing . ') as pr on pr.product_id=p.product_id';

        if ($min_price != null) {
            $add_where .= ' and pr.' . $channel_sell_price_col . '>=' . $db->escape($min_price) . ' ';
        }
        if ($max_price != null) {
            $add_where .= ' and pr.' . $channel_sell_price_col . '<=' . $db->escape($max_price) . ' ';
        }

        //lapakbangunan filter price checkbox
        if (count($arr_price) > 0) {
            $add_where .= ' AND (';
            $txt_or = '';
            foreach ($arr_price as $key => $value) {
                if ($key > 0) {
                    $txt_or = ' OR ';
                }
                $xprice = explode('-', $value);
                $price1 = carr::get($xprice, 0);
                $price2 = carr::get($xprice, 1);
                if ($price1 == '~') {
                    $add_where .= $txt_or . 'pr.' . $channel_sell_price_col . ' <= ' . $db->escape($price2);
                } else if ($price2 == '~') {
                    $add_where .= $txt_or . 'pr.' . $channel_sell_price_col . ' >= ' . $db->escape($price1);
                } else {
                    $add_where .= $txt_or . '(pr.' . $channel_sell_price_col . ' BETWEEN ' . $db->escape($price1) . ' AND ' . $db->escape($price2) . ') ';
                }
            }
            $add_where .= ') ';
        }

        $q.=' left join product as pp on pp.product_id=p.parent_id';
        $q.=' left join vendor as v on v.vendor_id=p.vendor_id';
        //$q.=' left join product as ps on ps.parent_id=p.product_id';
        $q.=' left join product_category as pc on pc.product_category_id=p.product_category_id';
        $q.=' left join product_type as pt on pt.product_type_id=p.product_type_id';

        if (strlen($deal_type) > 0) {
            if ($deal_type != 'mpu') {
                $merchant_category=cobj::get($org,'merchant_category');
                $q_deal = '
                    select
                        cd.cms_deal_id as cms_deal_id,
                        cdd.product_id as product_id,
                        cdd.priority
                    from
                        cms_deal_setting as cds
                        inner join cms_deal as cd on cd.cms_deal_setting_id=cds.cms_deal_setting_id
                        inner join cms_deal_detail as cdd on cdd.cms_deal_id=cd.cms_deal_id
                        inner join product as p on p.product_id=cdd.product_id
                    where
                        cdd.status>0
                        and cd.status>0
                        and cd.is_active>0
                        and p.is_active>0
                        and p.visibility <> "not_visibility_individually"
                        and p.status>0
                        and p.status_confirm = "CONFIRMED"
                        and cds.code = ' . $db->escape($deal_type) . '
                        and cds.merchant_category= '.$db->escape($merchant_category).'
                ';
                $q_deal_org = $q_deal . "
                        and cd.org_id =" . $db->escape($org_product_parent_id) . "
                ";
                $r_org = cdbutils::get_row($q_deal_org);
                $q_deal.="
                        and cd.org_id is null
                ";
                if ($r_org != null) {
                    $q_deal = $q_deal_org;
                }
                $q .= '
                    inner join ( ' . $q_deal . ' ) as cd on cd.product_id=p.product_id

                ';
            }
        }

        if ($attributes != null && is_array($attributes)) {
            foreach ($attributes as $cat_id => $att_ids) {
                $c++;
                $q.='
                        left join product_group_attribute as pga' . $c . ' on pga' . $c . '.product_id=p.product_id and pga' . $c . '.attribute_category_id=' . $db->escape($cat_id);
                if (count($att_ids) > 0) {

                    $att_id_in = implode(',', $att_ids);
                    $add_where .='and ((pga' . $c . '.attribute_id in (' . ($att_id_in) . ')';
                }

                //$add_where .='and pga'.$c.'.attribute_id='.$db->escape($att_id).'';
                $add_where .=' and (case when pga' . $c . '.status is null then 1 else pga' . $c . '.status end) >0 )';
                $add_where .=' or (p.product_id in (select parent_id from product where status>0 and product_id in (select product_id from product_group_attribute where attribute_id in (' . ($att_id_in) . '))) ))';
            }
        }
        $q.=' where p.status>0 and p.is_active>0 and (v.org_id is null or v.org_id=' . $db->escape($org_parent_id) . ') and p.status_confirm="confirmed" ' . $add_where;
        $q.=" and (case when p.parent_id is null then 1=1 else case when ((p.visibility='catalog_search') or(p.visibility='" . $visibility . "')) then 1=1 else pp.status_confirm='CONFIRMED' and pp.status>0 end end)";
        //$q.=" and (case when (ps.product_id is null and p.product_data_type='configurable') then 0=1 else case when (p.parent_id is null and p.product_data_type='simple') then 1=1 else ps.status>0 end end)";
        //$q.=" and (p.visibility <> 'not_visibility_individually')";
        if (strlen($non_configurable) > 0) {
            $q.=" and p.product_data_type='simple' ";
        } 
        if (strlen($stock) > 0 && $stock > 0) {
            $q.=" and p.stock>0";
        }
        if (strlen($visibility) > 0) {
            $q.=" and ((p.visibility='catalog_search') or(p.visibility='" . $visibility . "'))";
        }
        if (strlen($product_type) > 0) {
            $q.=" and pt.name=" . $db->escape($product_type);
        }

        if (CF::domain() == 'shop.cucigu.com') {
            if (strlen($search) > 0) {
                $q.=" and (";
                $q.="
                    MATCH (p.name) AGAINST ('>(\"" . $search . "\") (" . $search . ")' IN BOOLEAN MODE) or
                    p.product_tag like '%" . $db->escape_like($search) . "%'
                ";
                $q.="  )";
            }
        } else {
            if (strlen($search) > 0) {
                if (strpos($search, '#') !== false) {
                    $q.=" and (";
                    $q.="
                        MATCH (p.name) AGAINST ('>(\"" . $search . "\") (" . $search . ")' IN BOOLEAN MODE) or
                        p.product_tag like '%" . $db->escape_like($search) . "%'
                    ";
                    $q.="  )";
                } else {
                    $q.=" and (";
                    $q.="
                        MATCH (p.name) AGAINST ('>(\"" . $search . "\") (" . $search . ")' IN BOOLEAN MODE) or
                        p.product_tag like '%" . $db->escape_like($search) . "%'
                    ";
                    $q.="  )";
                }
            }
        }
        $q.=" and pc.status>0 ";
        if (strlen($product_category_lft) > 0) {
            $q.=" and pc.lft>=" . $product_category_lft . "";
        }
        if (strlen($product_category_rgt) > 0) {
            $q.=" and pc.rgt<=" . $product_category_rgt . "";
        }
        if (strlen($available) > 0) {
            $q.=" and p.is_available=" . $available . "";
        }

        if (ccfg::get('hide_nostock')) {
            $q.=" and (p.stock>0 or p.is_package_product=1)";
        }
        if (strlen($org_parent_id) > 0) {
            $q.=" and (v.org_id is null or v.org_id=" . $db->escape($org_parent_id) . ")";
        }
        $q_deals = array();
        if (strlen($prelove_last_deal) > 0) {
            $q_deals[] = " p.end_bid_date > " . $db->escape(date("Y-m-d H:i:s")) . " AND "
                    . " p.start_bid_date <= " . $db->escape(date("Y-m-d H:i:s")) . " AND p.already_buy = '0'";
        }
        if (strlen($prelove_upcoming_deal) > 0) {
            $q_deals[] = " p.start_bid_date > " . $db->escape(date("Y-m-d H:i:s")) . " ";
        }
        if (strlen($prelove_end_deal) > 0) {
            $q_deals[] = " (p.end_bid_date <= " . $db->escape(date("Y-m-d H:i:s")) . " OR p.already_buy = '1' )";
        }
        $q_prod_conditions = array();
        if (strlen($prelove_new) > 0) {
            $q_prod_conditions[] = 'p.condition = ' . $db->escape($prelove_new);
        }
        if (strlen($prelove_used) > 0) {
            $q_prod_conditions[] = 'p.condition = ' . $db->escape($prelove_used);
        }
        
        org::concat_query('p', $q, $org_id);        
        
        if (count($q_deals) > 0) {
            $q.= "
                and (" . implode(' or ', $q_deals) . ")
            ";
        }

        if (count($q_prod_conditions) > 0) {
            $q.= "
                and (" . implode(' or ', $q_prod_conditions) . ")
            ";
        }

        $q_order = "
                ((pr.ho_sell_price - pr." . $channel_sell_price_col . ") / pr.ho_sell_price ) * 100 desc,p.updated desc
        ";

        if (strlen($sortby) > 0) {
            switch ($sortby) {
                case 'countdown': {
                        $q_order = "
                            p.stock asc
                    ";
                        break;
                    }
                case 'is_new': {
                        $q_order = "
                            p.updated desc
                    ";
                        break;
                    }
                case 'is_updated': {
                        $q_order = "
                        order by
                            p.created desc
                    ";
                        break;
                    }
                case 'popular': {
                        $q_order = "
                            p.is_hot_item desc,p.updated desc
                    ";
                        break;
                    }
                case 'diskon': {
                        $q_order = "
                            ((pr.ho_sell_price - pr." . $channel_sell_price_col . ") / pr.ho_sell_price ) * 100 desc,p.updated desc
                    ";
                        break;
                    }
                case 'min_price': {
                        $q_order = "
                            pr." . $channel_sell_price_col . " asc
                    ";
                        break;
                    }
                case 'max_price': {
                        $q_order = "
                            pr." . $channel_sell_price_col . " desc
                    ";
                        break;
                    }
                case 'A-Z': {
                        $q_order = "
                            p.name asc,p.updated desc
                    ";
                        break;
                    }
                case 'Z-A': {
                        $q_order = "
                            p.name desc,p.updated desc
                    ";
                        break;
                    }
                case 'priority': {
                        $q_order = "
                            cd.priority asc
                    ";
                        break;
                }
            }
        }else{
            //if sortby null and deal type not null
            if(strlen($deal_type)){
                if ($deal_type != 'mpu') {    
                    $q_order = "
                            cd.priority asc
                    ";
                }
            }
        }

        if (strlen($search) > 0) {
            if (strpos($search, '#') !== false) {
                $additional_order = " 
                    p.product_tag like '%" . $db->escape_like($search) . "%' DESC,
                    MATCH (p.name) AGAINST ('>(\"" . $search . "\") (" . $search . ")' IN BOOLEAN MODE) DESC
                ";
                if(strlen($q_order)>0) {
                    $additional_order.= ",";
                }
                $q_order = $additional_order.$q_order;
            } else {
                $additional_order = " MATCH (p.name) AGAINST ('>(\"" . $search . "\") (" . $search . ")' IN BOOLEAN MODE) DESC ";
                if(strlen($q_order)>0) {
                    $additional_order.= ",";
                }
                $q_order = $additional_order.$q_order;
            }
        }

        if(strlen($q_order)>0) {
            $q_order_deal = '';
            if (strlen($deal_type) > 0 && ($deal_type != 'mpu')) {
                $q_order_deal = "
                    cd.priority asc
                ";
                if (strlen($q_order) > 0) {
                    $q_order_deal .= ",";
                }
                $q_order = $q_order_deal.$q_order;
            }
            $q.=" order by ".$q_order;
        }

        if (strlen($limit_start) > 0 && strlen($limit_end) > 0) {
            $end = ($limit_end - $limit_start) + 1;
            if ($end <= 0) {
                $q.= "limit 0";
            } else {
                $q.= " limit " . $limit_start . "," . $end . " ";
            }
        }

//        cdbg::var_dump($q);
//        die;
        if (isset($_GET['debug'])) {
            die($q);
        }
        return $q;
    }
    
    public static function get_query($options) {
        
        $member = member::get_data_member();
        $attributes = carr::get($options, 'attributes');
        $min_price = carr::get($options, 'min_price');
        $max_price = carr::get($options, 'max_price');
        $visibility = carr::get($options, 'visibility');
        $product_type = carr::get($options, 'product_type');
        $search = carr::get($options, 'search');
        $deal_type = carr::get($options, 'deal_type');
        $product_category_lft = carr::get($options, 'product_category_lft');
        $product_category_rgt = carr::get($options, 'product_category_rgt');
        $limit_start = carr::get($options, 'limit_start');
        $limit_end = carr::get($options, 'limit_end');
        $product_category_id = carr::get($options, 'product_category_id');
        $available = carr::get($options, 'available');
        $sortby = carr::get($options, 'sortby');
        $non_configurable = carr::get($options, 'non_configurable');
        $stock = carr::get($options, 'stock');
        $org_shared_product=null;
        
        $db = CDatabase::instance();
        if ($product_category_id != null) {
            $row = cdbutils::get_row('select lft,rgt from product_category where status>0 and product_category_id=' . $db->escape($product_category_id));
            if ($row != null) {
                $product_category_lft = $row->lft;
                $product_category_rgt = $row->rgt;
            }
        }

        if (strlen($visibility) == 0) {
            $visibility = 'catalog';
            if (strlen($search) > 0) {
                $visibility = 'search';
            }
        }

        if ($visibility == 'none') {
            $visibility = null;
        }

        $channel_sell_price_col = 'channel_sell_price';
        $channel_updown_col = 'channel_updown';
        $channel_profit_col = 'channel_profit';

        if ($member) {
            $channel_sell_price_col = 'channel_sell_price_member';
            $channel_updown_col = 'channel_updown_member';
            $channel_profit_col = 'channel_profit_member';
        }

        $percent_org = "0.9";
        $org_id = isset($options['org_id']) ? $options['org_id'] :  CF::org_id();
        $org = org::get_org($org_id);
        $org_parent_id = $org_id;
        $org_product_parent_id = $org_id;
        if ($org->parent_id != null && $org->parent_id > 0) {
            $org_parent_id = $org->parent_id;
            if (ccfg::get('have_parent_setting_price')) {
                $org_parent_id = $org->parent_id;
            } elseif (ccfg::get('have_org_first_parent') > 0) {
                $org_first_parent = org::get_first_parent_org($org_id);
                $org_parent_id = cobj::get($org_first_parent, 'org_id');
                $org_product_parent_id = cobj::get($org_first_parent, 'org_id');
            }
        }
        
        /*
         * org_shared_product a/ org yang bisa share productnya
         * untuk saat ini org_shared_product = org_parent_id karna semua downline parentnya langsung ke compromall
         * kedepan ada kemungkinan org shared product != org parent satu tingkat diatasnya, melainkan org_share_product = org nya compromall
         */
        $org_shared_product = $org_parent_id;
        if (strtolower($org->org_category) == 'express') {
            $percent_org = "0.6";
        }


        $q_pricing = "select
                        p.product_id
                        ,p.name as product_name
                        ,p.purchase_price_nta as purchase_price_nta
                        ,p.purchase_price as purchase_price
                        ,p.sell_price as vendor_sell_price
                        ,p.sell_price_nta as vendor_nta
                        ,p.sell_price-p.sell_price_nta as vendor_commission_value
                        ,p.ho_upselling as upselling
                        ,p.sell_price+p.ho_upselling as ho_sell_price
                        ,(p.sell_price-p.sell_price_nta)+p.ho_upselling as channel_commission_full
                        , p.ho_upselling as channel_commision_ho
                        ,case when value is null then 0 else case when pr.type = 'percent' then ((p.sell_price+p.ho_upselling)*pr.value/100) else pr.value end end as promo_total
                        , ((p.sell_price-p.sell_price_nta)+p.ho_upselling) - ((p.ho_upselling)+(case when value is null then 0 else case when pr.type = 'percent' then ((p.sell_price+p.ho_upselling)*pr.value/100) else pr.value end end))  as channel_commission_share
                        , (1-p.percent_share) * ((p.sell_price-p.sell_price_nta)+p.ho_upselling) - ((p.ho_upselling)+(case when value is null then 0 else case when pr.type = 'percent' then ((p.sell_price+p.ho_upselling)*pr.value/100) else pr.value end end)) as channel_commission_share_ho
                        , (p.percent_share) * ((p.sell_price-p.sell_price_nta)+p.ho_upselling) - ((p.ho_upselling)+(case when value is null then 0 else case when pr.type = 'percent' then ((p.sell_price+p.ho_upselling)*pr.value/100) else pr.value end end)) as channel_commission_value
                        ,case when pp.up-pp.down is null then 0 else pp.up-pp.down end as channel_updown
                        ,(p.sell_price+p.ho_upselling) - (case when value is null then 0 else case when pr.type = 'percent' then ((p.sell_price+p.ho_upselling)*pr.value/100) else pr.value end end) +(case when pp.up-pp.down is null then 0 else pp.up-pp.down end) as channel_sell_price
                        , ((p.percent_share) * ((p.sell_price-p.sell_price_nta)+p.ho_upselling) - ((p.ho_upselling)+(case when value is null then 0 else case when pr.type = 'percent' then ((p.sell_price+p.ho_upselling)*pr.value/100) else pr.value end end))) + (case when pp.up-pp.down is null then 0 else pp.up-pp.down end) as channel_profit
                        ,case when ((pp.up_member-pp.down_member is null)or(pp.up_member-pp.down_member = 0)) then case when pp.up-pp.down is null then 0 else pp.up-pp.down end else pp.up_member-pp.down_member end as channel_updown_member
                        ,(p.sell_price+p.ho_upselling) - (case when value is null then 0 else case when pr.type = 'percent' then ((p.sell_price+p.ho_upselling)*pr.value/100) else pr.value end end) +(case when ((pp.up_member-pp.down_member is null)or(pp.up_member-pp.down_member = 0)) then case when pp.up-pp.down is null then 0 else pp.up-pp.down end else pp.up_member-pp.down_member end) as channel_sell_price_member
                        , ((p.percent_share) * ((p.sell_price-p.sell_price_nta)+p.ho_upselling) - ((p.ho_upselling)+(case when value is null then 0 else case when pr.type = 'percent' then ((p.sell_price+p.ho_upselling)*pr.value/100) else pr.value end end))) + (case when ((pp.up_member-pp.down_member is null)or(pp.up_member-pp.down_member = 0)) then case when pp.up-pp.down is null then 0 else pp.up-pp.down end else pp.up_member-pp.down_member end) as channel_profit_member
                        ,pp.org_id as pp_org_id
                        ,pp.product_id as pp_product_id
                        
                    from (select " . $percent_org . " as percent_share,ppp.* from product as ppp where ppp.status>0)as p
                    left join (
                        select pg.priority,pgd.product_id,pgd.type,pgd.value
                        from
                            promo_group_detail as pgd
                            left join promo_group as pg on pgd.promo_group_id=pg.promo_group_id
                        where
                            pg.status>0
                            and pgd.status>0
                            and pg.is_active>0
                            and pg.status_confirm='confirmed'
                            and date(pg.date_start)<=" . $db->escape(date('Y-m-d')) . "
                            and date(pg.date_end)>=" . $db->escape(date('Y-m-d')) . "
                        group by
                            pgd.product_id
                        having pg.priority=min(pg.priority)

                    ) as pr on pr.product_id=p.product_id
                    left join (select * from product_price as pp where  ( (not(pp.org_id is null) and pp.org_id=" . $db->escape($org_parent_id) . ") or pp.org_id is null or pp.org_id=".$db->escape($org_id)." ) and pp.status=1 ) as pp on pp.product_id=p.product_id 
                    ";

        $q = 'select
                distinct case when p.parent_id is null then p.product_id else case when ((p.visibility="catalog_search") or(p.visibility="' . $visibility . '")) then p.product_id else p.parent_id end end as parent_product_id
                ,p.org_id 
                ,p.product_id
                ,p.code as product_code
                ,p.image_name
                ,p.product_data_type
                ,p.name
                ,p.sku
                ,p.url_key
                ,p.is_hot_item
                ,p.new_product_date
                ,p.weight
                ,p.stock
                ,p.quota
                ,p.tax
                ,p.show_minimum_stock
                ,p.is_available
                ,p.purchase_price_nta
                ,p.purchase_price
                ,p.sell_price_start
                ,p.sell_price_end
                ,pr.vendor_sell_price
                ,pr.vendor_nta
                ,pr.vendor_commission_value
                ,pr.upselling
                ,pr.ho_sell_price
                ,pr.channel_commission_full
                ,pr.channel_commision_ho
                ,pr.promo_total
                ,pr.channel_commission_share
                ,pr.channel_commission_share_ho
                ,pr.channel_commission_value
                ,pr.' . $channel_updown_col . ' as channel_updown
                ,pr.' . $channel_sell_price_col . ' as channel_sell_price
                ,pr.' . $channel_profit_col . ' as channel_profit
                ,case when round(((pr.ho_sell_price - pr.' . $channel_sell_price_col . ') / pr.ho_sell_price ) * 100) = 0 then round(((pr.ho_sell_price - pr.' . $channel_sell_price_col . ') / pr.ho_sell_price ) * 100,2) else round(((pr.ho_sell_price - pr.' . $channel_sell_price_col . ') / pr.ho_sell_price ) * 100) end as promo_text
                ,p.file_path 
                ,pr.pp_org_id
                ,pr.pp_product_id 
                from product as p 
        ';
        $c = 0;
        $db = CDatabase::instance();
        $add_where = '';
        $q.=' inner join (' . $q_pricing . ') as pr on pr.product_id=p.product_id';

        if ($min_price != null) {
            $add_where .= ' and pr.' . $channel_sell_price_col . '>=' . $db->escape($min_price) . ' ';
        }
        if ($max_price != null) {
            $add_where .= ' and pr.' . $channel_sell_price_col . '<=' . $db->escape($max_price) . ' ';
        }

        $q.=' left join product as pp on pp.product_id=p.parent_id';
        $q.=' left join vendor as v on v.vendor_id=p.vendor_id';
        $q.=' left join product_category as pc on pc.product_category_id=p.product_category_id';
        $q.=' left join product_type as pt on pt.product_type_id=p.product_type_id';

        if (strlen($deal_type) > 0) {
            if ($deal_type != 'mpu') {
                $merchant_category=cobj::get($org,'merchant_category');
                $q_deal = '
                    select
                        cd.cms_deal_id as cms_deal_id,
                        cdd.product_id as product_id,
                        cdd.priority
                    from
                        cms_deal_setting as cds
                        inner join cms_deal as cd on cd.cms_deal_setting_id=cds.cms_deal_setting_id
                        inner join cms_deal_detail as cdd on cdd.cms_deal_id=cd.cms_deal_id
                        inner join product as p on p.product_id=cdd.product_id
                    where
                        cdd.status>0
                        and cd.status>0
                        and cd.is_active>0
                        and p.is_active>0
                        and p.visibility <> "not_visibility_individually"
                        and p.status>0
                        and p.status_confirm = "CONFIRMED"
                        and cds.code = ' . $db->escape($deal_type) . '
                        and cds.merchant_category= '.$db->escape($merchant_category).'
                ';
                $q_deal_org = $q_deal . "
                        and cd.org_id =" . $db->escape($org_product_parent_id) . "
                ";
                $r_org = cdbutils::get_row($q_deal_org);
                $q_deal.="
                        and cd.org_id is null
                ";
                if ($r_org != null) {
                    $q_deal = $q_deal_org;
                }
                $q .= '
                    inner join ( ' . $q_deal . ' ) as cd on cd.product_id=p.product_id

                ';
            }
        }

        if ($attributes != null && is_array($attributes)) {
            foreach ($attributes as $cat_id => $att_ids) {
                $c++;
                $q.='
                        left join product_group_attribute as pga' . $c . ' on pga' . $c . '.product_id=p.product_id and pga' . $c . '.attribute_category_id=' . $db->escape($cat_id);
                if (count($att_ids) > 0) {

                    $att_id_in = implode(',', $att_ids);
                    $add_where .='and ((pga' . $c . '.attribute_id in (' . ($att_id_in) . ')';
                }

                //$add_where .='and pga'.$c.'.attribute_id='.$db->escape($att_id).'';
                $add_where .=' and (case when pga' . $c . '.status is null then 1 else pga' . $c . '.status end) >0 )';
                $add_where .=' or (p.product_id in (select parent_id from product where status>0 and product_id in (select product_id from product_group_attribute where attribute_id in (' . ($att_id_in) . '))) ))';
            }
        }
        $q.=' where p.status>0 and p.is_active>0 and (v.org_id is null or v.org_id=' . $db->escape($org_parent_id) . ' or v.org_id='.$db->escape($org_id).' or v.org_id='.$db->escape($org_shared_product).') and p.status_confirm="confirmed" ' . $add_where;
        $q.=" and (case when p.parent_id is null then 1=1 else case when ((p.visibility='catalog_search') or(p.visibility='" . $visibility . "')) then 1=1 else pp.status_confirm='CONFIRMED' and pp.status>0 end end)";

        if (strlen($non_configurable) > 0) {
            $q.=" and p.product_data_type='simple' ";
        }
        if (strlen($stock) > 0 && $stock > 0) {
            $q.=" and p.stock>0";
        }
        if (strlen($visibility) > 0) {
            $q.=" and ((p.visibility='catalog_search') or(p.visibility='" . $visibility . "'))";
        }
        if (strlen($product_type) > 0) {
            $q.=" and pt.name=" . $db->escape($product_type);
        }
        
        if (strlen($search) > 0) {
            if (strpos($search, '#') !== false) {
                $search_string_before = substr($search, strpos($search, '#') + 1);
                //Replace all non word characters with spaces (I prefer this approach). This can be accomplished with regex:
                $search_string = preg_replace('/[^\p{L}\p{N}_]+/u', ' ', $search_string_before);
                //Replace characters-operators with spaces:
                $search_string = preg_replace('/[+\-><\(\)~*\"@]+/', ' ', $search_string);
                $q.=" and (";
                $q.="
                    MATCH (p.name) AGAINST ('>(\"" . $db->escape_str($search_string) . "\") (" . $db->escape_str($search_string) . ")' IN BOOLEAN MODE) or
                    p.product_tag like '%" . $db->escape_like($search_string_before) . "%'
                ";
                $q.="  )";
            } else {
                $search_string_before = $search;
                //Replace all non word characters with spaces (I prefer this approach). This can be accomplished with regex:
                $search_string = preg_replace('/[^\p{L}\p{N}_]+/u', ' ', $search_string_before);
                //Replace characters-operators with spaces:
                $search_string = preg_replace('/[+\-><\(\)~*\"@]+/', ' ', $search_string);
                $q.=" and (";
                $q.="
                    MATCH (p.name) AGAINST ('>(\"" . $db->escape_str($search_string) . "\") (" . $db->escape_str($search_string) . ")' IN BOOLEAN MODE) or
                    p.product_tag like '%" . $db->escape_like($search_string_before) . "%' or
                    p.name like '%" . $db->escape_like($search_string_before) . "%'
                ";
                $q.="  )";
            }
        }
        $q.=" and pc.status>0 ";
        if (strlen($product_category_lft) > 0) {
            $q.=" and pc.lft>=" . $product_category_lft . "";
        }
        if (strlen($product_category_rgt) > 0) {
            $q.=" and pc.rgt<=" . $product_category_rgt . "";
        }
        if (strlen($available) > 0) {
            $q.=" and p.is_available=" . $available . "";
        }

        if (ccfg::get('hide_nostock')) {
            $q.=" and (p.stock>0 or p.is_package_product=1)";
        }
        if (strlen($org_parent_id) > 0) {
            //$q.=" and (v.org_id is null or v.org_id=" . $db->escape($org_parent_id) . ")";
        }
        $q_deals = array();
        $q_prod_conditions = array();
        
        org::concat_query('p', $q, $org_id);    
        
        if(!empty($org_shared_product)){
            $q .= ' or (p.org_id = '.$db->escape($org_shared_product).' and p.product_id=pr.pp_product_id)  ';
        }
        
        if (count($q_deals) > 0) {
            $q.= "
                and (" . implode(' or ', $q_deals) . ")
            ";
        }

        if (count($q_prod_conditions) > 0) {
            $q.= "
                and (" . implode(' or ', $q_prod_conditions) . ")
            ";
        }

        $q_order = "
                ((pr.ho_sell_price - pr." . $channel_sell_price_col . ") / pr.ho_sell_price ) * 100 desc,p.updated desc
        ";

        if (strlen($sortby) > 0) {
            switch ($sortby) {
                case 'countdown': {
                        $q_order = "
                            p.stock asc
                    ";
                        break;
                    }
                case 'is_new': {
                        $q_order = "
                            p.updated desc
                    ";
                        break;
                    }
                case 'is_updated': {
                        $q_order = "
                        order by
                            p.created desc
                    ";
                        break;
                    }
                case 'popular': {
                        $q_order = "
                            p.is_hot_item desc,p.updated desc
                    ";
                        break;
                    }
                case 'diskon': {
                        $q_order = "
                            ((pr.ho_sell_price - pr." . $channel_sell_price_col . ") / pr.ho_sell_price ) * 100 desc,p.updated desc
                    ";
                        break;
                    }
                case 'min_price': {
                        $q_order = "
                            pr." . $channel_sell_price_col . " asc
                    ";
                        break;
                    }
                case 'max_price': {
                        $q_order = "
                            pr." . $channel_sell_price_col . " desc
                    ";
                        break;
                    }
                case 'A-Z': {
                        $q_order = "
                            p.name asc,p.updated desc
                    ";
                        break;
                    }
                case 'Z-A': {
                        $q_order = "
                            p.name desc,p.updated desc
                    ";
                        break;
                    }
                case 'priority': {
                        $q_order = "
                            cd.priority asc
                    ";
                        break;
                }
            }
        }

        if (strlen($search) > 0) {
            if (strpos($search, '#') !== false) {
                $search_string_before = substr($search, strpos($search, '#') + 1);
                //Replace all non word characters with spaces (I prefer this approach). This can be accomplished with regex:
                $search_string = preg_replace('/[^\p{L}\p{N}_]+/u', ' ', $search_string_before);
                //Replace characters-operators with spaces:
                $search_string = preg_replace('/[+\-><\(\)~*\"@]+/', ' ', $search_string);
                $additional_order = " 
                    p.product_tag like '%" . $db->escape_like($search_string_before) . "%' DESC,
                    MATCH (p.name) AGAINST ('>(\"" . $db->escape_str($search_string) . "\") (" . $db->escape_str($search_string) . ")' IN BOOLEAN MODE) DESC
                ";
                if(strlen($q_order)>0) {
                    $additional_order.= ",";
                }
                $q_order = $additional_order.$q_order;
            } else {
                $search_string_before = $search;
                //Replace all non word characters with spaces (I prefer this approach). This can be accomplished with regex:
                $search_string = preg_replace('/[^\p{L}\p{N}_]+/u', ' ', $search_string_before);
                //Replace characters-operators with spaces:
                $search_string = preg_replace('/[+\-><\(\)~*\"@]+/', ' ', $search_string);
                $additional_order = "
                    MATCH (p.name) AGAINST ('>(\"" . $db->escape_str($search_string) . "\") (" . $db->escape_str($search_string) . ")' IN BOOLEAN MODE) DESC,
                    p.name like '%" . $db->escape_like($search_string_before) . "%'
                ";
                if(strlen($q_order)>0) {
                    $additional_order.= ",";
                }
                $q_order = $additional_order.$q_order;
            }
        }

        if(strlen($q_order)>0) {
            $q_order_deal = '';
            if (strlen($deal_type) > 0 && ($deal_type != 'mpu')) {
                $q_order_deal = "
                    cd.priority asc
                ";
                if (strlen($q_order) > 0) {
                    $q_order_deal .= ",";
                }
                $q_order = $q_order_deal.$q_order;
            }
            $q.=" order by ".$q_order;
        }

        if (strlen($limit_start) > 0 && strlen($limit_end) > 0) {
            $end = ($limit_end - $limit_start) + 1;
            if ($end <= 0) {
                $q.= "limit 0";
            } else {
                $q.= " limit " . $limit_start . "," . $end . " ";
            }
        }

        if (isset($_GET['debug'])) {
            cdbg::var_dump($q);
        }
        
        return $q;
    }

    //untuk API
    public function array_result_product($options) {
        $res = self::get_result($options);
        
        if ($res == null)
        return array();
        $arr_product = array();
        foreach ($res as $row) {
            $product_type = carr::get($options, 'product_type');
            $product_id = cobj::get($row, 'product_id', NULL);
            $product_code = cobj::get($row, 'product_code', NULL);
            $name = cobj::get($row, 'name', NULL);
            $sku = cobj::get($row, 'sku', NULL);
            $url_key = cobj::get($row, 'url_key', NULL);
            $file_path = cobj::get($row, 'file_path', NULL);
            $weight = cobj::get($row, 'weight', NULL);
            $tax = cobj::get($row, 'tax', NULL);
            if ($tax == NULL) {
                $tax = 0;
            }
            $quick_overview = cobj::get($row, 'quick_overview', NULL);
            $overview = cobj::get($row, 'overview', NULL);
            $spesification = cobj::get($row, 'spesification', NULL);
            $faq = cobj::get($row, 'faq', NULL);
            $stock = cobj::get($row, 'stock', NULL);
            $hot = (cobj::get($row, 'is_hot_item', NULL) > 0) ? '1' : '0';
//            if (!empty(cobj::get($row, 'new_product_date', NULL))) {
            $new = '0';
            if (strlen(cobj::get($row, 'new_product_date', NULL)) > 0) {
                if (strtotime(cobj::get($row, 'new_product_date', NULL)) >= strtotime(date('Y-m-d'))) {
                    $new = '1';
                }
            }
            $stock = cobj::get($row, 'stock', NULL);
            $show_minimum_stock = cobj::get($row, 'show_minimum_stock', NULL);
            $is_available = cobj::get($row, 'is_available', NULL);
            $image_name = cobj::get($row, 'image_name', NULL);
            $image_url = cobj::get($row, 'file_path', NULL); //image::get_image_url($image_name,$product_type);


            $product = array();
            $product['product_id'] = $product_id;
            $product['org_id'] = cobj::get($row, 'org_id');
            $product['url_key'] = $url_key;
            $product['product_code'] = $product_code;
            $product['product_name'] = $name;
            $product['product_sku'] = $sku;
            $product['weight'] = $weight;
            $product['image_url'] = $image_url;
            $product['is_available'] = $is_available;
            $product['tax'] = $tax;
            $detail_flag = array();
            $detail_flag["hot"] = $hot;
            $detail_flag["new"] = $new;
            $product['flag'] = $detail_flag;
            $product['quick_overview'] = $quick_overview;
            $product['stock'] = $stock;
            $product['show_minimum_stock'] = $show_minimum_stock;
            $product['overview'] = $overview;
            $product['spesification'] = $spesification;
            $product['faq'] = $faq;
            $detail_price = array();
            $detail_price["sell_price"] = cobj::get($row, 'ho_sell_price', NULL) + $tax;
            $detail_price["sell_price_promo"] = cobj::get($row, 'channel_sell_price', NULL) + $tax;
            $detail_price["promo_text"] = cobj::get($row, 'promo_text', NULL);
            if ($detail_price["promo_text"] == 0.00) {
                $detail_price["promo_text"] = 0;
            }
            $product['detail_price'] = $detail_price;
            $arr_product[] = $product;
        }
        return $arr_product;
    }

    public function get_attribute_all($options) {
        $error = 0;
        $arr = array();
        if (!is_array($options)) {
            $options[] = $options;
        }
        $product_id = carr::get($options, 'product_id');
        if ($product_id == null) {
            $error++;
        }
        if ($error == 0) {
            $val = product::get_attribute_category_all($product_id);
            if (count($val) > 0) {
                $keys = array_keys($val);
                foreach ($val as $k => $v) {
                    $arr_att_category = array();
                    $arr_att_category['attribute_category_id'] = carr::get($v, 'attribute_category_id');
                    $arr_att_category['attribute_category_type'] = carr::get($v, 'attribute_category_type');
                    $arr_att_category['attribute_category_code'] = carr::get($v, 'attribute_category_code');
                    $arr_att_category['attribute_category_url_key'] = carr::get($v, 'attribute_category_url_key');
                    $arr_att_category['attribute_category_name'] = carr::get($v, 'attribute_category_name');
                    $arr['attribute_category_list'][$k] = $arr_att_category;
                }
                $arr['attribute'] = product::generate_sub_attribute($val, 0, $product_id, carr::get($val[$keys[0]], 'attribute_category_id'), array());
            }
        }
        return $arr;
    }

    public function generate_sub_attribute($data_attribute, $depth, $product_id, $attribute_category_id, $arr_attribute_id = array()) {
        $arr = array();
        $temp = product::get_attribute_list($product_id, $attribute_category_id, $arr_attribute_id);
        if (isset($temp['data']))
            $temp = $temp['data'];
        if ($depth >= count($data_attribute))
            return;


        foreach ($temp as $k_temp => $v_temp) {
            $next_arr_attribute_id = array_merge($arr_attribute_id, array('att_cat_' . $attribute_category_id => $k_temp));
            $keys = array_keys($data_attribute);
            $data_category = carr::get($data_attribute, $keys[$depth]);
            //$arr[$keys[$depth]][]=carr::get($v_temp,'attribute_key');
            $attribute_id = carr::get($v_temp, 'attribute_id');
            $arr[$attribute_id] = array();
            $arr[$attribute_id]['depth'] = $depth + 1;
            $arr[$attribute_id]['attribute_category_id'] = $attribute_category_id;
            $arr[$attribute_id]['attribute_category_type'] = carr::get($data_category, 'attribute_category_type');
            $arr[$attribute_id]['attribute_category_code'] = $keys[$depth];
            $arr[$attribute_id]['attribute_category_url_key'] = carr::get($data_category, 'attribute_category_url_key');
            $arr[$attribute_id]['attribute_category_name'] = carr::get($data_category, 'attribute_category_name');
            $arr[$attribute_id]['attribute_id'] = carr::get($v_temp, 'attribute_id');
            $arr[$attribute_id]['attribute_code'] = carr::get($v_temp, 'attribute_code');
            $arr[$attribute_id]['attribute_key'] = carr::get($v_temp, 'attribute_key');
            $arr[$attribute_id]['attribute_url_key'] = carr::get($v_temp, 'attribute_url_key');
            if (isset($keys[$depth + 1])) {
                $next_attribute_category_id = $data_attribute[$keys[$depth + 1]]['attribute_category_id'];

                $sub = product::generate_sub_attribute($data_attribute, $depth + 1, $product_id, $next_attribute_category_id, $next_arr_attribute_id);
                $arr[$attribute_id]['sub_attribute'] = $sub;
            }
            if (!isset($arr[carr::get($v_temp, 'attribute_id')]['sub_attribute'])) {
                $attribute = array();

                foreach ($next_arr_attribute_id as $key_req => $value_req) {
                    if (substr($key_req, 0, 8) == 'att_cat_') {
                        $attribute[substr($key_req, 8)] = $value_req;
                    }
                }
                // get data product
                $product_simple = product::get_simple_product_from_attribute($product_id, $attribute);
                $data_product = product::get_product($product_simple->product_id);

                $product_name = carr::get($data_product, 'name');
                $stock = carr::get($data_product, 'stock', 0);
                $sku = carr::get($data_product, 'sku');
                $price = product::get_price($data_product['detail_price']);
                $arr[$attribute_id]['product_id'] = $product_simple->product_id;
                $arr[$attribute_id]['product_name'] = $product_name;
                $arr[$attribute_id]['price'] = $price;
                $arr[$attribute_id]['sku'] = $sku;
                $arr[$attribute_id]['stock'] = $stock;
            }
        }
        return $arr;
    }

    public function get_vendor($vendor_id) {
        $db = CDatabase::instance();

        $result = array();

        if (strlen($vendor_id) > 0) {
            $r = cdbutils::get_row("SELECT * FROM vendor WHERE status > 0 AND vendor_id=" . $db->escape($vendor_id));
            if ($r != NULL) {
                $result = (array) $r;
            }
        }

        return $result;
    }

    public static function get_product_category_footer() {
        $db = CDatabase::instance();
        $org = CF::org_id();
        $category = array();

        $q = '
                select * 
                from product_category 
                where status > 0 
                    and parent_id is null 
                    and org_id = ' . $db->escape($org) . '
                    order by product_category_id asc
                ';

        $result = $db->query($q);
        if ($result->count() > 0) {
            foreach ($result as $key => $val) {
                $product_category_id = cobj::get($val, 'product_category_id');
                $code = cobj::get($val, 'code');
                $name = cobj::get($val, 'name');
                $url_key = cobj::get($val, 'url_key');
                $desc = cobj::get($val, 'description');
                $new_name = '';

                if (strlen($name) > 24) {
                    $name = substr($name, 0, 24);

                    $name_arr = explode(' ', $name);
                    $end_sentance = end($name_arr);
                    $last_key = key($name_arr);
                    if (strlen($end_sentance) <= 3 && strlen($end_sentance) > 0) {
                        unset($name_arr[$last_key]);
                        foreach ($name_arr as $key => $val) {
                            $new_name.=$val . ' ';
                        }
                        $new_name = rtrim($new_name, ',.;: aA-');
                    }
                }

                if (strlen($new_name) == 0)
                    $new_name = $name;

                $category[] = array(
                    'id' => $product_category_id,
                    'code' => $code,
                    'name' => $new_name,
                    'url_key' => $url_key,
                    'desc' => $desc,
                    'url' => curl::httpbase() . 'product/category/' . $url_key,
                );
            }
        }

        return $category;
    }

    public function recalculate_stock_configurable($parent_id) {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $max_stock = cdbutils::get_value('select MAX(stock) as cnt from product where status > 0 and is_active = 1 and parent_id = ' . $db->escape($parent_id));

        if ($max_stock !== NULL) {
            $db->query('UPDATE product set stock=' . $max_stock . ' where product_id =' . $db->escape($parent_id));
        }
    }

}

