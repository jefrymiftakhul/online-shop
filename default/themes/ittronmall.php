<?php

return array(
    "theme_path" => "ittronmall",
    "checkbox" => "1.0",
    "radio" => "1.0",
//    "select2" => "4",
    "client_modules" => array(
        'bootstrap-3.3.5',
        'jquery',
        'chosen',
        'select2',
//        'select2-4.0',
        'font-awesome',
        'bootstrap-dropdown',
        'bootstrap-slider',
        'slick',
        'jquery.date-dropdowns',
        'zoomsl-3.0',
        'touchspin',
        
//        'flexslider',
//        'fancybox',
//        'jquery.history',
//        'touchswipe',
//        'validation',
//        'owl_carousel'
    ),
    "js" => array(
        "ostore.js",
    ),
    "css" => array(
    ),
    "config" => array(
    ),
);
