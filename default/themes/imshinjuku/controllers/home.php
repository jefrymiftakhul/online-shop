<?php

/**
 * @author: Ittronmall
 * @since:   2017-4-25
 * @filename: home.php
 * @filepath: C:\xampp\htdocs_pipo\application\ittronmall\default\themes\ittronmall-shinjuku\controllers\home.php
 */
class Home_Controller extends ProductController {
    function __construct() {
        parent::__construct();
    }

    public function index() {
        $app = CApp::instance();
        $org_id = CF::org_id();
        $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

        $slide_list_top = slide::get_slide($org_id, $this->page());


        $slide = $app->add_div()->add_class('home-slideshow row');
        $element_slide_show = $slide->add_element('62hall-slide-show', 'home_slideshow');
        $element_slide_show->set_slides($slide_list_top);

        $container = $app->add_div()->add_class('container');

        //category
        $container_page = $container->add_div()->add_class('container-page margin-top-64');
        $container_page_row = $container_page->add_div()->add_class('row');
        $container_page_title = $container_page_row->add_div()->add_class('col-md-12');
        $page_title = $container_page_title->add_div()->add_class('page-title hidden-xs margin-bottom-0 ');
        $page_title->add("OUR SERVICE");
        $page_icon = $page_title->add("<div class='ico-underline'></div>");

        $container_category_image = $container_page_row->add_div()->add_class('col-md-12');
        $category_image = $container_category_image->add_div()->add_class('category-image hidden-xs ');
        
        
        
        $menu_category = product_category::get_product_category_menu('product', '', $org_id);
        
//        $re_menu_category = array();
        foreach ($menu_category as $k => $v) {
            $name_menu_category = strtoupper($v['name']);
            $url_key = $v['url_key'];
            $url_menu = curl::base() . 'search?keyword=&category=';
            $url = $url_menu . $url_key;
            $category_single_image = $category_image->add_div()->add_class('col-lg-4 col-md-4 col-sm-4 col-xs-12 thumb');
            $image = $category_single_image->add("
                    <div id='bg'>
                        <a href='" . $url . "' class='image-category'>
                            <img src='" . $v['image_url'] . "' class='img-category-single-image' >
                            <div id='caption'> 
                                    " . $name_menu_category . "
                            </div>
                       </a>
                    </div>");
//            $all_re_menu_category = $v;
//            $all_re_menu_category['image_path'] = carr::get($v, 'image_url');
//            $re_menu_category[] = $all_re_menu_category;
        }
        
        $category_image_mobile = $container_page_title->add_div()->add_class('category-image visible-xs ');
        $element_category_image_mobile = $category_image_mobile->add_element('shinjuku-deal', 'mobile-category')
                ->set_title('OUR SERVICE')
                ->set_slides($menu_category)
                ->set_type('category')
                ->add_class('slide-deal shinjuku-deal');
        //end category
        //advertising
        $data_advertise = advertise::get_advertise($org_id, $this->page());
        $ads_first = carr::get($data_advertise, 0, array());
        
        $container_adds = $container->add_div()->add_class('container-adds');
        $container_adds_content = $container_adds->add_div()->add_class('row');
        $adds_content = $container_adds_content->add_div()->add_class('col-xs-12 col-sm-12 col-md-12 col-lg-12');

        foreach ($ads_first as $key => $value) {
            $image_url = carr::get($value, 'image_url');
            $url_link = carr::get($value, 'url_link');
            switch ($key) {
                case 0:
                    $adds_first_content = $adds_content->add_div()->add_class('col-lg-6 col-md-6 col-sm-6 col-xs-12');
                    $adds_first_content->add("<a href='" . $url_link . "'><img class='img-responsive first-adv' src='$image_url'></a>");
                    break;
                case 1:
                    $adds_second_content = $adds_content->add_div()->add_class('col-lg-6 col-md-6 col-sm-6 col-xs-12');
                    $adds_second_content->add("<a href='" . $url_link . "'><img class='img-responsive second-adv' src='$image_url'></a>");
                    break;
                case 2:
                    $adds_third_content = $adds_content->add_div()->add_class('col-lg-3 col-md-3 col-sm-3 col-xs-6');
                    $adds_third_content->add("<a href='" . $url_link . "'><img class='img-responsive third-adv' src='$image_url'></a>");
                    break;
                case 3:
                    $adds_forth_content = $adds_content->add_div()->add_class('col-lg-3 col-md-3 col-sm-3 col-xs-6');
                    $adds_forth_content->add("<a href='" . $url_link . "'><img class='img-responsive forth-adv' src='$image_url'></a>");
                    break;

                default:
                    break;
            }
        }
        //end advertising
        
        //deal
        $deal_product = deal::get_deal($org_id, $this->page(), 'deal_new', array('sortby' => 'priority'));
        $deal_hot = deal::get_deal($org_id, $this->page(), 'deal_hot', array('sortby' => 'priority'));
        $deal_best = deal::get_deal($org_id, $this->page(), 'deal_best_sell', array('sortby' => 'priority'));
//        $shinjuku_4 = deal::get_deal($org_id, $this->page(), 'shinjuku_4', array('sortby' => 'priority'));
//        $shinjuku_1_label=deal::get_deal_name($org_id, $this->page(), 'shinjuku_1',$this->_merchant_category);
//        $shinjuku_2_label=deal::get_deal_name($org_id, $this->page(), 'shinjuku_2',$this->_merchant_category);
//        $shinjuku_3_label=deal::get_deal_name($org_id, $this->page(), 'shinjuku_3',$this->_merchant_category);
//        $shinjuku_4_label=deal::get_deal_name($org_id, $this->page(), 'shinjuku_4',$this->_merchant_category);
 
        //Product NEW
        $container_page = $container->add_div()->add_class('container-page margin-top-64');
        $container_page_row = $container_page->add_div()->add_class('row');
        $container_product = $container_page_row->add_div()->add_class('col-md-12');
        $container_deal = $container_product->add_div()->add_class('container-deal');
        $element_deal = $container_deal->add_element('shinjuku-deal', 'deal1')
//                ->set_title(strtoupper($shinjuku_1_label))
                ->set_title(strtoupper('NEW PRODUCT'))
                ->set_slides($deal_product)
                ->add_class('slide-deal shinjuku-deal');
        
        //Product BEST
        $container_page = $container->add_div()->add_class('container-page');
        $container_page_row = $container_page->add_div()->add_class('row');
        $container_product = $container_page_row->add_div()->add_class('col-md-12');
        $container_deal = $container_product->add_div()->add_class('container-deal');
        $element_deal = $container_deal->add_element('shinjuku-deal', 'deal3')
                ->set_title(strtoupper('BEST SELLERS'))
                ->set_slides($deal_best)
                ->add_class('slide-deal shinjuku-deal');

        //Product HOT
        $container_page = $container->add_div()->add_class('container-page');
        $container_page_row = $container_page->add_div()->add_class('row');
        $container_product = $container_page_row->add_div()->add_class('col-md-12');
        $container_deal = $container_product->add_div()->add_class('container-deal');
        $element_deal = $container_deal->add_element('shinjuku-deal', 'deal2')
                ->set_title(strtoupper('HOT DEALS'))
                ->set_slides($deal_hot)
                ->add_class('slide-deal shinjuku-deal');
        
        
        //Product New
//        $container_page = $container->add_div()->add_class('container-page');
//        $container_page_row = $container_page->add_div()->add_class('row');
//        $container_product = $container_page_row->add_div()->add_class('col-md-12');
//        $container_deal = $container_product->add_div()->add_class('container-deal');
//        $element_deal = $container_deal->add_element('shinjuku-deal', 'deal4')
//                ->set_title(strtoupper($shinjuku_4_label))
//                ->set_slides($shinjuku_4)
//                ->add_class('slide-deal shinjuku-deal');     
        $js = "
            jQuery('.collapsed-icon').on('click', function(){
                jQuery('body').find('.main-menu-wrap').toggleClass('mobile-active');
            });
                ";
        $app->add_js($js);

        echo $app->render();
    }

}
