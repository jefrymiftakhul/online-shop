<?php

class Home_Controller extends ProductController {
    private $label_deal_new = 'New Products';
    private $label_deal_best_sell = 'Best Sellers';
    private $label_hot_deal = 'Hot Deals';
    function __construct() {
        parent::__construct();
    }

    public function index() {
        $app = CApp::instance();
        $org_id = CF::org_id();
        $db = CDatabase::instance();
        $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

        $slide_list_top = slide::get_slide($org_id, $this->page());

        $get_code_org = "default";
        if ($org_id) {
            $get_code = cdbutils::get_row("SELECT * FROM org WHERE org_id = " . $db->escape($org_id));
            $get_code_org = $get_code->code;
        }

        // header & kategori produk
        $header = $app->add_div()->add_class('header');
        $logo = $header->add_div()->add_class('header-logo');
        $logo->add_class('text-center');
        $logo->custom_css('padding','5px');
        // LOGO
        $item_image = cdbutils::get_value('select item_image from org where status > 0 and org_id = ' . $db->escape(CF::org_id()));
        $logo_img = curl::base() . 'application/adminittronmall/' . $get_code_org . '/upload/logo/item_image/' . $item_image;
        // END LOGO
        $logo->add('<a href="'.curl::base().'"><img src="'.$logo_img.'" style="height:55px;"></a>');
        $menu = $header->add_div()->add_class('header-menu');
        $product_category = product_category::get_product_category_menu('product', '', $org_id);
        foreach ($product_category as $key => $value) {
            $name = carr::get($value,'name');
            $url_key = carr::get($value,'url_key');
            $menu->add_div()->add_class('header-item')->add_action()->set_label($name)->set_link(curl::base().'search?keyword=&category='.$url_key);
        }
        // deal
        $deal_hot = deal::get_deal($org_id, $this->page(), 'deal_hot', array('sortby' => 'priority'));
        $new_products = deal::get_deal($org_id, $this->page(), 'deal_new', array('sortby' => 'priority'));
        $best_sell = deal::get_deal($org_id, $this->page(), 'deal_best_sell', array('sortby' => 'priority'));
        if(isset($_GET['debug111'])){
            cdbg::var_dump($deal_hot);
            cdbg::var_dump($new_products);
            cdbg::var_dump($best_sell);
            die();
        }
        // slide
//        // slide
//        $slide_list = slide::get_slide($org_id, $this->page());
//        $element_slide_show = $container_slide_menu->add_div()->add_class("container container-slide");
//        $element_slide = $element_slide_show->add_element('62hall-slide-show', 'element_slide_show');
//        $element_slide->set_type('product');
//        $element_slide->set_slides($slide_list);
        $slide = $app->add_div()->add_class('home-slideshow row');
        $element_slide_show = $slide->add_element('62hall-slide-show', 'home_slideshow');
        $element_slide_show->set_slides($slide_list_top);
        
        // data banner
        $data_advertise = advertise::get_advertise($org_id, $this->page());
        
        // banner atas
        $banner_top = carr::get($data_advertise,'0');
        if(isset($_GET['debug'])){
            cdbg::var_dump($data_advertise);
            die();
        }
        $ind = 0;
        
        $container = $app->add_div()->add_class('container');
        $row = $container->add_div()->add_class('row row-banner-top');
        $left = $row->add_div()->add_class('banner-left');
        $right = $row->add_div()->add_class('banner-right');
        if(is_array($banner_top)){
            foreach ($banner_top as $key => $value) {
                $image_url = carr::get($value,'image_url');
                $url_link = carr::get($value,'url_link');
                switch ($ind) {
                    case 0:
                        $banner_wrapper = $left->add_div()->add_class('banner-wrapper');
                        if(strlen($url_link) > 0){
                            $banner_wrapper->add('<a href="'.$url_link.'">');
                        }
                        $banner_wrapper->add_img()->set_src($image_url);
                        if(strlen($url_link) > 0){
                            $banner_wrapper->add('</a>');
                        }
                        break;
                    case 1:
                        $banner_wrapper = $right->add_div()->add_class('banner-wrapper');
                        if(strlen($url_link) > 0){
                            $banner_wrapper->add('<a href="'.$url_link.'">');
                        }
                        $banner_wrapper->add_img()->set_src($image_url);
                        if(strlen($url_link) > 0){
                            $banner_wrapper->add('</a>');
                        }
                        break;
                    case 2:
                        $banner_wrapper = $right->add_div()->add_class('banner-wrapper');
                        if(strlen($url_link) > 0){
                            $banner_wrapper->add('<a href="'.$url_link.'">');
                        }
                        $banner_wrapper->add_img()->set_src($image_url);
                        if(strlen($url_link) > 0){
                            $banner_wrapper->add('</a>');
                        }
                        break;
                }
                $ind++;
            }
        }
        
        $responsive = array();
        $responsive[] = array(
            'breakpoint' => '992',
            'slide_to_show' => '3',
            'slide_to_scroll' => '1',
            'dots' => 'false',
            'arrow' => 'false',
        );
        $responsive[] = array(
            'breakpoint' => '767',
            'slide_to_show' => '2',
            'slide_to_scroll' => '1',
            'dots' => 'false',
            'arrow' => 'false',
        );
        $responsive[] = array(
            'breakpoint' => '319',
            'slide_to_show' => '1',
            'slide_to_scroll' => '1',
            'dots' => 'false',
            'arrow' => 'false',
        );

        // Best Seller
        $deal_container = $container->add_div()->add_class('row container-deal');
        $deal_container->add_element('lastmenit-deal', 'deal-bestseller')
                    ->set_title($this->label_deal_best_sell)
                    ->set_slides($best_sell)
                    ->set_see_all(true)
                    ->set_link_see_all(curl::base().'show_product/show/deal_best_sell')
                    ->set_responsive($responsive)
                    ->add_class('slide-deal imlastmenit-deal');
        
        // New Products
        $deal_container = $container->add_div()->add_class('row container-deal');
        $deal_container->add_element('lastmenit-deal', 'deal-new-products')
                    ->set_title($this->label_deal_new)
                    ->set_slides($new_products)
                    ->set_see_all(true)
                    ->set_link_see_all(curl::base().'show_product/show/deal_new')
                    ->set_responsive($responsive)
                    ->add_class('slide-deal imlastmenit-deal');
        
        
        // banner bawah
        $banner_top = carr::get($data_advertise,'1');
        $ind = 0;
        
        $row = $container->add_div()->add_class('row row-banner-bottom');
        $left = $row->add_div()->add_class('banner-left');
        $left_top = $left->add_div()->add_class('banner-left-top');
        $left_bottom = $left->add_div()->add_class('banner-left-bottom');
        $right = $row->add_div()->add_class('banner-right');
        if(is_array($banner_top)){
            foreach ($banner_top as $key => $value) {
                $image_url = carr::get($value,'image_url');
                $url_link = carr::get($value,'url_link');
                switch ($ind) {
                    case 0:
                        $banner_wrapper = $left_top->add_div()->add_class('banner-wrapper');
                        if(strlen($url_link) > 0){
                            $banner_wrapper->add('<a href="'.$url_link.'">');
                        }
                        $banner_wrapper->add_img()->set_src($image_url);
                        if(strlen($url_link) > 0){
                            $banner_wrapper->add('</a>');
                        }
                        break;
                    case 1:
                        $banner_wrapper = $left_bottom->add_div()->add_class('banner-wrapper');
                        if(strlen($url_link) > 0){
                            $banner_wrapper->add('<a href="'.$url_link.'">');
                        }
                        $banner_wrapper->add_img()->set_src($image_url);
                        if(strlen($url_link) > 0){
                            $banner_wrapper->add('</a>');
                        }
                        break;
                    case 2:
                        $banner_wrapper = $left_bottom->add_div()->add_class('banner-wrapper');
                        if(strlen($url_link) > 0){
                            $banner_wrapper->add('<a href="'.$url_link.'">');
                        }
                        $banner_wrapper->add_img()->set_src($image_url);
                        if(strlen($url_link) > 0){
                            $banner_wrapper->add('</a>');
                        }
                        break;
                    case 3:
                        $banner_wrapper = $right->add_div()->add_class('banner-wrapper');
                        if(strlen($url_link) > 0){
                            $banner_wrapper->add('<a href="'.$url_link.'">');
                        }
                        $banner_wrapper->add_img()->set_src($image_url);
                        if(strlen($url_link) > 0){
                            $banner_wrapper->add('</a>');
                        }
                        break;
                }
                $ind++;
            }
        }
        // Deal Hot
        $deal_container = $container->add_div()->add_class('row container-deal');
        $deal_container->add_element('lastmenit-deal', 'deal-hot')
                    ->set_title($this->label_hot_deal)
                    ->set_slides($deal_hot)
                    ->set_see_all(true)
                    ->set_link_see_all(curl::base().'show_product/show/deal_hot')
                    ->set_responsive($responsive)
                    ->add_class('slide-deal imlastmenit-deal');
        echo $app->render();
    }
}
