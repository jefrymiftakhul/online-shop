<?php

return array(
    "theme_path" => "62hallmall",
    "client_modules" => array(
        'bootstrap-3.3.5',
        'jquery',
        'chosen',
        'select2',
        'font-awesome',
        'bootstrap-dropdown',
        'zoomsl-3.0',
        'fancybox',
        'flexslider',
        'jquery.history',
        'touchspin',
        'touchswipe',
        'bootstrap-slider',
        'validation',
        'owl_carousel132',
        '62hallmall',
        'swiper',
    ),
    "js" => array(
    ),
    "css" => array(
    ),
    "config" => array(
    ),
);
