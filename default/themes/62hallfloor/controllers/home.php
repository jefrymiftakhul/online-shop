<?php

/**
 *
 * @author Riza 
 * @since  Nov 12, 2015
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class Home_Controller extends ProductController {

    // DEAL TYPE
    private $deal_today = "deal_today";
    private $deal_weekly = "deal_weekly";
    private $deal_new_product = "deal_new";
    private $deal_best_selling = "deal_best_sell";
    private $deal_pick_for_you = "pick_for_you";
    // ADVERTISE 
    private $advertise_1 = 0;
    private $advertise_2 = 1;
    private $advertise_3 = 2;
    // PRODUCT CATEGORY
    private $category_product1 = "LINE1";
    private $category_product2 = "LINE2";
    // PRODUCT
    private $item_hot = "Hot";
    private $item_new = "New";

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $manager = CManager::instance();

        $db = CDatabase::instance();
        $app = CApp::instance();
        $user = $app->user();
        $org_id = $this->org_id();
        $menu_list = $this->menu_list();
        $link_list = $this->list_menu();
        $org_first_parent=org::get_first_parent_org($org_id);
        $org_first_parent_id=cobj::get($org_first_parent,'org_id');

        // ADVERTISE
        $data_advertise = advertise::get_advertise($org_id, $this->page());

        // SEARCH 
        // SHOPPING CART 
        $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

// $attribute_category=product::get_attribute_category_all(32);
        // //cdbg::var_dump($attribute_category);
// $attribute_category=product::get_attribute_category_all(36);
        // //cdbg::var_dump($attribute_category);
        // $attribute=product::get_attribute_list(32,3);
        // cdbg::var_dump($attribute);            
        // $attribute_multi=product::get_attribute_category_multi_product(array(32,36));
        // cdbg::var_dump($attribute_multi);            
        $product_category = product_category::get_product_category_menu($this->page());
        // Menu
        $menu_list = array(
            'name' => 'KATEGORI',
            'menu' => $product_category
        );
        // $test=shipping::check_product_available_shipping_city(100,339);
        // cdbg::var_dump($test);


        $container = $app->add_div()->add_class('container-floor');
        $container_slide_menu = $container->add_div()->add_class('container-slide-menu');
        $container_row_slide_menu = $container_slide_menu->add_div()->add_class('row');
        $container_col_slide_menu = $container_row_slide_menu->add_div()->add_class('col-md-12');


        $element_menu = $container_col_slide_menu->add_element('62hall-menu', 'menu-62hall-product');
        $element_menu->set_menu($menu_list)
                ->set_link($link_list)
                ->set_trigger_type('click');

        // slide
        $slide_list = slide::get_slide($org_id, $this->page());

        $element_slide_show = $container_col_slide_menu->add_element('62hall-slide-show', 'element_slide_show');
        $element_slide_show->set_type('product');
        $element_slide_show->set_slides($slide_list);

        // DEAL

        $deal = $container->add_div();
        $deal->add_class('bg-white hidden-sm hidden-xs container-deal');
        $deal_container = $deal->add_div()->add_class('container deal');
        $row_deal = $deal_container->add_div()->add_class('row');

        $deal_border = $row_deal->add_div()->add_class('deal-border col-md-12 margin-top-30');

        // Today Deal
        $deal_border_row = $deal_border->add_div()->add_class('row row-deal row-today-deal');
        $deal_border_col = $deal_border_row->add_div()->add_class('col-md-12');
        $data_today_deal = deal::get_deal($org_id, $this->page(), $this->deal_today);
        $class_md_lg = "";
        if (count($data_today_deal) <= 6) {
            $class_md_lg = " hidden-md hidden-lg";
        }
        $today_deals = $deal_border_col->add_div('today')->add_class('link-deal col-sm-3 col-md-3 col-lg-2 active upper border-right-gray link')
                ->add('<span class="glyphicon glyphicon-chevron-left deal-control deal-control-left' . $class_md_lg . '"></span>')
                ->add($this->deal_today())
                ->add('<span class="glyphicon glyphicon-chevron-right deal-control deal-control-right' . $class_md_lg . '"></span>');
        /*
         * test
         */
        /*
          $data_today_deal[] = $data_today_deal[0];
          $data_today_deal[] = $data_today_deal[0];
          $data_today_deal[] = $data_today_deal[0];
          $data_today_deal[] = $data_today_deal[0];
         */

        $deal_border_row = $deal_border->add_div()->add_class('row row-deal-product row-today-deal-product');
        $deal_border_col = $deal_border_row->add_div()->add_class('col-md-12');

        $element_today_deal = $deal_border_col->add_element('62hall-slide-product-responsive', 'today-deal');
        $element_today_deal
                ->set_list_item($data_today_deal)->set_margin(0);

        // Weekly Deal
        $deal_border_row = $deal_border->add_div()->add_class('row row-deal row-weekly-deal');
        $deal_border_col = $deal_border_row->add_div()->add_class('col-md-12');
        $data_weekly_deal = deal::get_deal($org_id, $this->page(), $this->deal_weekly);
        $class_md_lg = "";
        if (count($data_weekly_deal) <= 6) {
            $class_md_lg = " hidden-md hidden-lg";
        }
        $weekly_deals = $deal_border_col->add_div('weekly')->add_class('link-deal col-sm-3 col-md-3 col-lg-2 upper border-right-gray link')
                ->add('<span class="glyphicon glyphicon-chevron-left deal-control deal-control-left' . $class_md_lg . '"></span>')
                ->add($this->deal_weekly())
                ->add('<span class="glyphicon glyphicon-chevron-right deal-control deal-control-right' . $class_md_lg . '"></span>');


        $deal_border_row = $deal_border->add_div()->add_class('row row-deal-product row-weekly-deal-product');
        $deal_border_col = $deal_border_row->add_div()->add_class('col-md-12');
        $element_weekly_deal = $deal_border_col->add_element('62hall-slide-product-responsive', 'weekly-deal');
        $element_weekly_deal
                ->set_list_item($data_weekly_deal);

        // New Product Deal
        $deal_border_row = $deal_border->add_div()->add_class('row row-deal row-new-deal');
        $deal_border_col = $deal_border_row->add_div()->add_class('col-md-12');
        $data_new_product = deal::get_deal($org_id, $this->page(), $this->deal_new_product);
        $class_md_lg = "";
        if (count($data_new_product) <= 6) {
            $class_md_lg = " hidden-md hidden-lg";
        }
        $new_product = $deal_border_col->add_div('new')->add_class('link-deal col-sm-3 col-md-3 col-lg-2 upper border-right-gray link')
                ->add('<span class="glyphicon glyphicon-chevron-left deal-control deal-control-left' . $class_md_lg . '"></span>')
                ->add($this->deal_new())
                ->add('<span class="glyphicon glyphicon-chevron-right deal-control deal-control-right' . $class_md_lg . '"></span>');


        $deal_border_row = $deal_border->add_div()->add_class('row row-deal-product row-new-deal-product');
        $deal_border_col = $deal_border_row->add_div()->add_class('col-md-12');
        $element_new_product = $deal_border_col->add_element('62hall-slide-product-responsive', 'new-product');
        $element_new_product
                ->set_list_item($data_new_product);

        // Best Selling Deal
        $deal_border_row = $deal_border->add_div()->add_class('row row-deal row-best-deal');
        $deal_border_col = $deal_border_row->add_div()->add_class('col-md-12');
        $data_best_selling = deal::get_deal($org_id, $this->page(), $this->deal_best_selling);

        
        $class_md_lg = "";
        if (count($data_best_selling) <= 6) {
            $class_md_lg = " hidden-md hidden-lg";
        }
        $bestselling_product = $deal_border_col->add_div('best')->add_class('link-deal col-sm-3 col-md-3 col-lg-2 upper border-right-gray link')
                ->add('<span class="glyphicon glyphicon-chevron-left deal-control deal-control-left' . $class_md_lg . '"></span>')
                ->add($this->deal_best_sell())
                ->add('<span class="glyphicon glyphicon-chevron-right deal-control deal-control-right' . $class_md_lg . '"></span>');



        $deal_border_row = $deal_border->add_div()->add_class('row row-deal-product row-best-deal-product');
        $deal_border_col = $deal_border_row->add_div()->add_class('col-md-12');
        $element_best_selling = $deal_border_col->add_element('62hall-slide-product-responsive', 'best-selling');
        $element_best_selling
                ->set_list_item($data_best_selling);
        
        //~ untuk bukan mybigmall
        if($org_first_parent_id!=620371){
            $deal_border_row = $deal_border->add_div()->add_class('row row-deal row-picked-deal');
            $deal_border_col = $deal_border_row->add_div()->add_class('col-md-12');
            $picked_for_you_list = deal::get_deal($org_id, $this->page(), $this->deal_pick_for_you);

            $pickedforyou = $deal_border_col->add_div('pick')->add_class('link-deal col-sm-3 col-md-3 col-lg-2 upper border-right-gray link')
                ->add('<span class="glyphicon glyphicon-chevron-left deal-control deal-control-left' . $class_md_lg . '"></span>')
                ->add('Klik Langsung Kirim')
                ->add('<span class="glyphicon glyphicon-chevron-right deal-control deal-control-right' . $class_md_lg . '"></span>');
            $deal_border_row = $deal_border->add_div()->add_class('row row-deal-product row-picked-product');
            $deal_border_col = $deal_border_row->add_div()->add_class('col-md-12');
            $element_picked = $deal_border_col->add_element('62hall-slide-product-responsive', 'picked-product');
            $element_picked
                    ->set_list_item($picked_for_you_list);
        }
        
        $app->add_js("
                $('.deal-border .deal-control.deal-control-left').click(function() {
                    $(this).closest('.row.row-deal').next().find('.owl-carousel').trigger('prev.owl.carousel', [300]);
                    var element= $(this).closest('.row.row-deal').next().find('.owl-carousel')[0];
                   
                    setTimeout(function(){
                        var n = document.createTextNode(' ');
                        var disp = element.style.display;  // don't worry about previous display style

                        element.appendChild(n);
                        element.style.display = 'inline-block';

                        setTimeout(function(){
                            element.style.display = disp;
                            n.parentNode.removeChild(n);
                        },20);
                        
                    },100);
                })
                $('.deal-border .deal-control.deal-control-right').click(function() {
                    $(this).closest('.row.row-deal').next().find('.owl-carousel').trigger('next.owl.carousel', [300]);
                    var element= $(this).closest('.row.row-deal').next().find('.owl-carousel')[0];
                    setTimeout(function(){
                        var n = document.createTextNode(' ');
                        var disp = element.style.display;  // don't worry about previous display style

                        element.appendChild(n);
                        element.style.display = 'inline-block';

                        setTimeout(function(){
                            element.style.display = disp;
                            n.parentNode.removeChild(n);
                        },20);
                        
                    },100);
                })
                ");

        //$product_deal = $deal_container->add_div('product-deal')->add_class('row-fluid deal-product-border');

        /*
          $app->add_js("
          jQuery('.link-deal').on('click', function(){
          var div_parent = $(this).parents('div');
          div_parent.find('.link-deal').each(function(){
          jQuery(this).removeClass('active');
          });

          var id = jQuery(this).attr('id');
          jQuery('#'+id).addClass('active');
          });
          ");

          //visible in screen small screen
          $arr_deal_today = array(
          "title" => $this->deal_today(),
          "url" => "get_today_deals_sm",
          "div_id" => "sm_deal_today"
          );
          $arr_deal_weekly = array(
          "title" => $this->deal_weekly(),
          "url" => "get_weekly_deals_sm",
          "div_id" => "sm_deal_weekly"
          );
          $arr_deal_new = array(
          "title" => $this->deal_new(),
          "url" => "get_new_products_sm",
          "div_id" => "sm_new_products"
          );
          $arr_deal_best_sell = array(
          "title" => $this->deal_best_sell(),
          "url" => "get_bestselling_products_sm",
          "div_id" => "sm_bestselling_products"
          );

          $arr_deal = array($arr_deal_today, $arr_deal_weekly, $arr_deal_new, $arr_deal_best_sell);

          $container_small_deal = $app->add_div()->add_class("small-deal-menu visible-xs visible-sm");
          foreach ($arr_deal as $key => $value) {
          $deal_title = carr::get($value, 'title');
          $deal_url = carr::get($value, 'url');
          $deal_title_id = carr::get($value, 'div_id');

          $small_deal = $container_small_deal->add_div()->add_class("small-deal container");
          $small_deal_container = $small_deal->add_div()->add_class("small-deal-container row-fluid");
          $small_deal_container_title = $small_deal_container->add_div()->add_class('small-deal-container-title sm-deal-btn');
          $small_deal_container_title->add_div()->add_class('pull-right')->add("<span><i class='fa fa-caret-down'></i></span>");
          $small_deal_container_title
          ->add($deal_title)
          ->add_listener('click')
          ->add_handler('reload')
          ->set_target($deal_title_id)
          ->set_url(curl::base() . "home/" . $deal_url);

          $small_deal_container_content = $small_deal_container->add_div()->add_class("small-deal-container-content");
          $small_deal_container_content->add_div($deal_title_id);
          }

          $app->add_js(
          "jQuery('.sm-deal-btn').click(function(){
          var sm_clicked = jQuery(this).parents('.small-deal-container');
          var sm_clicked_title = sm_clicked.find('.small-deal-container-title');
          var sm_clicked_content = sm_clicked.find('.small-deal-container-content');
          var sm_icon_caret = jQuery('.small-deal-container-title span i.fa');
          var sm_all_title = jQuery('.small-deal-container-title');
          jQuery('.small-deal-container').find('.small-deal-container-content').not(sm_clicked_content).hide();

          //change icon to close
          sm_icon_caret.removeClass('fa-caret-right');
          sm_icon_caret.removeClass('fa-caret-down');

          sm_icon_caret.addClass('fa-caret-down');
          sm_all_title.removeClass('active');

          //selected deal change icon to open
          if (sm_clicked_content.is(':visible')) {
          sm_clicked_title.find('span i.fa').addClass('fa-caret-down');
          sm_clicked_title.removeClass('active');
          }
          else {
          sm_clicked_title.find('span i.fa').addClass('fa-caret-right');
          sm_clicked_title.addClass('active');
          }

          sm_clicked_content.slideToggle('fast');

          });"
          );


         */

        // ADVERTISE 1
        $advertise_top = isset($data_advertise[$this->advertise_1]) ? $data_advertise[$this->advertise_1] : array();

        $element_advertise = $container->add_element('62hall-advertise-responsive', 'advertise-top');
        $element_advertise->set_list($advertise_top);

        // MENU PRODUCT LINE 1
        //~khusus mybigmall
        if($org_first_parent_id==620371){
        $menu_product = $container->add_div()->add_class('bg-white menu-product container-menu-product container-menu-product-1');

        $menu_product_container = $menu_product->add_div()->add_class('container margin-top-20 margin-bottom-20');
        $row_menu_product_container = $menu_product_container->add_div()->add_class('row-fluid');

        $menu = $row_menu_product_container->add_div('menuproduct-line1-container')->add_class('col-xs-12 col-md-2 col-lg-2');
        $menu_image = $row_menu_product_container->add_div('menuimage-line1-container')->add_class('col-xs-4 col-sm-4 col-md-2 col-lg-2 padding-right-0');
        $product = $row_menu_product_container->add_div('product-line1-container')->add_class('col-xs-12 col-sm-8 col-md-8 col-lg-8');

        // Menu Line 1
        $data_category_line1 = cms_product::get_cms_product($org_id, $this->page(), $this->category_product1);
        //$data_category_line1 = array();
        $element_menu_line1 = $menu->add_element('62hall-menu-product', 'menu-product-top')
                ->set_menu($data_category_line1)
                ->set_target('product-line1-container')
                ->set_target_image('menuimage-line1-container');

        // Ready Get
        $first_category_line1 = isset($data_category_line1[0]['cms_product_product_category_id']) ? $data_category_line1[0]['cms_product_product_category_id'] : NULL;

        if (!empty($first_category_line1)) {
            $listener = $app->add_listener('ready');
            $listener->add_handler('reload')
                    ->set_target('product-line1-container')
                    ->set_url(curl::base() . "home/get_product_category/" . $first_category_line1);
            $listener->add_handler('reload')
                    ->set_target('menuimage-line1-container')
                    ->set_url(curl::base() . "home/get_product_category_image/" . $first_category_line1);
        }




        // MENU PRODUCT LINE 2
        $menu_product = $container->add_div()->add_class('bg-white menu-product');

        $menu_product_container = $menu_product->add_div()->add_class('container margin-top-20 margin-bottom-20');
        $row_menu_product_container = $menu_product_container->add_div()->add_class('row-fluid');

        // $menu = $row_menu_product_container->add_div('menuproduct-line2-container')->add_class('col-sm-2 col-md-2 col-lg-2');
        // $menu_image = $row_menu_product_container->add_div('menuimage-line2-container')->add_class('col-sm-2 col-md-2 col-lg-2 padding-right-0');
        // $product = $row_menu_product_container->add_div('product-line2-container')->add_class('col-sm-10 col-md-8 col-lg-8');

        $menu = $row_menu_product_container->add_div('menuproduct-line2-container')->add_class('col-xs-12 col-md-2 col-lg-2');
        $menu_image = $row_menu_product_container->add_div('menuimage-line2-container')->add_class('col-xs-4 col-sm-4 col-md-2 col-lg-2 padding-right-0');
        $product = $row_menu_product_container->add_div('product-line2-container')->add_class('col-xs-12 col-sm-8 col-md-8 col-lg-8');

        // Menu Line 2
        $data_category_line2 = cms_product::get_cms_product($org_id, $this->page(), $this->category_product2);
        //$data_category_line2 = array();
        $element_menu_line2 = $menu->add_element('62hall-menu-product', 'menu-product-bottom')
                ->set_menu($data_category_line2)
                ->set_target('product-line2-container')
                ->set_target_image('menuimage-line2-container');

        // Ready Get

        $first_category_line2 = isset($data_category_line2[0]['cms_product_product_category_id']) ? $data_category_line2[0]['cms_product_product_category_id'] : NULL;

        if (!empty($first_category_line2)) {
            $listener = $app->add_listener('ready');
            $listener->add_handler('reload')
                    ->set_target('product-line2-container')
                    ->set_url(curl::base() . "home/get_product_category/" . $first_category_line2);
            $listener->add_handler('reload')
                    ->set_target('menuimage-line2-container')
                    ->set_url(curl::base() . "home/get_product_category_image/" . $first_category_line2);
        }
        }


        // ADVERTISE 2
        $advertise_middle = isset($data_advertise[$this->advertise_2]) ? $data_advertise[$this->advertise_2] : array();

        $element_advertise = $container->add_element('62hall-advertise-responsive', 'advertise-middle');
        $element_advertise->set_list($advertise_middle);
        $no = 0;
        // 10 CATEGORY 
        if($org_id=='620044'){
            $data_category_floor = cms_floor::get_cms_floor($org_id, $this->page());
            //cdbg::var_dump($data_category_floor);
            foreach ($data_category_floor as $val) {
                $no++;
                $container_pc = $container->add_div()->add_class('container container-category container-category-' . $no);
                $pc_header = $container_pc->add_div()->add_class('category-header');
                $pc_header_background = $pc_header->add_div()->add_class('category-header-background');
                $pc_header_background->add('<span class="glyphicon glyphicon-chevron-right category-control category-control-right hidden-xs hidden-md hidden-lg"></span>');
                $pc_header_background->add('<span class="glyphicon glyphicon-chevron-left category-control category-control-left hidden-xs hidden-md hidden-lg"></span>');

                $pc_title = $pc_header->add_div()->add_class('category-header-title');
                $pc_title->add('<span class="category-no">' . $no . '</span>');
                $pc_title->add('<span class="category-name"><a href="'.curl::base().'product/category/'.carr::get($val,'product_category_id').'">' . carr::get($val,'name') . '</a></span>');
                $pc_title->add('<span class="category-space">&nbsp;</span>');
                $pc_body = $container_pc->add_div('category-body-' . $no)->add_class('category-body');
                $pc_body->add_listener('ready')->add_handler('reload')->set_target('category-body-' . $no)->set_url(curl::base() . 'home/reload_product_category_body/' . carr::get($val,'product_category_id').'/'. carr::get($val,'cms_floor_detail_id'));
            }
        }
        else{
            $q = "select * from product_category where product_type_id=1 and parent_id is null and status>0 order by lft";
            $r = $db->query($q);
            
            foreach ($r as $row) {
                $no++;
                $container_pc = $container->add_div()->add_class('container container-category container-category-' . $no);
                $pc_header = $container_pc->add_div()->add_class('category-header');
                $pc_header_background = $pc_header->add_div()->add_class('category-header-background');
                $pc_header_background->add('<span class="glyphicon glyphicon-chevron-right category-control category-control-right hidden-xs hidden-md hidden-lg"></span>');
                $pc_header_background->add('<span class="glyphicon glyphicon-chevron-left category-control category-control-left hidden-xs hidden-md hidden-lg"></span>');

                $pc_title = $pc_header->add_div()->add_class('category-header-title');
                $pc_title->add('<span class="category-no">' . $no . '</span>');
                $pc_title->add('<span class="category-name"><a href="'.curl::base().'product/category/'.$row->product_category_id.'">' . strtoupper($row->name) . '</a></span>');
                $pc_title->add('<span class="category-space">&nbsp;</span>');
                $pc_body = $container_pc->add_div('category-body-' . $no)->add_class('category-body');
                $pc_body->add_listener('ready')->add_handler('reload')->set_target('category-body-' . $no)->set_url(curl::base() . 'home/reload_product_category_body/' . $row->product_category_id);


            }
        }
        $app->add_js("
                $('.container-category .category-control.category-control-left').click(function() {
                    var active = $(this).closest('.container-category').find('.category-body .col-category-right.active');
                    var inactive = $(this).closest('.container-category').find('.category-body .col-category-right.inactive');
                    active.removeClass('active').addClass('inactive');
                    inactive.removeClass('inactive').addClass('active');
                })
                $('.container-category .category-control.category-control-right').click(function() {
                    var active = $(this).closest('.container-category').find('.category-body .col-category-right.active');
                    var inactive = $(this).closest('.container-category').find('.category-body .col-category-right.inactive');
                    active.removeClass('active').addClass('inactive');
                    inactive.removeClass('inactive').addClass('active');
                    
                    console.log($(this).closest('.row.row-deal').next());
                })
                ");

        // PICKED FOR YOU
        // untuk mybigmall
        if($org_first_parent_id=='620371'){
            $pickedforyou = $container->add_div()->add_class('container');

            $pickedforyou->add('<h4 class="pick-for-you-title line-side-header "><span>SPECIAL FOR YOU</span></h4>');
            //$pickedforyou->add('<h4 class="border-bottom-gray padding bold font-red">PILIHAN UNTUK ANDA</h4>');
            $container->add_br();

            $picked_for_you_list = deal::get_deal($org_id, $this->page(), $this->deal_pick_for_you);

            $container_picked = $container->add_div()->add_class("container");

            $element_picked_for_you = $container_picked->add_element('62hall-slide-product-responsive', 'picked-for-you')
                    // $element_picked_for_you->set_count_item(6)
                    ->set_list_item($picked_for_you_list);
        }

        // ADVERTISE 3
        $advertise_bottom = isset($data_advertise[$this->advertise_3]) ? $data_advertise[$this->advertise_3] : array();

        $element_advertise = $container->add_element('62hall-advertise-responsive', 'advertise-bottom');
        $element_advertise->set_list($advertise_bottom);

       $element_publication = $container->add_element('62hall-cms-publication', 'cms-publication');
       $element_testimonial = $container->add_element('62hall-cms-testimonial', 'cms-testimonial');
        if($org_first_parent_id!=620371){
            $bottom_area = $container->add_div('bottom-area');
            $row_bottom = $bottom_area->add_div()->add_class('container');
            $bottom_left = $row_bottom->add_div()->add_class('col-md-6');
            $bottom_right = $row_bottom->add_div()->add_class('col-md-6');
            
            $payment_logo = cms_options::get_image_list('payment');
            $delivery_logo = cms_options::get_image_list('delivery');
            
            if(count($payment_logo)>0){
                $bottom_left->add('<h3 class="title">Metode Pembayaran</h3>');
                $bottom_left->add('<ul class="list-inline payment-method">');
                foreach ($payment_logo as $payment_logo_k => $payment_logo_v) {
                    $name = carr::get($payment_logo_v, 'name');
                    $image_name = carr::get($payment_logo_v, 'image_name');
                    $image_url = carr::get($payment_logo_v, 'image_url');
                    $bottom_left->add('<li class="image-item">');
                        $bottom_left->add('<img src="'.$image_url.'" class="img-responsive" alt="'.$name.'">');
                    $bottom_left->add('</li>');
                }
                $bottom_left->add('</ul>');
            }
            if(count($delivery_logo)>0){
                $bottom_right->add('<h3 class="title">Jasa Pengiriman</h3>');
                $bottom_right->add('<ul class="list-inline expedition-list">');
                foreach ($delivery_logo as $delivery_logo_k => $delivery_logo_v) {
                    $name = carr::get($delivery_logo_v, 'name');
                    $image_name = carr::get($delivery_logo_v, 'image_name');
                    $image_url = carr::get($delivery_logo_v, 'image_url');
                    $bottom_right->add('<li class="image-item">');
                        $bottom_right->add('<img src="'.$image_url.'" class="img-responsive" alt="'.$name.'">');
                    $bottom_right->add('</li>');
                }
                $bottom_right->add('</ul>');
            }
        }
        echo $app->render();
    }

    public function reload_product_category_body($product_category_id,$cms_floor_detail_id='') {
        $app = CApp::instance();
        $db = CDatabase::instance();
    $org_id=CF::org_id();
        $row = cdbutils::get_row('select * from product_category where product_category_id=' . $db->escape($product_category_id));
        $category_image_url = curl::base() . 'cresenity/noimage';
        if (strlen($row->image_name) > 0) {
            $category_image_url = image::get_image_url($row->image_name);
        }
        if(strlen($cms_floor_detail_id)>0){
            $category_image_url = cdbutils::get_value('select image_path from cms_floor_detail where cms_floor_detail_id='.$db->escape($cms_floor_detail_id));
        }
        $qproduct = "
            select 
                * 
            from 
                product as p 
                inner join product_category as pc on p.product_category_id=pc.product_category_id 
        inner join vendor as v on v.vendor_id=p.vendor_id
            where 
                pc.lft>=" . $db->escape($row->lft) . " 
                and pc.rgt<=" . $db->escape($row->rgt) . " 
                and p.status_confirm='CONFIRMED'
                and p.is_active=1
                and p.status>0
                and p.visibility='catalog_search'
                and (v.org_id is null or v.org_id=".$db->escape($org_id).")
        
            order by (p.is_hot_item = 1
                or p.new_product_date >=  " . $db->escape(date('Y-m-d')) . "
                ) desc, rand() limit 8";
        
       
        $rproduct = $db->query($qproduct);
        $product_no = 0;
        $div_row = $app->add_div()->add_class('row');
        $div_col = $div_row->add_div()->add_class('col-md-4 hidden-sm hidden-xs');
        $div_row_product = $div_col->add_div()->add_class('row row-category-2');
        $list_product = array();
        $list_product_4 = array();
        foreach ($rproduct as $product_row) {
            $product_no++;
            //draw product
            $product = product::get_product($product_row->product_id);
            $list_product[] = $product;
            $product_id = !empty($product['product_id']) ? $product['product_id'] : NULL;
            $image_url = !empty($product['image_path']) ? $product['image_path'] : NULL;
            $name = !empty($product['name']) ? $product['name'] : NULL;
            $flag = $hot = ($product['is_hot_item'] > 0) ? 'Hot' : NULL;

            if (!empty($product['new_product_date'])) {
                if (strtotime($product['new_product_date']) >= strtotime(date('Y-m-d'))) {
                    $flag = $new = 'New';
                }
            }

            $promo = !empty($product['promo']) ? $product['promo'] : NULL;
            $stock = !empty($product['stock']) ? $product['stock'] : 0;
            $min_stock = !empty($product['show_min_stock']) ? $product['show_min_stock'] : 0;
            $price = !empty($product['detail_price']) ? $product['detail_price'] : array();
            $is_available = $product['is_available'];

            $element_product = CElement_Product_Product::factory();
            $element_product->set_key($product_id)
                    ->set_image($image_url)
                    ->set_column(1)
                    ->set_name($name)
                    ->set_flag($flag)
                    ->set_label_flag($flag)
                    ->set_price($price)
                    ->set_stock($stock)
                    ->set_min_stock($min_stock)
                    ->set_available($is_available);

            $element_html = $element_product->html();

            $div_row_product->add_div()->add_class('col-md-6 col-sm-6 col-xs-12')->add_div()->add_class('row')->add($element_html);
            if ($product_no <= '4') {
                $list_product_4[] = $element_html;
            }
            if ($product_no == '4') {
                $div_col = $div_row->add_div()->add_class('col-category-image col-md-4 col-sm-6 col-xs-12');
                $div_col->add('<a href="'.curl::base().'product/category/'.$row->product_category_id.'">' . '<img src="'.$category_image_url.'" class="category-image"/>' . '</a>');
                        //add_img()->set_src($category_image_url)->add_class('category-image');
                $div_col = $div_row->add_div()->add_class('col-md-4 col-sm-6 col-xs-12 hidden-xs active col-category-right');
                $div_row_product = $div_col->add_div()->add_class('row row-category-2 hidden-xs');
                $div_col_cloned = $div_row->add_div()->add_class('col-md-4 col-sm-6 col-xs-12 hidden-xs col-category-right inactive col-category-right-cloned');
                $div_row_product_cloned = $div_col_cloned->add_div()->add_class('row row-category-2 hidden-xs');
                foreach($list_product_4 as $html) {
                    $div_row_product_cloned->add_div()->add_class('col-md-6 col-sm-6 col-xs-12')->add_div()->add_class('row')->add($html);
                }
                
            }
        }
        $div_row = $app->add_div()->add_class('row hidden-sm hidden-md hidden-lg');
        $div_col = $div_row->add_div()->add_class('col-xs-12');
        $elm = $div_col->add_element('62hall-slide-product-responsive', '');
        $elm
                ->set_list_item($list_product);
        echo $app->render();
    }

    public function search() {
        $app = CApp::instance();

        $element_search = $app->add_element('62hall-search');
        ;
        $app->add_js("$('input').blur();");

        echo $app->render();
    }

    public function get_product_category_image($category_id = null) {
        $app = CApp::instance();
        if ($category_id == null) {
            $request = array_merge($_GET, $_POST);
            $category_id = carr::get($request, 'category_id');
        }

        // PRODUCT LINE 1
        $data_product_line1 = cms_product::get_cms_product_product_category($category_id);
        $image_url = carr::get($data_product_line1, 'image_url');

        $app->add('<img style="height: 532px" width="100%" src="' . $image_url . '"/>');

        echo $app->render();
    }

    public function get_product_category($category_id = null) {
        $app = CApp::instance();
        if ($category_id == null) {
            $request = array_merge($_GET, $_POST);
            $category_id = carr::get($request, 'category_id');
        }
        // PRODUCT LINE 1
        $data_product_line1 = cms_product::get_cms_product_product_category($category_id);
        $list_product = carr::get($data_product_line1, 'data');

        $elemet_product = $app->add_element('62hall-menu-product-category', 'menu-product-category-' . $category_id)
                ->set_margin(30)
                ->set_list_product($list_product);

        echo $app->render();
    }

    public function get_today_deals() {
        $app = CApp::instance();
        $user = $app->user();
        $org_id = $this->org_id();

        // TODAY DEAL
        $data_today_deal = deal::get_deal($org_id, $this->page(), $this->deal_today);
        $element_today_deal = $app->add_element('62hall-slide-product', 'today-deal');
        $element_today_deal->set_count_item(6)
                ->set_list_item($data_today_deal);

        echo $app->render();
    }

    public function get_weekly_deals() {
        $app = CApp::instance();
        $user = $app->user();
        $org_id = $this->org_id();

        // WEEKLY DEAL
        $data_weekly_deal = deal::get_deal($org_id, $this->page(), $this->deal_weekly);

        $element_weekly_deal = $app->add_element('62hall-slide-product', 'weekly-deal');
        $element_weekly_deal->set_count_item(6)
                ->set_list_item($data_weekly_deal);

        echo $app->render();
    }

    public function get_new_products() {
        $app = CApp::instance();
        $user = $app->user();
        $org_id = $this->org_id();

        // NEW PRODUCT
        $data_new_product = deal::get_deal($org_id, $this->page(), $this->deal_new_product);

        $element_new_product = $app->add_element('62hall-slide-product', 'new-product');
        $element_new_product->set_count_item(6)
                ->set_list_item($data_new_product);

        echo $app->render();
    }

    public function get_bestselling_products() {
        $app = CApp::instance();
        $user = $app->user();
        $org_id = $this->org_id();

        // BEST SELLING
        $data_best_selling = deal::get_deal($org_id, $this->page(), $this->deal_best_selling);

        $element_best_selling = $app->add_element('62hall-slide-product', 'best-selling');
        $element_best_selling->set_count_item(6)
                ->set_list_item($data_best_selling);

        echo $app->render();
    }

    public function get_today_deals_sm() {
        $app = CApp::instance();
        $user = $app->user();
        $org_id = $this->org_id();

        // TODAY DEAL
        $data_today_deal = deal::get_deal($org_id, $this->page(), $this->deal_today);
        $element_today_deal = $app->add_element('62hall-slide-product-responsive', 'today-deal-sm')
                // $element_today_deal->set_count_item(6)
                ->set_list_item($data_today_deal);

        echo $app->render();
    }

    public function get_weekly_deals_sm() {
        $app = CApp::instance();
        $user = $app->user();
        $org_id = $this->org_id();

        // WEEKLY DEAL
        $data_weekly_deal = deal::get_deal($org_id, $this->page(), $this->deal_weekly);

        $element_weekly_deal = $app->add_element('62hall-slide-product-responsive', 'weekly-deal-sm')
                // $element_weekly_deal->set_count_item(6)
                ->set_list_item($data_weekly_deal);

        echo $app->render();
    }

    public function get_new_products_sm() {
        $app = CApp::instance();
        $user = $app->user();
        $org_id = $this->org_id();

        // NEW PRODUCT
        $data_new_product = deal::get_deal($org_id, $this->page(), $this->deal_new_product);

        $element_new_product = $app->add_element('62hall-slide-product-responsive', 'new-product-sm')
                // $element_new_product->set_count_item(6)
                ->set_list_item($data_new_product);

        echo $app->render();
    }

    public function get_bestselling_products_sm() {
        $app = CApp::instance();
        $user = $app->user();
        $org_id = $this->org_id();

        // BEST SELLING
        $data_best_selling = deal::get_deal($org_id, $this->page(), $this->deal_best_selling);

        // $element_best_selling = $app->add_element('62hall-slide-product', 'best-selling-sm');
        // $element_best_selling->set_count_item(3)

        $element_best_selling = $app->add_element('62hall-slide-product-responsive', 'best-selling-sm')
                ->set_list_item($data_best_selling);
        echo $app->render();
    }

}
