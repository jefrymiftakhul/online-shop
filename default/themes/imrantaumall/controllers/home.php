<?php

/**
 * @author: Ittronmall
 * @since:   2017-5-8
 * @filename: home.php
 * @filepath: C:\xampp\htdocs_pipo\application\ittronmall\default\themes\fitgloss\controllers\home.php
 */
class Home_Controller extends ProductController {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $app = CApp::instance();
        $org_id = CF::org_id();
        $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");
        $session=Session::instance();

        $responsive = array();
        $responsive[] = array(
            'breakpoint' => '1200',
            'slide_to_show' => '4',
            'slide_to_scroll' => '1',
            'dots' => 'false',
        );        
        $responsive[] = array(
            'breakpoint' => '992',
            'slide_to_show' => '3',
            'slide_to_scroll' => '1',
            'dots' => 'false',
        );
        $responsive[] = array(
            'breakpoint' => '768',
            'slide_to_show' => '2',
            'slide_to_scroll' => '1',
            'dots' => 'false',
        );
        

        $slide_list_top = slide::get_slide($org_id, $this->page());


        $slide = $app->add_div()->add_class('home-slideshow row');
        $element_slide_show = $slide->add_element('62hall-slide-show', 'home_slideshow');
        $element_slide_show->set_slides($slide_list_top);

        //category
        $container_page_row = $app->add_div()->add_class('row');
        $category_image = $container_page_row->add_div()->add_class('category-image sixty-slider owl-carousel');
        $menu_category = product_category::get_product_category_menu('product', '', $org_id);
        foreach ($menu_category as $k => $v) {
            $name_menu_category = strtoupper($v['name']);
            $url_key = $v['url_key'];
            $url_menu = curl::base() . 'search?keyword=&category=';
            $url = $url_menu . $url_key;
            $category_single_image = $category_image->add_div()->add_class('col-lg-3 col-md-3 col-sm-3 col-xs-6 item');
            $image = $category_single_image->add("
                    <div id='bg'>
                        <a href='" . $url . "' class='image-category img-responsive'>
                            <img src='" . $v['image_url'] . "' class='img-category-single-image' >
                            <div id='caption'> 
                                    " . $name_menu_category . "
                            </div>
                       </a>
                    </div>");
        }

        $container = $app->add_div()->add_class('container');
        $container_category_image = $container_page_row->add_div()->add_class('col-md-12');
        //end category
        //product deal
        $data_advertise = advertise::get_advertise($org_id, $this->page());
        $adv = carr::get($data_advertise, 0, array());

        $container_adds = $container->add_div()->add_class('container-adds');
        $container_adds_content = $container_adds->add_div()->add_class('row');
        $adds_content = $container_adds_content->add_div()->add_class('col-lg-12');

        $deal_best_sell = deal::get_deal($org_id, $this->page(), 'deal_best_sell', array('sortby' => 'priority'));
        $deal_product_new = deal::get_deal($org_id, $this->page(), 'deal_new', array('sortby' => 'priority'));
        $deal_hot = deal::get_deal($org_id, $this->page(), 'deal_hot', array('sortby' => 'priority'));
        $container_deal = $container->add_div()->add_class('container-deal');
        
        //product new
        $container_deal_title = $container_deal->add_div()->add_class('container-deal-title');
        $container_deal_title_txt = $container_deal_title->add_div()->add_class('deal-title');
        $container_deal_view_all = $container_deal_title->add_div()->add_class('deal-view-all');
        $container_deal_title_txt->add(clang::__('New Product'));
        $container_deal_view_all->add("<a href='" . curl::base() . "show_product/show/deal_new' class='btn-view-all-product'>" . clang::__('See All') . "<div class='arrow-product'></div></a>");
        $container_deal_content = $container_deal->add_div()->add_class('container-deal-content');
        $element_deal = $container_deal_content->add_element('fitgloss-deal', 'deal_new')
                ->set_slides($deal_product_new)
                ->add_class('slide-deal fitgloss-deal')
                ->set_responsive($responsive);
        
        //product best seller
        $container_deal_title = $container_deal->add_div()->add_class('container-deal-title');
        $container_deal_title_txt = $container_deal_title->add_div()->add_class('deal-title');
        $container_deal_view_all = $container_deal_title->add_div()->add_class('deal-view-all');
        $container_deal_title_txt->add(clang::__('Best Seller'));
        $container_deal_view_all->add("<a href='" . curl::base() . "show_product/show/deal_best_sell' class='btn-view-all-product'>" . clang::__('See All') . "<div class='arrow-product'></div></a>");
        $container_deal_content = $container_deal->add_div()->add_class('container-deal-content');
        $element_deal = $container_deal_content->add_element('fitgloss-deal', 'best_sell')
                ->set_slides($deal_best_sell)
                ->add_class('slide-deal fitgloss-deal')
                ->set_responsive($responsive);


        //advertising
        $container_adv = $app->add_div()->add_class('row');
        foreach ($adv as $key => $value) {
            switch ($key) {
                case 0:
                    $image_url = carr::get($value, 'image_url');
                    $url_link = carr::get($value, 'url_link');

                    $container_adv->add_div()->add_class('col-md-12 container-adds-bottom')
                            ->add("<a href='" . $url_link . "'>
                                <div class='content-adv'>
                                    <div id='mainImage'>
                                        <img class='mainImage img-responsive' src='" . $image_url . "' />
                                        <!--<div id='startButton'>
                                            <div class='btn-adv'>" . clang::__('SEE ALL SELL') . "<div class='icon-adv-sale'></div></div>
                                        </div>-->
                                    </div>
                                 </div>
                                            
                               </a>");
                    break;
                default :
                    break;
            }
        }

        //hot deals        
        $container_deals = $app->add_div()->add_class('container');
        $container_deal_hot = $container_deals->add_div()->add_class('container-deal');
        $container_deal_title = $container_deal_hot->add_div()->add_class('container-deal-title');
        $container_deal_title_txt = $container_deal_title->add_div()->add_class('deal-title');
        $container_deal_view_all = $container_deal_title->add_div()->add_class('deal-view-all');
        $container_deal_title_txt->add(clang::__('Hot Deals'));
        $container_deal_view_all->add("<a href='" . curl::base() . "show_product/show/deal_hot' class='btn-view-all-product'>" . clang::__('See All') . "<div class='arrow-product'></div></a>");
        $container_deal_content = $container_deal_hot->add_div()->add_class('container-deal-content');
        $element_deal = $container_deal_content->add_element('fitgloss-deal', 'deal_hot')
                ->set_slides($deal_hot)
                ->add_class('slide-deal fitgloss-deal')
                ->set_responsive($responsive);

        $app->add_js("
            var owl = $('.owl-carousel').owlCarousel({
                loop:false,
                nav:false,
                responsiveClass:true,
                margin:20,
                responsive:{
                    100:{
                        items:2,
                        margin: 12
                    },
                    600:{
                        items:4,
                        margin: 15
                    },
                    1000:{
                        items:4,
                    }
                }
            });
            
            owl.on('onAnimationEnd',function(){
                console.log('after');
            });            
              
        ");
        echo $app->render();
    }

}
