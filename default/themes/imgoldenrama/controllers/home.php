<?php

class Home_Controller extends ProductController {

    private $label_deal_new = 'New Product';
    private $label_deal_best_sell = 'Best Seller';
    private $label_deal_hot = 'Hot Deals';

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $app = CApp::instance();
        $org_id = CF::org_id();
        $db = CDatabase::instance();
        $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

        $slide_list_top = slide::get_slide($org_id, $this->page());


        $slide = $app->add_div()->add_class('home-slideshow row');
        $element_slide_show = $slide->add_element('62hall-slide-show', 'home_slideshow');
        $element_slide_show->set_slides($slide_list_top);

        $q_menu_category = 'select * from product_category where status > 0 and depth = 0';
        $data_menu_category = product_category::get_product_category_menu($this->page());
        $arr_menu = array();
        $first_cat_id = 0;
        foreach ($data_menu_category as $key => $value) {
            if ($key == 0)
                $first_cat_id = carr::get($value, 'category_id');
            $arr_menu[] = array(
                'label' => carr::get($value, 'name'),
                'url_key' => carr::get($value, 'url_key'),
                'category_id' => carr::get($value, 'category_id'),
            );
        }
//        $product_category = $app->add_div()->add_class('product-category');
        $responsive = array();
        $responsive[] = array(
            'breakpoint' => '1200',
            'slide_to_show' => '6',
            'slide_to_scroll' => '1',
            'dots' => 'false',
        );
        $responsive[] = array(
            'breakpoint' => '767',
            'slide_to_show' => '3',
            'slide_to_scroll' => '1',
            'dots' => 'false',
        );
        $responsive[] = array(
            'breakpoint' => '480',
            'slide_to_show' => '1',
            'slide_to_scroll' => '1',
            'dots' => 'false',
            'arrow' => 'false',
        );
        $menu_category = $app->add_div()->add_class('menu-category');
        $element_menu_category = $menu_category->add_element('slide-menu', 'menu-category');
        $element_menu_category->set_url_reload(curl::base() . 'home/reload_category');
        $element_menu_category->set_target('result_category');
        $element_menu_category->set_slides($arr_menu);
        $element_menu_category->set_center_mode(false);
        $element_menu_category->set_slide_to_show(7);
        $element_menu_category->set_responsive($responsive);
        $result_category = $app->add_div('result_category');
        $app->add_listener('ready')->add_handler('reload')->set_url(curl::base() . 'home/reload_category/' . $first_cat_id)->set_target('result_category');
        $app->add_js("
            var active_cat_id = '" . $first_cat_id . "';
            $('.menu-category .slick-slide a[data-category-id='+active_cat_id+']').addClass('active');
            $('.menu-category .slick-slide a').each(function(e){
                $(this).click(function(evt){
                    $('.menu-category .slick-slide a').each(function(e){
                        $(this).removeClass('active');
                    });
                    $(this).addClass('active');
                });
            });
        ");
        // tentang kami
        $container = $app->add_div()->add_class('container');
        $secion_about = $container->add_div()->add_class('section-about');
        $row = $secion_about->add_div()->add_class('row');
        $col12 = $row->add_div()->add_class('col-xs-12');

        $home_section_title = cms_options::get('home_section_title');
        if ($home_section_title === null) {
            $home_section_title = 'TENTANG KAMI';
        }
        $home_section_tag_line = cms_options::get('home_section_tag_line');
        if ($home_section_tag_line === null) {
            $home_section_tag_line = 'Corporate tagline here';
        }
        $home_section_title_1 = cms_options::get('home_section_title_1');
        if ($home_section_title_1 === null) {
            $home_section_title_1 = 'EXCELLENCE';
        }
        $home_section_content_1 = cms_options::get('home_section_content_1');
        if ($home_section_content_1 === null) {
            $home_section_content_1 = 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using';
        }
        $home_section_title_2 = cms_options::get('home_section_title_2');
        if ($home_section_title_2 === null) {
            $home_section_title_2 = 'PROFESSIONAL';
        }
        $home_section_content_2 = cms_options::get('home_section_content_2');
        if ($home_section_content_2 === null) {
            $home_section_content_2 = 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using';
        }

        $home_section_title_3 = cms_options::get('home_section_title_3');
        if ($home_section_title_3 === null) {
            $home_section_title_3 = 'CUSTOMER ORIENTED';
        }
        $home_section_content_3 = cms_options::get('home_section_content_3');
        if ($home_section_content_3 === null) {
            $home_section_content_3 = 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using';
        }

        $home_section_title_4 = cms_options::get('home_section_title_4');
        if ($home_section_title_4 === null) {
        $home_section_title_4 = 'STATISFICATION GUARANTED';
        }
        $home_section_content_4 = cms_options::get('home_section_content_4');
        if ($home_section_content_4 === null) {
        $home_section_content_4 = 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using';
        }



        $section_wrapper = $col12->add_div()->add_class('section-about-header');
        $section_wrapper->add('<h3 class="section-about-title">' . $home_section_title . '</h3>'
                . '<p class="section-about-tagline">' . $home_section_tag_line . '</p>');
        $col3 = $row->add_div()->add_class('col-xs-12 col-sm-3');
        $image3 = curl::base() . 'application/ittronmall/default/media/img/goldenrama/3.png';
        $col3->add_div()->add_class('about-tile-image')->add('<img src="' . $image3 . '" alt="">');
        $col3->add('<h3 class="tiles-item-title">' . $home_section_title_1 . '</h3>');
        $col3->add('<p class="tiles-item-desc">' . $home_section_content_1 . '</p>');

        $col3 = $row->add_div()->add_class('col-xs-12 col-sm-3');
        $image4 = curl::base() . 'application/ittronmall/default/media/img/goldenrama/4.png';
        $col3->add_div()->add_class('about-tile-image')->add('<img src="' . $image4 . '" alt="">');
        $col3->add('<h3 class="tiles-item-title">' . $home_section_title_2 . '</h3>');
        $col3->add('<p class="tiles-item-desc">' . $home_section_content_2 . '</p>');

        $col3 = $row->add_div()->add_class('col-xs-12 col-sm-3');
        $image5 = curl::base() . 'application/ittronmall/default/media/img/goldenrama/5.png';
        $col3->add_div()->add_class('about-tile-image')->add('<img src="' . $image5 . '" alt="">');
        $col3->add('<h3 class="tiles-item-title">' . $home_section_title_3 . '</h3>');
        $col3->add('<p class="tiles-item-desc">' . $home_section_content_3 . '</p>');

        $col3 = $row->add_div()->add_class('col-xs-12 col-sm-3');
        $image6 = curl::base() . 'application/ittronmall/default/media/img/goldenrama/6.png';
        $col3->add_div()->add_class('about-tile-image')->add('<img src="' . $image6 . '" alt="">');
        $col3->add('<h3 class="tiles-item-title">' . $home_section_title_4 . '</h3>');
        $col3->add('<p class="tiles-item-desc">' . $home_section_content_4 . '</p>');

        $data_advertise = advertise::get_advertise($org_id, $this->page());
        $ads_first_row = carr::get($data_advertise, 0);
        $ads_first_col = carr::get($ads_first_row, 0);
        $url_link = carr::get($ads_first_col, 'url_link');
        $image_url = carr::get($ads_first_col, 'image_url');
        // ads top
        $section_ads = $container->add_div()->add_class('section-banner');
        $row = $section_ads->add_div()->add_class('row');
        $col12 = $row->add_div()->add_class('col-xs-12');
        $advertisement_top = $col12->add_div()->add_class('banner-top');
        $advertisement_top->add("<a href='" . $url_link . "'><img class='img-responsive banner-top-img' src='$image_url'></a>");

        $org_id = CF::org_id();
        $deal_new = deal::get_deal($org_id, $this->page(), 'deal_new', array('sortby' => 'priority'));
        $deal_hot = deal::get_deal($org_id, $this->page(), 'deal_hot', array('sortby' => 'priority'));
        $deal_best_sell = deal::get_deal($org_id, $this->page(), 'deal_best_sell', array('sortby' => 'priority'));

        $data_cms_deal = $db->query('select * from cms_deal_setting where status > 0 and org_id = ' . $db->escape($org_id));

        foreach ($data_cms_deal as $key => $value) {
            $code = cobj::get($value, 'code');
            $name = cobj::get($value, 'name');
            switch ($code) {
                case 'deal_new':
                    $this->label_deal_new = $name;
                    break;
                case 'deal_best_sell':
                    $this->label_deal_best_sell = $name;
                    break;
                case 'deal_hot':
                    $this->label_deal_hot = $name;
                    break;
            }
        }

        $responsive = array();
        $responsive[] = array(
            'breakpoint' => '1200',
            'slide_to_show' => '4',
            'slide_to_scroll' => '1',
            'dots' => 'false',
            'arrow' => 'false',
        );
        $responsive[] = array(
            'breakpoint' => '767',
            'slide_to_show' => '1',
            'slide_to_scroll' => '1',
            'dots' => 'false',
            'arrow' => 'false',
        );
        $responsive[] = array(
            'breakpoint' => '480',
            'slide_to_show' => '1',
            'slide_to_scroll' => '1',
            'dots' => 'false',
            'arrow' => 'false',
        );

        //Product New
        if (count($deal_new) > 0) {
            $container_page = $container->add_div()->add_class('container-page');
            $container_page_row = $container_page->add_div()->add_class('row');
            $container_product = $container_page_row->add_div()->add_class('col-md-12');
            $container_deal = $container_product->add_div()->add_class('container-deal');
            $element_deal = $container_deal->add_element('fitgloss-deal', 'deal-newproduct')
                    ->set_title($this->label_deal_new)
                    ->set_slides($deal_new)
                    ->set_responsive($responsive)
                    ->add_class('slide-deal goldenrama-deal');
            $view_all = $container->add_div()->add_class('view-all-wrapper');
            $view_all->add_action()->set_label('VIEW ALL')->add_class('view-all-btn inverse')->set_link(curl::base() . 'show_product/show/deal_new');
        }

        // Best
        if (count($deal_best_sell) > 0) {
            $container_page = $container->add_div()->add_class('container-page');
            $container_page_row = $container_page->add_div()->add_class('row');
            $container_product = $container_page_row->add_div()->add_class('col-md-12');
            $container_deal = $container_product->add_div()->add_class('container-deal');
            $element_deal = $container_deal->add_element('fitgloss-deal', 'deal-best')
                    ->set_title($this->label_deal_best_sell)
                    ->set_slides($deal_best_sell)
                    ->set_responsive($responsive)
                    ->add_class('slide-deal goldenrama-deal');
            $view_all = $container->add_div()->add_class('view-all-wrapper');
            $view_all->add_action()->set_label('VIEW ALL')->add_class('view-all-btn inverse')->set_link(curl::base() . 'show_product/show/deal_best_sell');
        }
        
        // Hot Deal
        if (count($deal_hot) > 0) {
            $container_page = $container->add_div()->add_class('container-page');
            $container_page_row = $container_page->add_div()->add_class('row');
            $container_product = $container_page_row->add_div()->add_class('col-md-12');
            $container_deal = $container_product->add_div()->add_class('container-deal');
//            $page_title = $container_deal->add_div()->add_class('page-title-custom');
//            $page_title->add('<span>'.$this->golden_deal_special().'</span>');
//            $page_title->add_div()->add_class('ico-underline');
            $element_deal = $container_deal->add_element('fitgloss-deal', 'deal-special')
                    ->set_title($this->label_deal_hot)
                    ->set_slides($deal_hot)
                    ->set_responsive($responsive)
                    ->add_class('slide-deal goldenrama-deal');
            $view_all = $container->add_div()->add_class('view-all-wrapper');
            $view_all->add_action()->set_label('VIEW ALL')->add_class('view-all-btn inverse')->set_link(curl::base() . 'show_product/show/deal_hot');
        }


        $ads_first_row = carr::get($data_advertise, 1, array());
        $section_ads = $container->add_div()->add_class('section-banner');
        $row = $section_ads->add_div()->add_class('row');
        foreach ($ads_first_row as $key => $value) {
            $url_link = carr::get($value, 'url_link');
            $image_url = carr::get($value, 'image_url');
            $col12 = $row->add_div()->add_class('col-xs-12 col-sm-4');
            $advertisement_top = $col12->add_div()->add_class('banner-bottom');
            $advertisement_top->add("<a href='" . $url_link . "'><img class='img-responsive banner-top-img' src='$image_url'></a>");
        }

        echo $app->render();
    }

    public function reload_category($cat_id = null) {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $options = array(
            'product_category_id' => $cat_id,
        );
        $url_key = cdbutils::get_value('select url_key from product_category where status > 0 and product_category_id = ' . $db->escape($cat_id));
        $data_product = product::get_result($options);
        $arr = array();
        foreach ($data_product as $key => $value) {
            $arr[] = product::get_product($value->product_id);
        }

        $responsive = array();
        $responsive[] = array(
            'breakpoint' => '480',
            'slide_to_show' => '1',
            'slide_to_scroll' => '1',
            'dots' => 'false',
            'arrow' => 'false',
        );
        $responsive[] = array(
            'breakpoint' => '767',
            'slide_to_show' => '2',
            'slide_to_scroll' => '1',
            'dots' => 'false',
            'arrow' => 'false',
        );

        $container = $app->add_div()->add_class('result-category-wrapper');
        if (count($arr) > 0) {
            $category_deal = $container->add_element('goldenrama-deal', 'category-deal');
            $category_deal->set_slides($arr);
            $category_deal->set_responsive($responsive);
            $category_deal->add_class('category-deal');
            $category_deal->set_using_title(false);
            $container->add_div()->add_class('view-all-wrapper')->add_action()->set_label('View All')->add_class('view-all-btn')->set_link(curl::base() . 'search?keyword=&category=' . $url_key);
        } else {
            $container->add('<span style="color:#ffffff;display:block;padding:20px;">Produk untuk kategori ini sedang kosong</span>');
        }
        echo $app->render();
    }

}
