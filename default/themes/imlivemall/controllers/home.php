<?php

/**
 *
 * @author Riza 
 * @since  Nov 12, 2015
 * @license http://ittron-indonesia.com ITTron Indonesia
 */
class Home_Controller extends ProductController {

    // DEAL TYPE
    private $deal_new_product = "deal_new";
    private $deal_best_sell = "deal_best_sell";
    private $deal_hot = "deal_hot";
    private $deal_special = "deal_special";
    // ADVERTISE 
    private $advertise_1 = 0;
    private $advertise_2 = 1;
    private $advertise_3 = 2;

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $app = CApp::instance();
        $org_id = $this->org_id();

        $responsive = array();
        $responsive[] = array(
            'breakpoint' => '1200',
            'slide_to_show' => '4',
            'slide_to_scroll' => '1',
            'dots' => 'false',
        );
        $responsive[] = array(
            'breakpoint' => '992',
            'slide_to_show' => '3',
            'slide_to_scroll' => '1',
            'dots' => 'false',
        );
        $responsive[] = array(
            'breakpoint' => '869',
            'slide_to_show' => '3',
            'slide_to_scroll' => '1',
            'dots' => 'false',
        );
        $responsive[] = array(
            'breakpoint' => '381',
            'slide_to_show' => '1',
            'slide_to_scroll' => '1',
            'dots' => 'false',
            'arrow' => 'false',
        );

        // SHOPPING CART 
        $app->add_listener('ready')
                ->add_handler('reload')
                ->set_target('shopping-cart')
                ->set_url(curl::base() . "products/shoppingcart/icon_shopping_cart");

        $container = $app->add_div()->add_class('container-floor');
        $container_slide_menu = $container->add_div()->add_class('container-slide-menu');

        // slide
        $slide_list = slide::get_slide($org_id, $this->page());
        $element_slide_show = $container_slide_menu->add_div()->add_class("container container-slide");
        $element_slide = $element_slide_show->add_element('62hall-slide-show', 'element_slide_show');
        $element_slide->set_type('product');
        $element_slide->set_slides($slide_list);

        // DEAL
        $data_hot_deal = deal::get_deal($org_id, $this->page(), 'deal_hot', array('sortby' => 'priority'));
        $data_today_deal = deal::get_deal($org_id, $this->page(), 'deal_best_sell', array('sortby' => 'priority'));
        $deal_new = deal::get_deal($org_id, $this->page(), 'deal_new', array('sortby' => 'priority'));

        $deal = $container->add_div();
        $deal->add_class('bg-white hidden-sm hidden-xs container-deal');
        $deal_container = $deal->add_div()->add_class('container deal');
        $row_deal = $deal_container->add_div()->add_class('container');
        $deal_border = $row_deal->add_div()->add_class('deal-border col-md-12 margin-top-30');

        // New Product Deal
        $deal_border_row = $deal_border->add_div()->add_class('row row-deal row-new-deal');
        $deal_border_col = $deal_border_row->add_div()->add_class('col-md-12');

        $new_product = $deal_border_row->add_div('new')->add_class('link-deal col-sm-3 col-md-3 col-lg-2 upper border-right-gray link')
                ->add('<a href="' . curl::base() . 'show_product/show/deal_new" target="_blank">' . $this->deal_new() . '</a>');
        $deal_border_row = $deal_border->add_div()->add_class('row');
        $deal_border_col = $deal_border_row->add_div()->add_class('');
        $element_new_product = $deal_border_col->add_element('fitgloss-deal', 'new-product');
        $element_new_product->set_slides($deal_new);
        $element_new_product->add_class('livemall-deal');
        $element_new_product->set_center_mode(false);
        $element_new_product->set_slide_to_show(6);
        $element_new_product->set_responsive($responsive);

        // Best Seller Deal
        $deal_border_row = $deal_border->add_div()->add_class('row row-deal row-seller-deal');
        $deal_border_col = $deal_border_row->add_div()->add_class('col-md-12');
        $today_deals = $deal_border_row->add_div('today')->add_class('link-deal col-sm-3 col-md-3 col-lg-2 active upper border-right-gray link')
                ->add('<a href="' . curl::base() . 'show_product/show/deal_best" target="_blank">' . $this->deal_best_sell() . '</a>');

        $deal_border_row = $deal_border->add_div()->add_class('row');
        $deal_border_col = $deal_border_row->add_div()->add_class('');
        $element_new_product = $deal_border_col->add_element('fitgloss-deal', 'today-deal');
        $element_new_product->set_slides($data_today_deal);
        $element_new_product->add_class('livemall-deal');
        $element_new_product->set_center_mode(false);
        $element_new_product->set_slide_to_show(6);
        $element_new_product->set_responsive($responsive);

        // Hot Deal
        $deal_border_row = $deal_border->add_div()->add_class('row row-deal row-hot-deal');
        $deal_border_col = $deal_border_row->add_div()->add_class('col-md-12');
        $hot_deals = $deal_border_row->add_div('hot')->add_class('link-deal col-sm-3 col-md-3 col-lg-2 active upper border-right-gray link')
                ->add('<a href="' . curl::base() . 'show_product/show/deal_hot" target="_blank">HOT DEALS</a>');

        $deal_border_row = $deal_border->add_div()->add_class('row');
        $deal_border_col = $deal_border_row->add_div()->add_class('');
        $element_new_product = $deal_border_col->add_element('fitgloss-deal', 'hot-deal');
        $element_new_product->set_slides($data_hot_deal);
        $element_new_product->add_class('livemall-deal');
        $element_new_product->set_center_mode(false);
        $element_new_product->set_slide_to_show(6);
        $element_new_product->set_responsive($responsive);

        // ADVERTISE
        $data_advertise = advertise::get_advertise($org_id, $this->page());
        $ads_first = carr::get($data_advertise, 0, array());
        $ads_sec = carr::get($data_advertise, 1, array());
        $container_adv = $container->add_div()->add_class('container row-adv');
        $container_adv_col = $container_adv->add_div()->add_class('row');

        foreach ($ads_first as $key => $value) {
            $image_url = carr::get($value, 'image_url');
            $url_link = carr::get($value, 'url_link');
            switch ($key) {
                case 0:
                    $adds_first_content = $container_adv_col->add_div()->add_class('col-lg-4 col-md-4 col-sm-4 col-xs-12');
                    $adds_first_content->add("<a href='" . $url_link . "'><img class='img-responsive adv' src='$image_url'></a>");
                    break;

                case 1:
                    $adds_second_content = $container_adv_col->add_div()->add_class('col-lg-4 col-md-4 col-sm-4 col-xs-12');
                    $adds_second_content->add("<a href='" . $url_link . "'><img class='img-responsive adv' src='$image_url'></a>");
                    break;

                case 2:
                    $adds_second_content = $container_adv_col->add_div()->add_class('col-lg-4 col-md-4 col-sm-4 col-xs-12');
                    $adds_second_content->add("<a href='" . $url_link . "'><img class='img-responsive adv' src='$image_url'></a>");
                    break;

                default:
                    break;
            }
        }

        //Product Categories
        $product_categories = product_category::get_product_category_menu('product', '', $org_id);
       
        $no = 0;

        foreach ($product_categories as $row) {
            $no++;
            $container_pc = $container->add_div()->add_class('container container-category container-category-' . $no);
            $pc_header = $container_pc->add_div()->add_class('category-header');
            $pc_header_background = $pc_header->add_div()->add_class('category-header-background');
            $pc_header_background->add('<span class="glyphicon glyphicon-chevron-right category-control category-control-right hidden-xs hidden-md hidden-lg"></span>');
            $pc_header_background->add('<span class="glyphicon glyphicon-chevron-left category-control category-control-left hidden-xs hidden-md hidden-lg"></span>');

            $pc_title = $pc_header->add_div()->add_class('category-header-title');
            $pc_title->add('<span class="category-no">' . $no . '</span>');
//            $pc_title->add('<span class="category-name"><a href="' . curl::base() . 'product/category/' . $row['url_key'] . '">' . strtoupper($row['name']) . '</a></span>');
            $pc_title->add('<span class="category-name"><a href="search?keyword=&category=' . $row['url_key'] . '">' . strtoupper($row['name']) . '</a></span>');
            $pc_title->add('<span class="category-space">&nbsp;</span>');
            $pc_body = $container_pc->add_div('category-body-' . $no)->add_class('category-body');
            $pc_body->add_listener('ready')->add_handler('reload')->set_target('category-body-' . $no)->set_url(curl::base() . 'home/reload_product_category_body/' . $row['category_id']);
        }

        $app->add_js("
                $('.container-category .category-control.category-control-left').click(function() {
                    var active = $(this).closest('.container-category').find('.category-body .col-category-right.active');
                    var inactive = $(this).closest('.container-category').find('.category-body .col-category-right.inactive');
                    active.removeClass('active').addClass('inactive');
                    inactive.removeClass('inactive').addClass('active');
                })
                $('.container-category .category-control.category-control-right').click(function() {
                    var active = $(this).closest('.container-category').find('.category-body .col-category-right.active');
                    var inactive = $(this).closest('.container-category').find('.category-body .col-category-right.inactive');
                    active.removeClass('active').addClass('inactive');
                    inactive.removeClass('inactive').addClass('active');
                    
                    console.log($(this).closest('.row.row-deal').next());
                })
                ");

        //ADV / BANNER
        foreach ($ads_sec as $k => $v) {
            switch ($k) {
                case 0:
                    $image_url = carr::get($v, 'image_url');
                    $url_link = carr::get($v, 'url_link');

                    $container_adv_sec = $container->add_div()->add_class('container ');
                    $add_sec = $container_adv_sec->add_div()->add_class('container-banner');
                    $add_sec->add("<a href='" . $url_link . "'>
                                <div class='content-adv'>
                                    <div id='mainImage'>
                                        <img class='mainImage img-responsive' src='" . $image_url . "' />
                                    </div>
                                 </div>
                               </a>");
                    break;
                default :
                    break;
            }
        }

        echo $app->render();
    }

    public function reload_product_category_body($product_category_id, $cms_floor_detail_id = '') {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $org_id = CF::org_id();
        $row = cdbutils::get_row('select * from product_category where product_category_id=' . $db->escape($product_category_id));
        $category_image_url = curl::base() . 'cresenity/noimage';
        if (strlen($row->image_name) > 0) {
            $category_image_url = image::get_image_url($row->image_name);
        }
        if (strlen($cms_floor_detail_id) > 0) {
            $category_image_url = cdbutils::get_value('select image_path from cms_floor_detail where cms_floor_detail_id=' . $db->escape($cms_floor_detail_id));
        }

        $org_first_parent = org::get_first_parent_org($org_id);
        $org_first_parent_id = cobj::get($org_first_parent, 'org_id');

        $qproduct = "
                select 
                    * 
                from 
                    product as p 
                    inner join product_category as pc on p.product_category_id=pc.product_category_id 
                    inner join vendor as v on v.vendor_id=p.vendor_id
                where 
                    pc.lft>=" . $db->escape($row->lft) . " 
                    and pc.rgt<=" . $db->escape($row->rgt) . " 
                    and p.status_confirm='CONFIRMED'
                    and p.is_active=1
                    and p.status>0
                    and p.visibility='catalog_search'
                    and p.product_type_id=1
                    and (v.org_id is null or v.org_id=" . $db->escape($org_id) . ")
                    and (p.org_id is null or p.org_id=" . $db->escape($org_id) . ")
            ";
        if (ccfg::get('hide_nostock')) {
            $qproduct.=" and (p.stock>0 or p.is_package_product=1)";
        }
        $qproduct.="
                order by (p.is_hot_item = 1
                    or p.new_product_date >=  " . $db->escape(date('Y-m-d')) . "
                    ) desc, p.updated limit 8";


        $rproduct = $db->query($qproduct);

        $all_product = array();
        foreach ($rproduct as $product_row) {
            $product = product::get_product($product_row->product_id);
            $all_product[] = $product;
        }
        if (isset($_GET['debug'])) {
            cdbg::var_dump($qproduct);
            die;
        }

        $product_no = 0;
        $div_row = $app->add_div()->add_class('row');
        $div_col = $div_row->add_div()->add_class('col-md-4 hidden-sm hidden-xs');
        $div_row_product = $div_col->add_div()->add_class('row row-category-2');
        $list_product = array();
        $list_product_4 = array();
        foreach ($all_product as $product) {
            $product_no++;
            $list_product[] = $product;
            $product_id = !empty($product['product_id']) ? $product['product_id'] : NULL;
            $image_url = !empty($product['image_path']) ? $product['image_path'] : NULL;
            $name = !empty($product['name']) ? $product['name'] : NULL;
            $flag = $hot = ($product['is_hot_item'] > 0) ? 'Hot' : NULL;

            if (!empty($product['new_product_date'])) {
                if (strtotime($product['new_product_date']) >= strtotime(date('Y-m-d'))) {
                    $flag = $new = 'New';
                }
            }

            $promo = !empty($product['promo']) ? $product['promo'] : NULL;
            $stock = !empty($product['stock']) ? $product['stock'] : 0;
            $min_stock = !empty($product['show_min_stock']) ? $product['show_min_stock'] : 0;
            $price = !empty($product['detail_price']) ? $product['detail_price'] : array();
            $is_available = $product['is_available'];
            $product_data_type = $product['product_data_type'];

            $element_product = CElement_IttronmallLivemall_Product::factory();
            $element_product->set_key($product_id)
                    ->set_image($image_url)
                    ->set_column(1)
                    ->set_name($name)
                    ->set_flag($flag)
                    ->set_label_flag($flag)
                    ->set_price($price)
                    ->set_stock($stock)
                    ->set_min_stock($min_stock)
                    ->set_available($is_available)
                    ->set_size('166x155');

            $element_html = $element_product->html();

            $div_row_product->add_div()->add_class('col-md-6 col-sm-6 col-xs-12 products-categories')->add_div()->add_class('')->add($element_html);
            if ($product_no <= '4') {
                $list_product_4[] = $element_html;
            }
            if ($product_no == '4') {
                $div_col = $div_row->add_div()->add_class('col-category-image col-md-4 col-sm-6 col-xs-12');
                $div_col->add('<a href="' . curl::base() . 'product/category/' . $row->product_category_id . '">' . '<img src="' . $category_image_url . '" class="category-image"/>' . '</a>');
                //add_img()->set_src($category_image_url)->add_class('category-image');
                $div_col = $div_row->add_div()->add_class('col-md-4 col-sm-6 col-xs-12 hidden-xs active col-category-right');
                $div_row_product = $div_col->add_div()->add_class('row row-category-2 hidden-xs');
                $div_col_cloned = $div_row->add_div()->add_class('col-md-4 col-sm-6 col-xs-12 hidden-xs col-category-right inactive col-category-right-cloned');
                $div_row_product_cloned = $div_col_cloned->add_div()->add_class('row row-category-2 hidden-xs');
                foreach ($list_product_4 as $html) {
                    $div_row_product_cloned->add_div()->add_class('col-lg-12 col-md-6 col-sm-6 col-xs-12')->add_div()->add_class('row')->add($html);
                }
            }
        }
        $div_row = $app->add_div()->add_class('row hidden-sm hidden-md hidden-lg');
        $div_col = $div_row->add_div()->add_class('col-xs-12');
        $elm = $div_col->add_element('livemall-slide', 'category');
        $elm->set_list_item($list_product);
//        $elm = $div_col->add_element('fitgloss-deal', 'category');
//        $elm->set_slides($list_product);
        if (isset($_GET['debug'])) {
            cdbg::var_dump($list_product);
            die;
        }
        echo $app->render();
    }

}
