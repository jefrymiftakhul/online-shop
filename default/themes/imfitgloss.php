<?php

return array(
    "theme_path" => "imfitgloss",
    "client_modules" => array(
        'bootstrap-3.3.5',
        'jquery',
        'chosen',
        'select2',
        'font-awesome',
        'bootstrap-dropdown',
        'zoomsl-3.0',
        'fancybox',
        'flexslider',
        'jquery.history',
        'touchspin',
        'touchswipe',
        'bootstrap-slider',
        'validation',
        'owl_carousel',
        'swiper',
        'slick',
        'Imlasvegas',
        'imfitgloss',
    ),
    "js" => array(
    ),
    "css" => array(
    ),
    "config" => array(
    ),
);
