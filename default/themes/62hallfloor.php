<?php

return array(
    "client_modules" => array(
        '62hallfamily',
        'bootstrap-3.3.5',
        'jquery',
        'chosen',
        'select2',
        'font-awesome',
        'bootstrap-dropdown',
        'zoomsl-3.0',
        'fancybox',
        'flexslider',
        'jquery.history',
        'touchspin',
        'touchswipe',
        'bootstrap-slider',
        'validation',
        'owl_carousel',
        '62hallfloor',
        'slick',
    ),
    "js" => array(
    ),
    "css" => array(
    ),
    "config" => array(
    ),
);
