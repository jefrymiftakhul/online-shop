<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Apr 21, 2016
     * @license http://ittron.co.id ITtron
     */
    class CIttronMallElement_Auth_Register extends CIttronMallElement {

        protected $title;
        protected $modal;
        protected $name;
        protected $email;
        protected $handphone;
        protected $register_gender;
        protected $subscribe_product;
        protected $subscribe_gold;
        protected $subscribe_service;
        protected $birthdate;

        public function __construct($id = "", $tag = "div") {
            parent::__construct($id, $tag);

            $this->title = true;
            $this->modal = true;
            $this->register_gender = 'male';
        }

        public static function factory($id = "", $tag = "div") {
            return new CIttronMallElement_Auth_Register($id, $tag);
        }

        public function html($indent = 0) {
            $html = CStringBuilder::factory();

            $container = $this->add_div();
            if ($this->modal == true) {
                $container = $this->add_control('modal-' . $this->id, 'ittronmall-modal');
            }
            if ($this->title) {
                $container->add_div()->add_class('os-modal-title')->add(clang::__('Register'));
            }

            $content = $container->add_div('register-modal-content-' . $this->id)->add_class('os-modal-content');
            $form = $content->add_form()->add_class('ost-form');

            $div_main = $form->add_div();
            $div_main->add_field()->set_label(clang::__('Nama Lengkap') .'<span class="red"> *</span>')->add_control('', 'text')
                    ->set_placeholder(clang::__('Nama Lengkap'))->set_name('register_name')
                    ->set_value($this->name);
            $div_main->add_field()->set_label(clang::__('Email').'<span class="red"> *</span>')->add_control('', 'text')
                    ->set_placeholder(clang::__('Ex. tokoonline@gmail.com'))->set_name('register_email')
                    ->set_value($this->email);
            $div_main->add_field()->set_label(clang::__('Password').'<span class="red"> *</span>')->add_control('', 'password')
                    ->set_placeholder(clang::__('Password'))->set_name('register_password');
            $div_main->add_field()->set_label(clang::__('Ulangi Password').'<span class="red"> *</span>')->add_control('', 'password')
                    ->set_placeholder(clang::__('Ulangi Password'))->set_name('register_retype_password');
            $div_main->add_field()->set_label(clang::__('No. Handphone').'<span class="red"> *</span>')->add_control('', 'text')
                    ->set_placeholder(clang::__('Ex. +628573169779'))->set_name('register_handphone')
                    ->set_value($this->handphone);
            $div_main->add_field()->set_label(clang::__('Tanggal Lahir').'<span class="red"> *</span>')->add_control($this->id . '-birthdate', 'ittronmall-date-dropdown')
                    ->add_class('ost-date-dropdown')->set_name($this->id . '_date_birth')
                    ->set_value($this->birthdate);

            $field_gender = $div_main->add_field();
            $male = $field_gender->add_control('', 'radio')->set_label_wrap(true)->set_name('register_gender')
                    ->set_value('male')->set_inline(true)->set_label(clang::__('Pria'));
            $female = $field_gender->add_control('', 'radio')->set_label_wrap(true)->set_name('register_gender')
                    ->set_value('female')->set_inline(true)->set_label(clang::__('Wanita'));
            if ($this->register_gender == 'male') {
                $male->set_checked(true);
            }
            else if ($this->register_gender == 'female') {
                $female->set_checked(true);
            }
            $org = org::get_info();
            $domain = carr::get($org, 'domain');
            $div_term_condition = $div_main->add_div()->add_class('term-condition')->add(clang::__('Dengan menekan Daftar akun, saya mengkonfirmasi telah menyetujui') . '<br>' .
                    '<a class="os-link" href="' . curl::base() . 'read/page/index/syarat-dan-ketentuan">' . clang::__('Syarat dan Ketentuan') . '</a> ' . clang::__('serta ')
                    . ' <a class="os-link" href="' . curl::base() . 'read/page/index/kebijakan-privasi">' . clang::__('Kebijakan Privasi') . '</a> ' . $domain);

            $param_input = array(
                'register_name' => "input[name=\'register_name\']",
                'register_email' => "input[name=\'register_email\']",
                'register_password' => "input[name=\'register_password\']",
                'register_retype_password' => "input[name=\'register_retype_password\']",
                $this->id . '_date_birth' => "input[name=\'" .$this->id ."_date_birth\']",
                'register_handphone' => "input[name=\'register_handphone\']",
                'register_gender' => "input[name=\'register_gender\']:checked",
                'subscribe_product' => "input[name=\'subscribe_product\']",
                'subscribe_gold' => "input[name=\'subscribe_gold\']",
                'subscribe_service' => "input[name=\'subscribe_service\']",
            );

            if ($this->modal == true) {
                $footer = $container->add_footer();
            }
            
            $row_footer = $div_main->add_div()->add_class('row');
            $button_container = $row_footer->add_div()->add_class('col-md-12 col-xs-12 txt-right');
            $action = $button_container->add_action()->set_label(clang::__('Daftar'))->set_submit(false)
                    ->add_class('ost-btn-submit');
            $action->add_listener('click')->add_handler('reload')->set_target('register-modal-content-' . $this->id)
                    ->add_param_input_by_name($param_input)
                    ->set_url(curl::base() . 'authentication/register')->set_method('post');

            $html->append(parent::html($indent));
            return $html->text();
        }

        public function js($indent = 0) {
            $js = CStringBuilder::factory();

            $js->append(parent::js($indent));
            return $js->text();
        }

        public function get_title() {
            return $this->title;
        }

        public function get_modal() {
            return $this->modal;
        }

        public function get_name() {
            return $this->name;
        }

        public function get_email() {
            return $this->email;
        }

        public function get_handphone() {
            return $this->handphone;
        }

        public function get_subscribe_product() {
            return $this->subscribe_product;
        }

        public function get_subscribe_gold() {
            return $this->subscribe_gold;
        }

        public function get_subscribe_service() {
            return $this->subscribe_service;
        }

        public function set_title($title) {
            $this->title = $title;
            return $this;
        }

        public function set_modal($modal) {
            $this->modal = $modal;
            return $this;
        }

        public function set_name($name) {
            $this->name = $name;
            return $this;
        }

        public function set_email($email) {
            $this->email = $email;
            return $this;
        }

        public function set_handphone($handphone) {
            $this->handphone = $handphone;
            return $this;
        }

        public function set_subscribe_product($subscribe_product) {
            $this->subscribe_product = $subscribe_product;
            return $this;
        }

        public function set_subscribe_gold($subscribe_gold) {
            $this->subscribe_gold = $subscribe_gold;
            return $this;
        }

        public function set_subscribe_service($subscribe_service) {
            $this->subscribe_service = $subscribe_service;
            return $this;
        }

        public function get_register_gender() {
            return $this->register_gender;
        }

        public function set_register_gender($register_gender) {
            $this->register_gender = $register_gender;
            return $this;
        }

        public function get_birthdate() {
            return $this->birthdate;
        }

        public function set_birthdate($birthdate) {
            $this->birthdate = $birthdate;
            return $this;
        }

                
    }
    