<?php

    /**
     *
     * @author Seians0077
     * @since  Apr 18, 2016
     * @license http://piposystem.com Piposystem
     */
    class CIttronMallElement_Auth_Forgot extends CIttronMallElement {

        protected $modal;
        protected $email;
        protected $title;

//        protected $url_redirect;

        public function __construct($id = "", $tag = "div") {
            parent::__construct($id, $tag);

            $this->modal = true;
            $this->email = '';
            $this->title = true;
//            $this->url_redirect = null;
        }

        public static function factory($id = '') {
            return new CIttronMallElement_Auth_Forgot($id);
        }

        public function html($indent = 0) {
            $html = new CStringBuilder();
            $html->set_indent($indent);

            $container = $this->add_div();

            if ($this->modal == true) {
                $container = $this->add_control('modal-' . $this->id, 'ittronmall-modal');
            }
            if ($this->title) {
                $container->add_div()->add_class('os-modal-title')->add(clang::__('Lupa Kata Sandi'));
            }

            $content = $container->add_div('forgot-modal-content-' . $this->id)->add_class('os-modal-content');

            $form = $content->add_form()->add_class('ost-form');
            $desc = $form->add_div();
            $desc->add(clang::__('Silahkan masukan alamat email Anda untuk mendapatkan password baru') .'<br/>');
            $form->add_field()->set_label(clang::__('Email'))->add_control('', 'text')
                    ->set_value($this->email)->set_placeholder(clang::__('Email'))
                    ->set_name('forgot_email_login');

            
            // footer - button reset password
            $footer = $content->add_div()->add_class('button-forgot');
            $param_input = array('forgot_email_login' => "input[name=\'forgot_email_login\']");
            $action = $footer->add_action()->set_label(clang::__('Reset Password'))->set_submit(false)
                    ->add_class('ost-btn-submit pull-right');
            $action->add_listener('click')->add_handler('reload')->set_target('forgot-modal-content-' . $this->id)
                    ->add_param_input_by_name($param_input)
                    ->set_url(curl::base() . 'authentication/forgot')->set_method('post');
            $content->add_div()->add_class('clear-both');

            if ($this->modal == true) {
                $container->add_footer();
            }

            $html->append(parent::html($indent));
            return $html->text();
        }

        public function js($indent = 0) {
            $js = new CStringBuilder();
            $js->set_indent($indent);
            $js->append(parent::js($indent));
            return $js->text();
        }

        function get_modal() {
            return $this->modal;
        }

        function set_modal($modal) {
            $this->modal = $modal;
            return $this;
        }

        function get_email() {
            return $this->email;
        }

        function set_email($email) {
            $this->email = $email;
            return $this;
        }

        function get_title() {
            return $this->title;
        }

        function set_title($title) {
            $this->title = $title;
            return $this;
        }

    }
    