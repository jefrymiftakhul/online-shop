<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Mar 22, 2016
     */
    class CIttronMallElement_Auth_Login extends CIttronMallElement {

        protected $modal;
        protected $email;
        protected $password;
        protected $title;
        protected $radio_option;
        protected $guest_button;
        protected $ask_to_register;
        protected $url_redirect;

        public function __construct($id = "", $tag = "div") {
            parent::__construct($id, $tag);

            $this->title = true;
            $this->modal = true;
            $this->email = '';
            $this->password = '';
            $this->radio_option = false;
            $this->guest_button = false;
            $this->ask_to_register = false;
            $this->url_redirect = null;
        }

        public static function factory($id = '') {
            return new CIttronMallElement_Auth_Login($id);
        }

        public function html($indent = 0) {
            $html = new CStringBuilder();
            $html->set_indent($indent);

            $container = $this->add_div();
            if ($this->modal == true) {
                $container = $this->add_control('modal-' . $this->id, 'ittronmall-modal')
                        ->add_class('modal-login');
            }
            if ($this->title) {
                $container->add_div()->add_class('os-modal-title')->add(clang::__('Login'));
            }
            $content = $container->add_div('login-modal-content_' . $this->id)->add_class('os-modal-content');
            $form = $content->add_form()->add_class('ost-form');

            if ($this->radio_option) {
                $form->add_control('', 'radio')->set_label(clang::__('Saya checkout sebagai tamu'))
                        ->set_inline(true)->set_label_wrap(true)->set_name('member_status');
                $form->add_control('', 'radio')->set_label(clang::__('Saya sudah jadi member di Cucigu'))
                        ->set_inline(true)->set_label_wrap(true)->set_name('member_status')->set_checked(true);
            }

            $form->add_field()->set_label(clang::__('Email'))->add_control('', 'text')
                    ->set_value($this->email)->set_placeholder(clang::__('Email'))
                    ->set_name('email_login');
            $password = $form->add_field()->set_label(clang::__('Password'))->add_control('', 'password')
                    ->set_value($this->password)->set_placeholder(clang::__('Password'))
                    ->set_name('password_login');


            $row_login_link = $form->add_div()->add_class('row');
            $left_footer = $row_login_link->add_div()->add_class('col-md-6 col-xs-6 link-reg-member');

            if ($this->ask_to_register) {
                $left_footer->add(clang::__('Belum jadi member ?')
                        . ' <a href="javascript:;" id="btn-open-reg-dialog" class="btn-open-reg-dialog" data-toggle="modal" data-target="#modal-register-trx">'
                        . clang::__('Daftar Sekarang!') . '</a>');
            }

            $right_footer = $row_login_link->add_div()->add_class('col-md-6 col-xs-6 txt-right');
            $right_footer->add('<div>');
            $right_footer->add('   <span>');
            $right_footer->add('       <a href="#" class="os-link" data-toggle="modal" data-target="#modal-forgot">' . clang::__('Lupa Kata Sandi?') . '</a>');
            $right_footer->add('   </span>');
            $right_footer->add('</div>');

            
            if ($this->modal == true) {
                $footer = $container->add_footer();
            }

            $param_input = array('email_login' => "#login-modal-content_" . $this->id ." input[name=\'email_login\']",
                'password_login' => "#login-modal-content_" . $this->id ." input[name=\'password_login\']");
            if (strlen($this->url_redirect) > 0) {
                $form->add_control('', 'hidden')->set_value($this->url_redirect)->set_name('redirect');
                $param_input['redirect'] = "input[name=\'redirect\']";
            }
            $data_addition = '';
            foreach ($param_input as $k => $inp) {
                if (strlen($data_addition) > 0) {
                    $data_addition.=',';
                }
                $data_addition.="'" . $k . "':$.cresenity.value('" . $inp . "')";
            }
            $data_addition = '{' . $data_addition . '}';
            if ($this->modal == true) {
                $content->add_js("
                    jQuery('body').on('keypress', '#login-modal-content_" . $this->id . " input[name=\'password_login\']', function(event){
                        if (event.which == 13) {
                            event.preventDefault();
                            $.cresenity.reload('login-modal-content_" . $this->id . "','" . curl::base() . "authentication/login','post'," .$data_addition .");
                        }
                    })
                    ");
            }
            
            $row_button_login_store = $form->add_div()->add_class('row btn-login-wrapper');
            if ($this->guest_button) {
                $button_container = $row_button_login_store->add_div()->add_class('col-md-6 col-xs-6');
                $button_guest_container = $row_button_login_store->add_div()->add_class('col-md-6 col-xs-6');
            }
            else {
                $button_container = $row_button_login_store->add_div()->add_class('col-md-12 col-xs-12');
            }
            $action = $button_container->add_action()->set_label(clang::__('Masuk'))->set_submit(false)
                    ->add_class('ost-btn-submit btn-login-store');
            $action->add_listener('click')->add_handler('reload')->set_target('login-modal-content_' . $this->id)
                    ->add_param_input_by_name($param_input)
                    ->set_url(curl::base() . 'authentication/login')->set_method('post');
            
            if ($this->guest_button) {
                $button_guest_container->add_action()->set_label(clang::__('Masuk Sebagai Tamu'))->set_submit(false)
                        ->set_link($this->url_redirect)
                        ->add_class('ost-btn-submit btn-login-guest');
            }
            
            $have_login_socmed = false;
            
            $login_fb = ccfg::get('login_facebook');
            $login_gmail = ccfg::get('login_gmail');

            if ($login_fb || $login_gmail) {
                $have_login_socmed = true;
            }
            
            $row_button_socmed = $form->add_div()->add_class('row btn-login-wrapper');
            if ($have_login_socmed) {
                // show separator line
                $line_socmed_wrapper = $row_button_socmed->add_div()->add_class('col-md-12 col-xs-12');
                $line_wrapper = $line_socmed_wrapper->add_div()->add_class('line-wrapper');
                $line_wrapper->add_div()->add_class('line')->add('&nbsp;');
                $line_wrapper->add_div()->add_class('txt-center line-label')
                        ->add_div()->add_class('line-text')->add(clang::__('Atau'));
                
                $socmed_container = $row_button_socmed->add_div()->add_class('col-md-12 col-xs-12 txt-center');
                if ($login_fb) {
                    // show fb button
                    $login_facebook = $socmed_container->add_action()
                        ->set_label('<i class="fa fa-facebook-square social-media"></i>'
                            .clang::__('Login with Facebook'))
                        ->add_class("btn-login-fb")
                        ->set_submit(false);
                }

                if ($login_gmail) {
                    // show gmail buttom
                }
            }

            $html->append(parent::html($indent));
            return $html->text();
        }

        public function js($indent = 0) {
            $js = new CStringBuilder();
            $js->set_indent($indent);
            $js->append(parent::js($indent));
            
//            echo '<textarea>';
//            echo $js->text();
//            echo '</textarea>';
            return $js->text();
        }

        function get_modal() {
            return $this->modal;
        }

        function set_modal($modal) {
            $this->modal = $modal;
            return $this;
        }

        function get_email() {
            return $this->email;
        }

        function get_password() {
            return $this->password;
        }

        function set_email($email) {
            $this->email = $email;
            return $this;
        }

        function set_password($password) {
            $this->password = $password;
            return $this;
        }

        function get_title() {
            return $this->title;
        }

        function set_title($title) {
            $this->title = $title;
            return $this;
        }

        function get_radio_option() {
            return $this->radio_option;
        }

        function set_radio_option($radio_option) {
            $this->radio_option = $radio_option;
            return $this;
        }

        function get_guest_button() {
            return $this->guest_button;
        }

        function set_guest_button($guest_button) {
            $this->guest_button = $guest_button;
            return $this;
        }

        function get_ask_to_register() {
            return $this->ask_to_register;
        }

        function set_ask_to_register($ask_to_register) {
            $this->ask_to_register = $ask_to_register;
            return $this;
        }

        function set_url_redirect($url_redirect) {
            $this->url_redirect = $url_redirect;
            return $this;
        }

    }
    