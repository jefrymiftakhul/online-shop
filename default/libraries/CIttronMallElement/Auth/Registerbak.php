<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Mar 22, 2016
     */
    class C62HallElement_Auth_Register extends C62HallElement {

        protected $using_dialog;
        protected $show_dialog;
        protected static $instance = null;

        public function __construct($id = "", $tag = "div") {
            parent::__construct($id, $tag);

            $this->using_dialog = true;
            $this->show_dialog = false;
        }

        public static function instance($id = '') {
            if (self::$instance == null) {
                self::$instance = new C62HallElement_Auth_Register($id);
            }
            return self::$instance;
        }

        public static function factory($id = '') {
            return new C62HallElement_Auth_Register($id);
        }

        public function html($indent = 0) {
            $html = new CStringBuilder();
            $html->set_indent($indent);

            $name = '';
            $email = '';
            $password = '';
            $retype_password = '';
            $handphone = '';

            $modal = $this->add_control('modal-' .$this->id, '62hall-modal');
            $modal->add_div()->add_class('os-modal-title')->add(clang::__('Register'));
            $form = $modal->add_form()->add_class('ost-form')->set_action(curl::base() . 'authentication/register');
            $div_main = $form->add_div();
            $div_main->add_field()->set_label(clang::__('Nama Lengkap'))->add_control('', 'text')->set_value($name)
                    ->set_placeholder(clang::__('Name'))->set_name('register_name');
            $div_main->add_field()->set_label(clang::__('Email'))->add_control('', 'text')->set_value($email)
                    ->set_placeholder(clang::__('Email'))->set_name('register_email');
            $div_main->add_field()->set_label(clang::__('Password'))->add_control('', 'password')->set_value($password)
                    ->set_placeholder(clang::__('Password'))->set_name('register_password');
            $div_main->add_field()->set_label(clang::__('Ulangi Password'))->add_control('', 'password')->set_value($retype_password)
                    ->set_placeholder(clang::__('Ulangi Password'))->set_name('register_retype_password');
            $div_main->add_field()->set_label(clang::__('No. Handphone'))->add_control('', 'text')->set_value($handphone)
                    ->set_placeholder(clang::__('Handphone'))->set_name('register_handphone');
            $div_main->add_field()->set_label(clang::__('Tanggal Lahir'))->add_control($this->id .'-birthdate', '62hall-date-dropdown')
                    ->add_class('ost-date-dropdown')->set_name($this->id .'_date_birth');

            $field_gender = $div_main->add_field();
            $field_gender->add_control('', 'radio')->set_label_wrap(true)->set_name('register_gender')->set_value('male')->set_inline(true)->set_label(clang::__('Pria'));
            $field_gender->add_control('', 'radio')->set_label_wrap(true)->set_name('register_gender')->set_value('female')->set_inline(true)->set_label(clang::__('Wanita'));
            $org = org::get_info();
            $domain = carr::get($org, 'domain');
            $div_main->add_control('', 'checkbox')->set_name('subscribe_product')->set_label_wrap(true)->set_label(clang::__('Saya ingin menerima penawaran produk ') . $domain . clang::__(' melalui email'));
            $div_main->add_control('', 'checkbox')->set_name('subscribe_gold')->set_label_wrap(true)->set_label(clang::__('Saya ingin menerima penawaran gold ') . $domain . clang::__(' melalui email'));
            $div_main->add_control('', 'checkbox')->set_name('subscribe_service')->set_label_wrap(true)->set_label(clang::__('Saya ingin menerima penawaran jasa ') . $domain . clang::__(' melalui email'));
            $div_term_condition = $div_main->add_div()->add_class('term-condition')->add(clang::__('Dengan menekan Daftar akun, saya mengkonfirmasi telah menyetujui') . '<br>' .
                    '<a class="ost-link" href="' . curl::base() . 'read/page/index/syarat-dan-ketentuan">' . clang::__('Syarat dan Ketentuan') . '</a> ' . clang::__('serta ') 
                    . ' <a class="ost-link" href="' . curl::base() . 'read/page/index/kebijakan-privasi">' . clang::__('Kebijakan Privasi') . '</a> ' . $domain);
            $footer = $modal->add_footer();
            $footer->add_action()->set_label(clang::__('Daftar'))->set_submit(true)->set_type('button')->add_class('ost-btn-submit');

            $html->append(parent::html($indent));
            return $html->text();
        }

        public function js($indent = 0) {
            $js = new CStringBuilder();
            $js->set_indent($indent);
            $js->append(parent::js($indent));
            return $js->text();
        }

    }
    