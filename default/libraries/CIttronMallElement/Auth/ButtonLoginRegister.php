<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Mar 22, 2016
     */
    class CIttronMallElement_Auth_ButtonLoginRegister extends CIttronMallElement {

        public static $instance = null;
        private $login = false;
        private $register = false;
        private $member_menu;

        public function __construct($id = "", $tag = "div") {
            parent::__construct($id, $tag);
            
            $this->member_menu = array(
                'my_account' => array(
                    'label' => clang::__('Akun'),
                    'url' => curl::base() .'member/profile'
                ),
                'my_order' => array(
                    'label' => clang::__('Pesanan Saya'),
                    'url' => curl::base() .'member/myOrder'
                ),
                'logout' => array(
                    'label' => clang::__('Keluar'),
                    'url' => curl::base() .'authentication/logout'
                )
            );
        }

        public static function factory($id = '') {
            return new CIttronMallElement_Auth_ButtonLoginRegister($id);
        }

        public static function instance($id = '') {
            if (self::$instance == null) {
                self::$instance = new C62HallElement_Auth_ButtonLoginRegister($id);
            }
            return self::$instance;
        }

        public function html($indent = 0) {
            $html = new CStringBuilder();
            $html->set_indent($indent);

            $session = Session::instance();
            if ($session->get('member_id')) {
                $html->appendln('<span class="head-navbar-item">');
                $html->appendln('   <span>');
                $html->appendln(        '<span class="hidden-xs">' .clang::__('Welcome') .',</span>');
                
                $content = '
                            <div class="txt-left">
                                <ul>';
//                                    <li><a href="#">' .clang::__('My Account') .'</a></li>
//                                    <li><a href="#">' .clang::__('My Order') .'</a></li>
//                                    <li><a href="#">' .clang::__('Logout') .'</a></li>
                foreach ($this->member_menu as $menu_k => $menu_v) {
                    $label = carr::get($menu_v, 'label');
                    $url = carr::get($menu_v, 'url');
                    $content .= '<li><a href="' .$url .'">' .$label .'</a></li>';
                }
                $content .= '</ul>
                            </div>
                            ';
                $label = '<span class="hidden-xs"><a href="#" class="ost-navbar-btn-link">' 
                            . $session->get('name') . '</a></span>';
                $label .= '<span class="os os-auth link-btn-auth hidden-sm hidden-lg hidden-md"></span>';
                $popover_html = C62HallFormInput_Popover::factory('popover-account')
                                                    ->set_label($label)
                                                    ->set_content($content)
                                                    ->html();
                $html->appendln($popover_html);
                $html->appendln('   </span>');
                $html->appendln('</span>');
            }
            else {
                if ($this->login) {
                    $html->appendln('<span class="head-navbar-item hidden-xs">');
                    $html->appendln('   <span>');
                    $html->appendln('       <a href="#" class="ost-navbar-btn-link btn-login" data-toggle="modal" data-target="#modal-login">' . clang::__('Login') . '</a>');
                    $html->appendln('   </span>');
                    $html->appendln('</span>');
                }
    //            $html->appendln(' | ');
                if ($this->register) {
                    $html->appendln('<span class="head-navbar-item hidden-xs">');
                    $html->appendln('   <span>');
                    $html->appendln('       <a href="#" class="ost-navbar-btn-link btn-register" data-toggle="modal" data-target="#modal-register">' . clang::__('Register') . '</a>');
                    $html->appendln('   </span>');
                    $html->appendln('</span>');
                }
                
                $html->appendln('<span class="head-navbar-item hidden-sm hidden-lg hidden-md">');
                $html->appendln('   <span>');
                
                $label = '<span class="os os-auth link-btn-auth"></span>';
                $content = '<div class="txt-left">';
                $content .= '   <ul>';
                if ($this->login) {
                    $content .= '       <li><a href="#" class="ost-navbar-btn-link btn-login" data-toggle="modal" data-target="#modal-login">' . clang::__('Login') . '</a></li>';
                }
                if ($this->register) {
                    $content .= '       <li><a href="#" class="ost-navbar-btn-link btn-register" data-toggle="modal" data-target="#modal-register">' . clang::__('Register') . '</a></li>';
                }
                $content .= '   </ul>';
                $content .= '</div>';
                $popover_html = C62HallFormInput_Popover::factory('popover-auth')
                                                    ->set_label($label)
                                                    ->set_content($content)
                                                    ->html();
                $html->appendln($popover_html);
                
                $html->appendln('   </span>');
                $html->appendln('</span>');
            }



            $html->append(parent::html($indent));
            return $html->text();
        }

        public function js($indent = 0) {
            $js = new CStringBuilder();
            $js->set_indent($indent);
            $js->append(parent::js($indent));
            return $js->text();
        }

        function get_login() {
            return $this->login;
        }

        function get_register() {
            return $this->register;
        }

        function set_login($login) {
            $this->login = $login;
            return $this;
        }

        function set_register($register) {
            $this->register = $register;
            return $this;
        }

    }
    