<?php

class C62HallElement_Search extends C62HallElement{
    
    private $keyword = NULL;
    private $type = "product";

    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public static function factory($id = ''){
        return new C62HallElement_Search($id);
    }
    
    public function set_keyword($keyword){
        $this->keyword = $keyword;
        return $this;
    }
    
    public function set_type($type){
        $this->type = $type;
        return $this;
    }
    
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        
        $request = $_GET;
        $q = carr::get($request, 'q');
        
        $form = $this->add_form()->add_class('ost-form')->set_method('GET')
                ->set_action(curl::base() . 'product/search');
        $search = $form->add_div();
        $input = $search->add_div();
        $button = $search->add_div();
        
//        $page = $input->add_control('page','hidden')->set_value($this->type);
        $placeholder = clang::__('Cari produk') .' ...';
        if ($this->type == 'service') {
            $placeholder = clang::__('Cari jasa') .' ...';
        }
        else if ($this->type == 'gold') {
            $placeholder = clang::__('Cari gold') .' ...';
        }
        
        $keyword = $input->add_field()
                    ->add_control("keyword", 'text')
                    ->set_name("q")->set_input_style('input-group')
                    ->set_button_position('right')->set_value($q)
                    ->set_placeholder($placeholder);
        $keyword->add_action('qs')->set_icon('search')->set_submit(true)->set_value('1')
                ->set_button(true)->add_class('ost-btn-submit');
        
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent=0) {
        $js = new CStringBuilder();
       
        $js->append(parent::js($indent));
        return $js->text();
    }
}
