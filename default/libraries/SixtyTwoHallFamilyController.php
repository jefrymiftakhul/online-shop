<?php

class SixtyTwoHallFamilyController extends CController {

    protected $_org_id = NULL;
    protected $_org_code = NULL;
    protected $_data_org;
    protected $_get_org_code;
    protected $_merchant_category=null;
    protected $_domain = NULL;
    protected $_have_product = NULL;
    protected $_have_gold = NULL;
    protected $_have_service = NULL;
    protected $_have_shinjuku = NULL;
    protected $_have_namecard = NULL;
    protected $_have_luckyday = NULL;
    protected $_have_pulsa = NULL;
    protected $_have_prelove = NULL;
    protected $_have_lokal = NULL;
    protected $_have_grosir = NULL;
    protected $_have_digital = NULL;
    protected $_have_voucher = NULL;
    protected $_have_event = NULL;
    protected $_have_k24 = NULL;
    protected $_have_zopim = NULL;
    protected $_zopim_id = '';
    protected $_have_register = NULL;
    protected $_have_deposit = NULL;
    protected $_have_topup_transfer = NULL;
    protected $_have_member_get_member = NULL;
    protected $_page='';
    protected $_list_menu = array();
    protected $_menu_list = array();

    public function __construct($api=null) {
        
        $link_menu = ccfg::get('link_menu');
        parent::__construct();
         
        $app = CApp::instance();
        $this->_org_id = CF::org_id();
        if($api==null){
            $no_web_front=ccfg::get('no_web_front');
            if($no_web_front==1){
                die('Not Found');
            }
        }
        $org = org::get_org($this->_org_id);
        if ($org) {
            $this->_domain = $org->domain;
            $this->_store = $org->name;
        }
        $r = org::get_org($this->_org_id);
        if ($r) {
            $this->_have_product = $r->have_product;
            $this->_have_gold = $r->have_gold;
            $this->_have_service = $r->have_service;
            $this->_have_shinjuku = $r->have_shinjuku;
            $this->_have_namecard = $r->have_namecard;
            $this->_have_luckyday = $r->have_luckyday;
            $this->_have_pulsa = $r->have_pulsa;
            $this->_have_zopim = $r->have_zopim;
            $this->_zopim_id = $r->zopim_id;
            $this->_have_register = $r->have_register;
            $this->_have_prelove = $r->have_prelove;
            $this->_have_voucher = $r->have_voucher;
            // $this->_have_event = $r->have_event;
            // $this->_have_k24 = $r->have_k24;
            $this->_have_lokal = $r->have_lokal;
            $this->_have_grosir = $r->have_grosir;
            $this->_have_deposit = $r->have_deposit;
            $this->_have_topup_transfer = $r->have_topup_transfer;
            $this->_have_member_get_member = $r->have_member_get_member;
            $this->_org_code = $r->code;
            $this->_merchant_category = $r->merchant_category;
            $this->_have_digital = cobj::get($r, 'have_digital');
        }
        $this->_data_org = $r;

        $this->_get_org_code = $r->code;
        if ($r->code == "62HallFamily") {
            $this->_get_org_code = "default";
        }

        if ($this->_org_id == null) {
            $this->_have_product = 1;
            $this->_have_gold = 0;
            $this->_have_service = 0;
            $this->_have_shinjuku = 0;
            $this->_have_namecard = 0;
            $this->_have_luckyday = 0;
            $this->_have_prelove = 0;
            $this->_have_voucher = 0;
            $this->_have_event = 0;
            $this->_have_lokal = 0;
            $this->_have_grosir = 0;
            $this->_have_pulsa = 0;
            $this->_have_k24 =0;
            $this->_have_zopim = 0;
            $this->_zopim_id = '';
            $this->_have_register = 0;
            $this->_have_deposit = 0;
            $this->_have_topup_transfer = 0;
            $this->_have_member_get_member = 0;
            $this->_have_digital = 0;
        }
        if ($this->_have_product > 0) {
            if($this->_page!='product'){
                $this->_list_menu[] = array(
                    'label' => 'HOME',
                    'url' => '/home',
                );
            }
        }
        if ($this->_have_gold > 0) {
            if($this->_page!='gold'){
                $this->_list_menu[] = array(
                    'label' => 'GOLD',
                    'url' => '/gold/home',
                );
            }
        }
        if ($this->_have_service > 0) {
            if($this->_page!='service'){
                $this->_list_menu[] = array(
                    'label' => 'JASA',
                    'url' => '/service/home',
                );
            }
        }
        if($this->_have_shinjuku>0){
            if($this->_page!='shinjuku'){
                $this->_list_menu[]= array(
                     'label' => 'SHINJUKU',
                     'url' => '/shinjuku/home',
                );
            }
        }
        if($this->_have_namecard>0){
            if($this->_page!='namecard'){
                $this->_list_menu[]= array(
                     'label' => 'NAME CARD',
                     'url' => '/namecard/home',
                );
            }
        }
        if($this->_have_luckyday>0){
            if($this->_page!='luckyday'){
                $this->_list_menu[]= array(
                    'label' => 'LUCKY DAY',
                    'url' => '/luckyday/home',
                );
            }
        }
        if($this->_have_pulsa>0){
            if($this->_page!='pulsa'){
                $this->_list_menu[]= array(
                    'label' => 'PULSA',
                    'url' => '/pulsa/home',
                );
            }
        }
        if($this->_have_prelove>0){
            if($this->_page!='prelove'){
                $this->_list_menu[]= array(
                    'label' => 'PRELOVE',
                    'url' => '/prelove/home',
                );
            }
        }
        if($this->_have_lokal>0){
            if($this->_page!='lokal'){
                $this->_list_menu[]= array(
                    'label' => 'LOKAL',
                    'url' => '/lokal/home',
                );
            }
        }
        if($this->_have_voucher>0){
            if($this->_page!='voucher'){
                $this->_list_menu[]= array(
                    'label' => 'VOUCHER',
                    'url' => '/voucher/home',
                );
            }
        }
        if($this->_have_event>0){
            if($this->_page!='event'){
                $this->_list_menu[]= array(
                    'label' => 'EVENT',
                    'url' => '/event/home',
                );
            }
        }
        if($this->_have_grosir>0){
            if($this->_page!='grosir'){
                $this->_list_menu[]= array(
                    'label' => 'GROSIR',
                    'url' => '/grosir/home',
                );
            }
        }
        
        if($this->_have_digital>0){
            if($this->_page!='digital'){
                $this->_list_menu[]= array(
                    'label' => 'DIGITAL',
                    'url' => '/digital/home',
                );
            }
        }
        if($this->_have_k24>0){
            if($this->_page!='k24'){
                $this->_list_menu[]= array(
                    'label' => 'HEALTH/OBAT',
                    'url' => '/product/home',
                );
            }
        }
        
        if($this->_page=='product'||$this->_page=='service'){
            $product_category = product_category::get_product_category_menu($this->_page);

            // Menu
            $this->_menu_list = array(
                'name' => 'KATEGORI',
                'menu' => $product_category
            );
        }
        
        if ($link_menu == 'custom') {
            if(!crequest::is_ajax()) {
                kinerjamall::kp_api_init();
            }
            $link_list = $this->list_menu();
            $product_category = product_category::get_product_category_menu($this->page());
//            $kinerjapay_product_category = Session::instance()->get('product_category_kp');
            $kinerjapay_product_category = array(
                array(
                    'vendor' => 'KP',
                    'name' => 'Voucher',
                    'category_id' => 'voucher',
                ),
                array(
                    'vendor' => 'KP',
                    'name' => 'F&B',
                    'category_id' => 'product',
                ),
            );

            $build_kinerja_link_list = array();
            if ($this->page() != 'product') {
                $build_kinerja_link_list[] = array('label'=>'HOME', 'url'=> curl::httpbase());
            }
//            $category_link_allowed = array('FSHN', 'elektronik', 'HPNTB', 'ETCRN');
            $category_link_allowed = array('FSHN', 'KLPK', 'elektronik', 'HPNTB', 'MNNBY', 'SHTCTK', 'ELKNK', 'OTOHB', 'FND', 'ETCRN');

            $page_link_allowed = array('GOLD','JASA','PULSA');
            foreach ($product_category as $product_category_k => $product_category_v) {
                $code = carr::get($product_category_v, 'code');
                if (in_array($code, $category_link_allowed)) {
                    $build_kinerja_link_list[] = $product_category_v;
                }
            }
            foreach ($link_list as $link_list_k => $link_list_v) {
                $label = carr::get($link_list_v, 'label');
                if (in_array($label, $page_link_allowed)) {
                    $build_kinerja_link_list[] = $link_list_v;
                }
            }
            
            // temp
//            if (count($kinerjapay_product_category) > 0) {
//                foreach($kinerjapay_product_category as $kp_k => $kp_v) {
//                    $build_kinerja_link_list[] = $kp_v;
//                }
//            }
            
            $kinerja_link_list = array();
            foreach ($build_kinerja_link_list as $list_k => $list_v) {
                $label = carr::get($list_v, 'label');
                $url = carr::get($list_v, 'url');
                $code = carr::get($list_v, 'code');
                $vendor = carr::get($list_v, 'vendor');
                if (strlen($code) > 0) {
                    $name = carr::get($list_v, 'name');
                    $tmp = explode(' ', $name);
                    $name = carr::get($tmp, 0, $name);
                    $label = strtoupper($name);
                    $url = curl::base(). $this->page().'/category/'.carr::get($list_v, 'category_id');
                }
                if (strlen($vendor) > 0) {
                    $label = strtoupper(carr::get($list_v, 'name'));
                    $url = curl::base().'kinerja/category/'.carr::get($list_v, 'category_id');
                }
                if ($label == 'PERALATAN') {
                    $label = 'ELEKTRONIK';
                }
                if ($label == 'KOMPUTER,LAPTOP') {
                    $label = 'KOMPUTER';
                }
                $arr['label'] = $label;
                $arr['url'] = $url;
                $kinerja_link_list[] = $arr;
            }

            if (count($kinerja_link_list) == 0) {
                $kinerja_link_list = $this->_list_menu;
            }

            $this->_list_menu = $kinerja_link_list;
        }
        
        if ($this->_have_zopim > 0) {
            $zopim_filter = '$zopim.livechat.departments.filter("Customer Service");';
            if(CF::domain()=='mybigmall.co') {
                $zopim_filter = '$zopim.livechat.departments.filter("Customer Service","Dealership");';
            }
            $app->add_js('
                window.$zopim||(function(d,s){
                    var z=$zopim=function(c){
                            z._.push(c)
                    },
                    $=z.s=d.createElement(s),
                    e=d.getElementsByTagName(s)[0];
                    z.set=function(o){
                            z.set._.push(o)
                    };
                    z._=[];
                    z.set._=[];
                    $.async=!0;
                    $.setAttribute("charset","utf-8");
                    $.src="//v2.zopim.com/?' . $this->_zopim_id . '";
                    z.t=+new Date;
                    $.type="text/javascript";

                    e.parentNode.insertBefore($,e)
                })
                (document,"script");
                $zopim(function() {
                    $zopim.livechat.addTags("' . CF::domain() . '");
                    '.$zopim_filter.'

                    $zopim.livechat.theme.setColors({badge: "#0000FF", primary: "#FF0000"});
                    $zopim.livechat.theme.reload(); // apply new theme settings
                });


            ');
        }
        
        
    }
    public static function factory($api=null){
        return new SixtyTwoHallFamilyController($api);
    }
    
    public function list_menu() {
        return $this->_list_menu;
    }

    public function menu_list() {
        return $this->_menu_list;
    }

    public function page() {
        return $this->_page;
    }

    public function domain() {
        return $this->_domain;
    }

    public function org_id() {
        return $this->_org_id;
    }

    public function org_code() {
        return $this->_org_code;
    }

    public function have_product() {
        return $this->_have_product;
    }

    public function have_prelove() {
        return $this->_have_prelove;
    }
    public function have_voucher() {
        return $this->_have_voucher;
    }

    public function have_lokal() {
        return $this->_have_lokal;
    }

    public function have_grosir() {
        return $this->_have_lokal;
    }

    public function have_gold() {
        return $this->_have_gold;
    }

    public function have_service() {
        return $this->_have_service;
    }

    public function have_register() {
        return $this->_have_register;
    }

    public function have_deposit() {
        return $this->_have_deposit;
    }

    public function have_topup_transfer() {
        return $this->_have_topup_transfer;
    }

    public function have_member_get_member() {
        return $this->_have_member_get_member;
    }
    
    public function have_digital() {
        return $this->_have_digital;
    }

    public function store() {
        return $this->_store;
    }

    public function get_org_code() {
        return $this->_get_org_code;
    }

    public function data_org() {
        return $this->_data_org;
    }

    public function email_format($base_class) {
        $classname = ucfirst($base_class) . "Email";
        $file = CF::get_files('libraries', "FormatEmail/" . $classname);
        include_once $file[0];
        $tbclass = new $classname();
        return $tbclass;
    }

    private function send_email_registration($recipient, $data) {
        $tbclass = $this->email_format("registration");
        $format = $tbclass::format($recipient, $data);
        $result=false;
        try {
            $result= cmail::send_smtp($recipient, $format['subject'], $format['message']);
        } catch(Exception $ex) {
            //do nothing
            if(isset($_GET['debug'])) {
                cdbg::var_dump($ex->getMessage());
            }
        }
        return $result;
    }

    // same with resend_verification at cms
    public function send_verification($id) {
        $db = CDatabase::instance();
        $org_id = CF::org_id();

        // data email member
        $data_email = member::get_email_member($id);
        $data_org = org::get_org($org_id);

        if (!empty($data_email) > 0) {
            $email = $data_email->email;
            $member_id = $data_email->member_id;
            $name = $data_email->name;
            $org_name = !empty($data_org->name) ? $data_org->name : NULL;

            $this->send_email_registration($email, array(
                'logo' => curl::httpbase() . 'application/adminittronmall/' . $this->_get_org_code . '/upload/logo/item_image/' . $this->_data_org->item_image,
                'subject' => $this->_store . ' - Welcome to ' . $this->_store,
                'member_id' => $member_id,
                'name' => $name,
                'org_name' => $org_name,
                    )
            );
            return true;
        } else {
            throw new Exception('Resend verification email failed.');
        }
    }

    private function send_email_transaction($recipient, $data) {
        $db = CDatabase::instance();
        $element_invoice = CFactory::create_element('62hall-invoice', 'element_invoicel');
        $element_invoice->set_booking_code($data['booking_code'])
                ->set_recipient($recipient)
                ->set_org_name($data['org_name'])
                ->set_domain($data['domain'])
                ->set_type_email($data['type'])
                ->set_status($data['status'])
                ->set_logo($data['logo']);

        $result = null;

        $type_email_transaction = NULL;
        if ($data['type'] == 'order') {
            $type_email_transaction = 'ORDER';
        } else if ($data['type'] == 'confirm') {
            $type_email_transaction = 'CONFIRM_BANK_TRANSFER';
        } else if ($data['type'] == 'payment') {
            $type_email_transaction = 'TRANSACTION_SUCCESS';
        } else if ($data['type'] == 'shipping') {
            $type_email_transaction = 'SHIPPING';
        } else if ($data['type'] == 'arrival') {
            $type_email_transaction = 'ARRIVAL';
        } else if ($data['type'] == 'request') {
            if ($data['status'] == 'ORDER') {
                $type_email_transaction = 'REQUEST';
            } else if ($data['status'] == 'SUCCESS') {
                $type_email_transaction = 'APPROVE_REQUEST';
            } else if ($data['status'] == 'FAILED') {
                $type_email_transaction = 'REJECT_REQUEST';
            }
        }


        $cc = email::get_email_transaction('cc', $type_email_transaction);
        $bcc = email::get_email_transaction('bcc', $type_email_transaction);

        if ($data['type'] == 'request') {
            if ($data['status'] == 'ORDER') {
                $query = "select
                                v.email as email_vendor
                              from transaction as t
                              inner join transaction_detail as td on td.transaction_id = t.transaction_id
                              inner join vendor as v on v.vendor_id = td.vendor_id
                              where
                                td.status > 0
                                and t.booking_code = " . $db->escape($data['booking_code']) . "
                                order by t.transaction_id desc
                            ";
                $data_vendor = cdbutils::get_row($query);
                if ($data_vendor) {
                    if (strlen($data_vendor->email_vendor)) {
                        $bcc[] = $data_vendor->email_vendor;
                    }
                }
            }
        }
        try {
            $email_confirmation=  cms_options::get('email_confirmation');
            if(strlen($email_confirmation)>0){
                $bcc[]=$email_confirmation;
            }
            $result = cmail::send_smtp($recipient, $data['subject'], $element_invoice->generate_email(), array(), $cc, $bcc);
        } catch (Exception $e) {

        }


        return $result;
    }

    public function send_transaction($type, $booking_code, $status = NULL) {
        $db = CDatabase::instance();
        $transaction_contact = transaction::get_transaction_contact('booking_code', $booking_code);
        $transaction_id = cdbutils::get_value('select transaction_id from transaction where status > 0 and booking_code ='.$db->escape($booking_code));
        if($transaction_id != null){
            if($status == 'SUCCESS'){
                try {
                     email::send('TransactionVendor',$transaction_id);
//                    $emails = email::send('TransactionVendor',$transaction_id,null,true);
//                    foreach ($emails as $key => $email) {
//                        echo $email['messages'];
//                    }
                } catch (Exception $e) {
                    // do nothing when error sending email
                }
            }
        }
        if (count($transaction_contact->shipping_email) > 0) {
            //$email = 'riza.armanda@yahoo.co.id';
            $email = $transaction_contact->shipping_email;
            //$org_name = !empty($data_org->name) ? $data_org->name : NULL;

            $this->send_email_transaction($email, array(
                'logo' => curl::httpbase() . 'application/adminittronmall/' . $this->_get_org_code . '/upload/logo/item_image/' . $this->_data_org->item_image,
                'subject' => $this->_store . ' - Order ' . $type . ' - ' . $status . ' - ' . $booking_code,
                'org_name' => $this->_store,
                'domain' => $this->_domain,
                'type' => $type,
                'booking_code' => $booking_code,
                'status' => $status,
                    )
            );
            return true;
        } else {
            throw new Exception('Resend verification email failed.');
        }
    }

}
