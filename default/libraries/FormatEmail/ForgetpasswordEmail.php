<?php

    class ForgetpasswordEmail {
        /**
         * format
         *
         * @param   recipient   $recipient  Berisi nama email penerima
         * @param   data        $data       Berisi data array isi email
         * array(
         *      "logo"      => logo
         *      "subject"   => subject email
         *      "content"   => content email
         *      "footer"    => footer email
         * )
         */
        
        public static function format($recipient='',$data=''){
            $subject    = $data['subject'];
            
            $sosial_media = NULL;
            $facebook = cms_options::get('media_fb');
            $twitter = cms_options::get('media_tw');
            $instagram = cms_options::get('media_ig');
            $google_plus = cms_options::get('media_gp');
            
            $link_facebook = NULL;
            if($facebook){
                $link_facebook = '<a target="_blank" href="' . $facebook . '"><img style="margin:10px"src="'. curl::httpbase() . 'application/ittronmall/default/media/img/imlasvegas/white-fb.png' . '"/></a>';
            }
            
            $link_twitter = NULL;
            if($twitter){
                $link_twitter = '<a target="_blank" href="' . $twitter . '"><img style="margin:10px"src="'. curl::httpbase() . 'application/ittronmall/default/media/img/imlasvegas/white-tw.png' . '"/></a>';
            }
            $link_instagram = NULL;
            if ($instagram) {
                $link_instagram = '<a target="_blank" href="' . $instagram . '"><img style="margin:10px"src="' . curl::httpbase() . 'application/ittronmall/default/media/img/imlasvegas/white-ig.png' . '"/></a>';
            }
            $link_google_plus = NULL;
            if ($google_plus) {
                $link_google_plus = '<a target="_blank" href="' . $google_plus . '"><img style="margin:10px"src="' . curl::httpbase() . 'application/ittronmall/default/media/img/imlasvegas/white-gp.png' . '"/></a>';
            }
            $footer = CView::factory('email/footer');
            $sosmed = array(
                $link_facebook, $link_twitter, $link_instagram, $link_google_plus
            );
            $footer->sosmed = $sosmed;
            $footer->member_email = $recipient;
            $footer->org_id = CF::org_id();
                $sosial_media = '<div style="width:98%;background:#ccc;">
                            <center>
                                <div style="margin-left:20px; margin-right:20px; border-top:2px solid #6b6b6b; border-bottom: 2px solid #6b6b6b;padding-top:20px; padding-bottom:20px;font-size:14px">
                                    Ikuti kami untuk memperoleh update promo terbaru
                                    <div style="width:220px;margin:0 auto;">
                                            ' . $link_facebook . '
                                            ' . $link_twitter . '
                                            ' . $link_instagram . '
                                            ' . $link_google_plus . '
                                    </div>
                                    Email ini dikirimkan kepada <span style="color: #fe0100;">' . $recipient . '</span>.<br>
                                    Jika Anda membutuhkan bantuan, silahkan klik di <a href="' . curl::httpbase() . 'read/page/index/' . '" style="color:#fe0100; text-decoration:underline;">sini</a>.
                                </div>
                            </center>
                        </div>';
            
            $message    = '
                <body style="padding-top: 25px;font-family: "Open Sans", Helvetica, Arial, sans-serif;font-size:20px;">
                    <div style="
                        width: 98%;
                        border-top: 13px solid #b90303;
                        text-align: center;
                        padding-top: 12px;
                    ">
                        <img src="'.$data['logo'].'" width="250" height="90"><br/><br/>
                    </div>
                    <div style="padding:20px;width:100%">'. $data['content'] .'</div>
                    '.$footer->render().'
                </body>
            ';
            $result     = array(
                'subject'   => $subject,
                'message'   => $message
            );
            return $result;
        }
        
    }