<?php

    class RegistrationkpEmail {
        /**
         * format
         *
         * @param   recipient   $recipient  Berisi nama email penerima
         * @param   data        $data       Berisi data array isi email
         * array(
         *      "logo"      => logo
         *      "subject"   => subject email
         *      "content"   => content email
         *      "footer"    => footer email
         * )
         */
        
        public static function format($recipient='',$data=''){
            $subject    = $data['subject'];
            
            $sosial_media = NULL;
            $facebook = cms_options::get('media_fb');
            $twitter = cms_options::get('media_tw');
            $instagram = cms_options::get('media_ig');
            $google_plus = cms_options::get('media_gp');
            
            $link_facebook = NULL;
            if($facebook){
                $link_facebook = '<a target="_blank" href="' . $facebook . '"><img style="margin:10px"src="'. curl::httpbase() . 'application/62hallfamily/default/media/img/social-media/facebook.png' . '"/></a>';
            }
            
            $link_twitter = NULL;
            if($twitter){
                $link_twitter = '<a target="_blank" href="' . $twitter . '"><img style="margin:10px"src="'. curl::httpbase() . 'application/62hallfamily/default/media/img/social-media/facebook.png' . '"/></a>';
            }
            $link_instagram = NULL;
            if ($instagram) {
                $link_instagram = '<a target="_blank" href="' . $instagram . '"><img style="margin:10px"src="' . curl::httpbase() . 'application/62hallfamily/default/media/img/social-media/instagram.png' . '"/></a>';
            }
            $link_google_plus = NULL;
            if ($google_plus) {
                $link_google_plus = '<a target="_blank" href="' . $google_plus . '"><img style="margin:10px"src="' . curl::httpbase() . 'application/62hallfamily/default/media/img/social-media/google_plus.png' . '"/></a>';
            }

                $sosial_media = '<div style="width:98%;background:#ccc;">
                            <center>
                                <div style="margin-left:20px; margin-right:20px; border-top:2px solid #6b6b6b; border-bottom: 2px solid #6b6b6b;padding-top:20px; padding-bottom:20px;font-size:14px">
                                    Ikuti kami untuk memperoleh update promo terbaru
                                    <div style="width:220px;margin:0 auto;">
                                            ' . $link_facebook . '
                                            ' . $link_twitter . '
                                            ' . $link_instagram . '
                                            ' . $link_google_plus . '
                                    </div>
                                    Email ini dikirimkan kepada <span style="color: #fe0100;">' . $recipient . '</span>.<br>
                                    Jika Anda membutuhkan bantuan, silahkan klik di <a href="' . curl::httpbase() . 'read/page/index/' . '" style="color:#fe0100; text-decoration:underline;">sini</a>.
                                </div>
                            </center>
                        </div>';
            
            
            $message    = '
                <body style="padding-top: 25px;font-family: "Open Sans", Helvetica, Arial, sans-serif;font-size:20px;">
                    <div style="width:98%;border-bottom:2px solid #fe0100;">
                        <img src="'.$data['logo'].'" width="250" height="90"><br/><br/>
                    </div>
                    <div style="padding:20px;width:100%">
                        <div style="font-size:24px;">
                            <b>Selamat Bergabung di ' . $data['org_name'] . '</b>
                        </div>
                        <b style="font-size:22px">Hi ' . $data['name'] . ',</b>
                        <div style="margin-top:30px;font-size:18px;">
                        Akun Anda telah aktif, Anda terdaftar menggunakan<br>
                        Email:<span class="color:#fe0100;">' . $recipient . '</span><br>
                        Password:<span class="color:#fe0100;">' . $data['password_real'] . '</span><br>

                        Ayo, kunjungi ' . $data['org_name'] . ' sekarang juga!<br>
                        Temukan barang yang anda cari dengan harga menarik.
                        </div>
                    </div>
                    ' . $sosial_media . '
                    <div style="width:98%;margin:10px auto;font-size:18px;color:#fe0100 !important;">
                        <center>
                            <a target="_blank" href="' . curl::httpbase() . 'read/page/index/syarat-dan-ketentuan' . '">
                                <span style="color:#fe0100 !important;"><u>Syarat dan Ketentuan</u></span>
                            </a>&nbsp;|&nbsp;
                            <a target="_blank" href="' . curl::httpbase() . 'read/page/index/kebijakan-privasi' . '">
                                <span style="color:#fe0100 !important;"><u>Kebijakan Privasi</u></span>
                            </a>
                        </center>
                    </div>
                </body>
            ';
            $result     = array(
                'subject'   => $subject,
                'message'   => $message
            );
            return $result;
        }

    }