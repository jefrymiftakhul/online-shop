<?php

    class RegistrationEmail {
        /**
         * format
         *
         * @param   recipient   $recipient  Berisi nama email penerima
         * @param   data        $data       Berisi data array isi email
         * array(
         *      "logo"      => logo
         *      "subject"   => subject email
         *      "content"   => content email
         *      "footer"    => footer email
         * )
         */
        
        public static function format($recipient='',$data=''){
            $type = $data['type'];
            $subject    = $data['subject'];
            $transaction_contact = $data['transaction_contact'];
            $transaction_detail = $data['transaction_detail'];
            $transaction_payment = $data['transaction_payment'];
            
            $name = $transaction_contact->shipping_first_name;
            
            $header_message = NULL;
            if($type == 'order'){
                $header_message = "Kami telah menerima pesanan Anda. Kami akan memproses pesanan Anda setelah pembayaran dikonfirmasi.";
            }
            else if($type == 'payment'){
                $header_message = "Kami telah menerima pembayaran Anda. Kami akan segera mengirim pesanan Anda ke alamat pengiriman.";
            }
            
            $table_contact = NULL;
            $table_contact .= '<table style="width:100%; font-size:18px">';
            $table_contact .= '<tr>';
            $table_contact .= '<th style="background-color: #fe0100" colspan="2">';
            $table_contact .= 'Detail Pesanan';
            $table_contact .= '</th>';
            $table_contact .= '</tr>';
            
            $table_contact .= '<tr>';
            $table_contact .= '<td>';
            $table_contact .= 'Order ID : ' . $transaction_contact->booking_code . '<br>';
            $table_contact .= 'Tanggal : ' . $transaction_contact->date;
            $table_contact .= '</td>';
            $table_contact .= '<td>';
            $table_contact .= 'Email : ' . $transaction_contact->shipping_email . '<br>';
            $table_contact .= 'Telepon : ' . $transaction_contact->shipping_phone;
            $table_contact .= '</td>';
            $table_contact .= '</tr>';
            
            $table_contact = NULL;
            $table_contact .= '<table style="width:100%; font-size:18px; border: 1px solid #ccc;" border="0">';
            $table_contact .= '<tr>';
            $table_contact .= '<th style="background-color: #fe0100" colspan="2">';
            $table_contact .= 'Detail Pesanan';
            $table_contact .= '</th>';
            $table_contact .= '</tr>';
            
            foreach ($transaction_detail as $detail){
                $table_contact .= '<tr>';
                $table_contact .= '<td>';
                $table_contact .= 'Order ID : ' . $transaction_contact->booking_code . '<br>';
                $table_contact .= 'Tanggal : ' . $transaction_contact->date;
                $table_contact .= '</td>';
                $table_contact .= '<td>';
                $table_contact .= 'Email : ' . $transaction_contact->shipping_email . '<br>';
                $table_contact .= 'Telepon : ' . $transaction_contact->shipping_phone;
                $table_contact .= '</td>';
                $table_contact .= '</tr>';
            }
            
            $table_address = NULL;
            $table_address .= '<table style="width:100%; font-size:18px">';
            $table_address .= '<tr>';
            $table_address .= '<th style="background-color: #fe0100">';
            $table_address .= 'Alamat';
            $table_address .= '</th>';
            $table_address .= '</tr>';
            
            foreach ($transaction_contact as $contact){
                $table_address .= '<tr>';
                $table_address .= '<td>';
                $table_address .= 'Order ID : ' . $transaction_contact->booking_code . '<br>';
                $table_address .= 'Tanggal : ' . $transaction_contact->date;
                $table_address .= '</td>';
                $table_address .= '<td>';
                $table_address .= 'Email : ' . $transaction_contact->shipping_email . '<br>';
                $table_address .= 'Telepon : ' . $transaction_contact->shipping_phone;
                $table_address .= '</td>';
                $table_address .= '</tr>';
            }
            
            $sosial_media = NULL;
            $facebook = cms_options::get_facebook();
            $twitter = cms_options::get_twitter();
            
            $link_facebook = NULL;
            if($facebook){
                $link_facebook = '<a target="_blank" href="' . $facebook . '"><img style="margin:10px"src="'. curl::httpbase() . 'application/62hallfamily/default/media/img/social-media/facebook.png' . '"/></a>';
            }
            
            $link_twitter = NULL;
            if($twitter){
                $link_twitter = '<a target="_blank" href="' . $twitter . '"><img style="margin:10px"src="'. curl::httpbase() . 'application/62hallfamily/default/media/img/social-media/facebook.png' . '"/></a>';
            }
            
            if($link_facebook || $link_twitter){
                $sosial_media = '<div style="width:98%;background:#ccc;">
                            <center>
                                <div style="margin-left:20px; margin-right:20px; border-top:2px solid #6b6b6b; border-bottom: 2px solid #6b6b6b;padding-top:20px; padding-bottom:20px;font-size:14px">
                                    Ikuti kami untuk memperoleh update promo terbaru
                                    <div style="width:220px;margin:0 auto;">

                                        <img style="margin:10px"src="'. curl::httpbase() . 'application/62hallfamily/default/media/img/social-media/twitter.png' . '"/>
                                    </div>
                                    Email ini dikirimkan kepada <span style="color: #fe0100;">' . $recipient . '</span>.<br>
                                    Jika Anda membutuhkan bantuan, silahkan klik di <a href="' . curl::httpbase() . 'read/page/index' . '" style="color:#fe0100; text-decoration:underline;">sini</a>.
                                </div>
                            </center>
                        </div>';
            }
            
            $message    = '
                <body style="padding-top: 25px;font-family: "Open Sans", Helvetica, Arial, sans-serif;font-size:20px;">
                    <div style="width:98%;border-bottom:2px solid #fe0100;">
                        <img src="'.$data['logo'].'" width="250" height="90"><br/><br/>
                    </div>
                    <div style="padding:20px;width:100%">
                        <div style="font-size:24px;">
                            <b>Hi ' . $name . ',</b>
                        </div>
                        <div style="margin-top:30px;font-size:18px;">'
                        . $header_message . 
                        '<br><br>
                        <table width="100%">
                            <tr>
                                <td width="200px">
                                    Kode Pembayaran
                                </td>
                                <td>
                                : ' . $transaction_contact->booking_code . '
                                </td>
                            </tr>
                            <tr>
                                <td width="200px">
                                    Tanggal Pembelian
                                </td>
                                <td>
                                : ' . $transaction_contact->date . '
                                </td>
                            </tr>
                            <tr>
                                <td width="200px">
                                    Metode Pembayaran
                                </td>
                                <td>
                                : ' . $transaction_payment->payment_type . '
                                </td>
                            </tr>
                            <tr>
                                <td width="200px">
                                    Status
                                </td>
                                <td>
                                : ' . $transaction_payment->payment_status . '
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br><br>
                    ' . $table_contact . '
                    ' . $sosial_media . '
                    <div style="width:98%;margin:10px auto;font-size:18px;color:#fe0100 !important;">
                        <center>
                            <a target="_blank" href="' . curl::httpbase() . 'read/page/index/syarat-dan-ketentuan' . '">
                                <span style="color:#fe0100 !important;"><u>Syarat dan Ketentuan</u></span>
                            </a>&nbsp;|&nbsp;
                            <a target="_blank" href="' . curl::httpbase() . 'read/page/index/kebijakan-privasi' . '">
                                <span style="color:#fe0100 !important;"><u>Kebijakan Privasi</u></span>
                            </a>
                        </center>
                    </div>
                </body>
            ';
            $result     = array(
                'subject'   => $subject,
                'message'   => $message
            );
            return $result;
        }

    }