<?php

class CApiClient {

    protected $api_url;
    protected $api_auth;
    protected $api_session_id;
    protected $product_category_code;
    protected $product_code;
    protected static $instance;
    protected $engine;
    protected $org_id;
    protected $username;
    protected $debug;

    protected function __construct($product_category_code, $product_code) {
        $this->api_url = ccfg::get('api_url');
        $this->api_auth = ccfg::get('api_auth');
        $this->product_category_code = strtoupper($product_category_code);
        $this->product_code = strtoupper($product_code);
        switch ($product_category_code) {
            case "PG":
                $this->engine = CApiClientEngine_PaymentGatewayEngine::factory($this);
                break;
            case "PU":
                $this->engine = CApiClientPulsaEngine::factory($this);
                break;
            default:
                throw new Exception('Product Category Not Found');
                break;
        }

        $app = CApp::instance();
        $org_id = null;
        $org = $app->org();
        if ($org != null) {
            $org_id = $org->org_id;
        }
        $user = $app->user();
        $username = null;
        if ($user != null) {
            $username = $user->username;
        }
        $this->org_id = $org_id;
        $this->username = $username;
        $this->debug = true;
    }

    public static function instance($product_category_code, $product_code) {
        $product_category_code = strtoupper($product_category_code);
        $product_code = strtoupper($product_code);
        if (self::$instance == null) {
            self::$instance = array();
        }
        if (!isset(self::$instance[$product_category_code])) {
            self::$instance[$product_category_code] = array();
        }
        if (!isset(self::$instance[$product_category_code][$product_code])) {
            self::$instance[$product_category_code][$product_code] = new CApiClient($product_category_code, $product_code);
        }
        return self::$instance[$product_category_code][$product_code];
    }

    public function org_id() {
        return $this->org_id;
    }

    public function username() {
        return $this->username;
    }

    public function api_auth() {
        return $this->api_auth;
    }

    public function api_url() {
        return $this->api_url;
    }

    public function api_session_id() {
        return $this->session()->get('api_session_id');
    }

    public function product_category_code() {
        return $this->product_category_code;
    }

    public function product_code() {
        return $this->product_code;
    }

    public function error() {
        return CApiClientError::instance();
    }

    public function session() {
        return CApiClientSession::instance($this->product_category_code);
    }

    public function engine() {
        return $this->engine;
    }

    public function exec($method, $request) {

        $data = array(
            'err_code' => '0',
            'err_message' => '',
            'data' => array(
            ),
        );
        $session = $this->session();
        if (strlen($this->api_session_id()) == 0) {
            $data['login_data'] = $this->engine->login($request);
            $error = $this->error();
            $err_code = $error->code();
            if ($err_code != 0) {
                $data['err_code'] = $err_code;
                $data['err_message'] = $error->get_err_message();
            }
            if ($this->debug) {
                $data['last_request'] = $this->engine->last_request();
                $data['last_response'] = $this->engine->last_response();
            }
        }
        $captcha_image = $session->get('captcha_image');
        $captcha_success = $session->get('captcha_success');
        if ($this->error()->code() == 0) {
            if (($captcha_image != null && $captcha_success == true) || $captcha_image == null || $method == 'captcha_commit') {
                $engine_data = $this->engine->$method($request);
                $error = $this->error();
                $err_code = $error->code();
                if ($err_code != 0) {
                    $data['err_code'] = $err_code;
                    $data['err_message'] = $error->get_err_message();
                    if ($this->debug) {
                        $data['last_request'] = $this->engine->last_request();
                        $data['last_response'] = $this->engine->last_response();
                    }
                }
                $data['data'] = $engine_data;
            } else {
                $data['data'] = carr::get($data, 'login_data');
            }
        }

        $data['next_method'] = $this->engine->next_method($method);
        return $data;
    }

}
