<?php

class CElement_Filter extends CElement {

    private $key = NULL;
    private $start = 0;
    private $count = 0;
    private $list_filter = array();
    private $filter_product = TRUE;
    private $list_filter_product = array();
    private $filter_price = TRUE;
    private $list_filter_price = array();
    private $min_price = 0;
    private $max_price = 0;
    private $filter_component_control = array();
    private $type_select = 'select';
    private $type_image = 'image';
    private $type_colorpicker = 'colorpicker';
    private $param = array();
    private $param_filter_attribut = array();
    private $product_visibility = 'catalog_search';
    private $filter_name = '';
    private $filter_page = '';
    private $filter_product_category_id = '';
    private $filter_category_lft = '';
    private $filter_category_rgt = '';
    private $filter_category = false;
    private $filter_location = false;
    private $filter_province = false;
    private $filter_city = false;
    private $list_filter_location = array();
    private $list_filter_province = array();
    private $list_filter_city = array();
    private $list_filter_category = array();
    private $filter_title = 'Filter';
    private $filter_prelove = false;
	private $use_title = true;

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_Filter($id);
    }

    public function set_key($key) {
        $this->key = $key;

        return $this;
    }

    public function set_filter_page($val) {
        $this->filter_page = $val;

        return $this;
    }

    public function set_use_title($bool) {
        $this->use_title = $bool;

        return $this;
    }
	
	public function set_filter_prelove($filter_prelove) {
		$this->filter_prelove = $filter_prelove;
	}

    public function set_filter_name($val) {
        $this->filter_name = $val;

        return $this;
    }
    
    public function set_filter_product_category_id($val) {
        $this->filter_product_category_id = $val;
        return $this;
    }

    public function set_filter_category_lft($val) {
        $this->filter_category_lft = $val;

        return $this;
    }

    public function set_filter_category_rgt($val) {
        $this->filter_category_rgt = $val;

        return $this;
    }

    public function set_product_visibility($val) {
        $this->product_visibility = $val;

        return $this;
    }

    public function set_list_filter($list_filter) {
        $this->list_filter = $list_filter;

        return $this;
    }

    public function set_filter_product($filter_product) {
        $this->filter_product = $filter_product;

        return $this;
    }

    public function set_list_filter_product($list_filter_product) {
        $this->list_filter_product = $list_filter_product;

        return $this;
    }

    public function set_filter_price($filter_price) {
        $this->filter_price = $filter_price;

        return $this;
    }

    public function set_min_price($val) {
        $this->min_price = $val;

        return $this;
    }

    public function set_max_price($val) {
        $this->max_price = $val;

        return $this;
    }

    public function set_list_filter_price($list_filter_price) {
        $this->list_filter_price = $list_filter_price;

        return $this;
    }
	
	public function set_filter_province($filter_province) {
		$this->filter_province = $filter_province;
	}

	public function set_filter_city($filter_city) {
		$this->filter_city = $filter_city;
	}

	public function set_list_filter_province($list_filter_province) {
		$this->list_filter_province = $list_filter_province;
	}

	public function set_list_filter_city($list_filter_city) {
		$this->list_filter_city = $list_filter_city;
	}
	
	public function set_filter_title($filter_title) {
		$this->filter_title = $filter_title;
	}
	
	public function set_filter_location($filter_location) {
		$this->filter_location = $filter_location;
	}

	public function set_list_filter_location($list_filter_location) {
		$this->list_filter_location = $list_filter_location;
	}
	
	public function set_filter_category($filter_category) {
		$this->filter_category = $filter_category;
	}

	public function set_list_filter_category($list_filter_category) {
		$this->list_filter_category = $list_filter_category;
	}
	
    private function generate_list($type, $data = array()) {
        $list = array();

        foreach ($data as $key => $row_data) {
            if ($type == $this->type_select) {
                //$value = carr::get($row_data, 'attribute_key');
                $value['key'] = $key;
                $value['label'] = carr::get($row_data, 'attribute_key');
            }
            if ($type == $this->type_image) {
                $value['key'] = $key;
                $value['label'] = carr::get($row_data, 'attribute_key');
                $value['value'] = carr::get($row_data, 'file_path');
            }
            if ($type == $this->type_colorpicker) {
                $hex_code = carr::get($row_data, 'hex_code');
                if (strlen($hex_code) == 0) {
                    $hex_code = carr::get($row_data, 'attribute_key');
                }
                $value['key'] = $key;
                $value['label'] = carr::get($row_data, 'attribute_key');
                $value['hex_code'] = $hex_code;
                
            }

            $list[$key] = $value;
        }

        return $list;
    }

    private function generate_form($container, $name, $data) {
        $id = carr::get($data, 'attribute_category_id');
        $type = carr::get($data, 'attribute_category_type');
        $attribute = carr::get($data, 'attribute', array());
        $list = $this->generate_list($type, $attribute);
        $control = array();
        if ($type == $this->type_select) {
            if (count($list) > 0) {
                $filter_select = $container->add_div()->add_class('horizontal-overflow')->custom_css('max-height', '200px');

                $div_container = $filter_select->add_div('container-' . $name)->add_class('padding-left30 filter-checkbox');
                foreach ($list as $key => $val) {
                    $control = $div_container->add_field()
                            ->add_control('catalog_filter_' . $id . '_' . carr::get($val,'key'), 'checkbox')
                            ->add_class('input-filter checkbox-62hallfamily' . $name)
                            ->set_value($key);
                    $div_container->add("
						<label for='catalog_filter_".$id."_".carr::get($val,'key')."'>" . carr::get($val,'label') . "</label>
					");

                    $this->param[] = 'catalog_filter_' . $id . '_' . carr::get($val,'key');
                    //$this->filter_component_control['click'][] = $control;
                }
            }
        } else if ($type == $this->type_image) {
            $div_container = $container->add_div('container-' . $name)
                    ->add_class('container-input-image');
            $index = 1;


            foreach ($list as $key => $value) {
                $image_hidden = $div_container->add_control('catalog_filter_' . $id . '_' . $key, 'hidden')
                                ->add_class('input-image')->set_value('');
                $this->param[] = 'catalog_filter_' . $id . '_' . $key;
                $div_image = $div_container->add_div('div_catalog_filter_' . $id . '_' . $key)
                        ->add_class('input-filter btn-colors margin-bottom-10 link ')
                        ->custom_css('display', 'inline-block')
                        ->custom_css('padding', '2px');

                $control = $div_image->add_img('filter_image_' . $key)
                        ->set_src($value['value'])
                        ->set_attr('key', $key)
                        ->set_attr('rel', 'catalog_filter_' . $id . '_' . $key)
                        ->set_attr('active', '0');

                //$this->filter_component_control['click'][] = $control;
                $index++;
            }
        }
        else if ($type == $this->type_colorpicker) {
            if (count($list) > 0) {
                $filter_select = $container->add_div()->add_class('horizontal-overflow')->custom_css('max-height', '200px');

                $div_container = $filter_select->add_div('container-' . $name)->add_class('filter-checkbox');
                
                $div_container->add('<ul class="list-inline">');
                foreach ($list as $key => $val) {
                    $control_id = 'catalog_filter_'.$id.'_'.carr::get($val,'key');
                    $control_key = carr::get($val,'key');
                    $control_label = carr::get($val,'label');
                    $control_color = carr::get($val, 'hex_code');
                    
                    $li_control = '<li>';
                        $li_control .= '<div class="checkbox-colorpicker">';
                            $li_control .= '<input type="checkbox" name="'.$control_id.'" id="'.$control_id.'" class="input-filter checkbox-62hallfamilyGDCOLOR validate[]" value="'.$control_key.'">';
                            $li_control .= '<label for="'.$control_id.'" style="background-color: '.$control_color.'">'.$control_label.'</label>';
                        $li_control .= '</div>';
                    $li_control .= '</li>';
//                    
                    $control = $div_container->add($li_control);

                    $this->param[] = 'catalog_filter_' . $id . '_' . carr::get($val,'key');
                    //$this->filter_component_control['click'][] = $control;
                }
                $div_container->add('</ul>');
            }
        }
        return $control;
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
		$db = CDatabase::instance();

        $filter = $this->add_div()->add_class('filter row margin-bottom-20');

        if($this->use_title){
            $filter_title = $filter->add_div()->add_class('col-md-12 bold font-size22 margin-bottom-10')
                ->add($this->filter_title);
        }

        // SET PARAM
        $param = array();
        $param[] = 'arr_product_id';

        if ($this->filter_product) {
            $param[] = 'sortby';
        }

        if ($this->filter_price) {
            $param[] = 'price';
        }


        $this->param = $param;
		if($this->filter_location){
			$filter_location = $filter->add_div()->add_class('col-md-12');
            $filter_location_title = $filter_location->add_div('filter-location')
                    ->add_class('col-md-12 title-filter font-red link border-bottom');

            $title = $filter_location_title->add_div()
                    ->add_class('col-md-11 padding-0')
                    ->add(clang::__('Location'));

            $caret = $filter_location_title->add_div()
                    ->add_class('col-md-1 padding-0 text-right')
                    ->add('<i class="icon fa fa-caret-down font-size18"></i>');

            $filter_location_container = $filter_location->add_div()
                    ->add_class('col-md-12 padding-0 container-filter container-filter-location margin-bottom-10');
			$filter_location_container->add_control('jstree_search', 'text')->set_placeholder(clang::__('Cari Lokasi'))->add_class('filter-search-field');
			$div_location = $filter_location_container->add_div('location_result')->add_class('location_result');
			$div_location
                    ->add_element('tree-select', 'tree')
                    ->set_target('location_result')
                    ->set_callback(array('Product_Controller', 'callback_ajax'), __FILE__);
		}
		
		// Filter Category Product
		if($this->filter_category){
			if(count($this->list_filter_category)>0){
				$filter_category = $filter->add_div()->add_class('col-md-12');
				$filter_category_title = $filter_category->add_div('filter-category')
						->add_class('col-md-12 padding-0 title-filter font-red margin-top-10 margin-bottom-10 link border-bottom');

				$title = $filter_category_title->add_div()
						->add_class('col-md-11 padding-0')
						->add(clang::__('Category'));

				$caret = $filter_category_title->add_div()
						->add_class('col-md-1 padding-0 text-right')
						->add('<i class="icon fa fa-caret-down font-size18"></i>');

				$filter_category_container = $filter_category->add_div()
						->add_class('col-md-12 padding-0 container-filter container-filter-category margin-bottom-10');
				// generate html from array district given

				$filter_category_container->add('<ul>');
				foreach ($this->list_filter_category as $key => $value) {
					$filter_category_container->add('<li><a href="'.curl::base().$this->filter_page.'/product/category/'.$value->url_key.'">'.clang::__($value->name).'</a></li>');
				}
				$filter_category_container->add('</ul>');
			}
		}
		
		
		if($this->filter_province){
			$filter_province = $filter->add_div()->add_class('col-md-12');
            $filter_province_title = $filter_province->add_div('filter-province')
                    ->add_class('col-md-12 padding-0 title-filter font-red margin-top-10 margin-bottom-10 link border-bottom');

            $title = $filter_province_title->add_div()
                    ->add_class('col-md-11 padding-0')
                    ->add(clang::__('Kabupaten'));

            $caret = $filter_province_title->add_div()
                    ->add_class('col-md-1 padding-0 text-right')
                    ->add('<i class="icon fa fa-caret-down font-size18"></i>');

            $filter_province_container = $filter_province->add_div()
                    ->add_class('col-md-12 padding-0 container-filter container-filter-province margin-bottom-10');
			$filter_province_container->add_control('search_province', 'text')->set_placeholder(clang::__('Cari Lokasi Provinsi'))->add_class('filter-search-field');
			$div_filter_district_result = $filter_province_container->add_div('province_result')->add_class('province_result');
			// generate html from array district given
			if($this->list_filter_province != null){
				foreach ($this->list_filter_province as $key => $value) {
					//$div_checkbox = $div_filter_district_result->add_div()->add_class('checkbox');
					$checkbox = $div_filter_district_result->add_control('d_'.$value->province_id,'checkbox')->set_name('province[]')->add_attr('data-search',strtolower($value->name))->set_label('<label for="d_'.$value->province_id.'">'.ucfirst(strtolower($value->name)).'</label>');
				}
			}
		}
		if($this->filter_city){
			$filter_city = $filter->add_div()->add_class('col-md-12');
            $filter_city_title = $filter_city->add_div('filter-city')
                    ->add_class('col-md-12 padding-0 title-filter font-red margin-top-10 margin-bottom-10 link border-bottom');

            $title = $filter_city_title->add_div()
                    ->add_class('col-md-11 padding-0')
                    ->add(clang::__('Kota'));

            $caret = $filter_city_title->add_div()
                    ->add_class('col-md-1 padding-0 text-right')
                    ->add('<i class="icon fa fa-caret-down font-size18"></i>');

            $filter_city_container = $filter_city->add_div()
                    ->add_class('col-md-12 padding-0 container-filter container-filter-city margin-bottom-10');
			$filter_city_container->add_control('search_city', 'text')->set_placeholder(clang::__('Cari Kota'))->add_class('filter-search-field');
			$div_filter_district_result = $filter_city_container->add_div('city_result')->add_class('city_result');
			// generate html from array district given
			if($this->list_filter_city != null){
				foreach ($this->list_filter_city as $key => $value) {
					//$div_checkbox = $div_filter_district_result->add_div()->add_class('checkbox');
					$checkbox = $div_filter_district_result->add_control('dc_'.$value->city_id,'checkbox')->set_name('city[]')->add_attr('data-search',strtolower($value->name))->set_label('<label for="dc_'.$value->city_id.'">'.ucfirst(strtolower($value->name)).'</label>');
				}
			}
		}
		
        if ($this->filter_product) {
            $filter_product = $filter->add_div()->add_class('col-md-12');
            $filter_product_title = $filter_product->add_div('filter-product')
                    ->add_class('col-md-12 padding-0 title-filter font-red margin-top-10 margin-bottom-10 link border-bottom');

            $title = $filter_product_title->add_div()
                    ->add_class('col-md-11 padding-0')
                    ->add(clang::__('Atur Berdasarkan'));

            $caret = $filter_product_title->add_div()
                    ->add_class('col-md-1 padding-0 text-right')
                    ->add('<i class="icon fa fa-caret-down font-size18"></i>');

            $filter_product_container = $filter_product->add_div()
                    ->add_class('col-md-12 padding-0 container-filter container-filter-product margin-bottom-10');

            $filter_product_control = $filter_product_container->add_control('sortby', 'select')
                    ->add_class('select-62hallfamily')
                    ->set_list($this->list_filter_product)
                    ->set_applyjs('select2')
                    ->custom_css('height', '44px')
					->set_value('diskon');
					
            $this->filter_component_control['change'][] = $filter_product_control;
        }
		
		if($this->filter_prelove){
			$filter_category = $filter->add_div()->add_class('col-md-12');
            $filter_category_title = $filter_category->add_div('filter-category')
                    ->add_class('col-md-12 padding-0 title-filter prelove font-red margin-top-10 margin-bottom-10 link border-bottom');

            $title = $filter_category_title->add_div()
                    ->add_class('col-md-11 padding-0')
                    ->add(clang::__('Category'));

            $caret = $filter_category_title->add_div()
                    ->add_class('col-md-1 padding-0 text-right')
                    ->add('<i class="icon fa fa-caret-down font-size18"></i>');

            $filter_category_container = $filter_category->add_div()
                    ->add_class('col-md-12 padding-0 container-filter prelove container-filter-category margin-bottom-10');
			$q = 'select * from product_category where status > 0 and parent_id is null and product_type_id = 9';
			$data_prelove_category = $db->query($q);
			if($data_prelove_category != null){
				$filter_category_container->add('<ul>');
				$current_menu = crouter::complete_uri();
				$arr_current = explode('/', $current_menu);
				$active_menu = $arr_current[(count($arr_current)-1)];
				foreach ($data_prelove_category as $key => $value) {
				 	$filter_category_container->add('<li>');
				 		if($active_menu == $value->url_key){
				 			$filter_category_container->add('<a class="active" href="'.curl::base().'prelove/product/category/'.$value->url_key.'" datta-value="'.$value->product_category_id.'">');
				 		}else{
				 			$filter_category_container->add('<a href="'.curl::base().'prelove/product/category/'.$value->url_key.'" datta-value="'.$value->product_category_id.'">');
				 		}
				 			$filter_category_container->add($value->name);
				 		$filter_category_container->add('</a>');
				 	$filter_category_container->add('</li>');
				}	
				$filter_category_container->add('</ul>');
			}
            $filter_deal = $filter->add_div()->add_class('col-md-12');
            $filter_deal_title = $filter_deal->add_div('filter-deal')
                    ->add_class('col-md-12 padding-0 title-filter prelove font-red margin-top-10 margin-bottom-10 link border-bottom');
            $title = $filter_deal_title->add_div()
                    ->add_class('col-md-11 padding-0')
                    ->add(clang::__('Deals Type'));

            $caret = $filter_deal_title->add_div()
                    ->add_class('col-md-1 padding-0 text-right')
                    ->add('<i class="icon fa fa-caret-down font-size18"></i>');
			$filter_deal_container = $filter_deal->add_div()
                    ->add_class('col-md-12 padding-0 container-filter container-filter-province container-filter-deal margin-bottom-10');
			$available_deal_type = array(
				'last_deal'=>'Last Deal',
				'upcoming_deal'=>'Upcoming Deal',
				'end_deal'=>'End Deal',
			);
			foreach ($available_deal_type as $key => $value) {
				//$div_checkbox = $div_filter_district_result->add_div()->add_class('checkbox');
				$checkbox = $filter_deal_container->add_control($key,'checkbox');
				$checkbox->set_name('deal_type[]')->set_value($key)->add_attr('data-search',strtolower($key))->set_label('<label for="'.$key.'">'.ucfirst(strtolower($value)).'</label>');
                                $this->filter_component_control['change'][] = $checkbox;
                                $this->param[] = $key;
			}
            $filter_condition = $filter->add_div()->add_class('col-md-12');
			$filter_condition_title = $filter_condition->add_div('filter-condition')
                    ->add_class('col-md-12 padding-0 title-filter font-red margin-top-10 margin-bottom-10 link border-bottom');
			$title = $filter_condition_title->add_div()
                    ->add_class('col-md-11 padding-0')
                    ->add(clang::__('Condition'));

            $caret = $filter_condition_title->add_div()
                    ->add_class('col-md-1 padding-0 text-right')
                    ->add('<i class="icon fa fa-caret-down font-size18"></i>');
			$filter_condition_container = $filter_condition->add_div()
                    ->add_class('col-md-12 padding-0 container-filter container-filter-province container-filter-condition margin-bottom-10');
			$available_condition_type = array(
				'new'=>'New',
				'used'=>'Used',				
			);
			foreach ($available_condition_type as $key => $value) {
				//$div_checkbox = $div_filter_district_result->add_div()->add_class('checkbox');
				$checkbox = $filter_condition_container->add_control($key,'checkbox');
				$checkbox->set_name('deal_type[]')->set_value($key)->add_attr('data-search',strtolower($key))->set_label('<label for="'.$key.'">'.ucfirst(strtolower($value)).'</label>');
                                $this->filter_component_control['change'][] = $checkbox;
                                $this->param[] = $key;
			}
		}
		
        if ($this->filter_price) {
            $filter_price = $filter->add_div()->add_class('col-md-12');

            $filter_price_title = $filter_price->add_div('filter-price')
                    ->add_class('col-md-12 padding-0 title-filter font-red margin-top-10 margin-bottom-10 link border-bottom');

            $title = $filter_price_title->add_div()
                    ->add_class('col-md-11 padding-0')
                    ->add(clang::__('Harga'));

            $caret = $filter_price_title->add_div()
                    ->add_class('col-md-1 padding-0 text-right')
                    ->add('<i class="icon fa fa-caret-down font-size18"></i>');

            $filter_price_container = $filter_price->add_div()
                    ->add_class('col-md-12 padding-0 container-filter-price margin-bottom-10 container-filter');

            
            $diff = $this->max_price-$this->min_price;
           
                $step = floor($diff/100) ;
            
            $filter_price_control = $filter_price_container->add_div()
                    ->add_class('col-md-12')
                    ->add('<input id="price" style="width:100%;" type="text" value="" data-slider-min="' . $this->min_price . '" data-slider-max="' . $this->max_price . '" data-slider-step="'.$step.'" data-slider-value="[' . $this->min_price . ',' . $this->max_price . ']"/>');

            $min_price = $filter_price_container->add_div()
                    ->add_class('col-md-6 bold padding-0')
                    ->add('Rp. ' . product::format_k($this->min_price));

            $max_price = $filter_price_container->add_div()
                    ->add_class('col-md-6 bold padding-0 text-right')
                    ->add('Rp. ' . product::format_k($this->max_price));
            $this->filter_component_control['mouseup'][] = $filter_price_control;
			// $filter_price_input=$filter_price_container->add_div()->add_class('row');
			// $filter_price_min=$filter_price_input->add_div()->add_class('col-md-5');
			// $filter_price_min->add_control('input_min_price','text');
			// $filter_price_sd=$filter_price_input->add_div()->add_class('col-md-2');
			// $filter_price_sd->add_control('','label')->set_value('s/d');
			// $filter_price_max=$filter_price_input->add_div()->add_class('col-md-5');
			// $filter_price_max->add_control('input_max_price','text');
        }


        foreach ($this->list_filter as $key_filter => $list_filter) {
            $filter_component = $filter->add_div()->add_class('col-md-12');
            $key_filter = explode(' ', $key_filter);
            $key_filter = implode('_', $key_filter);

            $filter_component_title = $filter_component->add_div('filter-' . $key_filter)
                    ->add_class('col-md-12 padding-0 title-filter font-red margin-top-10 margin-bottom-10 link border-bottom');

            $title = $filter_component_title->add_div()
                    ->add_class('col-md-11 padding-0')
                    ->add(clang::__(carr::get($list_filter, 'attribute_category_name')));

            $caret = $filter_component_title->add_div()
                    ->add_class('col-md-1 padding-0 text-right')
                    ->add('<i class="icon fa fa-caret-down font-size18"></i>');

            $filter_component_container = $filter_component->add_div()
                    ->add_class('col-md-12 padding-0 margin-bottom-10 container-filter container-filter-' . $key_filter);

            $this->generate_form($filter_component_container, $key_filter, $list_filter);
        }
        if (count($this->filter_component_control) > 0) {
            foreach ($this->filter_component_control as $k => $v) {
                if (count($v) > 0) {
                    foreach ($v as $control) {
                        $control->add_listener($k)
                                ->add_handler('reload')
                                ->set_target('page-product-category')
                                ->add_param_input($this->param)
                                ->set_url(curl::base() . 'reload/reload_product_filter?source=filter&visibility=' . $this->product_visibility . '&filter_page=' . $this->filter_page . '&filter_product_category_id='.$this->filter_product_category_id.'&filter_name=' . urlencode($this->filter_name) . '&filter_category_lft=' . $this->filter_category_lft . '&filter_category_rgt=' . $this->filter_category_rgt);
                    }
                }
            }
        }
        $implode_param = '';
        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();
        $data_addition = '';

        foreach ($this->param as $inp) {
            if (strlen($data_addition) > 0)
                $data_addition.=',';
            $data_addition.="'" . $inp . "':$.cresenity.value('#" . $inp . "')";
        }

        //$data_addition = '{' . $data_addition . '}';
        $js->appendln("
                $('#price').slider({});
                
                $('.title-filter').click(function(){
                    var id = $(this).attr('id');
                    //$('.container-'+id).toggleClass('hide-me');
                    $('.container-'+id).toggle();										
                    
                    if($(this).find('.icon').hasClass('fa-caret-right')){
                        $(this).find('.icon').removeClass('fa-caret-right');
                        $(this).find('.icon').addClass('fa-caret-down');
                    }
                    else{
                        $(this).find('.icon').removeClass('fa-caret-down');
                        $(this).find('.icon').addClass('fa-caret-right');
                    }
                });
                
				$('#search_province').bind('propertychange change click keyup input paste',function(){
					var kueri = $(this).val();
					if(kueri !=''){
						$('#province_result > label').css('display','none');
						$('#province_result input[data-search*='+kueri+']').parent().css('display','block');
					}
				});
				
				$('#search_city').bind('propertychange change click keyup input paste',function(){
					var kueri = $(this).val();
					if(kueri !=''){
						$('#city_result > label').css('display','none');
						$('#city_result input[data-search*='+kueri+']').parent().css('display','block');
					}
				});

//				$('#filter-prelove input:checkbox').click(function(){
//					var param = [];
//					$('#filter-prelove input:checkbox').each(function(){
//						if(this.checked){
//							param.push(this.value);
//						}
//					});
//					console.log(param);
//				});

                $('.input-filter').click(function(){
                    if($(this).hasClass('btn-colors')) {
                        var image = jQuery(this).find('img');
                        var rel = image.attr('rel');
                        var hidden = jQuery('#'+rel);
                        var active = image.attr('active');
                        var key = image.attr('key');




                        if(active=='0'){
                                image.attr('active','1');
                                hidden.val(key);
                                jQuery(this).addClass('active');
                        }else{
                                image.attr('active','0');
                                hidden.val('');
                                jQuery(this).removeClass('active');
                        }
                }
                $.cresenity.reload('page-product-category','" . curl::base() . 'reload/reload_product_filter?source=filter_reload&visibility=' . $this->product_visibility . '&filter_page=' . $this->filter_page . '&filter_name=' . urlencode($this->filter_name) . '&filter_category_lft=' . $this->filter_category_lft . '&filter_category_rgt=' . $this->filter_category_rgt . "','get',{" . $data_addition . "});

                });
                $('#tree').on('changed.jstree', function (e, data) {
                    var i, j, r = [];
                    j = data.selected.length;
                    for(i = 0; i < j; i++) {
                      r.push(data.instance.get_node(data.selected[i]).id);
                    }
                    $.cresenity.reload('page-product-category','" . curl::base() . 'reload/reload_product_filter?source=filter_reload&visibility=' . $this->product_visibility . '&filter_page=' . $this->filter_page . '&filter_name=' . urlencode($this->filter_name) . '&filter_category_lft=' . $this->filter_category_lft . '&filter_category_rgt=' . $this->filter_category_rgt . "','get',{" . $data_addition . ",'product_location':r});
		});
                
        ");

        $js->append(parent::js($indent));
        return $js->text();
    }

}
