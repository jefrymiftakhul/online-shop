<?php

class CElement_Event_Card extends CElement
{
    protected $product;
    public function __construct($id = "", $tag = "div")
    {
        parent::__construct($id, $tag);
    }

    public static function factory($id = "", $tag = "div")
    {
        return new CElement_Event_Card($id, $tag);
    }

    public function html($indent = 0)
    {
        $html = CStringBuilder::factory();
        $session = Session::instance();
        $db = CDatabase::instance();
        $sess_member_id = $session->get('member_id');
        $product = $this->product;
        $name = carr::get($product, 'name');
        $limit = 40;
        if (strlen($name) > $limit) {
            $name = substr($name, 0, strrpos(substr($name, 0, $limit), ' ')) . '...';
        }
		$detail_price = carr::get($product,'detail_price');
        $price = carr::get($detail_price, 'channel_sell_price');
        $quota = carr::get($product, 'quota');
        $product_id = carr::get($product, 'product_id');
        $member_id = carr::get($product, 'member_id');
        $description = carr::get($product, 'description');

        $limit = 250;
        if (strlen($description) > $limit) {
            $description = substr($description, 0, strrpos(substr($description, 0, $limit), ' ')) . '...';
        }

        $url_key = carr::get($product, 'url_key');
        $image_path = carr::get($product, 'image_path');
        $image_url = carr::get($product, 'image_url');

        $image = image::get_image_url($image_path,'view_all');
        $image = $image_url;
        $url_product = curl::base() . 'event/product/item/' . $url_key;
        
        $this->add('<a href="' . $url_product . '">');
        $wrapper = $this->add_div()->add_class('event-prod-card-wrapper');
        $wrapper->add_attr('style', 'background-image:url(' . $image . ');background-position:center;background-repeat:no-repeat;background-size:cover;height:300px;');
        $price_wrapper = $wrapper->add_div()->add_class('price-wrapper');
        $price_wrapper->add_div()->add_class('price')->add('Rp. '.ctransform::thousand_separator($price).' / '.clang::__('Person'));
        $price_wrapper->add_div()->add_class('quota')->add('Quota : '.$quota.' / '.clang::__('Sesi'));

        $box_bottom = $this->add_div()->add_class('box-bottom-wrapper');
        $box_bottom->add('<h4 class="event-title">'.strtoupper($name).'</h4>');
        $box_bottom->add('<div class="event-description">'.$description.'</div>');
        $this->add('</a>');

        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0)
    {
        $js = CStringBuilder::factory();

        $js->append(parent::js($indent));
        return $js->text();
    }

    public function get_product()
    {
        return $this->product;
    }

    public function set_product($product)
    {
        $this->product = $product;
        return $this;
    }

}
