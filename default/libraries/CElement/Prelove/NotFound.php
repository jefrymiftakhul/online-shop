<?php

    /**
     *
     * @author Seians0077
     * @since  Apr 22, 2016
     * @license http://piposystem.com Piposystem
     */
    class CElement_Prelove_NotFound extends CElement {

        public function __construct($id = "", $tag = "div") {
            parent::__construct($id, $tag);
        }

        public static function factory($id = "", $tag = "div") {
            return new CElement_Prelove_NotFound($id, $tag);
        }


        public function html($indent = 0) {
            $html = new CStringBuilder();
            
            $wrapper = $this->add_div()->add_class('product-not-found-wrapper');
            
//            $wrapper_row = $wrapper->add_div()->add_class('row');
//            $left_container = $wrapper_row->add_div()->add_class('col-md-5 left-container');
//            $right_container = $wrapper_row->add_div()->add_class('col-md-7 right-container');

            $image_send = curl::base() . 'application/lastmenit/default/media/img/asset/product-not-found.jpg';
            $wrapper->add('<img src="' . $image_send . '" class="img-responsive" />');

            $wrapper->add_div()->add_class('lbl-title-product-not-found txt-center')
                    ->add(clang::__('PRODUCT NOT FOUND'));
            $label = $wrapper->add_div()->add_class('lbl-product-not-found txt-center');
            $label->add(clang::__('Sorry, The product you are looking for can not be found.'));
            $label->add_br();
            $label->add(clang::__('Please come back to our home'));

            $btn_wrapper = $wrapper->add_div()->add_class('txt-center');
            $button = $btn_wrapper->add_action()
                    ->set_label('<i class="fa fa-home"></i> ' .clang::__('BACK TO HOME'))
                    ->add_class('btn-lastm btn-back-to-home')
                    ->set_link(curl::base());
            
            
//            $line_wrapper = $right_container->add_div()->add_class('line-wrapper');
//            $line_wrapper->add_div()->add_class('line')->add('&nbsp;');
//            $line_wrapper->add_div()->add_class('txt-center line-label')
//                    ->add_div()->add_class('line-text')->add(clang::__('Atau'));
            
            
            $html->appendln(parent::html($indent));
            return $html->text();
        }

    }
    