<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Jun 12, 2016
     * @license http://ittron.co.id ITtron
     */
    class CElement_Prelove_Card extends CElement {

        protected $product;

        public function __construct($id = "", $tag = "div") {
            parent::__construct($id, $tag);
        }

        public static function factory($id = "", $tag = "div") {
            return new CElement_Prelove_Card($id, $tag);
        }

        public function html($indent = 0) {
            $html = CStringBuilder::factory();
            $session = Session::instance();
            $sess_member_id = $session->get('member_id');

            // <editor-fold defaultstate="collapsed" desc="initialize">
            $product = $this->product;
            $end_bid_date = carr::get($product, 'end_bid_date');
            $start_bid_date = carr::get($product, 'start_bid_date');
            $name = carr::get($product, 'name');
            $user_last_bid_price = carr::get($product, 'user_last_bid_price');
            $start_price = carr::get($product, 'start_price');
            if (strlen($user_last_bid_price) == 0) {
                $user_last_bid_price = $start_price;
            }
            $price_buy_it_now = carr::get($product, 'price_buy_it_now');
            $already_buy = carr::get($product, 'already_buy');

            $product_id = carr::get($product, 'product_id');
            $member_id = carr::get($product, 'member_id');
            $status_transaction = carr::get($product, 'status_transaction');
            $total_bid = carr::get($product, 'total_bid', 0);
            $url_key = carr::get($product, 'url_key');
            $image_path = carr::get($product, 'image_path');
            $image_name = carr::get($product, 'image_name');
            $filename = carr::get($product, 'filename');
            $image = image::get_image_url($image_name);
            $url_product = curl::base() .'prelove/product/index/'. $url_key . '/' . $product_id;
            // </editor-fold>

            $wrapper = $this->add_div()->add_class('prod-card-wrapper');
            $wrapper->add('<a href="' . $url_product . '">');
            $prod_image = $wrapper->add_div()->add_class('prod-image');
            $product_info = $wrapper->add_div()->add_class('prod-info');
            $prod_name = $product_info->add_div()->add_class('prod-name');
            $prod_price_label = $product_info->add_div()->add_class('prod-price-lbl');
            $prod_price = $product_info->add_div()->add_class('prod-price');
            $prod_box = $product_info->add_div()->add_class('prod-box');
			$prod_image->add_attr('style', 'background-image:url('.$image.');background-position:center;background-repeat:no-repeat;background-size:cover;width:100%;height:200px');
            if (strlen($price_buy_it_now) > 0) {
                $prod_image->add_div()->add_class('buy-it-now-wrapper')
                        ->add(clang::__("BUY IT NOW") . " 
                            <span class='price'>Rp. " . ctransform::thousand_separator($price_buy_it_now).'</span>');
            }
            $prod_name->add($name);
            $prod_price_label->add(clang::__('Last Price'));
            $prod_price->add('Rp. ' . ctransform::thousand_separator($user_last_bid_price));

            $is_started = false;
            if (time() >= strtotime($start_bid_date)) {
                $is_started = true;
            }

            // time handling
            if ($is_started) {
                $time = prelove::time_diff($end_bid_date);
            }
            else {
                $time = prelove::time_diff($start_bid_date);

                $end_time = prelove::time_diff($end_bid_date, $start_bid_date);
                $end_hour = $end_time['hour'];
                $end_minutes = $end_time['minutes'];
                $end_second = $end_time['second'];
            }

            $hour = $time['hour'];
            $minutes = $time['minutes'];
            $second = $time['second'];

            $times_up = false;
            if ($hour == 0 && $minutes == 0 && $second == 0) {
                $times_up = true;
            }

            if (!$already_buy) {
                if ($is_started) {
                    // check if times still counting
                    if (!$times_up) {
                        $prod_box->add('<div class="lbl-time-mode" _st="end" d="' . $end_bid_date . '"> ' . clang::__('ENDING IN') . '</div>');
                    }
                }
                else {
                    $prod_box->add('
                    <div class="lbl-time-mode" _lb="' . clang::__('ENDING IN') . '"
                    _st="start" _h="' . $end_hour . '" _m="' . $end_minutes . '" _s="' . $end_second . '"> '
                            . clang::__('STARTING IN') . '</div>');
                }
            }

            // if product has been started and times up, so show the winner or show status END DEALS
            if (($is_started && $times_up) || $already_buy) {
                $end_offer_wrapper = $prod_box->add_div()->add_class('end-offer-wrapper');

                // check status_transaction first
                if ($status_transaction == 'DEFAULT' || $status_transaction == 'CLOSED') {
                    $end_offer_wrapper->add_div()->add(clang::__('END DEALS'));
                    $end_offer_wrapper->add_div()->add_class('end-offer-lbl')
                            ->add($total_bid . ' ' . clang::__('OFFER'));
                }
                else if ($status_transaction == 'SOLD') {
                    $winner = prelove_product::get_winner($product_id);
                    if ($winner == null) {
                        $end_offer_wrapper->add_div()->add(clang::__('END DEALS'));
                        $end_offer_wrapper->add_div()->add_class('end-offer-lbl')
                                ->add($total_bid . ' ' . clang::__('OFFER'));
                    }
                    else {
                        $member_name = carr::get($winner, 'member_name');
                        $end_offer_wrapper->add_div()->add(clang::__('WINNER'));
                        $end_offer_wrapper->add_div()->add_class('end-offer-lbl')
                                ->add($member_name);
                    }
                }
            }
            else {
                $prod_box->add('<div class="offer"><span class="offer-count">' . $total_bid . '</span><br/>' . clang::__('OFFER') . '</div>');
                $prod_box->add('<div class="time-left"><span class="hour">' . $hour . '</span><br/>' . clang::__('HOURS') . '</div>');
                $prod_box->add('<div class="time-left"><span class="minute">' . $minutes . '</span><br/>' . clang::__('MINS') . '</div>');
                $prod_box->add('<div class="time-left"><span class="second">' . $second . '</span><br/>' . clang::__('SECS') . '</div>');
            }
            $wrapper->add('</a>');

            $html->append(parent::html($indent));
            return $html->text();
        }

        public function js($indent = 0) {
            $js = CStringBuilder::factory();

            $js->append(parent::js($indent));
            return $js->text();
        }

        public function get_product() {
            return $this->product;
        }

        public function set_product($product) {
            $this->product = $product;
            return $this;
        }

    }
    