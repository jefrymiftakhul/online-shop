<?php

    /* 
    * To change this license header, choose License Headers in Project Properties.
    * To change this template file, choose Tools | Templates
    * and open the template in the editor.
    */
    class CElement_Prelove_HomeDeal extends CElement_Prelove_Slideshow {

        public function __construct($id = "", $tag = "div") {
            parent::__construct($id, $tag);

            $this->responsive = prelove_home::deals_responsive_size();
            $this->slide_to_scroll = 4;
            $this->slide_to_show = 4;
            $this->dots = "false";
        }

        public static function factory($id = "", $tag = "div") {
            return new CElement_Prelove_HomeDeal($id, $tag);
        }

        public function html($indent = 0) {
            $html = new CStringBuilder();

            $classes = $this->classes;
            $classes = implode(" ", $classes);
            if (strlen($classes) > 0) {
                $classes = " " . $classes;
            }

            $counter = 0;
            $html->append('<div id="' . $this->id . '" class="' . $classes . '">');
            foreach ($this->slides as $k => $v) {
                $image_path = carr::get($v, 'image_path');
                //$image_url = image::get_url($image_path);
                $html->appendln('<div>');
                $html->appendln('   <div>');
//                $html->appendln('       <img class="img-responsive" src="' . $image_url . '"/>');
                $html->appendln(CElement_Prelove_Card::factory()->set_product($v)->html());
                $html->appendln('   </div>');
                $html->appendln('</div>');
                
            }
            $html->append('</div>');
            $html->appendln(parent::html($indent));

            return $html->text();
        }

    }
    