<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Jun 13, 2016
     * @license http://ittron.co.id ITtron
     */
    class CElement_Prelove_Details extends CElement {

        protected $preview;
        protected $product;
		protected $url_product;

        public function __construct($id = "", $tag = "div") {
            parent::__construct($id, $tag);

            $this->preview = false;
        }

        public static function factory($id = "", $tag = "div") {
            return new CElement_Prelove_Details($id, $tag);
        }

        public function html($indent = 0) {
            // register zoomsl for zoom image product
            // CClientModules::instance()->register_module('zoomsl-3.0');

            $html = CStringBuilder::factory();
            $session = Session::instance();
            $db = CDatabase::instance();
            $sess_member_id = $session->get('member_id');
            // <editor-fold defaultstate="collapsed" desc="initialize">
            $product = $this->product;
            $end_bid_date = carr::get($product, 'end_bid_date');
            $start_bid_date = carr::get($product, 'start_bid_date');
            $name = carr::get($product, 'name');
            $condition = carr::get($product, 'condition');
            $deposit_hold = carr::get($product, 'deposit_hold');
            $price_increment = carr::get($product, 'price_increment');
            $user_last_bid_price = carr::get($product, 'user_last_bid_price');
            $start_price = carr::get($product, 'start_price');
            if (strlen($user_last_bid_price) == 0) {
                $user_last_bid_price = $start_price;
            }
            $already_buy = carr::get($product, 'already_buy');
            $price_buy_it_now = carr::get($product, 'price_buy_it_now');

            $product_id = carr::get($product, 'product_id');
            $member_id = carr::get($product, 'member_id');
            $status_transaction = carr::get($product, 'status_transaction');
            $total_bid = carr::get($product, 'total_bid', 0);
            $url_key = carr::get($product, 'url_key');
            $image_path = carr::get($product, 'image_path');
            $image_name = carr::get($product, 'image_name');
            $filename = carr::get($product, 'filename');
            $image = image::get_image_url($image_name);
            $url_product = curl::base() . $url_key . '-' . $product_id;

            $description = carr::get($product, 'description');
            $product_category_name = carr::get($product, 'product_category_name');
            $product_category_url_key = carr::get($product, 'product_category_url_key');
            $user_id_last_bid = carr::get($product, 'user_id_last_bid');
            $member_name_last_bid = '';
			$member_last_bid_ref_code = '';
            if (strlen($user_id_last_bid) > 0) {
                $member_last_bid = member::get_member_by_id($user_id_last_bid);
                // $member_last_bid_ref_code = cdbutils::get_value('select ref_code from member where member_id = '.$user_id_last_bid);
                if ($member_last_bid != null) {
                    $member_name_last_bid = cobj::get($member_last_bid, 'name');
                }
            }

            // $member_ref_code = carr::get($product, 'member_ref_code');
            $member_name = carr::get($product, 'member_name');
            $member_email = carr::get($product, 'member_email');
            $member_last_login = carr::get($product, 'member_last_login');
            $member_join_date = carr::get($product, 'member_join_date');
            $member_image = carr::get($product, 'member_image');
            // $member_image_url = image::get_url_member($member_image);
            $member_image_url = '';
            $province_name = carr::get($product, 'province_name');
            $city_name = carr::get($product, 'city_name');
            // </editor-fold>

            $container = $this->add_div()->add_class('prod-detail-content');

            // header
            $container_header_row = $container->add_div()->add_class('row header-detail');
            $breadcrumb_wrapper = $container_header_row->add_div()->add_class('col-md-12')
                            ->add_div()->add_class('breadcrumb-wrapper');

            // <editor-fold defaultstate="collapsed" desc="BREADCRUMB">
            $arr_breadcrumb = array();

            $url = curl::base().'prelove/home';
            $arr_breadcrumb[] = array(
                'label' => clang::__('Home'),
                'url' => $url
            );
            $url = curl::base() . 'prelove/product/category/' . $product_category_url_key;
            $arr_breadcrumb[] = array(
                'label' => $product_category_name,
                'url' => $url,
            );
            $arr_breadcrumb[] = array(
                'label' => $name,
                'url' => '#',
                'current' => true,
            );
            prelove::render_breadcrumb($breadcrumb_wrapper, $arr_breadcrumb);
            // </editor-fold>
            // main content
            $container_row = $container->add_div()->add_class('row prod-main-content');
            $left_content = $container_row->add_div()->add_class('col-md-4 text-center left-product-info');
            $mid_content = $container_row->add_div()->add_class('col-md-5 prod-detail-info center-product-info');
            $right_content = $container_row->add_div()->add_class('col-md-3');

            // bottom row
            $container_bottom_row = $container->add_div()->add_class('row btm-row');
            $description_content = $container_bottom_row->add_div()->add_class('col-md-12');
//			$image = str_replace('.com', '.local', $image);
            $div_overflow = $left_content->add_div()->add_class('product-wrapper-over');
//            $div_overflow->add('<img src="' . $image . '" class="img-product" style="height:340px;margin:0 auto;"/>');
			$data_image = product::get_product_image($product_id);

            $element_galery_product = $div_overflow->add_element('prelove-galery-product', 'galery-product-' . $product_id);
            $element_galery_product->set_images($data_image);

			// strip tags to avoid breaking any html
			$name_subbed = strip_tags($name);
//			if (strlen($name) > 50) {
//
//				// truncate string
//				$stringCut = substr($name, 0, 50);
//
//				// make sure it ends in a word so assassinate doesn't become ass...
//				$name_subbed = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
//			}
            $product_name = $mid_content->add_div()->add_class('prod-detail-name');
            $product_name->add($name_subbed);
            $product_cond = $mid_content->add_div()->add_class('prod-condition');
            $product_cond->add(clang::__('Condition') . ' : ' . clang::__(ucfirst(strtolower($condition))));
            
            //$product_waranty = $mid_content->add_div()->add_class('prod-waranty');
            //$product_waranty->add(clang::__('Garansi') . ' : Rp. ' .  ctransform::thousand_separator($deposit_hold));
            
            $user_last_bid_price_label = 'Rp. ' . ctransform::thousand_separator($user_last_bid_price);
            $prod_price_label = $mid_content->add_div()->add_class('prod-detail-price-lbl');
			$price_label_row = $mid_content->add_div()->add_class('row');
			$row_label_left = $price_label_row->add_div()->add_class('col-xs-9');
			$row_label_right = $price_label_row->add_div()->add_class('col-xs-3');
//            $product_price = $row_label_left->add_div()->add_class('prod-detail-price');
            $product_price_right = $row_label_right->add_div()->add_class('prod-detail-price');
            if (strlen($member_name_last_bid) > 0) {
//                $product_email = $mid_content->add_div()->add_class('prod-member-email');
//                $product_email->add(clang::__('By') . ' : ' . $member_name_last_bid);
			  $member_name_last_bid = ' <a class="link-prelove" href="'.curl::base() . 'user/' . $member_last_bid_ref_code.'">('.$member_name_last_bid.')</a>';
            }

            if (strlen($price_buy_it_now) > 0) {
                $product_buy_price_label = $mid_content->add_div()->add_class('prod-detail-price-lbl');
                $product_buy_price = $mid_content->add_div()->add_class('prod-detail-price');
            }

            $prod_box = $mid_content->add_div()->add_class('prod-box');

            $row_label_left->add(clang::__('Last Price'));
			$row_label_left->add_br();
            $row_label_left->add('<span class="prod-detail-price">'.$user_last_bid_price_label .$member_name_last_bid.'</span>');
			$row_label_left->add_br();

            if (strlen($price_buy_it_now) > 0) {
                $row_label_left->add(clang::__('Buy It Now'));
				$row_label_left->add_br();
                $row_label_left->add('<span class="prod-detail-price">Rp. ' . ctransform::thousand_separator($price_buy_it_now).'</span>');				
            }

            $is_started = false;
            if (time() >= strtotime($start_bid_date)) {
                $is_started = true;
            }

            // time handling
            if ($is_started) {
                $time = prelove::time_diff($end_bid_date);
            }
            else {
                $time = prelove::time_diff($start_bid_date);

                $end_time = prelove::time_diff($end_bid_date, $start_bid_date);
                $end_hour = $end_time['hour'];
                $end_minutes = $end_time['minutes'];
                $end_second = $end_time['second'];
            }

            $hour = $time['hour'];
            $minutes = $time['minutes'];
            $second = $time['second'];

            $times_up = false;
            if ($hour == 0 && $minutes == 0 && $second == 0) {
                $times_up = true;
            }

            if (!$already_buy) {
                if ($is_started) {
                    // check if times still counting
                    if (!$times_up) {
                        $prod_box->add('<div class="lbl-time-mode"> ' . clang::__('ENDING IN') . '</div>');
                    }
                }
                else {
                    // This deal is not started yet
                    // starting in..
                    $prod_box->add('
                        <div class="lbl-time-mode" _lb="' . clang::__('ENDING IN') . '"
                        _st="start" _h="' . $end_hour . '" _m="' . $end_minutes . '" _s="' . $end_second . '"> '
                            . clang::__('STARTING IN') . '</div>');
                }
            }

            // if product has been started and times up, so show the winner or show status END DEALS
            if (($is_started && $times_up) || $already_buy) {
				$prod_box->add_br();
                $end_offer_wrapper = $prod_box->add_div()->add_class('end-offer-wrapper');

                // check status_transaction first
                if ($status_transaction == 'DEFAULT' || $status_transaction == 'CLOSED') {
                    $end_offer_wrapper->add_div()->add(clang::__('END DEALS'));
                    $end_offer_wrapper->add_div()->add_class('end-offer-lbl')
                            ->add($total_bid . ' ' . clang::__('OFFER'));
                }
                else if ($status_transaction == 'SOLD') {
                    $winner = prelove_product::get_winner($product_id);
                    if ($winner == null) {
                        $end_offer_wrapper->add_div()->add(clang::__('END DEALS'));
                        $end_offer_wrapper->add_div()->add_class('end-offer-lbl')
                                ->add($total_bid . ' ' . clang::__('OFFER'));
                    }
                    else {
                        $member_winner_name = carr::get($winner, 'member_name');
                        $end_offer_wrapper->add_div()->add(clang::__('WINNER'));
                        $end_offer_wrapper->add_div()->add_class('end-offer-lbl')
                                ->add($member_winner_name);
                    }
                }
            }
            else {
				$product_price_right->add('<div class="offer"><div class="offer-count">' . $total_bid . '</div><div>' . clang::__('OFFER') . '</div></div>');
//                $prod_box->add('<div class="offer"><span class="offer-count">' . $total_bid . '</span><br/>' . clang::__('OFFER') . '</div>');
                $prod_box->add('<div class="time-left"><span class="hour">' . $hour . '</span><br/>' . clang::__('HOURS') . '</div>');
                $prod_box->add('<div class="time-left"><span class="minute">' . $minutes . '</span><br/>' . clang::__('MINS') . '</div>');
                $prod_box->add('<div class="time-left"><span class="second">' . $second . '</span><br/>' . clang::__('SECS') . '</div>');
            }

            if ($this->preview) {
                $preview_wrapper = $mid_content->add_div()->add_class('preview-wrapper');
                $preview_wrapper->add_action()->set_label('back to member area')->add_class('btn-prelove')
                        ->set_link(curl::base() . 'member/upload/productList');
            }

            if ($is_started && !$already_buy) {
                if (!$times_up && ($sess_member_id !== $member_id)) {
					$btn_wrapper = $mid_content->add_div()->add_class('btn_wrapper');
					$btn_offer = $btn_wrapper->add_action()->set_label('OFFER')->set_submit(false)->add_class('btn-modal btn-prelove btn-offer');
					if(strlen($price_buy_it_now) == 0){
					  $btn_offer->custom_css('width','100%');
					}
                    $pid = (string) $product_id;
                    $pid = security::encrypt($pid);
                    if (strlen($sess_member_id) == 0) {
                        // $btn_offer->add_class("btn-modal");
//                        $btn_offer->add_attr('data-target', 'modal-body-login');
//                        $btn_offer->add_attr('data-modal', 'login_form');
						$btn_offer->add_listener('click')->add_handler('custom')->set_js("
							$('#modal-offer-nologin').modal('show');
						");
						$modal_wrapper = $btn_wrapper->add_div('modal-offer-nologin')->add_class('modal fade  modal-prelove modal-offer modal-prelove')->add_attr('role','dialog')->custom_css('display','none');
						$modal_dialog = $modal_wrapper->add_div()->add_class('modal-dialog')->add_attr('role','document');
						$modal_content = $modal_dialog->add_div()->add_class('modal-content');
						$modal_header = $modal_content->add_div()->add_class('modal-header');
						$modal_header->add('<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">×</span>
										</button>
										<h4 class="modal-title">PLEASE LOGIN</h4>');
						$modal_body = $modal_content->add_div()->add_class('modal-body');
						$modal_body_content = $modal_body->add_div()->add_class('offer-content');
						$modal_body_content->add_div()->add_class('text_info')->add(clang::__('Silahkan login member terlebih dahulu sebelum melakukan offer atau beli.'));
						$modal_body_content->add('<a href="http://'.ccfg::get('domain_admin').'/" taget="_blank" class="btn btn-login-dialog btn-dialog-prelove">LOGIN</a>');
                    }
                    else {
                        $btn_offer->add_listener('click')->add_handler('custom')->set_js("
                            $('#modal-offer').modal('show');
                        ");
                        // $btn_offer->add_attr('data-toggle', 'modal');
                        $btn_offer->add_attr('data-target', '#modal-offer');
                        $mid_content->add_element('product-offer-modal', 'offer')->set_product_id($pid)
                                ->set_price_increment($price_increment)
                                ->set_user_last_bid_price($user_last_bid_price);
                    }


                   if (strlen($price_buy_it_now) > 0) {
                       $btn_buy = $btn_wrapper->add_action()->set_label('BUY IT NOW')->set_submit(false)->add_class('btn-prelove btn-buy-it-now');
                       if (strlen($sess_member_id) == 0) {
                           $btn_buy->add_listener('click')->add_handler('custom')->set_js("
							   $('#modal-offer-nologin').modal('show');
						");
                       }
                       else {
                           $btn_buy->add_listener('click')->add_handler('custom')->set_js("
                               $('#modal-buy_now').modal('show');
                           ");
                           // $btn_buy->add_attr('data-toggle', 'modal');
                           $btn_buy->add_attr('data-target', '#modal-buy_now');
                           $mid_content->add_element('product-offer-modal', 'buy_now')->set_product_id($pid)
                                   ->set_mode('buy')->set_price_buy_it_now($price_buy_it_now);
                       }

                   }

//                    $file = CF::get_file('data', 'setting');
//                    if (file_exists($file)) {
//                        $setting = include $file;
//                        $failed_commission = carr::get($setting, 'failed_commission');
//                        $failed_fee = carr::get($failed_commission, 'failed_fee');
//
//                        $price = $start_price;
//                        if (strlen($price_buy_it_now) > 0) {
//                            $price = $price_buy_it_now;
//                        }
//                        
//                        $percent_failed_fee = carr::get($failed_fee, 'fee');
//                        $failed_fee = ($price * $percent_failed_fee) / 100;
//
//                        $guarantee_content = $mid_content->add_div()->add_class('guarantee-content');
//                        $guarantee_content->add($failed_fee);
//                    }
                }
                else {
                    if ($sess_member_id != null && $sess_member_id == $member_id) {
                        // if member see ur own product
                        $mid_content->add_control('', 'label')->set_value(clang::__('You cant offer your own product'));
                    }
                }
            }
            // $data_total = prelove_seller::get_total_transaction($member_id);
			$total_product = prelove_seller::get_total_product($member_id);			
			$total_success = prelove_seller::get_total_success_transaction($member_id);
//            $total_settle = carr::get($data_total, 'SETTLE', 0);
//            $total_failed = carr::get($data_total, 'FAILED', 0);
			
            $total_transaction = $total_success . '/' .$total_product;

            // right content
			
			
			$detail_prelove_product = $right_content->add_element('prelove-detail-info');
			$detail_prelove_product->set_product_id($product_id);
            $detail_prelove_product->set_url_shared_twitter(curl::httpbase().$this->url_product);
            $detail_prelove_product->set_url_shared_facebook(curl::httpbase().$this->url_product);
            $detail_prelove_product->set_url_shared_google(curl::httpbase().$this->url_product);
			 
//            $seller_wrapper = $right_content->add_div()->add_class('seller-wrapper');
//            $seller_wrapper->add_div()->add_class('title txt-center')->add(clang::__('SELLER'));
//            $seller_content = $seller_wrapper->add_div()->add_class('seller-content');
////            $seller_content->add_div()->add_class('image')
////                    ->add('<img src="' . $member_image_url . '" class="img-responsive"/>');
//            $seller_content->add_div()->add_class('seller-info-image')->add_attr('style','margin:20px auto;padding-top:20px;width:100px;height:100px;background:url('.$member_image_url.');background-position:center;background-size:cover;');
//            $seller_name = $seller_content->add_div()->add_class('seller-name txt-center');
////            $seller_name->add($member_name);
//            // $seller_name->add_action()->set_label($member_name)
//            //         ->set_link(curl::base() . 'user/' . $member_ref_code);
//            $seller_name = $seller_content->add_div()->add_class('seller-address txt-center');
//            if (strlen($province_name) > 0 && strlen($city_name) > 0) {
//                $seller_name->add($province_name . ', ' . $city_name);
//            }
//            $seller_info = $seller_content->add_div()->add_class('seller-info txt-center');
//            $seller_info->add(clang::__('Last Online') . ': ' . date("d F Y", strtotime($member_last_login)));
//            $seller_info->add_br();
//            $seller_info->add(clang::__('Join') . ': ' . date("d F Y", strtotime($member_join_date)));
//            $seller_info->add_br();
//            $seller_info->add(clang::__('Product Sold') . ': ' . $total_transaction);

            // $seller_info->add_br();
            // $seller_info->add_action()->set_label('SEND MESSAGE')->add_class('btn-prelove')->set_link('#');
            
            // DESCRIPTION ROW
            $desc_wrapper = $description_content->add_div()->add_class('description-wrapper');
            $desc_wrapper->add('<h4 class="product_name_description">'.$name.'<h4>');
            $desc_wrapper->add('<h4 class="title">' . clang::__('Description') . '</h4>');
            $desc_wrapper->add(nl2br($description));

            // <editor-fold defaultstate="collapsed" desc="PRODUCT SELLER">
            $options = array();
            $options['limit_start'] = 0;
            $options['limit_end'] = 8;
            $options['sord'] = prelove_product::__ORD_LASTOFFER;
            $options['member_id'] = $member_id;
            $options['except_product_id'] = $product_id;
            $options['deal_types'] = prelove_product::__AVAILABLE_OFFER;
            $result_product_seller = prelove_product::get_results($options);
            $total_product_seller = $result_product_seller['total'];
            $product_seller_list = $result_product_seller['products'];
            if ($total_product_seller > 0) {
                // Product seller container
                $col_seller = $container->add_div()->add_class('col-sm-12 home-deal-prelove product-seller');
                $col_seller->add_div()->add(
                        '<h4 class="deal-title">' 
                            .'<div class="deal-title-wrapper">' 
                                .clang::__('PRODUCT SELLER') 
                            .'</div>' 
                        .'</h4>');
                $col_seller->add_element('home-deal')->add_class('def-slider')
                        ->set_slides($product_seller_list);
            }
            // </editor-fold>

            $html->append(parent::html($indent));
            return $html->text();
        }

        public function js($indent = 0) {
            $js = CStringBuilder::factory();

            // <editor-fold defaultstate="collapsed" desc="JS IMAGE">
            $js->appendln('
                jQuery(".img-product").imagezoomsl({
                    zoomrange: [3,3],
                    magnifiersize: [350, 500],
                    innerzoom: true,                    
                });');
            // </editor-fold>



            $js->append(parent::js($indent));
            return $js->text();
        }

        public function get_product() {
            return $this->product;
        }
		
		public function set_url_product($url_product) {
			$this->url_product = $url_product;
		}

		
        public function set_product($product) {
            $this->product = $product;
            return $this;
        }

        public function get_preview() {
            return $this->preview;
        }

        public function set_preview($preview) {
            $this->preview = $preview;
            return $this;
        }

    }
    