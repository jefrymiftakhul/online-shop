<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Mar 23, 2016
     */
    class CElement_Prelove_SlideHome extends CElement_Slideshow {
        
        public function __construct($id = "", $tag = "div") {
            parent::__construct($id, $tag);
            
            $this->dots = "false";
            $this->infinite = "true";
            $this->autoplay = true;
            $this->autoplay_speed = 5000;
        }
        
        public static function factory($id = "", $tag = "div") {
            return new CElement_Prelove_SlideHome($id, $tag);
        }
        
        public function html($indent = 0){
            $html = new CStringBuilder();

            $classes = $this->classes;
            $classes = implode(" ", $classes);
            if (strlen($classes) > 0) {
                $classes = " " . $classes;
            }
            
            if (count($this->slides) == 0) {
                $html->append('<div class="home-no-slide">');
                $html->append('</div>');
                
            }

            $html->append('<div id="' . $this->id . '" class="' .$classes .'">');
            foreach ($this->slides as $k => $v) {
                $image_url = carr::get($v, 'image_url');
                $url_link = carr::get($v, 'url_link');
                $html->appendln('<div>');
                $html->appendln('   <h3>');
                if (strlen($url_link) > 0) {
                    $html->appendln('   <a href="' .$url_link .'" target="__blank">');
                }
                $html->appendln('       <img class="img-responsive" src="' . $image_url . '"/>');
                if (strlen($url_link) > 0) {
                    $html->appendln('   </a>');
                }
                $html->appendln('   </h3>');
                $html->appendln('</div>');
            }
            $html->append('</div>');
            $html->appendln(parent::html($indent));

            return $html->text();
        }
    }
    