<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Jun 14, 2016
     * @license http://ittron.co.id ITtron
     */
    class CElement_Prelove_SearchResults extends CElement {

        protected $product_result;
        protected $total_result;
        protected $limit_show_page;
        protected $item_per_page;
        protected $page_start;
        protected $current_page;
        protected $query;
        protected $target;
        protected $reload;
        protected $url;
        protected $product_element;
        protected $total_item_per_row;
        
        public function __construct($id = "", $tag = "div") {
            parent::__construct($id, $tag);
            
            $this->page_start = 1;
            $this->limit_show_page = 7;
            $this->current_page = 1;
            $this->reload = false;
            $this->target = $this->id . '-result-content';
            $this->query = '';
            $this->url = curl::base() ;
            $this->product_element = 'product-card';
            $this->total_item_per_row = 3;
        }

        public static function factory($id = "", $tag = "div") {
            return new CElement_Prelove_SearchResults($id, $tag);
        }

        public function html($indent = 0) {
            $html = CStringBuilder::factory();
            
            $product = $this->product_result;
            
            $result_container = $this->add_div($this->id . '-result-content')->add_class('result-content');
            $result_container->add_control('q', 'hidden')->set_value($this->query);
            $result_container->add_control('current_page', 'hidden')->set_value($this->current_page);
            $result_container->add_control('per_page', 'hidden')->set_value($this->item_per_page);

            $result_row = $result_container->add_div()->add_class('row');
            foreach ($product as $product_k => $product_v) {				//cdbg::var_dump($product_v);
                if ($this->total_item_per_row == 3) {
                    $result_item = $result_row->add_div()->add_class('col-lg-4 col-md-6 col-sm-6 col-xs-6');
                }
                $result_item->add_element($this->product_element)->set_product($product_v);
            }
            if ($this->total_result > 0) {
                $this->pagination($result_container);
            }
            
            $html->append(parent::html($indent));
            return $html->text();
        }
        
        protected function pagination(&$result_container) {
            $limit_show_page = $this->limit_show_page;

            $item_per_page = $this->item_per_page;
            $page_start = $this->page_start;
            $page_end = $this->limit_show_page;
            $total_result = $this->total_result;
            $page = $this->current_page;

            $result_container->add_element('pagination')
                    ->set_total_result($total_result)
                    ->set_item_per_page($item_per_page)
                    ->set_page_start($page_start)
                    ->set_current_page($page);
        }

        public function js($indent = 0) {
            $js = CStringBuilder::factory();

            $vjs = CView::factory('js/product');
            $vjs->target = $this->target;
            $vjs->reload = $this->reload;
            $vjs->url = $this->url;
            $vjs_val = str_replace('<script>', '', $vjs->render());
            $js->appendln($vjs_val);
            
            $js->append(parent::js($indent));
            return $js->text();
        }
        
        
        public function get_product_result() {
            return $this->product_result;
        }

        public function get_total_result() {
            return $this->total_result;
        }

        public function get_limit_show_page() {
            return $this->limit_show_page;
        }

        public function get_item_per_page() {
            return $this->item_per_page;
        }

        public function get_page_start() {
            return $this->page_start;
        }

        public function set_product_result($product_result) {
            $this->product_result = $product_result;
            return $this;
        }

        public function set_total_result($total_result) {
            $this->total_result = $total_result;
            return $this;
        }

        public function set_limit_show_page($limit_show_page) {
            $this->limit_show_page = $limit_show_page;
            return $this;
        }

        public function set_item_per_page($item_per_page) {
            $this->item_per_page = $item_per_page;
            return $this;
        }

        public function set_page_start($page_start) {
            $this->page_start = $page_start;
            return $this;
        }

        public function get_current_page() {
            return $this->current_page;
        }

        public function set_current_page($current_page) {
            $this->current_page = $current_page;
            return $this;
        }

        public function get_query() {
            return $this->query;
        }

        public function set_query($query) {
            $this->query = $query;
            return $this;
        }

        public function get_target() {
            return $this->target;
        }

        public function get_reload() {
            return $this->reload;
        }

        public function set_target($target) {
            $this->target = $target;
            return $this;
        }

        public function set_reload($reload) {
            $this->reload = $reload;
            return $this;
        }

        public function get_url() {
            return $this->url;
        }

        public function set_url($url) {
            $this->url = $url;
            return $this;
        }

        public function get_product_element() {
            return $this->product_element;
        }

        public function set_product_element($product_element) {
            $this->product_element = $product_element;
            return $this;
        }

        public function get_total_item_per_row() {
            return $this->total_item_per_row;
        }

        public function set_total_item_per_row($total_item_per_row) {
            $this->total_item_per_row = $total_item_per_row;
            return $this;
        }

    }
    