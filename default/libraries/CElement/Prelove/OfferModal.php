<?php

/**
 *
 * @author Raymond Sugiarto
 * @since  Jun 14, 2016
 * @license http://ittron.co.id ITtron
 */
class CElement_Prelove_OfferModal extends CElement {

    protected $product_id;    
    protected $price_increment;
    protected $user_last_bid_price;
    protected $price_buy_it_now;
    protected $mode;
    protected $generated_modal_id;

    public function __construct($id = "", $tag = "div") {
        parent::__construct($id, $tag);

        $this->mode = 'offer';
    }

    public static function factory($id = "", $tag = "div") {
        return new CElement_Prelove_OfferModal($id, $tag);
    }
   
    public function html($indent = 0) {
        $html = CStringBuilder::factory();

        if ($this->mode == 'offer') {
            $title = clang::__('OFFER');
        }
        else if ($this->mode == 'buy') {
            $title = clang::__('BUY IT NOW');
        }

        $container = $this->add_control('modal-' . $this->id, '62hall-modal')
                        ->add_class('modal-offer modal-prelove')->set_title($title);
        $content = $container->add_div()->add_class('offer-content');
        $form = $content->add_form()->add_class('form-prelove form-offer');
        $db = CDatabase::instance();

        if ($this->mode == 'offer') {
            $this->generated_modal_id = 'modal-offer';
            $form_row = $form->add_div()->add_class('row price-component');
            $form_row->add_div()->add_class('col-md-5 col-sm-5')->add(clang::__('INCREMENT BID'));
            $form_row->add_div()->add_class('col-md-1 col-sm-1 txt-right hidden-xs')->add(':');
            $form_row->add_div()->add_class('col-md-5 col-sm-5pl-0')->add('Rp. ' . ctransform::thousand_separator($this->price_increment));

            $last_price = $this->user_last_bid_price + $this->price_increment;
            $form_row = $form->add_div()->add_class('row price-component');
            $form_row->add_div()->add_class('col-md-5 col-sm-5')->add(clang::__('TOTAL PRICE'));
            $form_row->add_div()->add_class('col-md-1 col-sm-1 txt-right hidden-xs')->add(':');
            $form_row->add_div()->add_class('col-md-5 col-sm-5 pl-0')->add('Rp. <span class="total-price">' . ctransform::thousand_separator($last_price) . '</span>');

            //$ongkir = $form->add_element('shipping-raja-ongkir','test_offer');
			//$ongkir->set_product_id(security::decrypt($this->get_product_id()));

            $wrapper = $form->add_div()->add_class('price-increment-wrapper');
            $max_increment = 5;
            for ($i = 1; $i <= 5; $i++) {
                $selected = '';
                if ($i == 1) {
                    $selected = 'selected';
                }
                $item = $wrapper->add_div()->add_class('item ' . $selected)->add($i . 'X');
                $item->add_listener('click')->add_handler('custom')
                        ->set_js("
                                jQuery(this).parents('.price-increment-wrapper').find('.item.selected').removeClass('selected');
                                jQuery(this).addClass('selected');
                                jQuery(this).parents('.form-offer').find('#qty_bid').val('" . $i . "');
                                jQuery(this).parents('.form-offer').find('.total-price').html(
                                    $.cresenity.thousand_separator(" . ($this->user_last_bid_price + ($this->price_increment * $i)) . "));
                            ");
            }

            $form->add_control('qty_bid', 'hidden')->set_value('1');
            $form->add_control('last_bid', 'hidden')->set_value($this->user_last_bid_price);
            $form->add_control('pid', 'hidden')->set_value($this->product_id);
            $label = clang::__('OFFER NOW');
            $url = curl::base() . 'prelove/order/bid';
            $param_input = array('last_bid', 'pid', 'qty_bid');
        }
        else if ($this->mode == 'buy') {
            $this->generated_modal_id = 'modal-buy_now';
            $form->add_div()->add_class('txt-center')->add(clang::__("Are you sure buy this product ?"));
            $form->add_div()->add_class('price-buy-it-now')->add(clang::__('Price') . ' : Rp.'
                    . ctransform::thousand_separator($this->price_buy_it_now));
			//$shipping = $form->add_element('shipping-raja-ongkir','buy')->set_product_id(security::decrypt($this->get_product_id()));
            $label = clang::__('BUY IT NOW');
            $param_input = array('pid');
            $url = curl::base() . 'prelove/order/buyNow';
        }        
        $btn_offer_now = $form->add_action()->set_label($label)->set_submit(false)
                ->add_class('btn-dialog-prelove btn-offer-now');
        // $btn_offer_now->add_listener('click')->add_handler('dialog')
        //         ->set_method('post')->set_title(' ')
        //         ->set_js_class('cresenity')
        //         ->set_target('offer-information-' . $this->id)
        //         ->set_url($url)
        //         ->add_param_input($param_input);

                // $button_js = "var data = {};
                // // $('#".$this->generated_modal_id."').modal('hide');
                // if ($('#last_bid').val() !== undefined){
                //   data['last_bid'] = $('#last_bid').val();
                // }
                // if ($('#pid').val() !== undefined){
                //   data['pid'] = $('#pid').val();
                // }
                // if ($('#qty_bid').val() !== undefined){
                //   data['qty_bid'] = $('#qty_bid').val();
                // }                
                // $.cresenity.show_dialog('offer-information-".$this->id."','" .$url . "','post',' ',data);";

                $button_js = "var data = {};
                // $('#".$this->generated_modal_id."').modal('hide');
                if ($('#last_bid').val() !== undefined){
                    data['last_bid'] = $('#last_bid').val();
                }
                if ($('#pid').val() !== undefined){
                    data['pid'] = $('#pid').val();
                }
                if ($('#qty_bid').val() !== undefined){
                    data['qty_bid'] = $('#qty_bid').val();
                }
                var a = $.cresenity.show_dialog('offer-information-".$this->id."','" .$url . "','post',' ',data);
                console.log(a);";

                $javascript = "
                    $.ajax({
                        method : 'POST',
                        url : '".$url."',
                        data : {
                            last_bid:$('#last_bid').val(),
                            pid:$('#pid').val(),
                            qty_bid:$('#qty_bid').val()
                        }
                    }).done(function(msg){
                        msg = JSON.parse(msg);
                            var bid = msg['bid'];
                            var result = msg['result'];
                            var title = 'ERROR';
                            if(msg['error'] == 0){
                                title = 'SUCCESS';
                                result = 'Penawaran Anda telah kami terima dengan nominal Rp. '+bid;
                            }
                            $(document.body).append('<div class=\"dialog-information\" style=\"position:fixed;width:100%;height:100%;text-align:center;z-index:99999;background:rgba(0,0,0,0.2);\"><div class=\"content-wrapper\"><h4 class=\"modal-title\">'+title+'</h4><div class=\"content-dialog-information\">'+result+'</div><a href=\"#\" class=\"btn-dialog\" onclick=\"location.reload();\">OK</a></div></div>');
                    });
                ";

        // $btn_offer_now->add_listener('click')->add_handler('custom')
        //         ->set_js($button_js);
        $btn_offer_now->add_listener('click')->add_handler('custom')
                ->set_js($javascript);
        $form->add_div('offer-information-' . $this->id)->add_class('offer-information');
        $div_info_ongkir = $form->add_div()->add_class('ongkir_info');
        $div_info_ongkir->add(clang::__(')* Harga belum termasuk ongkir. Ongkir di tanggung pemenang'));
        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = CStringBuilder::factory();
        $js->append(parent::js($indent));
        return $js->text();
    }

    public function get_product_id() {
        return $this->product_id;
    }

    public function get_price_increment() {
        return $this->price_increment;
    }

    public function set_product_id($product_id) {
        $this->product_id = $product_id;
        return $this;
    }

    public function set_price_increment($price_increment) {
        $this->price_increment = $price_increment;
        return $this;
    }

    public function get_user_last_bid_price() {
        return $this->user_last_bid_price;
    }

    public function set_user_last_bid_price($user_last_bid_price) {
        $this->user_last_bid_price = $user_last_bid_price;
        return $this;
    }

    public function get_price_buy_it_now() {
        return $this->price_buy_it_now;
    }

    public function set_price_buy_it_now($price_buy_it_now) {
        $this->price_buy_it_now = $price_buy_it_now;
        return $this;
    }

    public function get_mode() {
        return $this->mode;
    }

    public function set_mode($mode) {
        $this->mode = $mode;
        return $this;
    }

}
