<?php

class CElement_Prelove_DetailInfo extends CElement
{

    private $url_shared_facebook  = null;
    private $url_shared_twitter   = null;
    private $url_shared_google    = null;
    private $text_twitter         = '';
    private $url_shared_instagram = null;
    private $info_pengiriman      = null;
    private $product_id           = '';
    private $shipping_day         = '(2-28 hari)';

    public function __construct($id = '')
    {
        parent::__construct($id);
    }

    public static function factory($id = '')
    {
        return new CElement_Prelove_DetailInfo($id);
    }

    public function set_url_shared_facebook($url)
    {
        $this->url_shared_facebook = $url;
        return $this;
    }

    public function set_url_shared_twitter($url)
    {
        $this->url_shared_twitter = $url;
        return $this;
    }

    public function set_url_shared_google($url)
    {
        $this->url_shared_google = $url;
        return $this;
    }

    public function set_text_twitter($text)
    {
        $this->text_twitter = $text;
        return $this;
    }

    public function set_url_shared_instagram($url)
    {
        $this->url_shared_instagram = $url;
        return $this;
    }

    public function set_info_pengiriman($info_pengiriman)
    {
        $this->info_pengiriman = $info_pengiriman;
        return $this;
    }

    public function set_product_id($id)
    {
        $this->product_id = $id;
        return $this;
    }

    public function html($indent = 0)
    {

        $html = new CStringBuilder();
        $html->set_indent($indent);
		$db = CDatabase::instance();
		$q = 'select p.*, m.name as member_name
			from product p 
			left join member m on m.member_id = p.member_id
			where p.status > 0 
			and p.product_id ='.$db->escape($this->product_id);
		$data_product = cdbutils::get_row($q);
		$this->text_twitter = cobj::get($data_product,'name');
		// initial variable from product
		$member_id = cobj::get($data_product,'member_id');
		$member_name = cobj::get($data_product,'member_name');
		$province_id = cobj::get($data_product,'province_id');
		$city_id = cobj::get($data_product,'city_id');
		$province = cdbutils::get_value('select name from province where status > 0 and province_id = '.$db->escape($province_id));
		$city = cdbutils::get_value('select name from city where status > 0 and city_id = '.$db->escape($city_id));
		
		// get count success transaction and total product from seller
		$total_product = prelove_seller::get_total_product($member_id);			
		$total_success = prelove_seller::get_total_success_transaction($member_id);
		$accordion = $this->add_div('accordion')->add_class('panel-group accordion-detail-prelove')->add_attr('role','tablist')->add_attr('aria-multiselectable','true');
		// collapse number #1
		$panel_default = $accordion->add_div()->add_class('panel panel-default');
			// heading
			$panel_heading = $panel_default->add_div('headingShare')->add_class('panel-heading')->add_attr('role','tab');
			$panel_heading->add('<h4 class="panel-title">
					  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseShare" aria-expanded="true" aria-controls="collapseShare">
						Share This Product
					  </a>
					</h4>');
			// content
			$panel_content = $panel_default->add_div('collapseShare')->add_class('panel-collapse collapse')->add_attr('role','tabpanel')->add_attr('aria-labelledby','headingShare');
				$body_panel_content = $panel_content->add_div()->add_class('panel-body');
					$center_body_panel_content = $body_panel_content->add_div()->add_class('text-center');
					$center_body_panel_content->add_div()->add_class('icon-facebook-red margin-right-10 facebook link')->custom_css('display','inline-block');
					$center_body_panel_content->add_div()->add_class('icon-twitter-red margin-right-10 twitter link')->custom_css('display','inline-block');
					$center_body_panel_content->add_div()->add_class('icon-google-red margin-right-10 google link')->custom_css('display','inline-block');
		// collapse number #2
		$panel_default = $accordion->add_div()->add_class('panel panel-default');
			// heading
			$panel_heading = $panel_default->add_div('headingBarang')->add_class('panel-heading')->add_attr('role','tab');
			$panel_heading->add('<h4 class="panel-title">
					  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseBarang" aria-expanded="true" aria-controls="collapseBarang">
						Informasi Asal Barang
					  </a>
					</h4>');
			// content
			$panel_content = $panel_default->add_div('collapseBarang')->add_class('panel-collapse collapse')->add_attr('role','tabpanel')->add_attr('aria-labelledby','headingBarang');
				$body_panel_content = $panel_content->add_div()->add_class('panel-body');
					$body_panel_content->add('Lokasi Produk : '.$city.','.$province);
		// collapse number #3
		$panel_default = $accordion->add_div()->add_class('panel panel-default');
			// heading
			$panel_heading = $panel_default->add_div('headingSeller')->add_class('panel-heading')->add_attr('role','tab');
			$panel_heading->add('<h4 class="panel-title">
					  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeller" aria-expanded="true" aria-controls="collapseSeller">
						Informasi Seller
					  </a>
					</h4>');
			// content
			$panel_content = $panel_default->add_div('collapseSeller')->add_class('panel-collapse collapse in')->add_attr('role','tabpanel')->add_attr('aria-labelledby','headingSeller');
				$body_panel_content = $panel_content->add_div()->add_class('panel-body');
					$body_panel_content->add(clang::__('Nama :').' '.$member_name.'<br>
					  '.clang::__('Telah Menjual :').' '.$total_success.'<br>
					  '.clang::__('Total Produk :').' '.$total_product.'<br>');
        
        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0)
    {
        $js = new CStringBuilder();

        $js->appendln("
                $('.tombol').click(function(){
                    var id = $(this).attr('id');
                    $('.container-'+id).toggle();
                });
               ");
        $text_twitter = str_replace("\r\n", '', $this->text_twitter);
        $text_twitter = strip_tags($text_twitter);
        $js->appendln("
            $('.facebook').click(function(){
                var fbpopup = window.open('http://www.facebook.com/sharer/sharer.php?u=" . $this->url_shared_facebook . "', 'pop', 'width=600, height=400, scrollbars=no');
                return false;
            });
            $('.twitter').click(function(){
                                var twpopup = window.open(\"//twitter.com/intent/tweet?url=" . $this->url_shared_twitter . "&text=" . $text_twitter . "&original_referer=" . $this->url_shared_twitter . "\",600, 400);
                return false;
            });
             $('.google').click(function(){
                                var twpopup = window.open('//plus.google.com/share?url=" . $this->url_shared_google . "','', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=600');
                return false;
            });

        ");
        $js->append(parent::js($indent));
        return $js->text();
    }

}
