<?php

class CElement_Slideswiper extends CElement{
    
    protected $slides = array();
    protected $type='product';
    protected $link=NULL;
    protected $slide_per_view = 1;
    protected $height = '';
    protected $direction = 'horizontal';

    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public static function factory($id = ''){
        return new CElement_Slideswiper($id);
    }
    
    public function set_slides($slides) {
        $this->slides = $slides;
        return $this;
    }
    
    public function set_type($val) {
        $this->type = $val;
        return $this;
    }
    
    public function set_slide_per_view($slide_per_view) {
        $this->slide_per_view = $slide_per_view;
        return $this;
    }

    public function set_height($height) {
        $this->height = $height;
        return $this;
    }

    public function set_direction($direction) {
        $this->direction = $direction;
        return $this;
    }
        
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        
        $container = $this->add_div($this->id.'-swiper')->add_class('swiper-container');
            $swiper_wrapper = $container->add_div()->add_class('swiper-wrapper');
                if (count($this->slides) > 0) {
                    foreach ($this->slides as $slide_k => $slide_v) {
                        $url_link = carr::get($slide_v, 'url_link');
                        $image_url = carr::get($slide_v, 'image_url');
                        $name = carr::get($slide_v, 'name');
//                        cdbg::var_dump($slide_v);
                        $swiper_slide = $swiper_wrapper->add_div()->add_class('swiper-slide');
                            if (strlen($url_link) > 0) {
                                $swiper_slide->add('<a class="link" target="_blank" href="'.$url_link.'">');
                            }
                            $swiper_slide->add('<img src="' . $image_url . '" alt="'.$name.'" width="100%">');
                            if (strlen($url_link) > 0) {
                                $swiper_slide->add('</a>');
                            }
                        
                    }
                }
            $swiper_pagination = $container->add_div($this->id.'swiper-pagination')->add_class('swiper-pagination');
            
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent=0) {
        $js = new CStringBuilder();
        if (strlen($this->height) > 0) {
            $height = $this->height;   
            if ($this->slide_per_view > 1) {
                $height = $height + 15;     // 15 if for margin
            }
            $js->appendln("
                $('#".$this->id."-swiper').css('height','".$height."px');
                    ");
        }
        $js->appendln("
            var swiper = new Swiper('#".$this->id."-swiper', {
		autoplay: 4000,
                pagination: '#".$this->id."-swiper-pagination',
                paginationClickable: true,
                direction: '".$this->direction."',
                slidesPerView: ".$this->slide_per_view.",
                autoHeight : true,
            ");
        if (strlen($this->height) > 0) {
            $js->appendln("height: '".$this->height."',");
        }
        $js->appendln("});");
        $js->append(parent::js($indent));
        return $js->text();
    }
}
