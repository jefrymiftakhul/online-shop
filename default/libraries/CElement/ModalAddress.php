<?php

    class CElement_ModalAddress extends CElement {
        
        private $member_id = NULL;
        private $key = NULL;
        private $type = NULL;
        private $title = NULL;
        private $name = NULL;
        private $email = NULL;
        private $phone = NULL;
        private $address = NULL;
        private $province = NULL;
        private $city = NULL;
        private $districts = NULL;
        private $postal = NULL;
        private $submit_url = NULL;
        
        public function __construct($id = '') {
            parent::__construct($id);
        }

        public static function factory($id = '') {
            return new CElement_ModalAddress($id);
        }
        
        public function set_member_id($member_id){
            $this->member_id = $member_id;
            return $this;
        }
        
        public function set_key($key){
            $this->key = $key;
            return $this;
        }
        
        public function set_type($type){
            $this->type = $type;
            return $this;
        }
        
        public function set_title($title){
            $this->title = $title;
            return $this;
        }

        public function set_name($name){
            $this->name = $name;
            return $this;
        }

        public function set_email($email){
            $this->email = $email;
            return $this;
        }
        
        public function set_phone($phone){
            $this->phone = $phone;
            return $this;
        }
        
        public function set_address($address){
            $this->address = $address;
            return $this;
        }
        
        public function set_province($province){
            $this->province = $province;
            return $this;
        }
        
        public function set_city($city){
            $this->city = $city;
            return $this;
        }
        
        public function set_districts($districts){
            $this->districts = $districts;
            return $this;
        }
        
        public function set_postal($postal){
            $this->postal = $postal;
            return $this;
        }
        
        public function set_submit_url($url){
            $this->submit_url = $url;
            return $this;
        }
        
        public function html($index = 0) {
            $session = Session::instance();
            $html = new CStringBuilder();
          
            $form = $this->add_form()->add_class('form-62hallfamily');
            if(strlen($this->submit_url)>0) {
                $form->set_action($this->submit_url);
            }
            
            $row_fluid = $form->add_div()->add_class("row-fluid");
            $row_message = $row_fluid->add_div()->add_class('address-message');
            
            $row = $form->add_div()->add_class("col-md-12 margin-bottom-20");
            
            $key = $row
                    ->add_control('', 'hidden')
                    ->set_name('key')
                    ->set_value($this->key);
            
            $type = $row
                    ->add_control('', 'hidden')
                    ->set_name('type')
                    ->set_value($this->type);
            
            $name = $row
                    ->add_field()
                    ->set_label(clang::__('Nama Lengkap<red>*</red>'))
                    ->add_control("", 'text')
                    ->set_name("name")
                    ->add_class("form-control")
                    ->set_value($this->name);
            
            $email = $row
                    ->add_field()
                    ->set_label(clang::__('Email<red>*</red>'))
                    ->add_control("", 'text')
                    ->set_name("email")
                    ->add_class("form-control")
                    ->set_value($this->email);

			$handphone = $row
                    ->add_field()
                    ->set_label(clang::__('Nomor Handphone<red>*</red>'))
                    ->add_control("", 'text')
                    ->set_name("phone")
                    ->add_class("form-control numeric")
                    ->set_value($this->phone);
            
            $alamat = $row
                    ->add_field()
                    ->set_label(clang::__('Alamat<red>*</red>'))
                    ->add_control("", 'textarea')
                    ->set_name("address")
                    ->set_row(3)
                    ->add_class("form-control")
                    ->set_value($this->address);
            
            $left_form = $row->add_div()->add_class('col-md-6 padding-0');
            $right_form = $row->add_div()->add_class('col-md-6 padding-0');
            
            $province_id = $this->province;
            
            if($province_id==null) {
                $province_id = cdbutils::get_value("select province_id from province where status>0 and name='Jakarta' and country_id='94'");
            }
            
            $div_provinsi =  $left_form->add_div()->add_class('col-md-10 padding-0');
            $label_provinsi = $div_provinsi->add_field()
                    ->set_label(clang::__('Provinsi<red>*</red>'));
            
            $provinsi = $label_provinsi->add_div('div_province_pay')
                    ->add_control("province", 'province-select')
                    ->set_name("province")
                    ->set_all(false)
                    ->add_class("select-62hallfamily")
                    ->custom_css('height', '44px')
                    ->custom_css('width', '100%')
                    ->set_value($province_id)
                    ->set_country_id(94);
            
            $listener_change_province_select=$provinsi->add_listener('change');
            $listener_change_province_select
                    ->add_handler('reload')
                    ->set_target('div_city')
                    ->set_url(curl::base() .'reload/reload_control_city_new')
                    ->add_param_input(array('province'));
  
               
            $div_city =  $left_form->add_div()->add_class('col-md-10 padding-0');
            $field_city = $div_city
                    ->add_field()
                    ->set_label(clang::__('Kota<red>*</red>'));    
            
                 $city =  $field_city->add_div('div_city')       
                    ->add_control("city", 'city-select')
                    ->set_name("city")
                    ->add_class("select-62hallfamily")
                    ->set_all(false)
                    ->custom_css('width', '100%')
                    ->custom_css('height', '44px')
                    ->set_value($this->city)
                    ->set_province_id($province_id);
                 
                 $listener_change_city_select=$city->add_listener('change');
            $listener_change_city_select
                    ->add_handler('reload')
                    ->set_target('div_districts')
                    ->set_url(curl::base() .'reload/reload_control_districts_new')
                    ->add_param_input(array('city'));
                       
            $div_districts =  $right_form->add_div()->add_class('col-md-10 padding-0');
            $label_districts = $div_districts->add_div()->add_class('col-md-10 padding-0');
            $field_districts =$div_districts
                    ->add_field()
                    ->set_label(clang::__('Kecamatan<red>*</red>'));
            
            $districts = $field_districts->add_div('div_districts')
                    ->add_control("districts", 'districts-select')
                    ->set_name("districts")
                    ->set_all(false)
                    ->add_class("select-62hallfamily")
                    ->custom_css('height', '44px')
                    ->custom_css('width', '100%')
                    ->set_value($this->districts)
                    ->set_city_id($this->city);
            
            
            $kode_pos = $right_form->add_div()->add_class('col-md-10 padding-0')
                    ->add_field()
                    ->set_label(clang::__('Kode Pos<red>*</red>'))
                    ->add_control("", 'text')
                    ->set_name("postal")
                    ->add_class("form-control")
                    ->set_value($this->postal);
            
            $action = $row->add_field()
                    ->add_action()
                    ->set_label(clang::__('SIMPAN'))
                    ->add_class('btn-save-address btn-62hallfamily bg-red font-white border-3-red pull-right margin-top-20')
                    ->set_submit(true)
                    ->custom_css('padding-right', '0')
                    ->custom_css('width', '120px');
            
            $html->appendln(parent::html($index));

            return $html->text();
        }

        public function js($index = 0) {
            $js = new CStringBuilder();
            $js->appendln("
                    $('input').blur();
                    
                    $(document).on('keypress keyup blur', '.numeric', function (event){
                        $(this).val($(this).val().replace(/[^0-9]/g,''));
                        if ((event.which < 48 || event.which > 57)) {
                         event.preventDefault();
                        }
                    });
                    ");
            $js->appendln(parent::js($index));
            return $js->text();
        }

    }
    