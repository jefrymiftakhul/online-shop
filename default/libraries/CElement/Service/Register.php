<?php

    class CElement_Service_Register extends CElement {

        protected $trigger_button;
        protected $redirect;
        protected $reload;
        protected $use_icon;

        public function __construct($id = '') {
            parent::__construct($id);
            $this->trigger_button = false;
            $this->reload = true;
            // $this->redirect = curl::base() . curl::current() . CFRouter::$query_string;
            $this->redirect = curl::base() . 'member/confirm'; 
            $this->use_icon = true;
        }

        public static function factory($id = '') {
            return new CElement_Service_Register($id);
        }

        public function set_redirect($redirect) {
            $this->redirect = $redirect;
            return $this;
        }

        public function set_reload($reload) {
            $this->reload = $reload;
            return $this;
        }

        public function set_trigger_button($bool) {
            $this->trigger_button = $bool;
            return $this;
        }

        public function set_icon($use_icon) {
            $this->use_icon = $use_icon;
            return $this;
        }

        public function html($index = 0) {
            $html = new CStringBuilder();

            $is_hide = '';

            if ($this->trigger_button) {
                $is_hide = '';
                
                $icon = '';
                if ($this->use_icon) {
                    $icon = "<i class='glyphicon glyphicon-user'></i> ";
                }
                $class = '';
                if (count($this->classes) > 0) {
                    $class = implode(' ', $this->classes);
                }
                /*
                $register_btn = $this->add_action()
                        ->set_label($icon . clang::__("Daftarkan Jasa Usaha Anda"));
                $register_btn->add_class("btn-modal btn-62hallfamily bg-red font-size24 border-3-red");
                $register_btn->add_class($class);
                $register_btn->add_attr('data-target', 'modal-body-'.$this->id);
                $register_btn->add_attr('data-modal', 'register_service_form');
                $register_btn->add_attr('data-title', clang::__("Daftarkan Jasa Usaha Anda"));
                 * 
                 */
            }
            
            
            $modal = $this->add_div('custom_modal_'.$this->id)->add_class('modal');
            $modal_dialog = $modal->add_div()->add_class('modal-dialog');
            $modal_content = $modal_dialog->add_div()->add_class('modal-content clearfix');
            $modal_header = $modal_content->add_div()->add_class('modal-header'); 
            $modal_header->add("<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>");
            $modal_header->add("<h4 class='modal-title'>".clang::__('Daftar Jasa Usaha Anda')."</h4>");
            
                        
            $register_container = $modal_content->add_div('modal-body-'.$this->id)->add_class('modal-body register_service_form font-size14 clearfix '.$is_hide);
                    
            $form = $register_container->add_form();
            $form->add_class('registration form-auth form-62hallfamily');
            $form->set_action(curl::base() . 'auth/register');
            $form->set_ajax_submit(true);
            $form->set_validation(false);

            $row = $form->add_div()->add_class('row-fluid');

            $row_message = $row->add_div()->add_class('register-message auth-message');

            $row_9 = $row->add_div()->add_class('col-md-9');
            $row_field = $row->add_div()->add_class('col-md-12');
            
            $name = $row_9->add_field()
                    ->set_label(clang::__('Nama Usaha') . '<red>*</red>')
                    ->add_control('', 'text')
                    ->set_name('name')
                    ->add_validation('required')
                    ->add_class('col-md-8 col-xs-12');
            
            $email = $row_9->add_field()
                    ->add_label_class('inline-group')
                    ->set_label(clang::__('Email') . '<red>*</red>')
                    ->add_control('', 'text')
                    ->set_name('email')
                    ->add_class('col-md-12 col-xs-12');
            
            $bidang_usaha = $row_9->add_field()
                    ->set_label(clang::__('Bidang Usaha') . '<red>*</red>')
                    ->add_control('', 'text')
                    ->set_name('bidang_usaha')
                    ->add_validation('required')
                    ->add_class('col-md-12 col-xs-12');
            
            $alamat = $row_9->add_field()
                    ->set_label(clang::__('Alamat') . '<red>*</red>')
                    ->add_control('', 'text')
                    ->set_name('alamat')
                    ->add_validation('required')
                    ->add_class('col-md-12 col-xs-12');
            
            $action = $row_field->add_field()
                    ->add_action()
                    ->set_label(clang::__('Daftar'))
                    ->add_class('btn-auth btn-62hallfamily bg-red pull-left border-3-red margin-top-10')
                    ->custom_css('width', '100px')
                    ->set_submit(false);

            if ($this->reload == true) {
                $action->set_attr('on-reload', true);
                $row->add_control('', 'hidden')->set_name('redirect')
                        ->set_value($this->redirect);
            }
            
            $row_field->add_br();

            $html->appendln(parent::html($index));

            return $html->text();
        }

        public function js($index = 0) {
            $js = new CStringBuilder();
            $js->appendln(parent::js($index));
            return $js->text();
        }

    }
    