<?php

class CElement_Imlasvegas_Product_Overview extends CElement{
    
    private $key = NULL;

    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public static function factory($id = ''){
        return new CElement_Imlasvegas_Product_Overview($id);
    }
    
    public function set_key($key){
        $this->key = $key;
        return $this;
    }
    
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        
        $tabs = $this->add_tab_list('tabs-horizontal')->add_class('product-tab');
        
        $tab_overview = $tabs->add_tab('overview')
                ->set_label('<b>' . clang::__("Product Description") . '</b>')
                ->set_ajax_url(curl::base() . "product/overview/" . $this->key);
        
        $tab_spesifikasi = $tabs->add_tab('spesifikasi')
                ->set_label('<b>' . clang::__("Product Detail") . '</b>')
                ->set_ajax_url(curl::base() . "product/spesifikasi/" . $this->key);
        
        $tab_reviews = $tabs->add_tab('reviews')
                ->set_label('<b>' . clang::__("Reviews") . '</b>')
                ->set_ajax_url(curl::base() . "product/reviews/" . $this->key);
        
        $tab_faq = $tabs->add_tab('shipping')
                ->set_label('<b>' . clang::__("Shipping Information") . '</b>')
                ->set_ajax_url(curl::base() . "product/shipping_information/" . $this->key);
        
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent=0) {
        $js = new CStringBuilder();

        $vjs = "
            jQuery('#product_tab_overview-tab-widget .box-header h5').remove();
            jQuery('#product_tab_overview-tab-nav li a').click(function() {
                jQuery('.product-tab-caret').remove();
                jQuery(this).parents('li').append('<div class=\"product-tab-caret\"></div>');
            });
        ";

        $js->appendln($vjs);


        $js->append(parent::js($indent));
        return $js->text();
    }
}
