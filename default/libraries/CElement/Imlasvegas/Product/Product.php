<?php

class CElement_Imlasvegas_Product_Product extends CElement {

    private $key;
    private $name = NULL;
    private $image;
    private $column = NULL;
    private $flag = NULL;
    private $label_flag = NULL;
    private $stock = 0;
    private $product_hot = "Hot";
    private $product_new = "New";
    private $location_detail = NULL;
    private $min_stock = 0;
    private $price = 0;
    private $is_available = 1;
    private $slide_responsive = false;
    private $data = array();

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_Imlasvegas_Product_Product($id);
    }

    public function set_key($key) {
        $this->key = $key;
        return $this;
    }

    public function set_image($image) {
        $this->image = $image;
        return $this;
    }

    public function set_data($data) {
        $this->data = $data;
        return $this;
    }

    public function set_flag($flag) {
        $this->flag = ucfirst(strtolower($flag));
        return $this;
    }

    public function set_label_flag($set_flag) {
        $this->label_flag = ucfirst(strtolower($set_flag));
        return $this;
    }

    public function set_name($name) {
        $this->name = $name;
        return $this;
    }

    public function set_price($price) {
        $this->price = $price;
        return $this;
    }

    public function set_stock($stock) {
        $this->stock = $stock;
        return $this;
    }

    public function set_column($col) {
        $this->column = $col;
        return $this;
    }

    public function set_location_detail($location_detail) {
        $this->location_detail = $location_detail;
        return $this;
    }

    public function set_min_stock($min_stock) {
        $this->min_stock = $min_stock;
        return $this;
    }

    public function set_available($val) {
        $this->is_available = $val;
        return $this;
    }

    public function set_slide_responsive($bool) {
        $this->slide_responsive = $bool;
        return $this;
    }

    private function get_image() {
        /*
        $ch = curl_init(image::get_image_url($this->image, 'view_all'));
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_exec($ch);
        $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        */
        $retcode=200;
        if ($retcode == 500 or $this->image == NULL) {
            $this->image = curl::base() . 'application/62hallfamily/default/media/img/product/no-image.png';
        } else {
            if(isset($_GET['debug'])) {
                die('A');
            $this->image = image::get_raw_image_url($this->image, 'view_all');
            } else {
            $this->image = image::get_image_url($this->image, 'view_all');
                
            }
        }
        //curl_close($ch);
        return $this->image;
    }

    private function get_price() {
        $detail_price = $this->price;

        $promo_price = 0;
        $price = 0;

        if (!empty($detail_price['ho_sell_price']) and ! empty($detail_price['channel_sell_price'])) {
            if ($detail_price['ho_sell_price'] > $detail_price['channel_sell_price']) {
                $price = $detail_price['ho_sell_price'];
                $promo_price = $detail_price['channel_sell_price'];
            } else {
                $price = $detail_price['channel_sell_price'];
            }
        }

        return array('promo_price' => $promo_price, 'price' => $price);
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        
        $promo_text = carr::get($this->price, 'promo_text');
        $is_hot_item = carr::get($this->data, 'is_hot_item');
        $is_available = carr::get($this->data, 'is_available');
        $new_product_date = carr::get($this->data, 'new_product_date');
        $name = carr::get($this->data, 'name');
        $detail_price = carr::get($this->data, 'detail_price');
        $image_path = carr::get($this->data, 'image_path');
        $image_src = $this->get_image($image_path);

        $location_detail = $this->location_detail;
        if (empty($location_detail)) {
            $location_detail = curl::base() . 'product/item/';
        }

        $price_all = $this->get_price($detail_price);
        $price_promo = carr::get($price_all, 'promo_price');
        $price_normal = carr::get($price_all, 'price');
        
        $price = 'Rp '.ctransform::format_currency($price_normal);
        $price_strike = '';
        if ($price_promo > 0) {
            $price = 'Rp '.ctransform::format_currency($price_promo);
            $price_strike = 'Rp '.ctransform::format_currency($price_normal);
        }

        $container = $this->add_div()->add_class('col-md-4 col-sm-6');
        $product_div = $container->add_div()->add_class('product product-wrap')->add_attr("itemscope itemtype", "http://schema.org/Product")->add_attr('product-key', $this->key);
        $flag_div = $product_div->add_div()->add_class('product-flag');

        if ($promo_text > 0 ){
            $flag_div->add_div()->add_class('ico-promo');
        }
        $flag_div_wrapper = $flag_div->add_div()->add_class('flag-div-wrapper');
        if ($is_hot_item > 0) {
            $flag_div_wrapper->add_div()->add_class('ico-hot');
        }
        if (strtotime($new_product_date) > strtotime(date('Y-m-d'))) {
            $flag_div_wrapper->add_div()->add_class('ico-new');
        }

        // IMAGE
        $img_div = $product_div->add_div()->add_class('product-img');
        $img_div->add('<a href="'.$location_detail.$this->key.'"><img src="'.$image_src.'" class="img-responsive" alt="'.htmlspecialchars($name).'" itemprop="image"></a>');

        // SUMMARY    
        $limit_name = $name;
        if (strlen($name) > 35) {
            $limit_name = substr($name, 0, 60).'...';
        }
        $summary_div = $product_div->add_div()->add_class('product-summary');
        $item_price = $summary_div->add_div()->add_class('product-price');
        $item_price_promo = $item_price->add_div()->add_class('normal-price');
        $item_price_promo->add($price_strike);
        $item_price_normal = $item_price->add_div()->add_class('promo-price');
        $item_price_normal->add($price);

        $item_name = $summary_div->add_div()->add_class('product-name');
        $item_name->add('<a href="'.$location_detail.$this->key.'" itemprop="name" title="'.htmlspecialchars($name).'">'.$limit_name.'</a>');
        $item_action = $summary_div->add_div()->add_class('product-action');

        if ($is_available > 0) {
            $item_action->add_div()->add_class('ico-cart-white');
            $action = $item_action->add_action()->set_label(clang::__('Add to Cart'))->add_class('btn-buy btn btn-primary');
            $action->set_link($location_detail.$this->key);
        }
        else {
            $action = $item_action->add_div()->add_class('product-by-request')
                    ->add(clang::__('By Request'));
        }
        
        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $js->append(parent::js($indent));
        return $js->text();
    }

}
