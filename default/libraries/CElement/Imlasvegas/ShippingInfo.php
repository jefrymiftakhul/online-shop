<?php

class CElement_Imlasvegas_ShippingInfo extends CElement {

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_Imlasvegas_ShippingInfo($id);
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);

        $container_shipping = $this;

        if (CF::domain() == 'livetaste.ittronmall.com') {
            $container_shipping->add_div()->add('<b>PENGIRIMAN</b>');
            $container_shipping->add_div()->add('<red>*</red> ' . clang::__('Pengiriman Khusus Wilayah Surabaya'));
            $container_shipping->add_div()->add('<red>*</red> ' . clang::__('Buka Setiap Hari 11.00-21.00'));

            $container_shipping->add_div()->add('<br>');

            $container_shipping->add_div()->add(' <red>* ( ' . clang::__('Lihat ketentuan di tab Informasi Pengiriman') . ' )</red>');
        }

        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $js->append(parent::js($indent));
        return $js->text();
    }

}
