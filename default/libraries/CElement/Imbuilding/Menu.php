<?php

class CElement_Imbuilding_Menu extends CElement {

    private $menu = array();
    private $link = array();
    private $url_page = '';
    

    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public function set_menu($data_menu) {
        $this->menu = $data_menu;
        return $this;
    }

    public function set_link($data_menu) {
        $this->link = $data_menu;
        return $this;
    }

    public function set_url_page($val) {
        $this->url_page = $val;
        return $this;
    }

    public static function factory($id = '') {
        return new CElement_Imbuilding_Menu($id);
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);

        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();
        
        $js->append(parent::js($indent));
        return $js->text();
    }

}
