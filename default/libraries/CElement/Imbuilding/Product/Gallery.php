<?php

class CElement_Imbuilding_Product_Gallery extends CElement {

    protected $images = array();	
	protected $page ='product';

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_Imbuilding_Product_Gallery($id);
    }

    public function set_images($images) {
        $this->images = $images;
        return $this;
    }

    public function set_page($val) {
        $this->page = $val;
        return $this;
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);

        $image1 = isset($this->images[0]['image_path']) ? image::get_image_url($this->images[0]['image_path'], $this->page) : '';
        $image_large = isset($this->images[0]['image_path']) ? image::get_image_url($this->images[0]['image_path']) : '';

        
        $zoom_class = "cloud-zoom";
        $image_path = isset($this->images[0]['image_path']) ? $this->images[0]['image_path'] : '';
        

        if(!image::is_exists($image_path)) {
            //$image1 = curl::base().'application/62hallfamily/default/media/img/product/no-image.png';
            //$zoom_class = "";
        }
         
        $html->appendln('<div class="product-single-image">');
        $html->appendln('<div id="product-slider">');
        $html->appendln('<ul class="slides product-zoom-image">');
        $html->appendln('<li>
                            <img class="'.$zoom_class.' img-responsive" src="' . $image_large . '" data-large="' . $image_large . '" alt="" />
                            <a class="fullscreen-button" href="' . $image1 . '">
                                <div class="product-fullscreen">
                                    <i class="icons icon-resize-full-1"></i>
                                </div>
                            </a>
                        </li>');
        $html->appendln('</ul>');
        $html->appendln('</div>');

        if (count($this->images) > 1) {

            $html->appendln('<div id="product-carousel" style="height: 100px">');
            $html->appendln('<ul class="slides product-gallery margin-top-10">');

            foreach ($this->images as $images) {
                $html->appendln('<li>
                                <a class="fancybox" rel="product-images" href="' . image::get_image_url($images['image_path'], $this->page) . '"></a>
                                <img src="' . image::get_image_url($images['image_path'], 'thumbnails') . '" data-large="' . image::get_image_url($images['image_path']) . '" alt=""/>
                            </li>');
            }
            $html->appendln('</ul>');
            $html->appendln('<div class="product-arrows">
                                <div class="left-arrow">
                                        <i class="fa fa-chevron-left"></i>
                                </div>
                                <div class="right-arrow">
                                        <i class="fa fa-chevron-right"></i>
                                </div>
                        </div>');
            $html->appendln('</div>');
        }
        $html->appendln('</div>');

        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $script = "gallery_product();
                
                function gallery_product(){
		
                    $('#product-carousel').flexslider({
                        animation: 'slide',
                        controlNav: false,
                        animationLoop: false,
                        directionNav: false,
                        slideshow: false,
                        itemWidth: 70,
                        itemMargin: 0,
                        start: function(slider){

                        setActive($('#product-carousel li:first-child img'));
                        slider.find('.right-arrow').click(function(){
                                slider.flexAnimate(slider.getTarget('next'));
                        });

                        slider.find('.left-arrow').click(function(){
                                slider.flexAnimate(slider.getTarget('prev'));
                        });

                        slider.find('img').click(function(){
                                var large = $(this).attr('data-large');
                                setActive($(this));
                                $('#product-slider img').fadeOut(300, changeImg(large, $('#product-slider img')));
                                $('#product-slider a.fullscreen-button').attr('href', large);
                        });

                        function changeImg(large, element){
                                var element = element;
                                var large = large;
                                setTimeout(function(){ startF()},300);
                                function startF(){
                                        element.attr('src', large)
                                        element.attr('data-large', large)
                                        element.fadeIn(300);
                                }

                        }

                        function setActive(el){
                                var element = el;
                                $('#product-carousel img').removeClass('active-item');
                                element.addClass('active-item');
                        }

                    }
			
                    });
			
                    $('a.fullscreen-button').click(function(e){
                            e.preventDefault();
                            var target = $(this).attr('href');
                            $('#product-carousel a.fancybox[href='+target+']').trigger('click');
                    });
                    
                    $('.cloud-zoom').imagezoomsl({
                            zoomrange: [3, 3],
                            magnifiersize: [350, 500],
                    });

                    $('.fancybox').fancybox();

                }";

        $js->appendln($script);

        $js->append(parent::js($indent));
        return $js->text();
    }

}
