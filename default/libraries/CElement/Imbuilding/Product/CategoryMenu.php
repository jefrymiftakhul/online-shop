<?php

class CElement_Imbuilding_Product_CategoryMenu extends CElement{
    
    private $list_menu = array();
    private $head_menu = NULL;
    private $head_key = NULL;
    private $key = NULL;
    private $url_menu = NULL;
    private $menu_map = array();

    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public static function factory($id = ''){
        return new CElement_Imbuilding_Product_CategoryMenu($id);
    }
    
    public function set_key($key){
        $this->key = $key;
        return $this;
    }
    
    public function set_head_key($head_key){
        $this->head_key = $head_key;
        return $this;
    }

    public function set_menu_map($menu_map){
        $this->menu_map = $menu_map;
        return $this;
    }
    
    public function set_head_menu($head_menu){
        $this->head_menu = $head_menu;
        return $this;
    }
    
    public function set_list_menu($list_menu){
        $this->list_menu = $list_menu;
        return $this;
    }
    
    public function set_url_menu($url_menu){
        $this->url_menu = $url_menu;
        return $this;
    }
    
    private function cek_subnav($subnav){
        $result = FALSE;
        foreach ($subnav as $sub){
            if($sub['category_id'] == $this->key){
                $result = TRUE;
            }
        }
        
        return $result;
    }
    
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        
        if (count($this->list_menu) > 0) {
            $container_wrap = $this->add_div()->add_class('box-category');
            $container = $container_wrap->add_div()->add_class('txt-heading-wrap');
            $container->add_div()->add(clang::__('Categories'))->add_class('txt-heading');

            $arr_active = array();
            foreach ($this->menu_map as $key => $value) {
                $arr_active[] = carr::get($value, 'category_id');
            }

            $txt_menu = lapakbangunan::generate_menu($this->list_menu, 'product-category-menu', false, $arr_active);
            
            $container_menu = $container_wrap->add_div()->add_class('content-heading');
            $container_menu->add_div()->add($txt_menu);
        }
                

        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent=0) {
        $js = new CStringBuilder();
        

        $tjs = "
            jQuery('ul.product-category-menu li').click(function() {
                jQuery(this).siblings().removeClass('active');
                if (jQuery(this).hasClass('active')) {
                    jQuery(this).removeClass('active');
                }
                else {
                    jQuery(this).addClass('active');
                }
            });

            jQuery('ul.product-category-menu li .submenu-wrap').click(function(e) {
                e.stopPropagation();
            });

            jQuery('ul.product-category-menu li a').click(function(e) {
                e.stopPropagation();
            });
        ";
        $js->appendln($tjs);


        $js->append(parent::js($indent));
        return $js->text();
    }
}
