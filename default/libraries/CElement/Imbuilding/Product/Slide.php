<?php

class CElement_Imbuilding_Product_Slide extends CElement{
    
    protected $list_product = array();
    protected $element_title;
    protected $enable_button;
    protected $title_icon;
    protected $item_display;
    private $location_detail = NULL;
    protected $page='';

    public function __construct($id = '') {
        parent::__construct($id);
        $this->enable_button = true;
        $this->title_icon = '';
        $this->item_display = 5;
    }
    
    public static function factory($id = ''){
        return new CElement_Imbuilding_Product_Slide($id);
    }
    
    public function set_title($param) {
        $this->element_title = $param;
        return $this;
    }
    
    public function set_list_item($list){
        $this->list_product = $list;
        return $this;
    }
    
    public function set_enable_button($bool) {
        $this->enable_button = $bool;
        return $this;
    }
    
    public function set_title_icon($title_icon) {
        $this->title_icon = $title_icon;
        return $this;
    }
    
    public function set_item_display($int) {
        $this->item_display = $int;
        return $this;
    }
    
    public function set_location_detail($location_detail) {
        $this->location_detail = $location_detail;
        return $this;
    }
    
    public function set_page($val){
        $this->page = $val;
        return $this;
    }
        
    private function get_image($image_path = '') {
        $retcode=200;
        if ($retcode == 500 or $image_path == NULL) {
            $image_path = curl::base() . 'application/62hallfamily/default/media/img/product/no-image.png';
        } else {
            $image_path = image::get_image_url($image_path, 'view_all');
        }
        return $image_path;
    }
    
    private function get_price($detail_price) {
        $promo_price = 0;
        $price = 0;

        if (!empty($detail_price['ho_sell_price']) and ! empty($detail_price['channel_sell_price'])) {
            if ($detail_price['ho_sell_price'] > $detail_price['channel_sell_price']) {
                $price = $detail_price['ho_sell_price'];
                $promo_price = $detail_price['channel_sell_price'];
            } else {
                $price = $detail_price['channel_sell_price'];
            }
        }

        return array('promo_price' => $promo_price, 'price' => $price);
    }
    
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        
        $class = '';
        if (count($this->classes) > 0) {
            $class = implode(' ', $this->classes);
        }

        $slide_wrapper = $this->add_div()->add_class('slide-wrapper '.$class);
        $slide_element = $slide_wrapper->add_div($this->id.'-carousel')->add_class('key-deal-'.$this->id);
        if (count($this->list_product) > 0) {
            foreach ($this->list_product as $product_k => $product_v) {
                $product_id = carr::get($product_v, 'product_id');
                $url_key = carr::get($product_v, 'url_key');
                $code = carr::get($product_v, 'code');
                $name = carr::get($product_v, 'name');
                $sku = carr::get($product_v, 'sku');
                $weight = carr::get($product_v, 'weight');
                $image_path = carr::get($product_v, 'image_path');
                $image_src = $this->get_image($image_path);
                $is_hot_item = carr::get($product_v, 'is_hot_item');
                $new_product_date = carr::get($product_v, 'new_product_date');
                $is_available = carr::get($product_v, 'is_available');  // is_available > 0 tampilkan harga, else tampilkan "By Request"
                $quick_overview = carr::get($product_v, 'quick_overview');
                $stock = carr::get($product_v, 'stock');
                $show_minimum_stock = carr::get($product_v, 'show_minimum_stock');
                $product_data_type = carr::get($product_v, 'product_data_type');
                
                $detail_price = carr::get($product_v, 'detail_price');
                $promo_start_date=carr::get($detail_price,'promo_start_date');
                $promo_end_date=carr::get($detail_price,'promo_end_date');
                $promo_text = carr::get($detail_price, 'promo_text');
                
                $price_all = $this->get_price($detail_price);
                
                $price_promo = carr::get($price_all, 'promo_price');
                $price_normal = carr::get($price_all, 'price');
                
                $price = 'Rp '.ctransform::format_currency($price_normal);
                $price_strike = '';
                if ($price_promo > 0) {
                    $price = 'Rp '.ctransform::format_currency($price_promo);
                    $price_strike = 'Rp '.ctransform::format_currency($price_normal);
                }
                $page = $this->page.'/';
                if($this->page == 'product'){
                    $page = '';
                }
                $location_detail = curl::httpbase() . $page.'product/item/';
                
                // BUILD SNIGLE PRODUCT
                $item = $slide_element->add_div()->add_class('item');
                $product_div = $item->add_div()->add_class('product product-wrap')->add_attr("itemscope itemtype", "http://schema.org/Product")->add_attr('product-key', $url_key);
                $flag_div = $product_div->add_div()->add_class('product-flag');
                // FLAG
                if ($promo_text > 0 ){
                    // $flag_div->add_div()->add_class('promo')->add(round($promo_text, 1).'%');
                    $flag_div->add_div()->add_class('ico-promo');
                }
                $flag_div_wrapper = $flag_div->add_div()->add_class('flag-div-wrapper');
                if ($is_hot_item > 0) {
                    $flag_div_wrapper->add_div()->add_class('ico-hot');
                }
                if (strtotime($new_product_date) > strtotime(date('Y-m-d'))) {
                    $flag_div_wrapper->add_div()->add_class('ico-new');
                }
                    
                // IMAGE
                $img_div = $product_div->add_div()->add_class('product-img');
                $img_div->add('<a href="'.$location_detail.$url_key.'"><img src="'.$image_src.'" class="img-responsive" alt="'.$name.'" itemprop="image"></a>');
               
                // SUMMARY    
                $limit_name = $name;
                if (strlen($name) > 35) {
                    $limit_name = substr($name, 0, 60).'...';
                }
                $summary_div = $product_div->add_div()->add_class('product-summary');
                $item_price = $summary_div->add_div()->add_class('product-price');
                $item_price_promo = $item_price->add_div()->add_class('normal-price');
                $item_price_promo->add($price_strike);
                $item_price_normal = $item_price->add_div()->add_class('promo-price');
                $item_price_normal->add($price);

                $item_name = $summary_div->add_div()->add_class('product-name');
                $item_name->add('<a href="'.$location_detail.$url_key.'" itemprop="name" title="'.$name.'">'.$limit_name.'</a>');
                $item_action = $summary_div->add_div()->add_class('product-action');
                
                if ($is_available > 0) {
                    $item_action->add_div()->add_class('ico-cart-white');
                    $action = $item_action->add_action()->set_label(clang::__('Add to Cart'))->add_class('btn-buy btn btn-primary');
                    $action->set_link($location_detail.$url_key);
                }
                else {
                    $action = $item_action->add_div()->add_class('product-by-request')
                            ->add(clang::__('By Request'));
                }
            }
        }
        
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent=0) {
        $js = new CStringBuilder();
        
        $js->appendln("
            $('#".$this->id."-carousel').owlCarousel({
                autoPlay: true,");
        $js->appendln("items : ".$this->item_display.",");
        
        $js->appendln("
                responsive:{
                    0:{
                        items:1
                    },
                    400:{
                        items:2
                    },
                    640:{
                        items:3
                    },
                    1000:{
                        items:4
                    },
                },
                // itemsDesktop : [1400,4],
                // itemsDesktopSmall : [979,3],
                nav : true,
                navText : ['<i class=\"fa fa-chevron-left\"></i>','<i class=\"fa fa-chevron-right\"></i>'],
                pagination: false,
                theme : 'custom-carousel',
                loop: true,
            });
            ");

        $js->append(parent::js($indent));
        return $js->text();
    }
}
