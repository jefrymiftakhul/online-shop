<?php

class CElement_AdvertiseResponsive extends CElement{
    
    private $list = array();
    private $default_col = 12;


    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public static function factory($id = ''){
        return new CElement_AdvertiseResponsive($id);
    }
    

    public function set_list($list){
        $this->list = $list;
        return $this;
    }
    
    private function get_col($col_span){
        if ($col_span == 0){
            $col = $this->default_col;
        }else{
            $col = $this->default_col / 3 * $col_span;
        }

        return $col;
    }
    
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        
        if(!empty($this->list)){
            $div = $this->add_div()->add_class('bg-gray banner container-advertise');
            $container = $div->add_div()->add_class('container');
            $row = $container->add_div()->add_class('row-fluid');
        
            foreach ($this->list as $data){
                $col_span = carr::get($data, "col_span", 0);
                $image_url = carr::get($data, "image_url", NULL);
                $url_link = carr::get($data, "url_link");
                
                $col = $this->get_col($col_span);
                
                $class = NULL;
                if(!empty($url_link)){
                    $class =  ' link';
                }
                $class_col = 'col-md-' . $col;
                $class_col .= ' col-sm-' . $col;

                
                $advertise = $row->add_div()->add_class($class_col . $class);
                $image_object = CFactory::create_img();
                $image_object->set_src($image_url)
                            ->custom_css('width', '100%')
                            ;
                $advertise->add($image_object);
                if(!empty($url_link)){
                    $listener = $advertise->add_listener('click')
                            ->add_handler('custom')
                            ->set_js("
                                window.open('".$url_link."', '_blank');
                                ");
                }
            }
        }
        
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent=0) {
       $js = new CStringBuilder();

        $js->append(parent::js($indent));
        return $js->text();
    }
}
