<?php

class CElement_IttronmallFitgloss_Product_Detail extends CElement {

    private $key = NULL;
    private $detail_list = array();
    private $price = 0;
    private $stock = 0;
    private $minimum_stock = 10;
    private $attribute_list = array();
    private $with_qty = TRUE;
    private $type_select = 'select';
    private $type_image = 'image';
    private $total_param_input = array();
    private $url_share = '';
    private $text_share = '';

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_IttronmallFitgloss_Product_Detail($id);
    }

    public function set_key($key) {
        $this->key = $key;
        return $this;
    }

    public function set_minimum_stock($min_stock) {
        $this->minimum_stock = $min_stock;
        return $this;
    }

    public function set_detail_list($detail_list) {
        $this->detail_list = $detail_list;
        return $this;
    }

    public function with_qty($with_qty) {
        $this->with_qty = $with_qty;
        return $this;
    }

    public function set_attribute_list($attribute_list) {
        $this->attribute_list = $attribute_list;
        return $this;
    }

    private function get_price() {
        $detail_price = $this->price;

        $arr_price = product::get_price($detail_price);

        return $arr_price;
    }

    private function generate_list($type, $data = array(), &$smallest_value_key) {
        $list = array();

        $index = 1;
        foreach ($data as $key => $row_data) {
            $price = carr::get($row_data, 'price');

            if ($index == 1) {
                $smallest_value_key = $key;
                $smallest_value = $price;
            } else {
                if ($smallest_value > $price) {
                    $smallest_value = $price;
                    $smallest_value_key = $key;
                }
            }

            if ($type == $this->type_select) {
                $value = carr::get($row_data, 'attribute_key');
            }

            if ($type == $this->type_image) {
                $value['key'] = carr::get($row_data, 'attribute_key');
                $value['value'] = carr::get($row_data, 'file_path');
            }

            $list[$key] = $value;
            $index++;
        }

        return $list;
    }

    private function generate_form($container, $code, $data) {
        $smallest_value_key = '';

        $id = carr::get($data, 'attribute_category_id');
        $type = carr::get($data, 'attribute_category_type');
        $prev = carr::get($data, 'prev_attribute_category');
        $next = carr::get($data, 'next_attribute_category');
        $attribute = carr::get($data, 'attribute', array());
        $price = carr::get($data, 'price');
        $list = $this->generate_list($type, $attribute, $smallest_value_key);

        $control = array();
        foreach ($prev as $row_prev) {
            $arr_param['att_cat_' . $id] = 'att_cat_' . $id;
            if (carr::get($this->total_param_input, 'att_cat_' . $id) == null) {
                $this->total_param_input['att_cat_' . $id] = 'att_cat_' . $id;
            }
        }
        if ($type == $this->type_select) {
            $div_container = $container->add_div('container-' . $code);
            $select = $div_container->add_field()
                    ->add_control('att_cat_' . $id, 'select')
                    ->add_class('select-62hallfamily att_cat_' . $id)
                    ->custom_css('margin-left', '40px;')
                    ->custom_css('margin-top', '-45px;')
                    ->custom_css('border-radius', '0px;')
                    ->set_list($list)
                    ->set_value($smallest_value_key)
            ;
            $control[] = $select;
        } else if ($type == $this->type_image) {
            $div_container = $container->add_div('container-' . $code)
                    ->add_class('container-input-image');
            $index = 1;
            foreach ($list as $key => $value) {
                $active = NULL;
                if ($index == 1) {
                    $active = 'active';
                    $div_container->add_control('att_cat_' . $id, 'hidden')
                            ->set_value($key);
                }
                $image = $div_container->add_div($key)
                        ->add_class('btn-colors margin-bottom-10 link  att_cat_' . $id . ' ' . $active)
                        ->custom_css('display', 'inline-block')
                        ->custom_css('padding', '2px')
                        ->add('<img src="' . $value['value'] . '" style="width:20px; height:20px"/>');


                $control[] = $image;

                $index++;
            }
        }

        if (count($control) > 0) {
            foreach ($control as $obj) {
                if (!empty($next) and count($list) > 0) {
                    $listener = $obj->add_listener('ready');

                    $handler = $listener->add_handler('reload')
                            ->set_target('container-' . $next)
                            ->add_param_input($arr_param)
                            ->set_url(curl::base() . 'product/generate_attribute/' . $next . '/' . $this->key);



                    if ($type == $this->type_image) {
                        $listener = $obj->add_listener('click');

                        $handler = $listener->add_handler('reload')
                                ->set_target('container-' . $next)
                                ->add_param_input($arr_param)
                                ->set_url(curl::base() . 'product/generate_attribute/' . $next . '/' . $this->key);
                    }
                    if ($type == $this->type_select) {

                        $listener = $obj->add_listener('change');

                        $handler = $listener->add_handler('reload')
                                ->set_target('container-' . $next)
                                ->add_param_input($arr_param)
                                ->set_url(curl::base() . 'product/generate_attribute/' . $next . '/' . $this->key);
                    }
                } else if (empty($next) and count($list) > 0) {
                    if ($type == $this->type_select) {
                        $listener = $obj->add_listener('ready');

                        $handler = $listener->add_handler('reload')
                                ->set_target('stock-price')
                                ->add_param_input($arr_param)
                                ->set_url(curl::base() . 'reload/reload_stock_price_product/' . $code . '/' . $this->key);

                        $listener = $obj->add_listener('change');

                        $handler = $listener->add_handler('reload')
                                ->set_target('stock-price')
                                ->add_param_input($arr_param)
                                ->set_url(curl::base() . 'reload/reload_stock_price_product/' . $code . '/' . $this->key);
                    }
                    if ($type == $this->type_image) {
                        $listener = $obj->add_listener('ready');

                        $handler = $listener->add_handler('reload')
                                ->set_target('stock-price')
                                ->add_param_input($arr_param)
                                ->set_url(curl::base() . 'reload/reload_stock_price_product/' . $code . '/' . $this->key);

                        $data_addition = '';
                        foreach ($arr_param as $inp) {
                            if (strlen($data_addition) > 0)
                                $data_addition.=',';
                            $data_addition.="'" . $inp . "':$.cresenity.value('#" . $inp . "')";
                        }

                        $data_addition = '{' . $data_addition . '}';

                        $listener = $obj->add_listener('click');

                        $handler = $listener->add_handler('custom')
                                ->set_js("
                                        var key=$(this).attr('id');
                                        $('#" . 'att_cat_' . $id . "').val(key);

                                        $.cresenity.reload('stock-price','" . curl::base() . "reload/reload_stock_price_product/" . $code . "/" . $this->key . "','get'," . $data_addition . ");
                                        ");
                    }
                }
            }
        }

        return $control;
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        $product_id = carr::get($this->detail_list, 'product_id', NULL);
        $name = carr::get($this->detail_list, 'name', NULL);
        $sku = carr::get($this->detail_list, 'sku', NULL);
        $availability = carr::get($this->detail_list, 'is_available', NULL);
        $quick_overview = carr::get($this->detail_list, 'quick_overview', NULL);
        $stock = carr::get($this->detail_list, 'stock', 0);
        $weight = carr::get($this->detail_list, 'weight', 0);
        $show_minimum_stock = carr::get($this->detail_list, 'show_minimum_stock', 0);
        $this->stock = $stock;
        $this->price = carr::get($this->detail_list, 'detail_price', 0);
        $detail_price = carr::get($this->detail_list, 'detail_price', 0);

        $array_size = carr::get($this->detail_list, 'size');
        $array_color = carr::get($this->detail_list, 'color');

        $url_key_id = carr::get($this->detail_list, 'url_key');
        if (strlen($url_key_id) == 0) {
            $url_key_id = $product_id;
        }
        $this->url_share = curl::httpbase() . 'product/item/' . $url_key_id;
        $this->text_share = htmlspecialchars($name);

        // Build UI
        $detail_product = $this->add_div()->add_class('detail-product');
        $product_summary_div = $detail_product->add_div()->add_class('product-summary');
        $product_action_div = $detail_product->add_div()->add_class('product-action');
        $product_stock_div = $detail_product->add_div()->add_class('product-stock');
        $product_share_div = $detail_product->add_div()->add_class('product-share');

        // Product Summary
        $product_name_div = $product_summary_div->add_div()->add_class('product-name');
        $product_name_div->add('<h3 class="title">' . htmlspecialchars($name) . '</h3>');

        // $container_review->add_div()->add_class('container-write-review')->add(' | 0 '.clang::__('reviews').' | '.clang::__('Write a reviews'));

        $stock_info = '';
        $container_price = $product_name_div->add_div('stock-price')->add_class('container-price');
        if ($availability > 0) {
            $price = $this->get_price();
            if ($price['promo_price'] > 0) {
                $container_price->add_div()->add_class('detail-promo-price')->add('<strike>Rp ' . ctransform::format_currency($price['price']) . '</strike>');
                $container_price->add_div()->add_class('detail-normal-price')->add('Rp ' . ctransform::format_currency($price['promo_price']));
            } else {
                $container_price->add_div()->add_class('detail-normal-price')->add('Rp ' . ctransform::format_currency($price['price']));
            }
            $stock_info = 'STOCK <span class="stock">' . $stock . ' ITEM</span>';
            if ($show_minimum_stock > 0) {
                if ($stock > 0 && $stock <= $show_minimum_stock) {
                    $stock_info = 'TINGGAL <span class="stock">SISA ' . $stock . ' ITEM</span>';
                }
            }
        } else {
            $product_name_div->add('Kisaran Harga<br>Rp. ' . ctransform::format_currency($detail_price['sell_price_start']) . ' - Rp. ' . ctransform::format_currency($detail_price['sell_price_end']));
        }

        // form attribute
        $attribute = $product_name_div->add_div()->add_class('col-md-12 col-12');
        $attribute_col = $attribute->add_div()->add_class('col-md-4');
        $rowform = $attribute->add_div()->add_class('row');
        $form_atribute = $attribute->add_div()->add_class('col-md-8');
        foreach ($this->attribute_list as $code_attribute => $attribut_list) {
            $form_atribute->add_div()->add_class('product-attribute')->add($attribut_list['attribute_category_name']);
            $this->generate_form($form_atribute, $code_attribute, $attribut_list);
        }

        $product_act = $product_action_div->add_div()->add_class('col-md-12 col-xs-12');
        $product_act_qty = $product_act->add_div()->add_class('col-md-8 col-sm-8 col-xs-8');
        $product_btn_act = $product_act->add_div()->add_class(' col-md-4 col-sm-4 col-xs-4');
        if ($this->with_qty) {
            $container_qty = $product_act_qty->add_div()->add_class('container-qty');
            $container_qty->add_div()->add(clang::__('Quantity'))->add(' :');
            $container_qty->add_div()->add_class('qty-wrapper')->add("<input type='text' id='qty' name='qty' value='1' style='z-index:0;' readonly>");
        }

        $container_product_code = $container_price->add_div()->add_class('container-product-code');
        $table = "
        <table class='table-detail-product'>
            <tr>
                <td class='td-title'>" . clang::__('Product Code') . "</td>
                <td class='tb-colon'> : </td>
                <td>" . $sku . "</td>
            </tr>";

        if (strlen($stock_info) > 0) {
            $table .= "<tr>
                    <td class='td-title'>" . clang::__('Availability') . "</td>
                    <td class='tb-colon'>:</td>
                    <td class='td-title'><b>" . $stock_info . "</b></td>
                </tr>";
        }
        $table .= "
        </table>";
        $container_product_code->add($table);

        $form = $product_btn_act->add_form('form-detail-product')->set_action(curl::base() . 'request/add/');
        $form->add_control('product_id', 'hidden')->set_value($product_id);
        $form->add_control('page', 'hidden')->set_value('product');
        
//        $leftform = $rowform->add_div()->add_class(' col-xs-6 col-sm-3 margin-top-10');
//        $rigthform = $rowform->add_div('add_to_cart_container')->add_class('col-xs-6 col-sm-9 add_to_cart_container');


//        $rowform = $form->add_div()->add_class('row');
//// cdbg::var_dump($this->attribute_list);die; 
//        // form attribute
//        $form_atribute = $rowform->add_div()->add_class('col-md-12');
//        foreach ($this->attribute_list as $code_attribute => $attribut_list) {
//            $form_atribute->add_div()->add_class('product-attribute')->add($attribut_list['attribute_category_name']);
//            $this->generate_form($form_atribute, $code_attribute, $attribut_list);
//        }
//                
        if (isset($_GET['is_debug'])) {
            cdbg::var_dump($this->attribute_list);
        }
//
//        $leftform = $rowform->add_div()->add_class(' col-xs-6 col-sm-3 margin-top-10');
//        $rigthform = $rowform->add_div('add_to_cart_container')->add_class('col-xs-6 col-sm-9 add_to_cart_container');

        
        
        // if ($this->with_qty) {
        //     $leftform->add_div('div_qty')->add('JUMLAH<br><br>')->add("<input type='text' id='qty' name='qty' value='1' style='z-index:0'>");
        // }
        // $rigthform->add('BERAT<br><br>'.$weight);

        if ($availability > 0) {

            $action = $form->add_action()
                    ->set_label("<div class='ico-cart-button'></div><div class='txt-cart-button'>" . clang::__('ADD TO CART') . "</div>")
                    ->add_class('btn-add-cart btn-primary');
            $cart = $action->add_listener('click')->add_handler('reload');

            foreach ($this->attribute_list as $key => $attribut_list) {
                $cart->add_param_input('att_cat_' . carr::get($attribut_list, 'attribute_category_id'));
            }

            $cart->add_param_input('qty')
                    ->set_target('shopping-cart') // shopping cart
                    ->set_url(curl::base() . 'products/shoppingcart/add_item_cart/' . $product_id); // proses shopping cart

            $cart = $action->add_listener('click')->add_handler('reload');
            $cart->add_param_input('qty')
                    ->set_target('shopping-cart-secondary') // shopping cart
                    ->set_url(curl::base() . 'products/shoppingcart/icon_shopping_cart'); // proses shopping cart
            /*
              $dialog = $action->add_handler('dialog')
              ->set_title('<center>Tambah Belanjaan</center>')
              ->set_url(curl::base() . 'products/shoppingcart/dialog_add_cart');
             */
        } else {
            $this->stock = 100;
            $action = $form->add_action()
                    ->set_label('Request')
                    ->add_class('btn-primary btn-add-cart')
                    ->set_submit_to(curl::base() . 'request/add/')
                    ->set_submit(true);
        }


        $container_share = $product_share_div->add_div()->add_class('container-share');
        $container_share->add_div()->add_class('txt-share')->add(clang::__('SHARE'));
        $container_share->add_div()->add_class('ico-share-tw twitter');
        $container_share->add_div()->add_class('ico-share-fb facebook');
        // $container_share->add_div()->add_class('ico-share-ig');
        $container_share->add_div()->add_class('ico-share-gp google');

        if (CF::domain() == 'tokolasvegas.com') {
            $container_shipping = $product_share_div->add_div()->add_class('container-shipping');
            $container_shipping->add_div()->add('<b>PENGIRIMAN</b>');
            $container_shipping->add_div()->add('<red>*</red> ' . clang::__('Pengiriman Order GRATIS'));
            $container_shipping->add_div()->add('<red>*</red> ' . clang::__('Cash on Delivery Tersedia'));

            $container_shipping->add_div()->add('<br><b>ESTIMASI PENGIRIMAN</b>');
            $container_shipping->add_div()->add('Jakarta 1-3 hari kerja, luar Jakarta 2 - 6 hari kerja <br><br>');

            $container_shipping->add_div()->add(' <red>* ( ' . clang::__('Lihat ketentuan di tab shipping information') . ' )</red>');
        } else {
            $container_shipping = $product_share_div->add_div()->add_class('container-shipping');
            $element_overview = $container_shipping->add_element('62hall-shipping-info', 'container-shipping-info');
        }

        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $max = $this->stock;
        if ($max < 1)
            $max = 1;
        $js->appendln("
            $('.btn-colors').click(function(){
                id = $(this).attr('id');
                $('#colors').val(id);
                $('.btn-colors').removeClass('active');
                $(this).addClass('active');
            });  
            
            $('#qty').TouchSpin({
                min: 1,
                max: " . $max . ",
                verticalbuttons: false
            });
		");

        $js->appendln("
            $('.facebook').click(function(){
                var fbpopup = window.open('http://www.facebook.com/sharer/sharer.php?u=" . $this->url_share . "', 'pop', 'width=600, height=400, scrollbars=no');
                return false;
            });
            $('.twitter').click(function(){
                                var twpopup = window.open(\"//twitter.com/intent/tweet?url=" . $this->url_share . "&text=" . $this->text_share . "&original_referer=" . $this->url_share . "\",600, 400);
                return false;
            });
             $('.google').click(function(){
                                var twpopup = window.open('//plus.google.com/share?url=" . $this->url_share . "','', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=600');
                return false;
            });
        ");

        $js->append(parent::js($indent));
        return $js->text();
    }

}
