<?php

    class CElement_IttronmallFitgloss_ChangePassword extends CElement {

        protected $trigger_button = TRUE;
        
        public function __construct($id = '') {
            parent::__construct($id);

        }

        public static function factory($id = '') {
            return new CElement_IttronmallFitgloss_ChangePassword($id);
        }

        public function set_trigger_button($bool) {
            $this->trigger_button = $bool;
            return $this;
        }
        
        public function html($index = 0) {
            $session = Session::instance();
            $html = new CStringBuilder();
            
            // BUTTON
            if ($this->trigger_button) {
                
                $change_password_btn = $this->add_action()->set_label(clang::__("Ubah Password"));
                $change_password_btn->add_class("btn-modal");
                $change_password_btn->add_class($class . 'btn-change-password');
                $change_password_btn->add_attr('data-target', 'modal-body-'.$this->id);
                $change_password_btn->add_attr('data-modal', 'change_password_form');
                $change_password_btn->add_attr('data-title', clang::__("Masukkan Password Baru Anda"));
            }


            
            $modal = $this->add_div('custom_modal_'.$this->id)->add_class('modal');
            $modal_dialog = $modal->add_div()->add_class('modal-dialog');
            $modal_content = $modal_dialog->add_div()->add_class('modal-content clearfix');
            $modal_header = $modal_content->add_div()->add_class('modal-header'); 
            $modal_header->add("<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>");
            $modal_header->add("<h4 class='modal-title'>".clang::__('Masukkan Password Baru Anda')."</h4>");
            
            
                        
            $login_form = $modal_content->add_div('modal-body-'.$this->id)->add_class('modal-body change_password_form clearfix '.$is_hide);
            $form = $login_form->add_form()->add_class('form-62hallfamily');
            $form->set_action(curl::base() . 'guest/forgotPassword');
            $form->set_ajax_submit(false);
            $form->set_validation(false);

            $row = $form->add_div()->add_class("col-md-12");

            $row_message = $row->add_div()->add_class('login-message forgot-password-message');
            
            $title = $row->add_field()
                    ->add(clang::__('Silahkan masukan alamat email Anda untuk mendapatkan password baru'));
            
            if(!empty($this->title_header)){
                $title_header = $form->add_div()->add_class('col-md-12')
                        ->add($this->title_header);
            }
            
            $input_email= $row
                    ->add_field()
                    ->set_label(clang::__('Email'))
                    ->add_control("", 'text')
                    ->set_name("email")
                    ->add_class("form-control")
                    ->add_validation("required");


            $row->add_control('', 'hidden')->set_name('redirect')->set_value(curl::base() . curl::current() . CFRouter::$query_string);
            if ($this->trigger_button == false) {
                $row->add_control('', 'hidden')->set_name('location')->set_value('on_page');
            }
            
            $action = $row->add_field()
                    ->add_action()
                    ->set_label(clang::__('Reset Password'))
                    ->add_class('btn-auth btn-62hallfamily bg-red font-white margin-top-30 border-3-red pull-right upper')
//                    ->custom_css('padding-right', '0')
                    ->set_submit(false);
            
            $row->add_br();
            
            if(!empty($this->title_header)){
                $title_header = $form->add_div()->add_class('col-md-12')
                        ->add($this->title_header);
            }
            
            if ($this->reload == true) {
                $action->set_attr('on-reload', true);
                $row->add_control('', 'hidden')->set_name('redirect')
                        ->set_value($this->redirect);
            }
            
            $html->appendln(parent::html($index));

            return $html->text();
        }

        public function js($index = 0) {
            $js = new CStringBuilder();
            $js->appendln(parent::js($index));
            return $js->text();
        }

    }
    