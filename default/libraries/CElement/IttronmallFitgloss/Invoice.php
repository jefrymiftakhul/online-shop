<?php

class CElement_IttronmallFitgloss_Invoice extends CElement {

    protected $booking_code;
    protected $type_email;
    protected $status;
    protected $logo = NULL;
    protected $domain = NULL;
    protected $org_name = NULL;
    protected $recipient = NULL;
    protected $transaction_header = FALSE;
    protected $transaction_header_request = FALSE;
    protected $transaction_method = TRUE;

    public function __construct($id = '') {
        parent::__construct($id);
        $this->type_email = 'order';
    }

    public static function factory($id = '') {
        return new CElement_IttronmallFitgloss_Invoice($id);
    }

    public function set_booking_code($val) {
        $this->booking_code = $val;
        return $this;
    }

    public function set_logo($val) {
        $this->logo = $val;
        return $this;
    }

    public function set_type_email($val) {
        $this->type_email = $val;
        return $this;
    }

    public function set_status($val) {
        $this->status = $val;
        return $this;
    }

    public function set_org_name($val) {
        $this->org_name = $val;
        return $this;
    }

    public function set_domain($val) {
        $this->domain = $val;
        return $this;
    }

    public function set_recipient($val) {
        $this->recipient = $val;
        return $this;
    }

    public function html($index = 0) {
        $html = new CStringBuilder();
        $db = CDatabase::instance();
        if (strlen($this->booking_code) > 0) {
            $transaction = cdbutils::get_row('select * from transaction where status>0 and booking_code=' . $db->escape($this->booking_code));
            $check_transaction_status = cobj::get($transaction, 'transaction_status');
            $transaction_type = cobj::get($transaction, 'type');
            $total_deal = cobj::get($transaction, 'total_deal');
            $invoice_info = transaction::get_transaction_contact('booking_code', $this->booking_code);
            $data_product = transaction::get_transaction_detail('transaction_id', $invoice_info->transaction_id);
            $data_payment = transaction::get_transaction_payment('transaction_id', $invoice_info->transaction_id);
            $total_price = 0;
            $total_charge = 0;
            $total_shipping = 0;
            $total_payment = 0;
            $payment_type = '';
            $payment_payment_status = '';
            $product_type = cobj::get($transaction, 'product_type');
            $shipping_status = cobj::get($transaction, 'shipping_status');
            $transaction_shipping = cdbutils::get_row("select * from transaction_shipping where shipping_status=" . $db->escape($shipping_status) . " and transaction_id=" . $db->escape($invoice_info->transaction_id));
            $expedition_no = cobj::get($transaction_shipping, 'expedition_no');
            $expedition = cobj::get($transaction_shipping, 'expedition_name');


            if ($invoice_info) {
                $total_price = $invoice_info->channel_sell_price;
                $total_shipping = $invoice_info->total_shipping;
                if ($transaction_type == 'sell') {
                    $total_price = $total_deal;
                }
            }

            if ($data_payment) {
                $total_charge = $data_payment->total_charge;
                $total_payment = $total_price + $total_shipping + $total_charge;
                $payment_type = $data_payment->payment_type;
                $payment_payment_status = $data_payment->payment_status;
            }

            $invoice = curl::base() . "payment/payment_confirmation/" . $invoice_info->transaction_id;

            $container = $this->add_div()->add_class('container margin-top-30');
            if ($payment_payment_status == 'PENDING') {
                $title_content = $container->add_div()->add_class('col-md-12 padding-0 bold font-size24')
                        ->add(clang::__('TERIMA KASIH'));

                $title_content->add_div()->add_class('label-invoice')->add('<p>Pesanan Anda telah diterima. Pesanan Anda akan segera diproses setelah pembayaran Anda telah dikonfirmasi. <br/>Setelah pembayaran dilakukan, silahkan klik link berikut: <br/><a href=' . $invoice . ' ><span style="color: red">www.' . CF::domain() . $invoice . '</a></span> <br/>Link juga kami kirimkan ke email Anda.</p>');
            }

            $title_content = $container->add_div()->add_class('col-md-12 padding-0 bold font-size24')
                    ->add(clang::__('Invoice Anda'));
            $content = $container->add_div()->add_class('col-md-12 padding-0 margin-top-20');

            $transaction_date_hold = NULL;
            if ($this->type_email == 'order') {
                $transaction_date_hold = '
                            Batas Waktu
                        : ' . ctransform::format_datetime($invoice_info->date_hold);
            }

            $table_pesanan = $content->add_div()->add_class('margin-bottom-20');
            $table_pesanan->add('<table class="table-62hallfamily">');
            $table_pesanan->add('<tr>');
            $table_pesanan->add('<th colspan="2" style="padding:10px;text-align:left;">');
            $table_pesanan->add(clang::__('Detail Pesanan'));
            $table_pesanan->add('</th>');
            $table_pesanan->add('</tr>');

            $table_pesanan->add('<tr>');
            $table_pesanan->add('<td width ="30%">');
            $table_pesanan->add('Order ID: ' . $invoice_info->booking_code . '<br>');
            $table_pesanan->add('Tanggal: ' . ctransform::format_date(substr($invoice_info->date, 0, 10)) . '<br>');
            $table_pesanan->add($transaction_date_hold);
            $table_pesanan->add('</td>');
            $table_pesanan->add('<td width ="70%">');
            $table_pesanan->add('Email: ' . $invoice_info->billing_email . '<br>');
            $table_pesanan->add('Telepon: ' . $invoice_info->billing_phone);
            if ($shipping_status == 'ON_SHIPPING' || $shipping_status == 'DELIVERED') {
                $table_pesanan->add('<br>No Resi : ' . $expedition_no . '-' . $expedition);
            }
            $table_pesanan->add('</td>');
            $table_pesanan->add('</tr>');
            $table_pesanan->add('</table>');


            $table_alamat = $content->add_div()->add_class('margin-bottom-20');
            $table_alamat->add('<table class="table-62hallfamily">');
            $table_alamat->add('<tr>');
            $table_alamat->add('<th style="padding:10px;text-align:left;">');
            $table_alamat->add(clang::__('Alamat Pembayaran'));
            $table_alamat->add('</th>');
            $table_alamat->add('<th style="padding:10px;text-align:left;">');
            $table_alamat->add(clang::__('Alamat Pengiriman'));
            $table_alamat->add('</th>');
            $table_alamat->add('</tr>');

            $table_alamat->add('<tr>');
            $table_alamat->add('<td>');
            $table_alamat->add($invoice_info->billing_first_name . ' ' . $invoice_info->billing_last_name . '<br>');
            $table_alamat->add($invoice_info->billing_address . '<br>');
            $table_alamat->add($invoice_info->billing_city . ' ' . $invoice_info->billing_postal_code . '<br>');
            $table_alamat->add('No Handphone ' . $invoice_info->billing_phone);
            $table_alamat->add('</td>');
            $table_alamat->add('<td>');
            $table_alamat->add($invoice_info->shipping_first_name . ' ' . $invoice_info->shipping_last_name . '<br>');
            $table_alamat->add($invoice_info->shipping_address . '<br>');
            $table_alamat->add($invoice_info->shipping_city . ' ' . $invoice_info->shipping_postal_code . '<br>');
            $table_alamat->add('No Handphone ' . $invoice_info->shipping_phone);
            $table_alamat->add('</td>');
            $table_alamat->add('</tr>');
            $table_alamat->add('</table>');

//            if($transaction_type=='sell'){
//                $table_produk = $content->add_div()->add_class('margin-bottom-20');
//                $table_produk->add('<table class="table-62hallfamily">');
//                $table_produk->add('<tr>');
//                $table_produk->add('<th style="padding:10px;text-align:left;">');
//                $table_produk->add(clang::__('Penjualan Produk'));
//                $table_produk->add('</th>');
//                $table_produk->add('<th width="100px" style="padding:10px;text-align:left;">');
//                $table_produk->add(clang::__('Jumlah'));
//                $table_produk->add('</th>');
//                $table_produk->add('<th style="padding:10px;text-align:left;">');
//                $table_produk->add(clang::__('Harga'));
//                $table_produk->add('</th>');
//                $table_produk->add('<th style="padding:10px;text-align:left;">');
//                $table_produk->add(clang::__('Total'));
//                $table_produk->add('</th>');
//                $table_produk->add('</tr>');
//                foreach ($data_product as $row_product) {
//                    $table_produk->add('<tr>');
//                    $table_produk->add('<td>');
//                    $table_produk->add($row_product->name);
//                    $table_produk->add('</td>');
//                    $table_produk->add('<td>');
//                    $table_produk->add($row_product->qty);
//                    $table_produk->add('</td>');
//                    $table_produk->add('<td class="text-right">');
//                    $table_produk->add(ctransform::format_currency($row_product->channel_sell_price));
//                    $table_produk->add('</td>');
//
//                    $sub_total = $row_product->qty * $row_product->transaction_price;
//
//                    $table_produk->add('<td class="text-right">');
//                    $table_produk->add(ctransform::format_currency($sub_total));
//                    $table_produk->add('</td>');
//                    $table_produk->add('</tr>');
//                }
//            }
            if ($transaction_type == 'sell') {
                $table_produk_sell = $content->add_div('')->add_class('margin-bottom-20');
                $table_produk_sell->add('<table class="table-62hallfamily">');
                $table_produk_sell->add('<tr>');
                $table_produk_sell->add('<th style="padding:10px;text-align:left;">');
                $table_produk_sell->add(clang::__('Produk Di Jual'));
                $table_produk_sell->add('</th>');
                $table_produk_sell->add('<th width="100px" style="padding:10px;text-align:left;">');
                $table_produk_sell->add(clang::__('Jumlah'));
                $table_produk_sell->add('</th>');
                $table_produk_sell->add('<th style="padding:10px;text-align:left;">');
                $table_produk_sell->add(clang::__('Harga'));
                $table_produk_sell->add('</th>');
                $table_produk_sell->add('<th style="padding:10px;text-align:left;">');
                $table_produk_sell->add(clang::__('Total'));
                $table_produk_sell->add('</th>');
                $table_produk_sell->add('</tr>');
                foreach ($data_product as $row_product) {
                    $product_name = $row_product->name;
                    $table_produk_sell->add('<tr>');
                    $table_produk_sell->add('<td>');
                    $table_produk_sell->add($product_name);
                    $table_produk_sell->add('</td>');
                    $table_produk_sell->add('<td>');
                    $table_produk_sell->add($row_product->qty);
                    $table_produk_sell->add('</td>');
                    $table_produk_sell->add('<td class="text-right">');
                    $table_produk_sell->add(ctransform::format_currency($row_product->channel_sell_price));
                    $table_produk_sell->add('</td>');

                    $sub_total = $row_product->qty * $row_product->transaction_price;

                    $table_produk_sell->add('<td class="text-right">');
                    $table_produk_sell->add(ctransform::format_currency($sub_total));
                    $table_produk_sell->add('</td>');
                    $table_produk_sell->add('</tr>');
                }
//                $table_produk->add_br();
            }
            $table_produk = $content->add_div()->add_class('margin-bottom-20');
            $table_produk->add('<table class="table-62hallfamily">');
            $table_produk->add('<tr>');
            $table_produk->add('<th style="padding:10px;text-align:left;">');
            $table_produk->add(clang::__('Produk'));
            $table_produk->add('</th>');
            $table_produk->add('<th width="100px" style="padding:10px;text-align:left;">');
            $table_produk->add(clang::__('Jumlah'));
            $table_produk->add('</th>');
            $table_produk->add('<th style="padding:10px;text-align:left;">');
            $table_produk->add(clang::__('Harga'));
            $table_produk->add('</th>');
            $table_produk->add('<th style="padding:10px;text-align:left;">');
            $table_produk->add(clang::__('Total'));
            $table_produk->add('</th>');
            $table_produk->add('</tr>');

            if ($transaction_type == 'sell') {
                $table_produk->add('<tr>');
                $table_produk->add('<td>');
                $table_produk->add('Deal Sell Gold');
                $table_produk->add('</td>');
                $table_produk->add('<td>');
                $table_produk->add('1');
                $table_produk->add('</td>');
                $table_produk->add('<td class="text-right">');
                $table_produk->add(ctransform::format_currency($total_deal));
                $table_produk->add('</td>');

                $sub_total = $total_deal;

                $table_produk->add('<td class="text-right">');
                $table_produk->add(ctransform::format_currency($sub_total));
                $table_produk->add('</td>');
                $table_produk->add('</tr>');
            } else {
                foreach ($data_product as $row_product) {
                    $product_name = $row_product->name;
                    if ($product_type == 'pulsa') {
                        $product_name.=' - ' . $row_product->pulsa_phone_number;
                    }
                    if (strlen($product_name) == 0) {
                        $product_name = $row_product->product_name;
                    }
                    $table_produk->add('<tr>');
                    $table_produk->add('<td>');
                    $table_produk->add($product_name);
                    $table_produk->add('</td>');
                    $table_produk->add('<td>');
                    $table_produk->add($row_product->qty);
                    $table_produk->add('</td>');
                    $table_produk->add('<td class="text-right">');
                    $table_produk->add(ctransform::format_currency($row_product->channel_sell_price));
                    $table_produk->add('</td>');

                    $sub_total = $row_product->qty * $row_product->transaction_price;

                    $table_produk->add('<td class="text-right">');
                    $table_produk->add(ctransform::format_currency($sub_total));
                    $table_produk->add('</td>');
                    $table_produk->add('</tr>');
                }
            }
            $table_produk->add('<tr>');
            $table_produk->add('<td colspan="3" class="text-right">');
            $table_produk->add('<span class="font-red">Pengiriman</span>');
            $table_produk->add('</td>');
            $table_produk->add('<td class="text-right">');
            $table_produk->add(ctransform::format_currency($total_shipping));
            $table_produk->add('</td>');
            $table_produk->add('</tr>');
            $table_produk->add('<tr>');
            $table_produk->add('<td colspan="3" class="text-right">');
            $table_produk->add('<span class="font-red">Charge</span>');
            $table_produk->add('</td>');
            $table_produk->add('<td class="text-right">');
            $table_produk->add(ctransform::format_currency($total_charge));
            $table_produk->add('</td>');
            $table_produk->add('</tr>');
            $table_produk->add('<tr>');
            $table_produk->add('<td colspan="3" class="text-right">');
            $table_produk->add('<span class="font-red ">Total</span>');
            $table_produk->add('</td>');
            $table_produk->add('<td class="text-right">');

            $table_produk->add(ctransform::format_currency($total_payment));
            $table_produk->add('</td>');
            $table_produk->add('</tr>');
            $table_produk->add('</table>');

            $table_metode_pemesanan = $content->add_div()->add_class('margin-bottom-20');
            $table_metode_pemesanan->add('<table class="table-62hallfamily">');
            $table_metode_pemesanan->add('<tr>');
            $table_metode_pemesanan->add('<th style="padding:10px;text-align:left;width:50%;">');
            $table_metode_pemesanan->add(clang::__('Metode Pembayaran'));
            $table_metode_pemesanan->add('</th>');
            $table_metode_pemesanan->add('<th style="padding:10px;text-align:left;width:50%;">');
            $table_metode_pemesanan->add(clang::__('Status Pembayaran'));
            $table_metode_pemesanan->add('</th>');
            $table_metode_pemesanan->add('</tr>');

            $table_metode_pemesanan->add('<tr>');
            $table_metode_pemesanan->add('<td>');

            $str_payment_type = str_replace('_', ' ', $payment_type);
            $str_payment_type = ucwords($str_payment_type);
            if ($payment_type == 'bank_transfer') {
                $str_payment_type.=' (' . ucwords(str_replace('_', ' ', $data_payment->bank)) . '-' . $data_payment->account_number . ')';
            }
            if ($payment_type == 'speedcash') {
                $file = CF::get_file('data', 'speedcash_code');
                $speedcash_data = array();
                if (file_exists($file)) {
                    $speedcash_data = include $file;
                }
                $speedcash_code = carr::get($speedcash_data, $data_payment->sc_method, 'Unknown');
                $str_payment_type.='(' . $speedcash_code . ')';
            }
            $table_metode_pemesanan->add(ucwords($str_payment_type));
            $table_metode_pemesanan->add('</td>');
            $table_metode_pemesanan->add('<td>');

            $transaction_status = NULL;
            if ($this->type_email == 'order') {
                if ($invoice_info->order_status != 'APPROVED') {
                    $transaction_status = $invoice_info->order_status;
                    if ($transaction_status == 'SUCCESS') {
                        $transaction_status = 'PAYMENT SUCCESS';
                    }
                }
            } else if ($this->type_email == 'payment') {
                $transaction_status = $invoice_info->payment_status;
                if ($transaction_status == 'SUCCESS') {
                    $transaction_status = 'PAYMENT SUCCESS';
                }
            }

            $table_metode_pemesanan->add(strtoupper($transaction_status));

            $table_metode_pemesanan_action = $table_metode_pemesanan->add_div()->add_class('invoice-action-row text-right');

            if ($payment_type == 'bank_transfer' and $payment_payment_status == 'PENDING' and $check_transaction_status == 'PENDING') {
                $link = curl::httpbase() . 'payment/payment_confirmation/' . $invoice_info->transaction_id;
                $table_metode_pemesanan_action->add('<a target="_blank" class="btn-62hallfamily bg-red border-3-red" id="confirm" href = ' . $link . '>KONFIRMASI</a>');
//                $link = curl::httpbase() . 'payment/reset_payment/' . $invoice_info->transaction_id;
//                $table_metode_pemesanan_action->add('<a target="_blank" class="btn-62hallfamily bg-red border-3-red" id="payment" href = ' . $link . '>GANTI PEMBAYARAN</a>');
            }

            $table_metode_pemesanan->add('</td>');
            $table_metode_pemesanan->add('</tr>');
            $table_metode_pemesanan->add('</table>');

            $page_action = $container->add_div()
                    ->add_class('col-md-12 padding-0 margin-bottom-40 text-right');
            if ($invoice_info->order_status == 'APPROVED') {
                $action_request_payment = $page_action->add_action()
                        ->add_class('btn-62hallfamily bg-red border-3-red')
                        ->set_label(clang::__('PEMBAYARAN'))
                        ->set_link(curl::base() . 'payment/set_session_transaction_from_database/' . $invoice_info->transaction_id);
            } else {
                $action_cek_status = $page_action->add_action()
                        ->add_class('btn-62hallfamily bg-red border-3-red')
                        ->set_label(clang::__('CEK STATUS ORDER'))
                        ->set_link(curl::base() . 'retrieve/status_order/' . $invoice_info->transaction_id);
            }

            $action_kembali = $page_action->add_action()
                    ->add_class('btn-62hallfamily bg-red border-3-red')
                    ->set_label(clang::__('KEMBALI'))
                    ->set_link(curl::base());
        } else {
            
        }
        $html->appendln(parent::html($index));
        return $html->text();
    }

    public function js($index = 0) {
        $js = new CStringBuilder();
        $custom_js = "";
        $js->appendln($custom_js);
        $js->appendln(parent::js($index));
        return $js->text();
    }

    public function generate_email() {
        $message = NULL;
        $db = CDatabase::instance();

        if (strlen($this->booking_code) > 0) {
            $transaction_info = transaction::get_transaction_contact('booking_code', $this->booking_code);
            $transaction_detail = transaction::get_transaction_detail('transaction_id', $transaction_info->transaction_id);
            $transaction_payment = transaction::get_transaction_payment('transaction_id', $transaction_info->transaction_id);

            $org_id = $transaction_info->org_id;
            $org = cdbutils::get_row('select * from org where org_id=' . $db->escape($org_id));
            $domain = $org->domain;
            $table_item_sell = '';
            $total_price = 0;
            $total_charge = 0;
            $total_shipping = 0;
            $total_payment = 0;
            $payment_type = '';
            $payment_payment_status = '';

            if ($transaction_info) {
                $total_price = $transaction_info->channel_sell_price;
                $total_shipping = $transaction_info->total_shipping;
            }

            if ($transaction_payment) {
                $total_charge = $transaction_payment->total_charge;
                $total_payment = $total_price + $total_shipping + $total_charge;
                $payment_type = $transaction_payment->payment_type;
                $payment_payment_status = $transaction_payment->payment_status;
            }

            $name = $transaction_info->billing_first_name;

            $product_type = NULL;
            if ($transaction_info->product_type == 'gold') {
                if ($transaction_info->type == 'buy') {
                    $product_type = 'beli emas';
                } else {
                    $product_type = 'jual emas';
                }
            } else if ($transaction_info->product_type == 'service') {
                $product_type = 'jasa';
            }

            // HEADER
            $header_message = NULL;
            if ($this->type_email == 'order') {
                $this->transaction_header = TRUE;
                $header_message = "Kami telah menerima pesanan " . $product_type . " Anda. Kami akan memproses pesanan " . $product_type . " Anda setelah pembayaran dikonfirmasi.";
            } else if ($this->type_email == 'payment') {
                if ($this->status == 'SUCCESS') {
                    $this->transaction_header = TRUE;
                    $header_message = "Kami telah menerima pembayaran Anda. Pesanan akan segera kami proses. Anda akan mendapat email konfirmasi apabila produk pesanan telah dikirim.";
                } else if ($this->status == 'FAILED') {
                    $this->transaction_header = TRUE;
                    $header_message = "Mohon maaf request pembayaran Anda untuk pesanan ini gagal. Silahkan melakukan proses ulang pembayaran atau melakukan konfirmasi pembayaran dengan klik disini.";
                }
            } else if ($this->type_email == 'confirm') {
                $this->transaction_header = TRUE;
                $header_message = "Konfirmasi pembayaran anda telah kami terima.  Kami akan segera memproses konfirmasi pembayaran anda.";
            } else if ($this->type_email == 'shipping') {
                $this->transaction_header = TRUE;
                $header_message = "Pesanan Anda telah dikirim pada: " . date('d/m/Y H:i');
            } else if ($this->type_email == 'arrival') {
                $this->transaction_header = TRUE;
                $header_message = "Pesanan Anda telah sampai pada: " . date('d/m/Y H:i');
            } else if ($this->type_email == 'request') {
                if ($this->status == 'ORDER') {
                    $this->transaction_header_request = TRUE;
                    $header_message = "Request pesanan Anda telah kami terima. Harap menunggu request pesanan Anda tiba setelah proses konfirmasi dari kami.";
                } else if ($this->status == 'SUCCESS') {
                    $header_message = "Request pesanan Anda tersedia dan telah kami periksa.<br>Request barang pesanan Anda adalah sebagai berikut:";
                } else if ($this->status == 'FAILED') {
                    $this->transaction_method = FALSE;
                    $header_message = "Mohon maaf request pesanan Anda untuk saat ini tidak tersedia.<br>Request barang pesanan Anda cari adalah sebagai berikut:";
                }
            }

            // TRANSACTION STATUS
            $transaction_status = NULL;
            if ($this->type_email == 'order') {
                $transaction_status = $transaction_info->order_status;
                if ($transaction_status == 'SUCCESS') {
                    $transaction_status = 'PAYMENT SUCCESS';
                }
            } else if ($this->type_email == 'payment') {
                $transaction_status = $transaction_info->payment_status;
                if ($transaction_status == 'SUCCESS') {
                    $transaction_status = 'PAYMENT SUCCESS';
                }
            } else if ($this->type_email == 'confirm') {
                $transaction_status = $transaction_info->payment_status;
                if ($transaction_status == 'SUCCESS') {
                    $transaction_status = 'PAYMENT SUCCESS';
                }
            }
            $konfirmasi = NULL;
            $guide_permata_va = NULL;
            if ($transaction_payment) {
                if ($transaction_payment->payment_type == 'bank_transfer' and $transaction_status == 'PENDING') {
                    $link = curl::httpbase() . 'payment/payment_confirmation/' . $transaction_info->transaction_id;

                    $konfirmasi .='<a target="_blank" style="
                                                background: #F7A63A !important;
                                                padding: 8px !important;
                                                border-radius: 0px !important;
                                                color: #fff !important;
                                                font-size: 14px !important;
                                                font-weight: bold !important;
                                                text-decoration: none !important;
                                                float: right;
                                            " href = ' . $link . '>Konfirmasi</a>';
                }

                if ($transaction_payment->payment_type == 'permata_va') {
                    $guide_permata_va .= '<b style="font-size: 16px">Petunjuk Melakukan Transfer ke Permata VA</b><br><br>';
                    $guide_permata_va .= '<b>1. Membayar melalui ATM Mandiri/Jaringan ATM Bersama:<b><br>
                                            - Pada menu utama, pilih "Other Transaction".<br>
                                            - Pilih "Transfer".<br>
                                            - Pilih "Online Inter Bank".<br>
                                            - masukan nomor 013 ' . $transaction_payment->account_number . ' (kode 013 dan 16 digit Virtual Account)<br>
                                            - Masukan total yang harus dibayarkan. Jumlah yang tidak tepat akan membatalkan transaksi.<br>
                                            - Jumlah yang harus dibayar dan nomor rekening akan muncul pada halaman konfirmasi pembayaran. <br>
                                            - Jika informasi sudah benar, pilih "Correct".<br><br>';

                    $guide_permata_va .= '<b>2. Membayar melalui ATM BCA/Jaringan ATM PRIMA:<b><br>
                                            - Pada menu utama, pilih "Other Transaction".<br>
                                            - Pilih "Transfer".<br>
                                            - Pilih "Other Bank Account".<br>
                                            - Masukan kode 013 untuk Bank Permata dan pilih "Correct".<br>
                                            - Masukan total yang harus dibayarkan. Jumlah yang tidak tepat akan membatalkan transaksi.<br>
                                            - Masukan ' . $transaction_payment->account_number . ' (16 digit virtual account) lalu klik "Correct".<br>
                                            - Jumlah yang harus dibayar dan nomor rekening akan muncul pada halaman konfirmasi pembayaran. Jika informasi sudah benar, pilih "Correct".<br><br>';

                    $guide_permata_va .= '<b>3. Membayar melalui ATM Permata/ATM Alto:<b><br>
                                            - Pada menu utama, pilih "Other Transaction".<br>
                                            - Pilih "Payment Transaction".<br>
                                            - Pilih "Other".<br>
                                            - Pilih pembayaran Virtual Account.<br>
                                            - Masukan 16 digit Virtual Account ' . $transaction_payment->account_number . '<br>
                                            - Nomor virtual account dan jumlah yang harus dibayarkan akan muncul pada halaman konfirmasi pembayaran. Pilih "Correct".<br>
                                            - Pilih rekening pembayaran anda, kemudian pilih "Correct".<br><br>';
                }
            }
            // PAYMENT TYPE
            $str_payment_type = '';
            if ($transaction_payment) {
                $str_payment_type = str_replace('_', ' ', $payment_type);
            }
            if ($payment_type == 'bank_transfer') {
                $str_payment_type.=' (' . ucwords(str_replace('_', ' ', $transaction_payment->bank)) . '-' . $transaction_payment->account_number . ')';
            }
            if ($payment_type == 'speedcash') {
                $file = CF::get_file('data', 'speedcash_code');
                $speedcash_data = array();
                if (file_exists($file)) {
                    $speedcash_data = include $file;
                }
                $speedcash_code = carr::get($speedcash_data, $data_payment->sc_method, 'Unknown');
                $str_payment_type.='(' . $speedcash_code . ')';
            }

            // TRANSACTION HEADER
            $transaction_header = NULL;
            if ($this->transaction_header) {

                $transaction_date_hold = NULL;
                if ($this->type_email == 'order') {
                    $transaction_date_hold = '
                                <tr>
                                    <td width="200px">
                                        Batas Waktu
                                    </td>
                                    <td>
                                    : ' . date('d/m/Y H:i', strtotime($transaction_info->date_hold)) . '
                                    </td>
                                </tr>';
                }

                $transaction_header .= '<table width="100%" style="font-weight:bold">
                                <tr>
                                    <td width="200px">
                                        Kode Pembelian
                                    </td>
                                    <td>
                                    : ' . $transaction_info->booking_code . '
                                    </td>
                                </tr>
                                <tr>
                                    <td width="200px">
                                        Tanggal Pembelian
                                    </td>
                                    <td>
                                    : ' . date('d/m/Y H:i', strtotime($transaction_info->date)) . '
                                    </td>
                                </tr>
                                <tr>
                                    <td width="200px">
                                        Metode Pembayaran
                                    </td>
                                    <td>
                                    : ' . ucwords(str_replace('_', ' ', $payment_type)) . '
                                    </td>
                                </tr>
                                <tr>
                                    <td width="200px">
                                        Status
                                    </td>
                                    <td>
                                    : ' . ucfirst($transaction_status) . '
                                    </td>
                                </tr>
                                ' . $transaction_date_hold . '
                            </table>';
            }

            $transaction_header_request = NULL;
            if ($this->transaction_header_request) {
                $transaction_header .= '<table width="100%" style="font-weight:bold">
                                <tr>
                                    <td width="200px">
                                        Kode Request
                                    </td>
                                    <td>
                                    : ' . $transaction_info->booking_code . '
                                    </td>
                                </tr>
                                <tr>
                                    <td width="200px">
                                        Tanggal Request
                                    </td>
                                    <td>
                                    : ' . date('d/m/Y H:i', strtotime($transaction_info->date)) . '
                                    </td>
                                </tr>
                                <tr>
                                    <td width="200px">
                                        Status
                                    </td>
                                    <td>
                                    : ' . ucfirst($transaction_status) . '
                                    </td>
                                </tr>
                            </table>';
            }

            // TABLE CONTACT
            $table_contact = NULL;
            $table_contact .= '<table style="width:98%; font-size:18px; border:1px solid #ccc;" cellspacing="0">';
            $table_contact .= '<tr>';
            $table_contact .= '<th style="background-color: #F3F3F3; padding:10px; color:#333" colspan="2" align="left">';
            $table_contact .= 'Detail Pesanan';
            $table_contact .= '</th>';
            $table_contact .= '</tr>';

            $table_contact .= '<tr>';
            $table_contact .= '<td style="padding: 10px; border: 1px solid #ccc">';
            $table_contact .= 'Order ID : ' . $transaction_info->booking_code . '<br>';
            $table_contact .= 'Tanggal : ' . date('d/m/Y H:i', strtotime($transaction_info->date));
            $table_contact .= '</td>';
            $table_contact .= '<td style="padding: 10px; border: 1px solid #ccc">';
            $table_contact .= 'Email : ' . $transaction_info->shipping_email . '<br>';
            $table_contact .= 'Telepon : ' . $transaction_info->shipping_phone;
            $table_contact .= '</td>';
            $table_contact .= '</tr>';
            $table_contact .= '</table>';

            // TABLE ADDRESS
            $table_address = NULL;
            $table_address .= '<table style="width:98%; font-size:18px; border:1px solid #ccc;" cellspacing="0">';
            $table_address .= '<tr>';
            $table_address .= '<th style="background-color: #F3F3F3; border:1px solid #ccc; padding:10px; color:#333;" align="left">';
            $table_address .= 'Alamat Pembayaran';
            $table_address .= '</th>';
            $table_address .= '<th style="background-color: #F3F3F3; border:1px solid #ccc; padding:10px; color:#333;" align="left">';
            $table_address .= 'Alamat Pengiriman';
            $table_address .= '</th>';
            $table_address .= '</tr>';

            $table_address .= '<tr>';
            $table_address .= '<td style="padding: 10px; border: 1px solid #ccc">';
            $table_address .= $transaction_info->billing_first_name . ' ' . $transaction_info->billing_last_name . '<br>';
            $table_address .= $transaction_info->billing_address . '<br>';
            $table_address .= $transaction_info->billing_city . ' ' . $transaction_info->billing_postal_code . '<br>';
            $table_address .= 'No Handphone ' . $transaction_info->billing_phone;
            $table_address .= '</td>';
            $table_address .= '<td style="padding: 10px; border: 1px solid #ccc">';
            $table_address .= $transaction_info->shipping_first_name . ' ' . $transaction_info->shipping_last_name . '<br>';
            $table_address .= $transaction_info->shipping_address . '<br>';
            $table_address .= $transaction_info->shipping_city . ' ' . $transaction_info->shipping_postal_code . '<br>';
            $table_address .= 'No Handphone ' . $transaction_info->shipping_phone;
            $table_address .= '</td>';
            $table_address .= '</tr>';
            $table_address .= '</table>';

            if ($transaction_info->type == 'sell') {
                $table_item_sell = '';
                $table_item_sell .= '<table style="width:98%; font-size:18px; border:1px solid #ccc;" cellspacing="0">';
                $table_item_sell .= '<tr>';
                $table_item_sell .= '<th style="background-color: #F3F3F3; padding:10px; color:#333; border:1px solid #ccc;" align="left">';
                $table_item_sell .= 'Produk';
                $table_item_sell .= '</th>';
                $table_item_sell .= '<th style="background-color: #F3F3F3; padding:10px; color:#333; width:100px; border:1px solid #ccc;" align="left">';
                $table_item_sell .= 'Jumlah';
                $table_item_sell .= '</th>';
                $table_item_sell .= '<th style="background-color: #F3F3F3; padding:10px; color:#333; border:1px solid #ccc;"  align="left">';
                $table_item_sell .= 'Harga';
                $table_item_sell .= '</th>';
                $table_item_sell .= '<th style="background-color: #F3F3F3; padding:10px; color:#333; border:1px solid #ccc;" align="left">';
                $table_item_sell .= 'Total';
                $table_item_sell .= '</th>';
                $table_item_sell .= '</tr>';
                foreach ($transaction_detail as $row_product) {
                    $product_name = $row_product->name;
                    $table_item_sell .= '<tr>';
                    $table_item_sell .= '<td style="padding: 10px; border: 1px solid #ccc">';
                    $table_item_sell .= $product_name;
                    $table_item_sell .= '</td>';
                    $table_item_sell .= '<td style="padding: 10px; border: 1px solid #ccc; text-align:right">';
                    $table_item_sell .= $row_product->qty;
                    $table_item_sell .= '</td>';
                    $table_item_sell .= '<td style="padding: 10px; border: 1px solid #ccc; text-align:right">';
                    $table_item_sell .= ctransform::format_currency($row_product->transaction_price);
                    $table_item_sell .= '</td>';

                    $sub_total = $row_product->qty * $row_product->channel_sell_price;

                    $table_item_sell .= '<td style="padding: 10px; border: 1px solid #ccc; text-align:right">';
                    $table_item_sell .= ctransform::format_currency($sub_total);
                    $table_item_sell .= '</td>';
                    $table_item_sell .= '</tr>';
                }
                $table_item_sell .='</table>';
//                $table_produk->add_br();
            }
            // TABLE ITEM
            $table_item = NULL;
            $table_item .= '<table style="width:98%; font-size:18px; border:1px solid #ccc;" cellspacing="0">';
            $table_item .= '<tr>';
            $table_item .= '<th style="background-color: #F3F3F3; padding:10px; color:#333; border:1px solid #ccc;" align="left">';
            $table_item .= 'Produk';
            $table_item .= '</th>';
            $table_item .= '<th style="background-color: #F3F3F3; padding:10px; color:#333; width:100px; border:1px solid #ccc;" align="left">';
            $table_item .= 'Jumlah';
            $table_item .= '</th>';
            $table_item .= '<th style="background-color: #F3F3F3; padding:10px; color:#333; border:1px solid #ccc;"  align="left">';
            $table_item .= 'Harga';
            $table_item .= '</th>';
            $table_item .= '<th style="background-color: #F3F3F3; padding:10px; color:#333; border:1px solid #ccc;" align="left">';
            $table_item .= 'Total';
            $table_item .= '</th>';
            $table_item .= '</tr>';

            if ($transaction_info->type == 'sell') {
                $table_item.='<tr>';
                $table_item.='<td>';
                $table_item.='Deal Sell Gold';
                $table_item.='</td>';
                $table_item.='<td>';
                $table_item.='1';
                $table_item.='</td>';
                $table_item.='<td class="text-right">';
                $table_item.=ctransform::format_currency($transaction_info->total_deal);
                $table_item.='</td>';

                $sub_total = $transaction_info->total_deal;

                $table_item.='<td class="text-right">';
                $table_item.=ctransform::format_currency($sub_total);
                $table_item.='</td>';
                $table_item.='</tr>';
            } else {
                foreach ($transaction_detail as $row_product) {
                    $product_name = $row_product->name;
                    if ($transaction_info->product_type == 'pulsa') {
                        $product_name.=' - ' . $row_product->pulsa_phone_number;
                    }
                    if (strlen($product_name) == 0) {
                        $product_name = $row_product->product_name;
                    }

                    $table_item .= '<tr>';
                    $table_item .= '<td style="padding: 10px; border: 1px solid #ccc">';
                    $table_item .= $product_name;
                    $table_item .= '</td>';
                    $table_item .= '<td style="padding: 10px; border: 1px solid #ccc; text-align:right">';
                    $table_item .= $row_product->qty;
                    $table_item .= '</td>';
                    $table_item .= '<td style="padding: 10px; border: 1px solid #ccc; text-align:right">';
                    $table_item .= ctransform::format_currency($row_product->transaction_price);
                    $table_item .= '</td>';

                    $sub_total = $row_product->qty * $row_product->channel_sell_price;

                    $table_item .= '<td style="padding: 10px; border: 1px solid #ccc; text-align:right">';
                    $table_item .= ctransform::format_currency($sub_total);
                    $table_item .= '</td>';
                    $table_item .= '</tr>';
                }
            }
            $table_item .='<tr>';
            $table_item .='<td style="padding: 10px; border: 1px solid #ccc; text-align:right" colspan="3">';
            $table_item .='<b style="color:#333">Pengiriman</b>';
            $table_item .='</td>';
            $table_item .='<td style="padding: 10px; border: 1px solid #ccc; text-align:right">';
            $table_item .= ctransform::format_currency($total_shipping);
            $table_item .='</td>';
            $table_item .='</tr>';

            $table_item .='<tr>';
            $table_item .='<td style="padding: 10px; border: 1px solid #ccc; text-align:right;" colspan="3">';
            $table_item .='<b style="color:#333">Charge</b>';
            $table_item .='</td>';
            $table_item .='<td style="padding: 10px; border: 1px solid #ccc; text-align:right;">';
            $table_item .=ctransform::format_currency($total_charge);
            $table_item .='</td>';
            $table_item .='</tr>';

            $table_item .='<tr>';
            $table_item .='<td style="padding: 10px; border: 1px solid #ccc; text-align:right;" colspan="3">';
            $table_item .='<b style="color:#333">Total</b>';
            $table_item .='</td>';
            $table_item .='<td style="padding: 10px; border: 1px solid #ccc; text-align:right;">';

            $total_payment = $total_price + $total_charge + $total_shipping;

            $table_item .= ctransform::format_currency($total_payment);
            $table_item .='</td>';
            $table_item .='</tr>';
            $table_item .='</table>';

            // TABLE METODE
            $table_metode = NULL;
            $table_metode .='<table style="width:98%; font-size:18px" cellspacing="0">';
            $table_metode .='<tr>';
            $table_metode .='<th style="padding: 10px; border: 1px solid #ccc; background-color: #F3F3F3; color: #333; text-align:left">';
            $table_metode .='Metode Pembayaran';
            $table_metode .='</th>';
            $table_metode .='<th style="padding: 10px; border: 1px solid #ccc; background-color: #F3F3F3; color: #333; text-align:left">';
            $table_metode .='Status Pembayaran';
            $table_metode .='</th>';
            $table_metode .='</tr>';

            $table_metode .='<tr>';
            $table_metode .='<td style="padding: 10px; border: 1px solid #ccc">';
            $table_metode .= ucwords($str_payment_type);

            $table_metode .='</td>';
            $table_metode .='<td style="padding: 10px; border: 1px solid #ccc">';
            $table_metode .= ucfirst($transaction_status);
            $table_metode .= $konfirmasi;

            $table_metode .='</td>';
            $table_metode .='</tr>';
            $table_metode .='</table>';

            if ($this->transaction_method == FALSE) {
                $table_metode = NULL;
                $table_metode .='<table style="width:98%; font-size:18px" cellspacing="0">';
                $table_metode .='<tr>';
                $table_metode .='<th style="padding: 10px; border: 1px solid #ccc; background-color: #F3F3F3; color: #333; text-align:left">';
                $table_metode .='Keterangan Vendor';
                $table_metode .='</th>';
                $table_metode .='</tr>';

                $table_metode .='<tr>';
                $table_metode .='<td style="padding: 10px; border: 1px solid #ccc">';
                $table_metode .='Mohon maaf pesanan anda saat ini belum tersedia. <br> Terima Kasih telah memesan produk kami.';
                $table_metode .='</td>';
                $table_metode .='</tr>';
                $table_metode .='</table>';
            }



            $link_belanja_lagi = NULL;
            if ($this->type_email == 'payment' and $this->type_email == 'request') {
                $link_belanja_lagi .='<br><br>';
                $link_belanja_lagi .='<span style="font-size:18px">Terima kasih telah berbelanja di ' . $this->org_name . '</span><br><br>';
                $link_belanja_lagi .='<a href="' . curl::httpbase() . '" style="background: #F3F3F3; padding:10px;font-size:20px;text-decoration:none !important;color:white"><b>Belanja Lagi</b></a><br>';
            }


            $sosial_media = NULL;
            $facebook = cms_options::get('media_fb');
            $twitter = cms_options::get('media_tw');
            $instagram = cms_options::get('media_ig');
            $google_plus = cms_options::get('media_gp');

            $link_facebook = NULL;
            if ($facebook) {
                $link_facebook = '<a target="_blank" href="' . $facebook . '"><img style="margin:10px"src="' . curl::httpbase() . 'application/ittronmall/default/media/img/imlasvegas/white-fb.png' . '"/></a>';
            }

            $link_twitter = NULL;
            if ($twitter) {
                $link_twitter = '<a target="_blank" href="' . $twitter . '"><img style="margin:10px"src="' . curl::httpbase() . 'application/ittronmall/default/media/img/imlasvegas/white-tw.png' . '"/></a>';
            }

            $link_instagram = NULL;
            if ($instagram) {
                $link_instagram = '<a target="_blank" href="' . $instagram . '"><img style="margin:10px"src="' . curl::httpbase() . 'application/ittronmall/default/media/img/imlasvegas/white-ig.png' . '"/></a>';
            }
            $link_google_plus = NULL;
            if ($google_plus) {
                $link_google_plus = '<a target="_blank" href="' . $google_plus . '"><img style="margin:10px"src="' . curl::httpbase() . 'application/ittronmall/default/media/img/imlasvegas/white-gp.png' . '"/></a>';
            }

            $footer = CView::factory('email/footer');
            $sosmed = array(
                $link_facebook, $link_twitter, $link_instagram, $link_google_plus
            );
            $footer->sosmed = $sosmed;
            $footer->member_email = $this->recipient;
            $footer->org_id = CF::org_id();
            $link_help = curl::httpbase() . 'read/page/index/beli-produk-di-' . cstr::sanitize($org->name);

            $sosial_media = '<div style="width:98%;background:#ccc;">
                            <center>
                                <div style="margin-left:20px; margin-right:20px; border-top:2px solid #6b6b6b; border-bottom: 2px solid #6b6b6b;padding-top:20px; padding-bottom:20px;font-size:14px">
                                    Ikuti kami untuk memperoleh update promo terbaru
                                    <div style="width:220px;margin:0 auto;">
                                        ' . $link_facebook . '
                                        ' . $link_twitter . '
                                        ' . $link_instagram . '
                                        ' . $link_google_plus . '
                                    </div>
                                    Email ini dikirimkan kepada <span style="color: #F3F3F3;">' . $this->recipient . '</span>.<br>
                                    Jika Anda membutuhkan bantuan, silahkan klik di <a href="' . $link_help . '" style="color:#F3F3F3; text-decoration:underline;">sini</a>.
                                </div>
                            </center>
                        </div>';

            $message = '
                <body style="padding-top: 8px;font-family: "Open Sans", Helvetica, Arial, sans-serif;font-size:20px;">
                    <div style="
                        width: 98%;
                        border-bottom: 2px solid #F3F3F3;
                        text-align: center;
                        border-top: 10px solid #b90303;
                        padding-bottom: 8px;
                        padding-top: 13px;">
                        <img src="' . $this->logo . '" width="250" height="90"><br/><br/>
                    </div>
                    <div style="padding:20px;width:100%">
                        <div style="font-size:24px;">
                            <b>Hi ' . $name . ',</b>
                        </div>
                        <div style="margin-top:30px;font-size:18px;">' .
                    '<b>' . $header_message . '</b>' .
                    '<br><br>
                        <b>
                        ' . $transaction_header . '
                        ' . $transaction_header_request . '
                        </b>
                    </div>
                    <br><br>
                    ' . $table_contact . '
                    <br><br>
                    ' . $table_address . '
                    <br><br>
                    ' . $table_item_sell . '
                    <br><br>
                    ' . $table_item . '
                    <br><br>
                    ' . $table_metode . '
                    ' . $guide_permata_va . '
                    ' . $link_belanja_lagi . '
                   ' . $footer->render() . '
                </body>
            ';
        }

//   echo $footer->render();
        return $message;
    }

}
