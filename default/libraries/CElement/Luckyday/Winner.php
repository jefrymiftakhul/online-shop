<?php

/**
 * Description of Winner
 *
 * @author Ecko Santoso
 * @since 19 Sep 16
 */
class CElement_Luckyday_Winner extends CElement {
    
    protected $data;
    
    public function __construct($id = '') {
        parent::__construct($id);
        $this->data = array();
    }
    
    public static function factory($id = '') {
        return new CElement_Luckyday_Winner($id);
    }
    
    public function set_list($list) {
        $this->data = $list;
        return $this;
    }
    
    private function get_image($image_name) {
        $retcode=200;
        if ($retcode == 500 or $image_name == NULL) {
            $image_name = curl::base() . 'application/62hallfamily/default/media/img/product/no-image.png';
        } else {
            $image_name = image::get_image_url($image_name, 'view_all');
        }
        //curl_close($ch);
        return $image_name;
    }
    
    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        
        $title_div = $this->add_div()->add_class('widget-title');
        $title_div->add('<h3>Winner</h3> <!--a href="">More</a-->');
        $container = $this->add_div($this->id.'-carousel')->add_class('winner-carousel');
        $container_inner = $container->add_div()->add_class('outer-item');
        if ($this->data != null) {
            foreach ($this->data as $list_k => $list_v) {
                $product_name = carr::get($list_v, 'name');
                $email = carr::get($list_v, 'email');
                $member_name = carr::get($list_v, 'member_name');
                $updated = carr::get($list_v, 'updated');
                
                $text_content = '<p class="truncate">'. $member_name. '<br><span class="subtitle">'.$product_name.'</span></p>';
                $image_src = $this->get_image(carr::get($list_v, 'image_name'));
                $item = $container_inner->add_div()->add_class('item');
                    $content_img = $item->add_div()->add_class('content-img');
                        $img = $content_img->add_img()->set_src($image_src)->add_class('img-responsive');
                    $content = $item->add_div()->add_class('content');
                        $content->add($text_content);
            }
        }
        
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent = 0) {
        $js = new CStringBuilder();
        
//        $js->appendln('
//            $("#'.$this->id.'-carousel").owlCarousel({
//                loop: true,
//                autoplay: true,
//                items: 1,
//                nav: false,
//                autoplayHoverPause: true,
//                animateOut: "slideOutUp",
//                animateIn: "slideInUp"
//              }); ');
        $js->appendln("
            $('.winner-carousel .outer-item .item').removeClass('active');
            $('.winner-carousel .outer-item .item').on('click', function(){
                $('.winner-carousel .outer-item .item').removeClass('active');
                $(this).addClass('active');
            });
            
            function animateUpDown() {
                $('.winner-carousel').css('overflow', 'hidden');
                var container_height = $('.winner-carousel').height();
                var container_item_height = $('.winner-carousel .outer-item').height();
                var heightToAnimate = container_item_height - container_height;
                $('.winner-carousel .outer-item').css('position', 'relative');
                
            
              $('.winner-carousel .outer-item').animate({
                top: '-'+heightToAnimate+'px'
              }, 2000, function () {
                $('.winner-carousel .outer-item').animate({
                  top: '0px'
                }, 2000, function () {
                  animateUpDown();
                });
              })
            }
            
            animateUpDown();
            ");
        $js->append(parent::js($indent));
        return $js->text();
    }
}
