<?php

/**
 * Description of ProgressBar
 *
 * @author Ecko Santoso
 * @since 02 Sep 16
 */
class CElement_Luckyday_ProgressBar extends CElement {
    
    protected $max;
    protected $ongoing;

    public function __construct($id= '') {
        parent::__construct($id);
        
        $this->max = 0;
        $this->ongoing = 0;
    }
    
    public static function factory($id = '') {
        return new CElement_Luckyday_ProgressBar($id);
    }
    
    public function set_max($max) {
        $this->max = $max;
        return $this;
    }
    
    public function set_ongoing($ongoing) {
        $this->ongoing = $ongoing;
        return $this;
    }
    
    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        
        $quota = $this->max;
        $slot = $this->ongoing;
        $slot_empty = $quota - $slot;
        $bar_percent = ($slot * 100) / $quota;
        
        $progress = $this->add_div()->add_class('progress');
            $progress_bar = $progress->add_div()->add_class('progress-bar progress-bar-warning')
                    ->add_attr('role', 'progressbar')
                    ->add_attr('aria-valuenow', $slot)
                    ->add_attr('aria-valuemin', 0)
                    ->add_attr('aria-valuemax', $quota)
                    ->add_attr('style', 'width : '.$bar_percent.'%');
                $progress_bar->add_div()->add_class('sr-only')->add('Ayo buruan kurang '.$slot_empty.' lagi');
        $progress_label = $this->add_div()->add_class('progress-bar-label clearfix');
            //$progress_label_quota = $progress_label->add_div()->add_class('pull-left')->add('Total '.$quota);
            $progress_label_slot_empty = $progress_label->add_div()->add_class('pull-right')->add('Kurang '.$slot_empty);
        
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent = 0) {
        $js = new CStringBuilder();
        
        $js->append(parent::js($indent));
        return $js->text();
    }
}
