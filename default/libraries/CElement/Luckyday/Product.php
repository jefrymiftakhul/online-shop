<?php

class CElement_Luckyday_Product extends CElement {

    private $column = NULL;
    private $product_id = '';
    private $product_url = NULL;
    private $product;
    private $using_class_div_gold;

    public function __construct($id = '') {
        parent::__construct($id);
        $this->using_class_div_gold = false;
        $this->product = array();
    }

    public static function factory($id = '') {
        return new CElement_Luckyday_Product($id);
    }

    public function set_product_id($id) {
        $this->product_id = $id;
        return $this;
    }
    
    public function set_product_url($name) {
        $this->product_url = $name;
        return $this;
    }

    public function set_using_class_div_gold($bool) {
        $this->using_class_div_gold = $bool;
        return $this;
    }

    public function set_column($val) {
        $this->column = $val;
        return $this;
    }
    
    public function set_product($array) {
        $this->product = $array;
        return $this;
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        if (empty($this->column)) {
            $col = 3;
            $col_small = 3;
            $col_xsmall = 2;
        } else {
            $col = 12 / $this->column;
            $col_small = 24 / $this->column;
            $col_xsmall = 36 / $this->column;
        }

        if ($col_xsmall > 6 && $col_xsmall <= 9) {
            $col_xsmall = 6;
        }
        if ($col_xsmall > 9) {
            $col_xsmall = 12;
        }

        if ($this->column == 3) {
            $col_xsmall = 6;
        }

        if ($col_small > 4 && $col_small < 9) {
            $col_small = 4;
        }
        $class_div_gold = '';
        if ($this->using_class_div_gold) {
            $class_div_gold = 'col-xs-' . $col_xsmall . ' col-sm-' . $col_small . ' col-md-' . $col;
        }
        
        $product = $this->product;
            $image_name = cobj::get($product, 'image_name');
            $image_url = cobj::get($product, 'file_path');
            $name = cobj::get($product, 'name');
            $quota = cobj::get($product, 'quota');
            $slot_empty = cobj::get($product, 'stock');
            $slot = $quota - $slot_empty;
            $product_url = cobj::get($product, 'url_key');
            $bar_percent = ($slot * 100) / $quota;
            
        $product_item = $this->add_div()->add_class('luckyday-product-item '.$class_div_gold);
            $image_div = $product_item->add_div()->add_class('image');
                $image_src = image::get_image_url($image_name, 'view_all');
                $image_div->add_img()->set_src($image_src);
                
            $product_summary_div = $product_item->add_div()->add_class('summary');
                $product_summary_div->add('<h3 class="title">'.$name.'</h3>');
                
                $progress = $product_summary_div->add_element('62hall-luckyday-progress', 'progress-'.$this->product_id);
                $progress->set_max($quota)
                        ->set_ongoing($slot);
//                $progress = $product_summary_div->add_div()->add_class('progress');
//                    $progress_bar = $progress->add_div()->add_class('progress-bar progress-bar-warning')
//                            ->add_attr('role', 'progressbar')
//                            ->add_attr('aria-valuenow', $slot)
//                            ->add_attr('aria-valuemin', 0)
//                            ->add_attr('aria-valuemax', $quota)
//                            ->add_attr('style', 'width : '.$bar_percent.'%');
//                        $progress_bar->add_div()->add_class('sr-only')->add('Ayo buruan kurang '.$slot_empty.' lagi');
//                $progress_label = $product_summary_div->add_div()->add_class('progress-bar-label clearfix');
//                    $progress_label_quota = $progress_label->add_div()->add_class('pull-left')->add('Total '.$quota);
//                    $progress_label_slot_empty = $progress_label->add_div()->add_class('pull-right')->add('Kurang '.$slot_empty);
                $product_action = $product_summary_div->add_action('detail_'.$this->product_id)->set_label('Beli')->add_class('btn btn-62hallfamily bg-red border-3-red');
        $html->append(parent::html(), $indent);
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $js->append(parent::js($indent));
        $js->appendln("
                $('#detail_" . $this->product_id . "').click(function(){
                    window.location='" . curl::base() . "luckyday/product/item/" . $this->product_url . "';
                });
            ");
        return $js->text();
    }

}
