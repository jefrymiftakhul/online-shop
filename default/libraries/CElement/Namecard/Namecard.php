<?php

class CElement_Namecard_Namecard extends CElement {

    private $column = NULL;
    private $name = NULL;
    private $product_url = NULL;
    private $product_id = '';
    private $image;
    private $price_sell = NULL;
    private $price_buy = NULL;
    private $stock;
    private $using_class_div_gold;

    public function __construct($id = '') {
        parent::__construct($id);
        $this->using_class_div_gold = false;
    }

    public static function factory($id = '') {
        return new CElement_Namecard_Namecard($id);
    }

    public function set_image($image) {
        $this->image = $image;
        return $this;
    }

    public function set_stock($val) {
        $this->stock = $val;
        return $this;
    }

    public function set_product_id($id) {
        $this->product_id = $id;
        return $this;
    }

    public function set_using_class_div_gold($bool) {
        $this->using_class_div_gold = $bool;
        return $this;
    }

    public function set_name($name) {
        $this->name = $name;
        return $this;
    }
    
    public function set_product_url($name) {
        $this->product_url = $name;
        return $this;
    }

    public function set_price_sell($sell) {
        $this->price_sell = $sell;
        return $this;
    }

    public function set_price_buy($buy) {
        $this->price_buy = $buy;
        return $this;
    }

    public function set_column($val) {
        $this->column = $val;
        return $this;
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        if (empty($this->column)) {
            $col = 3;
            $col_small = 3;
            $col_xsmall = 2;
        } else {
            $col = 12 / $this->column;
            $col_small = 24 / $this->column;
            $col_xsmall = 36 / $this->column;
        }

        if ($col_xsmall > 6 && $col_xsmall <= 9) {
            $col_xsmall = 6;
        }
        if ($col_xsmall > 9) {
            $col_xsmall = 12;
        }

        if ($this->column == 3) {
            $col_xsmall = 6;
        }

        if ($col_small > 4 && $col_small < 9) {
            $col_small = 4;
        }
        $class_div_gold = '';
        if ($this->using_class_div_gold) {
            $class_div_gold = 'col-xs-' . $col_xsmall . ' col-sm-' . $col_small . ' col-md-' . $col;
        }
        
        $html->appendln('<div class="product-namecard container product-container ' . $class_div_gold . '">');
        $html->appendln('<center class="namecard-product-title">' . $this->name . '</center>');
        $html->appendln('<center class="namecard-product-price">' . $this->price_sell . '</center>');

        $image = image::get_image_url($this->image, 'namecard');

        $html->appendln('<img width="100%" class="margin" src="' . $image . '"/>');
        
        $html->appendln('<center><input style="width:85%" class="btn btn-success btn-flex bg-red" type="button" id="detail_' . $this->product_id . '" name="detail_' . $this->product_id . '" value="Detail"></center>');
        $html->appendln('<div class="margin-top-20"></div>');
        $html->appendln('</div>');
        $html->append(parent::html(), $indent);
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $js->append(parent::js($indent));
        $js->appendln("
			$('#detail_" . $this->product_id . "').click(function(){
				window.location='" . curl::base() . "namecard/product/item/" . $this->product_url . "';
			});
		");
        return $js->text();
    }

}
