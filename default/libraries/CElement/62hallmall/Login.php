<?php

    class CElement_62hallmall_Login extends CElement {

        protected $session;
        protected $trigger_button;
        protected $redirect;
        protected $reload;
        protected $register_button;
        protected $guest;
        protected $action_url;
        protected $action_next;
        protected $store;
        protected $use_icon;
        protected $element_register = NULL;
        protected $element_register_login = NULL;
        protected $is_login = true;
        private $title_header = NULL;
        protected $display_cart_icon;

        public function __construct($id = '') {
            parent::__construct($id);
            $this->reload = true;
            $this->redirect = curl::base() . curl::current() . CFRouter::$query_string;
            $this->register_button = false;
            $this->guest = false;
            $this->action_url = curl::base() . 'auth/login';
            $this->action_next = false;
            $this->use_icon = true;
            $this->display_cart_icon = false;

            $session = Session::instance();
            if (strlen($session->get('member_id') == 0)) {
                $this->session = false;
            }
            else {
                $this->session = true;
            }
            $org_id = ccfg::get('org_id');
            $data_org = org::get_org($org_id);

            if (!empty($data_org)) {
                $this->store = $data_org->name;
            }
        }

        public static function factory($id = '') {
            return new CElement_62hallmall_Login($id);
        }

        public function set_session($session) {
            $this->session = $session;
            return $this;
        }

        public function set_trigger_button($bool) {
            $this->trigger_button = $bool;
            return $this;
        }

        public function set_action_url($url) {
            $this->action_url = $url;
            return $this;
        }

        public function set_redirect($redirect) {
            $this->redirect = $redirect;
            return $this;
        }

        public function set_reload($reload) {
            $this->reload = $reload;
            return $this;
        }

        public function set_register_button($register_button) {
            $this->register_button = $register_button;
            return $this;
        }

        public function set_icon($use_icon) {
            $this->use_icon = $use_icon;
            return $this;
        }

        public function set_title_header($title_header) {
            $this->title_header = $title_header;
            return $this;
        }

        public function set_guest($bool) {
            $this->guest = $bool;
            return $this;
        }

        public function set_action_next($bool) {
            $this->action_next = $bool;
            return $this;
        }

        public function set_is_login($bool) {
            $this->is_login = $bool;
            return $this;
        }

        public function set_element_register($element) {
            $this->element_register = $element;
            return $this;
        }
        
        public function display_cart_icon($bool) {
            $this->display_cart_icon = $bool;
            return $this;
        }

        public function html_button($indent = 0) {
            $session = Session::instance();
            $is_hide = '';
            if ($this->trigger_button) {
                $div_button = $this->add_div()->add_class('auth hidden-sm hidden-xs');
                if ($this->session == true) {
                    $div_button->add('<ul class="list-inline usermenu">');
                    $member_name = $session->get('name');
                    $member_welcome = $member_name;
                    $member_email = $session->get('email');

                    if (strpos($member_welcome, " ") !== FALSE) {
                        $arr_name = explode(" ", $member_welcome);
                        $member_welcome = $arr_name[0];
                    }

                    $data_member = member::get_data_member();
                    if ($data_member->image_name) {
                        $image = image::get_image_url_front($data_member->image_name, 'view_thumbnail');
                    }
                    else {
                        $image = curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/member/user_red_32x32.png';
                    }

                    $container_welcome = $div_button->add_div()->add_class('welcome');

                    $container_welcome->add('<li class="pull-right">');
                    $user_login = $container_welcome->add_div()->add_class('dropdown');

                    $user_login->add('<img style="height:30px;width:30px;border-radius:50%;border:1px solid #ccc;" src="' . $image . '" class="margin-right-10"/><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">'
                            . 'Welcome, <span class="font-red bold">' . $member_welcome . '</span>'
                            . '</a>');
                    $container_login = $user_login->add('<ul class="dropdown-menu font-black container-welcome triangle-up" role="menu">');
                    $content_login = $container_login->add('<li style="width:100%"><center>');

                    if ($data_member->image_name) {
                        $image_profile = image::get_image_url_front($data_member->image_name, 'view_profile');
                    }
                    else {
                        $image_profile = curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/member/user_gray_150x150.png';
                    }

                    $image_user = $content_login->add('<img style="height:80px;width:80px;border-radius:50%;border:1px solid #ccc;" src="' . $image_profile . '" class="margin-bottom-10"/>');

                    $name = $content_login->add_div()->add_class('font-big bold')
                            ->add($member_name);

                    $email = $content_login->add_div()->add_class('font-small bold')
                            ->add('(' . $member_email . ')');

                    $akun_btn = $content_login->add_action()
                            ->set_label(clang::__("AKUN"))
                            ->add_class('btn-62hallfamily-small bg-red font-white border-3-red font-size12')
                            ->custom_css('margin-right', '10px')
                            ->custom_css('padding-top', '2px !important')
                            ->custom_css('width', '70px')
                            ->set_link(curl::base() . 'member/account');

                    $logout_btn = $content_login->add_action()
                            ->set_label(clang::__("KELUAR"))
                            ->add_class('btn-62hallfamily-small bg-red font-white border-3-red font-size12')
                            ->custom_css('width', '75px')
                            ->custom_css('padding-top', '2px !important')
                            ->set_link(curl::base() . 'member/account/logout');

                    $content_login->add('</center></li>');
                    $container_welcome->add('</li>');
                    $div_button->add('</ul>');
                }
                else {
                    $icon = '';
                    $div_button->add('<ul class="list-inline usermenu pull-right">');
                    if ($this->display_cart_icon) {
                        $div_button->add('<li id="shopping-cart" class="shopping-cart"></li>');
                    }
                    if ($this->element_register != null) {
                        $this->element_register->set_trigger_button(true);
                        $div_button->add('<li class="non-active">');
                        $div_button->add($this->element_register->html_button());
                        $div_button->add('</li>');
                    }
                    $div_button->add('<li class="active">');
                    if ($this->use_icon) {
                        $icon = "<i class='glyphicon glyphicon-log-in'></i> ";
                    }

                    $class = '';
                    if (count($this->classes) > 0) {
                        $class = implode(' ', $this->classes);
                    }
                    $login_btn = $div_button->add_action()->set_label($icon . clang::__("Login"));
                    $login_btn->add_class("btn-modal btn-login btn-danger");
                    $login_btn->add_class($class);
                    $login_btn->add_attr('data-target', 'modal-body-' . $this->id);
                    $login_btn->add_attr('data-modal', 'login_form');
                    $login_btn->add_attr('data-title', clang::__("LOGIN"));
                    $div_button->add('</li>');
                }
                $div_button->add('</ul>');
            }
            else {
                
            }

//            $html->appendln(parent::html($indent));
            //return $html->text();
        }

        public function html($index = 0) {
            $session = Session::instance();
            $html = new CStringBuilder();
            $is_hide = '';
            $this->html_button();
            // BUTTON
            if ($this->trigger_button) {
                $is_hide = '';



                $modal = $this->add_div('custom_modal_' . $this->id)->add_class('modal');
                $modal_dialog = $modal->add_div()->add_class('modal-dialog');
                $modal_content = $modal_dialog->add_div()->add_class('modal-content clearfix');
                $modal_header = $modal_content->add_div()->add_class('modal-header');
                $modal_header->add("<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>");
                $modal_header->add("<h4 class='modal-title'>" . clang::__('LOGIN') . "</h4>");



                $login_form = $modal_content->add_div('modal-body-' . $this->id)->add_class('modal-body login_form clearfix ' . $is_hide);
            }
            else {
                $login_form = $this->add_div()->add_class('login_form clearfix');
                $modal_content = $this->add_div()->add_class('row');
            }
            if ($this->element_register != null) {
                $this->element_register->set_trigger_button(false);
                $this->add($this->element_register->html());
            }

            $form = $login_form->add_form()->add_class('form-login form-62hallfamily');
            $form->set_action($this->action_url);
            $form->set_ajax_submit(false);
            $form->set_validation(false);

            $row_fluid = $form->add_div()->add_class("row-fluid");
            $row_message = $row_fluid->add_div()->add_class('login-message auth-message');

            $row = $form->add_div()->add_class("col-md-12");

            if ($this->guest) {
                $choise = $row->add_field()
                        ->add_control('', 'radio')
                        ->set_name('choise_login')
                        ->set_value('guest')
                        ->set_label(clang::__('Saya checkout sebagai tamu'))
                        ->add_class('bold');
            }
            $element_register = '';
            $element_register_html = '';
            if ($this->register_button) {
                $element_register = CElement_62hallmall_Register::factory()
                        ->set_trigger_button(true)
                        ->set_icon(FALSE)
                        ->set_trigger_button_label('<b><u>'.clang::__('Register Now').'</u></b>')
                        ->set_trigger_button_class('bold font-red');
                $element_register_html = clang::__('Belum Jadi Member?') . '</span><span class="font-red">' . $element_register->html() . '</span>';
                $this->element_register_login = $element_register;
            }
            //$this->element_register = $element_register;
            if (!$this->is_login) {
                $choise = $row->add_field()
                        ->add_control('', 'radio')
                        ->set_name('choise_login')
                        ->set_value('member')
                        ->set_label(clang::__('Saya sudah jadi member di ')
                                . $this->store . '<br><span class="normal">'
                                . $element_register_html
                        )
                        ->add_class('bold');
            }

            if (!empty($this->title_header)) {
                $title_header = $form->add_div()->add_class('col-md-12')
                        ->add($this->title_header);
            }

            $input_username = $row
                    ->add_field()
                    ->set_label(clang::__('Email'))
                    ->add_control("email_login", 'text')
                    ->set_name("email_login")
                    ->add_class("form-control")
                    ->set_placeholder('Email Anda');

            $input_password = $row
                    ->add_field()
                    ->set_label(clang::__('Password'))
                    ->add_control("password_login", 'password')
                    ->set_name("password_login")
                    ->add_class("form-control");

            //$row->add_control('', 'hidden')->set_name('redirect')->set_value(curl::base() . curl::current() . CFRouter::$query_string);
            if ($this->trigger_button == false) {
                $row->add_control('', 'hidden')->set_name('location')->set_value('on_page');
            }

            
            $form_action = $row->add_div()->add_class('form-inline pull-right')->custom_css('margin-top', '10px');
            $forget_password = $form_action->add_field()
                    ->add_action()
                    ->set_label(clang::__('Lupa kata sandi') . '?')
                    ->add_class('pull-right font-black btn-forgot-password')
                    ->custom_css('margin-right', '30px');

            if ($this->action_next) {
                $action = $form_action->add_field()
                        ->add_action()
                        ->set_label(clang::__('Lanjutkan'))
                        ->add_class('btn-next-' . $this->id . ' btn-62hallfamily bg-red font-white border-3-red pull-right upper font-size18')
                        ->custom_css('padding-right', '0')
                        ->set_submit(false);
            }
            else {
                $action = $form_action->add_field()
                        ->add_action()
                        ->set_label(clang::__('Masuk'))
                        ->add_class('btn btn-auth pull-right btn-flex')
                        ->set_submit(false);

                // $div_login_row = $row->add_div()->add_class("row div-login-socmed");
                // $div_login_row->add_div()->add_class("col-sm-3");
                // $div_login_socmed = $div_login_row->add_div()->add_class("col-sm-6");

                // $login_facebook = $div_login_socmed->add_action()
                //         ->set_label(clang::__("<div><img src='" . curl::httpbase() . "application/torsb2c/default/media/img/fb-logo.png'/></div><div>Login with Facebook</div>"))
                //         ->add_class("login-fb")
                //         ->set_submit(false);
            }


            $have_login_socmed = false;
            
            $login_fb = ccfg::get('login_facebook');
            $login_google = ccfg::get('login_google');
            
            if ($login_fb || $login_google) {
                $have_login_socmed = true;
            }
            
            $row_button_socmed = $form->add_div()->add_class('row btn-login-wrapper');
            if ($have_login_socmed) {
                // show separator line
                $line_socmed_wrapper = $row_button_socmed->add_div()->add_class('col-md-12 col-xs-12');
                $line_wrapper = $line_socmed_wrapper->add_div()->add_class('line-wrapper');
                $line_wrapper->add_div()->add_class('line')->add('&nbsp;');
                $line_wrapper->add_div()->add_class('txt-center line-label')
                        ->add_div()->add_class('line-text')->add(clang::__('Atau'));
                
                $socmed_container = $row_button_socmed->add_div()->add_class('col-md-12 col-xs-12 txt-center');
                if ($login_fb) {
                    // show fb button
                    $login_facebook = $socmed_container->add_action()
                        ->set_label('<i class="fa fa-facebook-square social-media"></i>'
                            .clang::__('Login with Facebook'))
                        ->add_class("login-fb")
                        ->set_submit(false);
                }

                if ($login_google) {
                    // show gmail buttom
                    $div_login_google = $socmed_container->add_div()->add_class("login-google");

                    $login_google = $div_login_google->add('<div
                          class="g-signin2"
                          data-cookiepolicy="single_host_origin"
                          data-onsuccess="onSignIn"
                          data-onfailure="onSignInFailure"
                          data-width="standart"
                          data-longtitle="true">
                        </div>');
                }
            }

            if ($this->reload == true) {
                $action->set_attr('on-reload', true);
                $row->add_control('', 'hidden')->set_name('redirect')->add_class('redirect redirect-for-login')
                        ->set_value($this->redirect);
            }

            $row->add_control('', 'hidden')->add_class('state-from')->set_name('state-from')->set_value('login');

            $row->add_br();

            if (!empty($this->title_header)) {
                $title_header = $form->add_div()->add_class('col-md-12')
                        ->add($this->title_header);
            }

            // form forgot password
            $forgot_password_form = $modal_content->add_div()->add_class('modal-body forgot_password_form clearfix hide');
            $form = $forgot_password_form->add_form()->add_class('form-62hallfamily');
            $form->set_action(curl::base() . 'member/forgotpassword');
            $form->set_ajax_submit(false);
            $form->set_validation(false);

            $row = $form->add_div()->add_class("col-md-12");

            $row_message = $row->add_div()->add_class('login-message forgot-password-message');

            $title = $row->add_field()
                    ->add_div()
                    ->custom_css('margin-left', '0px')
                    ->add(clang::__('Silahkan masukan alamat email Anda untuk mendapatkan password baru'));

            if (!empty($this->title_header)) {
                $title_header = $form->add_div()->add_class('col-md-12')
                        ->add($this->title_header);
            }

            $input_email = $row
                    ->add_field()
                    ->set_label(clang::__('Email'))
                    ->add_control("", 'text')
                    ->set_name("email")
                    ->add_class("form-control");

            $action = $row->add_field()
                    ->add_action()
                    ->set_label(clang::__('Reset Password'))
                    ->add_class('btn-reset-password btn-62hallfamily bg-red font-white margin-20 border-3-red pull-right upper')
                    ->custom_css('padding-right', '0')
                    ->set_submit(false);

            $row->add_br();

            // Success forgot password
            $success_forgot_password = $modal_content->add_div()->add_class('modal-body success_forgot_password clearfix hide');
            $row = $success_forgot_password->add_div()->add_class("col-md-12");

            $message = $row->add_field()
                    ->add('<center>
                        Sebuah email telah dikirim ke alamat email Anda,<br>
                        <div class="bold email-success"></div><br>
                        Email ini berisikan cara untuk mendapatkan password baru.<br><br>
                        Diharapkan menunggu beberapa saat, selama pengiriman<br>
                        email dalam proses. Mohon diperhatikan bahwa alamat email diatas<br>
                        adalah benar, dan periksalah folder junk dan spam atau filter<br>
                        jika tidak menerima email tersebut.
                        </center>');

            $html->appendln(parent::html($index));
            //$html->appendln($this->element_register->html());

            return $html->text();
        }

        public function js($index = 0) {
            $js = new CStringBuilder();

            $js->appendln("
                var form_auth = {
                    clear_message: function clear_auth_message() {
                        $('.form-auth .auth-message').html('');
                    },
                    show_message: function(form, error, message){
                        if (error == 0) {
                            form.find('.auth-message').html('<div class=\"success-message\">'+message+' <a href=\"#\" class=\"close close_message font-gray\" data-dismiss=\"success-message\" aria-label=\"close\">&times;</a></div>');
                        }
                        else {
                            form.find('.auth-message').html('<div class=\"error-message\">'+message+' <a href=\"#\" class=\"close close_message font-gray\" data-dismiss=\"success-message\" aria-label=\"close\">&times;</a></div>');
                        }
                    },
                    reset_form : function(form) {
                        form.find('input[type=\"text\"], input[type=\"password\"], textarea').val('');
                    }
                };
                
                jQuery.getScript( 'https://apis.google.com/js/platform.js', function() { });
                
                function submit_form(element){
                    form_auth.clear_message();
                    var data_msg_class = jQuery(element).attr('data-msg-class');
                    var form = jQuery(element).closest('form');
                    var div_msg = form.find('.' + data_msg_class);
                    var url = form.attr('action');
                    var state = 'login';
                    if (jQuery(element).hasClass('btn-62hallfamily-reg')) {
                        url = '" . curl::base() . "auth/register';
                        state = 'register';
                    }
                    if (jQuery(element).hasClass('registration')) {
                        url = '" . curl::base() . "auth/register';
                        state = 'register';
                    }
                    var on_reload = jQuery('.btn-auth').attr('on-reload');
                    $.ajax({
                        type: 'post',
                        dataType: 'json',
                        url: url,
                        data: form.serialize(),
                        beforeSend: function(){
//                            form_auth.show_message(form, '0', 'loading...');
                        },
                        success: function(result) {
                            form_auth.clear_message();
                            if (result.error == 0) {
                                // redirect
                                if (typeof on_reload === 'undefined') {
                                    // do nothing
                                    form_auth.show_message(form, result.error, result.message);
                                }
                                else {
                                    window.location = form.find('.redirect.redirect-for-' + state).attr('value');
                                }

                                form_auth.reset_form(form);
                            }
                            else {
                                form_auth.show_message(form, result.error, result.message);
                            }
                        },
                        error: function() {
                            form_auth.clear_message();
                        },
                    });
                }
                $(document).on('keypress','#password_login',function(event) {
                    if (event.which == 13) {
                        event.preventDefault();
                        submit_form(this);
                    }
                });
                
                
                

                jQuery('body').on('click', '.btn-auth', function(evt){
                    evt.preventDefault();
                    submit_form(this);
                });
                
                jQuery('.btn-next-" . $this->id . "').click(function(evt){
                    form_auth.clear_message();
                    var data_msg_class = jQuery(this).attr('data-msg-class');
                    var form = jQuery(this).parents('form');
                    var div_msg = form.find('.' + data_msg_class);
                    var url = form.attr('action');
                    var on_reload = jQuery(this).attr('on-reload');
                    
                    $.ajax({
                        type: 'post',
                        dataType: 'json',
                        url: url,
                        data: form.serialize(),
                        success: function(result) {
                            if (result.error == 0) {
                                // redirect
                                if (typeof on_reload === 'undefined') {
                                    // do nothing
                                    form_auth.show_message(form, result.error, result.message);
                                }
                                else {
                                    window.location = '" . curl::base() . "products/updatecontact';
                                }

                                form_auth.reset_form(form);
                            }
                            else {
                                form_auth.show_message(form, result.error, result.message);
                            }
                        },
                        error: function() {
                            form_auth.clear_message();
                        }
                    });
                });

                var form_forgot_password = {
                    clear_message: function clear_forgot_password_message() {
                        $('.forgot-password-message').html('');
                    },
                    show_message: function(form, error, message){
                        if (error == 0) {
                            form.find('.forgot-password-message').html('<div class=\"success-message\">'+message+' <a href=\"#\" class=\"close font-gray\" data-dismiss=\"success-message\" aria-label=\"close\">&times;</a></div>');
                        }
                        else {
                            form.find('.forgot-password-message').html('<div class=\"error-message\">'+message+' <a href=\"#\" class=\"close font-gray\" data-dismiss=\"error-message\" aria-label=\"close\">&times;</a></div>');
                        }
                    },
                    reset_form : function(form) {
                        form.find('input[type=\"text\"], input[type=\"password\"], textarea').val('');
                    }
                };
                
                jQuery(document).on('click', '.error-message .close', function(e) {
                    jQuery(this).parent().hide();
                });

                jQuery('body').on('click', '.btn-forgot-password', function(evt){
                    $('.modal-title').text('Lupa Password');
                    $('.login_form').hide();
                    $('.forgot_password_form').removeClass('hide');
                    $('.forgot_password_form').show();
                 });


                 jQuery('body').on('click', '.btn-reset-password', function(evt){
                     form_forgot_password.clear_message();
                     var form = jQuery(this).parents('form');
                     var url = form.attr('action');
                     var data = form.serialize();

                     $.ajax({
                         type: \"post\",
                         dataType: 'json',
                         url: url,
                         data: data,
                         success: function(result) {
                             if (result.error == 0) {
                                 $('.modal-title').text('Password Anda akan direset');
                                 $('.forgot_password_form').hide();
                                 $('.success_forgot_password').removeClass('hide');
                                 $('.email-success').html(result.email);

                                 form_forgot_password.reset_form(form);
                             }
                             else {
                                 form_forgot_password.show_message(form, result.error, result.message);
                             }
                         },
                         error: function() {
                             form_forgot_password.clear_message();
                         }
                     });
                 });

                 
				 jQuery('body').on('click', '.btn-login', function(evt){
                     $('.modal-title').html('LOGIN');
                     $('.forgot_password_form').addClass('hide');
                     $('.success_forgot_password').addClass('hide');
                 });
                 
            ");
            if ($this->element_register) {
                $js->appendln($this->element_register->js());
            }
            if ($this->element_register_login) {
                $js->appendln($this->element_register_login->js());
            }
            $js->appendln(parent::js($index));
            return $js->text();
        }

    }
    