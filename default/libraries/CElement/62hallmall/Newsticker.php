<?php

/**
 * Description of Newsticker
 *
 * @author Ecko Santoso
 * @since 04 Okt 16
 */
class CElement_62hallmall_Newsticker extends CElement {

    protected $list_data = array();
    protected $element_title;
    protected $item_display;
    protected $page = '';

    public function __construct($id = '') {
        parent::__construct($id);
        $this->item_display = 1;
    }

    public static function factory($id = '') {
        return new CElement_62hallmall_Newsticker($id);
    }

    public function set_title($param) {
        $this->element_title = $param;
        return $this;
    }

    public function set_list_data($list) {
        $this->list_data = $list;
        return $this;
    }

    public function set_item_display($int) {
        $this->item_display = $int;
        return $this;
    }

    public function set_page($val) {
        $this->page = $val;
        return $this;
    }

    private function get_image($image_path = '') {
        $retcode = 200;
        if ($retcode == 500 or $image_path == NULL) {
            $image_path = curl::base() . 'application/62hallfamily/default/media/img/product/no-image.png';
        } else {
            $image_path = image::get_image_url($image_path, 'view_all');
        }
        return $image_path;
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);

        $slide_header = $this->add_div()->add_class('newsticker-header');
        if (strlen($this->element_title) == 0) {
            $slide_header->add_class('hide');
        }
        if (strlen($this->element_title) > 0) {
            $slide_header->add('<h3>');
            $slide_header->add($this->element_title . '</h3>');
        }

        $slide_wrapper = $this->add_div()->add_class('newsticker-wrapper');
        $slide_element = $slide_wrapper->add_div($this->id . '-carousel');
        if (count($this->list_data) > 0) {
            foreach ($this->list_data as $data_k => $data_v) {
                $cms_terms_id = carr::get($data_v, 'cms_terms_id');
                $term_name = carr::get($data_v, 'term_name');
                $term_slug = carr::get($data_v, 'term_slug');
                $cms_post_id = carr::get($data_v, 'cms_post_id');
                $post_content = carr::get($data_v, 'post_content');
                $post_title = carr::get($data_v, 'post_title');
                $post_name = carr::get($data_v, 'post_name');
                $post_permalink = carr::get($data_v, 'post_permalink');
                $post_permalink = curl::httpbase() . $post_permalink;
                // BUILD SNIGLE DATA
                $item = $slide_element->add_div()->add_class('item');
                $newsticker_div = $item->add_div()->add_class('newsticker')->add_attr("itemscope itemtype", "http://schema.org/NewsArticle");
                $newsticker_div->add('<p class="p-newsticker"><span class="text-danger">UPDATE : </span><a href="' . $post_permalink . '">' . $post_title . '</a></p>');
            }
        }

        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $js->appendln("
            $('#" . $this->id . "-carousel').owlCarousel({
                autoPlay: true,");
        $js->appendln("items : " . $this->item_display . ",");

        $js->appendln("
                itemsDesktop : [1199,5],
                itemsDesktopSmall : [979,3],
                navigation : false,
                navigationText : ['<','>'],
                pagination: false,
                theme : 'newsticker-carousel',
                slideSpeed: 500,
                addClassActive: true,
//                transitionStyle: 'slideUp'
            });
            ");

        $js->append(parent::js($indent));
        return $js->text();
    }

}
