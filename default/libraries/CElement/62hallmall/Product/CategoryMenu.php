<?php

class CElement_62hallmall_Product_CategoryMenu extends CElement{
    
    private $list_menu = array();
    private $head_menu = NULL;
    private $head_key = NULL;
    private $key = NULL;
    private $url_menu = NULL;

    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public static function factory($id = ''){
        return new CElement_Product_CategoryMenu($id);
    }
    
    public function set_key($key){
        $this->key = $key;
        return $this;
    }
    
    public function set_head_key($head_key){
        $this->head_key = $head_key;
        return $this;
    }
    
    public function set_head_menu($head_menu){
        $this->head_menu = $head_menu;
        return $this;
    }
    
    public function set_list_menu($list_menu){
        $this->list_menu = $list_menu;
        return $this;
    }
    
    public function set_url_menu($url_menu){
        $this->url_menu = $url_menu;
        return $this;
    }
    
    private function cek_subnav($subnav){
        $result = FALSE;
        foreach ($subnav as $sub){
            if($sub['category_id'] == $this->key){
                $result = TRUE;
            }
        }
        
        return $result;
    }
    
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        
        $category_menu = $this->add_div()->add_class('category-menu margin-bottom-20 margin-top-20');
        
        $url_menu = $this->url_menu;
        if (empty($url_menu)){
            $url_menu = curl::base() . 'product/category/';
        }
        
        $header = $category_menu->add_div()->add_class('header upper bold font-size20')->add('<a class="font-red " href="' . $url_menu . $this->head_key . '">' . $this->head_menu . '</a>');
        $menu = $category_menu->add_div();
        
        foreach ($this->list_menu as $data_menu){
            $menu_category = $menu->add_div()->add_class('menu-category');
            $active = NULL;
            $cek_subnav = $this->cek_subnav($data_menu['subnav']);
            
            $icon = '<i class="fa fa-caret-right"></i>';
            if(count($data_menu['subnav']) > 0 and ($this->key == $data_menu['category_id'] or $cek_subnav)){
                $icon = '<i class="fa fa-caret-down"></i>';
            }
            
            $active = ($data_menu['category_id'] == $this->key or $cek_subnav) ? 'active' : NULL;
            
            $head_menu_category = $menu_category->add_div()->add_class('head-menu-category bold font-gray-dark ')->add('<a href="' . $url_menu . $data_menu['category_id'] .  '" class="' . $active . '">' . $data_menu['name'] . ' ' . $icon . '</a>');
            $submenu_category_container = $head_menu_category->add_div()->add_class('submenu-category normal');
            
            if(count($data_menu['subnav']) > 0 and ($this->key == $data_menu['category_id'] or $cek_subnav)){
                foreach ($data_menu['subnav'] as $submenu){
                    
                    $active = ($submenu['category_id'] == $this->key) ? 'active' : NULL;
                
                    $submenu_category = $submenu_category_container->add_div()->add('<a href="' . $url_menu . $submenu['category_id'] .  '" class="' . $active . '">' . $submenu['name'] . '</a>');
                }
            }
        }
        
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent=0) {
        $js = new CStringBuilder();
        
        $js->append(parent::js($indent));
        return $js->text();
    }
}
