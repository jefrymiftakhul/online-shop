<?php

class CElement_62hallmall_Product_Slide extends CElement{
    
    protected $page='';
    protected $count_item = 6;
    protected $list_product = array();
    protected $count_slide = 1;
    
    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public static function factory($id = ''){
        return new CElement_62hallmall_Product_Slide($id);
    }
    
    public function set_page($val){
        $this->page=$val;
        return $this;
        
    }
    public function set_count_item($count) {
        $this->count_item = $count;
        return $this;
    }
    
    public function set_list_item($list){
        $this->list_product = $list;
        return $this;
    }
    
    private function get_count_slide($count){
        $count_slide = $count / $this->count_item;
        if($count % $this->count_item != 0){
            $count_slide += 1;
        }
        
        $this->count_slide = $count_slide;
        return (int) $this->count_slide;
    }
    
    private function get_loop_item($slide){
        $start = ($slide - 1) * $this->count_item;
        $end = ($this->count_item * $slide) - 1;
        if($end > (count($this->list_product) - 1)){
            $end = count($this->list_product) - 1;
        }
        return array('start' => $start, 'end' => $end);
    }
    
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        
        $html->appendln('<div id="' . $this->id . '" class="carousel slide product-slide" data-ride="carousel">');
        $html->appendln('<ol class="carousel-indicators">');
        
        $count_list_product = count($this->list_product);
        $this->count_slide = $this->get_count_slide($count_list_product);
        
        for ($no = 0; $no < $this->count_slide; $no++){
            $html->appendln('<li data-target="#' . $this->id . '" data-slide-to="' . $no . '" class="active"></li>');
        }
        $html->appendln('</ol>');
        // $html->appendln('<div class="container" style="padding-left:0px;padding-right:28px;">');
        $html->appendln('<div>');
        $html->appendln('<div class="row">');
        
        $html->appendln('<div class="carousel-inner" role="listbox">');
       
        for ($slide = 1; $slide <= $this->count_slide; $slide++){
            $active = ($slide == 1) ? 'active' : '';
            $html->appendln('<div class="item ' . $active . '">');
            
            $loop_item = $this->get_loop_item($slide);
            
            for ($item = $loop_item['start']; $item <= $loop_item['end']; $item++){
                $product_id = !empty($this->list_product[$item]['product_id']) ? $this->list_product[$item]['product_id'] : NULL;
                $url_key = !empty($this->list_product[$item]['url_key']) ? $this->list_product[$item]['url_key'] : NULL;
                $image_url = !empty($this->list_product[$item]['image_path']) ? $this->list_product[$item]['image_path'] : NULL;
                $name = !empty($this->list_product[$item]['name']) ? $this->list_product[$item]['name'] : NULL;
                $flag = $hot = ($this->list_product[$item]['is_hot_item'] > 0) ? 'Hot' : NULL;
            
                if(!empty($this->list_product[$item]['new_product_date'])){
                    if(strtotime($this->list_product[$item]['new_product_date']) >= strtotime(date('Y-m-d'))){
                        $flag = $new = 'New';
                    }
                }   
                
                $promo = !empty($this->list_product[$item]['promo']) ? $this->list_product[$item]['promo'] : NULL;
                $stock = !empty($this->list_product[$item]['stock']) ? $this->list_product[$item]['stock'] : 0;
                $min_stock = !empty($this->list_product[$item]['show_min_stock']) ? $this->list_product[$item]['show_min_stock'] : 0;
                $price = !empty($this->list_product[$item]['detail_price']) ? $this->list_product[$item]['detail_price'] : array();
                $is_available = $this->list_product[$item]['is_available'];
                $page=$this->page.'/';
                if($this->page=='product'){
                    $page='';
                }
                $location_detail=curl::httpbase() . $page.'product/item/';
                $element_product = CElement_62hallmall_Product_Product::factory();
                $element_product->set_key($url_key)
                        ->set_image($image_url)
                        ->set_column($this->count_item)
                        ->set_name($name)
                        ->set_flag($flag)
                        ->set_label_flag($flag)
                        ->set_price($price)
                        ->set_stock($stock)
                        ->set_min_stock($min_stock)
                        ->set_location_detail($location_detail)
                        ->set_available($is_available);
                
                $element_html = $element_product->html();
                
                $html->appendln($element_html);
            }
            $html->appendln('</div>');
        }
        
       
        $html->appendln('</div>');
        
        if(count($this->list_product) > $this->count_item){
            $html->appendln('
                            <a class=" left carousel-control" href="#'. $this->id .'" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="right carousel-control" href="#'. $this->id .'" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                          </a>');

        }
        
        $html->appendln('</div></div></div>');
        $html->appendln('</br>');
        
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent=0) {
        $js = new CStringBuilder();

        $js->append(parent::js($indent));
        return $js->text();
    }
}
