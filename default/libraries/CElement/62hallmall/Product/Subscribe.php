<?php

class CElement_62hallmall_Product_Subscribe extends CElement{
    
    private $type = 1;

    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public function set_type($type){
        $this->type = $type;
        return $this;
    }

    public static function factory($id = ''){
        return new CElement_62hallmall_Product_Subscribe($id);
    }
    
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        
        //$form = $this->add_form()->add_class('form-62hallfamily');
        $container = $this->add_div()->add_class('newsletter-form');
        $container->add_control('type','hidden')->set_value($this->type);
        $message = $container->add_div('message-subscribe');
        
        $input_subscribe = $container->add_div()->add_class('form-group');
        $input_subscribe->add_control("email_subscribe", 'text')
            ->set_name("email_subscribe")
            ->set_placeholder('Masukkan email Anda')
            ->add_class("form-control");
        
        $action_subscribe = $container->add_div()->add_class('btn-area');
        $action_subscribe->add_action()
                    ->set_label(clang::__('Register'))
                    ->add_class('btn-register-subscribe btn-success btn-block')
                    ->add_listener('click')
                    ->add_handler('reload')
                    ->add_param_input(array('email_subscribe','type'))
                    ->set_target('message-subscribe')
//                    ->set_url(curl::base() . 'subscribe/user_subscribe/?gender=male');
                    ->set_url(curl::base() . 'subscribe/user_subscribe');
        
//        $action_subscribe->add_action()
//                    ->set_label(clang::__('Wanita'))
//                    ->add_class('btn-register-subscribe btn-danger pull-right')
//                    ->add_listener('click')
//                    ->add_handler('reload')
//                    ->add_param_input(array('email_subscribe','type'))
//                    ->set_target('message-subscribe')
//                    ->set_url(curl::base() . 'subscribe/user_subscribe/?gender=female');
        
        
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent=0) {
        $js = new CStringBuilder();
        
        $js->appendln("
                $('input').blur();
                $('.btn-register-subscribe').click(function(){
                   $('#email_subscribe').focus(); 
                });
                ");
        
        $js->append(parent::js($indent));
        return $js->text();
    }
}
