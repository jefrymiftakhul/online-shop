<?php

class CElement_62hallmall_Product_Detail extends CElement {

    private $key = NULL;
    private $detail_list = array();
    private $price = 0;
    private $stock = 0;
    private $minimum_stock = 10;
    private $attribute_list = array();
    private $with_qty = TRUE;
    private $type_select = 'select';
    private $type_image = 'image';
    private $total_param_input = array();
    private $lock_max_qty = false;

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_62hallmall_Product_Detail($id);
    }

    public function set_key($key) {
        $this->key = $key;
        return $this;
    }

    public function set_minimum_stock($min_stock) {
        $this->minimum_stock = $min_stock;
        return $this;
    }

    public function set_detail_list($detail_list) {
        $this->detail_list = $detail_list;
        return $this;
    }

    public function with_qty($with_qty) {
        $this->with_qty = $with_qty;
        return $this;
    }

    public function set_attribute_list($attribute_list) {
        $this->attribute_list = $attribute_list;
        return $this;
    }

    private function get_price() {
        $detail_price = $this->price;

        $arr_price = product::get_price($detail_price);

        return $arr_price;
    }

    private function generate_list($type, $data = array()) {
        $list = array();

        foreach ($data as $key => $row_data) {
            if ($type == $this->type_select) {
                $value = carr::get($row_data, 'attribute_key');
            }
            if ($type == $this->type_image) {
                $value['key'] = carr::get($row_data, 'attribute_key');
                $value['value'] = carr::get($row_data, 'file_path');
            }

            $list[$key] = $value;
        }

        return $list;
    }

    private function generate_form($container, $code, $data) {
        $id = carr::get($data, 'attribute_category_id');
        $type = carr::get($data, 'attribute_category_type');
        $prev = carr::get($data, 'prev_attribute_category');
        $next = carr::get($data, 'next_attribute_category');
        $attribute = carr::get($data, 'attribute', array());
        $list = $this->generate_list($type, $attribute);

        $control = array();
        foreach ($prev as $row_prev) {
            $arr_param['att_cat_' . $id] = 'att_cat_' . $id;
            if (carr::get($this->total_param_input, 'att_cat_' . $id) == null) {
                $this->total_param_input['att_cat_' . $id] = 'att_cat_' . $id;
            }
        }
        if ($type == $this->type_select) {
            $div_container = $container->add_div('container-' . $code);
            $select = $div_container->add_field()
                    ->add_control('att_cat_' . $id, 'select')
                    ->add_class('select-62hallfamily margin-bottom-10 att_cat_' . $id)
                    ->custom_css('margin-left', '15px')
                    ->custom_css('width', '130px')
                    ->set_list($list);
            $control[] = $select;
        } else if ($type == $this->type_image) {
            $div_container = $container->add_div('container-' . $code)
                    ->add_class('container-input-image');
            $index = 1;
            foreach ($list as $key => $value) {
                $active = NULL;
                if ($index == 1) {
                    $active = 'active';
                    $div_container->add_control('att_cat_' . $id, 'hidden')
                            ->set_value($key);
                }
                $image = $div_container->add_div($key)
                        ->add_class('btn-colors margin-bottom-10 link  att_cat_' . $id . ' ' . $active)
                        ->custom_css('display', 'inline-block')
                        ->custom_css('padding', '2px')
                        ->add('<img src="' . $value['value'] . '" style="width:20px; height:20px"/>');


                $control[] = $image;

                $index++;
            }
        }

        if (count($control) > 0) {
            foreach ($control as $obj) {
                if (!empty($next) and count($list) > 0) {
                    $listener = $obj->add_listener('ready');

                    $handler = $listener->add_handler('reload')
                            ->set_target('container-' . $next)
                            ->add_param_input($arr_param)
                            ->set_url(curl::base() . 'product/generate_attribute/' . $next . '/' . $this->key);



                    if ($type == $this->type_image) {
                        $listener = $obj->add_listener('click');

                        $handler = $listener->add_handler('reload')
                                ->set_target('container-' . $next)
                                ->add_param_input($arr_param)
                                ->set_url(curl::base() . 'product/generate_attribute/' . $next . '/' . $this->key);
                    }
                    if ($type == $this->type_select) {

                        $listener = $obj->add_listener('change');

                        $handler = $listener->add_handler('reload')
                                ->set_target('container-' . $next)
                                ->add_param_input($arr_param)
                                ->set_url(curl::base() . 'product/generate_attribute/' . $next . '/' . $this->key);
                    }
                } else if (empty($next) and count($list) > 0) {
                    if ($type == $this->type_select) {
                        $listener = $obj->add_listener('ready');

                        $handler = $listener->add_handler('reload')
                                ->set_target('stock-price')
                                ->add_param_input($arr_param)
                                ->set_url(curl::base() . 'reload/reload_stock_price_product/' . $code . '/' . $this->key);
                     
                        $listener = $obj->add_listener('change');

                        $handler = $listener->add_handler('reload')
                                ->set_target('stock-price')
                                ->add_param_input($arr_param)
                                ->set_url(curl::base() . 'reload/reload_stock_price_product/' . $code . '/' . $this->key);
                    }
                    if ($type == $this->type_image) {
                        $listener = $obj->add_listener('ready');

                        $handler = $listener->add_handler('reload')
                                ->set_target('stock-price')
                                ->add_param_input($arr_param)
                                ->set_url(curl::base() . 'reload/reload_stock_price_product/' . $code . '/' . $this->key);

                        foreach ($arr_param as $inp) {
                            if (strlen($data_addition) > 0)
                                $data_addition.=',';
                            $data_addition.="'" . $inp . "':$.cresenity.value('#" . $inp . "')";
                        }

                        $data_addition = '{' . $data_addition . '}';

                        $listener = $obj->add_listener('click');

                        $handler = $listener->add_handler('custom')
                                ->set_js("
                                        var key=$(this).attr('id');
                                        $('#" . 'att_cat_' . $id . "').val(key);

                                        $.cresenity.reload('stock-price','" . curl::base() . "reload/reload_stock_price_product/" . $code . "/" . $this->key . "','get'," . $data_addition . ");
                                        ");
                    }
                }
            }
        }

        return $control;
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
       
        $product_id = carr::get($this->detail_list, 'product_id', NULL);
        $name = carr::get($this->detail_list, 'name', NULL);
        $sku = carr::get($this->detail_list, 'sku', NULL);
        $availability = carr::get($this->detail_list, 'is_available', NULL);
        $quick_overview = carr::get($this->detail_list, 'quick_overview', NULL);
        $stock = carr::get($this->detail_list, 'stock', 0);
        $weight = carr::get($this->detail_list, 'weight', 0);
        $show_minimum_stock = carr::get($this->detail_list, 'show_minimum_stock', 0);
        $this->stock = $stock;
        $this->price = carr::get($this->detail_list, 'detail_price', 0);
        $detail_price = carr::get($this->detail_list, 'detail_price', 0);
        $variations = carr::get($this->detail_list, 'variations', array());
        $variation_label = carr::get($this->detail_list, 'variation_label', array());
        
        if (isset($_GET['debug'])) {
            cdbg::var_dump($this->detail_list);
        }
        
        $vendor_price = carr::get($detail_price, 'vendor_price');
        
        if ($vendor_price == '1') {
            $sku = $product_id;
            $this->lock_max_qty = true;
        }

        $array_size = carr::get($this->detail_list, 'size');
        $array_color = carr::get($this->detail_list, 'color');
        
        // Build UI
        $detail_product = $this->add_div()->add_class('detail-product');
        $form = $detail_product->add_form('form-detail-product')->set_action(curl::base() . 'request/add/');
            $product_summary_div = $form->add_div()->add_class('product-summary');
            $product_stock_div = $form->add_div()->add_class('product-stock');
            $product_action_div = $form->add_div()->add_class('product-action');
            
        // Product Summary
            $product_name_div = $product_summary_div->add_div()->add_class('product-name');
                $product_name_div->add('<h3 class="title">' . htmlspecialchars($name) . '</h3>');
                
                $product_name_div->add('SKU : '. $sku);
            $product_price_div = $product_summary_div->add_div()->add_class('product-price');
                $item_price_normal = $product_price_div->add_div()->add_class('normal-price');
                $item_price_promo = $product_price_div->add_div()->add_class('promo-price');
                
            // form variation
            if (count($variations) > 0) {
//                $product_variation_div = $product_summary_div->add_div()->add_class('product-variation');
//                $product_div_variation = $product_summary_div->add_div('product-variation')->add('VARIASI<br><br>');
                $product_div_variation = $product_summary_div->add_div()->add_class('product-variation')->add('<h4 class="title">PILIH '.$variation_label.'</h4><br>');
                $product_div_variation_input = $product_div_variation->add_div();
                $ul = $product_div_variation_input->add_ul()->add_class('list-unstyled varian-list');
                foreach ($variations as $variations_k => $variations_v) {
                    $no = carr::get($variations_v, 'no');
                    $desc = carr::get($variations_v, 'desc');
                    $add_price = carr::get($variations_v, 'add_price');
                    $var_stock = carr::get($variations_v, 'stock');
                    $add_price_total = carr::get($variations_v, 'add_price_total');

                    $var_label = $desc;
                    if ($add_price > 0) {
                        $var_label = $desc.' + IDR '.$add_price;
                    }
                    $li = $ul->add_li();
                    $li->add_control('', 'radio')->set_label($var_label)->set_name('variation')->set_value($no);
                }
            }    
            
            
            if ($availability > 0) {
                if ($vendor_price == '1') {
                    $price = $detail_price;
                }
                else {
                    $price = $this->get_price();
                }
                if ($price['promo_price'] > 0) {
                    $item_price_normal->add('<strike>IDR '.ctransform::format_currency($price['price']).'</strike>');
                        $item_price_normal->add('<span class="promo-value"> Discount '.carr::get($detail_price,'promo_text', 0).'%</span>');
                    $item_price_promo->add('IDR '.ctransform::format_currency($price['promo_price']));
                } else {
                    $item_price_promo->add('IDR '.ctransform::format_currency($price['price']));
                }
                        
                $stock_info = 'STOCK <span class="stock">'.$stock.' ITEM</span>';
                if ($stock >= 1000) {
                    $stock_info = '';
                }
                if ($show_minimum_stock > 0) {
                    if ($stock > 0 && $stock <= $show_minimum_stock) {
                        $stock_info = 'TINGGAL <span class="stock">SISA '.$stock.' ITEM</span>';
                    }
                }
                $product_stock_div->add($stock_info);
            }
            else {
                $item_price_promo->add('Kisaran Harga<br>IDR. ' . ctransform::format_currency($detail_price['sell_price_start']) . ' - IDR. ' . ctransform::format_currency($detail_price['sell_price_end']) );
            }

        $form->add_control('product_id', 'hidden')->set_value($product_id);
        $form->add_control('page', 'hidden')->set_value('product');
        $rowform = $form->add_div()->add_class('row');

        // form attribute
        $form_atribute = $rowform->add_div()->add_class('col-md-12');
        foreach ($this->attribute_list as $code_attribute => $attribut_list) {
            $form_atribute->add_div()->add_class('bold font-size16 margin-bottom-10')->add($attribut_list['attribute_category_name']);
            $this->generate_form($form_atribute, $code_attribute, $attribut_list);
        }

        $leftform = $rowform->add_div()->add_class(' col-xs-6 col-sm-3 margin-top-10');
        $rigthform = $rowform->add_div('add_to_cart_container')->add_class('col-xs-6 col-sm-9 margin-top-10');

        if ($this->with_qty) {
            $leftform->add_div('div_qty')->add('JUMLAH<br><br>')->add("<input type='text' id='qty' name='qty' value='1' style='z-index:0'>");
        }

//        $rigthform->add('BERAT<br><br>'.$weight);
        
        // form variation
//        if (count($variations) > 0) {
//            $product_div_variation = $rigthform->add_div('product-variation')->add('VARIASI<br><br>');
//            $product_div_variation_input = $product_div_variation->add_div();
//            foreach ($variations as $variations_k => $variations_v) {
//                $no = carr::get($variations_v, 'no');
//                $desc = carr::get($variations_v, 'desc');
//                $add_price = carr::get($variations_v, 'add_price');
//                $var_stock = carr::get($variations_v, 'stock');
//                $add_price_total = carr::get($variations_v, 'add_price_total');
//
//                $var_label = $desc;
//                if ($add_price > 0) {
//                    $var_label = $desc.' + IDR '.$add_price;
//                }
//
//                $product_div_variation_input->add_control('', 'radio')->set_label($var_label)->set_name('variation')->set_value($no);
//            }
//        }    
        
        if ($availability > 0) {
            
            if (($stock*1) > 0) {
                if ($vendor_price == '1') {
                    $form->set_action(curl::base() . 'products/updatecontact/update_contact?api=kinerjapay');
                    $action = $form->add_action()
                        ->set_label(clang::__('Beli Sekarang'))
                        ->add_class('btn-green')
                        ->custom_css('height', '34px')
                        ->custom_css('margin-top', '30px')
                        ->set_submit_to(curl::base() . 'products/updatecontact/update_contact?api=kinerjapay')
                        ->set_submit(true);
                }
                else {
                    $action = $form->add_action()
                            ->set_label(clang::__('Beli Sekarang'))
                            ->add_class('btn-add-cart btn-green')
                            ->custom_css('margin-top', '30px')
                            ->add_listener('click');

                    $cart = $action->add_handler('reload');

                    foreach ($this->attribute_list as $key => $attribut_list) {
                        $cart->add_param_input('att_cat_' . carr::get($attribut_list, 'attribute_category_id'));
                    }

                    $cart->add_param_input('qty')
                            ->set_target('shopping-cart') // shopping cart
                            ->set_url(curl::base() . 'products/shoppingcart/add_item_cart/' . $product_id); // proses shopping cart
                }
            }
//            else {
//                $form->add('<button type="button" class="btn-add-cart btn-green" style="margin-top:30px;">Tidak bisa</button>');
//            }

            /*
              $dialog = $action->add_handler('dialog')
              ->set_title('<center>Tambah Belanjaan</center>')
              ->set_url(curl::base() . 'products/shoppingcart/dialog_add_cart');
             */
        } else {
            $this->stock = 100;
            $action = $form->add_action()
                    ->set_label('Request')
                    ->add_class('btn-green')
                    ->custom_css('height', '34px')
                    ->set_submit_to(curl::base() . 'request/add/')
                    ->set_submit(true);
        }
//        $html->appendln('</div>');

        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();
        $max = $this->stock;
        if ($max < 1)
            $max = 1;
        if ($this->lock_max_qty == true) {
            $max = 1;
        }
        $js->appendln("
            $('.btn-colors').click(function(){
                id = $(this).attr('id');
                $('#colors').val(id);
                $('.btn-colors').removeClass('active');
                $(this).addClass('active');
            });  
            
            $('#qty').TouchSpin({
                min: 1,
                max: " . $max . ",
                verticalbuttons: false
            });
		");

        $js->append(parent::js($indent));
        return $js->text();
    }

}
