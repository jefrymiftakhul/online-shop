<?php

class CElement_62hallmall_Product_Overview extends CElement{
    
    private $key = NULL;

    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public static function factory($id = ''){
        return new CElement_62hallmall_Product_Overview($id);
    }
    
    public function set_key($key){
        $this->key = $key;
        return $this;
    }
    
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        
        $tabs = $this->add_tab_list('tabs-horizontal')->add_class('tabs-horizontal tabs-overview-product');
        
        $tab_overview = $tabs->add_tab('overview')
                ->set_label('<b>' . clang::__("DESKRIPSI PRODUK") . '</b>')
                ->set_ajax_url(curl::base() . "product/overview/" . $this->key);
        
        $tab_spesifikasi = $tabs->add_tab('spesifikasi')
                ->set_label('<b>' . clang::__("SPESIFIKASI") . '</b>')
                ->set_ajax_url(curl::base() . "product/spesifikasi/" . $this->key);
        
        $tab_reviews = $tabs->add_tab('reviews')
                ->set_label('<b>' . clang::__("ULASAN") . '</b>')
                ->set_ajax_url(curl::base() . "product/reviews/" . $this->key);
        
        $tab_faq = $tabs->add_tab('faq')
                ->set_label('<b>' . clang::__("FAQ") . '</b>')
                ->set_ajax_url(curl::base() . "product/faq/" . $this->key);
         
//        $tab_spesifikasi = $tabs->add_tab('info_seller')
//                ->set_label('<b>' . clang::__("INFO SELLER") . '</b>')
//                ->set_ajax_url(curl::base() . "product/info_seller/" . $this->key);
        
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent=0) {
       $js = new CStringBuilder();

        $js->append(parent::js($indent));
        return $js->text();
    }
}
