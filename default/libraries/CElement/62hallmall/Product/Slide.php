<?php

class CElement_62hallmall_Product_Slide extends CElement{
    
    protected $list_product = array();
    protected $element_title;
    protected $enable_button;
    protected $title_icon;
    protected $item_display;
    private $location_detail = NULL;
    protected $page='';

    public function __construct($id = '') {
        parent::__construct($id);
        $this->enable_button = true;
        $this->title_icon = '';
        $this->item_display = 5;
    }
    
    public static function factory($id = ''){
        return new CElement_62hallmall_Product_Slide($id);
    }
    
    public function set_title($param) {
        $this->element_title = $param;
        return $this;
    }
    
    public function set_list_item($list){
        $this->list_product = $list;
        return $this;
    }
    
    public function set_enable_button($bool) {
        $this->enable_button = $bool;
        return $this;
    }
    
    public function set_title_icon($title_icon) {
        $this->title_icon = $title_icon;
        return $this;
    }
    
    public function set_item_display($int) {
        $this->item_display = $int;
        return $this;
    }
    
    public function set_location_detail($location_detail) {
        $this->location_detail = $location_detail;
        return $this;
    }
    
    public function set_page($val){
        $this->page = $val;
        return $this;
    }
    
//    public function set_count_item(){
//        
//    }
        
    private function get_image($image_path = '') {
        $retcode=200;
        if ($retcode == 500 or $image_path == NULL) {
            $image_path = curl::base() . 'application/62hallfamily/default/media/img/product/no-image.png';
        } else {
            $image_path = image::get_image_url($image_path, 'view_all');
        }
        return $image_path;
    }
    
    private function get_price($detail_price) {
        $promo_price = 0;
        $price = 0;

        if (!empty($detail_price['ho_sell_price']) and ! empty($detail_price['channel_sell_price'])) {
            if ($detail_price['ho_sell_price'] > $detail_price['channel_sell_price']) {
                $price = $detail_price['ho_sell_price'];
                $promo_price = $detail_price['channel_sell_price'];
            } else {
                $price = $detail_price['channel_sell_price'];
            }
        }

        return array('promo_price' => $promo_price, 'price' => $price);
    }
    
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        
        $slide_header = $this->add_div()->add_class('slide-header');
        if (strlen($this->element_title) == 0) {
            $slide_header->add_class('hide');
        }
        if (strlen($this->element_title) > 0) {
            $slide_header->add('<h3>');
            if (strlen($this->title_icon) > 0) {
                $slide_header->add('<i class="carousel-title-icon '.$this->title_icon.'">'.$this->title_icon.'</i>');
            }
            $slide_header->add($this->element_title.'</h3>');
        }
        
        $slide_wrapper = $this->add_div()->add_class('slide-wrapper');
            $slide_element = $slide_wrapper->add_div($this->id.'-carousel');
            if (count($this->list_product) > 0) {
                foreach ($this->list_product as $product_k => $product_v) {
                    $product_id = carr::get($product_v, 'product_id');
                    $url_key = carr::get($product_v, 'url_key');
                    $code = carr::get($product_v, 'code');
                    $name = carr::get($product_v, 'name');
                    $sku = carr::get($product_v, 'sku');
                    $weight = carr::get($product_v, 'weight');
                    $image_path = carr::get($product_v, 'image_path');
                    $image_src = $this->get_image($image_path);
                    if($image_path == NULL) {
//                        $image_src = carr::get($product_v, 'image_url');
                        $image_src = carr::get($product_v, 'image_url_medium');
                    }
                    
                    $is_hot_item = carr::get($product_v, 'is_hot_item');
                    $new_product_date = carr::get($product_v, 'new_product_date');
                    $is_available = carr::get($product_v, 'is_available');  // is_available > 0 tampilkan harga, else tampilkan "By Request"
                    $quick_overview = carr::get($product_v, 'quick_overview');
                    $stock = carr::get($product_v, 'stock');
                    $show_minimum_stock = carr::get($product_v, 'show_minimum_stock');
                    $product_data_type = carr::get($product_v, 'product_data_type');
                    $detail_price = carr::get($product_v, 'detail_price');
                    $promo_start_date=carr::get($detail_price,'promo_start_date');
                    $promo_end_date=carr::get($detail_price,'promo_end_date');
                    $promo_text = carr::get($detail_price, 'promo_text');
                    
                    $price_all = $this->get_price($detail_price);
                    
                    $vendor_price = carr::get($detail_price, 'vendor_price');
                    if ($vendor_price == '1') {
                        $price_all = $detail_price;
                    }
                    
                    $price_promo = carr::get($price_all, 'promo_price');
                    $price_normal = carr::get($price_all, 'price');
                    
                    $price = 'IDR '.ctransform::format_currency($price_normal);
                    $price_strike = '';
                    if ($price_promo > 0) {
                        $price = 'IDR '.ctransform::format_currency($price_promo);
                        $price_strike = 'IDR '.ctransform::format_currency($price_normal);
                    }
                    $page = $this->page.'/';
                    if($this->page == 'product'){
                        $page = '';
                    }
                    if ($vendor_price == '1') {
                        $page ='kinerja'.'/';
                        $url_key = $product_id;
                    }
                    $location_detail = curl::httpbase() . $page.'product/item/';
                    
                    // BUILD SNIGLE PRODUCT
                    $item = $slide_element->add_div()->add_class('item');
                        $product_div = $item->add_div()->add_class('product')->add_attr("itemscope itemtype", "http://schema.org/Product")->add_attr('product-key', $url_key);
                            $flag_div = $product_div->add_div()->add_class('product-flag');
                            // FLAG
                            if ($promo_text > 0 ){
                                $flag_div->add_div()->add_class('promo')->add(round($promo_text, 1).'%');
                            }
                            if ($is_hot_item > 0) {
                                $flag_div->add_div()->add_class('guaranted')->add('Guaranted');
                            }
                                
                            // IMAGE
                            $img_div = $product_div->add_div()->add_class('product-img');
                                $img_div->add('<a href="'.$location_detail.$url_key.'"><img src="'.$image_src.'" class="img-responsive" alt="'.$name.'" itemprop="image"></a>');
//                                $img_div->add('<a href="'.$location_detail.$url_key.'"><img class="lazyOwl" data-src="'.$image_src.'" alt="'.$name.'" itemprop="image"></a>');
//                                $img_div->add_img()->set_src($image_src)->add_class('img-responsive')->add_attr('alt', $name)->add_attr("itemprop","image");
                            
                            // SUMMARY    
                            $summary_div = $product_div->add_div()->add_class('product-summary');
                                $item_name = $summary_div->add_div()->add_class('product-name');
                                    $item_name->add('<a href="'.$location_detail.$url_key.'" itemprop="name" title="'.$name.'">'.$name.'</a>');
                                $item_price = $summary_div->add_div()->add_class('product-price');
                                    $item_price_normal = $item_price->add_div()->add_class('promo-price');
                                        $item_price_normal->add($price);
                                    $item_price_promo = $item_price->add_div()->add_class('normal-price');
                                        $item_price_promo->add($price_strike);
                                $item_action = $summary_div->add_div()->add_class('product-action');
                                
                                if ($is_available > 0) {
                                    if(strlen($promo_end_date)>0){
                                        $obj_promo_end_date=new DateTime($promo_end_date.' 23:59:59');
                                        $date_now=new DateTime(date('Y-m-d H:i:s'));
                                        $diff=$date_now->diff($obj_promo_end_date);
                                        $days=$diff->days;
                                        $diff_format = $days.'H:'.$diff->format('%HJ:%iM:%sD');
                                        $time_left_div = $item_action->add_div()->add_class('time-left');
                                        $time_left_div->add(clang::__('Time Left'));
                                        $time_left_div->add_br();
                                        $time_left_div->add('<span class="time" promo_end_date="'.$promo_end_date.' 23:59:59">'.$diff_format.'</span>');
                                    }
                                    else {
                                        $action = $item_action->add_action()->set_label(clang::__('Buy'))->add_class('btn-buy');
                                        $action->set_link($location_detail.$url_key);
                                    }
                                }
                                else {
//                                    $action = $item_action->add_action()->set_label(clang::__('Request'))->add_class('btn-warning btn-block');
                                    $action = $item_action->add_div()->add_class('time-left')
                                            ->add(clang::__('By Request'));
                                }
                     
                }
            }
        
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent=0) {
        $js = new CStringBuilder();
        
        $js->appendln("
            $('#".$this->id."-carousel').owlCarousel({
                autoPlay: true,");
        $js->appendln("items : ".$this->item_display.",");
        
        $js->appendln("
                itemsDesktop : [1199,5],
                itemsDesktopSmall : [979,3],
                itemsMobile: [479, 2],
                navigation : true,
                navigationText : ['<','>'],
                pagination: false,
                theme : 'custom-carousel',
//                lazyLoad : true,
            });
            ");

        $js->append(parent::js($indent));
        return $js->text();
    }
}
