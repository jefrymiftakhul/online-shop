<?php

class CElement_Cms_Publication extends CElement{
    

    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public static function factory($id = ''){
        return new CElement_Cms_Publication($id);
    }
            
    public function html($indent=0) {
        $db = CDatabase::instance();
        $html = new CStringBuilder();
        $html->set_indent($indent);   
		
        $q = "SELECT * 
                FROM cms_publication 
                WHERE status > 0 
                    AND org_id = ".CF::org_id()."
                LIMIT 9";
        $data = $db->query($q);
        if (count($data) > 0) {
            $container = $this->add_div()->add_class('container');
            $container->add("<h4 class='line-side-header'><span>".clang::__('PUBLICATION')."</span></h4>");

            $container_cms = $container->add_div()->add_class('container-publication');
            foreach ($data as $key => $value) {
                $url_link = cobj::get($value, 'url_link');
                $image_url = cobj::get($value, 'image_url');

                if (strlen($url_link) == 0) {
                    $item = "<img src='".$image_url."'/>";
                }
                else {
                    $item = "<a target='_blank' href='".$url_link."'><img src='".$image_url."'/></a>";
                }

                $container_cms->add_div()->add_class('publication-item')->add($item);
            }
        }

        $html->append(parent::html(), $indent);
        return $html->text();
    }
    
    public function js($indent=0) {
       $js = new CStringBuilder();

        $js->append(parent::js($indent));
        return $js->text();
    }
}
