<?php

class CElement_Cms_Testimonial extends CElement{
    
    protected $total_testimonial = 0;

    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public static function factory($id = ''){
        return new CElement_Cms_Testimonial($id);
    }

    public function set_total_testimonial($data) {
        $this->total_testimonial = $data;
        return $this;
    }
            
    public function html($indent=0) {
        $db = CDatabase::instance();
        $html = new CStringBuilder();
        $html->set_indent($indent);   
		
        $q = "SELECT ct.*, m.name, m.code, m.image_name 
                FROM cms_testimonial ct 
                LEFT JOIN member m 
                    ON m.member_id = ct.member_id
                WHERE ct.status > 0 
                    AND status_testimonial = 'APPROVE'
                    AND ct.org_id = ".CF::org_id()."
                ORDER BY ct.created desc
                LIMIT 6";
        $data = $db->query($q);

        $this->set_total_testimonial(count($data));

        if (count($data) > 0) {

            $container = $this->add_div()->add_class('container');
            $container->add("<h4 class='line-side-header'><span>".clang::__('TESTIMONIAL')."</span></h4>");

            $container_cms = $container->add_div()->add_class('container-testimonial');
            
            $item_slider = "<div class='slide-testimonial'>";
            $col = 0;

            $img_quote = curl::base()."application/62hallfamily/default/media/img/testimonial/quote.png";
            foreach ($data as $key => $value) {
                $name = cobj::get($value, 'name');
                $code = cobj::get($value, 'code');
                $testimonial = cobj::get($value, 'testimonial');
                $image_name = cobj::get($value, 'image_name');

                if ($image_name) {
                    $image = image::get_image_url_front($image_name, '');
                }
                else {
                    $image = curl::base() . 'application/62hallfamily/default/media/img/testimonial/user.png';
                }

                $div_open = "<div class='row'>";
                $div_close = "";

                if ($col == 1) {
                    $div_open = "";
                    $div_close = "</div>";
                    $col = 0;
                }
                else {
                    $col = 1;
                }

                $item_slider .= $div_open;
                $item_slider .= "<div class='col-sm-6'>";
                $item_slider .= "   <div class='item-testimonial'>";
                // $item_slider .= "       <div class='testimonial-left'>";
                // $item_slider .= "           <img class='testimonial-image' src='".$image."'/>";
                // $item_slider .= "       </div>";
                $item_slider .= "       <div class='testimonial-right'>";
                $item_slider .= "           <div class='img-quote'><img src='".$img_quote."'/></div>";
                $item_slider .= "           <span class='testimonial-text'><p>".$testimonial."</p></span>";
                $item_slider .= "           <div class='img-quote right'><img src='".$img_quote."'/></div>";
                $item_slider .= "           <p class='testimonial-name'>".$name.' - '.$code."</p>";
                $item_slider .= "       </div>";
                $item_slider .= "   </div>";
                $item_slider .= "</div>";
                $item_slider .= $div_close;
            }
            if ($col == 1) {
                $item_slider .= "</div>";
            }
            $item_slider .= "</div>";

            $container_cms->add($item_slider);
        }


        $html->append(parent::html(), $indent);
        return $html->text();
    }
    
    public function js($indent=0) {
        $js = new CStringBuilder();
        if ($this->total_testimonial > 2) {
            $js->appendln("
                var owl = $('.slide-testimonial').owlCarousel({
                    items: 1,
                    loop:true,
                    // autoplayTimeout:5000,
                    autoplay: true,
                    autoPlaySpeed: 5000,
                    autoPlayTimeout: 5000,
                    dots: false,
                    nav:false,
                    callbacks:true,
                });
            ");
        }
        else {
            $js->appendln("
                var owl = $('.slide-testimonial').owlCarousel({
                    items: 1,
                    autoplayTimeout:5000,
                    dots: false,
                    nav:false,
                    callbacks:true,
                });
            ");
        }

        $js->append(parent::js($indent));
        return $js->text();
    }
}
