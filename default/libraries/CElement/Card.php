<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Jun 12, 2016
     * @license http://ittron.co.id ITtron
     */
    class CElement_Card extends CElement {

        protected $product;
        public $page = '';

        public function __construct($id = "", $tag = "div") {
            parent::__construct($id, $tag);
        }

        public static function factory($id = "", $tag = "div") {
            return new CElement_Card($id, $tag);
        }

        public function html($indent = 0) {
            $html = CStringBuilder::factory();
            $session = Session::instance();
            $sess_member_id = $session->get('member_id');

//===============================================================================================================
//			Initial Variable
            $product = $this->product;
            $name = carr::get($product, 'name');
			$limit = 60;
			if (strlen($name) > $limit)
				$name = substr($name, 0, strrpos(substr($name, 0, $limit), ' ')) . '...';
            $detail_price = carr::get($product,'detail_price');
            $price = carr::get($detail_price, 'channel_sell_price');
            $product_id = carr::get($product, 'product_id');
            $member_id = carr::get($product, 'member_id');

            $url_key = carr::get($product, 'url_key');
            $image_path = carr::get($product, 'image_url');
            $image_name = carr::get($product, 'image_name');
            $filename = carr::get($product, 'filename');
            $image = $image_path;

            $url_product = curl::base() .$this->page.'/product/item/' . $url_key;
// ===============================================================================================================
//			Div Element Card
            $wrapper = $this->add_div()->add_class('lokal-prod-card-wrapper');
            $wrapper->add('<a href="' . $url_product . '">');
            $prod_image = $wrapper->add_div()->add_class('prod-image');
            $product_info = $wrapper->add_div()->add_class('prod-info');
            $prod_name = $product_info->add_div()->add_class('prod-name');
            $prod_price_label = $product_info->add_div()->add_class('prod-price-lbl');
            $prod_price = $product_info->add_div()->add_class('prod-price');
            $prod_box = $product_info->add_div()->add_class('prod-box');
			$prod_image->add_attr('style', 'background-image:url('.$image.');background-position:center;background-repeat:no-repeat;background-size:cover;width:100%;height:180px');
            $prod_name->add($name);
//            $prod_price_label->add(clang::__('SOLD').' 250');
            $prod_price->add('Rp. ' . ctransform::thousand_separator($price));
            $wrapper->add('</a>');

            $html->append(parent::html($indent));
            return $html->text();
        }

        public function js($indent = 0) {
            $js = CStringBuilder::factory();

            $js->append(parent::js($indent));
            return $js->text();
        }

        public function get_product() {
            return $this->product;
        }

		public function set_page($page){
			$this->page = $page;
			return $this;
		}

        public function set_product($product) {
            $this->product = $product;
            return $this;
        }

    }
