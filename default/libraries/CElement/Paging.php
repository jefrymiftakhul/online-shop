<?php

class CElement_Paging extends CElement{
    
    private $total_data;
	private $data_per_page;
    private $active;
	private $options_get=array();
	private $param_input=array();
	private $url_reload='';
	private $reload_target='';

    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public static function factory($id = ''){
        return new CElement_Paging($id);
    }
    
	public function set_total_data($val){
		$this->total_data=$val;
		return $this;
	}
	
	public function set_data_per_page($val){
		$this->data_per_page=$val;
		return $this;
	}

	public function set_active($val){
		$this->active=$val;
		return $this;
	}
	
	public function set_options_get($key,$val){
		$this->options_get[$key]=$val;
		return $this;
	}
	
	public function set_param_input($val){
		$arr_val=array();
		if(!is_array($val)){
			$arr_val[]=$val;
		}else{
			$arr_val=$val;
		}
		foreach($arr_val as $value){
			$this->param_input[]=$value;
		}
		return $this;
	}
	
	public function set_url_reload($val){
		$this->url_reload=$val;
		return $this;
	}

	public function set_reload_target($val){
		$this->reload_target=$val;
		return $this;
	}
	
    public function html($indent=0) {
        $html = new CStringBuilder();
        // pagination
        $div_pagination = $this->add_div()
                ->add_class('pagination col-md-12 padding-0')
                ->add('<center>');

        $count_page = (int) ($this->total_data/$this->data_per_page);
 
        if ($this->total_data % $this->data_per_page != 0) {
            $count_page += 1;
        }
		$url_options_get='';
		foreach($this->options_get as $key=>$val){
			$url_options_get.='&'.$key.'='.$val;
		}
	
        if ($count_page > 0) {
            if ($this->active > 1) {
                $previous_page = $this->active - 1;
                $div_pagination->add_action()
                        ->set_label('<i class="fa fa-fast-backward"></i>')
                        ->add_class('font-gray-dark')
                        ->add_listener('click')
                        ->add_handler('reload')
                        ->set_target($this->reload_target)
                        ->add_param_input($this->param_input)
                        ->set_url($this->url_reload.'?active=1'.$url_options_get);
                $div_pagination->add_action()
                        ->set_label('<i class="fa fa-backward"></i>')
                        ->add_class('font-gray-dark')
                        ->add_listener('click')
                        ->add_handler('reload')
                        ->set_target($this->reload_target)
                        ->add_param_input($this->param_input)
                        ->set_url($this->url_reload.'?active='.$previous_page.$url_options_get);
            }


            $start_page = $this->active - 5;
            if ($start_page < 1) {
                $start_page = 1;
            }

            $end_page = $this->active + 5;
            if ($end_page > $count_page) {
                $end_page = $count_page;
            }

            for ($i = $start_page; $i <= $end_page; $i++) {
                $this_page = $this->active;
                $class = NULL;
                $class = 'font-black';
                if ($this_page == $i) {
                    $class = 'btn-pagination bg-red font-white';
                }
                $div_pagination->add_action()
                        ->set_label($i)
                        ->add_class($class)
                        ->add_listener('click')
                        ->add_handler('reload')
                        ->set_target($this->reload_target)
                        ->add_param_input($this->param_input)
                        ->set_url($this->url_reload.'?active='.$i.$url_options_get);
            }
            if ($this->active < $count_page) {
                $next_page = $this->active + 1;
                $div_pagination->add_action()
                        ->set_label('<i class="fa fa-forward"></i>')
                        ->add_class('font-gray-dark')
                        ->add_listener('click')
                        ->add_handler('reload')
                        ->set_target($this->reload_target)
                        ->add_param_input($this->param_input)
                        ->set_url($this->url_reload.'?active='.$next_page.$url_options_get);
                $div_pagination->add_action()
                        ->set_label('<i class="fa fa-fast-forward"></i>')
                        ->add_class('font-gray-dark')
                        ->add_listener('click')
                        ->add_handler('reload')
                        ->set_target($this->reload_target)
                        ->add_param_input($this->param_input)
                        ->set_url($this->url_reload.'?active='.$count_page.$url_options_get);
            }
        }
        
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent=0) {
       $js = new CStringBuilder();

        $js->append(parent::js($indent));
        return $js->text();
    }
}
