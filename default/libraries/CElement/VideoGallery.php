<?php

class CElement_VideoGallery extends CElement{
    
    protected $active;
    protected $max_col=4;
    protected $play;
    
    
    public function __construct($id = '') {
        parent::__construct($id);
        $this->active=1;
    }
    
    
    public static function factory($id = ''){
        return new CElement_VideoGallery($id);
    }
    
    public function set_active($val){
        $this->active=$val;
        return $this;
    }
    
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        $org_id=CF::org_id();
        $db=CDatabase::instance();
        $limit=12;
        $active=$this->active;
        $offset = ($active - 1) * $limit;
        $q="
            select
                *
            from
                cms_video
            where
                status>0
                and org_id=".$db->escape($org_id)."
            order by
                updated desc
        ";            
        $r=$db->query($q);
        $total_data=$r->count();
        $q .= " LIMIT ".$offset.", ".$limit;
        $container_video_gallery=$this->add_div('container_video_gallery')->add_class('container-video-gallery');
        $container_video_gallery->add_div()->add_class('video-gallery-header')->add(clang::__('Video Gallery'));
        $data=$db->query($q);
        if($data->count()>0){
            $count_col=$this->max_col;
            $i = 0;
            foreach($data as $row){
                if($count_col==$this->max_col){
                    $div_row=$container_video_gallery->add_div()->add_class('row video-gallery-row');
                    $count_col=0;
                }
                $active = '';
                if ($i == 0) {
                    $active = 'active';
                }

                $div_col=$div_row->add_div()->add_class('col-md-3 video-gallery click-play')->set_attr('url-key',$row->url_key);
                $div_col_video = $div_col->add_div()->add_class('col-video-gallery '.$active);
                $div_image=$div_col_video->add_div();
                $div_image->add('<img src="//i3.ytimg.com/vi/'.$row->video_id.'/default.jpg" width="100%" >');
                $div_info=$div_col_video->add_div()->add_class('video-gallery-info');
                $div_info->add_div()->add_class('video-gallery-title')->add($row->title);
                $div_info->add_div()->add_class('video-gallery-description')->add($row->description);
                $count_col++;
                $i++;
            }
        }
        
        
        $div_paging=$container_video_gallery->add_div('div_paging');
        $element_paging = $div_paging->add_element('62hall-paging', 'paging');
        $element_paging->set_total_data($total_data);
        $element_paging->set_data_per_page($limit);
        $element_paging->set_active($active);
        $element_paging->set_reload_target('container_video_gallery');
        $element_paging->set_url_reload(curl::base() . 'reload/reload_video_gallery/');

        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent=0) {
        $js = new CStringBuilder();
        $js->append("
            $('.click-play').click(function(){
                var url_key=$(this).attr('url-key');

                jQuery('.col-video-gallery').removeClass('active');
                jQuery(this).find('.col-video-gallery').addClass('active');
                
                $.cresenity.reload('container_video','" . curl::base() . "reload/reload_video','get',{'url_key':url_key});
            });
            
        ");
        $js->append(parent::js($indent));
        return $js->text();
    }
}
