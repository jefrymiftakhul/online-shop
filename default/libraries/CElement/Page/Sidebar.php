<?php

class CElement_Page_Sidebar extends CElement{
    
    private $list_menu = array();
    private $active;


    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public static function factory($id = ''){
        return new CElement_Page_Sidebar($id);
    }
    
    public function set_active($active){
        $this->active = $active;
        return $this;
    }
    
    public function set_list_menu($list_menu){
        $this->list_menu = $list_menu;
        return $this;
    }

    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        
        $sidebar_page_container = $this->add_div()->add_class('col-md-12 sidebar-page');
        
        foreach ($this->list_menu as $menu){
            $header_menu = $sidebar_page_container->add_div()->add_class('row')->add_div()
                    ->add_class('col-md-11 margin-top-20 margin-bottom-10 font-size24 bold border-bottom')
                    ->add($menu['title_menu']);
            
            if(count($menu['menu'])){
                foreach ($menu['menu'] as $list_menu){
                    $active = NULL;
                    if ($this->active == $list_menu['name']){
                        $active = 'active';
                    }
                    
                    $sidebar_page_container->add_div()->add_class('row')->add_div()
                            ->add_class('col-md-12')
                            ->add('<a href="' . $list_menu['url'] . '" data-name="'.  htmlspecialchars($list_menu['name']).'" class="pull-left font-size14 font-gray-dark normal margin-10 ' . $active . '">' . $list_menu['label'] . '</a>');
                }
            }
            
        }
        
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent=0) {
       $js = new CStringBuilder();

        $js->append(parent::js($indent));
        return $js->text();
    }
}
