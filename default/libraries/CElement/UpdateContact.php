<?php

    class CElement_UpdateContact extends CElement {

		private $country_id='';
		private $using_contact_pay=null;
		private $using_contact_shipping=null;
		private $using_shipping=null;
		private $data_value=array();
		private $default_province='Jawa Timur';
		private $default_city='KOTA SURABAYA';
		private $default_province_id='';
		private $default_city_id='';
		private $page;
		private $using_raja_ongkir = false;
		private $raja_ongkir_product = null;
        private $raja_ongkir_data = array();

        public function __construct($id = '') {
            parent::__construct($id);
            $db = CDatabase::instance();
            $controller = CF::instance();
            if (method_exists($controller, 'page')) {
                $this->page = $controller->page();
            }
            $this->using_contact_pay = true;
            $this->using_contact_shipping = true;
            $this->using_shipping = true;
            $q = "
				select
					p.province_id as province_id,
					c.city_id as city_id
				from
					province as p
					inner join city as c on c.province_id=p.province_id
				where
					p.status>0
					and c.status>0
					and p.name=" . $db->escape($this->default_province) . "
					and c.name=" . $db->escape($this->default_city) . "
			";
            $r = cdbutils::get_row($q);
            if ($r) {
                $this->default_province_id = $r->province_id;
                $this->default_city_id = $r->city_id;
            }
        }

        public static function factory($id = '') {
            return new CElement_UpdateContact($id);
        }

		public function set_using_raja_ongkir($using_raja_ongkir) {
			$this->using_raja_ongkir = $using_raja_ongkir;
		}
		
		public function set_raja_ongkir_product($raja_ongkir_product) {
			$this->raja_ongkir_product = $raja_ongkir_product;
		}

		public function set_country_id($id = '') {
            $this->country_id = $id;
            return $this;
        }

        public function set_using_shipping($bool) {
            $this->using_shipping = $bool;
            return $this;
        }
        public function set_raja_ongkir_data($data) {
            $this->raja_ongkir_data = $data;
            return $this;
        }

        public function set_using_contact_shipping($bool) {
            $this->using_contact_shipping = $bool;
            return $this;
        }

        public function set_data_value($data_value) {
            $this->data_value = $data_value;
            return $this;
        }

        public function html($index = 0) {
            $db = CDatabase::instance();
            $html = new CStringBuilder();
            $session = Session::instance();
            $r = false;
            $member_id = $session->get('member_id');
            $reload_member_address_payment = 0;
            $reload_member_address_shipping = 0;
            $data_value = $this->data_value;
            $this->add_div('reload_pay');
            $this->add_div('reload_shipping');
            $row_update_contact = $this->add_div()->add_class('row row-update-contact');
            if ($this->using_contact_pay) {
                $div_contact_payment = $row_update_contact->add_div('div_contact_pay')->add_class('col-md-12');
                $div_contact_payment->add("
					<div class='row txt-title-contact'>
                        Alamat Pembayaran
                    </div>
				");
                if ($member_id != null) {
                    $q = "
						select 
							member_address_id,
							name,
                                                        province_id,
                                                        city_id,
							address
						from 
							member_address 
						where 
							status>0
							and is_active>0
							and type='payment'
							and member_id=" . $db->escape($member_id) . "
							
					";
                    $r = cdbutils::get_row($q);
                    
                    if ($r) {
                        $reload_member_address_payment = 1;
                    }
                    $div_member_address_payment = $div_contact_payment->add_div('div_member_address_payment');
                    $control_member_address_select = $div_member_address_payment->add_field()->set_label('Auto Fill')->add_control('member_address_payment', 'member-address-select')->add_class('width-40-percent');
                    $control_member_address_select->set_type('payment');
                    $control_member_address_select->set_member($member_id);
                    $control_member_address_select->set_all(false);
                    $listener_member_address_select = $control_member_address_select->add_listener('change');
                    $listener_member_address_select
                            ->add_handler('reload')
                            ->set_target('reload_pay')
                            ->set_url(curl::base() . 'reload/reload_member_address')
                            ->add_param_input(array('member_address_payment'));
                    if ($reload_member_address_payment > 0) {
                        $listener_member_address_select = $control_member_address_select->add_listener('ready');
                        $listener_member_address_select
                                ->add_handler('reload')
                                ->set_target('reload_pay')
                                ->set_url(curl::base() . 'reload/reload_member_address')
                                ->add_param_input(array('member_address_payment'));
                    }
                }

                $row = $div_contact_payment->add_div()->add_class('row');
                $col = $row->add_div()->add_class('col-md-12');
                $col->add_field()->set_label('Nama Lengkap')
                        ->add_control('name_pay', 'text')
                        ->set_value(carr::get($data_value, 'name_pay'));

                $row = $div_contact_payment->add_div()->add_class('row');
                $col = $row->add_div()->add_class('col-md-12');
                $col->add_field()->set_label('Email')->add_control('email_pay', 'text')
                        ->set_value(carr::get($data_value, 'email_pay'));

                $row = $div_contact_payment->add_div()->add_class('row');
                $col = $row->add_div()->add_class('col-md-12');
                $col->add_field()->set_label('Alamat')->add_control('address_pay', 'text')
                        ->set_value(carr::get($data_value, 'address_pay'));

                $row = $div_contact_payment->add_div()->add_class('row');
                $col = $row->add_div()->add_class('col-md-12');
                $field = $col->add_field();
                $field->set_label('No.HP');
                $field->add_control('phone_area_pay', 'text')->add_class('width-10-percent')->set_value('+62');
                $field->add_control('phone_pay', 'text')->add_class('row-phone')->set_value(carr::get($data_value, 'phone_pay'));
                
                $row = $div_contact_payment->add_div()->add_class('row row-double');
                $col1 = $row->add_div()->add_class('col-xs-6');
                $col2 = $row->add_div()->add_class('col-xs-6');
                $control_province_select = $col1->add_field()->set_label('Propinsi')->add_control('province_pay', 'province-select')->set_value(carr::get($data_value, 'province_pay'));

                $control_province_select->set_all(false);
                $control_province_select->set_country_id($this->country_id);
                if (strlen($this->default_province_id) > 0) {
                    $control_province_select->set_value($this->default_province_id);
                }

                $province_pay_sess = carr::get($data_value, 'province_pay');
                $city_pay_sess = carr::get($data_value, 'city_pay');
                $districts_pay_sess = carr::get($data_value, 'districts_pay');
                if (strlen($province_pay_sess) > 0) {
                    $control_province_select->set_value($province_pay_sess);
                    $this->default_city_id = $city_pay_sess;
                }

                $listener_province_select = $control_province_select->add_listener('change');
                $listener_province_select
                        ->add_handler('reload')
                        ->set_target('div_city_pay')
                        // ->set_url(curl::base() . 'reload/reload_city/' . $this->default_city_id.'/'.$districts_pay_sess)
                        ->set_url(curl::base() . 'reload/reload_city/')
                        ->add_param_input(array('province_pay'));
                if ($reload_member_address_payment == 0) {
                    $listener_province_select = $control_province_select->add_listener('ready');
                    $listener_province_select
                            ->add_handler('reload')
                            ->set_target('div_city_pay')
                            ->set_url(curl::base() . 'reload/reload_city/' . $this->default_city_id.'/'.$districts_pay_sess)
                            ->add_param_input(array('province_pay'));
                }

                $div_city = $col2->add_div('div_city_pay');
                $control_city_select = $div_city->add_field()->set_label('Kota')->add_control('city_pay', 'city-select')->add_class('width-40-percent')
                        ->set_value(carr::get($data_value, 'city_pay'));
                $control_set_city_select = $control_city_select;
                
                if($r){
                    $province_id = $r->province_id;
                        $control_set_city_select->set_province_id($province_id);
                }
                
                $control_city_select->set_all(false);
                $listener_city_select = $control_city_select->add_listener('change');
                $listener_city_select
                        ->add_handler('reload')
                        ->set_target('div_districts_pay')
                        ->set_url(curl::base() . 'reload/reload_districts?page=' . $this->page)
                        ->add_param_input(array('city_pay'));

                $row = $div_contact_payment->add_div()->add_class('row row-double');
                $col1 = $row->add_div()->add_class('col-xs-6');
                $col2 = $row->add_div()->add_class('col-xs-6');
                $div_districts = $col1->add_div('div_districts_pay');
                $control_districts_select = $div_districts->add_field()->set_label('Kecamatan')->add_control('districts_pay', 'districts-select')->add_class('width-40-percent')
                        ->set_value(carr::get($data_value, 'districts_pay'));
                $control_districts_select->set_all(false);
                
                if($r){
                    $city_id = $r->city_id;
                        $control_districts_select->set_city_id($city_id);
                }

                $col2->add_field()->set_label('Kode Pos')->add_control('post_code_pay', 'text')->set_value(carr::get($data_value, 'post_code_pay'));

				$row_update_contact->add_div()->add_class('col-md-12 padding-0')->add_div()->add_class('separate-dark');
            }
            if ($this->using_contact_shipping) {
                $div_contact_shipping = $row_update_contact->add_div('div_contact_shipping')->add_class('col-md-12');
                $div_contact_shipping->add("
					<div class='row padding-0 div-pengiriman'>
						<div class='col-sm-6 padding-0'>
							<div class='txt-title-contact'>
                                Alamat Pengiriman
                            </div>
						</div>
						<div class='col-sm-6 checkbox-same'>
							<input type='checkbox' id='check_same' name='check_same'><label for='check_same' class='noselect'>&nbsp;Samakan dengan alamat pembayaran</label>
						</div>
					</div>
				");
                if ($member_id != null) {
                    $q = "
						select 
							member_address_id,
							name,
                                                        province_id,
                                                        city_id,
							address
						from 
							member_address 
						where 
							status>0
							and is_active>0
							and type='shipping'
							and member_id=" . $db->escape($member_id) . "
							
					";
                    $r = cdbutils::get_row($q);
                    
                    if ($r) {
                        $reload_member_address_shipping = 1;
                    }
                    $div_member_address_shipping = $div_contact_shipping->add_div('div_member_address_shipping');
                    $control_member_address_select = $div_member_address_shipping->add_field()->set_label('Auto Fill')->add_control('member_address_shipping', 'member-address-select')->add_class('width-40-percent');
                    $control_member_address_select->set_type('shipping');
                    $control_member_address_select->set_member($member_id);
                    $control_member_address_select->set_all(false);
                    $listener_member_address_select = $control_member_address_select->add_listener('change');
                    $listener_member_address_select
                            ->add_handler('reload')
                            ->set_target('reload_shipping')
                            ->set_url(curl::base() . 'reload/reload_member_address')
                            ->add_param_input(array('member_address_shipping'));
                    if ($reload_member_address_shipping > 0) {
                        $listener_member_address_select = $control_member_address_select->add_listener('ready');
                        $listener_member_address_select
                                ->add_handler('reload')
                                ->set_target('reload_shipping')
                                ->set_url(curl::base() . 'reload/reload_member_address')
                                ->add_param_input(array('member_address_shipping'));
                    }
                }
                $div_contact_shipping->add_field()->set_label('Nama Lengkap')->add_control('name_shipping', 'text')
                        ->set_value(carr::get($data_value, 'name_shipping'));
                $div_contact_shipping->add_field()->set_label('Email')->add_control('email_shipping', 'text')
                        ->set_value(carr::get($data_value, 'email_shipping'));
                $div_contact_shipping->add_field()->set_label('Alamat')->add_control('address_shipping', 'text')
                        ->set_value(carr::get($data_value, 'address_shipping'));
                $field = $div_contact_shipping->add_field();
                $field->set_label('No.HP');
                $field->add_control('phone_area_shipping', 'text')->add_class('width-10-percent')->set_value('+62');
                ;
                $field->add_control('phone_shipping', 'text')->add_class('row-phone')
                        ->set_value(carr::get($data_value, 'phone_shipping'));

                $row = $div_contact_shipping->add_div()->add_class('row row-double');
                $col1 = $row->add_div()->add_class('col-xs-6');
                $col2 = $row->add_div()->add_class('col-xs-6');
                $control_province_select = $col1->add_field()->set_label('Propinsi')->add_control('province_shipping', 'province-select')->set_value(carr::get($data_value, 'province_shipping'));

                $control_province_select->set_all(false);
                $control_province_select->set_country_id($this->country_id);
                $control_province_select->set_page($this->page);

                if (strlen($this->default_province_id) > 0) {
                    $control_province_select->set_value($this->default_province_id);
                }
                $province_shipping_sess = carr::get($data_value, 'province_shipping');
                $city_shipping_sess = carr::get($data_value, 'city_shipping');
                $districts_shipping_sess = carr::get($data_value, 'districts_shipping');
                
                if (strlen($province_shipping_sess) > 0) {
                    $control_province_select->set_value($province_shipping_sess);
                    $this->default_city_id = $city_shipping_sess;
                }
                $same_location_shipping_price=false;
                if(ccfg::get('have_shipping_location_from_shipping_price')){
                    if($this->using_raja_ongkir==false){
                        $same_location_shipping_price=true;
                    }
                }
                $control_province_select->set_same_location_shipping_price($same_location_shipping_price);
                $listener_province_select = $control_province_select->add_listener('change');
                if($this->using_raja_ongkir){
                    $listener_province_select
                    ->add_handler('reload')
                    ->set_target('div_city_shipping')
                    ->set_url(curl::base() . 'reload/reload_city?page=' . $this->page.'&raja_ongkir=1')
                    ->add_param_input(array('province_shipping'));
                }
                else{
                    $listener_province_select
                    ->add_handler('reload')
                    ->set_target('div_city_shipping')
                    ->set_url(curl::base() . 'reload/reload_city?page=' . $this->page)
                    ->add_param_input(array('province_shipping'));
                }


                if ($reload_member_address_shipping == 0) {
                    $listener_province_select = $control_province_select->add_listener('ready');
					if($this->using_raja_ongkir){
						$listener_province_select
                            ->add_handler('reload')
                            ->set_target('div_city_shipping')
                            ->set_url(curl::base() . 'reload/reload_city/' . $this->default_city_id .'/'.$districts_shipping_sess. '?page=' . $this->page.'&raja_ongkir=1')
                            ->add_param_input(array('province_shipping'));
					}else{
						$listener_province_select
                            ->add_handler('reload')
                            ->set_target('div_city_shipping')
                            ->set_url(curl::base() . 'reload/reload_city/' . $this->default_city_id.'/'.$districts_shipping_sess)
                            // ->set_url(curl::base() . 'reload/reload_city/' . $this->default_city_id . '?page=' . $this->page)
                            ->add_param_input(array('province_shipping'));
					}
                }
                $div_city = $col2->add_div('div_city_shipping');
                $control_city_select = $div_city->add_field()->set_label('Kota')->add_control('city_shipping', 'city-select')->set_value(carr::get($data_value, 'city_shipping'));
                $control_city_select->set_all(false);
                $control_city_select->set_page($this->page);
                $control_city_select->set_same_location_shipping_price($same_location_shipping_price);
                $listener_city_select = $control_city_select->add_listener('change');
                $listener_city_select
                        ->add_handler('reload')
                        ->set_target('div_districts_shipping')
                        ->set_url(curl::base() . 'reload/reload_districts/?page=' . $this->page)
                        ->add_param_input(array('city_shipping'));
                
                if($r){
                   
                $control_city_select->set_province_id($r->province_id);
                }

                $row = $div_contact_shipping->add_div()->add_class('row row-double');
                $col1 = $row->add_div()->add_class('col-xs-6');
                $col2 = $row->add_div()->add_class('col-xs-6');
                $div_districts = $col1->add_div('div_districts_shipping');
                $control_districts_select = $div_districts->add_field()->set_label('Kecamatan')->add_control('districts_shipping', 'districts-select')->add_class('width-40-percent')
                        ->set_value(carr::get($data_value, 'districts_shipping'));

                if($r){
                    $control_districts_select->set_city_id($r->city_id);
                }
                $control_districts_select->set_all(false);
                $control_districts_select->set_same_location_shipping_price($same_location_shipping_price);
                $listener_districts_select = $control_districts_select->add_listener('change');
                
                if($this->using_shipping){
                    $listener_city_select
                            ->add_handler('reload')
                            ->set_target('div_shipping_price')
                            ->add_param_input('shipping_type_id')
                            ->add_param_input('city_shipping')
                            ->set_url(curl::base() . 'reload/reload_shipping_price?page=' . $this->page);
                }

                $col2->add_field()->set_label('Kode Pos')->add_control('post_code_shipping', 'text')->add_class('width-40-percent')
                        ->set_value(carr::get($data_value, 'post_code_shipping'));
            }

            $row_update_contact->add_div()->add_class('col-md-12 padding-0')->add_div()->add_class('separate-dark');


            if($this->using_shipping){
                $div_shipping = $row_update_contact->add_div('div_shipping');
                
                $data_type_shipping = shipping::get_shipping_type();
                
                $list_shipping_type = array();
                foreach ($data_type_shipping as $type_shipping){
                    $shipping_type_id = cobj::get($type_shipping, 'shipping_type_id');
                    $name = cobj::get($type_shipping, 'name');
                    
                    $list_shipping_type[$shipping_type_id] = $name;
                }
                    

				$div_shipping_type = $div_shipping->add_div('div_shipping_type')->add_class('col-md-12');
				$control_shipping_type = $div_shipping_type->add_field()->set_label(clang::__('Tipe Pengiriman'))
						->add_control('shipping_type_id', 'select')
                                                ->custom_css('width', '200px')
						->set_list($list_shipping_type);
				
				$control_shipping_type->add_listener('change')
						->add_handler('reload')
						->set_target('div_shipping_price')
						->add_param_input('shipping_type_id')
						->add_param_input('districts_shipping')
						->set_url(curl::base() . 'reload/reload_shipping_price?page='.$this->page);
				
				$control_shipping_type->add_listener('ready')
						->add_handler('reload')
						->set_target('div_shipping_price')
						->add_param_input('shipping_type_id')
						->add_param_input('districts_shipping')
						->set_url(curl::base() . 'reload/reload_shipping_price?page='.$this->page);
				
				$div_shipping_price = $div_shipping->add_div('div_shipping_price')->add_class('col-md-12');
			}

            if($this->using_raja_ongkir){
                $org_id=CF::org_id();
                $q_data_org = 'select * from org where status > 0 and org_id = '.$db->escape($org_id);
                $data_org = cdbutils::get_row($q_data_org);
                $raja_ongkir_weight = carr::get($this->raja_ongkir_data, 'weight');
                $div_shipping = $row_update_contact->add_div('div_raja_ongkir')->add_class('col-md-12');
                $div_shipping->add_control('weight_shipping', 'hidden')->set_value($raja_ongkir_weight);
                // $div_shipping->add_control('origin_shipping', 'hidden');
                // $div_shipping->add_control('shipping_price', 'hidden')->set_value();
                $field_service = $div_shipping->add_field()->set_label('Pilih Service');
                $json_shipping = cobj::get($data_org,'shipping');
                $available_service = json_decode($json_shipping,true);

                $list = array();
                if(count($available_service) > 0){
                    foreach ($available_service as $key => $value) {
                        if($value != null){
                            $val = explode('_', $key);
                            $list[strtolower($val[0])] = strtoupper($val[0]);
                        }
                    }
                }
                
                $service = $field_service->add_control('service_type','select')->set_list($list)->set_applyjs('select2')->custom_css('width','40%');
                
                $service->add_listener('change')->add_handler('reload')->set_url(curl::base().'raja_ongkir/get_cost')->set_target('service_reload')->add_param_input(array('service_type','city_shipping','weight_shipping','city_shipping','districts_shipping'));

                $service->add_listener('ready')->add_handler('reload')->set_url(curl::base().'raja_ongkir/get_cost')->set_target('service_reload')->add_param_input(array('service_type','city_shipping','weight_shipping','city_shipping','districts_shipping'));
                $field_service->add_div('service_reload')->add_class('col-xs-12 col-sm-10 col-sm-offset-2')->custom_css('padding',' 20px 0px 0px 30px');
            }
            $html->appendln(parent::html($index));

            return $html->text();
        }

        public function js($index = 0) {
            $js = new CStringBuilder();
            $js_reload="$.cresenity.reload('div_shipping_price','" . curl::base() . 'reload/reload_shipping_price?page=' . $this->page . "','get',{'shipping_type_id':$.cresenity.value('#shipping_type_id'),'districts_shipping':$.cresenity.value('#districts_shipping'),'total_weight':$.cresenity.value('#total_weight')});";
            if($this->using_raja_ongkir){
                $js_reload="$.cresenity.reload('service_reload','" . curl::base() . "raja_ongkir/get_cost','get',{'service_type':$.cresenity.value('#service_type'),'city_shipping':$.cresenity.value('#city_shipping'),'districts_shipping':$.cresenity.value('#districts_shipping'),'weight_shipping':$.cresenity.value('#weight_shipping'),'origin_shipping':$.cresenity.value('#origin_shipping')});";
            }
            $custom_js = "";
            if ($this->using_contact_shipping) {
                $custom_js = "
					$('#name_pay').focus();
					function reload_member_address(data){
						if(data.type=='payment'){
							$('#name_pay').val(data.name);
							$('#email_pay').val(data.email);
							$('#address_pay').val(data.address);
							$('#phone_area_pay').val('+62');
							$('#phone_pay').val(data.phone);
							$('#post_code_pay').val(data.postal);
							$('#province_pay').select2('data',{province_id:data.province_id,name:data.province});
							$('#province_pay').val(data.province_id);
							$('#city_pay').select2('data',{city_id:data.city_id,name:data.city});
							$('#city_pay').val(data.city_id);
							$('#districts_pay').select2('data',{districts_id:data.districts_id,name:data.districts});
							$('#districts_pay').val(data.districts_id);
							
							if($('#check_same').is(':checked')){
								$('#name_shipping').val(data.name);
								$('#email_shipping').val(data.email);
								$('#address_shipping').val(data.address);
								$('#phone_area_shipping').val('+62');
								$('#phone_shipping').val(data.phone);
								$('#post_code_shipping').val(data.postal);
								province_select2_make_same();
								city_select2_make_same();
								districts_select2_make_same();
							}
						}
						
						if(data.type=='shipping'){
							$('#name_shipping').val(data.name);
							$('#email_shipping').val(data.email);
							$('#address_shipping').val(data.address);
							$('#phone_area_shipping').val('+62');
							$('#phone_shipping').val(data.phone);
							$('#post_code_shipping').val(data.postal);
							$('#province_shipping').select2('data',{province_id:data.province_id,name:data.province});
							$('#province_shipping').val(data.province_id);
							$('#city_shipping').select2('data',{city_id:data.city_id,name:data.city});
							$('#city_shipping').val(data.city_id);
							$('#districts_shipping').select2('data',{districts_id:data.districts_id,name:data.districts});
							$('#districts_shipping').val(data.districts_id);
							".$js_reload."
						}
					
					
					}
				
                                        
					function province_select2_make_same(){
							var data=$('#province_pay').select2('data');
							if(data){
								$('#province_shipping').select2('data',{province_id:data.province_id,name:data.name});
								$('#province_shipping').val(data.province_id);
							}
					}
					function city_select2_make_same(){
							var data=$('#city_pay').select2('data');
							if(data){
								$('#city_shipping').select2('data',{city_id:data.city_id,name:data.name});
								$('#city_shipping').val(data.city_id);
							}
					}
					function districts_select2_make_same(){
							var data=$('#districts_pay').select2('data');
							if(data){
								$('#districts_shipping').select2('data',{districts_id:data.districts_id,name:data.name});
								$('#districts_shipping').val(data.districts_id);
                                                                ".$js_reload."
							}
					
					}
					
					$('#check_same').change(function(){
						if($(this).is(':checked')){
							var name_shipping=$('#name_pay').val();
							var email_shipping=$('#email_pay').val();
							var address_shipping=$('#address_pay').val();
							var phone_area_shipping=$('#phone_area_pay').val();
							var phone_shipping=$('#phone_pay').val();
							var post_code_shipping=$('#post_code_pay').val();

							$('#name_shipping').val(name_shipping);
							$('#email_shipping').val(email_shipping);
							$('#address_shipping').val(address_shipping);
							$('#phone_area_shipping').val(phone_area_shipping);
							$('#phone_shipping').val(phone_shipping);
							$('#post_code_shipping').val(post_code_shipping);

							$('#member_address_shipping').attr('readonly',true);
							$('#member_address_shipping').select2('data',{member_address_id:'',name:'',address:''});
							$('#member_address_shipping').val('');
							$('#name_shipping').attr('readonly',true);
							$('#email_shipping').attr('readonly',true);
							$('#address_shipping').attr('readonly',true);
							$('#phone_area_shipping').attr('readonly',true);
							$('#phone_shipping').attr('readonly',true);
							$('#province_shipping').select2('readonly',true);
							$('#city_shipping').select2('readonly',true);
							$('#districts_shipping').select2('readonly',true);
							$('#post_code_shipping').attr('readonly',true);
							province_select2_make_same();
							city_select2_make_same();
							districts_select2_make_same();
						}else{

							$('#member_address_shipping').attr('readonly',false);
							$('#name_shipping').attr('readonly',false);
							$('#email_shipping').attr('readonly',false);
							$('#address_shipping').attr('readonly',false);
							$('#phone_area_shipping').attr('readonly',false);
							$('#phone_shipping').attr('readonly',false);
							$('#province_shipping').select2('readonly',false);
							$('#city_shipping').select2('readonly',false);
							$('#districts_shipping').select2('readonly',false);
							$('#post_code_shipping').attr('readonly',false);
						
						}
                        
                                            
					});
					
					$('#name_pay').change(function(){
						if($('#check_same').is(':checked')){
							$('#name_shipping').val($(this).val());
						}
					});
					$('#email_pay').change(function(){
						if($('#check_same').is(':checked')){
							$('#email_shipping').val($(this).val());
						}
					});
					$('#address_pay').change(function(){
						if($('#check_same').is(':checked')){
							$('#address_shipping').val($(this).val());
						}
					});
					$('#phone_area_pay').change(function(){
						if($('#check_same').is(':checked')){
							$('#phone_area_shipping').val($(this).val());
						}
					});
					$('#phone_pay').change(function(){
						if($('#check_same').is(':checked')){
							$('#phone_shipping').val($(this).val());
						}
					});
					$('#post_code_pay').change(function(){
						if($('#check_same').is(':checked')){
							$('#post_code_shipping').val($(this).val());
						}
					});
					
					$(document).on('change','#province_pay',function(){
						if($('#check_same').is(':checked')){						
							province_select2_make_same();
						}
					});
					$(document).on('change','#city_pay',function(){
						if($('#check_same').is(':checked')){
							city_select2_make_same();
						}
					});
					$(document).on('change','#district_pay',function(){
						if($('#check_same').is(':checked')){
							districts_select2_make_same();
						}
					});
					
				";
            }
            $js->appendln($custom_js);
            $js->appendln(parent::js($index));
            return $js->text();
        }

    }
    