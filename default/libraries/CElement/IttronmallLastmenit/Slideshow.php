<?php

class CElement_IttronmallLastmenit_SlideShow extends CElement_Slideshow {

    protected $slides = array();
    protected $type = 'product';
    protected $link = NULL;
    protected $class = NULL;

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_IttronmallLastmenit_SlideShow($id);
    }

    public function set_slides($slides) {
        $this->slides = $slides;
        return $this;
    }

    public function additional_class($class) {
        $this->class = $class;
        return $this;
    }

    public function set_type($val) {
        $this->type = $val;
        return $this;
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        $class = '';

        $store = NULL;
        $domain = NULL;
        $org_id = CF::org_id();
        $data_org = org::get_org($org_id);
        if (!empty($data_org)) {
            $store = $data_org->name;
            $domain = $data_org->domain;
        }

        if (is_array($this->class)) {
            foreach ($this->class as $key => $val) {
                $class .= $val . ' ';
            }
        } else if (is_string($this->class))
            $class = $this->class;

        $carousel_id = $this->id . '-slider';

        $html->appendln('<div id="' . $carousel_id . '" class="carousel slide slide-show" data-ride="carousel">');
        $html->appendln('<ol class="carousel-indicators ' . $class . ' ">');

        $count = count($this->slides);
        cdbg::var_dump($count);
        die;

        for ($no = 0; $no < $count; $no++) {
            $active = ($no == 0) ? 'active' : '';
            $html->appendln('<li data-target="#' . $carousel_id . '" data-slide-to="' . $no . '" class="' . $active . '"></li>');
        }
        $html->appendln('</ol>');
        $html->appendln('<div class="carousel-inner" role="listbox">');

        $no = 0;
        foreach ($this->slides as $slide) {
            $no++;
            $active = ($no == 1) ? 'active' : '';

            $html->appendln('<div class="item ' . $active . '">');
            if (isset($slide['url_link'])) {
                if (strlen($slide['url_link']) > 0) {
                    $html->appendln('<a class="link" target="_blank" href="' . $slide['url_link'] . '">');
                }
            }
            if (isset($slide['image_url'])) {
                $html->appendln('<img src="' . $slide['image_url'] . '" alt="" width="100%">');
            }
            if (isset($slide['image_path'])) {
                $html->appendln('<img src="' . $slide['image_path'] . '" alt="" width="100%">');
            }
            if (isset($slide['url_link'])) {
                if (strlen($slide['url_link']) > 0) {
                    $html->appendln('</a>');
                }
            }
            
            $html->appendln('</div>');
        }
        $html->appendln('</div>');
        $html->appendln('<a class="left carousel-control" href="#' . $carousel_id . '" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="right carousel-control" href="#' . $carousel_id . '" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                          </a>');
        $html->appendln('</div>');

        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $js->appendln("
            jQuery('.slide-show').carousel({
                interval: 5000,
                pause: 'hover'
            });

            jQuery('input').focus(function(){
               jQuery('.slide-show').carousel('pause');
            }).blur(function() {
               jQuery('.slide-show').carousel('cycle');
            });
            ");

        $js->append(parent::js($indent));
        return $js->text();
    }

}
