<?php

class CElement_IttronmallLastmenit_Product extends CElement {

    private $class = 'fitgloss';
    private $data_product = null;
    private $product_hot = "Hot";
    private $product_new = "New";
    private $price = 0;
    private $data = array();
    protected $limit = 10;
    protected $page = '';

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_IttronmallLastmenit_Product($id);
    }

    // giving class for multiple styling purpose
    public function set_class($class) {
        $this->class = $class;
        return $this;
    }

    public function set_data_product($data) {
        $this->data_product = $data;
        return $this;
    }

    public function set_price($price) {
        $this->price = $price;
        return $this;
    }

    public function set_limit($val) {
        $this->limit = $val;
        return $this;
    }

    public function set_page($val) {
        $this->page = $val;
        return $this;
    }

    private function get_image($image_name) {
        $return = curl::base() . 'application/ittronmall/default/media/img/product/no-image.png';
        if ($image_name != NULL) {
            $return = image::get_image_url($image_name, '240x240');
        }
        return $return;
    }

    private function get_price() {
        $detail_price = $this->price;

        $promo_price = 0;
        $price = 0;

        if (!empty($detail_price['ho_sell_price']) and ! empty($detail_price['channel_sell_price'])) {
            if ($detail_price['ho_sell_price'] > $detail_price['channel_sell_price']) {
                $price = $detail_price['ho_sell_price'];
                $promo_price = $detail_price['channel_sell_price'];
            } else {
                $price = $detail_price['channel_sell_price'];
            }
        }

        return array('promo_price' => $promo_price, 'price' => $price);
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        // data product
        $data_product = $this->data_product;

        $image_url = carr::get($data_product, 'image_url');
        $file_path = carr::get($data_product, 'file_path');
        $image_src = null;
        if ($file_path != NULL) {
            $image_src = $file_path;
        }
        if ($this->data_product) {
            $image_src = $image_url;
        }
        if ($image_src == null) {
            $image_src = curl::base() . 'application/ittronmall/default/media/img/product/no-image.png';
        }
        
        // override before
        $image_name = carr::get($data_product, 'image_path');
        $image_src = $this->get_image($image_name);

        $name = carr::get($data_product, 'name');
        $url_key = carr::get($data_product, 'url_key');
        $is_hot_item = carr::get($this->data_product, 'is_hot_item');
        $is_available = carr::get($this->data_product, 'is_available');
        $new_product_date = carr::get($this->data_product, 'new_product_date');
        // data product price
        $detail_price = carr::get($data_product, 'detail_price');
        $channel_sell_price = carr::get($detail_price, 'channel_sell_price');
        $ho_sell_price = carr::get($detail_price, 'ho_sell_price');
        $price_all = $this->get_price($detail_price);
        $price_promo = carr::get($price_all, 'promo_price');
        $price_normal = carr::get($price_all, 'price');
        $promo_value = carr::get($detail_price, 'promo_value');

        if (isset($_GET['debug'])) {
            cdbg::var_dump($ho_sell_price);
        }

        $price = 'Rp ' . ctransform::format_currency($channel_sell_price);
        $price_sale = 'Rp ' . ctransform::format_currency($ho_sell_price);
        $price_strike = '';
        $percent_sale = '';
        $percent_label = '';
        if ($promo_value != 0) {
            $promo_type = carr::get($detail_price, 'promo_type');
            $promo_value = carr::get($detail_price, 'promo_value');
            if ($promo_type == 'amount') {
                $price_promo = $price_normal - $promo_value;
//                $price = 'Rp ' . ctransform::format_currency($price_promo);
//                $price_strike = 'Rp ' . ctransform::format_currency($price_normal);
                if ($price_normal !== 0) {
                    $percent_sale = '- '.round(($promo_value / $price_normal) * 100);
                }
                $percent_label = $percent_sale . '%';
            } else if ($promo_type == 'percent') {
                $price_percent = $price_normal * ($promo_value / 100);
                $price_promo = $price_normal - $price_percent;
//                $price = 'Rp ' . ctransform::format_currency($price_promo);
//                $price_strike = 'Rp ' . ctransform::format_currency($price_normal);
                $percent_sale = $promo_value;
                $percent_label = '- '.$percent_sale . '%';
            }
        }

        $product_wrapper = $this->add_div()->add_class('product-wrapper ' . $this->class);
        $product_wrapper->add('<a href="' . curl::base() . 'product/item/' . $url_key . '">');
        $product_icon_wrapper = $product_wrapper->add_div()->add_class('product-icon-wrapper');
        if ($is_hot_item > 0) {
            $product_icon_wrapper->add_div()->add_class('icon-sale-fitgloss');
        }
        if (strtotime($new_product_date) > strtotime(date('Y-m-d'))) {
            $product_icon_wrapper->add_div()->add_class('ico-new');
        }
        $product_wrapper_image = $product_wrapper->add_div()->add_class('product-wrapper--image');
        $product_wrapper_image->add('<img src="' . $image_src . '" class="img-responsive">');
        $product_wrapper_description = $product_wrapper->add_div()->add_class('product-wrapper--description');
        $product_title = $product_wrapper_description->add_div()->add_class('product-wrapper--description-title');
        $product_title->add($name);

        if ($promo_value != 0) {
//            $product_price = $product_wrapper_description->add_div()->add_class('product-wrapper--description-price');
//            $product_price->add();
            $product_price = $product_wrapper_description->add_div()->add_class('product-wrapper--description-price-promo');
            $product_price->add($price);
            $product_tag= $product_wrapper_description->add_div()->add_class('promo-price-text');
            $product_tag->add($percent_label);
            $product_price_disc = $product_wrapper_description->add_div()->add_class('product-wrapper--description-price-discount');
            $product_price_disc->add($price_sale);
        } else {
            $product_price = $product_wrapper_description->add_div()->add_class('product-wrapper--description-price');
            $product_price->add($price);
        }
        $product_wrapper->add('</a>');

        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $js->append(parent::js($indent));
        return $js->text();
    }

}
