<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Mar 23, 2016
     */
    class CElement_IttronmallLastmenit_SlideDeal extends CElement_SlickSlideShow{
        private $using_title = true;
        private $title = 'Slide';
        private $see_all = false;
        private $link_see_all = '/';

        public function __construct($id = "", $tag = "div") {
            parent::__construct($id, $tag);
            // responsive slide setting
            $responsive = array();
//            $responsive[] = array(
//                'breakpoint' => '1200',
//                'slide_to_show' => '4',
//                'slide_to_scroll' => '1',
//                'dots' => 'false',
//            );
             $responsive[] = array(
                'breakpoint' => '992',
                'slide_to_show' => '3',
                'slide_to_scroll' => '1',
                'dots' => 'false',
            );
            $responsive[] = array(
                'breakpoint' => '768',
                'slide_to_show' => '3',
                'slide_to_scroll' => '1',
                'dots' => 'false',
            );
           $responsive[] = array(
               'breakpoint' => '420',
               'slide_to_show' => '1',
               'slide_to_scroll' => '1',
               'dots' => 'false',
               'arrow' => 'false',
           );
           $responsive[] = array(
               'breakpoint' => '320',
               'slide_to_show' => '2',
               'slide_to_scroll' => '1',
               'dots' => 'false',
               'arrow' => 'false',
           );
            $this->dots = "false";
            $this->infinite = "true";
            $this->autoplay = false;
            $this->autoplay_speed = 5000;
            $this->slide_to_show = 4;
            $this->slide_to_scroll = 1;
            $this->responsive = $responsive;
        }
        
        public static function factory($id = "", $tag = "div") {
            return new CElement_IttronmallLastmenit_SlideDeal($id, $tag);
        }
        
        public function set_title($title){
            $this->title = $title;
            return $this;
        }

        public function set_see_all($bool){
            $this->see_all = $bool;
            return $this;
        }

        public function set_link_see_all($link){
            $this->link_see_all = $link;
            return $this;
        }
        
        public function set_using_title($bool){
            $this->using_title = $bool;
            return $this;
        }
        
        public function html($indent = 0) {
            $html = new CStringBuilder();

            $classes = $this->classes;
            $classes = implode(" ", $classes);
            if (strlen($classes) > 0) {
                $classes = " " . $classes;
            }

            $counter = 0;
            $see_all = '';
            if($this->see_all == true){
                if(count($this->slides)<=4){
                    $see_all = '<a class="deal-see-all" style="right: 14px !important;" href="'.$this->link_see_all.'">See All</a>';
                }else{
                    $see_all = '<a class="deal-see-all" href="'.$this->link_see_all.'">See All</a>';
                }
            }
            if($this->using_title){
                    $html->append('<div class="page-title"><span>'.$this->title.'</span>'.$see_all.'
                    <div class="ico-underline"></div>
                </div>');
            }
            $html->append('<div id="' . $this->id . '" class="' . $classes . '">');
            foreach ($this->slides as $k => $v) {
                $image_path = carr::get($v, 'image_path');
                //$image_url = image::get_url($image_path);
                $html->appendln('<div>');
                $html->appendln('   <div>');
//                $html->appendln('       <img class="img-responsive" src="' . $image_url . '"/>');
                $html->appendln(CElement_IttronmallLastmenit_Product::factory()->set_data_product($v)->set_class($classes)->html());
                $html->appendln('   </div>');
                $html->appendln('</div>');
                
            }
            $html->append('</div>');
            $html->appendln(parent::html($indent));

            return $html->text();
        }                
    }
    