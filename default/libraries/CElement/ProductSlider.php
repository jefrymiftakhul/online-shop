<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class CElement_ProductSlider extends CElement_SlickSlideShow
{
    public function __construct($id = "", $tag = "div")
    {
        parent::__construct($id, $tag);
        $this->responsive = prelove_home::lokal_responsive_size();
        $this->dots = "false";
        $this->page = 'product';
        $this->slide_to_scroll = 4;
        $this->slide_to_show = 4;
    }

    public static function factory($id = "", $tag = "div")
    {
        return new CElement_ProductSlider($id, $tag);
    }

    public function html($indent = 0)
    {
        $html = new CStringBuilder();

        $classes = $this->classes;
        $classes = implode(" ", $classes);
        if (strlen($classes) > 0) {
            $classes = " " . $classes;
        }

        $counter = 0;
        $html->append('<div id="' . $this->id . '" class="' . $classes . '">');
        foreach ($this->slides as $k => $v) {
            $html->appendln('<div>');
            $html->appendln('   <div>');
            $html->appendln(CElement_Card::factory()->set_product($v)->set_page($this->page)->html());
            $html->appendln('   </div>');
            $html->appendln('</div>');

        }
        $html->append('</div>');
        $html->appendln(parent::html($indent));

        return $html->text();
    }

}
