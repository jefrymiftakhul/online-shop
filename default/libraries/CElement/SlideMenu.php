<?php

    class CElement_SlideMenu extends CElement_SlickSlideShow{
        private $title = 'Slide';
        private $target = '';
        private $url_reload = '';
        public function __construct($id = "", $tag = "div") {
            parent::__construct($id, $tag);
            $this->url_reload = curl::base();
            $this->target = 'div';
            $this->dots = "false";
            $this->infinite = "true";
            $this->autoplay = false;
            $this->autoplay_speed = 5000;
            $this->auto_width = true;
        }
        
        public static function factory($id = "", $tag = "div") {
            return new CElement_SlideMenu($id, $tag);
        }
        
        public function set_title($title){
            $this->title = $title;
            return $this;
        }
        public function set_url_reload($url){
            $this->url_reload = $url;
            return $this;
        }
        
        public function set_target($target){
            $this->target = $target;
            return $this;
        }
        
        public function html($indent = 0) {
            $html = new CStringBuilder();

            $classes = $this->classes;
            $classes = implode(" ", $classes);
            if (strlen($classes) > 0) {
                $classes = " " . $classes;
            }
            $html->append('<ul id="' . $this->id . '" class="' . $classes . '">');
            foreach ($this->slides as $k => $v) {
                $label = carr::get($v,'label');
                $product_category_id = carr::get($v,'category_id');
                $html->appendln('<li>');
                $html->appendln('   <a href="#" data-category-id="'.$product_category_id.'">');
                $html->appendln('       '.$label);
                $html->appendln('   </a>');
                $html->appendln('</li>');
                
            }
            $html->append('</ul>');
            $html->appendln(parent::html($indent));

            return $html->text();
        }   
        
        public function js($indent = 0) {
            $js = new CStringBuilder();
            $js->append("
                $('#".$this->id." li a').click(function(e){
                    e.preventDefault();
                    var cat_id = $(this).attr('data-category-id');
                    $.cresenity.reload('".$this->target."','".$this->url_reload."/'+cat_id);
                });
            ");
            $js->append(parent::js($indent));
            return $js->text();
        }
    }
    