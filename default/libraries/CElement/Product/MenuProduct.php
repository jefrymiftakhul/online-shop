<?php

class CElement_Product_MenuProduct extends CElement {

    private $image = NULL;
    private $list_product = array();
    private $count_product = 8;
    private $count_item_per_row = 4;
    private $location_detail = NULL;
    private $product_type_name = 'product';

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_Product_MenuProduct($id);
    }

    public function set_product_type_name($name) {
        $this->product_type_name = $name;
        return $this;
    }

    public function set_image($image) {
        $this->image = $image;
        return $this;
    }

    public function set_list_product($list) {
        $this->list_product = $list;
        return $this;
    }

    public function set_location_detail($location_detail) {
        $this->location_detail = $location_detail;
        return $this;
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);

        //$html->appendln('<div class="col-md-12 margin-top-10" style="padding-left:0px; padding-right:0px;">');
        $container = $this->add_div()->add_class('col-md-12 padding-0');
        
        $count_list = count($this->list_product);
        if ($count_list < $this->count_product) {
            $this->count_product = count($this->list_product);
        }

        $location_detail = $this->location_detail;
        if (empty($location_detail)) {
            $location_detail = curl::base() . 'product/item/';
            if ($this->product_type_name == 'service') {
                $location_detail = curl::base() . 'service/item/';
            }
        }

        $index = 0;
        for ($product = 0; $product < $this->count_product; $product++) {
            if ($index % 4 == 0 or $index == 0) {
                $container_product = $container->add_div()->add_class('row');
            }

            $product_id = !empty($this->list_product[$product]['product_id']) ? $this->list_product[$product]['product_id'] : NULL;
            $url_key = !empty($this->list_product[$product]['url_key']) ? $this->list_product[$product]['url_key'] : NULL;

            $image_url = !empty($this->list_product[$product]['image_path']) ? $this->list_product[$product]['image_path'] : NULL;
            $name = !empty($this->list_product[$product]['name']) ? $this->list_product[$product]['name'] : NULL;
            $is_hot = !empty($this->list_product[$product]['is_hot_item']) ? $this->list_product[$product]['is_hot_item'] : 0;

            $flag = $hot = ($is_hot > 0) ? 'Hot' : NULL;


            if (!empty($this->list_product[$product]['new_product_date'])) {
                if (strtotime($this->list_product[$product]['new_product_date']) >= strtotime(date('Y-m-d'))) {
                    $flag = $new = 'New';
                }
            }

            $promo = !empty($this->list_product[$product]['promo']) ? $this->list_product[$product]['promo'] : NULL;
            $stock = !empty($this->list_product[$product]['stock']) ? $this->list_product[$product]['stock'] : 0;
            $min_stock = !empty($this->list_product[$product]['show_min_stock']) ? $this->list_product[$product]['show_min_stock'] : 0;
            $price = !empty($this->list_product[$product]['detail_price']) ? $this->list_product[$product]['detail_price'] : array();
            $is_available = $this->list_product[$product]['is_available'];

            $key = $url_key;
            if($key==null) $key = $product_id;
            
            $element_product = CElement_Product_Product::factory();
            $element_product->set_key($key)
                    ->set_location_detail($location_detail)
                    ->set_image($image_url)
                    ->set_column($this->count_item_per_row)
                    ->set_name($name)
                    ->set_flag($flag)
                    ->set_label_flag($flag)
                    ->set_price($price)
                    ->set_min_stock($min_stock)
                    ->set_stock($stock)
                    ->set_available($is_available);

            $element_html = $element_product->html();

            $container_product->add($element_html);

            //$html->appendln($element_html);
            $index++;
        }

        //$html->appendln('</div>');

        $html->append(parent::html(), $indent);
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $js->appendln("
                jQuery('.menu-link-" . $this->id . "').on('click', function(){
                    var id = jQuery(this).attr('id');
                    jQuery('.menu-link-" . $this->id . "').removeClass('active');
                    jQuery('#'+id).addClass('active');
                });
                ");

        $js->append(parent::js($indent));
        return $js->text();
    }

}
