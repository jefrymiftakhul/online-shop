<?php

class CElement_Product_MenuHorizontal extends CElement {

    private $menu = array();
    private $active = NULL;
    private $target = NULL;
    private $target_image = NULL;
    private $product_type_name = 'product';

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_Product_MenuHorizontal($id);
    }

    public function set_product_type_name($name) {
        $this->product_type_name = $name;
        return $this;
    }

    public function set_menu($menu) {
        $this->menu = $menu;
        return $this;
    }

    public function set_target($target) {
        $this->target = $target;
        return $this;
    }

    public function set_target_image($target_image) {
        $this->target_image = $target_image;
        return $this;
    }
    
    public function build() {
        $container = $this->add_div()->add_class('container-product-menu-horizontal');
        $row_header = $this->add_div()->add_class('row row-header');
        $header = $row_header->add_div()->add_class('col-md-12');
        $row_body = $this->add_div()->add_class('row row-body');
        
        
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);

        
        $this->build();
        $html->append(parent::html(), $indent);
        $title = NULL;
        if (count($this->menu) > 0) {
            $title = $this->menu[0]['name_category'];
        }

        $menu_container = $this->add_div()->add_class('menu-product');
        $menu_header = $menu_container->add_div()->add_class('bold font-red font-big d-pull-right')->add($title);

        $menu_sub = $menu_container->add_div()->add_class('d-pull-right col-md-12 padding-right0 sub-category-menu');
        $url = "home/get_product_category/";
        $url_image = "home/get_product_category_image/";
        if ($this->product_type_name == 'service') {
            $url = "service/home/get_service_category/";
            $url_image = "service/home/get_service_category_image/";
        }
        if (count($this->menu) > 0) {
            $no = 1;
            $active = "";
            $list = array();
            foreach ($this->menu as $data_menu) {
                $menu_id = carr::get($data_menu, 'cms_product_product_category_id');
                $product_category = carr::get($data_menu, 'product_category', NULL);

                if ($no == 1)
                    $active = 'active';
                else {
                    $active = '';
                }
                $menu = $menu_sub->add_div($this->id . '-' . $no)->add_class('menu-id-' . $menu_id . ' d-text-right menu-link ' . $active)->set_attr('data-menu_id', $menu_id)
                        ->add('<span>' . trim(htmlspecialchars($product_category)) . '</span> &nbsp;<i class="fa fa-angle-right"></i>');

                $list[$menu_id] = $product_category;

                $listener = $menu->add_listener('click');
                $listener->add_handler('custom')
                        ->set_js("
                        $.cresenity.reload('" . $this->target . "','" . curl::base() . $url . $menu_id . "','get',{});
                        $.cresenity.reload('" . $this->target_image . "','" . curl::base() . $url_image . $menu_id . "','get',{});    
                        var menu_id = $(this).attr('data-menu_id');
                        $('#" . $this->id . "_select').val(menu_id);
                    ");

                $no++;
            }
            $control = $menu_container->add_div()->add_class('form-62hallfamily')->add_control($this->id . '_select', 'select')->add_class('select-category-menu select-62hallfamily')->set_list($list);
            $listener = $control->add_listener('change');
            $listener->add_handler('custom')
                    ->set_js("
                        $.cresenity.reload('" . $this->target . "','" . curl::base() . $url . "','get',{'category_id':$('#" . $this->id . "_select').val()});
                        $.cresenity.reload('" . $this->target_image . "','" . curl::base() . $url_image . "','get',{'category_id':$('#" . $this->id . "_select').val()});    
                        $(this).parent().find('div.menu-link').removeClass('active');
                        $(this).parent().find('div.menu-id-'+$('#" . $this->id . "_select').val()).addClass('active');
                    ");
        }

        
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $js->appendln("
                jQuery('.menu-link').on('click', function(){
                    var div_parent = $(this).parents('div');
                    div_parent.find('.menu-link').each(function(){
                        jQuery(this).removeClass('active');
                    });
                    
                    var id = jQuery(this).attr('id');
                    jQuery('#'+id).addClass('active');
                });
                ");

        $js->append(parent::js($indent));
        return $js->text();
    }

}
