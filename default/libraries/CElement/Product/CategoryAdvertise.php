<?php

class CElement_Product_CategoryAdvertise extends CElement{
    
    private $list_advertise = array();
    // private $image_size = 33;

    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public static function factory($id = ''){
        return new CElement_Product_CategoryAdvertise($id);
    }
    
    public function set_list_advertise($list_advertise){
        $this->list_advertise = $list_advertise;
        return $this;
    }
    
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        
        $div_category_advertise = $this->add_div()->add_class('category-advertise margin-bottom-20 padding-0');
        
        foreach ($this->list_advertise as $data_image){
            $image = carr::get($data_image, 'image_url', NULL);
            $col_span = $data_image['col_span'];
            
            //$width = $this->image_size * $col_span;
            $width = $col_span * 33;
            $class = "col-md-" . $width;
            
            //$div_category_advertise->add('<img src="' . $image  . '" style="width:' . $width . '%" />');
            $div_category_advertise->add('<img src="' . $image  . '" class="padding-0" style="height: 380px; width: '.$width.'%" />');
        }
        
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent=0) {
       $js = new CStringBuilder();

        $js->append(parent::js($indent));
        return $js->text();
    }
}
