<?php

class CElement_Product_ProductCart extends CElement {

    private $key;
    private $cart_list = array();
    private $count_item;
    private $shipping_price;

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_Product_ProductCart($id);
    }

    public function set_key($key) {
        $this->key = $key;
        return $this;
    }

    public function set_cart_list($cart_list) {
        $this->cart_list = $cart_list;
        return $this;
    }

    public function set_count_item($count_item) {
        $this->count_item = $count_item;
        return $this;
    }

    public function set_shipping_price($shipping_price) {
        $this->shipping_price = $shipping_price;
        return $this;
    }

    private function get_image($image_name) {
        $ch = curl_init(image::get_image_url($image_name, 'view_all'));
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_exec($ch);
        $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($retcode == 500 or $image_name == NULL) {
            $image_name = curl::base() . 'application/ittronmall/default/media/img/product/no-image.png';
        } else {
            $image_name = image::get_image_url($image_name, 'view_all');
        }
        curl_close($ch);
        return $image_name;
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        $db = CDatabase::instance();

        $cart = Cart62hall_Product::factory();
        $cart_list = $cart->get_cart();
        $count_item = $cart->item_count();

        if (count($this->cart_list) > 0) {
            $cart_list = $this->cart_list;
        }
        if ($this->count_item > 0) {
            $count_item = $this->count_item;
        }

        $div_shopping_cart = $this->add_div()->add_class('col-md-12 container-cart margin-bottom-20');
        $total_harga = 0;
        $shipping_price = '-';
        if (strlen($this->shipping_price) > 0) {
            $shipping_price = $this->shipping_price;
        }
        if ($count_item > 0) {
            $items = $div_shopping_cart->add_div()->add_class('horizontal-overflow');
            foreach ($cart_list as $data_cart) {
                $product_id = $data_cart['product_id'];
                if (strlen($product_id) > 0) {
                    $total_harga += $data_cart['sub_total'];
                    //$image = $this->get_image($data_cart['image']);
//                    $image = $data_cart['image'];
                    //if (ccfg::get('is_direct_image')) {
                        $image = $data_cart['image'];
                    //}

//                    cdbg::var_dump($image);

                        
                    $container_item = $items->add_div()->add_class('container-item');
                    $container_item_left = $container_item->add_div()->add_class('container-item-left');
                    $container_item_right = $container_item->add_div()->add_class('container-item-right');
                    $container_item_left->add_div()->add_class('container-item-qty')->add_div()->add_class('product-cart-qty')->add($data_cart['qty']);
                    $wrapper_image = $container_item_left->add_div()->add_class('outside-element-center-vertical wrapper-image');
//                    $wrapper_image->add('<span class="helper-image"></span>');
                    $wrapper_image->add("<img class='element-center-vertical' src='" . $image . "'>");
                    $content_right = $container_item_right->add_div()->add_class('content-item-right');
                    $content_right->add_div()->add_class('product-contact-title')->add($data_cart['name']);
                            
                $html_attribute = '';
                $html_attribute = '<table class="table-product-attribute">';
               
                if ( !empty($data_cart['attribute']) and is_array($data_cart['attribute']) ) {
                    foreach ( $data_cart['attribute'] as $cat_id => $value ) {
                          
                        $att_cat_name = cdbutils::get_value('select name from attribute_category where attribute_category_id=' . $db->escape($cat_id));
                        $attribute_key = attribute::get_attribute_key($value);
                        $html_attribute .= '<tr>';
                        $html_attribute .= '    <td>' . $att_cat_name . '</td>';
                        $html_attribute .= '    <td>&nbsp;:&nbsp;</td>';
                        $html_attribute .= '    <td>' . $attribute_key . '</td>';
                        $html_attribute .= '</tr>';
                     }
                }
                $html_attribute .= '</table>';
                $content_right->add_div()->add_class('product-attribute')->add($html_attribute);

                    $container_price_qty = $content_right->add_div()->add_class('container-price-qty');
                    $container_price_qty->add_div()->add_class('product-contact-total')->add('Qty : ' . $data_cart['qty']);
                    $container_price_qty->add_div()->add_class('product-contact-price')->add_class('text-right')->add('Rp ' . ctransform::format_currency($data_cart['sell_price']));
                } else {
                    $shipping_price = $data_cart['sell_price'];
                }
            }

            $container_subtotal = $items->add_div()->add_class('container-contact-subtotal');
            $container_subtotal_row = $container_subtotal->add_div()->add_class('row');
            $container_subtotal_left = $container_subtotal_row->add_div()->add_class('col-xs-6')->add(clang::__('Subtotal'));
            $container_subtotal_right = $container_subtotal_row->add_div()->add_class('col-xs-6 text-right bold')->add('Rp ' . ctransform::format_currency($total_harga));

            $container_subtotal_row = $container_subtotal->add_div()->add_class('row');
            $container_subtotal_left = $container_subtotal_row->add_div()->add_class('col-xs-6')->add(clang::__('Pengiriman'));
            $container_subtotal_right = $container_subtotal_row->add_div('shipping-cost')->add_class('col-xs-6 text-right bold')->add('Rp ' . ctransform::format_currency($shipping_price));

            if ($shipping_price != '-') {
                $total_harga = $total_harga + $shipping_price;
            }

            $container_grand_total = $items->add_div()->add_class('container-grand-total');
            $grand_total_row = $container_grand_total->add_div()->add_class('row');
            $grand_total_left = $grand_total_row->add_div()->add_class('col-xs-6 txt-total')->add(clang::__('Total'));
            $grand_total_right = $grand_total_row->add_div('total-payment')->add_class('col-xs-6 txt-total-price')->add('Rp ' . ctransform::format_currency($total_harga));
        }

        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $js->append(parent::js($indent));
        return $js->text();
    }

}
