<?php

class CElement_Product_HomeProduct extends CElement{
    
    private $key;


    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public static function factory($id = ''){
        return new CElement_Product_HomeProduct($id);
    }
    
    public function set_key($key){
        $this->key = $key;
        return $this;
    }
    
    
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
       
        if($this->flag == $this->product_new){
            $this->image_flag = "flag-red.png";
        }
        else if($this->flag == $this->product_hot){
            $this->image_flag = "flag-yellow.png";
        }
        
        if (empty($this->column)){
            $col = 3;
        }
        else{
            $col = 12 / $this->column;
        }
        
        $html->appendln("<div class='col-md-" . $col . "'>");
        $html->appendln("<div class='product'>");
        $image = curl::base() . "application/62hallfamily/default/media/img/products/" . $this->image;
        $html->appendln("<a href='" . curl::base() . 'product/item/' . $this->key . "'><img width='100%' class='padding' src='" . $image . "'/></a>");
        
        if(!empty($this->promo)){
            $image_promo = curl::base() . "application/62hallfamily/default/media/img/62hallfamily/promo.png";
            $html->appendln("<div class='promo font-white bold italic'><img width='100%' src='" . $image_promo . "' /><div class='promo-text small'>" . $this->promo . " %<br>Off</div></div>");
        }
        
        $image_flag = curl::base() . "application/62hallfamily/default/media/img/62hallfamily/" . $this->image_flag;
        $html->appendln("<div class='name font-gray small'>" . $this->name . "</div>");
        
        if(!empty($this->image_flag)){
            $html->appendln("<div class='flag small'><img width='100%' src='" . $image_flag . "' /><div class='flag-label bold italic font-white'><center>" . $this->flag . "<br> Products</center></div></div>");
        }
        
        if(!empty($this->price_promo)){
            $html->appendln("<div class='price'><strike class='small font-gray'>" . $this->price . "</strike><div class='bold font-black'>" . $this->price_promo .  "</div></div>");
        }
        else{
            $html->appendln("<div class='bold font-black'>" . $this->price .  "</div></div>");
        }
        
        $html->appendln("</div></div>");
        
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent=0) {
       $js = new CStringBuilder();

        $js->append(parent::js($indent));
        return $js->text();
    }
}
