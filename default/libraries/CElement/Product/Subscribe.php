<?php

class CElement_Product_Subscribe extends CElement{
    
    private $type = 1;

    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public function set_type($type){
        $this->type = $type;
        return $this;
    }

    public static function factory($id = ''){
        return new CElement_Product_Subscribe($id);
    }
    
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        
        //$form = $this->add_form()->add_class('form-62hallfamily');
        $container = $this->add_div()->add_class('form-62hallfamily col-md-12 padding-0');
        $container->add_control('type','hidden')->set_value($this->type);
        $message = $container->add_div('message-subscribe');
        
        $input_subscribe = $container->add_div()->add_class('col-md-9 col-xs-11 padding-0');
        $input_subscribe->add_control("email_subscribe", 'text')
            ->set_name("email_subscribe")
            ->set_placeholder('Masukkan email Anda')
            ->add_class("form-control");
        
        $action_subscribe = $container->add_div()->add_class('col-md-2 col-xs-1 padding-0');
//                    ->custom_css('margin-left', '-7px !important');
        $action_subscribe->add_action()
                    ->set_label(clang::__('Daftar'))
                    ->add_class('btn-register-subscribe btn-62hallfamily bg-red font-white border-3-red pull-right upper')
//                    ->custom_css('height', '35px')
                    ->add_listener('click')
                    ->add_handler('reload')
                    ->add_param_input(array('email_subscribe','type'))
                    ->set_target('message-subscribe')
                    ->set_url(curl::base() . 'subscribe/user_subscribe/');
        
        
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent=0) {
        $js = new CStringBuilder();
        
        $js->appendln("
                $('input').blur();
                $('.btn-register-subscribe').click(function(){
                   $('#email_subscribe').focus(); 
                });
                ");
        
        $js->append(parent::js($indent));
        return $js->text();
    }
}
