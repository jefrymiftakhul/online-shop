<?php

class CElement_Product_MenuProductResponsive extends CElement {

    private $list_product = array();
    private $location_detail = NULL;
    private $product_type_name = 'product';
    private $margin=10;

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_Product_MenuProductResponsive($id);
    }

    public function set_list_product($list) {
        $this->list_product = $list;
        return $this;
    }
    public function set_margin($margin) {
        $this->margin = $margin;
        return $this;
    }

    public function set_location_detail($location_detail) {
        $this->location_detail = $location_detail;
        return $this;
    }

    public function set_product_type_name($name) {
        $this->product_type_name = $name;
        return $this;
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);

        $location_detail = $this->location_detail;
        if (empty($location_detail)) {
            $location_detail = curl::base() . 'product/item/';
            if ($this->product_type_name == 'service') {
                $location_detail = curl::base() . 'service/item/';
            }
            if ($this->product_type_name == 'shinjuku') {
                $location_detail = curl::base() . 'shinjuku/item/';
            }
        }

        $container_slide = $this->add_div()->add_class("col-md-12");
        $container_slide_content = $container_slide->add_div()->add_class("row");

        $box_slider = $container_slide_content->add_div()->add_class("owl-carousel sixty-slider");
        $i = 0;
        if($this->product_type_name=='k24'){
            for($i=0;$i<16;$i++){
                $list_product_k24[]=$this->list_product[$i];
            }
            $this->list_product=$list_product_k24;
        }
        if (count($this->list_product) > 0) {
            foreach ($this->list_product as $key => $value) {
                $product_id = carr::get($value, 'product_id');
                $url_key = carr::get($value, 'url_key');
                $image_url = carr::get($value, 'image_path');
                $name = carr::get($value, 'name');
                $flag = $hot = (carr::get($value, "is_hot_item") > 0) ? 'Hot' : NULL;
                
                if(!empty(carr::get($value, 'new_product_date'))){
                    if(strtotime(carr::get($value, 'new_product_date')) >= strtotime(date('Y-m-d'))){
                        $flag = $new = 'New';
                    }
                }   
                $promo = carr::get($value, 'promo');
                $stock = carr::get($value, 'stock');
                $min_stock = carr::get($value, 'show_min_stock');
                $price = carr::get($value, 'detail_price');
                $is_available = carr::get($value, 'is_available');
                if($this->product_type_name=='k24'){
                    $name = carr::get($value, 'name');
                    $image_url='http://www.k24klik.com/images/product/'.carr::get($value,'image');
                    $is_available=1;
                    $price['price']=carr::get($value, 'price');
                    $price['promo_price']=0;
                }
                $key = $url_key;
                if($key==null) $key = $product_id;

                $element_product = CElement_Product_Product::factory();
                $element_product->set_key($url_key)
                        ->set_slide_responsive(true)
                        ->set_image($image_url)
                        ->set_location_detail($location_detail)
                        ->set_name($name)
                        ->set_flag($flag)
                        ->set_label_flag($flag)
                        ->set_price($price)
                        ->set_stock($stock)
                        ->set_min_stock($min_stock)
                        ->set_product_type($this->product_type_name)
                        ->set_available($is_available);
                
                $element_html = $element_product->html();

                if ($i % 2 == 0) {
                    $new_slider = $box_slider->add_div()->add($element_html)->add_class("owl-item box-item");
                    $old_slider = $new_slider;
                }
                else {
                    $old_slider->add_div()->add($element_html);
                }
                $i++;
            }
        }
        $html->append(parent::html(), $indent);
        return $html->text();

    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $js->appendln("
                jQuery('.menu-link-" . $this->id . "').on('click', function(){
                    var id = jQuery(this).attr('id');
                    jQuery('.menu-link-" . $this->id . "').removeClass('active');
                    jQuery('#'+id).addClass('active');
                });
                ");

        $js->appendln("
            $('.owl-carousel').owlCarousel({
                loop:false,
                margin:".$this->margin.",
                autoplay:true,
                autoplayTimeout:5000, 
                dots: true,
                nav:false,
                responsive:{
                    0:{
                        items:1
                    },
					450:{
						items:2
					},
                    600:{
                        items:2
                    },
                    991:{
                        items:2
                    },
                    992:{
                        items:3
                    },
                    1200:{
                        items:4
                    }
                }
            });
            ");

        $js->append(parent::js($indent));
        return $js->text();
    }

}
