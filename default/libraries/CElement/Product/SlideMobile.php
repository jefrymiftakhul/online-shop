<?php

class CElement_Product_SlideMobile extends CElement {

    protected $list_product = array();

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public function set_list_item($list) {
        $this->list_product = $list;
        return $this;
    }

    public static function factory($id = '') {
        return new CElement_Product_SlideMobile($id);
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);

        // cdbg::var_dump($this->list_product);

        $container_slide = $this->add_div()->add_class("col-md-12");
        $container_slide_content = $container_slide->add_div()->add_class("row");

        $box_slider = $container_slide_content->add_div($this->id . "_slider")->add_class("carousel carousel-showsixmoveone slide");
        if (!is_array($this->list_product)) {
            $this->list_product = array();
        }
        $container_content = $box_slider->add_div()->add_class('carousel-inner');
        $i=0;
        foreach ($this->list_product as $key => $value) {
            $product_id = carr::get($value, 'product_id');
            $image_url = carr::get($value, 'image_path');
            $name = carr::get($value, 'name');
            $flag = $hot = (carr::get($value, "is_hot_item") > 0) ? 'Hot' : NULL;

            if (!empty(carr::get($value, 'new_product_date'))) {
                if (strtotime(carr::get($value, 'new_product_date')) >= strtotime(date('Y-m-d'))) {
                    $flag = $new = 'New';
                }
            }

            $promo = carr::get($value, 'promo');
            $stock = carr::get($value, 'stock');
            $min_stock = carr::get($value, 'show_min_stock');
            $price = carr::get($value, 'detail_price');
            $is_available = carr::get($value, 'is_available');


            $element_product = CElement_Product_Product::factory();
            $element_product->set_key($product_id)
                    ->set_slide_responsive(true)
                    ->set_image($image_url)
                    ->set_name($name)
                    ->set_flag($flag)
                    ->set_label_flag($flag)
                    ->set_price($price)
                    ->set_stock($stock)
                    ->set_min_stock($min_stock)
                    ->set_available($is_available);

            $element_html = $element_product->html();
            $active = '';
            if($i==0) $active=" active";
            $item_content = $container_content->add_div()->add_class("item".$active);
            $col_content = $item_content->add_div()->add_class('col-xs-12 col-sm-4 col-md-2');
            
            $col_content->add_div()->add($element_html);
            $i++;
        }


        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $js->appendln("
            (function(){
               
                jQuery('#" . $this->id . "_slider" . "').carousel({ interval: 2000 });
                
              }());

              (function(){
                
                    $('#" . $this->id . "_slider" . " .item').each(function(){
                      var itemToClone = jQuery(this);

                      for (var i=1;i<6;i++) {
                        itemToClone = itemToClone.next();

                        // wrap around if at end of item collection
                        if (!itemToClone.length) {
                          itemToClone = jQuery(this).siblings(':first');
                        }

                        // grab item, clone, add marker class, add to collection
                        itemToClone.children(':first-child').clone()
                          .addClass('cloneditem-'+(i))
                          .appendTo($(this));
                      }
                    });
              
              }());
            ");


        $js->append(parent::js($indent));
        return $js->text();
    }

}
