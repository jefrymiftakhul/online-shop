<?php

class CElement_Product_Product extends CElement {

    private $key;
    private $name = NULL;
    private $image;
    private $column = NULL;
    private $flag = NULL;
    private $label_flag = NULL;
    private $stock = 0;
    private $product_hot = "Hot";
    private $product_new = "New";
    private $location_detail = NULL;
    private $min_stock = 0;
    private $price = 0;
    private $is_available = 1;

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_Product_Product($id);
    }

    public function set_key($key) {
        $this->key = $key;
        return $this;
    }

    public function set_image($image) {
        $this->image = $image;
        return $this;
    }

    public function set_flag($flag) {
        $this->flag = ucfirst(strtolower($flag));
        return $this;
    }

    public function set_label_flag($set_flag) {
        $this->label_flag = ucfirst(strtolower($set_flag));
        return $this;
    }

    public function set_name($name) {
        $this->name = $name;
        return $this;
    }

    public function set_price($price) {
        $this->price = $price;
        return $this;
    }

    public function set_stock($stock) {
        $this->stock = $stock;
        return $this;
    }

    public function set_column($col) {
        $this->column = $col;
        return $this;
    }

    public function set_location_detail($location_detail) {
        $this->location_detail = $location_detail;
        return $this;
    }

    public function set_min_stock($min_stock) {
        $this->min_stock = $min_stock;
        return $this;
    }

    public function set_available($val) {
        $this->is_available = $val;
        return $this;
    }

    private function get_image() {
        $ch = curl_init(image::get_image_url($this->image, 'view_all'));
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_exec($ch);
        $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($retcode == 500 or $this->image == NULL) {
            $this->image = curl::base() . 'application/62hallfamily/default/media/img/product/no-image.png';
        } else {
            $this->image = image::get_image_url($this->image, 'view_all');
        }
        curl_close($ch);
        return $this->image;
    }

    private function get_price() {
        $detail_price = $this->price;

        $promo_price = 0;
        $price = 0;

        if (!empty($detail_price['ho_sell_price']) and ! empty($detail_price['channel_sell_price'])) {
            if ($detail_price['ho_sell_price'] > $detail_price['channel_sell_price']) {
                $price = $detail_price['ho_sell_price'];
                $promo_price = $detail_price['channel_sell_price'];
            } else {
                $price = $detail_price['channel_sell_price'];
            }
        }

        return array('promo_price' => $promo_price, 'price' => $price);
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);

        $detail_price = $this->price;
        $data_price = $this->get_price();

        if ($this->flag == $this->product_new) {
            $flag = 'red';
        } else if ($this->flag == $this->product_hot) {
            $flag = 'yellow';
        }

        if (empty($this->column)) {
            $col = 3;
            $col_small = 2;
            $col_xsmall = 2;
        } else {
            $col = 12 / $this->column;
            $col_small = 24 / $this->column;
            $col_xsmall = 36 / $this->column;
        }

        if ($col_xsmall > 6 && $col_xsmall <= 9) {
            $col_xsmall = 6;
        }
        if ($col_xsmall > 9) {
            $col_xsmall = 12;
        }

        $container = $this->add_div()->add_class('container product-container col-xs-' . $col_xsmall . ' col-sm-' . $col_small . ' col-md-' . $col);
        $product = $container->add_div()->add_class('product margin-bottom-10');

        $image = $this->get_image();

        $location_detail = $this->location_detail;
        if (empty($location_detail)) {
            $location_detail = curl::base() . 'product/item/';
        }

        $product->add('<a href="' . $location_detail . $this->key . '"><img src="' . $image . '" width="100%"></a>');

        if (isset($detail_price['promo_text']) and $detail_price['promo_text'] > 0) {
            $promo = $product->add_div()
                    ->add_class('promo font-white bold italic');
            $promo_text = $promo->add_div()
                    ->add_class('promo-text');
            $promo_text->add(round($detail_price['promo_text'], 1) . "<br>%");
        }

        if ($this->stock < $this->min_stock) {
            $remain = $product->add_div()
                    ->add_class('remain font-white bold font-small');

            if (!empty($this->flag)) {
                $remain->custom_css('bottom', '60px');
            }

            if ($detail_price['promo_text'] > 0) {
                $remain->custom_css('bottom', '73px');
            }

            $remain_text = $remain->add_div();
            $remain_text->add('Remain ' . $this->stock);
        }

        $bottom = '24px';
        if (!empty($this->flag)) {
            $bottom = '24px';
        }

        if ($data_price['promo_price'] > 0) {
            $bottom = '37px';
        }

        $name = $this->name;
        if (strlen($this->name) > 40) {
            $name = substr($this->name, 0, 40) . '...';
        }

        $product->add_div()->add_class('name font-gray small')
                ->add('<div style="position:absolute; width:165px; bottom:' . $bottom . '">' . htmlspecialchars($name) . '</div>');

        if (!empty($this->flag)) {
            $flag = $product->add_div()->add_class('flag-' . $flag . ' small');
            $flag->add_div()->add_class('flag-label bold italic font-white')
                    ->add('<center>' . $this->label_flag . '</center>');
        }


        $container_price = $product->add_div()->add_class('col-md-9 pull-right padding-0');

        if ($this->is_available) {
            if ($data_price['promo_price'] > 0) {
                $price = $container_price->add_div()->add_class('bold font-gray font-size12 text-right bottom-product-price')
                        ->add('<strike>Rp. ' . ctransform::format_currency($data_price['price']) . '</strike>');

                $promo_price = $container_price->add_div()->add_class('bold font-black text-right bottom-product-price bottom-product-price-promo')
                        ->add('Rp. ' . ctransform::format_currency($data_price['promo_price']));
            } else {
                $price = $container_price->add_div()->add_class('bold font-black text-right bottom-product-price')
                        ->add('Rp. ' . ctransform::format_currency($data_price['price']));
            }
        } else {
            $price = $container_price->add_div()->add_class('bold font-black text-right bottom-product-price')
                    ->add('By Request');
        }

        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $js->append(parent::js($indent));
        return $js->text();
    }

}
