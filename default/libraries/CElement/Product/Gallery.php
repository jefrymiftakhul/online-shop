<?php

class CElement_Product_Gallery extends CElement {

    protected $images = array();	
	protected $page ='product';

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_Product_Gallery($id);
    }

    public function set_images($images) {
        $this->images = $images;
        return $this;
    }

    public function set_page($val) {
        $this->page = $val;
        return $this;
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);

        $container = $this->add_div()->add_class('container-gallery');
        $gallery_list = $container->add_div()->add_class('gallery-list');
        $gallery_detail = $container->add_div()->add_class('text-center gallery-image img-responsive');
        
        $first_image = '';
        foreach ($this->images as $key => $value) {
                $image_path = carr::get($value, 'image_path');
                $is_active = '';
                if ($key == 0) {
                    $first_image = $image_path;
                    $is_active = ' active ';
                }

                $gallery_list->add_div()->add_class('outside-element-center-vertical gallery-list-image'.$is_active)->add("<img class='element-center-vertical' src='".$image_path."'>");
            // }
        }

        $gallery_detail->add("<img src='".$first_image."'>");


        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $script = "
            jQuery('.gallery-list-image').click(function() {
                jQuery('.gallery-list-image').removeClass('active');
                jQuery(this).addClass('active');
                jQuery('.gallery-image img').attr('src', jQuery(this).find('img').attr('src'));
            });
        ";

        $js->append($script);

        $js->append(parent::js($indent));
        return $js->text();
    }

}
