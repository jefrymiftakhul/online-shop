<?php

    /**
     *         
     * @author Raymond Sugiarto
     * @since  May 10, 2016
     * @license http://ittron.co.id ITtron
     */
    class CElement_FormRequest extends CElement {

        public function __construct($id = "", $tag = "div") {
            parent::__construct($id, $tag);
        }

        public static function factory($id = "", $tag = "div") {
            return new CElement_FormRequest($id, $tag);
        }
        
        protected function do_process(){
            $post = $_POST;
            if (isset($post['send_request'])) {
                $db = CDatabase::instance();
                $session = Session::instance();
                
                $org_id = CF::org_id();
                $member_id = $session->get('member_id');
                $name = carr::get($post, 'name');
                $email = carr::get($post, 'email');
                $phone = carr::get($post, 'phone');
                $product_request = carr::get($post, 'product_request');
                
                $err_code = 0;
                $err_message = '';
                if ($err_code == 0) {
                    if (strlen($name) == 0) {
                        $err_code++;
                        $err_message = clang::__('Nama harus diisi');
                    }
                }
                if ($err_code == 0) {
                    if (strlen($email) == 0) {
                        $err_code++;
                        $err_message = clang::__('Email harus diisi');
                    }
                }
                if ($err_code == 0) {
                    if (!cvalid::email($email)) {
                        $err_code++;
                        $err_message = clang::__('Email anda tidak valid');
                    }
                }
                if ($err_code == 0) {
                    if (strlen($phone) == 0) {
                        $err_code++;
                        $err_message = clang::__('No Telp harus diisi');
                    }
                }
                if ($err_code == 0) {
                    if (strlen($product_request) == 0) {
                        $err_code++;
                        $err_message = clang::__('Produk yang diinginkan harus diisi');
                    }
                }
                if ($err_code == 0) {
                    $data_request = array(
                        'org_id' => $org_id,
                        'member_id' => $member_id,
                        'customer_name' => $name,
                        'customer_email' => $email,
                        'phone' => $phone,
                        'description' => $product_request,
                        'created' => date('Y-m-d H:i:s'),
                        'createdby' => $name,
                        'updated' => date('Y-m-d H:i:s'),
                        'updatedby' => $name,
                        'status' => '1'
                    );
                    $db->insert('request_product', $data_request);
                    $message = clang::__('Terima kasih telah melakukan request produk kepada kami. Request anda segera kami proses.');
                    $message .= ' ' .clang::__('Silahkan') .' ';
                    $message .= '<a href="' .curl::base() .'" class="os-link">' .clang::__('Lanjutkan Belanja') .'</a>';
                    cmsg::add('success', $message);
                    return true;
                }
                else {
                    cmsg::add('error', $err_message);
                }
                return false;
            }
        }

        public function html($indent = 0) {
            $html = CStringBuilder::factory();
            
            $post = $_POST;
            
            $name = '';
            $email = '';
            $phone = '';
            $product_request = '';
            $member_id = '';
            
            if (isset($post['send_request'])) {
                $name = carr::get($post, 'name');
                $email = carr::get($post, 'email');
                $phone = carr::get($post, 'phone');
                $product_request = carr::get($post, 'product_request');
            }
            if ($this->do_process()) {
                $name = '';
                $email = '';
                $phone = '';
                $product_request = '';
            }

            $session = Session::instance();
            $member_id = $session->get('member_id');
            if (strlen($member_id) > 0) {
                $db = CDatabase::instance();
                $q = 'SELECT name, email, phone 
                        FROM member 
                        WHERE status > 0 
                            AND member_id = '.$db->escape($member_id);
                $data = cdbutils::get_row($q);

                if (count($data) > 0) {
                    $name = cobj::get($data, 'name');
                    $email = cobj::get($data, 'email');
                    $phone = cobj::get($data, 'phone');
                }
            }
            
            $request_container = $this->add_div()->add_class('form-request alert alert-warning fade in alert-dismissable');
            $msg = cmsg::get('success');
            $request_container->add_div()->add(cmsg::flash_all());
            // if (!$msg) {
                $form_request_info = $request_container->add_div()->add_class('form-request-info');
                $form_request_info->add(clang::__('<span style="color:red;">PENCARIAN TIDAK TERSEDIA</span>'));
                $form_request_info->add_br();
                $form_request_info->add(clang::__('Maaf produk yang Anda cari tidak tersedia. Silahkan coba pencarian yang lain atau kembali ke halaman utama'));
                
                $this->add_action()
                        ->set_label(clang::__('Home'))
//                        ->set_attr('', $v)
                        ->set_link(curl::base().'home')
                        ->add_class('btn btn-primary btn-imlasvegas-secondary');
            // }

            
            $html->append(parent::html($indent));
            return $html->text();
        }

        public function js($indent = 0) {
            $js = CStringBuilder::factory();

            $js->append(parent::js($indent));
            return $js->text();
        }

    }
    