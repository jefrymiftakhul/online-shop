<?php

class CElement_Gold_Contact extends CElement{
    
    private $header = NULL;
    private $store = NULL;
    private $address = NULL;
    private $telp = NULL;
    private $bbm = NULL;
    private $wa = NULL;
    private $email = NULL;
    private $barcode = NULL;

    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public static function factory($id = ''){
        return new CElement_Gold_Contact($id);
    }
            
  
    public function set_header($header){
        $this->header = $header;
        return $this;
    }
    
    public function set_store($store){
        $this->store = $store;
        return $this;
    }
    
    public function set_address($address){
        $this->address = $address;
        return $this;
    }
    
    public function set_phone($telp){
        $this->telp = $telp;
        return $this;
    }
    
    public function set_bbm($bbm){
        $this->bbm = $bbm;
        return $this;
    }
    
    public function set_wa($wa){
        $this->wa = $wa;
        return $this;
    }
    
    public function set_email($email){
        $this->email = $email;
        return $this;
    }
    
    public function set_barcode($barcode){
        $this->barcode = $barcode;
        return $this;
    }
    
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);        
       
        $html->appendln('<div class="sidebar margin-30">');
        $html->appendln('<div class="header bg-red font-white bold">' . $this->header . '</div>');
        $html->appendln('<center class="font-red font-size20 upper bold margin-top-10">'.$this->store.' <span class="font-yellow">GOLD</span></center>');
        $html->appendln('<table class="margin-left-10" width="100%">');
        $html->appendln('<tr>');
        $html->appendln('<td width="30px" valign="top"><div class="icon-company-red"></div></td><td>' . $this->address . '</td>');
        $html->appendln('</tr>');
        $html->appendln('<tr>');
        $html->appendln('<td valign="top"><div class="icon-phone-red"></div></td><td>' . $this->telp . '</td>');
        $html->appendln('</tr>');
        $html->appendln('<tr>');
        $html->appendln('<td valign="top"><div class="icon-bbm-red"></div></td><td>' . $this->bbm . '</td>');
        $html->appendln('</tr>');
        $html->appendln('<tr>');
        $html->appendln('<td valign="top"><div class="icon-wa-red"></div></td><td>' . $this->wa . '</td>');
        $html->appendln('</tr>');
        $html->appendln('<tr>');
        $html->appendln('<td valign="top"><div class="icon-email-red"></div></td><td>' . $this->email . '</td>');
        $html->appendln('</tr>');
        if($this->barcode){
            $html->appendln('<tr>');
            $html->appendln('<td colspan="2" style="text-align:center"><img src="' . $this->barcode . '" style="width:150px; height:150px"/></td>');
            $html->appendln('</tr>');
        }
        $html->appendln('</table>');
        $html->appendln('</div>');
        
        $html->append(parent::html(), $indent);
        return $html->text();
    }
    
    public function js($indent=0) {
       $js = new CStringBuilder();

        $js->append(parent::js($indent));
        return $js->text();
    }
}
