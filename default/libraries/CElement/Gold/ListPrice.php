<?php

class CElement_Gold_ListPrice extends CElement{
    
    private $header = NULL;
    private $list_price = NULL;
   

    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public static function factory($id = ''){
        return new CElement_Gold_ListPrice($id);
    }
            
  
    public function set_header($header){
        $this->header = $header;
        return $this;
    }
    
    public function set_list_price($list_price){
        $this->list_price = $list_price;
        return $this;
    }
    
    private function cek_string($data){
        $replace = array('.', ',', '-');
        $data = str_replace($replace, '', $data);
        
        if(ctype_alnum($data)){
            return FALSE;
        }
        else{
            return TRUE;
        }
    }
       
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);        
       
        $html->appendln('<div class="sidebar">');
        $html->appendln('<div class="header bg-red upper font-white bold">' . $this->header . '</div>');
        
        $html->appendln('<table class="table table-striped" style="margin:0px">');
        $html->appendln('<tr>');
        $html->appendln('<th width="30%">LM(gr)</th>');
        $html->appendln('<th width="35%">jual</th>');
        $html->appendln('<th>beli</th>');
        $html->appendln('</tr>');
        $html->appendln('</table>');
        $html->appendln('<div class="horizontal-overflow" style="max-height:255px">');
        $html->appendln('<table class="table table-striped" style="margin:0px">');
        foreach ($this->list_price as $data){
            $class = NULL;
            $sell_price=ctransform::format_currency($data['sell']);
            if($this->cek_string($data['sell'])){
                $class = "label bg-red pull-right";
				$sell_price='Sold Out';
            }
            
            $html->appendln('<tr height="25px">');
            $html->appendln('<td width="32%" align="center">' . $data['name'] . '</td>');
            $html->appendln('<td width="36%" align="right" >' . ctransform::format_currency($data['buy']) . '</td>');
            $html->appendln('<td align="right" class="' . $class . '">' .$sell_price  . '</td>');
            $html->appendln('</tr>');
        }
        $html->appendln('</table>');
        $html->appendln('</div>');
        $html->appendln('</div>');
        
        $html->append(parent::html(), $indent);
        return $html->text();
    }
    
    public function js($indent=0) {
       $js = new CStringBuilder();

        $js->append(parent::js($indent));
        return $js->text();
    }
}
