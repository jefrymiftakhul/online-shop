<?php

class CElement_Gold_Gold extends CElement{
    
	private $array_list=array(
			'1'=>'1',
			'2'=>'2',
			'3'=>'3',
			'4'=>'4',
			'5'=>'5',
			'6'=>'6',
			'7'=>'7',
			'8'=>'8',
			'9'=>'9',
			'10'=>'10',
		);

    private $column = NULL;
    private $name = NULL;
    private $product_id = '';
    private $image;
    private $price_sell = NULL;
    private $price_buy = NULL;
	private $control_qty_select=null;
	private $stock;
	private $using_class_div_gold;

    public function __construct($id = '') {
        parent::__construct($id);
		$this->using_class_div_gold=false;
    }
    
    public static function factory($id = ''){
        return new CElement_Gold_Gold($id);
    }
    
	public function create_control_qty_select(){
		$this->control_qty_select=CFactory::create_control("qty_".$this->product_id,"select")->set_list($this->array_list);
	}
    public function set_image($image){
        $this->image = $image;
        return $this;
    }
    public function set_stock($val){
        $this->stock = $val;
        return $this;
	}
    public function set_product_id($id){
        $this->product_id = $id;
        return $this;
    }
    
    public function set_using_class_div_gold($bool){
        $this->using_class_div_gold = $bool;
        return $this;
    }
    public function set_name($name){
        $this->name = $name;
        return $this;
    }
    
    public function set_price_sell($sell){
        $this->price_sell = $sell;
        return $this;
    }
    
    public function set_price_buy($buy){
        $this->price_buy = $buy;
        return $this;
    }
    
	public function set_column($val){
		$this->column=$val;
		return $this;
	}
	
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        if (empty($this->column)) {
            $col = 3;
            $col_small = 3;
            $col_xsmall = 2;
        } else {
            $col = 12 / $this->column;
            $col_small = 24 / $this->column;
            $col_xsmall = 36 / $this->column;
        }

        if ($col_xsmall > 6 && $col_xsmall <= 9) {
            $col_xsmall = 6;
        }
        if ($col_xsmall > 9) {
            $col_xsmall = 12;
        }

        if ($this->column == 3) {
            $col_xsmall = 6;
        }

        if ($col_small > 4 && $col_small < 9) {
            $col_small = 4;
        }
		$class_div_gold='';
		if($this->using_class_div_gold){
			$class_div_gold='col-xs-' . $col_xsmall . ' col-sm-' . $col_small . ' col-md-' . $col;
		}
		$this->create_control_qty_select();
		$price_buy='Sold Out';
		$btn_buy='<input style="width:46%" class="btn-62hallfamily-small bg-red small border-none" type="button" id="sold_out_'.$this->product_id.'" name="sold_out_'.$this->product_id.'" value="Sold Out" readonly';
		if($this->stock>0){
			$price_buy=$this->price_buy;
			$btn_buy='<input style="width:46%" class="btn-62hallfamily-small bg-red small border-none" type="button" id="buy_'.$this->product_id.'" name="buy_'.$this->product_id.'" value="Beli"';
		}
        $html->appendln('<div class="product-gold container product-container '.$class_div_gold.'">');
        $html->appendln('<center class="bold font-big">'.$this->name.'</center>');
        
        $html->appendln('<div class="bold font-green font-big">' . $this->price_sell . '</div>');
        $html->appendln('<div class="bold font-red font-big">' . $price_buy . '</div>');
        
        $image = image::get_image_url($this->image, 'view_all');
        
        $html->appendln('<img width="100%" class="margin" src="' . $image . '"/>');
        $html->appendln('<center>');
        $html->appendln($this->control_qty_select->html());
        $html->appendln('</center>');
        $html->appendln('<input style="width:46%" class="col-md-6 btn-62hallfamily-small-green margin-right-10 small border-none" type="button" id="sell_'.$this->product_id.'" name="sell_'.$this->product_id.'" value="Jual">');
        $html->appendln($btn_buy);
        $html->appendln('<div class="margin-top-20"></div>');
        $html->appendln('</div>');
                
        $html->append(parent::html(), $indent);
        return $html->text();
    }
    
    public function js($indent=0) {
       $js = new CStringBuilder();
	
        $js->append(parent::js($indent));
		$js->appendln("
			$('#buy_".$this->product_id."').click(function(){
				var qty=$('#qty_".$this->product_id."').val();
				window.location='".curl::base()."gold/product/buy/".$this->product_id."/'+qty;
			});
			$('#sell_".$this->product_id."').click(function(){
				var qty=$('#qty_".$this->product_id."').val();
				window.location='".curl::base()."gold/product/sell/".$this->product_id."/'+qty;
			});
		");
		$js->appendln($this->control_qty_select->js());
        return $js->text();
    }
}
