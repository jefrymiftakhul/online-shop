<?php

class CElement_Gold_SellContact extends CElement{
    

    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public static function factory($id = ''){
        return new CElement_Gold_SellContact($id);
    }
            
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);   
		
        $html->append(parent::html(), $indent);
        return $html->text();
    }
    
    public function js($indent=0) {
       $js = new CStringBuilder();

        $js->append(parent::js($indent));
        return $js->text();
    }
}
