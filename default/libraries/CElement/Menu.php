<?php

class CElement_Menu extends CElement {

    private $menu = array();
    private $link = array();
    private $url_page = '';
    private $custom_menu = "";
    private $absolute_menu = NULL;
    private $position = 'left';
    private $url_menu = NULL;
    private $trigger_type = "click";
    

    public function __construct($id = '') {
        parent::__construct($id);
        
        $member_btn = "";
        $org_id = CF::org_id();
        $db = CDatabase::instance();
        $have_register = 1;
        if ($org_id != null) {
            $have_register = cdbutils::get_value('select have_register from org where org_id=' . $db->escape($org_id));
        }
        if (member::get_data_member() == false) {
            if ($have_register > 0) {
                //$member_btn .= '<li class="visible-sm visible-xs login-register-sm"><a href="javascript:;" class="btn btn-modal btn-register" data-target="modal-body-register" data-modal="register_form" data-title="REGISTER">' . clang::__("Register") . '</a></li>';
            }
            //$member_btn .= '<li class="visible-sm visible-xs login-register-sm"><a href="javascript:;" class="btn btn-modal btn-login" data-target="modal-body-login" data-modal="login_form" data-title="LOGIN">' . clang::__("Login") . '</a></li>';
            //$member_btn .= '<li class="visible-sm visible-xs login-register-sm"><hr style="margin:0"/></li>';
        } else {

            $data_member = member::get_data_member();
            $image_profile = curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/member/user_gray_150x150.png';
            if ($data_member->image_name) {
                $image_profile = image::get_image_url_front($data_member->image_name, 'view_profile');
            }

            $member_name = $data_member->name;
            $member_welcome = $member_name;
            $member_email = $data_member->email;

            if (strpos($member_welcome, " ") !== FALSE) {
                $arr_name = explode(" ", $member_welcome);
                $member_welcome = $arr_name[0];
            }
            /*
            $member_btn .= '<li class="visible-sm visible-xs login-register-sm">
			<div id="145692765956d6f3ab8a973349085708" class="" style="padding-top:20px; padding-bottom:20px; background-color:#000">			
				
					
				<center>
					<img style="height:80px;width:80px;border-radius:50%;border:1px solid #ccc;" src="' . $image_profile . '" class="margin-bottom-10">				
					<div id="145692765956d6f3ab8a973956095487" class=" font-big bold">' . $member_name . '</div>				
					<div id="145692765956d6f3ab8a973891875693" class=" font-small bold">(' . $member_email . ')</div>				
					<a id="145692765956d6f3ab8ad5b871568201" href="/member/account" class="btn  btn-62hallfamily-small bg-red font-white border-3-red font-size12" style="margin-right:10px;padding-top:2px !important;width:70px; height:auto;">AKUN</a>				
					<a id="145692765956d6f3ab8ad5b860218619" href="/member/account/logout" class="btn  btn-62hallfamily-small bg-red font-white border-3-red font-size12" style="width:75px;padding-top:2px !important;height:auto;">KELUAR</a>
				</center>
			</div>';
             * 
             */
            
        }

        $this->custom_menu = $member_btn;
    }
    
    public function set_menu($data_menu) {
        $this->menu = $data_menu;
        return $this;
    }

    public function set_link($data_menu) {
        $this->link = $data_menu;
        return $this;
    }

    public function set_url_page($val) {
        $this->url_page = $val;
        return $this;
    }

    public static function factory($id = '') {
        return new CElement_Menu($id);
    }

    public function set_custom_menu($val) {
        $this->custom_menu = $val;
        return $this;
    }
    
    public function set_absolute_menu($absolute_menu) {
        $this->absolute_menu = $absolute_menu;
        return $this;
    }

    public function set_position_absolute_menu($position) {
        $this->position = strtolower($position);
        return $this;
    }
    
    public function set_url_menu($url_menu) {
        $this->url_menu = $url_menu;
        return $this;
    }
    
    public function set_trigger_type($trigger_type) {
        $this->trigger_type = $trigger_type;
        return $this;
    }
    
    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        
//        $html->appendln('<div class="bg-gray-dark menu">');
        $html->appendln('<div class="container-menu" style="position:relative !important">');

        if (!empty($this->absolute_menu) and $this->position == 'left') {
            $html->appendln('<ul class="nav navbar-nav bigmall-left">');
        }
        else if(!empty($this->absolute_menu) and $this->position == 'right'){
            $html->appendln('<ul class="nav navbar-nav bigmall-right">');
        }
        else{
            $html->appendln('<ul class="nav navbar-nav">');
        }
        if (empty($this->absolute_menu)) {
//            $html->appendln('<li id="nav-side-bar" class="link padding-0">
//                                <i class="fa fa-navicon font-red"></i><label class="nav-side-bar-label">&nbspKATEGORI</label>
//                            </li>');
        }

        if (!empty($this->absolute_menu) and $this->position == 'left') {
            //$html->appendln('<li class="categories-absolute"><a></a></li>');
        }

        $url_menu = $this->url_menu;
        if (empty($url_menu)) {
//            $url_menu = curl::base() . $this->url_page . 'product/category/';
            $url_menu = curl::base() . $this->url_page . 'search?keyword=&category=';
        }
     
           if (isset($this->menu['name'])) {
            $html->appendln('<li class="dropdown categories normal category-menu ">');
            $html->appendln('<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">'
                    . ' ' . $this->menu['name'] . ''
                    . '<div style="float: right; margin-left: 10px;">
                        <div class="ico-category" style="float: right;"></div>
                          </div>'
                    . '</a>');
            $html->appendln('<ul id="menu-categories" class="dropdown-menu categories padding-0"  role="menu">');

            $html->appendln($this->custom_menu);

            $trigger_class = "dropdown-submenu-hover";
            if ($this->trigger_type == "click") {
                $trigger_class = "dropdown-submenu-click";
            }
            
//            cdbg::var_dump($this->menu);
//            die(__CLASS__ . ' - ' . __FUNCTION__);
            
            foreach ($this->menu['menu'] as $data_menu) {
                // sub menu
                $url = $url_menu . carr::get($data_menu,'url_key');
                if (count($data_menu['subnav']) > 0) {
                    if ($this->trigger_type == 'click') {
                        $url = 'javascript:;';
                    }
                    $html->appendln('<li class="dropdown-submenu ' . $trigger_class . ' categories-submenu"><a href="' . $url . '">'
                            . '<div class="icon-"></div>'
                            . $data_menu['name']
                            . '</a>');
                    $html->appendln('<ul class="dropdown-menu padding-0 bg-gray child-1">');
                    if ($this->trigger_type == 'click') {
                        $html->appendln('<li><a class="font-black link-submenu-all" href="' . $url_menu . carr::get($data_menu,'url_key') . '">'
                                . '<div class="icon-"></div>'
                                . 'Semua ' . $data_menu['name']
                                . '</a>');
                    }
                    foreach ($data_menu['subnav'] as $submenu) {
                        // sub sub menu
                        $url = $url_menu . carr::get($submenu,'url_key');

                        if (count($submenu['subnav']) > 0) {
                            if ($this->trigger_type == 'click') {
                                $url = 'javascript:;';
                            }
                            $html->appendln('<li class="dropdown-submenu ' . $trigger_class . ' categories-submenu"><a href="' . $url . '">'
                                    . '<div class="icon-"></div>'
                                    . $submenu['name']
                                    . '</a>');
                            $html->appendln('<ul class="dropdown-menu padding-0 child-2">');
                            if ($this->trigger_type == 'click') {
                                $html->appendln('<li><a class="font-black link-submenu-all" href="' . $url_menu . carr::get($submenu,'url_key') . '">'
                                    . '<div class="icon-"></div>'
                                    . 'Semua ' . $submenu['name']
                                    . '</a>');
                            }
                            foreach ($submenu['subnav'] as $subsubmenu) {
                                $url = $url_menu . carr::get($subsubmenu,'url_key');
                                $html->appendln('<li><a class="font-black" href="' . $url . '">'
                                        . '<div class="icon-"></div>'
                                        . $subsubmenu['name']
                                        . '</a>');
                            }
                            $html->appendln('</ul>');
                            $html->appendln('</li>');
                        } else {
                            $html->appendln('<li><a class="font-black" href="' . $url_menu . carr::get($submenu,'url_key') . '">'
                                    . '<div class="icon-"></div>'
                                    . $submenu['name']
                                    . '</a>');
                        }
                    }
                    $html->appendln('</ul>');
                    $html->appendln('</li>');
                } else {
                    $html->appendln('<li>'
//                            . '<a href="' . $url_menu . $data_menu['category_id'] . '">'
                            . '<a href="' . $url_menu . $data_menu['url_key'] . '">'
                            . '<div class="icon-"></div>'
                            . $data_menu['name']
                            . '</a>');
                }

                $html->appendln('</li>');
            }
            $html->appendln('</ul>');
            $html->appendln('</li>');
        }

        if (isset($this->link)) {
            foreach ($this->link as $data_link) {
                $html->appendln('<li class="inline-block"><a class="font-size18 normal" href="' . $data_link['url'] . '">' . $data_link['label'] . '</a></li>');
            }
        }


        $html->appendln('</ul>');

        if (!empty($this->absolute_menu) and $this->position == 'left') {
            $html->appendln('<div class="categories absolute-menu absolute-left bg-red padding-40 font-white">');
            $html->appendln($this->generate_absolute_menu($this->absolute_menu));
            $html->appendln('</div>');
        }

        if (!empty($this->absolute_menu) and $this->position == 'right') {
            $html->appendln('<div class="categories absolute-menu absolute-right bg-red padding-40 font-white">');
            $html->appendln($this->generate_absolute_menu($this->absolute_menu));
            $html->appendln('</div>');
        }

        $html->appendln('</div>');

        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();
        $js->appendln("
            $('.dropdown-toggle').on('touchstart',function(e){
                e.stopPropagation();
            });
        ");
        $js->appendln("
            $('.container-menu .dropdown-submenu.dropdown-submenu-click').click(function (evt) {
                evt.stopPropagation();
                if($(this).hasClass('open')) {
                    $(this).removeClass('open');
                } else {
                    if ($(window).width > 767) {
                        $(this).parent().find('.dropdown-submenu.dropdown-submenu-click').removeClass('open');
                    }
                    $(this).addClass('open');
                }
            });    
            
            ");
        $js->append(parent::js($indent));
        return $js->text();
    }

}
