<?php

class CElement_Search extends CElement{
    
    private $keyword = NULL;
    private $type = "product";

    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public static function factory($id = ''){
        return new CElement_Search($id);
    }
    
    public function set_keyword($keyword){
        $this->keyword = $keyword;
        return $this;
    }
    
    public function set_type($type){
        $this->type = $type;
        return $this;
    }
    
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        
        $form = $this->add_form()->add_class('form-62hallfamily')
                ->set_method('GET')
                ->set_action(curl::base() . 'search');
        $search = $form->add_div()->add_class('col-md-12 col-xs-12 margin-bottom-20');
        $input = $search->add_div()->add_class('col-md-11 col-xs-11');
        $button = $search->add_div()->add_class('col-md-1 col-xs-1 padding-0');
        
        $page = $input->add_control('page','hidden')
                ->set_value($this->type);

        $keyword = $input->add_field()
                    ->add_control("keyword", 'text')
                    ->set_name("keyword")
                    ->add_class("form-control search")
                    ->custom_css('margin-top', '2px !important')
                    ->set_placeholder('Cari produk ...');
        
//        $button->add_action()
//                ->set_label('<i class="fa fa-seacrh"></i>')
//                ->add_class('btn-62hallfamily bg-red padding-left-0 border-3-red')
//                ->custom_css('height', '35px')
//                ->custom_css('margin-top', '27px !important')
//                ->set_submit(true);
//        
        $button->add('<button type="submit" class="btn-62hallfamily bg-red padding-left-0 border-3-red" style="height:35px;margin-top:27px !important"><i class="fa fa-search"></i></button>');
        
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent=0) {
        $js = new CStringBuilder();
       
        $js->append(parent::js($indent));
        return $js->text();
    }
}
