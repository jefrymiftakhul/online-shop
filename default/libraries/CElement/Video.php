<?php

class CElement_Video extends CElement{
    
    protected $url;
    protected $url_key;
    
    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public static function factory($id = ''){
        return new CElement_Video($id);
    }
    
    public function set_url_key($val){
        $this->url_key=$val;
        return $this;
    }

    public function set_url($val){
        $this->url=$val;
        return $this;
    }
    
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        $org_id=CF::org_id();
        $db=CDatabase::instance();
        $url_key=$this->url_key;
        $url=$this->url;
        $title='';
        $video_id="";
        $description='';
        $url_prev='';
        $url_next='';
        $q="
            select
                *
            from
                cms_video
            where
                status>0
                and org_id=".$db->escape($org_id)."
        ";    
        $video=$db->query($q);
        if($video->count()>0){
            if(strlen($url_key)==0){
                $url=$video[0]->url;
                $video_id=$video[0]->video_id;
                $title=$video[0]->title;
                $description=$video[0]->description;
                if(isset($video[1])){
                    $url_next=$video[1]->url_key;
                }
            }else{
                $i=0;
                foreach($video as $row_video){
                    if($row_video->url_key==$url_key){
                        $url=$row_video->url;
                        $video_id=$row_video->video_id;
                        $title=$row_video->title;
                        $description=$row_video->description;
                        if(isset($video[$i+1])){
                            $url_next=$video[$i+1]->url_key;
                        }
                        break;
                    }
                    $url_prev=$row_video->url_key;
                    $i++;
                }
            }
        }
        $html->append('
            <div class="container-video">
                <iframe id="player" type="text/html"
                    src="//www.youtube.com/embed/'.$video_id.'?enablejsapi=1"
                    frameborder="0">
                </iframe>
            </div>
        ');
        $prev='';
        if(strlen($url_prev)>0){
            $prev='
                <a class="video-prev-next right carousel-control" url-key="'.$url_prev.'" role="button">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only"></span>
                </a>

            ';
        }
        $next='';
        if(strlen($url_next)>0){
            $next='
                <a class="video-prev-next right carousel-control " url-key="'.$url_next.'" role="button">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only"></span>
                </a>
            ';
        }
        $html->append('
            <div class="row container-video-info">
                <div class="col-md-1">
                    '.$prev.'
                </div>
                <div class="col-md-9">
                    <div class="video-title" video-title="'.$title.'">
                    '.$title.'
                    </div>
                    <div class="video-description">
                    '.$description.'
                    </div>
                </div>
                <div class="col-md-1">
                    '.$next.'
                </div>
            </div>
        ');
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent=0) {
        $js = new CStringBuilder();
        $js->append("
            $('.video-prev-next').click(function(){
                var url_key=$(this).attr('url-key');
                if(url_key.length>0){
                    jQuery('.col-video-gallery').removeClass('active');
                    jQuery('.video-gallery[url-key='+url_key+']').find('.col-video-gallery').addClass('active');
                    $.cresenity.reload('container_video','" . curl::base() . "reload/reload_video','get',{'url_key':url_key});
                }
            });
            
        ");
        
        $js->append(parent::js($indent));
        return $js->text();
    }
}
