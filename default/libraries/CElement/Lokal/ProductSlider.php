<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class CElement_Lokal_ProductSlider extends CElement_Lokal_SlideShow
{

    public function __construct($id = "", $tag = "div")
    {
        parent::__construct($id, $tag);

        $this->responsive = prelove_home::lokal_responsive_size();
        $this->slide_to_scroll = 3;
        $this->slide_to_show = 3;
        $this->dots = "false";
    }

    public static function factory($id = "", $tag = "div")
    {
        return new CElement_Lokal_ProductSlider($id, $tag);
    }

    public function html($indent = 0)
    {
        $html = new CStringBuilder();

        $classes = $this->classes;
        $classes = implode(" ", $classes);
        if (strlen($classes) > 0) {
            $classes = " " . $classes;
        }

        $counter = 0;
        if(count($this->slides) > 0){
            $html->append('<div id="' . $this->id . '" class="' . $classes . '">');
            foreach ($this->slides as $k => $v) {
                $html->appendln('<div>');
                $html->appendln('   <div>');
                $html->appendln(CElement_Lokal_Card::factory()->set_product($v)->html());
                $html->appendln('   </div>');
                $html->appendln('</div>');

            }
            $html->append('</div>');
        }
        $html->appendln(parent::html($indent));

        return $html->text();
    }

}
