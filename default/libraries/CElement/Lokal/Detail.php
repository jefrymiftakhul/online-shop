<?php

class CElement_Lokal_Detail extends CElement
{

    private $key = null;
    private $detail_list = array();
    private $price = 0;
    private $price_promo = null;

    private $form = null;
    private $type_select = "select";
    private $type_image = "image";
    private $attribute_list = array();

    private $with_qty = true;

    public function __construct($id = '')
    {
        parent::__construct($id);
    }

    public static function factory($id = '')
    {
        return new CElement_Lokal_Detail($id);
    }

    public function set_key($key)
    {
        $this->key = $key;
        return $this;
    }

    public function set_minimum_stock($min_stock)
    {
        $this->minimum_stock = $min_stock;
        return $this;
    }

    public function set_detail_list($detail_list)
    {
        $this->detail_list = $detail_list;
        return $this;
    }

    public function set_attribute_list($attribute_list)
    {
        $this->attribute_list = $attribute_list;
        return $this;
    }

    public function with_qty($with_qty)
    {
        $this->with_qty = $with_qty;
        return $this;
    }

    private function get_price()
    {
        $detail_price = $this->price;

        $arr_price = product::get_price($detail_price);

        return $arr_price;
    }
    public function html($indent = 0)
    {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        $db = CDatabase::instance();

        $product_id = carr::get($this->detail_list, 'product_id', null);
        $name = carr::get($this->detail_list, 'name', null);
        $sku = carr::get($this->detail_list, 'sku', null);
        $availability = carr::get($this->detail_list, 'is_available', null);
        $quick_overview = carr::get($this->detail_list, 'quick_overview', null);
        $this->price = carr::get($this->detail_list, 'detail_price', 0);
        $detail_price = carr::get($this->detail_list, 'detail_price', 0);
        $province_id = carr::get($this->detail_list, 'province_id');
        $city_id = carr::get($this->detail_list, 'city_id');
        $stock = carr::get($this->detail_list, 'stock', 0);

        $province_name = cdbutils::get_value('select name from province where province_id =' . $db->escape($province_id));
        $city_name = cdbutils::get_value('select name from city where city_id =' . $db->escape($city_id));

        $array_size = carr::get($this->detail_list, 'size');
        $array_color = carr::get($this->detail_list, 'color');

        $html->appendln('<div class="detail-product margin-20">');
        $html->appendln('<h3 class="bold">' . $name . '</h3>');

        $html->appendln('<br>');
        $html->appendln($province_name);
        $html->appendln($city_name);
        $html->appendln('<br>');

        $html->appendln('<div class="font-big bold">Rincian Singkat</div>');
        $html->appendln($quick_overview);

        $html->appendln('<br><br>');
        $html->appendln('<div class="font-big bold">Tersisa ' . $stock . '</div>');

        $price = $this->get_price();

        $html->appendln('<br>');
        if ($availability > 0) {

            if ($price['promo_price'] > 0) {
                $html->appendln('<strike class="font-black"> Rp. ' . ctransform::format_currency($price['price']) . '</strike>');
                $html->appendln('<div class="font-red font-size24 bold"> Rp. ' . ctransform::format_currency($price['promo_price']) . '</div>');

            } else {
                $html->appendln('<div class="font-red font-size24 bold"> Rp. ' . ctransform::format_currency($price['price']) . '</div>');
            }
        } else {
            $html->appendln('<div class="font-black bold">Kisaran Harga</div><div class="font-red font-size24 bold">Rp. ' . ctransform::format_currency($detail_price['sell_price_start']) . ' - Rp. ' . ctransform::format_currency($detail_price['sell_price_end']) . '</div>');
        }

        $form = $this->add_form('form-detail-product')->set_action(curl::base() . 'request/add/');
        $form->add_control('product_id', 'hidden')->set_value($product_id);
        $form->add_control('page', 'hidden')->set_value('service');

        $rowform = $form->add_div()->add_class('row margin-top-20');
        $leftform = $rowform->add_div()->add_class('col-md-3');
        $rigthform = $rowform->add_div()->add_class('col-md-8');

        if ($availability > 0) {
            $action = $leftform->add_action()
                ->set_label(clang::__('<i class="fa fa-send"></i> Pesan'))
                ->add_class('btn-62hallfamily bg-red border-3-red')
                ->custom_css('height', '34px')
                ->set_link(curl::base() . 'lokal/updatecontact/update_contact/' . $product_id);
        } else {
            $action = $leftform->add_action()
                ->set_label('Request')
                ->add_class('btn-62hallfamily bg-red border-3-red')
                ->custom_css('height', '34px')
                ->set_submit_to(curl::base() . 'request/add/')
                ->set_submit(true);

        }
        $html->appendln('</div>');
        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0)
    {
        $js = new CStringBuilder();

        $js->append(parent::js($indent));
        return $js->text();
    }
}
