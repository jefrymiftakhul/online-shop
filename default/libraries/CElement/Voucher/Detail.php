<?php

class CElement_Voucher_Detail extends CElement {

    private $key = NULL;
    private $detail_list = array();
    private $price = 0;
    private $stock = 0;
    private $minimum_stock = 10;
    private $attribute_list = array();
    private $with_qty = TRUE;
    private $type_select = 'select';
    private $type_image = 'image';
    private $total_param_input = array();

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_Voucher_Detail($id);
    }

    public function set_key($key) {
        $this->key = $key;
        return $this;
    }

    public function set_minimum_stock($min_stock) {
        $this->minimum_stock = $min_stock;
        return $this;
    }

    public function set_detail_list($detail_list) {
        $this->detail_list = $detail_list;
        return $this;
    }

    public function with_qty($with_qty) {
        $this->with_qty = $with_qty;
        return $this;
    }

    public function set_attribute_list($attribute_list) {
        $this->attribute_list = $attribute_list;
        return $this;
    }

    private function get_price() {
        $detail_price = $this->price;

        $arr_price = product::get_price($detail_price);

        return $arr_price;
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        $db = CDatabase::instance();

        $product_id = carr::get($this->detail_list, 'product_id', NULL);
        $name = carr::get($this->detail_list, 'name', NULL);
        $sku = carr::get($this->detail_list, 'sku', NULL);
        $availability = carr::get($this->detail_list, 'is_available', NULL);
        $stock = carr::get($this->detail_list, 'stock', 0);
        $this->stock = $stock;
        $this->price = carr::get($this->detail_list, 'detail_price', 0);
        $detail_price = carr::get($this->detail_list, 'detail_price', 0);
        $end_bid_date = carr::get($this->detail_list,'end_bid_date');

        $image_logo = carr::get($this->detail_list,'file_path_voucher');
        $image = curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/member/user_gray_150x150.png';
        if ($image_logo != null) {
            $image = $image_logo;
        }

        $html->appendln('<div class="detail-product margin-20">');
            $html->appendln('<div class="detail-product">');
            $html->appendln('<div class="image_profile" style="text-align:center;margin-bottom:20px;">');
                $html->appendln('<img src="' . $image . '" style="width:150px;margin:0 auto;">');
            $html->appendln('</div>');
        $html->appendln('</div>');
        
        $expired = $end_bid_date;
        if($expired != null){
            $is_started = false;
            if (time() >= strtotime($expired)) {
                $is_started = true;
            }
            $end_day = '00';
            $end_hour = '00';
            $end_minutes = '00';
            $end_second = '00';
            if(!$is_started){
                $time = prelove::time_diff_day($expired);

                $end_day = $time['day'];
                $end_hour = $time['hour'];
                $end_minutes = $time['minutes'];
                $end_second = $time['second'];
            }
            $html->appendln('<br>');
            $html->appendln(clang::__('Expired Date :'));
            $html->appendln('<div class="time_wrapper">');
                $html->appendln('<div class="time-left"><span class="day">' . $end_day . '</span><br/>' . clang::__('DAYS') . '</div>');
                $html->appendln('<div class="time-left"><span class="hour">' . $end_hour . '</span><br/>' . clang::__('HOURS') . '</div>');
                $html->appendln('<div class="time-left"><span class="minute">' . $end_minutes . '</span><br/>' . clang::__('MINS') . '</div>');
                $html->appendln('<div class="time-left"><span class="second">' . $end_second . '</span><br/>' . clang::__('SECS') . '</div>');
            $html->appendln('</div>');
        }

        if ($availability > 0) {
            $html->appendln('<div id="stock-price">');
                $html->appendln('<span class="font-gray-dark">Tersisa ' . $stock . '</span>');
                $html->appendln('<br>');

                $html->appendln('<br>');

                $price = $this->get_price();

                if ($price['promo_price'] > 0) {
                    $html->appendln('<strike class="font-black"> Rp. ' . ctransform::format_currency($price['price']) . '</strike>');
                    $html->appendln('<div class="font-red font-size24 bold"> Rp. ' . ctransform::format_currency($price['promo_price']) . '</div>');
                } else {
                    $html->appendln('<div class="font-red font-size24 bold"> Rp. ' . ctransform::format_currency($price['price']) . '</div>');
                }
            $html->appendln('</div>');
        } else {
            $html->appendln('<div class="font-black bold">Kisaran Harga</div><div class="font-red font-size24 bold">Rp. ' . ctransform::format_currency($detail_price['sell_price_start']) . ' - Rp. ' . ctransform::format_currency($detail_price['sell_price_end']) . '</div>');
        }

        $form = $this->add_form('form-detail-product')->set_action(curl::base() . 'request/add/');
        $form->add_control('product_id', 'hidden')->set_value($product_id);
        $form->add_control('page', 'hidden')->set_value('product');

        if ($availability > 0) {
            if ($this->with_qty) {
                $form->add_div('div_qty')->add_class('margin-top-20')->add("<input type='text' id='qty' name='qty' value='1' style='z-index:0'>");
            }
            $action = $form->add_action('beli')
                    ->set_label(clang::__('BELI SEKARANG'))
                    ->add_class('btn-62hallfamily bg-red border-3-red btn-add-cart margin-top-30')
                    ->custom_css('height', '34px')
                    ->set_link(curl::base() . 'voucher/updatecontact/update_contact/' . $product_id.'?qty=1');
                    if($stock == 0){
                        $action->add_class('disabled');
                        $form->add_br();
                        $form->add('<span style="font-size:12px;">'.clang::__('Maaf anda tidak dapat membeli produk ini karena stok habis').'</span>');
                    }
        } else {
            $this->stock = 100;
            $action = $form->add_action()
                    ->set_label('Request')
                    ->add_class('btn-62hallfamily bg-red border-3-red margin-top-20')
                    ->custom_css('height', '34px')
                    ->set_submit_to(curl::base() . 'request/add/')
                    ->set_submit(true);
        }
        $html->appendln('</div>');

        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();
        $max = $this->stock;
        if ($max < 1)
            $max = 1;
        $product_id = carr::get($this->detail_list, 'product_id', null);
        $js->appendln("
            $('.btn-colors').click(function(){
                id = $(this).attr('id');
                $('#colors').val(id);
                $('.btn-colors').removeClass('active');
                $(this).addClass('active');
            });  
            
            $('#qty').TouchSpin({
                min: 1,
                max: " . $max . ",
                verticalbuttons: true
            }).change(function(){
                var qty = this.value;
                var src = $('#beli').attr('href','".curl::base() ."voucher/updatecontact/update_contact/". $product_id."?qty='+qty);
            });
		");

        $js->append(parent::js($indent));
        return $js->text();
    }

}
