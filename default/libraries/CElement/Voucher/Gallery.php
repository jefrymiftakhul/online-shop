<?php

class CElement_Voucher_Gallery extends CElement {

    protected $images = array();	
                                	protected $page ='product';

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_Voucher_Gallery($id);
    }

    public function set_images($images) {
        $this->images = $images;
        return $this;
    }

    public function set_page($val) {
        $this->page = $val;
        return $this;
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        $zoom_class = "cloud-zoom";
		$image_src = $this->images;
        
        $html->appendln('<div class="col-md-12 categories product-single-image">');
        $html->appendln('<div id="product-slider">');
        $html->appendln('<ul class="slides">');
        $html->appendln('<li>
							<div style="height:460px;">
								<img class="'.$zoom_class.'" style="width:100%;height:auto;max-height:460px;margin:0 auto;" src="' . $image_src . '" data-large="' . $image_src . '" alt="" />
								<a class="fullscreen-button" href="' . $image_src . '">
									<div class="product-fullscreen">
										<i class="icons icon-resize-full-1"></i>
									</div>
								</a>
							</div>
                        </li>');
        $html->appendln('</ul>');
        $html->appendln('</div>');

        if (count($this->images) > 1) {
            $html->appendln('<div id="product-carousel" style="height: 100px">');
            $html->appendln('<ul class="slides product-gallery margin-top-10">');

            foreach ($this->images as $images) {
                $html->appendln('<li>
                                <a class="fancybox" rel="product-images" href="' . image::get_image_url($images['image_path'], $this->page) . '"></a>
                                <img src="' . image::get_image_url($images['image_path'],'thumbnails') . '" data-large="' . image::get_image_url($images['image_path']) . '" alt=""/>
                            </li>');
            }
            $html->appendln('</ul>');
            $html->appendln('<div class="product-arrows">
                                <div class="left-arrow">
                                        <i class="fa fa-chevron-left"></i>
                                </div>
                                <div class="right-arrow">
                                        <i class="fa fa-chevron-right"></i>
                                </div>
                        </div>');
            $html->appendln('</div>');
        }
        $html->appendln('</div>');

        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $script = "gallery_product();
                
                function gallery_product(){
		
                    $('#product-carousel').flexslider({
                        animation: 'slide',
                        controlNav: false,
                        animationLoop: false,
                        directionNav: false,
                        slideshow: false,
                        itemWidth: 70,
                        itemMargin: 0,
                        start: function(slider){

                        setActive($('#product-carousel li:first-child img'));
                        slider.find('.right-arrow').click(function(){
                                slider.flexAnimate(slider.getTarget('next'));
                        });

                        slider.find('.left-arrow').click(function(){
                                slider.flexAnimate(slider.getTarget('prev'));
                        });

                        slider.find('img').click(function(){
                                var large = $(this).attr('data-large');
                                setActive($(this));
                                $('#product-slider img').fadeOut(300, changeImg(large, $('#product-slider img')));
                                $('#product-slider a.fullscreen-button').attr('href', large);
                        });

                        function changeImg(large, element){
                                var element = element;
                                var large = large;
                                setTimeout(function(){ startF()},300);
                                function startF(){
                                        element.attr('src', large)
                                        element.attr('data-large', large)
                                        element.fadeIn(300);
                                }

                        }

                        function setActive(el){
                                var element = el;
                                $('#product-carousel img').removeClass('active-item');
                                element.addClass('active-item');
                        }

                    }
			
                    });
			
                    $('a.fullscreen-button').click(function(e){
                            e.preventDefault();
                            var target = $(this).attr('href');
                            $('#product-carousel a.fancybox[href='+target+']').trigger('click');
                    });
                    
                    $('.cloud-zoom').imagezoomsl({
                            zoomrange: [2, 2],
                            magnifiersize: [350, 350],
                            innerzoom: true,
                    });

                    $('.fancybox').fancybox();

                }";

        $js->appendln($script);

        $js->append(parent::js($indent));
        return $js->text();
    }

}
