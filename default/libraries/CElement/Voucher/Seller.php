<?php

class CElement_Voucher_Seller extends CElement
{

    private $product_id           = '';

    public function __construct($id = '')
    {
        parent::__construct($id);
    }

    public static function factory($id = '')
    {
        return new CElement_Voucher_Seller($id);
    }

    public function set_product_id($id)
    {
        $this->product_id = $id;
        return $this;
    }

    public function html($indent = 0)
    {

        $html = new CStringBuilder();
        $html->set_indent($indent);
        $db = CDatabase::instance();
        if($this->product_id != null){
            $product = product::get_product($this->product_id);
            $box_seller = $this->add_div()->add_class('box-seller');
            $box_seller->add_div()->add_class('title')->add(clang::__('MERCHANT'));
            $content = $box_seller->add_div()->add_class('content');
            $q = 'select m.*
            from product p
            inner join member m on p.member_id = m.member_id
            where p.status > 0
            and p.product_id =
            ' . $db->escape($this->product_id);
            $data_seller = cdbutils::get_row($q);
            $image_seller = carr::get($product, 'file_path_voucher');
            $seller_name = cobj::get($data_seller, 'name');
            $seller_address = cobj::get($data_seller, 'address');
            $seller_phone = cobj::get($data_seller, 'phone');
            $image = '';
            $image = curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/member/user_gray_150x150.png';
            if ($image_seller != null) {
                $image = $image_seller;
            }
            $content->add_div()->add_class('image_profile')->custom_css('margin-bottom','20px')->add_img()->set_src($image);
            $content->add('<h4 class="seller_name">'.$seller_name.'</h4>');
            $content->add('<p class="seller_address">'.$seller_address.'</p>');
            $content->add('<p class="seller_phone">'.$seller_phone.'</p>');
        }
        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0)
    {
        $js = new CStringBuilder();

        $js->append(parent::js($indent));
        return $js->text();
    }

}
