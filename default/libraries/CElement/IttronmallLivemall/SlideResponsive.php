<?php

class CElement_IttronmallLivemall_SlideResponsive extends CElement {

    protected $list_product = array();
    private $margin = 10;

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public function set_list_item($list) {
        $this->list_product = $list;
        return $this;
    }

    public function set_margin($margin) {
        $this->margin = $margin;
        return $this;
    }

    public static function factory($id = '') {
        return new CElement_IttronmallLivemall_SlideResponsive($id);
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        $db = CDatabase::instance();

        $container_slide = $this->add_div()->add_class("col-md-12");
        $container_slide_content = $container_slide->add_div()->add_class("row");

        $box_slider = $container_slide_content->add_div()->add_class("owl-carousel sixty-slider");
        if (!is_array($this->list_product)) {
            $this->list_product = array();
        }
        $name_mpu = cdbutils::get_value('select name_mpu from org where status > 0 and org_id = ' . $db->escape(CF::org_id()));
        foreach ($this->list_product as $key => $value) {
            $product_id = carr::get($value, 'product_id');
            $is_mpu = carr::get($value, 'is_mpu');
            $url_key = carr::get($value, 'url_key');
            $image_url = carr::get($value, 'image_path');
            $name = carr::get($value, 'name');
            $flag = $hot = (carr::get($value, "is_hot_item") > 0) ? 'Hot' : NULL;
            $detail_price = carr::get($value, 'detail_price');
            $promo_end_date = carr::get($detail_price, 'promo_end_date');
//            if(!empty(carr::get($value, 'new_product_date'))){
            if (strlen(carr::get($value, 'new_product_date')) > 0) {
                if (strtotime(carr::get($value, 'new_product_date')) >= strtotime(date('Y-m-d'))) {
                    $flag = $new = 'New';
                }
            }
            $key = $url_key;
            if ($key == null)
                $key = $product_id;

            $promo = carr::get($value, 'promo');
            $stock = carr::get($value, 'stock');
            $min_stock = carr::get($value, 'show_minimum_stock');
            $price = carr::get($value, 'detail_price');
            $is_available = carr::get($value, 'is_available');
            $product_data_type = carr::get($value, 'product_data_type');

            $element_product = CElement_Product_Product::factory();
            $element_product->set_key($key)
                    ->set_slide_responsive(true)
                    ->set_image($image_url)
                    ->set_name($name)
                    ->set_flag($flag)
                    ->set_label_flag($flag)
                    ->set_price($price)
                    ->set_stock($stock)
                    ->set_min_stock($min_stock)
                    ->set_available($is_available);
//            if (strlen($name_mpu) > 0) {
//                $element_product->set_name_mpu($name_mpu);
//            }
//            if (strlen($promo_end_date) > 0) {
//                $element_product->set_promo_end_date($promo_end_date);
//            }

            $element_html = $element_product->html();
            $box_slider->add_div()->add($element_html)->add_class("owl-item box-item");
        }


        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $js->appendln("
            var owl = $('.owl-carousel').owlCarousel({
                loop:true,
                margin:" . $this->margin . ",
                // autoplay:true,
                autoplayTimeout:5000,
                dots: true,
                nav:false,
                callbacks:true,
                responsive:{
                    0:{
                        items:1
                    },
                    400:{
                        items:2
                    },
                    640:{
                        items:3
                    },
                    1000:{
                        items:5
                    },
                    1200:{
                        items:6
                    }
                }
            });
            
            owl.on('onAnimationEnd',function(){
                console.log('after');
            });
            
            ");


        $js->append(parent::js($indent));
        return $js->text();
    }

}
