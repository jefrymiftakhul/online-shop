<?php

class CElement_IttronmallLivemall_Menu extends CElement {

    private $menu = array();
    private $link = array();
    private $absolute_menu = NULL;
    private $position = 'left';
    private $url_menu = NULL;
    private $url_page = '';
    private $options = array();
    private $custom_menu = "";
    private $trigger_type = "hover";

    public function __construct($id = '') {
        parent::__construct($id);

        $member_btn = "";
        $org_id = CF::org_id();
        $db = CDatabase::instance();
        $have_register = 1;
        if ($org_id != null) {
            $have_register = cdbutils::get_value('select have_register from org where org_id=' . $db->escape($org_id));
        }
        if (member::get_data_member() == false) {
            if ($have_register > 0) {
                $member_btn .= '<li class="visible-sm visible-xs login-register-sm"><a href="javascript:;" class="btn btn-modal btn-register" data-target="modal-body-register" data-modal="register_form" data-title="REGISTER">' . clang::__("Register") . '</a></li>';
            }
            $member_btn .= '<li class="visible-sm visible-xs login-register-sm"><a href="javascript:;" class="btn btn-modal btn-login" data-target="modal-body-login" data-modal="login_form" data-title="LOGIN">' . clang::__("Login") . '</a></li>';
            $member_btn .= '<li class="visible-sm visible-xs login-register-sm"><hr style="margin:0"/></li>';
        } else {

            $data_member = member::get_data_member();
            $image_profile = curl::base() . 'application/62hallfamily/default/media/img/62hallfamily/member/user_gray_150x150.png';
            if ($data_member->image_name) {
                $image_profile = image::get_image_url_front($data_member->image_name, 'view_profile');
            }

            $member_name = $data_member->name;
            $member_welcome = $member_name;
            $member_email = $data_member->email;

            if (strpos($member_welcome, " ") !== FALSE) {
                $arr_name = explode(" ", $member_welcome);
                $member_welcome = $arr_name[0];
            }
            $member_btn .= '<li class="visible-sm visible-xs login-register-sm">
			<div id="145692765956d6f3ab8a973349085708" class="" style="padding-top:20px; padding-bottom:20px; background-color:#000">			
				
					
				<center>
					<img style="height:80px;width:80px;border-radius:50%;border:1px solid #ccc;" src="' . $image_profile . '" class="margin-bottom-10">				
					<div id="145692765956d6f3ab8a973956095487" class=" font-big bold">' . $member_name . '</div>				
					<div id="145692765956d6f3ab8a973891875693" class=" font-small bold">(' . $member_email . ')</div>				
					<a id="145692765956d6f3ab8ad5b871568201" href="/member/account" class="btn  btn-62hallfamily-small bg-red font-white border-3-red font-size12" style="margin-right:10px;padding-top:2px !important;width:70px; height:auto;">AKUN</a>				
					<a id="145692765956d6f3ab8ad5b860218619" href="/member/account/logout" class="btn  btn-62hallfamily-small bg-red font-white border-3-red font-size12" style="width:75px;padding-top:2px !important;height:auto;">KELUAR</a>
				</center>
			</div>';
        }

        $this->custom_menu = $member_btn;
    }

    public static function factory($id = '') {
        return new CElement_IttronmallLivemall_Menu($id);
    }

    public function set_trigger_type($trigger_type) {
        $this->trigger_type = $trigger_type;
        return $this;
    }

    public function set_menu($data_menu) {
        $this->menu = $data_menu;
        return $this;
    }

    public function set_options($array) {
        $this->options = $array;
        return $this;
    }

    public function set_url_menu($url_menu) {
        $this->url_menu = $url_menu;
        return $this;
    }

    public function set_link($link) {
        $this->link = $link;
        return $this;
    }

    public function set_absolute_menu($absolute_menu) {
        $this->absolute_menu = $absolute_menu;
        return $this;
    }

    public function set_position_absolute_menu($position) {
        $this->position = strtolower($position);
        return $this;
    }

    public function set_url_page($val) {
        $this->url_page = $val;
        return $this;
    }

    public function set_custom_menu($val) {
        $this->custom_menu = $val;
        return $this;
    }

    public function generate_absolute_menu($type) {
        $absolute_menu = '';
        switch ($type) {
            case 'gold-search': {
                    $category_id = carr::get($this->options, 'category_id');
                    $gold_weight = carr::get($this->options, 'gold_weight');
                    $absolute_menu = "";
                    $absolute_menu .= "<div class='font-size24 col-md-12'>Anda ingin Jual atau Beli mudah, aman &amp; terpercaya ?";
                    $absolute_menu .= "<form class='form-62hallfamily font-size20' action='" . curl::base() . 'gold/search' . "' method='GET'>";

                    $absolute_menu .= "<div class='row'>";
                    $absolute_menu .= "<div class='font-size24 bold col-md-12 '>CEK DISINI !</div>";
                    $absolute_menu .= "</div>";

                    $absolute_menu .= "<div class='row'>";
                    $absolute_menu .= "<div class='col-md-12 margin-top-20'>Jenis Logam Mulia</div>";
                    $absolute_menu .= "</div>";

                    $absolute_menu .= "<select id='category' name='category' class='select-62hallfamily'>";
                    $absolute_menu .= "<option value='' disabled selected hidden></option>";

                    $gold_category = product_category::get_product_category_menu('gold');
                    foreach ($gold_category as $data_gold_category) {
                        $selected = '';
                        if ($data_gold_category['category_id'] == $category_id) {
                            $selected = 'selected';
                        }
                        $absolute_menu .= "<option value='" . $data_gold_category['category_id'] . "' " . $selected . ">" . $data_gold_category['name'] . "</option>";
                    }
                    $absolute_menu .= "</select>";

                    $absolute_menu .= "<div class='row'>";
                    $absolute_menu .= "<div class='col-md-12 margin-top-20'>Pecahan</div>";
                    $absolute_menu .= "</div>";

                    $gold_pecahan = array();
                    if ($category_id) {
                        $gold_pecahan = product::get_weight_gold($category_id);
                    }

                    $absolute_menu .= "<div class='row'>";
                    $absolute_menu .= "<div id='div_gold_weight' class='col-md-12'>";
                    $absolute_menu .= "<select id='gold_weight' name='gold_weight' class='select-62hallfamily'>";
                    $absolute_menu .= "<option value='' disabled selected hidden></option>";
                    if (count($gold_pecahan) > 0) {
                        foreach ($gold_pecahan as $weight) {
                            $selected = '';
                            if ($weight == $gold_weight) {
                                $selected = 'selected';
                            }
                            $absolute_menu .= "<option value='" . $weight . "'  " . $selected . ">" . $weight . " gr</option>";
                        }
                    }
                    $absolute_menu .= "</select>";
                    $absolute_menu .= "</div>";
                    $absolute_menu .= "</div>";

                    $absolute_menu .= "<div class='row'>";
                    $absolute_menu .= "<div class='col-md-12 margin-top-40'>";
                    $absolute_menu .= "<input type='submit' class='col-md-12 margin-top-10 btn-62hallfamily bg-yellow font-size22 border-none' value='Cari'>";
                    $absolute_menu .= "</div>";
                    $absolute_menu .= "</div>";

                    $absolute_menu .= "</form>";
                    $absolute_menu .= "</div>";
                    break;
                }
            case "service-search" : {
                    $category_id = carr::get($this->options, 'category_id');

                    $service_category = product_category::get_product_category_menu('service');
                    $absolute_menu = "";
                    $absolute_menu .= "<div class='font-size32 service-categories-home bold col-md-12'>Temukan jasa yang Anda butuhkan disini sekarang juga !";
                    $absolute_menu .= "<form class='form-62hallfamily font-size20' action='" . curl::base() . 'service/product/category' . "' method='GET'>";

                    $absolute_menu .= "<div class='row'>";
                    $absolute_menu .= "<div class='col-md-12 margin-top-20'>Get Started</div>";
                    $absolute_menu .= "</div>";

                    $absolute_menu .= "<select name='category' class='select-62hallfamily normal'>";
                    $absolute_menu .= "<option value='' selected hidden>Kategori Jasa</option>";

                    foreach ($service_category as $data_service_category) {
                        $selected = '';
                        if ($data_service_category['category_id'] == $category_id) {
                            $selected = 'selected';
                        }
                        $absolute_menu .= "<option value='" . $data_service_category['category_id'] . "' " . $selected . ">" . $data_service_category['name'] . "</option>";
                    }
                    $absolute_menu .= "</select>";

                    //            $absolute_menu .= "<div class='row'>";
                    //            $absolute_menu .= "<div class='col-md-12 margin-top-20'>Kota</div>";
                    //            $absolute_menu .= "</div>";
                    //            $absolute_menu .= "<select class='select-62hallfamily normal'>";
                    //            $absolute_menu .= "<option value='' disabled selected hidden><span class='font-gray-soft normal'>Kota...</span></option>";
                    //            
                    //            $absolute_menu .= "</select>";
                    //            $select_city = CFormInput62HallFamilyInput_CitySelect::factory('service-city');
                    //            $html_select_city = $select_city->html();
                    //            $app->add_js($select_city->js());
                    //            
                    //            $absolute_menu .= $html_select_city;
                    $absolute_menu .= "<div class='row'>";
                    $absolute_menu .= "<div class='col-md-12 margin-top-40'>";
                    $absolute_menu .= "<input type='submit' class='col-md-12 margin-top-10 btn-62hallfamily bg-yellow font-size24 border-none' value='Cari'>";
                    $absolute_menu .= "</div>";
                    $absolute_menu .= "</div>";

                    $absolute_menu .= "</form>";
                    $absolute_menu .= "</div>";
                    break;
                }
            case "shinjuku-search" : {
                    $category_id = carr::get($this->options, 'category_id');

                    $shinjuku_category = product_category::get_product_category_menu('shinjuku');
                    $absolute_menu = "";
                    $absolute_menu .= "<div class='font-size32 shinjuku-categories-home bold col-md-12'>Temukan item Shinjuku yang Anda butuhkan disini sekarang juga !";
                    $absolute_menu .= "<form class='form-62hallfamily font-size20' action='" . curl::base() . 'shinjuku/product/category' . "' method='GET'>";

                    $absolute_menu .= "<div class='row'>";
                    $absolute_menu .= "<div class='col-md-12 margin-top-20'>Get Started</div>";
                    $absolute_menu .= "</div>";

                    $absolute_menu .= "<select name='category' class='select-62hallfamily normal'>";
                    $absolute_menu .= "<option value='' disabled selected hidden>Kategori Shinjuku</option>";

                    foreach ($shinjuku_category as $data_shinjuku_category) {
                        $selected = '';
                        if ($data_shinjuku_category['category_id'] == $category_id) {
                            $selected = 'selected';
                        }
                        $absolute_menu .= "<option value='" . $data_shinjuku_category['category_id'] . "' " . $selected . ">" . $data_shinjuku_category['name'] . "</option>";
                    }
                    $absolute_menu .= "</select>";

                    $absolute_menu .= "<div class='row'>";
                    $absolute_menu .= "<div class='col-md-12 margin-top-40'>";
                    $absolute_menu .= "<input type='submit' class='col-md-12 margin-top-10 btn-62hallfamily bg-yellow font-size24 border-none' value='Cari'>";
                    $absolute_menu .= "</div>";
                    $absolute_menu .= "</div>";

                    $absolute_menu .= "</form>";
                    $absolute_menu .= "</div>";
                }
        }
        return $absolute_menu;
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);

        $html->appendln('<div class="bg-gray-dark menu">');
        $html->appendln('<div class="container container-menu">');
        $html->appendln('<div class="row bold">');
        $html->appendln('<div class="">');

        if (!empty($this->absolute_menu) and $this->position == 'left') {
            $html->appendln('<ul class="nav navbar-nav bigmall-left">');
        } else if (!empty($this->absolute_menu) and $this->position == 'right') {
            $html->appendln('<ul class="nav navbar-nav bigmall-right">');
        } else {
            $html->appendln('<ul class="nav navbar-nav">');
        }
        if (empty($this->absolute_menu)) {
//            $html->appendln('<li id="nav-side-bar" class="link padding-0">
//					<i class="fa fa-navicon font-red"></i><label class="nav-side-bar-label">&nbspKATEGORI</label>
//				</li>');

            $html->appendln('<li id="nav-side-bar" class="link padding-0">
                    <i class="fa fa-list font-red"></i><label class="nav-side-bar-label">&nbspKATEGORI</label>');
            $html->appendln('<span id="menu-icon-toggle" class="menu-icon-toggle hidden-md hidden-sm hidden-lg pull-right"><a href="#"></a>
            <a href="#"></a>
            <a href="#"></a></span>');
            $html->appendln('</li>');
        }

        if (!empty($this->absolute_menu) and $this->position == 'left') {
            //$html->appendln('<li class="categories-absolute"><a></a></li>');
        }

        $url_menu = $this->url_menu;
        if (empty($url_menu)) {
            $url_menu = curl::base() . $this->url_page . 'search?keyword=&category=';
        }
        if (isset($this->menu['name'])) {
            $html->appendln('<li class="dropdown bg-red font-white categories normal category-menu bigmall-menu-category">');
            $html->appendln('<a href="#" class="dropdown-toggle font-size18" data-toggle="dropdown" role="button" aria-expanded="false">' . $this->menu['name'] . '</a>');
            $html->appendln('<ul id="menu-categories" class="dropdown-menu categories bg-black padding-0"  role="menu">');

            $html->appendln($this->custom_menu);

            $trigger_class = "dropdown-submenu-hover";
            if ($this->trigger_type == "click") {
                $trigger_class = "dropdown-submenu-click";
            }
            $arr_menu=carr::get($this->menu,'menu',array());
            foreach ($arr_menu as $data_menu) {
                // sub menu
                $url = $url_menu . carr::get($data_menu, 'url_key');
                if (count($data_menu['subnav']) > 0) {
                    if ($this->trigger_type == 'click') {
                        $url = 'javascript:;';
                    }
                    $html->appendln('<li class="dropdown-submenu ' . $trigger_class . ' categories-submenu"><a href="' . $url . '">'
                            . '<div class="icon-"></div>'
                            . $data_menu['name']
                            . '</a>');
                    $html->appendln('<ul class="dropdown-menu padding-0 bg-gray child-1">');
                    if ($this->trigger_type == 'click') {
                        $html->appendln('<li><a class="font-black link-submenu-all" href="' . $url_menu . carr::get($data_menu, 'url_key') . '">'
                                . '<div class="icon-"></div>'
                                . 'Semua ' . $data_menu['name']
                                . '</a>');
                    }
                    $arr_submenu=carr::get($data_menu,'subnav',array());
                    foreach ($arr_submenu as $submenu) {
                        // sub sub menu
                        $url = $url_menu . carr::get($submenu, 'url_key');

                        if (count($submenu['subnav']) > 0) {
                            if ($this->trigger_type == 'click') {
                                $url = 'javascript:;';
                            }
                            $html->appendln('<li class="dropdown-submenu ' . $trigger_class . ' categories-submenu"><a class="font-black" href="' . $url . '">'
                                    . '<div class="icon-"></div>'
                                    . $submenu['name']
                                    . '</a>');
                            $html->appendln('<ul class="dropdown-menu padding-0 bg-white child-2">');
                            if ($this->trigger_type == 'click') {
                                $html->appendln('<li><a class="font-black link-submenu-all" href="' . $url_menu . carr::get($submenu, 'url_key') . '">'
                                        . '<div class="icon-"></div>'
                                        . 'Semua ' . $submenu['name']
                                        . '</a>');
                            }
                            foreach ($submenu['subnav'] as $subsubmenu) {
                                $url = $url_menu . carr::get($subsubmenu, 'url_key');
                                $html->appendln('<li><a class="font-black" href="' . $url . '">'
                                        . '<div class="icon-"></div>'
                                        . $subsubmenu['name']
                                        . '</a>');
                            }
                            $html->appendln('</ul>');
                            $html->appendln('</li>');
                        } else {
                            $html->appendln('<li><a class="font-black" href="' . $url_menu . carr::get($submenu, 'url_key') . '">'
                                    . '<div class="icon-"></div>'
                                    . $submenu['name']
                                    . '</a>');
                        }
                    }
                    $html->appendln('</ul>');
                    $html->appendln('</li>');
                } else {
                    $html->appendln('<li><a href="' . $url_menu . $data_menu['url_key'] . '">'
                            . '<div class="icon-"></div>'
                            . $data_menu['name']
                            . '</a>');
                }

                $html->appendln('</li>');
            }
            $html->appendln('</ul>');
            $html->appendln('</li>');
        }

        if (isset($this->link)) {
            foreach ($this->link as $data_link) {
                $redirect=carr::get($data_link,'redirect');
                $new_window='';
                if($redirect){
                    $new_window='target="_blank"';
                }
                $html->appendln('<li class="inline-block bigmall-menu afx"><a class="font-size18 normal" href="' . $data_link['url'] . '" '.$new_window.'>' . $data_link['label'] . '</a></li>');
            }
        }


        $html->appendln('</ul>');

        if (!empty($this->absolute_menu) and $this->position == 'left') {
            $html->appendln('<div class="categories absolute-menu absolute-left bg-red padding-40 font-white">');
            $html->appendln($this->generate_absolute_menu($this->absolute_menu));
            $html->appendln('</div>');
        }

        if (!empty($this->absolute_menu) and $this->position == 'right') {
            $html->appendln('<div class="categories absolute-menu absolute-right bg-red padding-40 font-white">');
            $html->appendln($this->generate_absolute_menu($this->absolute_menu));
            $html->appendln('</div>');
        }

        $html->appendln('</div>');
        $html->appendln('</div>');
        $html->appendln('</div>');
        $html->appendln('</div>');

        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();
        if (!empty($this->absolute_menu)) {
            switch ($this->absolute_menu) {
                case 'gold-search': {
                        $data_addition = "'category':$.cresenity.value('#category')";
                        $data_addition = '{' . $data_addition . '}';
                        $js->appendln("
						$('#category').change(function(){
							$.cresenity.reload('div_gold_weight','" . curl::base() . "reload/reload_gold_weight/','get'," . $data_addition . " );
						});
					");
                    }
            }
        }

        $js->appendln("
            $('.container-menu .dropdown-submenu.dropdown-submenu-click').click(function (evt) {
                evt.stopPropagation();
                if($(this).hasClass('open')) {
                    $(this).removeClass('open');
                } else {
                    $(this).parent().find('.dropdown-submenu.dropdown-submenu-click').removeClass('open');
                    $(this).addClass('open');
                }            });
            
            $('#menu-icon-toggle').click(function(e){
                e.stopPropagation();
                e.preventDefault();
                var i = 1;
                $(this).parent().parent().find('.afx').each(function(){
                    if($(this).hasClass('afx-hide')){
                        $(this).removeClass('afx-hide');
                    }else{
                        $(this).addClass('afx-hide');
                    }
                });
            });
            
            ");
        $js->append(parent::js($indent));
        return $js->text();
    }

}
