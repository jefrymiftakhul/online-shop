<?php

class CElement_Shinjuku_Detail extends CElement{
    
    private $key = NULL;
    private $detail_list = array();
    private $price = 0;
    private $price_promo = NULL;
    
    private $form = NULL;
    private $type_select = "select";
    private $type_image = "image";
    private $attribute_list = array();
    
    private $with_qty = TRUE;

    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public static function factory($id = ''){
        return new CElement_Shinjuku_Detail($id);
    }
    
    public function set_key($key){
        $this->key = $key;
        return $this;
    }
    
    public function set_minimum_stock($min_stock){
        $this->minimum_stock = $min_stock;
        return $this;
    }
    
    public function set_detail_list($detail_list){
        $this->detail_list = $detail_list;
        return $this;
    }
    
    public function set_attribute_list($attribute_list){
        $this->attribute_list = $attribute_list;
        return $this;
    }
   
    public function with_qty($with_qty){
        $this->with_qty = $with_qty;
        return $this;
    }
    
    private function get_price(){
        $detail_price = $this->price;
        
        $arr_price = product::get_price($detail_price);
        
        return $arr_price;
    }
    
    private function generate_list($type, $data = array()){
        $list = array();
        
        foreach ($data as $key => $row_data){
            if($type == $this->type_select){
                $value = carr::get($row_data, 'attribute_key');
            }
            if($type == $this->type_image){
                $value['key'] = carr::get($row_data, 'attribute_key');
                $value['value'] = carr::get($row_data, 'file_path');
            }
            
            $list[$key] = $value;
        }
        
        return $list;
    }
    
    private function generate_form($container, $code, $data){
        $id = carr::get($data, 'attribute_category_id');
        $type = carr::get($data, 'attribute_category_type');
        $prev = carr::get($data, 'prev_attribute_category');
        $next = carr::get($data, 'next_attribute_category');
        $attribute = carr::get($data, 'attribute', array());
        $list = $this->generate_list($type, $attribute);
        $control=array();
        foreach ($prev as $row_prev){
            $arr_param[]='att_cat_'.$id ;

        }
        if($type == $this->type_select){
            $div_container = $container->add_div('container-' . $code);
            $select = $div_container->add_field()
                ->add_control('att_cat_'.$id , 'select')
                ->add_class('select-62hallfamily margin-bottom-10')
                ->custom_css('margin-left', '15px')
                ->custom_css('width', '130px')
                ->set_list($list);
            $control[]=$select;
        }
        else if($type == $this->type_image){
            $div_container = $container->add_div('container-' . $code)
                    ->add_class('container-input-image');
            $index = 1;
            foreach ($list as $key => $value){
                $active = NULL;
                if($index == 1){
                    $active = 'active';
                    $div_container->add_control('att_cat_'.$id , 'hidden')
                        ->set_value($key);
                }
                $image = $div_container->add_div($key)
                    ->add_class('btn-colors margin-bottom-10 link ' . $active)
                    ->custom_css('display', 'inline-block')
                    ->custom_css('padding', '2px')
                    ->add('<img src="' . $value['value'] . '" style="width:20px; height:20px"/>');
				
				$listener = $image->add_listener('click');
				
				$handler = $listener->add_handler('custom')
						->set_js("
							var key=$(this).attr('id');
							$('#".'att_cat_'.$id ."').val(key);
						");
					
				$control[]=$image;
                
                $index++;
            }
        }
		if(count($control)>0){
			foreach($control as $obj){
				if(!empty($next) and count($list) > 0){
					$listener = $obj->add_listener('ready');
					
					$handler = $listener->add_handler('reload')
							->set_target('container-' . $next)
							->add_param_input($arr_param)
							->set_url(curl::base() . 'shinjuku/product/generate_attribute/' . $next . '/' . $this->key);
                                        
					$listener = $obj->add_listener('click');
					
					$handler = $listener->add_handler('reload')
							->set_target('container-' . $next)
							->add_param_input($arr_param)
							->set_url(curl::base() . 'shinjuku/product/generate_attribute/' . $next . '/' . $this->key);
                                        
                                }         
			}			
		}
        
        return $container;
    }
    
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        
        $product_id = carr::get($this->detail_list, 'product_id', NULL);
        $name = carr::get($this->detail_list, 'name', NULL);
        $sku = carr::get($this->detail_list, 'sku', NULL);
        $availability = carr::get($this->detail_list, 'is_available', NULL);
        $quick_overview = carr::get($this->detail_list, 'quick_overview', NULL);
        $this->price = carr::get($this->detail_list, 'detail_price', 0);
        $detail_price = carr::get($this->detail_list, 'detail_price', 0);
        
        $array_size = carr::get($this->detail_list, 'size');
        $array_color = carr::get($this->detail_list, 'color');
        
        $html->appendln('<div class="detail-product margin-20">');
        $html->appendln('<h3 class="bold">' . $name . '</h3>');
        
        $html->appendln('<br><br>');
        $html->appendln('<div class="font-big bold">Rincian Singkat</div>');
        $html->appendln($quick_overview);
        
        $price = $this->get_price();
        
        $html->appendln('<br><br>');
        if ($availability>0) {
        
			if($price['promo_price'] > 0){
				$html->appendln('<strike class="font-black"> Rp. ' . ctransform::format_currency($price['price']) . '</strike>');
				$html->appendln('<div class="font-red font-size24 bold"> Rp. ' . ctransform::format_currency($price['promo_price']) . '</div>');

			}
			else{
				$html->appendln('<div class="font-red font-size24 bold"> Rp. ' . ctransform::format_currency($price['price']) . '</div>');
			}
		}else{
            $html->appendln('<div class="font-black bold">Kisaran Harga</div><div class="font-red font-size24 bold">Rp. ' . ctransform::format_currency($detail_price['sell_price_start']) . ' - Rp. ' . ctransform::format_currency($detail_price['sell_price_end']) . '</div>');
		}
        
        $form = $this->add_form('form-detail-product')->set_action(curl::base() . 'request/add/');
		$form->add_control('product_id','hidden')->set_value($product_id);
		$form->add_control('page','hidden')->set_value('shinjuku');
        $rowform = $form->add_div()->add_class('row');
        
        // form attribute
        $form_atribute = $rowform->add_div()->add_class('col-md-12');
        foreach ($this->attribute_list as $code_attribute => $attribut_list){
            $form_atribute->add_div()->add_class('bold font-size16 margin-bottom-10')->add($attribut_list['attribute_category_name']);
            $this->generate_form($form_atribute, $code_attribute, $attribut_list);
        }
        
        $rowform = $form->add_div()->add_class('row margin-top-20');
        $leftform = $rowform->add_div()->add_class('col-md-3');
        $rigthform = $rowform->add_div()->add_class('col-md-8');
        
		if($availability>0){
			$action = $leftform->add_action()
					->set_label(clang::__('<i class="fa fa-send"></i> Pesan'))
					->add_class('btn-62hallfamily bg-red border-3-red')
					->custom_css('height', '34px')
					->set_link(curl::base() . 'shinjuku/updatecontact/update_contact/' . $product_id);
        }else{
            $action = $leftform->add_action()
                    ->set_label('Request')
                    ->add_class('btn-62hallfamily bg-red border-3-red')
                    ->custom_css('height', '34px')
                    ->set_submit_to(curl::base() . 'request/add/')
                    ->set_submit(true);
		
		}
        $html->appendln('</div>');
        
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent=0) {
        $js = new CStringBuilder();
        
        $js->append(parent::js($indent));
        return $js->text();
    }
}
