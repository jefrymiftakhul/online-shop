<?php

class CElement_Imparoparo_Product_DetailInfo extends CElement
{

    private $url_shared_facebook  = null;
    private $url_shared_twitter   = null;
    private $url_shared_google    = null;
    private $text_twitter         = '';
    private $url_shared_instagram = null;
    private $info_pengiriman      = null;
    private $product_id           = '';
    private $shipping_day         = '(2-28 hari)';
    private $page = null;
    
    public function __construct($id = '')
    {
        parent::__construct($id);
    }
    
    public static function factory($id = '')
    {
        return new CElement_Imparoparo_Product_DetailInfo($id);
    }
    
    public function set_page($val){
        $this->page=$val;
        return $this;
    }
    
    public function set_url_shared_facebook($url)
    {
        $this->url_shared_facebook = $url;
        return $this;
    }

    public function set_url_shared_twitter($url)
    {
        $this->url_shared_twitter = $url;
        return $this;
    }

    public function set_url_shared_google($url)
    {
        $this->url_shared_google = $url;
        return $this;
    }

    public function set_text_twitter($text)
    {
        $this->text_twitter = $text;
        return $this;
    }

    public function set_url_shared_instagram($url)
    {
        $this->url_shared_instagram = $url;
        return $this;
    }

    public function set_info_pengiriman($info_pengiriman)
    {
        $this->info_pengiriman = $info_pengiriman;
        return $this;
    }

    public function set_product_id($id)
    {
        $this->product_id = $id;
        return $this;
    }

    public function html($indent = 0)
    {

        $html = new CStringBuilder();
        $html->set_indent($indent);
        $shipping_text = '<ul class="shipping-info">';
//        if (strlen($this->product_id) > 0) {
//            $arr_product_city = shipping::get_shipping_city_info($this->product_id);
//            if (count($arr_product_city) > 0) {
//                foreach ($arr_product_city as $province => $val) {
//                    $city      = carr::get($val, 'city');
//                    $city_name = '';
//                    if (!isset($city['all'])) {
//                        $city      = array_keys($city);
//                        $city      = array_map('strtolower', $city);
//                        $city      = array_map('ucwords', $city);
//                        $city_name = implode(',', $city);
//                        //$city_name=ucwords(strtolower($city_name),',');
//                    }
//                    $shipping_text .= '<li>' . $province . ' ' . $this->shipping_day;
//                    if (strlen($city_name) > 0) {
//                        $shipping_text .= ' (' . $city_name . ') ';
//                    }
//                    $shipping_text .= '</li>';
//                    
//                }
//            }else{
//                $shipping_text .= '<li>ALL INDONESIA ' . $this->shipping_day.'</li>';
//                $shipping_text .= '<li>JAWA (2-14 hari kerja)<br/>LUAR JAWA (14-28 hari kerja)</li>';
//
//            }
//        }
        if($this->page=='product'){
            $shipping_text.="<li>Batas komplain untuk product yang di terima adalah 3 Hari (Hari kerja Senin-Jumat) setelah barang di terima</li>";
        }
        $shipping_text .= '</ul>';
        
         $share_social = $this->add_div()->add_class('share-social');

        $shared = $share_social->add_div('shared')
                                  ->add_class('tombol font-size20 bold border-3-red link')
                                  ->add('<div style="cursor:pointer;display:inline-block;" class="icon-share" aria-hidden="true"> </div><div style="display:inline-block;font-size:13px;vertical-align: middle;
    margin-top: -25px;cursor:pointer;"> Share </div> ');
        
        $shared_container = $share_social->add_div()
                                            ->add_class('container-detail container-shared bg-white font-black border-3-red')
                                            
                                            ->custom_css('position', 'absolute')
//                                            ->custom_css('width', '140px')
//                                            ->custom_css('margin-top', '10px')
//                                            ->custom_css('margin-left', '-70px')
//                                            ->custom_css('display','none')
//                                            ->custom_css('padding','5px')
//                                            ->custom_css('box-shadow',' -1px 1px 13px 0px rgba(204,204,204,1)')
                                           
                                            
                                            ->add(''
                                                    .'<div style="display:inline-block;font-size:13px;vertical-align: middle;
    margin-top: -15px;"> Share to </div> '
                                                . '<div class="icon-facebook-share margin-right-10 facebook link" style="display: inline-block"></div>'
                                                . '<div class="icon-twitter-share margin-right-10 twitter link" style="display: inline-block"></div>'
                                                . '<div class="icon-google-share google link" style="display: inline-block"></div>'
                                                //. '<div class="icon-instagram-red" style="display:inline-block"></div>'
                                                 . '');
        $shared_container_social = $shared_container->add_div()
                                        ->add_class('container-detail-arrow');

        // Masih belum berfungsi
        //
        //$shipping_text.=' <br/><br/>Sehubungan dengan jadwal libur lebaran penjual dan jasa pengiriman, pesanan Anda mungkin akan mengalami keterlambatan pengiriman. Pengiriman akan kembali normal setelah hari Senin 11 Juli 2016.';

//        $pengiriman = $div_detail_info->add_div('pengiriman')
//                                      ->add_class('tombol bg-red font-size20 bold border-3-red link margin-top-10')
//                                      ->add('Info Pengiriman');
//        $pengiriman_container = $div_detail_info->add_div()
//                                                ->add_class('container-detail container-pengiriman bg-white font-black margin-bottom-10 border-3-red')
//                                                ->custom_css('display', 'block')
//                                                ->add($shipping_text);
//
        //        $email_us = $div_detail_info->add_div('email')
        //                ->add_class('tombol bg-red font-size20 bold border-3-red link margin-top-10')
        //                ->add('Email Us');
        //        $email_us_container = $div_detail_info->add_div()
        //                ->add_class('container-detail container-email bg-white font-black margin-bottom-10 border-3-red')
        //                ->custom_css('display', 'none');
        //
        //        $form_email_us = $email_us_container->add_form()
        //                ->add_class('form-62hallfamily');
        //
        //        $form_email_us->add_div()
        //                ->add('Email');
        //        $form_email_us->add_control('', 'text')
        //                ->set_name('email');
        //
        //        $form_email_us->add_div()
        //                ->add('Detail');
        //        $form_email_us->add_control('', 'textarea')
        //                ->set_name('detail')
        //                ->set_row(2)
        //                ->custom_css('width', '100%');
        //
        //        $action = $form_email_us->add_div()->add('<center>');
        //        $action->add_action()
        //            ->set_label('Submit')
        //            ->add_class('btn-62hallfamily bg-red border-3-red margin-top-10');
        //
        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0)
    {
        $js = new CStringBuilder();

        $js->appendln("
                $('.tombol').click(function(){
                    var id = $(this).attr('id');
                    $('.container-'+id).toggle();
                });
               ");
        $text_twitter = str_replace("\r\n", '', $this->text_twitter);
        $text_twitter = strip_tags($text_twitter);

        $js->appendln("
            $('.facebook').click(function(){
                var fbpopup = window.open('//www.facebook.com/sharer/sharer.php?u=" . $this->url_shared_facebook . "', 'pop', 'width=600, height=400, scrollbars=no');
                return false;
            });
            $('.twitter').click(function(){
                                var twpopup = window.open(\"//twitter.com/intent/tweet?url=" . $this->url_shared_twitter . "&text=" . $text_twitter . "&original_referer=" . $this->url_shared_twitter . "\",600, 400);
                return false;
            });
             $('.google').click(function(){
                                var twpopup = window.open('//plus.google.com/share?url=" . $this->url_shared_google . "','', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=600');
                return false;
            });

        ");
        $js->append(parent::js($indent));
        return $js->text();
    }

}
