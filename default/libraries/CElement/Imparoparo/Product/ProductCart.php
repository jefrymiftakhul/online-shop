<?php

class CElement_Imparoparo_Product_ProductCart extends CElement {

    private $key;
    private $cart_list = array();
    private $count_item;
    private $bool_image_chart = false;
    private $shipping_price;

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_Imparoparo_Product_ProductCart($id);
    }

    public function set_key($key) {
        $this->key = $key;
        return $this;
    }

    public function set_cart_list($cart_list) {
        $this->cart_list = $cart_list;
        return $this;
    }

    public function set_count_item($count_item) {
        $this->count_item = $count_item;
        return $this;
    }

    public function set_image_from_chart($bool) {
        $this->bool_image_chart = $bool;
        return $this;
    }

    public function set_shipping_price($shipping_price) {
        $this->shipping_price = $shipping_price;
        return $this;
    }

    private function get_image($image_name) {
        $ch = curl_init(image::get_image_url($image_name, 'view_all'));
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_exec($ch);
        $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($retcode == 500 or $image_name == NULL) {
            $image_name = curl::base() . 'application/ittronmall/default/media/img/product/no-image.png';
        } else {
            $image_name = image::get_image_url($image_name, 'view_all');
        }
        curl_close($ch);
        return $image_name;
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        $session = CSession::instance();
        $cart = Cart62hall_Product::factory();
        $cart_list = $cart->get_cart();
        $cart_list_image = $cart->get_cart();
        $count_item = $cart->item_count();
        $product_voucher_id = '';
        $voucher_code = '';
        $nominal_voucher = 0;
        $use_image_chart = $this->bool_image_chart;

        $data_voucher = $session->get('voucher_data');
        $is_package_product = $session->get('is_package_product');
        if (count($data_voucher) > 0) {
            $voucher_code = $data_voucher['voucher_code'];
            $type = $data_voucher['type'];
            $nominal_voucher = $data_voucher['nominal'];
            $product_voucher_id = $data_voucher['product_voucher_id'];
        }

//        cdbg::var_dump($image_chart);

        if (count($this->cart_list) > 0) {
            $cart_list = $this->cart_list;
        }

        if ($this->count_item > 0) {
            $count_item = $this->count_item;
        }

//        cdbg::var_dump($cart_list);
//        cdbg::var_dump($cart_list_image);
//        
        $data = array();
        foreach ($cart_list as $key => $value) {
            $data[$key] = $value;
            foreach ($cart_list_image as $key_img => $value_img) {
                if ($key == $key_img) {
                    $data[$key] = $value_img;
                }
            }
        }

//        $aaa = array_merge_recursive($cart_list,$cart_list_image);
//        cdbg::var_dump($aaa);
//        die();
        $div_shopping_cart = $this->add_div()->add_class('col-md-12 container-cart margin-bottom-20');
        $total_harga = 0;
        $total_harga_before_voucher = 0;
        $show_input_voucher = false;
        $shipping_price = '-';
        if (strlen($this->shipping_price) > 0) {
            $shipping_price = $this->shipping_price;
        }

        if ($count_item > 0) {
            $items = $div_shopping_cart->add_div()->add_class('horizontal-overflow');

            foreach ($data as $data_cart) {
                $product_id = $data_cart['product_id'];
                if (strlen($product_id) > 0) {
                    $total_harga += $data_cart['sub_total'];
                    $total_harga_before_voucher = $total_harga;
//                    $image = $this->get_image($data_cart['image']);
                    $image = $data_cart['image'];
                    if (ccfg::get('is_direct_image')) {
                        $image = $data_cart['image'];
                    }


                    $container_item = $items->add_div()->add_class('container-item');
                    $container_item_left = $container_item->add_div()->add_class('container-item-left');
                    $container_item_right = $container_item->add_div()->add_class('container-item-right');
//                    $container_item_left->add_div()->add_class('container-item-qty')->add_div()->add_class('product-cart-qty')->add($data_cart['qty']);
                    $wrapper_image = $container_item_left->add_div()->add_class('outside-element-center-vertical wrapper-image');
//                    $wrapper_image->add('<span class="helper-image"></span>');
                    $wrapper_image->add("<img class='element-center-vertical' src='" . $image . "'>");
                    $content_right = $container_item_right->add_div()->add_class('content-item-right');
                    $content_right->add_div()->add_class('product-contact-title')->add($data_cart['name']);

                    $container_price_qty = $content_right->add_div()->add_class('container-price-qty');
                    $container_price_qty->add_div()->add_class('product-contact-total')->add($data_cart['qty'] . 'x');
                    $container_price_qty->add_div()->add_class('product-contact-price')->add_class('text-right')->add('Rp ' . ctransform::format_currency($data_cart['sell_price']));
                } else {
                    $shipping_price = $data_cart['sell_price'];
                }
            }
            if (strlen($voucher_code) > 0) {
                $cek_voucher = product::voucher_product($voucher_code);
                if ($cek_voucher['err_code'] > 0) {
                    $voucher_code = '';
                } else {
                    if ($type == 'percent') {
                        $total_harga_diskon = $total_harga * ($nominal_voucher / 100);
                    } else {
                        $total_harga_diskon = $nominal_voucher;
                    }
                    $total_harga = $total_harga - $total_harga_diskon;
                    $show_input_voucher = true;
                }
            }
            $container_subtotal = $items->add_div()->add_class('container-contact-subtotal');
            $container_subtotal_row = $container_subtotal->add_div()->add_class('row');
            $container_subtotal_left = $container_subtotal_row->add_div()->add_class('col-xs-6')->add(clang::__('Subtotal'));
            $container_subtotal_right = $container_subtotal_row->add_div()->add_class('col-xs-6 text-right bold')->add('Rp ' . ctransform::format_currency($total_harga_before_voucher));


            $container_grand_total = $items->add_div()->add_class('container-grand-total');
            if ($shipping_price != '-' || $shipping_price == 0) {
                $total_harga = $total_harga + $shipping_price;

                if ($show_input_voucher) {
                    $container_subtotal_row->add_div()->add_class('col-xs-6')->add(clang::__('Voucher'));
                    $container_subtotal_row->add_div()->add_class('col-xs-6 text-right bold')->add('- Rp ' . ctransform::format_currency($total_harga_diskon));
                } else {
                    $items->add_control('reload_from', 'hidden')->set_value('ready');
                    $container_grand_total->add_div('voucher_shop');
                    $container_grand_total->add_listener('ready')
                            ->add_handler('reload')
                            ->set_target('voucher_shop')
                            ->add_param_input(array('voucher', 'reload_from'))
                            ->set_url(curl::base() . "reload/get_voucher");
                }
            }
            $container_subtotal_row = $container_subtotal->add_div()->add_class('row');
            $container_subtotal_left = $container_subtotal_row->add_div()->add_class('col-xs-6')->add(clang::__('Pengiriman'));
            $container_subtotal_right = $container_subtotal_row->add_div()->add_class('col-xs-6 text-right bold')->add('<span id="shipping-price-text">'.'Rp ' . ctransform::format_currency($shipping_price).'</span>');

            $grand_total_row = $container_grand_total->add_div()->add_class('row');
            $grand_total_left = $grand_total_row->add_div()->add_class('col-xs-4 txt-total')->add(clang::__('Total'));
            $grand_total_right = $grand_total_row->add_div()->add_class('col-xs-8 txt-total-price')->add('<span id="total-price-text">'.'Rp ' . ctransform::format_currency($total_harga).'</span>');
        }

        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $js->append(parent::js($indent));
        return $js->text();
    }

}
