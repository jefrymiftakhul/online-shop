<?php

class CElement_Imparoparo_Product_Gallery extends CElement {

    protected $images = array();
    protected $page = 'product';
    protected $product_id = null;

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_Imparoparo_Product_Gallery($id);
    }

    public function set_images($images) {
        $this->images = $images;
        return $this;
    }

    public function set_page($val) {
        $this->page = $val;
        return $this;
    }
    
    public function set_product_id($id) {
        $this->product_id = $id;
        return $this;
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        $first_image = curl::base() . 'application/ittronmall/default/media/img/product/no-image.png';


        foreach ($this->images as $key => $value) {
            $image_path = carr::get($value, 'image_path');
            $package = carr::get($value, 'is_package_product');
            $is_active = '';
            if ($key == 0) {
                $first_image = $image_path;
                $is_active = ' active ';
            }
        }

        $a = $this->images;
//        cdbg::var_dump($a);

        $image1 = isset($this->images[0]['image_path']) ? image::get_image_url($this->images[0]['image_path'], $this->page) : '';
        $image_large = isset($this->images[0]['image_path']) ? image::get_image_url($this->images[0]['image_path']) : '';

        $zoom_class = "cloud-zoom";
        $image_path = isset($this->images[0]['image_path']) ? $this->images[0]['image_path'] : curl::base() . 'application/ittronmall/default/media/img/product/no-image.png';


        if (!image::is_exists($image_path)) {
            //$image1 = curl::base().'application/62hallfamily/default/media/img/product/no-image.png';
            //$zoom_class = "";
        }
        
        $image_sold_out = '';
        
        // <editor-fold defaultstate="collapsed" desc="soldout old version">
//        $stock = 0;
//        $is_available = 0;
//        $product_date_type = '';
//        
//        if(strlen($this->product_id)>0){
//            $product = product::get_product($this->product_id);
//            $stock = carr::get($product, 'stock');
//            $is_available = carr::get($product, 'is_available');            
//            $product_date_type = carr::get($product, 'product_date_type');            
//        }
//        
//        if($stock==0 && $is_available!=0){
//            $image_sold_out = '<img src="' . curl::base() . 'application/ittronmall/default/media/img/Imparoparo/sprite-64.png" class="img-sold-out-single">';
//        }
        // </editor-fold>
        
        if(product::is_soldout($this->product_id)){
            $image_sold_out = '<img src="' . curl::base() . 'application/ittronmall/default/media/img/Imparoparo/sprite-64.png" class="img-sold-out-single">';
        }

        $html->appendln('<div class="product-single-image">');
        $html->appendln('<div id="product-slider">');
        $html->appendln('<ul class="slides">');
        $html->appendln('<li>
                            <img class="main-image ' . $zoom_class . '"  src="' . $image_path . '" data-large="' . $first_image . '" alt="" />
                                '.$image_sold_out.'
                            <a class="fullscreen-button" href="' . $image_path . '">
                                <div class="product-fullscreen">
                                    <i class="icons icon-resize-full-1"></i>
                                </div>
                            </a>
                        </li>');
        $html->appendln('</ul>');
        $html->appendln('</div>');
        
        
        if (count($this->images) > 1) {
            if (strlen($package) == 0) {

                $html->appendln('<div id="product-carousel" style="height: 100px">');
                $html->appendln('<ul class="slides product-gallery margin-top-10">');

                foreach ($this->images as $key => $value) {
                    $image_path = carr::get($value, 'image_path');

                    $replace_thumbs = str_replace("/res/show/", "/res/show/thumbnails/", $image_path);
                    $replace_large = str_replace("/res/show/", "/res/show/product/", $image_path);

                    if ($key == 0) {
                        $first_image = $image_path;
                        $is_active = ' active ';
                    }

                    $html->appendln('<li>
                                <a class="fancybox" rel="product-images" href="' . $image_path . '"></a>
                                <img src="' . $replace_thumbs . '" data-large="' . $image_path . '" alt=""/>
                            </li>');
                }
                $html->appendln('</ul>');
                $html->appendln('<div class="product-arrows">
                                <div class="left-arrow">
                                        <i class="fa fa-chevron-left"></i>
                                </div>
                                <div class="right-arrow">
                                        <i class="fa fa-chevron-right"></i>
                                </div>
                        </div>');
                $html->appendln('</div>');
            }
        }
        $html->appendln('</div>');


        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();


        $script = "gallery_product();
                
                function gallery_product(){
		
                    $('#product-carousel').flexslider({
                        animation: 'slide',
                        controlNav: false,
                        animationLoop: false,
                        directionNav: false,
                        slideshow: false,
                        itemWidth: 63,
                        itemMargin: 0,
                        start: function(slider){

                        setActive($('#product-carousel li:first-child img'));
                        slider.find('.right-arrow').click(function(){
                                slider.flexAnimate(slider.getTarget('next'));
                        });

                        slider.find('.left-arrow').click(function(){
                                slider.flexAnimate(slider.getTarget('prev'));
                        });

                        slider.find('img').click(function(){
                                var large = $(this).attr('data-large');
                                setActive($(this));
                                $('#product-slider img.main-image').fadeOut(300, changeImg(large, $('#product-slider img.main-image')));
                                $('#product-slider a.fullscreen-button').attr('href', large);
                        });

                        function changeImg(large, element){
                                var element = element;
                                var large = large;
                                setTimeout(function(){ startF()},300);
                                function startF(){
                                        element.attr('src', large)
                                        element.attr('data-large', large)
                                        element.fadeIn(300);
                                }

                        }

                        function setActive(el){
                                var element = el;
                                $('#product-carousel img').removeClass('active-item');
                                element.addClass('active-item');
                        }

                    }
			
                    });
			
                    $('a.fullscreen-button').click(function(e){
                            e.preventDefault();
                            var target = $(this).attr('href');
                            $('#product-carousel a.fancybox[href='+target+']').trigger('click');
                    });
                    
                    $('.cloud-zoom').imagezoomsl({
                            zoomrange: [3, 3],
                            magnifiersize: [350, 500],
                    });

                    $('.fancybox').fancybox();

                }";

        $js->appendln($script);


//            $script = "
//            jQuery('.gallery-list-image').click(function() {
//                jQuery('.gallery-list-image').removeClass('active');
//                jQuery(this).addClass('active');
//                jQuery('.gallery-image img').attr('src', jQuery(this).find('img').attr('src'));
//            });
//        ";
//            $js->append($script);

        $js->append(parent::js($indent));
        return $js->text();
    }

}
