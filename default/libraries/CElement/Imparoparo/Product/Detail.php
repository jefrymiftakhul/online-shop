<?php

class CElement_Imparoparo_Product_Detail extends CElement {

    private $key = NULL;
    private $detail_list = array();
    private $price = 0;
    private $stock = 0;
    private $minimum_stock = 10;
    private $attribute_list = array();
    private $with_qty = TRUE;
    private $type_select = 'select';
    private $type_image = 'image';
    private $total_param_input = array();
    private $url_share = '';
    private $text_share = '';

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_Imparoparo_Product_Detail($id);
    }

    public function set_key($key) {
        $this->key = $key;
        return $this;
    }

    public function set_minimum_stock($min_stock) {
        $this->minimum_stock = $min_stock;
        return $this;
    }

    public function set_detail_list($detail_list) {
        $this->detail_list = $detail_list;
        return $this;
    }

    public function with_qty($with_qty) {
        $this->with_qty = $with_qty;
        return $this;
    }

    public function set_attribute_list($attribute_list) {
        $this->attribute_list = $attribute_list;
        return $this;
    }

    private function get_price() {
        $detail_price = $this->price;

        $arr_price = product::get_price($detail_price);

        return $arr_price;
    }

    private function generate_list($type, $data = array(), &$smallest_value_key) {
        $list = array();

        $index = 1;
        foreach ($data as $key => $row_data) {
            $price = carr::get($row_data, 'price');

            if ($index == 1) {
                $smallest_value_key = $key;
                $smallest_value = $price;
            } else {
                if ($smallest_value > $price) {
                    $smallest_value = $price;
                    $smallest_value_key = $key;
                }
            }

            if ($type == $this->type_select) {
                $value = carr::get($row_data, 'attribute_key');
            }

            if ($type == $this->type_image) {
                $value['key'] = carr::get($row_data, 'attribute_key');
                $value['value'] = carr::get($row_data, 'file_path');
            }

            $list[$key] = $value;
            $index++;
        }

        return $list;
    }

    public function html($indent = 0) {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $html = new CStringBuilder();
        $html->set_indent($indent);
        $session = session::instance();

        $product_id = carr::get($this->detail_list, 'product_id', NULL);
        $name = carr::get($this->detail_list, 'name', NULL);
        $description = carr::get($this->detail_list, 'description', NULL);
        $url_key_id = carr::get($this->detail_list, 'url_key');
        $sku = carr::get($this->detail_list, 'sku', NULL);
        $availability = carr::get($this->detail_list, 'is_available', NULL);
        $quick_overview = carr::get($this->detail_list, 'quick_overview', NULL);
        $stock = carr::get($this->detail_list, 'stock', 0);
        $weight = carr::get($this->detail_list, 'weight', 0);
        $show_minimum_stock = carr::get($this->detail_list, 'show_minimum_stock', 0);
        $this->stock = $stock;
        $this->price = carr::get($this->detail_list, 'detail_price', 0);
        $detail_price = carr::get($this->detail_list, 'detail_price', 0);
        $product_data_type = carr::get($this->detail_list, 'product_data_type');
        $is_package_product = carr::get($this->detail_list, 'is_package_product');

        $array_size = carr::get($this->detail_list, 'size');
        $array_color = carr::get($this->detail_list, 'color');

        if (count($detail_price) == 0) {
            $detail_price = $product_v;
        }
        $promo_start_date = carr::get($detail_price, 'promo_start_date');
        $promo_end_date = carr::get($detail_price, 'promo_end_date');
        $promo_text = carr::get($detail_price, 'promo_text');
        $promo_value = carr::get($detail_price, 'promo_value');

        $price_all = $this->get_price($detail_price);

        $price_promo = carr::get($price_all, 'promo_price');
        $price_normal = carr::get($price_all, 'price');

        $percent_sale = '';
        $percent_label = '';
        if ($promo_value != 0) {
            $promo_type = carr::get($detail_price, 'promo_type');
            $promo_value = carr::get($detail_price, 'promo_value');
            if ($promo_type == 'amount') {
                $price_promo = $price_normal - $promo_value;
                $price = 'Rp ' . ctransform::format_currency($price_promo);
                $price_strike = 'Rp ' . ctransform::format_currency($price_normal);
                $percent_sale = round(($promo_value / $price_normal) * 100);
                $percent_label = $percent_sale . '%';
            } else if ($promo_type == 'percent') {
                $price_percent = $price_normal * ($promo_value / 100);
                $price_promo = $price_normal - $price_percent;
                $price = 'Rp ' . ctransform::format_currency($price_promo);
                $price_strike = 'Rp ' . ctransform::format_currency($price_normal);
                $percent_sale = $promo_value;
                $percent_label = $percent_sale . '%';
            }
        }

        if (strlen($url_key_id) == 0) {
            $url_key_id = $product_id;
        }
        $this->url_share = curl::httpbase() . 'product/item/' . $url_key_id;
        $this->text_share = htmlspecialchars($name);

        // Build UI
        $detail_product = $this->add_div()->add_class('detail-product');
        $product_action_div = $detail_product->add_div()->add_class('product-action');
        $product_summary_div = $detail_product->add_div()->add_class('product-summary');
        $product_stock_div = $detail_product->add_div()->add_class('product-stock');
        $product_share_div = $detail_product->add_div()->add_class('product-share');

        // Product Summary
        $product_name_div = $product_summary_div->add_div()->add_class('product-name');
        $product_name_div->add('<h3 class="title">' . htmlspecialchars($name) . '</h3>');
        
        // <editor-fold defaultstate="collapsed" desc="Wishlist button">
        //Wishlist button
//        $product_action_div->add_div('info_res');
//        $member_id = $session->get('member_id');
//        cdbg::var_dump($member_id);
//        die;
//        $like = 'Suka';
//        $is_active ='';
//        $liker = product::get_liker($product_id,$member_id);
//        if ($liker['is_like'] == true) {
//         $like = 'Tidak Suka';
//         $is_active = 'active';
//        }
//        $product_like = $product_name_div->add_div('wishlist_btn')->add_class('wishtlist');
//        $product_like->add_action()
//                ->add_class('btn btn-suka '.$is_active.'')
//                    ->set_label('<p class="wishtlist-text ">'.$like.' '.$liker['like_count'].'</p> ');
////                    
//       $action_pl = $product_like->add_listener('click');
//       if ($member_id != false){
//           $action_pl->add_handler('reload')
//                            ->set_target('info_res')
//                            ->set_url(curl::base() . 'product/wishlist/' . $product_id)
//                    ;
//       }
//       else{
//           $err_stock_err_message = 'Anda Harus Login untuk menambah produk ini ke '.clang::__('wishlist') .'<br><br>';
//           $action_pl->add_handler('custom')->set_js("
//                            $.app.show_dialog('body', 'Peringatan', '<center>" . $err_stock_err_message . "</center>');
//                      ");      
//       }
        //tab detail product
//        $tabs = $product_name_div->add_tab_list('tabs-horizontal')->add_class('product-tabs');
        // </editor-fold>
        
        //$data_detail_product = product::get_product($product_id);
        
        $data_detail_product = $this->detail_list;
        
        $data_attribute_category = product::get_attribute_category_all($product_id);
        $data_package = product::get_package_product($product_id);
//        cdbg::var_dump($this->detail_list);

        $count_package = count($data_package);
        
        
//        cdbg::var_dump($data_package);
//        die(__CLASS__ . ' - ' . __FUNCTION__);

        if($is_package_product){
            if ($count_package > 0) {
                
                //tab product package
                $tabs_package = $product_name_div->add_tab_list('tabs-horizontal')->set_ajax(false);
                $i = 1;
                
                
                
                $json_data_package = array();
                
                
                foreach ($data_package as $key => $value) {
                    
                    $row_product = cdbutils::get_row('SELECT product_id,stock,product_data_type FROM product WHERE product_id = ' . $db->escape(carr::get($value, 'product_id')));
                    $arr_product = array();
                    $arr_product['product_id']=$row_product->product_id;
                    $arr_product['stock']=$row_product->stock;
                    $arr_product['product_data_type']=$row_product->product_data_type;
                    $arr_product_simple = array();
                    if (carr::get($data_package[$key], 'product_data_type') == 'configurable') {
                        $q = 'SELECT * FROM product WHERE parent_id = ' . $db->escape(carr::get($value, 'product_id'));
                        $r = $db->query($q);
                        foreach($r as $row) {
                            $row_product_simple = cdbutils::get_row('SELECT product_id,stock,product_data_type FROM product WHERE product_id = ' . $db->escape($row->product_id));
                            $product_simple = array();
                            $product_simple['product_id']=$row_product_simple->product_id;
                            $product_simple['stock']=$row_product_simple->stock;
                            $product_simple['product_data_type']=$row_product_simple->product_data_type;
                            //get attribute from simple
                            $q2 = "select * from product_group_attribute where status>0 and product_id=".$db->escape($row_product_simple->product_id);
                            $r2 = $db->query($q2);
                            $attr=array();
                            foreach($r2 as $row_attr) {
                                $attr[$row_attr->attribute_category_id]=$row_attr->attribute_id;
                            }
                            $product_simple['attribute']=$attr;
                            $arr_product_simple[] = $product_simple;
                        }
                        $arr_product['simple']=$arr_product_simple;
                    }
                    $json_data_package[]=$arr_product;
                }
                

                $json_data_package = json_encode($json_data_package);
                $app->add_js("window.product_package = " . $json_data_package. ";");
                
                
                
                $product_name_div->add_control('count_package', 'hidden')->set_value($count_package);
                foreach ($data_package as $k => $v) {
                    $data_package_name = carr::get($v, 'name');
                    $tabs_package_products = $tabs_package->add_tab('tabs' . $i)->set_label(clang::__('Item ') . $i);
                    $tabs_package_products->add_div()->add_class('name-product-attribute')->add($i . ". " . $data_package_name);

                    $element_package_products = $tabs_package_products->add_element('paroparo-detail-information', 'paroparo-detail-' . $i);
                    $data_package_id = carr::get($v, 'product_id');
                    
                    $tabs_package_products->add_control('product_'.$i, 'hidden')->set_value($data_package_id);
                    $tabs_package_products->add_control('product_simple_'.$i, 'hidden');
                    
                    $data_detail_product = product::get_product($data_package_id);
                    
                    $data_attribut_category_package = product::get_attribute_category_all($data_package_id);

                    $data_attr_new = array();
                    
                    $data_attr_new = $data_attribut_category_package;
                    
//                    cdbg::var_dump($data_package_id);
//                    cdbg::var_dump($data_attr_new);
                    
                    $element_package_products
                            ->set_key($data_package_id)
                            ->set_detail_list($data_detail_product)
                            ->is_package_product($is_package_product)
                            ->count_item($count_package)
                            ->set_index_item($i)
                            ->set_attribute_list($data_attr_new);
                    $i++;
                }
                
                
                

                $total_sales = product::get_total_sales($product_id);
                $total_view = carr::get($this->detail_list, 'viewed');
                
                $show_minimum_stock = carr::get($this->detail_list, 'show_minimum_stock', 0);
                $stock = carr::get($this->detail_list, 'stock', 0);
                $is_available = carr::get($this->detail_list, 'is_available', 0);
                
                $stock_info = $stock;

                if($is_available) {
                    $stock_info = '<span class="stock">' . $stock . '</span>';
                } else {
                    $stock_info = '<span class="stock">' . "-" . '</span>';
                }

                
                $product_name_div->add_control('package_product', 'hidden')->set_value(1);
                
                $container_qty = $product_name_div->add_div()->add_class('col-xs-12 col-md-12 container-qty')->custom_css('margin-left', '0px');
                $container_qty->add_div()->add_class('col-md-5')->add(clang::__('Quantity'))->add(' :');
                $container_qty->add_div()->add_class('col-md-6 qty-wrapper')->custom_css('margin-left', '-15px')->add("<input type='text' class='qty' id='qty' name='qty' value='1' style='z-index:0' readonly>");

                $product_price = $product_name_div->add_div()->add_class('product-price-detail');
                if($is_package_product){
                    $product_price->custom_css('clear', 'both');
                    $product_price->custom_css('margin-top', '60px');
                }
                $price = $this->get_price();
                $price_strike = '';

                if ($price['promo_price'] > 0) {
                    $price_strike = ctransform::format_currency($price['price']);

                    $product_price->add('<p class="promo-price-text-detail">Rp ' . ctransform::format_currency($price['price']) . '</p> <p class="price-text-detail">Rp ' . ctransform::format_currency($price['promo_price']) . '</p>');

                    $title_inside = $product_price->add_div()->add_class('text-center product-detail');
                    $discon_text = $title_inside->add('<h3 class="product-discount-text-detail">' . $percent_label . '</h3> <h3 class="product-discount-text-off-detail">OFF</h3>');
        //            $tapered_div = $product_price->add_div()->add_class('single-product-detail');
        //            $product_price->add_div()->add_class('detail-promo-price')->add('<strike>Rp ' . ctransform::format_currency($price['price']) . '</strike>');
        //            $product_price->add_div()->add_class('detail-normal-price')->add('Rp ' . ctransform::format_currency($price['promo_price']));
                } else {
                    $product_price->add_div()->add_class('product-detail-normal-price')->add('<h2 class="product-discount-text">Rp ' . ctransform::format_currency($price['price']) . '</h2>');
                }
                
                $stok = $product_name_div->add_div()->add_class('col-md-12 padding-0 detail-stok')->add('<hr>');
                $stok->add_div()->add_class('col-xs-4 col-sm-4 col-md-4 info-col')->add('<div class="product-detail-info-container"><div class="info-icon"><div class="icon-eye-open"></div></div><div class="product-info-text"><p class="padding-left-10 eye-detail"><span class="info_stok">Dilihat : </span>' . $total_view . '</p>  </div></div>');
                $stok->add_div()->add_class('col-xs-4 col-sm-4 col-md-4 info-col')->add('<div class="product-detail-info-container"><div class="info-icon"><div class="icon-shop-cart"></div></div><div class="product-info-text"><p class="padding-left-10 shop-detail"><span class="info_stok">Stock : </span>' . $stock_info . '</p>  </div></div>');
                $stok->add_div()->add_class('col-xs-4 col-sm-4 col-md-4 info-col')->add('<div class="product-detail-info-container"><div class="info-icon"><div class="icon-tag-cart"></div></div><div class="product-info-text"><p class="padding-left-10 tag-detail"><span class="info_stok">Terjual : </span>' . $total_sales . '</p> </div></div> ');

                //info ongkir
                $ongkir = ccfg::get('min_order_free_ongkir');
                $nominal_ongkir = ctransform::format_currency($ongkir);
                $text_free_ongkir = 'Gratis Ongkir untuk pesanan lebih dari Rp' . $nominal_ongkir . ' khusus wilayah jabodetabek';
                $text_free_ongkir = '
                    FREE ONGKIR JABODETABEK dan BANDUNG minimal pembelanjaan Rp 60.000-
                    <br/>PROMO subsidi ongkir Rp 30.000-, minimal pembelanjaan Rp200.000-
                    ';
                $freeong = $product_name_div->add_div()->add_class('col-md-12 padding-0 product-action detail-info-ongkir')->add('<hr/>');
                $info_freeong = $freeong->add_div()->add_class('ongkir');
                $info_freeong->add_div()->add_class('col-md-3 freeong');
                $info_ongkir = $info_freeong->add_div()->add_class('col-md-9 detail-freeong')->add($text_free_ongkir);
            }
        }
        else {
            //not package product
            //
            //tab information
            $tabs = $product_name_div->add_tab_list('tabs-horizontal')->set_ajax(false);
            $tab_information = $tabs->add_tab('information')->set_label(clang::__('Information'))->set_active(true);
            $element_product_information = $tab_information->add_element('paroparo-detail-information', 'paroparo-detail');
            $element_product_information->set_key($product_id)
                    ->set_detail_list($data_detail_product)->set_attribute_list($data_attribute_category);

            //tab review
            $tab_informations = $tabs->add_tab('review')->set_label(clang::__('Review'))->set_active(false);
            $star_rate = 0;
            $q = "SELECT review.review as review_text, review.title, review.star, member.name as member_name, 
            review.created as review_date, member.image_name,product.file_path,product.image_name as img
                FROM review
                JOIN transaction_detail 
                    ON transaction_detail.transaction_detail_id = review.transaction_detail_id
                JOIN transaction 
                    ON transaction.transaction_id = transaction_detail.transaction_id
                JOIN member 
                    ON transaction.member_id = member.member_id
                JOIN product 
                    ON transaction_detail.product_id = product.product_id
                WHERE review.status > 0 
                    AND transaction_detail.product_id = " . $db->escape($product_id);
            $q_count = cdbutils::get_row('select sum(star) as star_count from review
                 JOIN transaction_detail 
                    ON transaction_detail.transaction_detail_id = review.transaction_detail_id where transaction_detail.product_id = ' . $db->escape($product_id));
            $data = $db->query($q);
            $count_data = count($data);

            $count_star = cobj::get($q_count, 'star_count');
            if ($count_star != null) {
                $star_rate = $count_star / $count_data;
            }

            $container = $tab_informations->add_div()->add_class('container-product-review')->custom_css('position', 'relative');


            // PRODUCT INFO

            $element_detail_info = $container->add_element('paroparo-detail-info', 'detail-info' . $product_id);
            $element_detail_info->set_product_id($product_id);
            //share fb
            $org_id = CF::org_id();
            $org = org::get_org($org_id);
            $url = '';
            $http = 'http';
            if (isset($_SERVER['HTTPS'])) {
                $http = 'https';
            }
            if ($org) {
                $url = $http . '://' . $org->domain . '/product/item/' . $product_id;
            }
            $url_image = '';
            $description = '';
            $q = "
                                    select
                                            *
                                    from
                                            product
                                    where
                                            product_id=" . $db->escape($product_id) . "
                            ";

            $r = $db->query($q);

            if ($r->count() > 0) {
                $row = $r[0];
                $url_image = $row->file_path;
                $description = $row->description;
            }

            $facebook_app_id = ccfg::get('facebook_app_id');
            $meta_facebook_app_id = '<meta property="fb:app_id" content="' . $facebook_app_id . '">';
            $additional_head = '
                                    ' . $meta_facebook_app_id . '
                                    <meta property="og:url" content="' . $url . '">
                                 
                                    <meta property="og:site_name" content="' . $name . '">
                                    <meta property="og:description" content="' . htmlspecialchars(strip_tags($description)) . '">
                                    <meta property="og:type" content="article">
                                    <meta property="og:image" content="' . $url_image . '">
                           ';
            $app->set_additional_head($additional_head);
            $element_detail_info->set_url_shared_facebook($url);
            $element_detail_info->set_url_shared_google($url);
            $element_detail_info->set_url_shared_twitter($url);
//            $element_detail_info->set_text_twitter(htmlspecialchars($data_detail_product['name'] . '-' . $org->name));
//            $element_detail_info->set_page('product');


            $title_average = $container->add_div()->add_class('product-star-title');

            $title_average->add_div()->add_class('txt-title-star')->add(clang::__('Penilaian Produk'));
            $star_average = $container->add_div()->add_class('product-star-icon')->add_div()->add_class('stars');
            $empty_star_index = 5 - $star_rate;

            for ($s = 1; $s <= $star_rate; $s++) {
                $star_average->add('<img src="' . curl::base() . 'application/ittronmall/default/media/img/Imparoparo/1_star.png">');
            }
            if (strpos($empty_star_index, '.') !== false) {
                $star_average->add('<img src="/application/ittronmall/default/media/img/Imparoparo/05_star.png">');
            }
            if ($empty_star_index > 0) {
                for ($a = 1; $a <= $empty_star_index; $a++) {
                    $star_average->add('<img src="/application/ittronmall/default/media/img/Imparoparo/0_star.png">');
                }
            }
            $star_average->add(' ' . round($star_rate, 1) . '/5 (dari ' . $count_data . ' Penilaian)');
            if (count($data) == 0) {
                $container->add_div()->add_div()->add_class('txt-review-name')->add(clang::__("There is no review for this product"));
            } 
            else {

                foreach ($data as $key => $value) {
                    $get_image = cobj::get($value, 'image_name');
                    $product_image = cobj::get($value, 'img');
                    $product_img = image::get_image_url($product_image, 'thumbnails');
                    $rating = cobj::get($value, 'star');
                    if (empty($get_image)) {
                        //$image = curl::base() . 'application/lapakbangunan/default/media/img/imbuilding/user_gray_150x150.png';
                        $image = curl::base() . 'application/ittronmall/default/media/img/Imparoparo/user_gray_150x150.png';
                    } else {
                        $image = image::get_image_url_front($get_image, 'view_profile');
                    }

                    $member_name = cobj::get($value, 'member_name');
                    $review_title = cobj::get($value, 'title');
                    $review_txt = cobj::get($value, 'review_text');
                    $review_date = cobj::get($value, 'review_date');
                    $review_date = Date('d F Y H:i:s', strtotime($review_date));
                    //            $star_average->add_div()->add_class('txt-review-msg');
                    $div_con = $container->add_div()->add_class('rating-box');
                    $container_left = $div_con->add_div()->add_class('product-review-icon');
                    $container_right = $div_con->add_div()->add_class('product-review-content');

                    $container_left->add_div()->add_class('image-photo-review')->add("<img src='" . $image . "'>");
                    $container_right->add_div()->add_div()->add_class('rating')->add($member_name);
                    $star = $container_right->add_div()->add_div()->add_class('txt-review-name');
                    $empty_star = 5 - $rating;
                    for ($a = 1; $a <= $rating; $a++) {
                        $star->add('<img src="/application/ittronmall/default/media/img/Imparoparo/1_star.png">');
                    }
                    if ($empty_star > 0) {
                        for ($a = 1; $a <= $empty_star; $a++) {
                            $star->add('<img src="/application/ittronmall/default/media/img/Imparoparo/0_star.png">');
                        }
                    }
                    $container_right->add_div()->add_div()->add_class('txt-review-msg')->add($review_title);
                    $container_right->add_div()->add_div()->add_class('txt-review-msg')->add($review_txt);
                    $container_right->add_div()->add_div()->add_class('image-photo-review')->add("<img src='" . $product_img . "'>");
                    $container_right->add_div()->add_div()->add_class('txt-review-date')->add($review_date);
                }
            }
        }

        //end tab review
//        $product_name_div->add('<hr/>');

//         $product_price = $product_name_div->add_div()->add_class('product-price-detail');
//         if($is_package_product){
//             $product_price->custom_css('clear', 'both');
//         }
//         $price = $this->get_price();
//         $price_strike = '';

//         if ($price['promo_price'] > 0) {
//             $price_strike = ctransform::format_currency($price['price']);

//             $product_price->add('<p class="promo-price-text-detail">Rp ' . ctransform::format_currency($price['price']) . '</p> <p class="price-text-detail">Rp ' . ctransform::format_currency($price['promo_price']) . '</p>');

//             $title_inside = $product_price->add_div()->add_class('text-center product-detail');
//             $discon_text = $title_inside->add('<h3 class="product-discount-text-detail">' . $percent_label . '</h3> <h3 class="product-discount-text-off-detail">OFF</h3>');
// //            $tapered_div = $product_price->add_div()->add_class('single-product-detail');
// //            $product_price->add_div()->add_class('detail-promo-price')->add('<strike>Rp ' . ctransform::format_currency($price['price']) . '</strike>');
// //            $product_price->add_div()->add_class('detail-normal-price')->add('Rp ' . ctransform::format_currency($price['promo_price']));
//         } else {
//             $product_price->add_div()->add_class('product-detail-normal-price')->add('<h2 class="product-discount-text">Rp ' . ctransform::format_currency($price['price']) . '</h2>');
//         }
        
        $i=1;

        if ($availability == 1) {
            if (!product::is_soldout($this->detail_list)) {

                $action = $product_name_div->add_action()
                        ->set_label("<div class='shoppingcart'>
                                            <div class='ico-comment visible-xs visible-sm hidden-md hidden-lg'></div>
                                            <div class='ico-cart-button'></div>
                                        </div>
                                     <div class='text-shopcart txt-cart-button'>" . clang::__('BELI SEKARANG') . "</div>")
                        //->add_class('btn-add-cart btn-primary')
                        ->add_class('button-shopping-cart')
                        ->add_listener('click');

                $cart = $action->add_handler('reload');
                
                if(!$is_package_product) $data_package=1;
                for ($index = 1; $index <= count($data_package); $index++) {
                    $attribute_list = $this->attribute_list;
                    if($is_package_product) {
                        
                        if(isset($data_package[$index-1]) && isset($data_package[$index-1]['product_id'])) {
                            $attribute_list = product::get_attribute_category_all($data_package[$index-1]['product_id']);
                        }
                        
                    }
                    foreach ($attribute_list as $key => $attribut_list) {
                        
                        $cart->add_param_input('att_cat_' . carr::get($attribut_list, 'attribute_category_id').'_'.$index);
                        //$i++;
                    }
                    $cart->add_param_input('product_'.$index);
                }

                $cart
                    ->add_param_input('qty')
                    ->set_target('shopping-cart') // shopping cart
                    ->set_url(curl::base() . 'products/shoppingcart/add_item_cart/' . $product_id); // proses shopping cart
            }
        }
        else if ($availability == 0) {

            $form = $product_name_div->add_form('form-detail-product')->set_action(curl::base() . 'request/add/');
            $form->add_control('product_id', 'hidden')->set_value($product_id);
            $form->add_control('page', 'hidden')->set_value('product');
            $rowform = $form->add_div()->add_class('row');

            $this->stock = 100;
//            $action = $form->add_action()
//                    ->set_label('REQUEST')
//                    ->add_class('btn-primary btn-add-cart')
//                    ->set_submit_to(curl::base() . 'request/add/')
//                    ->set_submit(true);
            $action = $form->add_action()
                        ->set_label("<div class='shoppingcart'>
                                            <div class='ico-comment visible-xs visible-sm hidden-md hidden-lg'></div>
                                            <div class='ico-cart-button'></div>
                                        </div>
                                     <div class='text-shopcart txt-cart-button'>" . clang::__('REQUEST') . "</div>")
                        ->add_class('button-shopping-cart')
                        ->set_submit_to(curl::base() . 'request/add/');
        }

        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();
        $data_product = $this->detail_list;
        $product_data_type = $data_product['product_data_type'];

        $max = $this->stock;
        if ($max < 1)
            $max = 1;

        $js->appendln("
            var max = 0;
            var product_type = '" . $product_data_type . "';
            $('.btn-colors').click(function(){
                id = $(this).attr('id');
                $('#colors').val(id);
                $('.btn-colors').removeClass('active');
                $(this).addClass('active');
            });  
            //if(product_type == 'simple'){
               $('#qty').TouchSpin({
                    min: 1,
                    max: " . $max . ",
                    verticalbuttons: false
                });
            //}
		");

        $js->appendln("
            $('.facebook').click(function(){
                var fbpopup = window.open('http://www.facebook.com/sharer/sharer.php?u=" . $this->url_share . "', 'pop', 'width=600, height=400, scrollbars=no');
                return false;
            });
            $('.twitter').click(function(){
                                var twpopup = window.open(\"//twitter.com/intent/tweet?url=" . $this->url_share . "&text=" . $this->text_share . "&original_referer=" . $this->url_share . "\",600, 400);
                return false;
            });
             $('.google').click(function(){
                                var twpopup = window.open('//plus.google.com/share?url=" . $this->url_share . "','', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=600');
                return false;
            });
        ");

        $js->appendln("
            /*jQuery('#product_tab_overview-tab-widget .box-header h5').remove();
            jQuery('#product_tab_overview-tab-nav li a').click(function() {
                jQuery('.product-tab-caret').remove();
                jQuery(this).parents('li').append('<div class=\"product-tab-caret\"></div>');
            });*/
        ");


        $js->appendln("            
            jQuery('body').on('click', '#btn-login-alternate', function(evt){
                 $('.modal-title').html('LOGIN');
                 $('.forgot_password_form').addClass('hide');
                 $('.success_forgot_password').addClass('hide');
             });
        ");

        $js->appendln("$(document).ready(function(){
                $('.nav-pills li:first').addClass('active');
              });");

        $js->append(parent::js($indent));
        return $js->text();
    }

}
