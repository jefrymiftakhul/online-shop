<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Jun 12, 2016
     * @license http://ittron.co.id ITtron
     */
    class CElement_Imparoparo_Product_Card extends CElement {

        protected $product;
        public $page = '';

        public function __construct($id = "", $tag = "div") {
            parent::__construct($id, $tag);
        }

        public static function factory($id = "", $tag = "div") {
            return new CElement_Imparoparo_Product_Card($id, $tag);
        }
        
        private function get_image($image_path = '') {
            $retcode = 200;
            if ($retcode == 500 or $image_path == NULL) {
                $image_path = curl::base() . 'application/paroparo/default/media/img/product/no-image.png';
            } else {
                $image_path = image::get_image_url($image_path, 'original');
            }
            return $image_path;
        }

        private function get_price($detail_price) {
            $promo_price = 0;
            $price = 0;

            if (!empty($detail_price['ho_sell_price']) and ! empty($detail_price['channel_sell_price'])) {
                if ($detail_price['ho_sell_price'] > $detail_price['channel_sell_price']) {
                    $price = $detail_price['ho_sell_price'];
                    $promo_price = $detail_price['channel_sell_price'];
                } else {
                    $price = $detail_price['channel_sell_price'];
                }
            }

            return array('promo_price' => $promo_price, 'price' => $price);
        }

        public function html($indent = 0) {
            $html = CStringBuilder::factory();
            $session = Session::instance();
            $product_v = $this->product;

            $product_id = carr::get($product_v, 'product_id');
            $url_key = carr::get($product_v, 'url_key');
            $code = carr::get($product_v, 'code');
            $name = carr::get($product_v, 'name');
            $sku = carr::get($product_v, 'sku');
            $weight = carr::get($product_v, 'weight');
            $image_path = carr::get($product_v, 'image_path');
            $file_path = carr::get($product_v, 'file_path');
            $image_url = carr::get($product_v, 'image_url');

            if (strlen($image_path) == 0) {
                $image_path = carr::get($product_v, 'image_name');
            }
            
            $image_src = $this->get_image($image_path);
            $is_hot_item = carr::get($product_v, 'is_hot_item');
            $new_product_date = carr::get($product_v, 'new_product_date');
            $is_available = carr::get($product_v, 'is_available');
            $quick_overview = carr::get($product_v, 'quick_overview');
            $stock = carr::get($product_v, 'stock');
            $show_minimum_stock = carr::get($product_v, 'show_minimum_stock');
            $product_data_type = carr::get($product_v, 'product_data_type');

            $detail_price = carr::get($product_v, 'detail_price');
            if (count($detail_price) == 0) {
                $detail_price = $product_v;
            }
            $promo_start_date = carr::get($detail_price, 'promo_start_date');
            $promo_end_date = carr::get($detail_price, 'promo_end_date');
            $promo_text = carr::get($detail_price, 'promo_text');
            $promo_value = carr::get($detail_price, 'promo_value');

            $price_all = $this->get_price($detail_price);

            $price_promo = carr::get($price_all, 'promo_price');
            $price_normal = carr::get($price_all, 'price');

            $price = 'Rp ' . ctransform::format_currency($price_normal);
            $price_strike = '';
    //                if ($price_promo > 0) {
    //                    $price = 'Rp '.ctransform::format_currency($price_promo);
    //                    $price_strike = 'Rp '.ctransform::format_currency($price_normal);
    //                }
            $percent_sale = '';
            $percent_label = '';
            if ($promo_value != 0) {
                $promo_type = carr::get($detail_price, 'promo_type');
                $promo_value = carr::get($detail_price, 'promo_value');
                if ($promo_type == 'amount') {
                    $price_promo = $price_normal - $promo_value;
                    $price = 'Rp ' . ctransform::format_currency($price_promo);
                    $price_strike = 'Rp ' . ctransform::format_currency($price_normal);
                    $percent_sale = round(($promo_value / $price_normal) * 100);
                    $percent_label = $percent_sale . '%';
                } else if ($promo_type == 'percent') {
                    $price_percent = $price_normal * ($promo_value / 100);
                    $price_promo = $price_normal - $price_percent;
                    $price = 'Rp ' . ctransform::format_currency($price_promo);
                    $price_strike = 'Rp ' . ctransform::format_currency($price_normal);
                    $percent_sale = $promo_value;
                    $percent_label = $percent_sale . '%';
                }
            }

            $page = '/';
            $location_detail = curl::httpbase() . $page . 'product/item/' . $url_key;

            if (strlen($name) > 35) {
                $name = substr($name, 0, 35) . ' ...';
            }
            
            $sold_out = '<div></div>';
            

            $single_category = $this->add_div()->add_class('single-product small');
            
            $sold_price = "";
            $class_sold_out = "";
            if (product::is_soldout($product_v)) {
                $sold_out = '<div class="img-sold-out">
                    <span class="title-sold-out">HABIS</span>
                </div>';
                $class_sold_out = " sold-out";
                $sold_price = "";
            }
            
            $single_category_top = $single_category->add_div()->add_class('single-product-outside'.$class_sold_out);
            $single_category_top->add('<p class="text-center title-product">' . htmlentities($name) . '</p>');
            $single_product_inside = $single_category_top->add_div()->add_class('single-product-inside')->add($sold_out);
            $img = $single_product_inside->add('<a href="' . $location_detail . '"><img src="' . $image_src . '" class="img-single-product img-responsive" alt="' . htmlspecialchars($name) . '" itemprop="image"></a>');
            $single_category_title = $single_category->add_div()->add_class('single-product-title-outside'.$class_sold_out);

            $class = "text-left price-div";
            $style = "width:100%;";
            if ($promo_value == 0) {
                $style="padding:0px;padding-top:5px;width:100%;";
                $class = "text-center price-div";
            }
            $price_div = $single_category_title->add_div()->add_class($class)->add_attr('style',$style);
            $price_text_margin_top = '10px';
            if ($promo_value != 0) {
                $price_div->add('<p class="promo-price-text">' . $price_strike . '</p>');
                $price_text_margin_top = '0px';
            }
            $style_price = "";
            if($promo_value == 0){
                $style_price = "";
            }
            $price_div->add('<p style="margin-top: ' . $price_text_margin_top . ';'.$style_price.'" class="price-text">' . $price . '</p>');

            //$price_div->add('<p class="promo-price-text">' . $price_strike . '</p> <p class="price-text">' . $price . '</p>');


            if ($promo_value != 0) {
                $title_inside = $single_category_title->add_div()->add_class('text-center single-product-title-inside');
                $discon_text = $title_inside->add('<h3 class="discount-text">' . $percent_label . '</h3> <h3 class="discount-text-off">OFF</h3>');
                $tapered_div = $single_category_title->add_div()->add_class('single-product-tapered-div');
            }
            
            $html->append(parent::html($indent));
            return $html->text();
        }

        public function js($indent = 0) {
            $js = CStringBuilder::factory();

            $js->append(parent::js($indent));
            return $js->text();
        }

        public function get_product() {
            return $this->product;
        }

		public function set_page($page){
			$this->page = $page;
			return $this;
		}

        public function set_product($product) {
            $this->product = $product;
            return $this;
        }

    }
