<?php

class CElement_Imparoparo_Product_Slide extends CElement {

    protected $list_product = array();
    protected $page = '';
    protected $limit = 20;
    protected $col_content = 3;
    protected $custom_content = false;

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_Imparoparo_Product_Slide($id);
    }

    public function set_list_item($data) {
        $this->list_product = $data;
        return $this;
    }

    public function set_page($val) {
        $this->page = $val;
        return $this;
    }

    public function set_limit($val) {
        $this->limit = $val;
        return $this;
    }
    
    public function set_col_content($val) {
        $this->col_content = $val;
        return $this;
    }

    public function custom_content($bool) {
        $this->custom_content = $bool;
        return $this;
    }

    private function get_image($image_path = '') {
        $retcode = 200;
        if ($retcode == 500 or $image_path == NULL) {
            $image_path = curl::base() . 'application/paroparo/default/media/img/product/no-image.png';
        } else {
            $image_path = image::get_image_url($image_path, 'view_all');
        }
        return $image_path;
    }

    private function get_price($detail_price) {
        $promo_price = 0;
        $price = 0;

        if (!empty($detail_price['ho_sell_price']) and ! empty($detail_price['channel_sell_price'])) {
            if ($detail_price['ho_sell_price'] > $detail_price['channel_sell_price']) {
                $price = $detail_price['ho_sell_price'];
                $promo_price = $detail_price['channel_sell_price'];
            } else {
                $price = $detail_price['channel_sell_price'];
            }
        }

        return array('promo_price' => $promo_price, 'price' => $price);
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);

        $class = '';
        if (count($this->classes) > 0) {
            $class = implode(' ', $this->classes);
        }

        $content_category_outside = $this->add_div()->add_class('content-product-outside');
        $content_category = $content_category_outside->add_div()->add_class('content-product');
        $row = $content_category->add_div()->add_class('row');
        $col_12 = $row->add_div()->add_class('im-nopadding col-lg-12');
        $col_12 = $row->add_div()->add_class('im-nopadding');

        if (count($this->list_product) > 0) {
            foreach ($this->list_product as $product_k => $product_v) {
                $product_v = (array) $product_v;
                if ($product_k >= $this->limit) {
                    break;
                }

                $product_id = carr::get($product_v, 'product_id');
                $url_key = carr::get($product_v, 'url_key');
                $code = carr::get($product_v, 'code');
                $name = carr::get($product_v, 'name');
                $sku = carr::get($product_v, 'sku');
                $weight = carr::get($product_v, 'weight');
                $image_path = carr::get($product_v, 'image_path');
                if (strlen($image_path) == 0) {
                    $image_path = carr::get($product_v, 'image_name');
                }
                $image_src = $this->get_image($image_path);
                $is_hot_item = carr::get($product_v, 'is_hot_item');
                $new_product_date = carr::get($product_v, 'new_product_date');
                $is_available = carr::get($product_v, 'is_available');  // is_available > 0 tampilkan harga, else tampilkan "By Request"
                $quick_overview = carr::get($product_v, 'quick_overview');
                $stock = carr::get($product_v, 'stock');
                $show_minimum_stock = carr::get($product_v, 'show_minimum_stock');
                $product_data_type = carr::get($product_v, 'product_data_type');

                $detail_price = carr::get($product_v, 'detail_price');
                if (count($detail_price) == 0) {
                    $detail_price = $product_v;
                }
                $promo_start_date = carr::get($detail_price, 'promo_start_date');
                $promo_end_date = carr::get($detail_price, 'promo_end_date');
                $promo_text = carr::get($detail_price, 'promo_text');
                $promo_value = carr::get($detail_price, 'promo_value');

                $price_all = $this->get_price($detail_price);

                $price_promo = carr::get($price_all, 'promo_price');
                $price_normal = carr::get($price_all, 'price');

                $price = 'Rp ' . ctransform::format_currency($price_normal);
                $price_strike = '';
                $percent_sale = '';
                $percent_label = '';
                if ($promo_value != 0) {
                    $promo_type = carr::get($detail_price, 'promo_type');
                    $promo_value = carr::get($detail_price, 'promo_value');
                    if ($promo_type == 'amount') {
                        $price_promo = $price_normal - $promo_value;
                        $price = 'Rp ' . ctransform::format_currency($price_promo);
                        $price_strike = 'Rp ' . ctransform::format_currency($price_normal);
                        $percent_sale = round(($promo_value / $price_normal) * 100);
                        $percent_label = $percent_sale . '%';
                    } else if ($promo_type == 'percent') {
                        $price_percent = $price_normal * ($promo_value / 100);
                        $price_promo = $price_normal - $price_percent;
                        $price = 'Rp ' . ctransform::format_currency($price_promo);
                        $price_strike = 'Rp ' . ctransform::format_currency($price_normal);
                        $percent_sale = $promo_value;
                        $percent_label = $percent_sale . '%';
                    }
                }

                $page = $this->page . '/';
                if ($this->page == 'product') {
                    $page = '';
                }
                $location_detail = curl::httpbase() . $page . 'product/item/' . $url_key;

                if (strlen($name) > 25) {
                    $name = substr($name, 0, 25) . ' ...';
                }

                $single_category_class = 'col-lg-3 col-md-6 col-sm-6 col-xs-12 relate-single-product';

                if ($this->custom_content) {
                    $col = $this->col_content;
                    $single_category_class = '';
                    $width = 100 / $col;
                    foreach ($col_type as $col_key => $type) {
                        $single_category_class .= 'col-paro-' . $type . ' ';
                    }
                    $single_category_class .= 'custom-single-product abcde';
                }

                $single_category = $col_12->add_div()->add_class("$single_category_class");
                $single_category_top = $single_category->add_div()->add_class('relate-single-product-outside');
                $single_category_top->add('<p class="text-center relate-title-product">' . htmlentities($name) . '</p>');
                $single_product_inside = $single_category_top->add_div()->add_class('relate-single-product-inside');
                $img = $single_product_inside->add('<a href="' . $location_detail . '"><img src="' . $image_src . '" class="img-relate-single-product img-responsive" alt="' . htmlspecialchars($name) . '" itemprop="image">' . $name . '</a>');
                $single_category_title = $single_category->add_div()->add_class('relate-single-product-title-outside');

                if ($promo_value != 0) {
                    $price_div = $single_category_title->add_div()->add_class('relate-price-div');
                    $price_div->add('<p class="relate-promo-price-text">' . $price_strike . '</p> <p class="relate-price-text">' . $price . '</p>');

                    $title_inside = $single_category_title->add_div()->add_class('text-center relate-single-product-title-inside');
                    $discon_text = $title_inside->add('<h3 class="relate-discount-text">' . $percent_label . '</h3> <h3 class="relate-discount-text-off">OFF</h3>');
                    $tapered_div = $single_category_title->add_div()->add_class('relate-single-product-tapered-div');
                } else {
                    $price_no_promo = $single_category_title->add_div()->add_class('relate-price-div-no-promo');
                    $price_no_promo->add('<p class="text-center relate-price-text-no-promo">' . $price . '</p>');
                }
                
//                $single_category = $col_12->add_div()->add_class($single_category_class);
//                $single_category_top = $single_category->add_div()->add_class('single-product-outside');
//                $single_category_top->add('<p class="text-center title-product">' . htmlentities($name) . '</p>');
//                $single_product_inside = $single_category_top->add_div()->add_class('single-product-inside');
//                $img = $single_product_inside->add('<a href="' . $location_detail . '"><img src="' . $image_src . '" class="img-single-product img-responsive" alt="' . htmlspecialchars($name) . '" itemprop="image">' . $name . '</a>');
//                $single_category_title = $single_category->add_div()->add_class('single-product-title-outside');
//
//                $price_div = $single_category_title->add_div()->add_class('text-left price-div');
//                $price_text_margin_top = '10px';
//                if ($promo_value != 0) {
//                    $price_div->add('<p class="promo-price-text">' . $price_strike . '</p>');
//                    $price_text_margin_top = '0px';
//                }
//                $price_div->add('<p style="margin-top: ' . $price_text_margin_top . '" class="price-text">' . $price . '</p>');
//
//                //$price_div->add('<p class="promo-price-text">' . $price_strike . '</p> <p class="price-text">' . $price . '</p>');
//
//
//                if ($promo_value != 0) {
//                    $title_inside = $single_category_title->add_div()->add_class('text-center single-product-title-inside');
//                    $discon_text = $title_inside->add('<h3 class="discount-text">' . $percent_label . '</h3> <h3 class="discount-text-off">OFF</h3>');
//                    $tapered_div = $single_category_title->add_div()->add_class('single-product-tapered-div');
//                }
            }
        }

        if ($this->custom_content) {
            $this->add('
                <style>
                    .custom-single-product{
                        background: transparent;
                        width: ' . $width . '%;
                        margin: 15px 0;
                        padding: 0 15px;
                        float: left;
                    }
                    .content-product{
                        margin-top:0 !important;
                    }
                    .relate-single-product-outside{
                        height: 81%;
                        border-top: 4px solid #7dc3f6;
                    }
                    .relate-single-product-outside:hover{
                        border-top: 4px solid #01ffff;
                    }
                    .relate-single-product-title-outside{
                        height: calc(100% - 81%) !important;
                    }
                    .relate-title-product{
                        font-size: 16px;
                        font-weight: normal;
                        margin-top: 1%;
                    }
                    .relate-promo-price-text{
                        font-size: 14px;
                    }
                    .relate-price-div{
                        padding: 2px 0 0 10px;
                    }
                    .relate-price-text{
                        font-weight: normal;
                        font-size: 19px;
                    }
                    .relate-single-product-title-inside{
                        padding: 7px 0;
                    }
                    .relate-single-product-title-inside h3{
                        font-size: 17px;
                    }
                    .relate-single-product-tapered-div{
                        border-bottom: 27px solid transparent;
                        border-right: 18px solid #ed7117;
                        border-top: 27px solid transparent;
                    }
                </style>
                ');
        }

        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $js->append(parent::js($indent));
        return $js->text();
    }

}
