<?php

class CElement_Imparoparo_Product_Overview extends CElement {

    private $key = NULL;
    private $detail_list = array();

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_Imparoparo_Product_Overview($id);
    }

    public function set_key($key) {
        $this->key = $key;
        return $this;
    }

    public function set_detail_list($detail_list) {
        $this->detail_list = $detail_list;
        return $this;
    }

    public function html($indent = 0) {
        $app = CApp::instance();
        $db = CDatabase::instance();
        $html = new CStringBuilder();
        $html->set_indent($indent);
        $session = session::instance();

        $product_id = carr::get($this->detail_list, 'product_id', NULL);
        $data_package = product::get_package_product($product_id);
        $count_package = count($data_package);

        $tabs = $this->add_tab_list('tabs-product')->add_class('product-tab');

        $tab_overview = $tabs->add_tab('description')
                ->set_label('<b>' . clang::__("Description") . '</b>')
                ->set_ajax_url(curl::base() . "product/description/" . $this->key);

        $tab_spesifikasi = $tabs->add_tab('video')
                ->set_label('<b>' . clang::__("Video") . '</b>')
                ->set_ajax_url(curl::base() . "product/video/" . $this->key);

        if ($count_package > 0) {
            $tab_reviews = $tabs->add_tab('reviews')
                    ->set_label('<b>' . clang::__("Review") . '</b>')
                    ->set_ajax_url(curl::base() . "product/reviews/" . $this->key);
        }

        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $vjs = "
            jQuery('#product_tab_overview-tab-widget .box-header h5').remove();
            jQuery('#product_tab_overview-tab-nav li a').click(function() {
                jQuery('.product-tab-caret').remove();
                jQuery(this).parents('li').append('<div class=\"product-tab-caret\"></div>');
            });
        ";

        $js->appendln($vjs);


        $js->append(parent::js($indent));
        return $js->text();
    }

}
