<?php

class CElement_Imparoparo_Product_Information extends CElement {

    private $key = NULL;
    private $detail_list = array();
    private $price = 0;
    private $stock = 0;
    private $minimum_stock = 10;
    private $attribute_list = array();
    private $with_qty = TRUE;
    private $type_select = 'select';
    private $type_image = 'image';
    private $total_param_input = array();
    private $url_share = '';
    private $text_share = '';
    private $data = array();
    private $count_item=1;
    private $is_package_product=false;
    private $index_item=1;

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_Imparoparo_Product_Information($id);
    }

    public function set_key($key) {
        $this->key = $key;
        return $this;
    }

    public function set_minimum_stock($min_stock) {
        $this->minimum_stock = $min_stock;
        return $this;
    }

    public function set_detail_list($detail_list) {
        $this->detail_list = $detail_list;
        return $this;
    }

    public function with_qty($with_qty) {
        $this->with_qty = $with_qty;
        return $this;
    }

    public function set_attribute_list($attribute_list) {
        $this->attribute_list = $attribute_list;
        return $this;
    }

    public function set_data($data) {
        $this->data = $data;
        return $this;
    }

    private function get_price() {
        $detail_price = $this->price;

        $arr_price = product::get_price($detail_price);

        return $arr_price;
    }
    
    public function is_package_product($bool) {
        $this->is_package_product = $bool;
        return $this;
    }
    
    public function count_item($data) {
        $this->count_item = $data;
        return $this;
    }
    
    public function set_index_item($data) {
        $this->index_item = $data;
        return $this;
    }

    private function generate_list($type, $data = array(), &$smallest_value_key) {
        $list = array();
        $value = array();

        $index = 1;
        foreach ($data as $key => $row_data) {
            $price = carr::get($row_data, 'price');

            if ($index == 1) {
                $smallest_value_key = $key;
                $smallest_value = $price;
            } else {
                if ($smallest_value > $price) {
                    $smallest_value = $price;
                    $smallest_value_key = $key;
                }
            }

            if ($type == $this->type_select) {
                $value = carr::get($row_data, 'attribute_key');
            }

            if ($type == $this->type_image) {
                $value['key'] = carr::get($row_data, 'attribute_key');
                $value['value'] = carr::get($row_data, 'file_path');
            }

            $list[$key] = $value;
            $index++;
        }

        return $list;
    }

    private function generate_form($container, $code, $data, $index) {
        
        $smallest_value_key = '';

        $id = carr::get($data, 'attribute_category_id');
        $type = carr::get($data, 'attribute_category_type');
        $prev = carr::get($data, 'prev_attribute_category');
        $next = carr::get($data, 'next_attribute_category');
        $attribute = carr::get($data, 'attribute', array());
        $price = carr::get($data, 'price');
        $list = $this->generate_list($type, $attribute, $smallest_value_key);

        $control = array();
        foreach ($prev as $row_prev) {
            $arr_param['att_cat_' . $id . "_" . $index] = 'att_cat_' . $id . "_" . $index;
            if (carr::get($this->total_param_input, 'att_cat_' . $id . "_" . $index) == null) {
                $this->total_param_input['att_cat_' . $id . "_" . $index] = 'att_cat_' . $id . "_" . $index;
            }
        }
        if ($type == $this->type_select) {
            $div_container = $container->add_div('container-' . $code.'-'.$index);
            $select = $div_container->add_field()
                    ->add_control('att_cat_' . $id . "_" . $index, 'select')
//                    ->add_class('select-62hallfamily bootstrap-select margin-bottom-20 att_cat_' . $id)
                    ->add_class('select-62hallfamily bootstrap-select margin-bottom-20 att_cat_' . $id . "_" . $index)
                    ->set_list($list)
//                    ->set_value($smallest_value_key)
            ;
            $control[] = $select;
        } 
        else if ($type == $this->type_image) {
            $div_container = $container->add_div('container-' . $code.'-'.$index)
                    ->add_class('container-input-image');
            $index = 1;
            foreach ($list as $key => $value) {
                $active = NULL;
                if ($index == 1) {
                    $active = 'active';
                    $div_container->add_control('att_cat_' . $id . "_" . $index, 'hidden')
                            ->set_value($key);
                }
                $image = $div_container->add_div($key)
                        ->add_class('btn-colors margin-bottom-10 link  att_cat_' . $id . "_" . $index . ' ' . $active)
                        ->custom_css('display', 'inline-block')
                        ->custom_css('padding', '2px')
                        ->add('<img src="' . $value['value'] . '" style="width:20px; height:20px"/>');


                $control[] = $image;

                $index++;
            }
        }

        if (count($control) > 0) {
            foreach ($control as $obj) {
                if (!empty($next) and count($list) > 0) {
                    $listener = $obj->add_listener('ready');

                    $handler = $listener->add_handler('reload')
                            ->set_target('container-' . $next.'-'.$index)
                            ->add_param_input($arr_param)
                            ->set_url(curl::base() . 'product/generate_attribute/' . $next . '/' . $this->key.'/'.$index);

                    if ($type == $this->type_image) {
                        $listener = $obj->add_listener('click');

                        $handler = $listener->add_handler('reload')
                                ->set_target('container-' . $next.'-'.$index)
                                ->add_param_input($arr_param)
                                ->set_url(curl::base() . 'product/generate_attribute/' . $next . '/' . $this->key.'/'.$index);
                    }

                    if ($type == $this->type_select) {

                        $listener = $obj->add_listener('change');

                        $handler = $listener->add_handler('reload')
                                ->set_target('container-' . $next.'-'.$index)
                                ->add_param_input($arr_param)
                                ->set_url(curl::base() . 'product/generate_attribute/' . $next . '/' . $this->key.'/'.$index);
                    }
                }
                else if (empty($next) and count($list) > 0) {
                    if ($type == $this->type_select) {
                        $listener = $obj->add_listener('ready');

                        $handler = $listener->add_handler('reload')
                                ->set_target('stock-price')
                                ->add_param_input($arr_param)
                                ->set_url(curl::base() . 'reload/reload_stock_price_product/' . $code . '/' . $this->key);

                        $listener = $obj->add_listener('change');

                        $handler = $listener->add_handler('reload')
                                ->set_target('stock-price')
                                ->add_param_input($arr_param)
                                ->set_url(curl::base() . 'reload/reload_stock_price_product/' . $code . '/' . $this->key);
                    }
                    if ($type == $this->type_image) {
                        $listener = $obj->add_listener('ready');

                        $handler = $listener->add_handler('reload')
                                ->set_target('stock-price')
                                ->add_param_input($arr_param)
                                ->set_url(curl::base() . 'reload/reload_stock_price_product/' . $code . '/' . $this->key);

                        $data_addition = '';
                        foreach ($arr_param as $inp) {
                            if (strlen($data_addition) > 0)
                                $data_addition.=',';
                            $data_addition.="'" . $inp . "':$.cresenity.value('#" . $inp . "')";
                        }

                        $data_addition = '{' . $data_addition . '}';

                        $listener = $obj->add_listener('click');

                        $handler = $listener->add_handler('custom')
                                ->set_js("
                                        var key=$(this).attr('id');
                                        $('#" . 'att_cat_' . $id . '_'. $index. "').val(key);

                                        $.cresenity.reload('stock-price','" . curl::base() . "reload/reload_stock_price_product/" . $code . "/" . $this->key . "','get'," . $data_addition . ");
                                        ");
                    }
                }
            }
        }

        return $control;
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);

        $product_id = carr::get($this->detail_list, 'product_id', NULL);
        $name = carr::get($this->detail_list, 'name', NULL);
        $sku = carr::get($this->detail_list, 'sku', NULL);
        $availability = carr::get($this->detail_list, 'is_available', NULL);
        $quick_overview = carr::get($this->detail_list, 'quick_overview', NULL);
        $stock = carr::get($this->detail_list, 'stock', 0);
        $weight = carr::get($this->detail_list, 'weight', 0);
        $show_minimum_stock = carr::get($this->detail_list, 'show_minimum_stock', 0);
        $this->stock = $stock;
        $this->price = carr::get($this->detail_list, 'detail_price', 0);
        $detail_price = carr::get($this->detail_list, 'detail_price', 0);

        $array_size = carr::get($this->detail_list, 'size');
        $array_color = carr::get($this->detail_list, 'color');

        $url_key_id = carr::get($this->detail_list, 'url_key');
        if (strlen($url_key_id) == 0) {
            $url_key_id = $product_id;
        }

        if (count($detail_price) == 0) {
            $detail_price = $product_v;
        }

        $promo_start_date = carr::get($detail_price, 'promo_start_date');
        $promo_end_date = carr::get($detail_price, 'promo_end_date');
        $promo_text = carr::get($detail_price, 'promo_text');
        $promo_value = carr::get($detail_price, 'promo_value');

        $price_all = $this->get_price($detail_price);

        $price_promo = carr::get($price_all, 'promo_price');
        $price_normal = carr::get($price_all, 'price');

        $percent_sale = '';
        $percent_label = '';
        if ($promo_value != 0) {
            $promo_type = carr::get($detail_price, 'promo_type');
            $promo_value = carr::get($detail_price, 'promo_value');
            if ($promo_type == 'amount') {
                $price_promo = $price_normal - $promo_value;
                $price = 'Rp ' . ctransform::format_currency($price_promo);
                $price_strike = 'Rp ' . ctransform::format_currency($price_normal);
                $percent_sale = round(($promo_value / $price_normal) * 100);
                $percent_label = $percent_sale . '%';
            }
            else if ($promo_type == 'percent') {
                $price_percent = $price_normal * ($promo_value / 100);
                $price_promo = $price_normal - $price_percent;
                $price = 'Rp ' . ctransform::format_currency($price_promo);
                $price_strike = 'Rp ' . ctransform::format_currency($price_normal);
                $percent_sale = $promo_value;
                $percent_label = $percent_sale . '%';
            }
        }


        $data_package = product::get_package_product($product_id);
        $package_stock = carr::get($data_package, 'stock');

        $this->url_share = curl::httpbase() . 'product/item/' . $url_key_id;
        $this->text_share = htmlspecialchars($name);

        // Build UI
        $detail_product = $this->add_div()->add_class('detail-product padding-top-10');
        $product_action_div = $detail_product->add_div()->add_class('col-md-12 padding-0 product-action');

        $form = $product_action_div->add_form('')->set_action(curl::base() . 'request/add/');
        
        $rowform = $form->add_div()->add_class('row');

        //form attribute
        $class = "col-xs-8 col-md-12";
        $form_atribute = $rowform->add_div()->add_class('product-attribute')->add_class($class);

        foreach ($this->attribute_list as $code_attribute => $attribut_list) {
            $label = $form_atribute->add_div()->add_class('col-md-4')->add($attribut_list['attribute_category_name'] . " :");
            $attribute_label = $form_atribute->add_div()->add_class('col-md-6');
            $this->generate_form($attribute_label, $code_attribute, $attribut_list, $this->index_item);   
        }            

        $display = "";
        if ($data_package > 0) {
            $display = "display:none";
        }
        
        //quantity
        if(!$this->is_package_product){
            if ($this->with_qty) {
                $container_qty = $rowform->add_div()->add_class('col-xs-12 col-md-12 container-qty')->add_attr('style', $display);
                $container_qty->add_div()->add_class('col-md-5')->add(clang::__('Quantity'))->add(' :');
                $container_qty->add_div()->add_class('col-md-6 qty-wrapper')->add("<input type='text' class='qty' id='qty' name='qty' value='1' style='z-index:0' readonly>");
            }
        }

        //stocks
        //
        //views
        $data_detail_product = product::get_product($product_id);
        //total sales
        $view_product = product::get_total_sales($product_id);

        $product_tag = array();

        $last_seen = carr::get($data_detail_product, 'viewed');
        $overview = carr::get($data_detail_product, 'overview');
        $show_minimum_stock = carr::get($data_detail_product, 'show_minimum_stock', 0);
        $tag = carr::get($data_detail_product, 'product_tag');
        $stock = carr::get($data_detail_product, 'stock', 0);

        if ($tag != null && $tag != '') {
            $product_tag = explode(",", $tag);
        }

        $stock_info = $stock;
        if ($show_minimum_stock > 0) {
            if ($stock > 0 && $stock <= $show_minimum_stock) {
                $stock_info = '<span class="stock">' . $stock . '</span>';
            }
        }


        if(!$this->is_package_product){
            $product_price = $detail_product->add_div()->add_class('product-price-detail');
            $product_price->custom_css('clear', 'both');
            $price = $this->get_price();
            $price_strike = '';

            if ($price['promo_price'] > 0) {
                $price_strike = ctransform::format_currency($price['price']);

                $product_price->add('<p class="promo-price-text-detail">Rp ' . ctransform::format_currency($price['price']) . '</p> <p class="price-text-detail">Rp ' . ctransform::format_currency($price['promo_price']) . '</p>');

                $title_inside = $product_price->add_div()->add_class('text-center product-detail');
                $discon_text = $title_inside->add('<h3 class="product-discount-text-detail">' . $percent_label . '</h3> <h3 class="product-discount-text-off-detail">OFF</h3>');
            } else {
                $product_price->add_div()->add_class('product-detail-normal-price')->add('<h2 class="product-discount-text">Rp ' . ctransform::format_currency($price['price']) . '</h2>');
            }

            $stok = $detail_product->add_div()->add_class('col-md-12 padding-0 detail-stok')->add('<hr>');
            $stok->add_div()->add_class('col-xs-4 col-sm-4 col-md-4 info-col')->add('<div class="product-detail-info-container"><div class="info-icon"><div class="icon-eye-open"></div></div><div class="product-info-text"><p class="padding-left-10 eye-detail"><span class="info_stok">Dilihat : </span>' . $last_seen . '</p>  </div></div>');
            $stok->add_div()->add_class('col-xs-4 col-sm-4 col-md-4 info-col')->add('<div class="product-detail-info-container"><div class="info-icon"><div class="icon-shop-cart"></div></div><div class="product-info-text"><p class="padding-left-10 shop-detail"><span class="info_stok">Stock : </span>' . $stock_info . '</p>  </div></div>');
            $stok->add_div()->add_class('col-xs-4 col-sm-4 col-md-4 info-col')->add('<div class="product-detail-info-container"><div class="info-icon"><div class="icon-tag-cart"></div></div><div class="product-info-text"><p class="padding-left-10 tag-detail"><span class="info_stok">Terjual : </span>' . $view_product . '</p> </div></div> ');

            //info ongkir
            $ongkir = ccfg::get('min_order_free_ongkir');
            $nominal_ongkir = ctransform::format_currency($ongkir);
            $freeong = $detail_product->add_div()->add_class('col-md-12 padding-0 product-action detail-info-ongkir')->add('<hr/>');
            $info_freeong = $freeong->add_div()->add_class('ongkir');
            $info_freeong->add_div()->add_class('col-md-3 freeong');
            //$text_free_ongkir = 'Gratis Ongkir untuk pesanan lebih dari Rp' . $nominal_ongkir . ' khusus wilayah jabodetabek';
            $text_free_ongkir = '
                FREE ONGKIR JABODETABEK dan BANDUNG minimal pembelanjaan Rp 60.000-
                <br/>PROMO subsidi ongkir Rp 10.000-, minimal pembelanjaan Rp.75.000- DILUAR JABODETABEK berlaku kelipatan
            ';
            $info_ongkir = $info_freeong->add_div()->add_class('col-md-9 detail-freeong')->add($text_free_ongkir);
        }


        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $max = $this->stock;
        if ($max < 1) {
            $max = 1;
        }
        
        $js->appendln("
            $('.btn-colors').click(function(){
                id = $(this).attr('id');
                $('#colors').val(id);
                $('.btn-colors').removeClass('active');
                $(this).addClass('active');
            });  
            
            
		");

        $js->appendln("
            $('.facebook').click(function(){
                var fbpopup = window.open('http://www.facebook.com/sharer/sharer.php?u=" . $this->url_share . "', 'pop', 'width=600, height=400, scrollbars=no');
                return false;
            });
            $('.twitter').click(function(){
                                var twpopup = window.open(\"//twitter.com/intent/tweet?url=" . $this->url_share . "&text=" . $this->text_share . "&original_referer=" . $this->url_share . "\",600, 400);
                return false;
            });
             $('.google').click(function(){
                                var twpopup = window.open('//plus.google.com/share?url=" . $this->url_share . "','', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=600');
                return false;
            });
        ");

//        $js->appendln("
//            /*jQuery('#product_tab_overview-tab-widget .box-header h5').remove();
//            jQuery('#product_tab_overview-tab-nav li a').click(function() {
//                jQuery('.product-tab-caret').remove();
//                jQuery(this).parents('li').append('<div class=\"product-tab-caret\"></div>');
//            });*/
//        ");
//        $js->appendln($vjs);
        $js->append(parent::js($indent));
        return $js->text();
    }

}
