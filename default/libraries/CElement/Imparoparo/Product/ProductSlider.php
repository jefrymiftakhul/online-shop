<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CElement_Imparoparo_Product_ProductSlider extends CElement_SlickSlideShow {

    public function __construct($id = "", $tag = "div") {
        parent::__construct($id, $tag);
        $responsive = array();
        $responsive[] = array(
            'breakpoint' => '1200',
            'slide_to_show' => '4',
            'slide_to_scroll' => '4',
            'dots' => 'false',
        );
        $responsive[] = array(
            'breakpoint' => '1100',
            'slide_to_show' => '4',
            'slide_to_scroll' => '4',
            'dots' => 'false',
        );
        $responsive[] = array(
            'breakpoint' => '992',
            'slide_to_show' => '3',
            'slide_to_scroll' => '3',
            'dots' => 'false',
        );
        $responsive[] = array(
            'breakpoint' => '768',
            'slide_to_show' => '2',
            'slide_to_scroll' => '2',
            'dots' => 'false',
        );

        $this->responsive = $responsive;
        $this->dots = "false";
        $this->page = 'product';
        $this->slide_to_scroll = 5;
        $this->infinite = "true";
        $this->slide_to_show = 5;
    }

    public static function factory($id = "", $tag = "div") {
        return new CElement_Imparoparo_Product_ProductSlider($id, $tag);
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();

        $classes = $this->classes;
        $classes = implode(" ", $classes);
        if (strlen($classes) > 0) {
            $classes = " " . $classes;
        }

        $counter = 0;
        $html->append('<div id="' . $this->id . '" class="product-slick-slider ' . $classes . '">');
        foreach ($this->slides as $k => $v) {
            $html->appendln('<div>');
            $html->appendln(CElement_Imparoparo_Product_Card::factory()->set_product($v)->set_page($this->page)->html());
            $html->appendln('</div>');
        }
        $html->append('</div>');
//        $html->appendln(parent::html($indent));

        return $html->text();
    }

}
