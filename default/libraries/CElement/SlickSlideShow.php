<?php

    class CElement_SlickSlideShow extends CElement {

        protected $slides = array();
        protected $type = 'product';
        protected $link = NULL;
        protected $page;
        protected $dots;
        protected $infinite;
        
        protected $slide_to_show;
        protected $slide_to_scroll;
        protected $responsive;
        protected $autoplay;
        protected $autoplay_speed;
        protected $auto_width;
        protected $center_mode = false;
        protected $swipe_to_slide = false;
        
        public function __construct($id = "", $tag = "div") {
            parent::__construct($id, $tag);
            $this->page = null;
            $this->slide_to_show = 1;
            $this->slide_to_scroll = 1;
            $this->responsive = array();
            $this->dots = "true";
            $this->infinite = "false";
            $this->autoplay = false;
            $this->autoplay_speed = 2000;
            $this->auto_width = false;
        }

        public static function factory($id = "", $tag = "div") {
            return new CElement_SlickSlideShow($id, $tag);
        }

        
        public function html($indent = 0) {
            $html = new CStringBuilder();

            $classes = $this->classes;
            $classes = implode(" ", $classes);
            if (strlen($classes) > 0) {
                $classes = " " . $classes;
            }

            $html->append('<div id="' . $this->id . '" class="' .$classes .'" style="visibility:hidden !important;">');
            foreach ($this->slides as $k => $v) {
                $content = carr::get($v, 'content');
                $html->appendln('
                    <div>
                        <h3>' .$content .'</h3>
                    </div>');
            }
            $html->append('</div>');
            $html->appendln(parent::html($indent));

            return $html->text();
        }

        public function js($indent = 0) {
            $js = new CStringBuilder();
            $responsive_set = array();
 
            foreach ($this->responsive as $k => $v) {
                $breakpoint = carr::get($v, 'breakpoint');
                $slide_to_show = carr::get($v, 'slide_to_show', 1);
                $slide_to_scroll = carr::get($v, 'slide_to_scroll', 1);
                $infinite = carr::get($v, 'infinite', 'true');
                $dots = carr::get($v, 'dots', 'true');
                $center_mode = carr::get($v,'centerMode');
                $arrows = carr::get($v,'arrows');
                $autowidth = carr::get($v,'variableWidth');
                if($autowidth == NULL){$autowidth = 'false';}
                if($center_mode == NULL){$center_mode = 'false';}
                if($arrows == NULL){$arrows = 'true';}
                $str = "{
                        breakpoint: " .$breakpoint ." ,
                        settings: {
                          slidesToShow: " .$slide_to_show .",
                          slidesToScroll: " .$slide_to_scroll .",
                          infinite: " .$infinite .",
                          dots: " .$dots .",
                          centerMode: ".$center_mode.",
                          arrows : ".$arrows.",
                          variableWidth : ".$autowidth."
                        }
                      }";
                $responsive_set[] = $str;
            }
            $responsive_str = '';
            if (count($responsive_set) > 0) {
                $responsive_str = 'responsive: [' .implode(',', $responsive_set) .'], ';
            }
            $js->appendln("
                jQuery('#".$this->id."').on('init',function(slick){
                  jQuery('#".$this->id."').css('visibility', 'visible');
                })
                .slick({
                    dots: " .$this->dots .",
                    infinite: " .$this->infinite .",
                    speed: 300,
                    slidesToShow: " .$this->slide_to_show .",
                    slidesToScroll: " .$this->slide_to_scroll .",");
            if ($this->autoplay) {
                $js->appendln('autoplay: true,');
                $js->appendln('autoplaySpeed: '.$this->autoplay_speed .',');
            }
            if ($this->center_mode) {
                $js->appendln('centerMode: true,');
            }
            
            if ($this->swipe_to_slide) {
                $js->appendln('swipeToSlide: true,');
            }
            if ($this->auto_width) {
                $js->appendln('variableWidth: true,');
            }
            $js->appendln($responsive_str);
            $js->appendln("
                    });
                ");
            $js->append(parent::js($indent));
            return $js->text();
        }

        public function set_slides($slides) {
            $this->slides = $slides;
            return $this;
        }

        public function set_center_mode($bool){
          $this->center_mode = $bool;
          return $this;
        }
        
        public function set_type($val) {
            $this->type = $val;
            return $this;
        }

        function set_page($page) {
            $this->page = $page;
            return $this;
        }
        
        function get_slide_to_show() {
            return $this->slide_to_show;
        }
        
        function set_swipe_to_slide($bool) {
            return $this->swipe_to_slide = $bool;
        }

        function get_slide_to_scroll() {
            return $this->slide_to_scroll;
        }

        function get_responsive() {
            return $this->responsive;
        }

        function set_slide_to_show($slide_to_show) {
            $this->slide_to_show = $slide_to_show;
            return $this;
        }

        function set_slide_to_scroll($slide_to_scroll) {
            $this->slide_to_scroll = $slide_to_scroll;
            return $this;
        }

        function set_responsive($responsive) {
            $this->responsive = $responsive;
            return $this;
        }

        public function get_dots() {
            return $this->dots;
        }

        public function set_dots($dots) {
            $this->dots = $dots;
            return $this;
        }
        public function set_auto_width($bool) {
            $this->auto_width = $bool;
            return $this;
        }

        
    }
    