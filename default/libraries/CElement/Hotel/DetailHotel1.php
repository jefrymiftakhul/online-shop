<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of Search
     *
     * @author Ittron-18
     */
    class CElement_Hotel_DetailHotel extends CElement {

        protected $data_room;
        protected $data_form;
        protected $atoken;
        protected $hotel_code;
        protected $max_night;
        protected $max_room;
        protected $request_args;

        public function __construct($id = "") {
            parent::__construct($id);
            $this->data_form = '';
            $this->atoken = '';
            $this->hotel_code = '';
            $this->data_room = array();
            $this->max_night = 9;
            $this->max_room = 9;
            $this->request_args = array();
        }

        public static function factory($id) {
            return new CElement_Hotel_DetailHotel($id);
        }

        public function set_data_room($data_room) {
            $this->data_room = $data_room;
            return $this;
        }

        public function set_data_form($data_form) {
            $this->data_form = $data_form;
            return $this;
        }

        public function set_atoken($atoken) {
            $this->atoken = $atoken;
            return $this;
        }

        public function set_hotel_code($hotel_code) {
            $this->hotel_code = $hotel_code;
            return $this;
        }

        public function set_request_args($request_args) {
            $this->request_args = $request_args;
            return $this;
        }

        public function generate_top_description() {
            $hotel_name = carr::get($this->data_room, 'hotel_name', array());
            $hotel_address = carr::get($this->data_room, 'address', array());
            $hotel_address = carr::get($hotel_address, '0');
            $hotel_images = carr::get($this->data_room, 'images', array());
            $rating = carr::get($this->data_room, 'rating');
            $rating = str_replace("*", '', $rating);
            
            $container = $this->add_div()
                    ->add_class('row');
            $container_image = $container->add_div()
                    ->add_class('col-md-12')
                    ->add_class('hotel-container-image');
            $container_title = $container_image->add_div()
                    ->add_class('hotel-container-title');
            $title = $container_title->add_div()
                    ->add($hotel_name)
                    ->add_class('hotel-ct-title');
            $title->add_div()
                    ->add_class('ico-'.$rating.'-star');
//            $container_title->add_div()
//                    ->add($hotel_city)
//                    ->add_class('hotel-ct-city');

            
            $container_top = $container_image->add_div()
                    ->add_class("hotel-container-table");
            $container_top_desc = $container_top->add_div()
                    ->add_class("col-md-3")
                    ->add_class("hotel-side-image");
            
            //address
            $container_top_desc->add_div()
                    ->add($hotel_address)
                    ->add_class("hotel-si-container") // si = side-image
                    ->add_class("hotel-ct-city");
            
            //Show map
            $container_top_desc->add_div()
                    ->add_class("hotel-detail-show-map")
                    ->add(clang::__("(Show Map)"))
                    ->add_class("hotel-ct-map"); //ct = container
            
            //Show Detail
            $container_top_desc->add_div()
                    ->add(clang::__("Show Detail"))
                    ->add_class("hotel-ct-show-detail");
            
            //Hotel Facility
            $container_top_desc->add_div()
                    ->add(clang::__("Hotel Facililty"))
                    ->add_class("hotel-ct-facility")
                    ->add_class("hotel-ct-button");
            //Hotel Info
            $container_top_desc->add_div()
                    ->add(clang::__("Hotel Info"))
                    ->add_class("hotel-ct-info")
                    ->add_class("hotel-ct-button");
            //Interest Place
            $container_top_desc->add_div()
                    ->add(clang::__("Interest Place"))
                    ->add_class("hotel-ct-interest")
                    ->add_class("hotel-ct-button");
            
            //price start from
            $container_top_desc->add_div()
                    ->add(clang::__("Price Start from"))
                    ->add_class("hotel-ct-price-sf");
            
            //price IDR
            $container_top_desc->add_div()
                    ->add("IDR 556.000")
                    ->add_class("hotel-ct-price-idr");
            
            //btn book now
            $container_top_desc->add_div()
                    ->add_class("hotel-ct-btn-book")
                    ->add(clang::__("Book<br>Now"));
            
              $container_image_top = $container_top->add_div()
                    ->add_class("col-md-9")
                    ->add_class("hotel-cib");
            
            $container_image_big = $container_image_top->add_div()
                    ->add_class('hotel-cib-select');
            
            if(count($hotel_images) > 0){
                $image_bigger = str_replace("/small/", "/bigger/", $hotel_images['0']['url']);
                
                $check_image = $this->check_big_image($image_bigger);
                if($check_image == 'false'){
                    $image_bigger = str_replace("/bigger/", '/', $image_bigger);
                }

                $image_object = CFactory::create_img();
                $image_object = $image_object->set_src($image_bigger);
                
                $container_image_big->add($image_object);
            }

            $container_image_small_row = $container_image->add_div()
                    ->add_class("row");
            $container_image_small = $container_image_small_row->add_div()
                    ->add_class('hotel-cis-container col-md-12');

            foreach($hotel_images as $key => $value){
                $image_bigger = str_replace("/small/", "/bigger/", $value['url']);
                $check_image = $this->check_big_image($image_bigger);
                
                if($check_image == 'false'){
                    $image_bigger = str_replace("/bigger/", '/', $value['url']);
                }

                $image_small = CFactory::create_img();
                $image_small = $image_small->set_src($image_bigger);
                $container_image_small->add_div()
                        ->add_class("hotel-cis-small")
                        ->add($image_small);
            }

            $btn_prev = $container_image_small->add_div()
                    ->add_class('hotel-cis-prev');
            $btn_prev->add_div()
                    ->add_class("hotel-cis-prev-btn");

            $btn_next = $container_image_small->add_div()
                    ->add_class('hotel-cis-next');
            $btn_next->add_div()
                    ->add_class("hotel-cis-next-btn");
        }

        public function check_big_image($image){
            $data = 'true';
            $ch = curl_init($image);
            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_exec($ch);
            $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if($retcode == 404){
                $data = 'false';
            }
            curl_close($ch);
            return $data;
        }
        

        public function generate_room() {
            $this->add_div('data_form')
                    ->add_class('hotel-form')
                    ->set_attr('hf', $this->data_form)
                    ->set_attr('atoken', $this->atoken)
                    ->set_attr('hc', $this->hotel_code);

            $container = $this->add_div()
                    ->add_class('row');

            $container_detail = $container->add_div()
                    ->add_class('col-md-12');

            $room_detail_all = $container_detail->add_div()
                    ->add_class('row');

            $room_detail = $room_detail_all->add_class('hotel-room-detail');

            $room_detail->add_div()
                    ->add(clang::__("Room Type"))
                    ->add_class('col-md-4 hotel-room-type');

            $room_detail->add_div()
                    ->add(clang::__("Guest Total"))
                    ->add_class('col-md-2 hotel-room');

            $room_detail->add_div()
                    ->add(clang::__("Board"))
                    ->add_class('col-md-2 hotel-room');
            
            $room_detail->add_div()
                    ->add(clang::__("Room"))
                    ->add_class('col-md-1 hotel-room');
            $room_detail->add_div()
                    ->add(clang::__("Price"))
                    ->add_class('col-md-3 hotel-room-price');
            $data_room_select = array();
            $data_night_select = array();

            for ($i = 0; $i <= $this->max_room; $i++) {
                $data_room_select[$i] = $i;
            }
            for ($i = 1; $i <= $this->max_night; $i++) {
                $data_night_select[$i] = $i;
            }

            $all_room_type = hotel::set_hidden_value($this->data_room);
            $room_detail->add_control('art', 'hidden')->set_value($all_room_type);

            $room_detail->add_control('hf', 'hidden')->set_value($this->data_form);

            $arr_room = carr::get($this->data_room, 'room_category', array());

            $i = 1;
            foreach ($arr_room as $key => $value) {
                $room_type = carr::get($value, 'room_type');
                if (count($room_type) < 1) {
                    // room not available
                }
                else {
                    foreach ($value['room_type'] as $room_type_k => $room_type_v) {
                        $price = $room_type_v['total_price'];
                        $price_ori = str_replace(".000", "", $price);
                        $price = ctransform::thousand_separator($price_ori, 0);

                        $row = $room_detail->add_div()
                                ->add_class('col-md-12')
                                ->add_class('hotel-row-detail');


                        $cont_room_type = $row->add_div()
                                ->add_class('col-md-4')
                                ->add_class('hotel-room-name');
                        $cont_room_type->add($room_type_v['room_type_name']);

                        $image_object = CFactory::create_img();
                        $image_object->set_src("http://aff.bstatic.com/images/hotel/max500/504/50479634.jpg");

                        $cont_room_type->add_div()
                                ->add($image_object)
                                ->add_class('hotel-image-room');
                        $cont_room_type->add_div()
                                ->add_class('hotel-text-more-about')
                                ->set_attr('k', $key)
                                ->set_attr('rtk', $room_type_k)
                                ->add(clang::__("More about this room"))
                                ->add_div()
                                ->add_class('hotel-icon-more-about');
                        
                        $occupation = $room_type_v['occupation']['max_adults'];
                        $row_max = $row->add_div()
                                ->add_class('hotel-number-night')
                                ->add_class('col-md-2');
                        $row_max->add_div()
                                ->add_class("hotel-".$occupation."-occupation");
                        $row_max->add_div()
                                ->add_class("hotel-text-black")
                                ->add("Max ".$occupation);

                        $row_board = $row->add_div()
                                ->add_class('hotel-number-night')
                                ->add_class('col-md-2');
                        $row_board->add_div()
                                ->add($room_type_v['board_code'])
                                ->add_class("hotel-text-black");

                        $row_board->add_div()
                                ->add(clang::__("Cancel Policy"))
                                ->add_class("hotel-text-black");

                        $room_value = 0;
                        if ($key == 0) {
                            $room_value = 1;
                        }
                        $row->add_div()
                                ->add_class('hotel-number-room')
                                ->add_class('col-md-1')
                                ->add_div()
                                ->add_control('room-' . $key . "_" . $room_type_k, 'select')
                                ->add_class('hotel-select-room')
                                ->set_list($data_room_select)
                                ->set_value($room_value);

                        $room_price = $row->add_div()
                                ->add_class('col-md-3')
                                ->add_class('hotel-price');

                        $room_price->add_div()
                                ->add_class('hotel-text-price')
                                ->add('IDR. ' . $price);
                        $url_submit = curl::base() . "hotel/reservationHotel?hc=" . $this->hotel_code . "&atoken=" . $this->atoken . "&rm=" . $key . "." . $room_type_k;

                        $action = $room_price->add_action()
                                ->set_label('Book Now')
                                ->add_class('btn-book')
                                ->set_submit(false);
                        $action->add_listener('click')->add_handler('custom')
                                ->set_js("
                                        jQuery(this).parents('form').attr('action','" . $url_submit . "');
                                        var total_room = jQuery(this).parents('.hotel-row-detail').find('.hotel-select-room').val();
                                        if(total_room == 0){
                                            alert('Please Select Number of Room');
                                        }
                                        else {
                                            //send_total_room();
                                            jQuery('.hotel-form-booking').submit();
                                        }");
                    } // end foreach room_type
                }
            } // end foreach room
        }

        public function generate_info() {
            $container = $this->add_div()
                    ->add_class('row');
            $container_info = $container->add_div()
                    ->add_class('col-md-12')
                    ->add_class('hotel-container-info');

            //row 1
            $container_info_item = $container_info->add_div()
                    ->add_class("hotel-ci-facility")
                    ->add_class("hotel-ci-item");
            $container_info_title = $container_info_item->add_div()
                    ->add_class('hotel-ci-title on');
            $container_info_title->add_div()
                    ->add_class('hotel-ci-title-icon');
            $container_info_title->add_div()
                    ->add_class('hotel-ci-title-text')
                    ->add(clang::__("Hotel Facility"));
            $container_info_content = $container_info_item->add_div()
                    ->add_class("hotel-ci-content")
                    ->add_class("row");

            
            //hotel facility 
            $arr_facility = carr::get($this->data_room, 'facilities_category', array());
            $container_info_content_box = $container_info_content->add_div()
                    ->add_class('col-sm-4');
            foreach($arr_facility as $af_k => $af_v){
                $facilities = $af_v['facilities'];
                foreach($facilities as $key => $value){
                    $container_info_content_box->add_div()
                            ->add_class("hotel-cicb-icon");
                    $container_info_content_box->add_div()
                            ->add(clang::__($value['name']))
                            ->add_class("hotel-cicb-text");
                }
                
            }
            
            
//            $container_info_content_box->add_div()
//                    ->add_class("hotel-cicb-icon");
//            $container_info_content_box->add_div()
//                    ->add(clang::__("Swimming Pool"))
//                    ->add_class("hotel-cicb-text");
//            $container_info_content_box = $container_info_content->add_div()
//                    ->add_class('col-sm-4');
//            $container_info_content_box->add_div()
//                    ->add_class("hotel-cicb-icon");
//            $container_info_content_box->add_div()
//                    ->add(clang::__("Parking Area"))
//                    ->add_class("hotel-cicb-text");
//            $container_info_content_box = $container_info_content->add_div()
//                    ->add_class('col-sm-4');
//            $container_info_content_box->add_div()
//                    ->add_class("hotel-cicb-icon");
//            $container_info_content_box->add_div()
//                    ->add(clang::__("Car Rental"))
//                    ->add_class("hotel-cicb-text");
//            $container_info_content_box = $container_info_content->add_div()
//                    ->add_class('col-sm-4');

            //row 2
            $container_info_item = $container_info->add_div()
                    ->add_class("hotel-ci-info")
                    ->add_class("hotel-ci-item");
            $container_info_title = $container_info_item->add_div()
                    ->add_class('hotel-ci-title on');
            $container_info_title->add_div()
                    ->add_class("hotel-ci-title-icon");
            $container_info_title->add_div()
                    ->add(clang::__("Hotel Info   <span class='hotel-detail-show-map'>(Show Map)</span>"))
                    ->add_class("hotel-ci-title-text");

            $container_info_content = $container_info_item->add_div()
                    ->add_class("hotel-ci-content")
                    ->add_class("row");
            $container_info_content_box = $container_info_content->add_div()
                    ->add_class('col-xs-6 col-md-4');
            $container_info_content_box->add_div()
                    ->add(clang::__("Check in from"))
                    ->add_class("hotel-cicb-text");
            $container_info_content_box = $container_info_content->add_div()
                    ->add_class('col-xs-6 col-md-8');
            $container_info_content_box->add_div()
                    ->add("14:00")
                    ->add_class("hotel-cicb-text");
            $container_info_content_box = $container_info_content->add_div()
                    ->add_class('col-xs-6 col-md-4');
            $container_info_content_box->add_div()
                    ->add(clang::__("Check out until"))
                    ->add_class("hotel-cicb-text");
            $container_info_content_box = $container_info_content->add_div()
                    ->add_class('col-xs-6 col-md-8');
            $container_info_content_box->add_div()
                    ->add("16:00")
                    ->add_class("hotel-cicb-text");

            //row 3
            $container_info_item = $container_info->add_div()
                    ->add_class("hotel-ci-interest")
                    ->add_class("hotel-ci-item");
            $container_info_title = $container_info_item->add_div()
                    ->add_class('hotel-ci-title on');
            $container_info_title->add_div()
                    ->add_class("hotel-ci-title-icon");
            $container_info_title->add_div()
                    ->add(clang::__("Places of Interest Around the Hotel"))
                    ->add_class("hotel-ci-title-text");

            $container_info_content = $container_info_item->add_div()
                    ->add_class("hotel-ci-content")
                    ->add_class("row");
            for ($i = 0; $i < 5; $i++) {
                $container_info_content_box = $container_info_content->add_div()
                        ->add_class('col-xs-6 col-md-4');
                $container_info_content_box->add_div()
                        ->add(clang::__("Pantai Kute"))
                        ->add_class("hotel-cicb-text");
                $container_info_content_box = $container_info_content->add_div()
                        ->add_class('col-xs-6 col-sm-8');
                $container_info_content_box->add_div()
                        ->add("800M")
                        ->add_class("hotel-cicb-text");
            }
        }

        function generate_reviews(){
            $container = $this->add_div()
                    ->add_class("hotel-detail-reviews")
                    ->add_class('row');

            $all_reviews = $container->add_div()
                    ->add(clang::__("All Reviews"))
                    ->add_class("hotel-text-review");

            $box = $container->add_div()
                    ->add_class("hotel-box-container");
            $box_container = $box->add_div()
                    ->add_class("row");
            $box_container_1 = $box_container->add_div()
                    ->add_class("col-md-4");
            $box_container_2 = $box_container->add_div()
                    ->add_class("col-md-2");
            $box_container_3 = $box_container->add_div()
                    ->add_class("col-md-6");

            $reviews = carr::get($this->data_room, 'reviews', array());
            $box->add($reviews);
        }

        public function modal_detail() {
            $html = new CStringBuilder();
            $html->appendln('
                <div id="modal_room_detail" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Room Detail</h4>
                            </div>');
            $html->appendln('
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="hotel-detail-image-container">');
            $html->appendln('               <div class="hotel-detail-image-box">
                                            </div>
                                            <div class="hotel-detail-navigator">
                                                <div class="hotel-detail-nav-back"></div>
                                                <div class="hotel-detail-nav-page"></div>
                                                <div class="hotel-detail-nav-next"></div>
                                            </div>
                                            <div class="hotel-detail-price">
                                                IDR. 750,000
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div>
                                            <b>Room Facility</b>
                                            <div class="hotel-detail-room-facility">');
            $html->appendln('
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>');
            $html->appendln('
                        </div>

                    </div>
                </div>');

            return $html->text();
        }

        public function modal_map() {
            $html = new CStringBuilder();
            //script google map
            $html->appendln('<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script> ');
            $html->appendln('
                <div id="modal_map_hotel" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Map</h4>
                            </div>');
            $html->appendln('
                            <div class="modal-body">
                                <div class="row hotel-map-content">');
            $html->appendln('
                                </div>
                            </div>');
            $html->appendln('
                        </div>
                    </div>
                </div>');

            return $html->text();
        }

        public function html($indent = 0) {
            $html = new CStringBuilder();

            $html->appendln($this->generate_top_description());
            $html->appendln($this->generate_room());
            $html->appendln($this->generate_info());
            $html->appendln($this->generate_reviews());
            $html->appendln($this->modal_detail());
            $html->appendln($this->modal_map());
            
            // add hidden fields
            foreach ($this->request_args as $k => $v) {
                $this->add_control($k, 'hidden')->set_value($v);
            }

            $html->set_indent($indent);
            $html->append(parent::html($indent));
            return $html->text();
        }

        public function js($indent = 0) {
            $js = CStringBuilder::factory();

            $js->appendln("
                var image_small = jQuery('.hotel-cis-small');
                var total_image = image_small.length;
                var cis_prev = 0;
                var cis_next = 0;
                image_show();");

            //when click small image
            $js->appendln("                               
                jQuery('.hotel-cis-small').click(function(){
                    image_small.removeClass('on');
                    var image_select = jQuery(this);
                    var image_url = image_select.children('img').attr('src');
                    var cont_image = jQuery('.hotel-cib-select img');
                    image_select.addClass('on');
                    cont_image.fadeOut('fast', function () {
                        cont_image.attr('src', image_url);
                        cont_image.fadeIn('fast');
                    });                   
                });");

            //when window resize
            $js->appendln("
                jQuery(window).resize(function() {
                    image_show();
                });");

            //prev small image
            $js->appendln("
                jQuery('.hotel-cis-prev-btn').click(function(){
                   if(cis_prev != 0){
                        cis_prev--;
                        cis_next--;                       
                        jQuery('.hotel-cis-small:eq('+cis_prev+')').removeClass('hide');
                        jQuery('.hotel-cis-small:eq('+cis_next+')').addClass('hide');
                    } 
                });");

            //next small image
            $js->appendln("
                jQuery('.hotel-cis-next-btn').click(function(){
                    if(cis_next != total_image){
                        jQuery('.hotel-cis-small:eq('+cis_prev+')').addClass('hide');
                        jQuery('.hotel-cis-small:eq('+cis_next+')').removeClass('hide');
                        cis_prev++;
                        cis_next++;
                    }                    
                });");

            //image count
            $js->appendln("
                function image_show(){                    
                    var size_container = jQuery('.hotel-cis-container').outerWidth(true);
                    var total_show = Math.floor(size_container / 85);
                    cis_next = parseInt(total_show);

                    if(total_image <= total_show){
                        jQuery('.hotel-cis-next-btn').addClass('hide');
                        jQuery('.hotel-cis-prev-btn').addClass('hide');
                    }
                    else{
                        jQuery('.hotel-cis-next-btn').removeClass('hide');
                        jQuery('.hotel-cis-prev-btn').removeClass('hide');   
                    }

                    image_small.removeClass('hide');
                    for(var i = total_show;i < total_image;i++){
                        jQuery('.hotel-cis-small:eq('+i+')').addClass('hide');
                    }
                }");

            //select night
            $js->appendln("
                jQuery('.hotel-select-room').change(function(){
                    var room_choose = jQuery(this).val();
                    jQuery('.hotel-select-room').val(0);
                    jQuery('.hotel-select-room').removeClass('on');
                    jQuery(this).val(room_choose);
                    jQuery(this).addClass('on');
                });
            ");

            //show hide information
            $js->appendln("
                jQuery('.hotel-ci-title-icon').click(function(){
                    var info_content = jQuery(this).parent('.hotel-ci-title').parent('.hotel-ci-item') .children('.hotel-ci-content');
                    if(jQuery(this).parent('.hotel-ci-title').hasClass('on')){
                        jQuery(this).parent('.hotel-ci-title').removeClass('on');
                        info_content.slideUp('fast');
                    }
                    else{
                        jQuery(this).parent('.hotel-ci-title').addClass('on');
                        info_content.slideDown('fast');
                    }
                });");



            //var for slider detail hotel
            $js->appendln("
                var hdi_now = 0;
                var total_hdi = 0;");

            $arr_room = carr::get($this->data_room, 'room_category', array());

            $request = $_GET;
            $check_in = carr::get($request, 'ci');
            $check_out = carr::get($request, 'co');
            $adult = carr::get($request, 'a');
            $child = carr::get($request, 'c');
            $country_code = carr::get($request, 'cc');
            $city_code = carr::get($request, 'ctc');
            $ipp = carr::get($request, 'ipp');

            $room_select = json_encode($arr_room);
            //button detail hotel, generate modal
            $js->appendln("
                var data_room_p = '" . $room_select . "';
                var data_room = jQuery.parseJSON(data_room_p);
                jQuery('.hotel-text-more-about').click(function(){                   
                    var key = jQuery(this).attr('k');
                    var room_type_key = jQuery(this).attr('rtk');
                    var room_select = data_room[key].room_type[room_type_key];
                    var price = room_select.total_basic_fare;
                    price = price.replace('.000', '');
                    price = numberThousand(price);
                    jQuery('.hotel-detail-price').html('IDR. '+price);
                        
                    jQuery('.hotel-detail-room-facility').html('');
                    // var temp_facility = ['Air Conditioner', 'TV', 'Bathroom'];
                    var temp_facility = room_select.facilities;
                    jQuery.each(temp_facility, function(index, value){
                        jQuery('.hotel-detail-room-facility').append(value.facility_name+'<br>');
                    });
                    
                    
                    jQuery('.hotel-detail-image-box').html('');
                    var temp_image = ['http://aff.bstatic.com/images/hotel/max500/504/50479634.jpg',
                                    'http://www.hotelbeds.com/giata/bigger/06/061863/061863a_hb_w_001.jpg',
                                    'http://www.hotelbeds.com/giata/bigger/40/403454/403454a_hb_w_003.jpg'];
                    jQuery.each(temp_image, function(index, value){
                          jQuery('.hotel-detail-image-box').append('<div class=\"hotel-detail-image\"><img src=\"'+value+'\"></div>');
                    });
                    reset_hotel_detail_image();
                    
                    jQuery('#modal_room_detail').modal('toggle');
                });");

            //slide detail hotel
            $js->appendln("
                reset_hotel_detail_image();
                function reset_hotel_detail_image(){
                    jQuery('.hotel-detail-image').addClass('hide');
                    jQuery('.hotel-detail-image:eq(0)').removeClass('hide');
                    hdi_now = 0;
                    total_hdi = jQuery('.hotel-detail-image').length;
                    jQuery('.hotel-detail-nav-page').html('1/'+total_hdi);
                }");

            //btn prev detail hotel
            $js->appendln("
                jQuery('.hotel-detail-nav-back').click(function() {
                    if(hdi_now > 0){
                        hdi_now--;
                        jQuery('.hotel-detail-image').addClass('hide');
                        jQuery('.hotel-detail-image:eq('+hdi_now+')').removeClass('hide');
                        jQuery('.hotel-detail-nav-page').html((hdi_now+1)+'/'+total_hdi);
                    }
                });
            ");

            //btn next detail hotel
            $js->appendln("
                jQuery('.hotel-detail-nav-next').click(function() {
                    if(hdi_now < (total_hdi -1)){
                        hdi_now++;
                        jQuery('.hotel-detail-image').addClass('hide');
                        jQuery('.hotel-detail-image:eq('+hdi_now+')').removeClass('hide');
                        jQuery('.hotel-detail-nav-page').html((hdi_now + 1)+'/'+total_hdi);
                    }
                });
            ");

            //click show map
            $js->appendln("
                jQuery('.hotel-detail-show-map').click(function(){
                    jQuery('#modal_map_hotel').modal('toggle');
                });
            ");

            $js->appendln("
                var map_opened = false;
                $('#modal_map_hotel').on('shown.bs.modal', function (e) {
                    if(map_opened == false){    
                        initialize_map();
                        map_opened = true;
                    }
                })");

            //create map
            $js->appendln("
                var lat = -7.3033448;
                var lng = 112.7451062;
                function initialize_map() {
                    var latlng = new google.maps.LatLng(lat, lng);
                    var myOptions = {
                        zoom: 16,
                        center: latlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    var map = new google.maps.Map(jQuery('.hotel-map-content')[0],
                            myOptions);
                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        title: 'Hotel',
                        // icon: 'http://i.imgur.com/TPnq84e.png'
                    });
                }                
                ");

            //click hotel facility top
            $js->appendln("
                jQuery('body').on('click', '.hotel-ct-facility', function(e){
                    jQuery('html, body').animate({
                        scrollTop: jQuery('.hotel-ci-facility').offset().top
                    }, 1000);
                });
            ");

            //click hotel information top
            $js->appendln("
                jQuery('body').on('click', '.hotel-ct-info', function(e){
                    jQuery('html, body').animate({
                        scrollTop: jQuery('.hotel-ci-info').offset().top
                    }, 1000);
                });
            ");

            //click hotel facility top
            $js->appendln("
                jQuery('body').on('click', '.hotel-ct-interest', function(e){
                    jQuery('html, body').animate({
                        scrollTop: jQuery('.hotel-ci-interest').offset().top
                    }, 1000);
                });
            ");

            //click hotel facility top
            $js->appendln("
                jQuery('body').on('click', '.hotel-ct-btn-book', function(e){
                    jQuery('html, body').animate({
                        scrollTop: jQuery('.hotel-room-detail').offset().top
                    }, 1000);
                });
            ");

            $js->appendln("
                function numberThousand(x) {
                    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                }                    
            ");

            $js->append(parent::js($indent));
            return $js->text();
        }

    }
    