<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of Search
     *
     * @author Ittron-18
     */
    class CElement_Hotel_Search extends CElement {

        protected $url_location;
        protected $local_url_location;
        protected $suid;
        protected $show_info;
        protected $subtitle;
        protected $show;
        protected $partner_hotel;

        public function __construct($id = "") {
            parent::__construct($id);
            $this->url_location = curl::httpbase();
            $this->local_url_location = curl::httpbase();
            $this->suid = null;
            $this->show_info = false;
            $this->subtitle = false;
            $this->show = true;
            $this->partner_hotel = false;
        }

        public static function factory($id) {
            return new CElement_Hotel_Search($id);
        }

        public function set_url($url) {
            $this->url_location = $url;
            return $this;
        }

        public function set_suid($suid) {
            $this->suid = $suid;
            return $this;
        }

        public function set_show($show) {
            $this->show = $show;
            return $this;
        }

        public function set_partner_hotel($show) {
            $this->partner_hotel = $show;
            return $this;
        }

        public function html($indent = 0) {
            $html = new CStringBuilder();
            $db = CDatabase::instance();
            $session = Session::instance();

            $html->set_indent($indent);

            $name = '';
            $id = $this->id;

            $city_val = "";
            $check_in_date = date("Y-m-d");
            $check_out_date = date("Y-m-d", strtotime("+1 day"));
            $adult_val = 1;
            $child_val = 0;
            $total_room_val = 1;
            $night_val = 1;

            $request = $_GET;

            if ($request != NULL) {
                $check_page = carr::get($request, 'cnc', '');
                $page = 'detail';
                if ($check_page != '') {
                    $page = 'search';
                }

                //if in search page
                if ($page == 'search') {
                    $city_val = carr::get($request, 'c');

                    $cnc = carr::get($request, 'cnc');
                    $temp_cnc = explode('.', $cnc);
                    if (count($temp_cnc) == 3) {
                        $check_in_date = trim($temp_cnc[0]);
                        $night_val = trim($temp_cnc[1]);
                        $check_out_date = trim($temp_cnc[2]);
                    }

                    $total_room_val = carr::get($request, 'r');

                    $pax_r = carr::get($request, 'p');
                    $temp_p = explode('.', $pax_r);
                    if (count($temp_p) == 2) {
                        $adult_val = trim($temp_p[0]);
                        $child_val = trim($temp_p[1]);
                    }
                }
                //if in detail page
                else if ($page == 'detail') {
                    $city_val = carr::get($request, 'hc');
                    $city_val = security::decrypt($city_val);
                    $check_in_date = carr::get($request, 'ci');
                    $check_out_date = carr::get($request, 'co');
                    $adult_val = carr::get($request, 'a');
                    $child_val = carr::get($request, 'c');
                    $night_val = abs(strtotime($check_out_date) - strtotime($check_in_date));
                    $night_val = $night_val / (60 * 60 * 24);

                    $total_room_val = carr::get($request, 'tr');
                }
            }
            $container = $this->add_div()->add_class($id .'-hotel-search-nav-container');
            
            if ($this->show_info) {
                $container->add_div()->add_class('air-search-info')->add(cms::get_option("hotel_search_info"));
            }

            
            if ($this->subtitle && $this->show) {
                // $post_title_container = $container->add_div()->add_class('el-product-subtitle-container');
                // $title_container = $post_title_container->add_div()->add_class('el-product-container');
                // $title_wrapper = $title_container->add_div()->add_class('subtitle-wrapper');
                // $title_wrapper->add_div()->add_class('line-wrapper');
                // $title_wrapper->add_div()->add_class('subtitle-label-wrapper')
                //         ->add_div()->add_class('subtitle-label')->add($this->subtitle);

                $box_title = $container->add_div()->add_class("search-title-box");
                $box_title->add_div()->add_class("search-title-box-style");
                $box_title_text = $box_title->add_div()->add_class("box-title-text");
                $box_title_text->add_div()->add_class('box-subtitle')->add_div()->add($this->subtitle);

            }

            if ($this->show) {
                ////$grid_container = $this->add_div()->add('a');
                $row = $container->add_div($id . '-container')->add_class('hotel-search-nav')
                        ->add_form($id . "-search_hotel_form");
                $row_in = $row->add_div()
                        ->add_class('row');
                $search_city_container = $row_in->add_div()->add_class('col-sm-8 hotel-input-container');

                $row_in_room_pax = $row_in->add_div()->add_class('col-sm-4 hotel-room-pax');
                $room_container = $row_in_room_pax->add_div()->add_class('col-xs-4');
                $adult_container = $row_in_room_pax->add_div()->add_class('col-xs-4');
                $child_container = $row_in_room_pax->add_div()->add_class('col-xs-4');
                $row_in = $row->add_div()
                        ->add_class('hotel-row-in')
                        ->add_class('row');
                $check_in_container = $row_in->add_div()->add_class('col-md-3 col-sm-5 hotel-input-container');
                $night_container = $row_in->add_div()->add_class('col-md-2 col-sm-2 hotel-label-pad');
                $check_out_container = $row_in->add_div()->add_class('col-md-3 col-sm-5 hotel-input-container');
                $button_container = $row_in->add_div()->add_class('col-md-4 col-sm-12')->add_class('hotel-row-btn')->custom_css('padding', '15px');

                $city = $search_city_container
                        ->add_field()
                        ->set_label(clang::__("Hotel Name, City, or Country of Destination"))
                        ->add_control($id . "-city", "hotel-select")
                        ->set_attr('tabindex', '1')
                        ->set_value($city_val)
                        ->custom_css('width', '100%');

                $data_room = array();
                for ($i = 1; $i <= hotel::$total_room; $i++) {
                    $data_room[$i] = $i;
                }
                $room = $room_container->add_div();
                $room->add_field()
                        ->set_label("Room")
                        ->add_control($id . "-nav_room", "dropdown")
                        ->add_class('d-bg-white') //change background to white
                        ->set_list($data_room)
                        ->set_value($total_room_val)
                        ->add_class('col-md-12')
                        ->set_attr('tabindex', '5');

                $data_pax = array();
                for ($i = 1; $i <= hotel::$total_adult; $i++) {
                    $data_pax[$i] = $i;
                }
                $pax = $adult_container->add_div();
                $pax->add_field()
                        ->set_label(clang::__("Adult"))
                        ->add_control($id . "-nav_pax", "dropdown")
                        ->add_class('d-bg-white') //change background to white
                        ->set_list($data_pax)
                        ->set_value($adult_val)
                        ->add_class('col-md-12')
                        ->set_attr('tabindex', '5');

                $data_child = array();
                for ($i = 0; $i < hotel::$total_adult; $i++) {
                    $data_child[$i] = $i;
                }
                $pax = $child_container->add_div();
                $pax->add_field()
                        ->set_label(clang::__("Child"))
                        ->add_control($id . "-nav_child", "dropdown")
                        ->add_class('d-bg-white') //change background to white
                        ->set_list($data_child)
                        ->set_value($child_val)
                        ->add_class('col-md-12')
                        ->set_attr('tabindex', '5');


                $check_in_container->add_field()
                        ->set_label(clang::__("Check in Date"))
                        ->add_control($id . "-check_in_date", 'date-hotel')
                        ->set_value($check_in_date)
                        ->add_class('start-date-hotel')
                        ->set_listener('change')
                        ->set_param('night-select')
                        ->set_target_changed('end-date-hotel');

                $data_night = array();
                for ($i = 1; $i <= 31; $i++) {
                    $data_night[$i] = $i;
                }
                $night = $night_container->add_div();
                $night->add_field()
                        ->set_label(clang::__("Night"))
                        ->add_control($id . "-night", "dropdown")
                        ->set_list($data_night)
                        ->set_value($night_val)
                        ->add_class('d-bg-white') //change background to white
                        ->add_class('col-md-12 night-select')
                        ->set_attr('tabindex', '5')
                        ->set_affect(true)
                        ->set_date_source('start-date-hotel')
                        ->set_date_target('end-date-hotel');
                $check_out_container->add_field()
                        ->set_label(clang::__("Check out Date"))
                        ->add_control($id . "-check_out_date", 'date-hotel')
                        ->set_value($check_out_date)
                        ->set_start_date($check_out_date)
                        ->set_listener('change')
                        ->set_param('night-select')
                        ->set_change_element(true)
                        ->set_target_element('start-date-hotel')
                        ->add_class('end-date-hotel');

                $button_container->add_div()
                        ->add_class('hotel-btn-search align-center hotel-btn')
                        ->add(clang::__('Search Hotel') . " &nbsp;
                      <span class='glyphicon glyphicon-search' aria-hidden='true'></span>");
            }

            if ($this->partner_hotel) {
                $col_partner_air = $container->add_div();
                $col_partner_air->add('<h1 class="title-info-partner">' .clang::__('Partner Hotel') .'</h1>');
                $list_image_partner = cms::get_image_list('hotel_partner');
                $col_list_image_partner = $col_partner_air->add_div()->add('<ul class="col-partner-list col-partner-list-hotel">');
                if (count($list_image_partner) > 0) {
                    foreach ($list_image_partner as $list) {
                        $col_list_image_partner->add('<li><img src="' . $list["image_url"] . '" alt="' . $list['name'] . '"></li>');
                    }
                }
                $col_list_image_partner->add('</ul>');
            }

            $html->append(parent::html($indent));
            return $html->text();
        }

        public function js($indent = 0) {
            $js = CStringBuilder::factory();

            // $js->appendln("                
            //     function callback_show_".$this->id."_check_in_date(base) {
            //         var co_val = jQuery('#".$this->id."_check_out_date-input').parent().datepicker().find('input').val();
            //         jQuery('#".$this->id."_check_in_date-input').parent().datepicker('setEndDate', '2016-01-15');
            //     }
            //     function callback_show_".$this->id."_check_out_date(base) {
            //         var ci_val = jQuery('#".$this->id."-check_in_date-input').parent().datepicker().find('input').val();
            //         jQuery('#".$this->id."-check_out_date-input').parent().datepicker('setStartDate', ci_val);
            //     }
            // ");

            $js->appendln("
            $('.hotel-btn-search').on('click', function(){
                var city = $('#" . $this->id . "-city').val();
                var check_in = $('#" . $this->id . "-check_in_date-input').val();
                var night = $('#" . $this->id . "-night-input').val();
                var check_out = $('#" . $this->id . "-check_out_date-input').val();
                var room = $('#" . $this->id . "-nav_room-input').val();
                var adult = $('#" . $this->id . "-nav_pax-input').val();
                var child = $('#" . $this->id . "-nav_child-input').val();
                    
                var url_a = '" . $this->url_location . "';
                var url_b = ''; // for check or search
                var url_c = ''; // for city or country or hotel
                var url_d = ''; // for city or hotel attr

                url_b += 'hotel/checkCountry?'; //first check city   
                url_c += 'c=' + city;
                
                url_d += '&cnc=' + check_in + '.' + night + '.' +check_out;
                url_d += '&r=' + room;
                url_d += '&p=' + adult + '.' + child;");
            if (strlen($this->suid) > 0) {
                $js->appendln("url_d += '&suid=" . $this->suid . "';");
            }
            $js->appendln("   
                $.ajax({
                    type    : 'GET',
                    url     : '" . $this->local_url_location . "'+url_b+url_c,
                    dataType: 'JSON'
                }).done(function(data){
                    var error = data.err_code;
                    var message = data.message;
                    var redir = false;
                    if (data.search_type == 'country'){
                        url_b = 'hotel/searchCountry?'; // change to search country 
                        redir = true;
                    }
                    else if(data.search_type == 'city'){
                        url_b = 'hotel/searchResult?'; // change to search hotel or city
                        redir = true;
                    }
                    else if(data.search_type == 'hotel_name'){
                        url_b = 'hotel/searchHotelName/?'; //change to search hotel name
//                        url_b = 'hotel/searchResult?';
                        redir = true;
                    }
                    else {
                        alert('Hotel not found');
                    }
                        
                    if(redir == true){
                        window.location.href = url_a+url_b+url_c+url_d;
                    }
                });
            });
        ");



            $js->append(parent::js($indent));

            return $js->text();
        }

        public function set_show_info($show_info) {
            $this->show_info = $show_info;
            return $this;
        }

        public function set_subtitle($subtitle) {
            $this->subtitle = $subtitle;
            return $this;
        }
    }
    