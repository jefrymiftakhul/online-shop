<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Nov 25, 2015
     */
    class CElement_Hotel_ReservationForm extends CElement {

        protected $form;
        protected $value;
        protected $child;
        protected $value_contact;

        public function __construct($id = '') {
            parent::__construct($id);

            $this->form = null;
        }

        public static function factory($id = '') {
            return new CElement_Hotel_ReservationForm($id);
        }

        public function set_form($form) {
            $this->form = $form;
            return $this;
        }

        public function set_value($value) {
            $this->value = $value;
            return $this;
        }

        public function set_child($child) {
            $this->child = $child;
            return $this;
        }

        public function set_value_contact($value_contact) {
            $this->value_contact = $value_contact;
            return $this;
        }

        public function generate_form($form_type, $form, &$div_form) {
            
            foreach ($form as $form_k => $form_v) {
                $field_notes = '';
                $type = carr::get($form_v, "type");
                $default = carr::get($form_v, "default");
                $label = carr::get($form_v, "label");
                $list = carr::get($form_v, "list");
                $rules = carr::get($form_v, "rules");
                $end_date = carr::get($form_v, "end_date");
                $start_date = carr::get($form_v, "start_date");

                $label = clang::__($label);
                switch ($type) {
                    case "select":
                        $type = "dropdown";
                        break;
                    case "text":
                        break;
                    case "phone":
                        $type = "phone-number";
                        break;
                    case "date":
                        $type = "date-dropdown";
                        break;
                    case "city":
                        $type = "city-select";
                        break;
                    case "province":
                        $type = "province-select";
                        break;
                    case "country":
                        $type = "country-select";
                        break;
                    default:
                        $type = "unknown";
                }
                if ($type != "unknown") {

                    if ($form_k == 'email') {
                        $field_notes = clang::__('Please make sure this is valid email. We will send your ticket to this email.');
                        $field_notes .= '<br/>' . clang::__('Example: example@gmail.com');
                    }
                    else if ($form_k == 'phone') {
                        $field_notes = clang::__('Example: +628123456789');
                    }
                    else if ($form_k == 'title') {
                        if (strlen($default) == 0) {
                            $default = 'MR';
                        }
                    }

                    if($form_k == 'phone'){
                        $type = 'phone-number';
                    }

                    if (in_array("required", $rules)) {
//                        $label = "<span class='form_label'>$label *</span>";
                        $label = "<span class='hotel-title-label'>$label *</span>";
                    }

                    $input_field = $div_form->add_field()->set_style_form_group('inline')->set_label($label);
                    $input = $input_field->add_control($form_type . "[" . $form_k . "]", $type);

                    if ($this->bootstrap == '3.3') {
                        $input_field->set_label_size(3)->set_inline_without_default('0');
                    }


                    if ($form_k == 'title') {
                        $input->add_class('col-md-3')->custom_css('padding-left', '0px');
                    }

                    if ($type == "dropdown") {
                        $input->set_list($list);
                    }
                    else if ($type == "date-dropdown") {
                        if (strlen($start_date) > 0) {
                            $input->set_start_date($start_date);
                        }
                        if (strlen($end_date) > 0) {
                            $input->set_end_date($end_date);
                        }
                    }

                    //data login
                    $obj_val = carr::get($this->value_contact, $form_k, $default);
                    if (strlen($obj_val) > 0) {
                        $input->set_value($obj_val);
                    }

                    $form_type_value = carr::get($this->value, $form_type);
                    $obj_val = carr::get($form_type_value, $form_k, $default);
                    if (strlen($obj_val) > 0) {
                        $input->set_value($obj_val);
                    }


                    if (in_array("required", $rules)) {
                        $input->add_validation('required');
                    }
                    if (strlen($field_notes) > 0) {
                        $info_field = $div_form->add_field()->set_style_form_group('inline');
                        if ($this->bootstrap == '3.3') {
                            $info_field->set_label_size(3)->set_inline_without_default('0');
                        }
                        $info_field->add_div()->add_class('airlines-field-info')->add($field_notes);
                    }
                }
                else {
                    // Error unknown control type
                }
            }
        }

        public function generate_form_child(){
            $this->add_div()
                ->add(clang::__("Child"))
                ->add_class("hotel-text-res-info");
            $box = $this->add_div()
                ->add_class("hotel-box-container");
            $box_value = $box->add_div()
                ->add_class("hotel-bc-value row");
            $box_value->add("<label class='col-md-3 control-label hotel-text-black'>".clang::__("Name")."</label>");
            $box_value->add_div()
                ->add_class("col-md-9")
                ->add_control('', 'text')
                ->add_validation('required')
                ->set_name('child_name');
        }

        public function html($indent = 0) {
            $html = new CStringBuilder();

            $wrapper = $this->add_div();
            $content = $wrapper->add_div();

            if ($this->form != null) {
                $form_contact = carr::get($this->form, 'contact');
                $form_passengers = carr::get($this->form, 'passengers');
                $form_adult = carr::get($form_passengers, 'adult');
                // print_r($form_adult)
//                $widget_contact = $content->add_widget()->add_class('b2c-widget')->set_title('Contact');
                $widget_contact = $content->add_div()->add_class('hotel-box-container');
                $this->generate_form('contact', $form_contact, $widget_contact);

                $additional_feature = $content->add_div()->custom_css("margin-left", "15px");
                $additional_feature->add_field()->add_control('', 'checkbox')->add_class('b2c-hotel-guest-ck')
                        ->set_label("I'm not a guest. I'm making this booking for someone else")
                        ->set_name('same_with_contact')->set_label_wrap(true);

                $div_guest = $content->add_div()->add_class('hidden b2c-hotel-guest');
//                $widget_passenger = $div_guest->add_widget()->add_class('b2c-widget')->set_title('Guest');
                $widget_passenger = $div_guest->add_div()->add_class('hotel-box-container');
                $this->generate_form('passengers', $form_adult, $widget_passenger);

                // if($this->child > 0){
                //     $this->generate_form_child();
                // }
                
                $container_special_request = $wrapper->add_div()
                        ->add_class('hotel-box-container');
                
                $container_special_request->add_div()
                        ->add(clang::__("Special Request"))
                        ->add_class('hotel-title-label');
                $container_special_request->add_div()
                        ->add_control('special_request', 'textarea')
                        ->add_class('hotel-textarea');
                $container_special_request->add_div()
                        ->add(clang::__("For special requests can be confirmed back to the hotel concerned. This is because policies and facilities of each hotel is different."));
            }

            $html->appendln(parent::html($indent));
            return $html->text();
        }

        public function js($indent = 0) {
            $js = new CStringBuilder();

            $js->appendln("
                    //set guest to not required first time
                    jQuery('.b2c-hotel-guest .hotel-box-container .form-group input').removeClass('validate[required]');

                    jQuery('.b2c-hotel-guest-ck').click(function(){
                    var checked = jQuery(this).is(':checked');
                    if (checked) {
                        jQuery('.b2c-hotel-guest').removeClass('hidden');
                        jQuery('.b2c-hotel-guest .hotel-box-container .form-group input').addClass('validate[required]');
                    }
                    else {
                        jQuery('.b2c-hotel-guest').addClass('hidden');
                        jQuery('.b2c-hotel-guest .hotel-box-container .form-group input').removeClass('validate[required]');
                    }
                });");
            
            $js->appendln(parent::js($indent));
            return $js->text();
        }

    }
    