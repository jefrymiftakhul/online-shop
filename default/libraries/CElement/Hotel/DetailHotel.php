<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of Search
     *
     * @author Ittron-18
     */
    class CElement_Hotel_DetailHotel extends CElement {

        protected $data_room;
        protected $data_form;
        protected $atoken;
        protected $hotel_code;
        protected $max_night;
        protected $max_room;
        protected $request_args;
        protected $promo_list;
        protected $hotel_description;
        protected $is_promo;
        protected $promo_type;
        protected $total_promo;

        public function __construct($id = "") {
            parent::__construct($id);
            $this->data_form = '';
            $this->atoken = '';
            $this->hotel_code = '';
            $this->data_room = array();
            $this->max_night = 9;
            $this->max_room = 5;
            $this->request_args = array();
            $this->promo_list = array();
            $this->hotel_description = '';
            $this->is_promo = 'false';
            $this->promo_type = '';
            $this->total_promo = 0;
        }

        public static function factory($id) {
            return new CElement_Hotel_DetailHotel($id);
        }

        public function set_data_room($data_room) {
            $this->data_room = $data_room;
            return $this;
        }

        public function set_data_form($data_form) {
            $this->data_form = $data_form;
            return $this;
        }

        public function set_atoken($atoken) {
            $this->atoken = $atoken;
            return $this;
        }

        public function set_promo_list($promo) {
            $this->promo_list = $promo;
            return $this;
        }

        public function set_hotel_code($hotel_code) {
            $this->hotel_code = $hotel_code;
            return $this;
        }

        public function set_request_args($request_args) {
            $this->request_args = $request_args;
            return $this;
        }

        public function set_is_promo($is_promo) {
            $this->is_promo = $is_promo;
            return $this;
        }

        public function set_promo_type($promo_type) {
            $this->promo_type = $promo_type;
            return $this;
        }

        public function set_total_promo($total_promo) {
            $this->total_promo = $total_promo;
            return $this;
        }

        public function generate_top_description() {
            $hotel_name = carr::get($this->data_room, 'hotel_name', array());
            $hotel_address = carr::get($this->data_room, 'address', array());
            $hotel_address = carr::get($hotel_address, '0');
            $hotel_images = carr::get($this->data_room, 'images', array());
            $rating = carr::get($this->data_room, 'rating');
            $rating = str_replace("*", '', $rating);

            $hotel_description = carr::get($this->data_room, 'description');
            if(strlen($hotel_description) > 0){
                $leng_desc = strlen($hotel_description) - 1;
                if($hotel_description[0] == '"'){
                    $hotel_description = substr($hotel_description, 1, $leng_desc);
                    $leng_desc--;
                }
                if($hotel_description[$leng_desc] == '"'){
                    $hotel_description = substr($hotel_description, 0, $leng_desc - 1);
                }
            }
            $this->hotel_description = $hotel_description;

            $start_from = carr::get($this->data_room, 'start_from');
            $start_from = str_replace(".000", "", $start_from);
            $start_from = ctransform::thousand_separator($start_from, 0);

            $reviews = carr::get($this->data_room, 'reviews', array());
            $total_review = count($reviews);
            
            
            $container = $this->add_div()
                    ->add_class('row');
            $container_image = $container->add_div()
                    ->add_class('col-md-12')
                    ->add_class('hotel-container-image');
            $container_title = $container_image->add_div()
                    ->add_class('hotel-container-title');
            $title = $container_title->add_div()
                    ->add($hotel_name)
                    ->add_class('hotel-ct-title');
            $title->add_div()
                    ->add_class('ico-'.$rating.'-star');
            
            $container_top = $container_image->add_div()
                    ->add_class("hotel-container-table");
            $container_top_desc = $container_top->add_div()
                    ->add_class("col-md-3")
                    ->add_class("hotel-side-image");
            
            //address
            $container_top_desc->add_div()
                    ->add($hotel_address)
                    ->add_class("hotel-si-container") // si = side-image
                    ->add_class("hotel-ct-city");
            
            //Show map
            $container_top_desc->add_div()
                    ->add_class("hotel-detail-show-map")
                    ->add(clang::__("(Show Map)"))
                    ->add_class("hotel-ct-map"); //ct = container

            //Hotel Facility
            $container_top_desc->add_div()
                    ->add(clang::__("Hotel Facililty"))
                    ->add_class("hotel-ct-facility")
                    ->add_class("hotel-ct-button");
            //Hotel Info
            $container_top_desc->add_div()
                    ->add(clang::__("Hotel Info"))
                    ->add_class("hotel-ct-info")
                    ->add_class("hotel-ct-button");
            //Interest Place
            $container_top_desc->add_div()
                    ->add(clang::__("Interest Place"))
                    ->add_class("hotel-ct-interest")
                    ->add_class("hotel-ct-button");

            //short description

            $desc = $hotel_description;
            if(strlen($desc) > 0){
                $desc = "\"".substr($desc, 0, 175)."...\""."<span class='hotel-ct-more-description hotel-hover'> more</span>";
            }
            $container_top_desc->add_div()
                    ->add($desc)
                    ->add_class("hotel-ct-short-description");          
            
            $container_desc_bottom = $container_top_desc->add_div()
                    ->add_class("hotel-desc-bottom");

            //price start from
            $container_desc_bottom->add_div()
                    ->add(clang::__("Price Start from"))
                    ->add_class("hotel-ct-price-sf");
            
            //price IDR
            $container_desc_bottom->add_div()
                    ->add("IDR ".$start_from)
                    ->add_class("hotel-ct-price-idr");
            
            //btn book now
            $container_desc_bottom->add_div()
                    ->add_class("hotel-ct-btn-book")
                    ->add(clang::__("Book Now"));
            

            $container_image_top = $container_top->add_div()
                    ->add_class("col-md-9")
                    ->add_class("hotel-cib");
            
            $container_image_big = $container_image_top->add_div()
                    ->add_class('hotel-cib-select');
            
            if(count($hotel_images) > 0){
                $image_bigger = str_replace("/small/", "/bigger/", $hotel_images['0']['url']);
                
                $check_image = $this->check_big_image($image_bigger);
                if($check_image == 'false'){
                    $image_bigger = str_replace("/bigger/", '/', $image_bigger);
                }

                $image_object = CFactory::create_img();
                $image_object = $image_object->set_src($image_bigger);
                
                $container_image_big->add($image_object);
            }
            else {
                $image_object = CFactory::create_img();
                $image_object = $image_object->set_src(curl::base()."cresenity/noimage");
                $container_image_big->add($image_object);
            }

            //mini review
            if($total_review > 0){
                $total_avg = 0;
                foreach($reviews as $r_key => $r_val){
                    $r_rating = carr::get($r_val, 'average_value');
                    $r_rating = number_format((float)$r_rating, 1, '.', '');
                    $total_avg += $r_rating;
                }
                $total_avg = $total_avg / $total_review;
                $total_avg = number_format((float)$total_avg, 1, '.', '');

                $first_review = carr::get($reviews, '0');
                $r_name = carr::get($first_review, 'user_review');
                $r_rating = carr::get($first_review, 'average_value');
                $r_rating = number_format((float)$r_rating, 1, '.', '');
                $r_date = carr::get($first_review, 'rate_date');
                $r_date = Date("d F Y", strtotime($r_date));

                $r_negative = carr::get($first_review, 'negative');
                $r_positive = carr::get($first_review, 'positive');
                $r_comment = carr::get($first_review, 'comment');

                if(strlen($r_name) == 0){
                    $r_name = clang::__("Anonymous");
                }

                $comment = '';
                if(strlen($r_positive) > 0){
                    $comment = $r_positive;
                }
                else {
                    $comment = $r_comment;
                }
                $comment = substr($comment, 0, 40);
                $comment = "\"".$comment."...\"";

                $container_image_big->add_div()
                    ->add_class("hotel-total-review")
                    ->add($total_avg)
                    ->add("<div>".$total_review." ".clang::__(" reviews")."</div>");

                $container_image_big->add_div()
                    ->add_class("hotel-comment-review")
                    ->add($comment)
                    ->add("<br><br><span>".$r_name."<br>".$r_date."</span>");
            }

            $container_image_small_row = $container_image->add_div()
                    ->add_class("row");
            $container_image_small = $container_image_small_row->add_div()
                    ->add_class('hotel-cis-container col-md-12');

            foreach($hotel_images as $key => $value){
                $image_bigger = str_replace("/small/", "/bigger/", $value['url']);
                
                $check_image = $this->check_big_image($image_bigger);
                
                if($check_image == 'false'){
                    $image_bigger = str_replace("/bigger/", '/', $image_bigger);
                }

                $image_small = CFactory::create_img();
                $image_small = $image_small->set_src($image_bigger);
                $container_image_small->add_div()
                        ->add_class("hotel-cis-small")
                        ->add($image_small);
            }

            $btn_prev = $container_image_small->add_div()
                    ->add_class('hotel-cis-prev');
            $btn_prev->add_div()
                    ->add_class("hotel-cis-prev-btn");

            $btn_next = $container_image_small->add_div()
                    ->add_class('hotel-cis-next');
            $btn_next->add_div()
                    ->add_class("hotel-cis-next-btn");
        }

        public function check_big_image($image){
            $data = 'true';
            $ch = curl_init($image);
            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_exec($ch);
            $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if($retcode == 404){
                $data = 'false';
            }
            curl_close($ch);
            return $data;
        }
        

        public function generate_room() {
            $request = $_GET;
            $tr_get = carr::get($request, 'tr');
            $this->add_div('data_form')
                    ->add_class('hotel-form')
                    ->set_attr('hf', $this->data_form)
                    ->set_attr('atoken', $this->atoken)
                    ->set_attr('hc', $this->hotel_code);

            $container = $this->add_div()
                    ->add_class('row');

            $container_detail = $container->add_div()
                    ->add_class('col-md-12');

            $arr_room = carr::get($this->data_room, 'room_category', array());

            if(count($arr_room) > 0){
                $room_detail_all = $container_detail->add_div()
                        ->add_class('row');

                $room_detail = $room_detail_all->add_class('hotel-room-detail');

                $room_detail->add_div()
                        ->add(clang::__("Room Type"))
                        ->add_class('col-sm-4 hotel-room-type');

                $room_detail->add_div()
                        ->add(clang::__("Guest Total"))
                        ->add_class('col-sm-2 hotel-room');

                $room_detail->add_div()
                        ->add(clang::__("Board"))
                        ->add_class('col-sm-2 hotel-room');
                
                $room_detail->add_div()
                        ->add(clang::__("Room"))
                        ->add_class('col-sm-1 hotel-room');
                $room_detail->add_div()
                        ->add(clang::__("Price"))
                        ->add_class('col-sm-3 hotel-room-price');
                $data_room_select = array();

                $all_room_type = hotel::set_hidden_value($this->data_room);
                $room_detail->add_control('art', 'hidden')->set_value($all_room_type);

                $room_detail->add_control('hf', 'hidden')->set_value($this->data_form);
            }
            else {
                $container_detail->add_div()
                    ->add_class('row')
                    ->add_class("text-center hotel-text-orange mtop-30")
                    ->add(clang::__("Sorry, no room available"));
            }


            $i = 1;
            foreach ($arr_room as $key => $value) {
                $room_type = carr::get($value, 'room_type');
                $breakfast = carr::get($value, 'breakfast');
                $spe = carr::get($value, 'classification_code');
                if (count($room_type) < 1) {
                    // room not available
                }
                else {
                    foreach ($value['room_type'] as $room_type_k => $room_type_v) {
                        $price = $room_type_v['total_price'];
                        $price_ori = str_replace(".000", "", $price);
                        $price = ctransform::thousand_separator($price_ori, 0);

                        $row = $room_detail->add_div()
                                ->add_class('col-sm-12')
                                ->add_class('hotel-row-detail');

                        if($this->is_promo == 'true'){
                            $row->add_div()
                                ->add_class("hotel-promo")
                                ->custom_css("margin-left", '-16px');
                        }

                        $cont_room_type = $row->add_div()
                                ->add_class('col-sm-4')
                                ->add_class('hotel-row-hvr')
                                ->add_class('hotel-room-name');

                        
                        $cont_room = $cont_room_type->add_div()
                                ->add_class("hotel-room-name-more");

                        $cont_room->add_div()
                                ->add($room_type_v['room_type_name']);

                        $cont_room->add_div()
                                ->add_class('hotel-text-more-about')
                                ->set_attr('k', $key)
                                ->set_attr('rtk', $room_type_k)
                                ->add(clang::__("More about this room"))
                                ->add_div()
                                ->add_class('hotel-icon-more-about');
                        
                        if($spe == 'SPE'){
                            $cont_room_type->add_div()
                                    ->add_class("hotel-room-spe");
                        }

                        $occupation = $room_type_v['occupation']['max_adults'];
                        $occupation_chd = $room_type_v['occupation']['max_children'];
                        $row_max = $row->add_div()
                                ->add_class('hotel-row-hvr')
                                ->add_class('hotel-number-night')
                                ->add_class('col-sm-2');
                        $row_max->add_div()
                                ->add_class("hotel-".$occupation."-occupation");
                        $row_max->add_div()
                                ->add_class("hotel-text-black")
                                ->add("Max ".$occupation);

                        $row_board = $row->add_div()
                                ->add_class('hotel-row-hvr')
                                ->add_class('hotel-number-night')
                                ->add_class('col-sm-2');
                        $row_board->add_div()
                                ->add($breakfast)
                                ->add_class("hotel-text-black");
                        $row_board->add_div()
                                ->add("(".$room_type_v['board_name'].")")
                                ->add_class("hotel-text-black");

                        $arr_cancel_policies = carr::get($room_type_v, 'cancellation_policies');
                        $data_cp = '';
                        $num_i = 0;
                        foreach($arr_cancel_policies as $acp_k => $acp_v){
                            $num_i++;
                            $cp_amount = carr::get($acp_v, "amount");
                            $cp_date_from = carr::get($acp_v, "date_from");
                            $cp_time = carr::get($acp_v, "time");

                            $cp_time = implode(":", str_split($cp_time, 2));
                            $cp_date_from = Date("d F Y", strtotime($cp_date_from));
                            $cp_amount = ctransform::thousand_separator($cp_amount, 0);

                            $br_char = "";
                            if($num_i > 1){
                                $br_char = "<br><br>";
                            }

                            $data_cp .= $br_char.clang::__("In the event of cancellation after ".$cp_time." on ".$cp_date_from." the following charges will be applied: IDR. ").$cp_amount;
                        }

                        if(count($arr_cancel_policies) > 0){
                            $cp_text = $row_board->add_div()
                                    ->add(clang::__("Cancel Policy"))
                                    ->add_class("hotel-cancel-policy");
                            $cp_popup = $cp_text->add_div()
                                    ->add($data_cp)
                                    ->add_class("hotel-cancel-policy-popup");

                            $cp_popup->add_div()
                                    ->add_class("hotel-cp-popup")
                                    ->add_div();
                        }


                        $available_count = $room_type_v['available_count'];

                        $room_value = 0;
                        if ($key == 0) {
                            $room_value = $tr_get;
                        }

                        //if room > available, set value to max available
                        if($tr_get > $available_count){
                            $room_value = $available_count;
                        }

                        if($available_count > 3){
                            $max_room_select = $this->max_room;


                        }
                        else {
                            $max_room_select = $available_count;
                        }
                        $data_room_select = array();
                        for ($i = 0; $i <= $max_room_select; $i++) {
                            $data_room_select[$i] = $i;
                        }

                        $row->add_div()
                                ->add_class('hotel-number-room')
                                ->add_class('hotel-row-hvr')
                                ->add_class('col-sm-1')
                                ->add_div()
                                ->add_control('room-' . $key . "_" . $room_type_k, 'select')
                                ->add_class('hotel-select-room')
                                ->set_list($data_room_select)
                                ->set_value($room_value);

                        $room_price = $row->add_div()
                                ->add_class('col-sm-3')
                                ->add_class('hotel-row-hvr')
                                ->add_class('hotel-price');

                        // if(count($this->promo_list) > 0){
                        //     $room_price->add_div()
                        //             ->add_class('hotel-text-price-s')
                        //             ->add('IDR. '. $price);      
                         
                        //     $total_promo = hotel::total_promo($this->promo_list);

                        //     $promo_price = $price_ori - $total_promo;
                        //     $promo_price = ctransform::thousand_separator($promo_price, 0);
                        //     $room_price->add_div()
                        //             ->add_class('hotel-text-price')
                        //             ->add('IDR. ' . $promo_price);                   
                        // }

                        if($this->is_promo == 'true'){
                            $room_price->add_div()
                                    ->add_class('hotel-text-price-s')
                                    ->add('IDR. '. $price); 

                            if($this->promo_type == 'amount') {
                                $total_promo = $this->total_promo;
                            }
                            else if($this->promo_type == 'percent') {
                                $total_promo = round(($this->total_promo / 100) * $price_ori);
                            }
                            else {
                                $total_promo = 0;
                            }

                            $promo_price = $price_ori - $total_promo;
                            $promo_price = ctransform::thousand_separator($promo_price, 0);
                            $room_price->add_div()
                                ->add_class('hotel-text-price')
                                ->add('IDR. '. $promo_price);

                        }                        
                        else {
                            $room_price->add_div()
                                    ->add_class('hotel-text-price')
                                    ->add('IDR. ' . $price);
                        }

                        $session = CApiClientSession::instance('HT', null);
                        $cap_session_id = $session->get_session_id();    
                        
                        $url_submit = curl::base() . "hotel/reservationHotel?hc=" . $this->hotel_code . "&atoken=" . $this->atoken . "&token=".$cap_session_id."&rm=" . $key . "." . $room_type_k;


                        $action = $room_price->add_action()
                                ->set_label('Book Now')
                                ->add_class('btn-book')
                                ->set_submit(false);
                        
                        
                        $room_last = "<span class='hotel-room-green'>".clang::__("Limited room available!")."</span>";
                        if($available_count > 3){
                            // $room_last = "<span class='hotel-room-red'>".clang::__("Our last ".$available_count." Room")."</span>";
                            $room_last = "<span class='hotel-room-red'>".clang::__("Room available")."</span>";
                        }

                        $room_price->add_div()
                                ->add_class("hotel-room-last")
                                ->add($room_last);

                        
                        $action->add_listener('click')->add_handler('custom')
                                ->set_js("
                                        jQuery(this).parents('form').attr('action','" . $url_submit . "');
                                        var total_room = jQuery(this).parents('.hotel-row-detail').find('.hotel-select-room').val();
                                        console.log(total_room);
                                        if(total_room == 0){
                                            alert('Please Select Number of Room');
                                        }
                                        else {
                                            //send_total_room();
                                            $.app.show_loading('hotel');
                                            jQuery('.hotel-form-booking').submit();
                                        }");

                        $room_description = $row->add_div()
                                ->add_class("col-sm-12")
                                ->add_class("hotel-room-description");
                        $row_room_description = $room_description->add_div()
                                ->add_class("hotel-row-description")
                                ->add_class("row");

                        $des_max_adult = $row_room_description->add_div()
                                ->add_class("hotel-rd-label") //rd = room description
                                ->add_class("col-sm-4");
                        $des_max_adult->add_div()
                                ->add_class("hotel-".$occupation."-occupation");
                        $des_max_adult->add_div()
                                ->add(clang::__("&nbsp;&nbsp; Max. ").$occupation.clang::__(" adults"));

                        if($occupation_chd > 0){
                            $row_room_description = $room_description->add_div()
                                    ->add_class("hotel-row-description")
                                    ->add_class("row");
                            $des_max_child = $row_room_description->add_div()
                                    ->add_class("hotel-rd-label") //rd = room description
                                    ->add_class("col-sm-4");
                            $des_max_child->add_div()
                                    ->add_class("hotel-".$occupation_chd."-occupation");
                            $des_max_child->add_div()
                                    ->add(clang::__("&nbsp;&nbsp; Max. ").$occupation_chd.clang::__(" child (under 12 years old)"));
                        }
                        
                        $room_facilities = $room_type_v['facilities'];

                        //check include 
                        $fc_include = 0;
                        foreach($room_facilities as $rf_key => $rf_val){
                            if($rf_val['fee'] == 'N'){
                                $check_number = $rf_val['facility_name'];
                                //check value number or not 1 2
                                if(!is_numeric($check_number)){
                                    $fc_include++;
                                }
                            }
                        }
                        //check not include
                        $fc_n_include = 0;
                        foreach($room_facilities as $rf_key => $rf_val){
                            if($rf_val['fee'] == 'Y'){
                                //check value number or not 1 2
                                $check_number = $rf_val['facility_name'];
                                if(!is_numeric($check_number)){
                                    $fc_n_include++;                        
                                }

                            }
                        }

                        if($fc_include > 0){
                            //new row
                            $row_room_description = $room_description->add_div()
                                    ->add_class("hotel-row-description")                                
                                    ->add_class("hotel-text-include")
                                    ->add_class("row");

                            $t_included = $row_room_description->add_div()
                                    ->add_class("hotel-text-orange")
                                    ->add(clang::__("Included"));

                            //new row
                            $row_room_description = $room_description->add_div()
                                    ->add_class("hotel-row-description")
                                    ->add_class("row");

                            //include
                            foreach($room_facilities as $rf_key => $rf_val){
                                if($rf_val['fee'] == 'N'){
                                    $check_number = $rf_val['facility_name'];
                                    //check value number or not 1 2
                                    if(!is_numeric($check_number)){
                                        $v_included = $row_room_description->add_div()
                                                ->add_class("hotel-rd-included")
                                                ->add_class("col-sm-4");

                                        $v_included->add_div()
                                                ->add_class('hotel-ico-check-green');
                                        $v_included->add_div()
                                                ->add($rf_val['facility_name']);                           
                                    }
                                }
                            }
                        }

                        if($fc_n_include > 0){
                            //new row for not include
                            $row_room_description = $room_description->add_div()
                                    ->add_class("hotel-row-description")
                                    ->add_class("hotel-text-not-include")
                                    ->add_class("row");

                            $t_included = $row_room_description->add_div()
                                    ->add_class("hotel-text-orange")
                                    ->add_class("hotel-10-margin")
                                    ->add(clang::__("Not Included"));

                            //new row
                            $row_room_description = $room_description->add_div()
                                    ->add_class("hotel-row-description")
                                    ->add_class("row");

                            //not include
                            foreach($room_facilities as $rf_key => $rf_val){
                                if($rf_val['fee'] == 'Y'){

                                    //check value number or not 1 2
                                    $check_number = $rf_val['facility_name'];
                                    if(!is_numeric($check_number)){
                                        $v_included = $row_room_description->add_div()
                                                ->add_class("hotel-rd-included")
                                                ->add_class("col-sm-4");

                                        $v_included->add_div()
                                                ->add_class('hotel-ico-cross');
                                        $v_included->add_div()
                                                ->add($rf_val['facility_name']);                           
                                    }

                                }
                            }
                        }
                    } // end foreach room_type
                }
            } // end foreach room
        }

        public function generate_info() {
            $container = $this->add_div()
                    ->add_class('row');
            $container_info = $container->add_div()
                    ->add_class('col-md-12')
                    ->add_class('hotel-container-info');

            
            //row 1
            $container_info_item = $container_info->add_div()
                    ->add_class("hotel-ci-description")
                    ->add_class("hotel-ci-item");
            $container_info_title = $container_info_item->add_div()
                    ->add_class('hotel-ci-title on');
            $container_info_title->add_div()
                    ->add_class('hotel-ci-title-icon');
            $container_info_title->add_div()
                    ->add_class('hotel-ci-title-text')
                    ->add(clang::__("Hotel Description"));
            $container_info_content = $container_info_item->add_div()
                    ->add_class("hotel-ci-content")
                    ->add_class("row");

            
            //hotel description 
            $data_description = $this->hotel_description;
            if(strlen($data_description) > 0){
                $data_description = "\"".$data_description."\"";
            }

            $container_info_content_box = $container_info_content->add_div()
                    ->add_class("col-md-12");
            $container_info_content_box->add_div()
                    ->add($data_description);

            

            //row 2
            $container_info_item = $container_info->add_div()
                    ->add_class("hotel-ci-facility")
                    ->add_class("hotel-ci-item");
            $container_info_title = $container_info_item->add_div()
                    ->add_class('hotel-ci-title on');
            $container_info_title->add_div()
                    ->add_class('hotel-ci-title-icon');
            $container_info_title->add_div()
                    ->add_class('hotel-ci-title-text')
                    ->add(clang::__("Hotel Facility"));
            $container_info_content = $container_info_item->add_div()
                    ->add_class("hotel-ci-content")
                    ->add_class("row");

            
            //hotel facility 
            $arr_facility = carr::get($this->data_room, 'facilities_category', array());
            foreach($arr_facility as $af_k => $af_v){
                $facilities = array();
                $arr_type = array('location', 'distance', 'points_of_interest');
                if(!in_array($af_v['type'], $arr_type)){
                    $facilities = $af_v['facilities'];
                    
                    $container_info_content->add_div()
                        ->add_class("col-sm-12 hotel-cic-title")
                        // ->custom_css("margin-top", "10px")
                        // ->custom_css("color", "#1B80C3")
                        // ->custom_css("font-weight", "bold")
                        // ->custom_css("padding-left", "43px")
                        ->add($af_v["name"]);
                }


                foreach($facilities as $key => $value){
                    $container_info_content_box = $container_info_content->add_div()
                            ->add_class('col-sm-4');
                    $container_info_content_box->add_div()
                            ->add_class("hotel-cicb-icon");
                    $container_info_content_box->add_div()
                            ->add(clang::__($value['name']))
                            ->add_class("hotel-cicb-text");
                }
                
            }
            
            //row 3
            $container_info_item = $container_info->add_div()
                    ->add_class("hotel-ci-info")
                    ->add_class("hotel-ci-item");
            $container_info_title = $container_info_item->add_div()
                    ->add_class('hotel-ci-title on');
            $container_info_title->add_div()
                    ->add_class("hotel-ci-title-icon");
            $container_info_title->add_div()
                    // ->add(clang::__("Hotel Info   <span class='hotel-detail-show-map'>(Show Map)</span>"))
                    ->add(clang::__("Hotel Info"))
                    ->add_class("hotel-ci-title-text");




            $country = carr::get($this->data_room, 'country');
            $city = carr::get($this->data_room, 'city');
            $check_in_min = carr::get($this->data_room, 'check_in_min');
            $check_out_max = carr::get($this->data_room, 'check_out_max');
            $check_in_min = Date("H:m", strtotime($check_in_min));
            $check_out_max = Date("H:m", strtotime($check_out_max));

            $arr_content = array(
                clang::__("Country") => $country,
                clang::__("City") => $city,
                clang::__("Check in") => $check_in_min,
                clang::__("Check out") => $check_out_max
            );

            $container_info_content = $container_info_item->add_div()
                    ->add_class("hotel-ci-content")
                    ->add_class("row");
                    
            foreach($arr_content as $arr_key => $arr_val){
                if(strlen($arr_val) > 0){
                    $container_info_content_box = $container_info_content->add_div()
                            ->add_class('col-xs-6 col-md-4');
                    $container_info_content_box->add_div()
                            ->add($arr_key)
                            ->add_class("hotel-cicb-text");
                    $container_info_content_box = $container_info_content->add_div()
                            ->add_class('col-xs-6 col-md-8');
                    $container_info_content_box->add_div()
                            ->add($arr_val)
                            ->add_class("hotel-cicb-text");
                }
            }
            //hotel info
            foreach($arr_facility as $af_k => $af_v){
                $facilities = array();
                if($af_v['type'] == 'location'){
                    //get hotel info detail bd = building
                    foreach($af_v['facilities'] as $bd_k => $bd_v){
                        $name = carr::get($bd_v, 'name');
                        $number = carr::get($bd_v, 'number');
                        if(strlen($number) > 0){
                            $container_info_content_box = $container_info_content->add_div()
                                ->add_class('col-xs-6 col-md-4');
                            $container_info_content_box->add_div()
                                    ->add($name)
                                    ->add_class("hotel-cicb-text");
                            $container_info_content_box = $container_info_content->add_div()
                                    ->add_class('col-xs-6 col-md-8');
                            $container_info_content_box->add_div()
                                    ->add($number)
                                    ->add_class("hotel-cicb-text");
                        }
                    }
                }
            }   
           


            //row 4
            $container_info_item = $container_info->add_div()
                    ->add_class("hotel-ci-interest")
                    ->add_class("hotel-ci-item");
            $container_info_title = $container_info_item->add_div()
                    ->add_class('hotel-ci-title on');
            $container_info_title->add_div()
                    ->add_class("hotel-ci-title-icon");
            $container_info_title->add_div()
                    ->add(clang::__("Places of Interest Around the Hotel"))
                    ->add_class("hotel-ci-title-text");

            $container_info_content = $container_info_item->add_div()
                    ->add_class("hotel-ci-content")
                    ->add_class("row");

            foreach($arr_facility as $af_k => $af_v){
                $facilities = array();
                if($af_v['type'] == 'points_of_interest'){
                    //get hotel info detail bd = building
                    foreach($af_v['facilities'] as $bd_k => $bd_v){
                        $name = carr::get($bd_v, 'name', ' ');
                        $dist = carr::get($bd_v, 'distance', ' ');

                        if($dist < 1000){
                            $dist = $dist." m";
                        }
                        else if($dist >= 1000){
                            $dist = (float)$dist / 1000;
                            $dist = $dist." km";
                        }

                        $container_info_content_box = $container_info_content->add_div()
                            ->add_class('col-xs-6 col-md-4');
                        $container_info_content_box->add_div()
                                ->add($name)
                                ->add_class("hotel-cicb-text");
                        $container_info_content_box = $container_info_content->add_div()
                                ->add_class('col-xs-6 col-md-8');
                        $container_info_content_box->add_div()
                                ->add($dist)
                                ->add_class("hotel-cicb-text");
                    }
                }
            }

            //row 5
            $container_info_item = $container_info->add_div()
                    ->add_class("hotel-ci-interest")
                    ->add_class("hotel-ci-item");
            $container_info_title = $container_info_item->add_div()
                    ->add_class('hotel-ci-title on');
            $container_info_title->add_div()
                    ->add_class("hotel-ci-title-icon");
            $container_info_title->add_div()
                    ->add(clang::__("Top Nearby Location"))
                    ->add_class("hotel-ci-title-text");

             //hotel distance airport
            $arr_distance = carr::get($this->data_room, 'distances', array());
            $arr_airport = carr::get($arr_distance, 'airports', '');
            if(count($arr_airport) > 0){
                $container_info_content = $container_info_item->add_div()
                        ->add_class("hotel-ci-content")
                        ->add_class("row");
            }

            foreach($arr_distance as $arr_key => $arr_value){
                foreach($arr_value as $ad_key => $ad_value){
                    $container_info_content_box = $container_info_content->add_div()
                            ->add_class('col-xs-6 col-sm-4');
                    $container_info_content_box->add_div()
                            ->add(clang::__("Distance to ").clang::__($arr_key))
                            ->add_class("hotel-cicb-text")
                            ->add_class("hotel-text-blue");
                    $container_info_content_box = $container_info_content->add_div()
                            ->add_class('col-xs-4 col-sm-4');
                    $content_box = $container_info_content_box->add_div()
                            ->add_class("hotel-cicb-text")
                            ->add_class("hotel-inline-content");
                    $content_box->add_div()
                            ->add_class("hotel-div-round");
                    $content_box->add_div()
                            ->add($ad_value['name']);
                    $container_info_content_box = $container_info_content->add_div()
                            ->add_class('col-xs-2 col-sm-4');
                    $container_info_content_box->add_div()
                            ->add("(".$ad_value['distance_km']." km)")
                            ->add_class("hotel-cicb-text");
                }
            }

            $container_info_content = $container_info_item->add_div()
                                ->add_class("hotel-ci-content")
                                ->add_class("row");

            foreach($arr_facility as $af_k => $af_v){
                $facilities = array();
                if($af_v['type'] == 'distance'){
                    //get hotel info detail bd = building
                    $i = 0;
                    foreach($af_v['facilities'] as $bd_k => $bd_v){
                        $name = carr::get($bd_v, 'name', ' ');
                        $dist = carr::get($bd_v, 'distance', ' ');

                        if($dist < 1000){
                            $dist = $dist." m";
                        }
                        else if($dist >= 1000){
                            $dist = (float)$dist / 1000;
                            $dist = $dist." km";
                        }

                        $place_of = '&nbsp;';
                        if($i == 0){
                            $place_of = clang::__("Place of Attraction");
                        }
                        $i++;
                        
                        $container_info_content_row = $container_info_content->add_div()
                                ->add_class("hotel-ci-content")
                                ->add_class("hotel-ci-padding")
                                ->add_class("row");

                        $container_info_content_box = $container_info_content_row->add_div()
                            ->add_class('col-xs-12 col-sm-4');
                        $container_info_content_box->add_div()
                                ->add($place_of)
                                // ->add_class("hotel-cicb-text")
                                ->add_class("hotel-text-blue");
                                
                        $container_info_content_box = $container_info_content_row->add_div()
                            ->add_class('col-xs-6 col-sm-4');
                        // $container_info_content_box->add('tes22');
                        $content_box = $container_info_content_box->add_div();
                        //         ->add_class("hotel-cicb-text")
                        //         ->add_class("hotel-inline-content");
                        $content_box->add_div()
                                ->add_class("hotel-div-round");
                        $content_box->add($name);

                        $container_info_content_box = $container_info_content_row->add_div()
                                ->add_class('col-xs-6 col-sm-4');
                        $container_info_content_box->add_div()
                                ->add("(".$dist.")")
                                ->add_class("hotel-cicb-text");
                    }
                }
            }   


            
        }

        function generate_reviews(){
            $reviews = carr::get($this->data_room, 'reviews', array());
            $total_review = count($reviews);
            // $this->add_div()->add($reviews);

            //tes reviews
            // $arrrr = array(
            //     "negative" => 'aaaaaaaaaaaaaaaaadfasfa'
            // );
            // $arrrr2 = array(
            //     "negative" => 'bbbbbbbbbbbbbbbbbbbbbbbbbd'
            // );
            // $arrrr3 = array(
            //     "negative" => 'ccccccccccccccccccccccccc'
            // );
            // $arrrr4 = array(
            //     "negative" => 'dddddddddddddd'
            // );
            // $arrrr5 = array(
            //     "negative" => 'eeeeeeeeeeeeeee'
            // );
            // $arrrr6 = array(
            //     "negative" => 'ffffffffffff'
            // );
            // $arrrr7 = array(
            //     "negative" => 'ggggggggggggg'
            // );
            // $arrrr8 = array(
            //     "negative" => 'hhhhhhhhhhhh'
            // );
            // $arrrr9 = array(
            //     "negative" => 'iiiiiiiiiiiii'
            // );
            // $arrrr10 = array(
            //     "negative" => 'jjjjjjjjjj'
            // );
            // $arrrr11 = array(
            //     "negative" => 'kkkkkkkkkkk'
            // );
            // $arrrr12 = array(
            //     "negative" => 'lllllllll'
            // );

            // $reviews = array(
            //     $arrrr, $arrrr2, $arrrr3, $arrrr4, $arrrr5, $arrrr6, $arrrr7, $arrrr8, $arrrr9, $arrrr10, $arrrr11, $arrrr12
            // );

            $container = $this->add_div()
                    ->add_class("hotel-detail-reviews")
                    ->add_class('row');

            $all_reviews = $container->add_div()
                    ->add(clang::__("All Reviews"))
                    ->add_class("hotel-text-review");

            $sort_by = $all_reviews->add_div()
                    ->add_class("hotel-review-sort");

            $sort_cont = $sort_by->add_div()
                    ->add("<span>".clang::__("Sort by")."</span>");

            // $sort_cont->add("<select name='hotel-ctr-sort-review' id='hotel-ctr-sort-review' class='hotel-ctr-sort-review'>
            //         <option value='Newest'>Newest</option>
            //         <option value='Older'>Older</option>
            //     </select>");

            $sort_cont->add_control("hotel-ctr-sort-review", "select")
                    ->add_class("hotel-ctr-sort-review")
                    ->set_list(array("Newest", "Older"));

            if($total_review == 0){
                $box = $container->add_div()
                        ->add_class("hotel-box-container");
                $box_container = $box->add_div()
                        ->add_class("row");
                $box_container->add_div()
                        ->add_class("col-md-12 txt-center")
                        ->add("Reviews Not Found");
            }

            foreach($reviews as $r_key => $r_val){
                $r_name = carr::get($r_val, 'user_review');
                $r_rating = carr::get($r_val, 'average_value');
                $r_rating = number_format((float)$r_rating, 1, '.', '');
                $r_negative = carr::get($r_val, 'negative');
                $r_positive = carr::get($r_val, 'positive');
                $r_comment = carr::get($r_val, 'comment');
                $r_date = carr::get($r_val, 'rate_date');
                $r_date = Date("d F Y h:i:s", strtotime($r_date));

                if(strlen($r_name) == 0){
                    $r_name = clang::__("Anonymous");
                }

                $box = $container->add_div()
                        ->add_class("hotel-review-sorting")
                        ->add_class("hotel-box-container");
                $box_container = $box->add_div()
                        ->add_class("row");
                $box_container_1 = $box_container->add_div()
                        ->add_class("col-md-2")
                        ->add($r_name."<br>")
                        ->add_class("hotel-text-blue hotel-text-bold")
                        ->add("<span class='hotel-text-black'>".$r_date."</span>");
                $box_container_2 = $box_container->add_div()
                        ->add_class("col-md-1");
                $box_container_2->add_div()
                        ->add_class("hotel-rating-review")
                        ->add($r_rating);
                $box_container_3 = $box_container->add_div()
                        ->add_class("col-md-9");

                if(strlen($r_negative) > 0){
                    $container_review = $box_container_3->add_div()
                            ->add_class("hotel-ct-review");
                    $container_review->add_div()
                            ->add_class("hotel-ico-minus");
                    $container_review->add_div()
                            ->add_class("hotel-review-list")
                            ->add($r_negative);
                }

                if(strlen($r_positive) > 0){
                    $container_review = $box_container_3->add_div()
                            ->add_class("hotel-ct-review");;
                    $container_review->add_div()
                            ->add_class("hotel-ico-plus");
                    $container_review->add_div()
                            ->add_class("hotel-review-list")
                            ->add($r_positive);
                }

                if(strlen($r_comment) > 0){
                    $container_review = $box_container_3->add_div()
                            ->add_class("hotel-ct-review");;
                    $container_review->add_div()
                            ->add_class("hotel-ico-check");
                    $container_review->add_div()
                            ->add_class("hotel-review-list")
                            ->add($r_comment);
                }

            }

            $btn_load_more = $container->add_div()
                ->add_class("row");
            $btn_load_more->add_div()
                ->add("<button style='width:100%' type='button' class='btn btn-book'>".clang::__("Load More")."</button>")
                ->add_class("hotel-btn-more-review");


        }

        public function modal_map() {
            $html = new CStringBuilder();
            //script google map
            $html->appendln('<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script> ');
            $html->appendln('
                <div id="modal_map_hotel" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Map</h4>
                            </div>');
            $html->appendln('
                            <div class="modal-body">
                                <div class="row hotel-map-content">');
            $html->appendln('
                                </div>
                            </div>');
            $html->appendln('
                        </div>
                    </div>
                </div>');

            return $html->text();
        }

        public function popup_info(){
            $db = CDatabase::instance();
            $dc_hotel_code = security::decrypt($this->hotel_code);

            $ip_address = crequest::remote_address();
            $dc_atoken = security::decrypt($this->atoken);

            $q = "SELECT COUNT(hotel_code)
                FROM log_view_hotel
                WHERE hotel_code = ".$db->escape($dc_hotel_code)." AND status > 0 
                AND ip_address != ".$db->escape($ip_address)." 
                AND atoken != ".$db->escape($dc_atoken)." 
                AND created > DATE_SUB(NOW(), INTERVAL 3 MINUTE)";
            $hotel_view = cdbutils::get_value($q);
            $hotel_view = $hotel_view - 1; // - this log

            if($hotel_view > 0){
                $box = $this->add_div()
                    ->add_class("hotel-popup-box")
                    ->add_class('hotel-inline-content');
                $box->add_div()
                    ->add_class('hotel-1-occupation');
                $box->add_div()
                    ->add_class('hotel-popup-text')
                    ->add($hotel_view.clang::__(" users are looking at this property right now"));
                $box->add_div()
                    ->add('x')
                    ->add_class('hotel-popup-box-x');               
            }
        }

        public function html($indent = 0) {
            $html = new CStringBuilder();

            $html->appendln($this->generate_top_description());
            $html->appendln($this->generate_room());
            $html->appendln($this->generate_info());
            $html->appendln($this->generate_reviews());
            $html->appendln($this->modal_map());
            $html->appendln($this->popup_info());
            
            // add hidden fields
            foreach ($this->request_args as $k => $v) {
                $this->add_control($k, 'hidden')->set_value($v);
            }

            $html->set_indent($indent);
            $html->append(parent::html($indent));
            return $html->text();
        }

        public function js($indent = 0) {
            $js = CStringBuilder::factory();

            $js->appendln("
                var image_small = jQuery('.hotel-cis-small');
                var total_image = image_small.length;
                var cis_prev = 0;
                var cis_next = 0;
                var g_review_show = 5; //initialize first show review
                var g_total_review_show = 5; //limit review show
                image_show();");

            //when click small image
            $js->appendln("                               
                jQuery('.hotel-cis-small').click(function(){
                    image_small.removeClass('on');
                    var image_select = jQuery(this);
                    var image_url = image_select.children('img').attr('src');
                    var cont_image = jQuery('.hotel-cib-select img');
                    image_select.addClass('on');
                    cont_image.fadeOut('fast', function () {
                        cont_image.attr('src', image_url);
                        cont_image.fadeIn('fast');
                    });                   
                });");

            //when window resize
            $js->appendln("
                jQuery(window).resize(function() {
                    image_show();
                });");

            //prev small image
            $js->appendln("
                jQuery('.hotel-cis-prev-btn').click(function(){
                   if(cis_prev != 0){
                        cis_prev--;
                        cis_next--;                       
                        jQuery('.hotel-cis-small:eq('+cis_prev+')').removeClass('hide');
                        jQuery('.hotel-cis-small:eq('+cis_next+')').addClass('hide');
                    } 
                });");

            //next small image
            $js->appendln("
                jQuery('.hotel-cis-next-btn').click(function(){
                    if(cis_next != total_image){
                        jQuery('.hotel-cis-small:eq('+cis_prev+')').addClass('hide');
                        jQuery('.hotel-cis-small:eq('+cis_next+')').removeClass('hide');
                        cis_prev++;
                        cis_next++;
                    }                    
                });");

            //image count
            $js->appendln("
                function image_show(){                    
                    var size_container = jQuery('.hotel-cis-container').outerWidth(true);
                    var total_show = Math.floor(size_container / 85);
                    cis_next = parseInt(total_show);

                    if(total_image <= total_show){
                        jQuery('.hotel-cis-next-btn').addClass('hide');
                        jQuery('.hotel-cis-prev-btn').addClass('hide');
                        var small_image_show = total_show - total_image;
                        small_image(small_image_show);
                    }
                    else{
                        jQuery('.hotel-cis-next-btn').removeClass('hide');
                        jQuery('.hotel-cis-prev-btn').removeClass('hide');   
                    }

                    image_small.removeClass('hide');
                    for(var i = total_show;i < total_image;i++){
                        jQuery('.hotel-cis-small:eq('+i+')').addClass('hide');
                    }
                }");

            //show no image if image not full container
            $js->appendln("
                function small_image(x){
                    jQuery('.hotel-no-image').remove();
                    for(var i = 0;i<x;i++){
                        jQuery('.hotel-cis-container').append('<div class=\"hotel-cis-small hotel-no-image\"><img style=\"border:1px solid black;opacity:1\" src=\"".curl::base()."cresenity/noimage/75/75\" class=\"hotel-cis-small\"/></div>')
                    }
                }
            ");

            //select night
            $js->appendln("
                jQuery('.hotel-select-room').change(function(){
                    var room_choose = jQuery(this).val();
                    jQuery('.hotel-select-room').val(0);
                    jQuery('.hotel-select-room').removeClass('on');
                    jQuery(this).val(room_choose);
                    jQuery(this).addClass('on');
                });
            ");

            //show hide information
            $js->appendln("
                jQuery('.hotel-ci-title-icon').click(function(){
                    var info_content = jQuery(this).parent('.hotel-ci-title').parent('.hotel-ci-item') .children('.hotel-ci-content');
                    if(jQuery(this).parent('.hotel-ci-title').hasClass('on')){
                        jQuery(this).parent('.hotel-ci-title').removeClass('on');
                        info_content.slideUp('fast');
                    }
                    else{
                        jQuery(this).parent('.hotel-ci-title').addClass('on');
                        info_content.slideDown('fast');
                    }
                });");



            //var for slider detail hotel
            $js->appendln("
                var hdi_now = 0;
                var total_hdi = 0;");

            $arr_room = carr::get($this->data_room, 'room_category', array());

            $request = $_GET;
            $check_in = carr::get($request, 'ci');
            $check_out = carr::get($request, 'co');
            $adult = carr::get($request, 'a');
            $child = carr::get($request, 'c');
            $country_code = carr::get($request, 'cc');
            $city_code = carr::get($request, 'ctc');
            $ipp = carr::get($request, 'ipp');

            $room_select = json_encode($arr_room);


            //button detail hotel, generate modal
            $js->appendln("
                var data_room_p = '" . $room_select . "';
                jQuery('.hotel-room-description').css('display', 'none');
                jQuery('.hotel-text-more-about').click(function(){      
                    if(jQuery(this).hasClass('active')){
                        jQuery(this).removeClass('active');
                    }
                    else{
                        jQuery(this).addClass('active');
                    }

                    var desc_container = jQuery(this).parents('.hotel-row-detail').children('.hotel-room-description');
                    desc_container.slideToggle('fast');                    
                });");

            //slide detail hotel
            $js->appendln("
                reset_hotel_detail_image();
                function reset_hotel_detail_image(){
                    jQuery('.hotel-detail-image').addClass('hide');
                    jQuery('.hotel-detail-image:eq(0)').removeClass('hide');
                    hdi_now = 0;
                    total_hdi = jQuery('.hotel-detail-image').length;
                    jQuery('.hotel-detail-nav-page').html('1/'+total_hdi);
                }");

            //btn prev detail hotel
            $js->appendln("
                jQuery('.hotel-detail-nav-back').click(function() {
                    if(hdi_now > 0){
                        hdi_now--;
                        jQuery('.hotel-detail-image').addClass('hide');
                        jQuery('.hotel-detail-image:eq('+hdi_now+')').removeClass('hide');
                        jQuery('.hotel-detail-nav-page').html((hdi_now+1)+'/'+total_hdi);
                    }
                });
            ");

            //btn next detail hotel
            $js->appendln("
                jQuery('.hotel-detail-nav-next').click(function() {
                    if(hdi_now < (total_hdi -1)){
                        hdi_now++;
                        jQuery('.hotel-detail-image').addClass('hide');
                        jQuery('.hotel-detail-image:eq('+hdi_now+')').removeClass('hide');
                        jQuery('.hotel-detail-nav-page').html((hdi_now + 1)+'/'+total_hdi);
                    }
                });
            ");

            //click show map
            $js->appendln("
                jQuery('.hotel-detail-show-map').click(function(){
                    jQuery('#modal_map_hotel').modal('toggle');
                });
            ");

            $js->appendln("
                var map_opened = false;
                $('#modal_map_hotel').on('shown.bs.modal', function (e) {
                    if(map_opened == false){    
                        initialize_map();
                        map_opened = true;
                    }
                })");

            
            $lat = carr::get($this->data_room, 'latitude');
            $long = carr::get($this->data_room, 'longitude');

            $hotel_image = carr::get($this->data_room, 'images', array());
            $hotel_image_js = curl::base()."cresenity/noimage/150/150";
            if(count($hotel_image) > 0){
                $hotel_image_js = $hotel_image[0]['url'];
                $hotel_image_js = str_replace("/small/", "/", $hotel_image_js);
            }
            $hotel_name_js = carr::get($this->data_room, 'hotel_name');
            $hotel_rating = carr::get($this->data_room, 'rating');
            $hotel_rating_js = str_replace("*", "", $hotel_rating);
            // $hotel_description_js = carr::get($this->data_room, 'description', '');
            $hotel_desc = '';
            if(strlen($this->hotel_description) > 0){
                $hotel_desc = substr($this->hotel_description, 0, 150);
            }
            $hotel_desc = str_replace("'", "\'", $hotel_desc);
            $hotel_desc = str_replace('"', '\"', $hotel_desc);

            $start_from_js = carr::get($this->data_room, 'start_from');
            $start_from_js = ctransform::thousand_separator($start_from_js, 0);
            
            //create map
            $js->appendln("
                var lat = '".$lat."';
                var lng = '".$long."';
                function initialize_map() {
                    var latlng = new google.maps.LatLng(lat, lng);
                    var myOptions = {
                        zoom: 16,
                        center: latlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        mapTypeControl: true,
                        navigationControl: true,
                        scaleControl: true
                    };
                    var map = new google.maps.Map(jQuery('.hotel-map-content')[0],
                            myOptions);
                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        title: 'Hotel',
                        icon: '".curl::httpbase()."application/torsb2c/default/media/img/hotel-map-ico.png'
                    });
                    var contentString = '<div> \
                                            <div align=\'left\'>\
                                                <img height=\'180px\' width=\'250px\' src=\'".$hotel_image_js."\'/>\
                                            </div>\
                                            <div class=\'hotel-map-detail\'>\
                                               <div>".$hotel_name_js."</div> \
                                               <div class=\'ico-".$hotel_rating_js."-star\'></div><div class=\'hotel-map-price\'>IDR. ".$start_from_js."</div>\
                                            </div>\
                                            <div class=\'hotel-map-description\'> \
                                                ".$hotel_desc." \
                                            </div> \
                                        </div>';
                    var infowindow = new google.maps.InfoWindow({
                        content: contentString
                    });

                    google.maps.event.addListener(marker, 'mouseover', function() {
                      infowindow.open(map,marker);
                    });
                }                
                ");

            //click hotel description more top
            $js->appendln("
                jQuery('body').on('click', '.hotel-ct-more-description', function(e){
                    jQuery('html, body').animate({
                        scrollTop: jQuery('.hotel-ci-description').offset().top
                    }, 1000);
                });
            ");

            //click hotel facility top
            $js->appendln("
                jQuery('body').on('click', '.hotel-ct-facility', function(e){
                    jQuery('html, body').animate({
                        scrollTop: jQuery('.hotel-ci-facility').offset().top
                    }, 1000);
                });
            ");

            //click hotel information top
            $js->appendln("
                jQuery('body').on('click', '.hotel-ct-info', function(e){
                    jQuery('html, body').animate({
                        scrollTop: jQuery('.hotel-ci-info').offset().top
                    }, 1000);
                });
            ");

            //click hotel facility top
            $js->appendln("
                jQuery('body').on('click', '.hotel-ct-interest', function(e){
                    jQuery('html, body').animate({
                        scrollTop: jQuery('.hotel-ci-interest').offset().top
                    }, 1000);
                });
            ");

            //click hotel facility top
            $js->appendln("
                jQuery('body').on('click', '.hotel-ct-btn-book', function(e){
                    jQuery('html, body').animate({
                        scrollTop: jQuery('.hotel-room-detail').offset().top
                    }, 1000);
                });
            ");

            //click hotel comment review
            $js->appendln("
                jQuery('body').on('click', '.hotel-total-review', function(e){
                    jQuery('html, body').animate({
                        scrollTop: jQuery('.hotel-text-review').offset().top
                    }, 1000);
                });
            ");

            //click hotel comment review
            $js->appendln("
                jQuery('body').on('click', '.hotel-comment-review', function(e){
                    jQuery('html, body').animate({
                        scrollTop: jQuery('.hotel-text-review').offset().top
                    }, 1000);
                });
            ");

            //order review
            $js->appendln("
                jQuery('body').on('change', '.hotel-ctr-sort-review', function(){
                    g_review_show = g_total_review_show;
                    jQuery('.hotel-review-sorting').each(function() {
                        $(this).insertBefore('.hotel-review-sorting:first');
                    });
                    load_first_review();
                });
            ");

            //hide all review, only show 5
            $js->appendln("
                load_first_review();
                function load_first_review(){
                    jQuery('.hotel-review-sorting').css('display', 'none');
                    for(var i = 0;i< g_total_review_show ;i++){
                        jQuery('.hotel-review-sorting:eq('+i+')').css('display', 'block');
                    }
                    hide_load_more(g_total_review_show);
                }
            ");

            //click load more
            $js->appendln("
                jQuery('body').on('click', '.hotel-btn-more-review', function(){
                    var r_show = g_review_show + g_total_review_show;
                    for(var i = g_review_show;i < r_show;i++){
                        jQuery('.hotel-review-sorting:eq('+i+')').show('fast');
                    }
                    g_review_show = r_show; //update g_review_show
                    hide_load_more(r_show);
                });
            ");

            $js->appendln("
                function hide_load_more(showing){
                    var total_review = jQuery('.hotel-review-sorting').length;
                    if(total_review <= 5 || total_review <= showing){
                        jQuery('.hotel-btn-more-review').hide();
                    }
                    else {
                        jQuery('.hotel-btn-more-review').show();
                    }
                }
            ");

            $js->appendln("
                    jQuery('.hotel-popup-box').slideDown('slow').delay(20000).fadeOut('slow');
            ");

            $js->appendln("
                jQuery('body').on('click', '.hotel-popup-box-x', function(){
                    jQuery('.hotel-popup-box').hide();
                });
            ");

            $js->append(parent::js($indent));
            return $js->text();
        }

    }
    