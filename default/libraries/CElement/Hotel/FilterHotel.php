<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of Search
     *
     * @author Ittron-18
     */
    class CElement_Hotel_FilterHotel extends CElement {

        protected $url_location;
        protected $filter_facility;

        public function __construct($id = "") {
            parent::__construct($id);
            $this->url_location = curl::httpbase();
        }

        public static function factory($id) {
            return new CElement_Hotel_FilterHotel($id);
        }

        public function set_url($url) {
            $this->url_location = $url;
            return $this;
        }

        public function set_filter_facility($filter_facility) {
            $this->filter_facility = $filter_facility;
            return $this;
        }

        public function html($indent = 0) {
            $html = new CStringBuilder();
            $db = CDatabase::instance();
            $session = Session::instance();

            $html->set_indent($indent);
            $id = $this->id;

            //Filter hotel name
            $html->appendln("<div class='el-hotel-panel'>"
                    . "<div class='el-hotel-panel-heading'>"
                    . "<span class='el-hotel-panel-title'>" . clang::__("Hotel Name") . "</span>"
                    . "<span class='pull-right el-clickable on'><i class='glyphicon glyphicon-triangle-bottom'></i></span>"
                    . "</div>"
                    . "<div class='el-hotel-panel-body box-body el-hotel-search-name'>"
                    . "<input type='text' class='form-control el-fil-hotel-name' id=" . $id . "_fil_hotel_name>"
                    . "<div class='btn-search-hotel-name' id='btn_search_hotel_name'>Go</div>"
                    . "</div>"
                    . "</div>");

            //Filter hotel star header
            $html->appendln("<div class='el-hotel-panel'>"
                    . "<div class='el-hotel-panel-heading'>"
                    . "<span class='el-hotel-panel-title'>" . clang::__("Star") . "</span>"
                    . "<span class='pull-right el-clickable on'><i class='glyphicon glyphicon-triangle-bottom'></i></span>"
                    . "</div>"
                    . "<div class='el-hotel-panel-body box-body'>"
            );
            //star data
//            <div class="squaredTwo">
//                <input type="checkbox" value="None" id="squaredTwo" name="check" />
//                <label for="squaredTwo"></label>
//            </div>
            $html->appendln("<div class='el-fil-hotel-5-star custom-check-box'>"
                    . "<input type='checkbox' class='el-fil-hotel-star-check' value='5'>"
                    . "<label for='custom-checkbox'></label> "
                    . "<div class='ico-5-star'> </div><span class='pull-right'>0</span></div>");
            $html->appendln("<div class='el-fil-hotel-4-star custom-check-box'>"
                    . "<input type='checkbox' class='el-fil-hotel-star-check' value='4'>"
                    . "<label for='custom-checkbox'></label> "
                    . "<div class='ico-4-star'> </div><span class='pull-right'>0</span></div>");
            $html->appendln("<div class='el-fil-hotel-3-star custom-check-box'>"
                    . "<input type='checkbox'  class='el-fil-hotel-star-check' value='3'>"
                    . "<label for='custom-checkbox'></label> "
                    . "<div class='ico-3-star'> </div><span class='pull-right'>0</span></div>");
            $html->appendln("<div class='el-fil-hotel-2-star custom-check-box'>"
                    . "<input type='checkbox'  class='el-fil-hotel-star-check' value='2'>"
                    . "<label for='custom-checkbox'></label> "
                    . "<div class='ico-2-star'> </div><span class='pull-right'>0</span></div>");
            $html->appendln("<div class='el-fil-hotel-1-star custom-check-box'>"
                    . "<input type='checkbox'  class='el-fil-hotel-star-check' value='1'>"
                    . "<label for='custom-checkbox'></label> "
                    . "<div class='ico-1-star small'> </div><span class='pull-right'>0</span></div>");
            $html->appendln("<div class='el-fil-hotel-0-star custom-check-box'>"
                    . "<input type='checkbox'  class='el-fil-hotel-star-check' value='0'>"
                    . "<label for='custom-checkbox'></label> "
                    . "<div class='ico-0-star'>" . clang::__('Not Yet Rated') . "</div><span class='pull-right'>0</span></div>");
            //filter hotel star footer
            $html->appendln("</div>"
                    . "</div>");

//      //Filter hotel facility header
//      $html->appendln("<div class='el-hotel-panel'>"
//              . "<div class='el-hotel-panel-heading'>"
//              . "<span class='el-hotel-panel-title'>".clang::__("Facility")."</span>"
//              . "<span class='pull-right el-clickable on'><i class='glyphicon glyphicon-triangle-bottom'></i></span>"
//              . "</div>"
//              . "<div class='el-hotel-panel-body box-body'>"
//              );
//      //Facility data
//      $html->appendln("<div><input type='checkbox'> <span style='vertical-align:top'>".clang::__('Pool')."</span></div>");
//      $html->appendln("<div><input type='checkbox'> <span style='vertical-align:top'>".clang::__('Internet')."</span></div>");
//      $html->appendln("<div><input type='checkbox'> <span style='vertical-align:top'>".clang::__('Gym/Fitness')."</span></div>");
//      $html->appendln("<div><input type='checkbox'> <span style='vertical-align:top'>".clang::__('Parking Lot')."</span></div>");
//      $html->appendln("<div><input type='checkbox'> <span style='vertical-align:top'>".clang::__('Spa/Sauna')."</span></div>");
//      $html->appendln("<div><input type='checkbox'> <span style='vertical-align:top'>".clang::__('Meeting Facilities')."</span></div>");
//      $html->appendln("<div><input type='checkbox'> <span style='vertical-align:top'>".clang::__('Restaurant')."</span></div>");
//      $html->appendln("<div><input type='checkbox'> <span style='vertical-align:top'>".clang::__('No-Smoking Room')."</span></div>");
//      $html->appendln("<div><input type='checkbox'> <span style='vertical-align:top'>".clang::__('Smoking Room')."</span></div>");
//      $html->appendln("<div><input type='checkbox'> <span style='vertical-align:top'>".clang::__('Complimentary Airport Shuttle')."</span></div>");
//      $html->appendln("<div><input type='checkbox'> <span style='vertical-align:top'>".clang::__('Bring Pets Allowed')."</span></div>");
//      $html->appendln("<div><input type='checkbox'> <span style='vertical-align:top'>".clang::__('Golf Course')."</span></div>");
//      //Filter hotel facility footer
//      $html->appendln("</div>"
//              . "</div>");
            //Filter hotel average header
            $html->appendln("<div class='el-hotel-panel'>"
                    . "<div class='el-hotel-panel-heading'>"
                    . "<span class='el-hotel-panel-title'>" . clang::__("Average per Night") . "</span>"
                    . "<span class='pull-right el-clickable on'><i class='glyphicon glyphicon-triangle-bottom'></i></span>"
                    . "</div>"
                    . "<div class='el-hotel-panel-body box-body'>"
            );
            //average data
            $html->appendln("<div class='el-fil-hotel-price-one custom-check-box'>"
                    . "<input type='checkbox' class='el-fil-hotel-price-check' price='-~500000'>"
                    . "<label for='custom-checkbox'></label> "
                    . "<span>" . clang::__('< Rp 500.000,00') . "</span></div>");
            $html->appendln("<div class='el-fil-hotel-price-two custom-check-box'>"
                    . "<input type='checkbox' class='el-fil-hotel-price-check' price='500001~1000000'>"
                    . "<label for='custom-checkbox'></label> "
                    . "<span>" . clang::__('Rp 500.001,00 - Rp 1.000.000,00') . "</span></div>");
            $html->appendln("<div class='el-fil-hotel-price-three custom-check-box'>"
                    . "<input type='checkbox' class='el-fil-hotel-price-check' price='1000001~2000000'>"
                    . "<label for='custom-checkbox'></label> "
                    . "<span>" . clang::__('Rp 1.000.001,00 - Rp 2.000.000,00') . "</span></div>");
            $html->appendln("<div class='el-fil-hotel-price-four custom-check-box'>"
                    . "<input type='checkbox' class='el-fil-hotel-price-check' price='2000001~5000000'>"
                    . "<label for='custom-checkbox'></label> "
                    . "<span>" . clang::__('Rp 2.000.001,00 - Rp 5.000.000,00') . "</span></div>");
            $html->appendln("<div class='el-fil-hotel-price-five custom-check-box'>
                            <input type='checkbox' class='el-fil-hotel-price-check' price='5000000~-'> 
                            <label for='custom-checkbox'></label>
                            <span>" . clang::__('> Rp 5.000.000,00') . "</span>
                        </div>");

            //filter hotel average footer
            $html->appendln("</div>"
                    . "</div>");


            if($this->filter_facility == 1){
                //Filter hotel facility header
                $html->appendln("<div class='el-hotel-panel'>"
                        . "<div class='el-hotel-panel-heading'>"
                        . "<span class='el-hotel-panel-title'>" . clang::__("Facility") . "</span>"
                        . "<span class='pull-right el-clickable on'><i class='glyphicon glyphicon-triangle-bottom'></i></span>"
                        . "</div>"
                        . "<div class='el-hotel-panel-body box-body el-fil-hotel-facility'>"
                );

                //facility data
                // $html->appendln("<div class='custom-check-box'>"
                //         . "<input type='checkbox' class='el-fil-hotel-facility-check' price='-~500000'>"
                //         . "<label for='custom-checkbox'></label> "
                //         . "<span>" . clang::__('aaaaaaaaa') . "</span></div>");


                //filter facility average footer
                $html->appendln("</div>"
                        . "</div>");
            }


            $html->append(parent::html($indent));
            return $html->text();
        }

        public function js($indent = 0) {
            $js = CStringBuilder::factory();

            $js->appendln("
                jQuery('.el-clickable').on('click', function () {
                    var box = jQuery(this).parent('div').parent('div').find('.box-body');
                    var btn = jQuery(this).find('.glyphicon');
                    if (jQuery(this).hasClass('on')) {
                        //btn change
                        btn.removeClass('glyphicon-triangle-bottom');
                        btn.addClass('glyphicon-triangle-right');

                        //hide box
                        box.hide('fast');
                        jQuery(this).removeClass('on');
                    }
                    else {
                        //btn change
                        btn.removeClass('glyphicon-triangle-right');
                        btn.addClass('glyphicon-triangle-bottom');

                        //show box
                        box.show('fast');
                        jQuery(this).addClass('on');
                    }            
                });
              ");

            $js->append(parent::js($indent));

            return $js->text();
        }

    }
    