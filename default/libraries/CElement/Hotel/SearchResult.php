<?php

    class CElement_Hotel_SearchResult extends CElement {

        protected $hotel_list;
        protected $atoken;
        protected $promo_list;

        public function __construct($id = '') {
            parent::__construct($id);
            $this->hotel_list = array();
            $this->atoken = '';
            $this->promo_list = array();
        }

        public static function factory($id = '') {
            return new CElement_Hotel_SearchResult($id);
        }

        public function set_hotel_list($hotel_list) {
            $this->hotel_list = $hotel_list;
            return $this;
        }

        public function set_promo_list($promo) {
            $this->promo_list = $promo;
            return $this;
        }

        public function get_hotel_list() {
            return $this->hotel_list;
        }
        
        public function set_atoken($atoken) {
            $this->atoken = $atoken;
            return $this;
        }

        public function get_atoken() {
            return $this->atoken;
        }

        public function check_big_image($image){
            $data = 'true';
            $ch = curl_init($image);
            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_exec($ch);
            $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if($retcode == 404){
                $data = 'false';
            }
            curl_close($ch);
            return $data;
        }
        
        public function generate_hotel_list() {    
            $request = $_GET;            
            $city_code = carr::get($request, 'ctc');
            $country_code = carr::get($request, 'cc');
            $adult = carr::get($request, 'a');
            $child = carr::get($request, 'c');
            $ipp = carr::get($request, 'ipp');
            $tr = carr::get($request, 'tr');



            foreach ($this->hotel_list as $list_k => $list_v) {
                $html = new CStringBuilder();
                $hotel_code = carr::get($list_v, 'hotel_code');
                $hotel_code = security::encrypt($hotel_code);
                $hotel_name = carr::get($list_v, 'hotel_name');
                $arr_address = carr::get($list_v, 'address');
                
                //facilities category
                $arr_class = array();
                $arr_category = carr::get($list_v, 'facilities_category');
                // $arr_not_show = array('distance', 'location', 'points_of_interest');

                $address = '';
                if (isset($arr_address[0])) {
                    $address = $arr_address[0];
                }
                
                $country = carr::get($list_v, 'country');
                $city = carr::get($list_v, 'city');
                
                $rating = carr::get($list_v, 'rating', 0);
                $rating = str_replace("*", '', $rating);
                $rating = substr($rating, 0, 1);
                $price = str_replace(".000", "", carr::get($list_v, 'start_from'));

                //promo                
                // $total_promo = 0;
                // if(count($this->promo_list) > 0){
                //     $total_promo = hotel::total_promo($this->promo_list);
                //     $price = $price - $total_promo;
                // }

                $is_promo = 'false';
                $total_promo = 0;
                $already_promo = 'false';
                foreach($this->promo_list as $pl_k => $pl_v){
                    $fare_type = carr::get($pl_v, 'fare_type');
                    $fare_amount = carr::get($pl_v, 'fare_amount');
                    $p_city_code = carr::get($pl_v, 'city_code');
                    $p_hotel_star = carr::get($pl_v, 'hotel_star');

                    // check already promo
                    if($already_promo == 'false') {
                        //check promo rating == hotel rating, or promo for all room
                        if($p_hotel_star == $rating || strlen($p_hotel_star) == 0){

                            if($fare_type == 'amount'){
                                $total_promo += $fare_amount;
                            }

                            if($fare_type == 'percent'){
                                $total_promo += round(($fare_amount / 100) * $price);
                            }

                            $price = $price - $total_promo;
                            $is_promo = 'true';
                            $already_promo = 'true';
                        }
                    }
                }



                $price_ori = $price;
                $price = ctransform::thousand_separator($price, 0);
                $image = carr::get($list_v, 'images', array());
                $image = carr::get($image, '0', array());
                $image = carr::get($image, 'url', curl::base() . 'cresenity/noimage');
//                $image = str_replace("/small/", "/bigger/", $image);
                $image = str_replace("/small/", "/", $image);
                $check_in = carr::get($list_v, 'check_in');
                $check_out = carr::get($list_v, 'check_out');
                //check image big size found or not
//                $check_image = $this->check_big_image($image);
//                if($check_image == 'false'){
//                    $image = str_replace("/bigger/", "/medium/", $image);
//                }
                

                $classification_code = carr::get($list_v, 'classification_code');
                $classification_text = carr::get($list_v, 'classification_text');
                
                $room_str = '';
                $room_str_content = '';
                $limit_room = 2;
                $counter_room = 0;
                $room_category = carr::get($list_v, 'room_category', array());
                foreach ($room_category as $room_category_k => $room_category_v) {
                    $breakfast = carr::get($room_category_v, 'breakfast');
                    $room_type = carr::get($room_category_v, 'room_type', array());
                    foreach ($room_type as $room_type_k => $room_type_v) {
                        $room_type_name = carr::get($room_type_v, 'room_type_name');
                        $available_count = carr::get($room_type_v, 'available_count');
                        if($available_count > hotel::$total_room){
                            $available_count = '';
                        }
                        if ($counter_room <= $limit_room) {
                            $room_str_content .= "<tr>";
                                $room_str_content .= "<td>" .$room_type_name ."</td>";
                                $room_str_content .= "<td> ( Tersedia " .$available_count ." ) </td>";
                            $room_str_content .= "</tr>";
                        }
                        else {
                            break;
                        }
                        $counter_room += 1;
                    }
                }
                if (strlen($room_str_content) > 0) {
                    $room_str = "<table>";
                    $room_str .= $room_str_content;
                    $room_str .= "</table>";
                }
                
                $atoken = $this->atoken;
                
                $star = "<div class='ico-".$rating."-star'> </div>";
                
                /*
                 * hc = hotel_code
                 * hn = hotel_name
                 * c = city
                 * r = rating
                 * p = price
                 * i = image
                 */

                $ac_class = '';
                foreach($arr_category as $ac_key => $ac_val){
                    $ac_type = carr::get($ac_val, "type");
                    
                    // if(!in_array($ac_type, $arr_not_show)){
                    if($ac_type == 'facilities'){
                        $ac_facility = carr::get($ac_val, "facilities");
                        foreach($ac_facility as $acf_key => $acf_val){
                            $acf_class = str_replace(" ", "~", $acf_val['name']);
                            $ac_class .= $acf_class." ";

                            if(!in_array($acf_class, $arr_class)){
                                $arr_class[] = $acf_class;
                            }
                        }
                    }
                }
                $dc_hotel_code = security::decrypt($hotel_code);
                $html->appendln("<div class='el-hotel-grid'>"
                        . "<div class='el-hotel-result' fc='".$ac_class."' hcd='".$dc_hotel_code."' hc='" . $hotel_code . "' hn='" . $hotel_name . "' c='" . $city . "' r='" . $rating . "' p='" . $price_ori . "' i='" . $image . "' cl='".$classification_code."' id='" . $hotel_code . "'>
                    ");

                $html->appendln("<div class='el-hotel-container'>
                    <div class='row'>");

                // promo
                if($is_promo == 'true'){
                    $html->appendln("<div class='hotel-promo'></div>");
                }

                //left container
                $html->appendln("<div class='col-md-4 el-hotel-img-container'>
                            <img src='" . $image . "' class='img-responsive el-hotel-image'>
                        </div>");

                //middle container
                $html->appendln("<div class='col-md-5 el-hotel-hotel-container'>");
                    $html->appendln("<div class='row'>");
                        $html->appendln("<div class='col-md-12 el-hotel-name'>" . $hotel_name ."</div>");
                    $html->appendln("</div>");
                    $html->appendln("<div class='row'>");
                        $html->appendln("<div class='col-md-12 el-hotel-address'>" . $address ." "
                                .$city .(isset($country) ? ", " .$country : " ") ."</div>");
                    $html->appendln("</div>");
                    $html->appendln("<div class='row'>");
                        $html->appendln("<div class='col-md-12 el-hotel-star'>" . $star ."</div>");
                    $html->appendln("</div>");
                    
                    if (strlen($room_str) > 0) {
                        $html->appendln("<div class='row el-hotel-room-wrapper'>");
                            $html->appendln("<div class='col-md-12 el-hotel-room'>" . $room_str ."</div>");
                        $html->appendln("</div>");
                    }
                $html->appendln("</div>");

                //right container
                $html->appendln("<div class='col-md-3 el-hotel-right-container'>");
                    if ($classification_code == 'SPE') {
                        $html->appendln("<div class='special-offer-wrapper'>");
                        $html->appendln("<div class='special-offer'></div>");
                        $html->appendln("</div>");
                    }
                    else if ($classification_code == 'NRF') {
                        $html->appendln("<div class='no-refund-wrapper'>");
                        $html->appendln("<div class='no-refund'></div>");   
                        $html->appendln("</div>");
                    }
                    else {
                        $html->appendln("<div class='special-offer-wrapper'></div>");
                    }


                    $html->appendln("<div class='el-hotel-price'>
                                      " . clang::__('Start from') . "<br>
                                      <span class='idr'>IDR " . $price . "</span><br>
                                      <a href='" . curl::base() . "hotel/hotelDetail/?hc=" . $hotel_code . "&atoken=" . $atoken . "&ci=".$check_in."&co=".$check_out."&ctc=".$city_code."&cc=".$country_code."&a=".$adult."&c=".$child."&tr=".$tr."' target='_blank'>
                                        <div id='" . $hotel_code . "' class='text-center btn-view-hotel-detail'>" . clang::__('View Detail') . " <span class='glyphicon glyphicon-search' aria-hidden='true'></span></div>
                                      </a>
                                    </div>"); 
                $html->appendln("</div>");

                
                
                $this->hotel_list[$list_k]['html'] = $html->text();
                $this->hotel_list[$list_k]['arr_facility'] = $arr_class;
            }
            
        }

        public function html($indent = 0) {
            $html = new CStringBuilder();
            $db = CDatabase::instance();
            $session = Session::instance();

            $html->set_indent($indent);

            $html->appendln($this->generate_hotel_list());

            $html->append(parent::html($indent));
            return $html->text();
        }

        public function js($indent = 0) {
            $js = CStringBuilder::factory();

            $js->append(parent::js($indent));
            return $js->text();
        }

    }
    