<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of ReservationDetail
     *
     * @author Ittron-18
     */
    class CElement_Hotel_ReservationDetail extends CElement {
        
        protected $data_hotel;
        protected $promo_list;
        protected $is_promo;
        protected $promo_type;
        protected $total_promo;
        protected $promo_name;
        
        public function __construct($id = "") {
            parent::__construct($id);
            $this->data_hotel = array();
            $this->promo_list = array();
            $this->is_promo = 'false';
            $this->promo_type = '';
            $this->total_promo = 0;
            $this->promo_name = '';
        }

        public static function factory($id) {
            return new CElement_Hotel_ReservationDetail($id);
        }
       
        public function set_data_hotel($data_hotel) {
            $this->data_hotel = $data_hotel;
            return $this;
        }

        public function set_promo_list($promo){
            $this->promo_list = $promo;
            return $this;
        }
        
        public function set_is_promo($is_promo) {
            $this->is_promo = $is_promo;
            return $this;
        }

        public function set_promo_type($promo_type) {
            $this->promo_type = $promo_type;
            return $this;
        }

        public function set_total_promo($total_promo) {
            $this->total_promo = $total_promo;
            return $this;
        }

        public function set_promo_name($promo_name) {
            $this->promo_name = $promo_name;
            return $this;
        }

        public function generate_detail() {
            $hotel_name = carr::get($this->data_hotel, 'hotel_name');
            $city = carr::get($this->data_hotel, 'city');
            $room_type_name = carr::get($this->data_hotel, 'room_type_name');
            $check_in = carr::get($this->data_hotel, 'check_in');
            $check_out = carr::get($this->data_hotel, 'check_out');
            $price = carr::get($this->data_hotel, 'price');
            $price_format = carr::get($this->data_hotel, 'price_format');
            $total_room = carr::get($this->data_hotel, 'total_room');
            $adult = carr::get($this->data_hotel, 'adult');
            $image_url = carr::get($this->data_hotel, 'image_url');
            
            $first_date = new DateTime($check_in);
            $second_date = new DateTime($check_out);
            $duration = $first_date->diff($second_date)->d;
            
            $container_detail = $this->add_div();

            $image_object = CFactory::create_img();
            $image_object->set_src($image_url);

            $hotel_info = $container_detail->add_div()
                    ->add_class("hotel-box-container");
            $image_info = $hotel_info->add_div()
                    ->add($image_object)
                    ->add_class("hotel-bc-image");
            $hotel_name_info = $hotel_info->add_div()
                    ->add($hotel_name)
                    ->add_class("hotel-bc-title");
            
            $hotel_name_info = $hotel_info->add_div()
                    ->add($city)
                    ->add_class("hotel-bc-title-s");
            $hotel_detail_info = $hotel_info->add_div()
                    ->add_class('hotel-bc-value')
                    ->add_class('row');
            
            $hotel_detail_info_title = $hotel_detail_info->add_div()
                    ->add(clang::__("Room Name"))
                    ->add_class("col-xs-4");
            $hotel_detail_info_value = $hotel_detail_info->add_div()
                    ->add(': ' . $room_type_name)
                    ->add_class("col-xs-8");
            
            $hotel_detail_info = $hotel_info->add_div()
                    ->add_class('hotel-bc-value')
                    ->add_class('row');
            $hotel_detail_info_title = $hotel_detail_info->add_div()
                    ->add(clang::__("Duration"))
                    ->add_class("col-xs-4");
            $hotel_detail_info_value = $hotel_detail_info->add_div()
                    ->add(': '.$duration.' '.clang::__(" Night"))
                    ->add_class("col-xs-8");
            
            $hotel_detail_info = $hotel_info->add_div()
                    ->add_class('hotel-bc-value')
                    ->add_class('row');
            $hotel_detail_info_title = $hotel_detail_info->add_div()
                    ->add(clang::__("Total Room"))
                    ->add_class("col-xs-4");
            $hotel_detail_info_value = $hotel_detail_info->add_div()
                    ->add(': '.$total_room.' Room')
                    ->add_class("col-xs-8");
            
            $hotel_detail_info = $hotel_info->add_div()
                    ->add_class('hotel-bc-value')
                    ->add_class('row');
            $hotel_detail_info_title = $hotel_detail_info->add_div()
                    ->add(clang::__("Total Guest"))
                    ->add_class("col-xs-4");
            $hotel_detail_info_value = $hotel_detail_info->add_div()
                    ->add(': '.$adult.' '.clang::__(' People'))
                    ->add_class("col-xs-8");
            
            $hotel_detail_info = $hotel_info->add_div()
                    ->add_class('hotel-bc-value')
                    ->add_class('row');
            $hotel_detail_info_title = $hotel_detail_info->add_div()
                    ->add(clang::__("Check in"))
                    ->add_class("col-xs-4");
            $hotel_detail_info_value = $hotel_detail_info->add_div()
                    ->add(': ' . $check_in)
                    ->add_class("col-xs-8");
            
            $hotel_detail_info = $hotel_info->add_div()
                    ->add_class('hotel-bc-value')
                    ->add_class('row');
            $hotel_detail_info_title = $hotel_detail_info->add_div()
                    ->add(clang::__("Check out"))
                    ->add_class("col-xs-4");
            $hotel_detail_info_value = $hotel_detail_info->add_div()
                    ->add(': ' . $check_out)
                    ->add_class("col-xs-8");

            $hotel_price = $container_detail->add_div()
                    ->add_class('hotel-box-container');

            $hotel_price_title = $hotel_price->add_div()
                    ->add(clang::__("Price Detail"))
                    ->add_class("hotel-bc-title");
            
            $hotel_price_box = $hotel_price->add_div()
                    ->add_class('row');
            $hotel_detail_info_title = $hotel_price_box->add_div()
                    ->add(clang::__("Price"))
                    ->add_class("col-xs-6");
            $hotel_detail_info_value = $hotel_price_box->add_div()
                    ->add($price_format)
                    ->add_class("hotel-bc-price")
                    ->add_class("col-xs-6");
            
            $hotel_line = $hotel_price->add_div()
                    ->add_class('hotel-line-container')
                    ->add_class('row');

            // $promo_total = 0;
            // foreach($this->promo_list as $pl_k => $pl_v){
            //     $fare_type = carr::get($pl_v, 'fare_type');
            //     $fare_amount = carr::get($pl_v, 'fare_amount');
            //     $promo_name = carr::get($pl_v, 'promo_name'); 
            //     $total_promo = 0;
            //     if($fare_type == 'amount'){
            //         $total_promo = $fare_amount;
            //     }
            //     if($fare_type == 'percent'){
            //         $total_promo = $price * $fare_amount;
            //     }

            //     $hotel_price_box = $hotel_price->add_div()
            //         ->add_class('row');
            //     $hotel_detail_info_title = $hotel_price_box->add_div()
            //             ->add($promo_name)
            //             ->add_class("col-xs-6");
            //     $hotel_detail_info_value = $hotel_price_box->add_div()
            //             ->add(ctransform::thousand_separator($total_promo, 0))
            //             ->add_class("hotel-bc-price")
            //             ->add_class("col-xs-6");
                
            //     $hotel_line = $hotel_price->add_div()
            //             ->add_class('hotel-line-container')
            //             ->add_class('row');

            //     $promo_total += $total_promo;
            // }
            
            $promo_total = 0;
            if($this->is_promo == 'true'){
                if($this->promo_type == 'amount') {
                    $promo_total = $this->total_promo;
                }
                
                if($this->promo_type == 'percent') {
                    $promo_total = round(($this->total_promo / 100) * $price);
                }

                $hotel_price_box = $hotel_price->add_div()
                    ->add_class('row');
                $hotel_detail_info_title = $hotel_price_box->add_div()
                    ->add($this->promo_name)
                    ->add_class("col-xs-6");
                $hotel_detail_info_value = $hotel_price_box->add_div()
                        ->add(ctransform::thousand_separator($promo_total, 0))
                        ->add_class("hotel-bc-price")
                        ->add_class("col-xs-6");                
                $hotel_line = $hotel_price->add_div()
                        ->add_class('hotel-line-container')
                        ->add_class('row');
            }

            $grand_price = $price - $promo_total;
            $grand_price = ctransform::thousand_separator($grand_price, 0);

            $hotel_line->add_div()
                    ->add_class('hotel-line');
            
            $hotel_price_box = $hotel_price->add_div()
                    ->add_class('row');
            $hotel_detail_info_title = $hotel_price_box->add_div()
                    ->add(clang::__("Total"))
                    ->add_class("col-xs-6");
            $hotel_detail_info_value = $hotel_price_box->add_div()
                    ->add($grand_price)
                    ->add_class("hotel-bc-price-total")
                    ->add_class("col-xs-6");
        }

        public function html($indent = 0) {
            $html = new CStringBuilder();

            $html->appendln($this->generate_detail());
            
            $html->set_indent($indent);
            $html->append(parent::html($indent));
            return $html->text();
        }

        public function js($indent = 0) {
            $js = CStringBuilder::factory();

            $js->append(parent::js($indent));
            return $js->text();
        }

    }
    