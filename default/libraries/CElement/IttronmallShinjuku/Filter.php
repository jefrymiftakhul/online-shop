<?php

class CElement_IttronmallShinjuku_Filter extends CElement {

    private $key = NULL;
    private $start = 0;
    private $count = 0;
    private $list_filter = array();
    private $filter_product = TRUE;
    private $list_filter_product = array();
    private $filter_price = TRUE;
    private $list_filter_price = array();
    private $min_price = 0;
    private $max_price = 0;
    private $filter_component_control = array();
    private $type_select = 'select';
    private $type_image = 'image';
    private $type_colorpicker = 'colorpicker';
    private $param = array();
    private $param_filter_attribut = array();
    private $product_visibility = 'catalog_search';
    private $filter_name = '';
    private $filter_page = '';
    private $filter_category_lft = '';
    private $filter_category_rgt = '';

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_IttronmallShinjuku_Filter($id);
    }

    public function set_key($key) {
        $this->key = $key;

        return $this;
    }

    public function set_filter_page($val) {
        $this->filter_page = $val;

        return $this;
    }

    public function set_filter_name($val) {
        $this->filter_name = $val;

        return $this;
    }

    public function set_filter_category_lft($val) {
        $this->filter_category_lft = $val;

        return $this;
    }

    public function set_filter_category_rgt($val) {
        $this->filter_category_rgt = $val;

        return $this;
    }

    public function set_product_visibility($val) {
        $this->product_visibility = $val;

        return $this;
    }

    public function set_list_filter($list_filter) {
        $this->list_filter = $list_filter;

        return $this;
    }

    public function set_filter_product($filter_product) {
        $this->filter_product = $filter_product;

        return $this;
    }

    public function set_list_filter_product($list_filter_product) {
        $this->list_filter_product = $list_filter_product;

        return $this;
    }

    public function set_filter_price($filter_price) {
        $this->filter_price = $filter_price;

        return $this;
    }

    public function set_min_price($val) {
        $this->min_price = $val;

        return $this;
    }

    public function set_max_price($val) {
        $this->max_price = $val;

        return $this;
    }

    public function set_list_filter_price($list_filter_price) {
        $this->list_filter_price = $list_filter_price;

        return $this;
    }

    private function generate_list($type, $data = array()) {
        $list = array();

        foreach ($data as $key => $row_data) {
            if ($type == $this->type_select) {
                //$value = carr::get($row_data, 'attribute_key');
                $value['key'] = $key;
                $value['label'] = carr::get($row_data, 'attribute_key');
            }
            if ($type == $this->type_image) {
                $value['key'] = $key;
                $value['label'] = carr::get($row_data, 'attribute_key');
                $value['value'] = carr::get($row_data, 'file_path');
            }
            if ($type == $this->type_colorpicker) {
                $hex_code = carr::get($row_data, 'hex_code');
                if (strlen($hex_code) == 0) {
                    $hex_code = carr::get($row_data, 'attribute_key');
                }
                $value['key'] = $key;
                $value['label'] = carr::get($row_data, 'attribute_key');
                $value['hex_code'] = $hex_code;
                
            }

            $list[$key] = $value;
        }

        return $list;
    }

    private function generate_form($container, $name, $data) {
        $id = carr::get($data, 'attribute_category_id');
        $type = carr::get($data, 'attribute_category_type');
        $attribute = carr::get($data, 'attribute', array());
        $list = $this->generate_list($type, $attribute);
        $control = array();

        if ($type == $this->type_select) {
            if (count($list) > 0) {
                $filter_select = $container->add_div()->add_class('horizontal-overflow')->custom_css('max-height', '200px');

                $div_container = $filter_select->add_div('container-' . $name)->add_class('padding-left30 filter-checkbox');
                foreach ($list as $key => $val) {
                    $control = $div_container->add_field()
                            ->add_control('catalog_filter_' . $id . '_' . carr::get($val,'key'), 'checkbox')
                            ->add_class('input-filter checkbox-62hallfamily' . $name)
                            ->set_value($key);
                    $div_container->add("
						<label for='catalog_filter_".$id."_".carr::get($val,'key')."'>" . carr::get($val,'label') . "</label>
					");

                    $this->param[] = 'catalog_filter_' . $id . '_' . carr::get($val,'key');
                    //$this->filter_component_control['click'][] = $control;
                }
            }
        } else if ($type == $this->type_image) {
            $div_container = $container->add_div('container-' . $name)
                    ->add_class('container-input-image');
            $index = 1;


            foreach ($list as $key => $value) {
                $image_hidden = $div_container->add_control('catalog_filter_' . $id . '_' . $key, 'hidden')
                                ->add_class('input-image')->set_value('');
                $this->param[] = 'catalog_filter_' . $id . '_' . $key;
                $div_image = $div_container->add_div('div_catalog_filter_' . $id . '_' . $key)
                        ->add_class('input-filter btn-colors margin-bottom-10 link ')
                        ->custom_css('display', 'inline-block')
                        ->custom_css('padding', '2px');

                $control = $div_image->add_img('filter_image_' . $key)
                        ->set_src($value['value'])
                        ->set_attr('key', $key)
                        ->set_attr('rel', 'catalog_filter_' . $id . '_' . $key)
                        ->set_attr('active', '0');

                //$this->filter_component_control['click'][] = $control;
                $index++;
            }
        }
        else if ($type == $this->type_colorpicker) {
            if (count($list) > 0) {
                $filter_select = $container->add_div()->add_class('horizontal-overflow')->custom_css('max-height', '200px');

                $div_container = $filter_select->add_div('container-' . $name)->add_class('filter-checkbox');
                
                $div_container->add('<ul class="list-inline">');
                foreach ($list as $key => $val) {
                    $control_id = 'catalog_filter_'.$id.'_'.carr::get($val,'key');
                    $control_key = carr::get($val,'key');
                    $control_label = carr::get($val,'label');
                    $control_color = carr::get($val, 'hex_code');
                    
                    $li_control = '<li>';
                        $li_control .= '<div class="checkbox-colorpicker">';
                            $li_control .= '<input type="checkbox" name="'.$control_id.'" id="'.$control_id.'" class="input-filter checkbox-62hallfamilyGDCOLOR validate[]" value="'.$control_key.'">';
                            $li_control .= '<label for="'.$control_id.'" style="background-color: '.$control_color.'">'.$control_label.'</label>';
                        $li_control .= '</div>';
                    $li_control .= '</li>';
//                    
                    $control = $div_container->add($li_control);

                    $this->param[] = 'catalog_filter_' . $id . '_' . carr::get($val,'key');
                    //$this->filter_component_control['click'][] = $control;
                }
                $div_container->add('</ul>');
            }
        }
        return $control;
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);


        $container_wrap = $this->add_div()->add_class('box-category');
        $container = $container_wrap->add_div()->add_class('txt-heading-wrap');
        $container->add_div()->add(clang::__('Price'))->add_class('txt-heading');

        $arr_price = array(
            '~-250000' => 'Under Rp 250,000',
            '250000-500000' => 'Rp 250,000 - Rp 500,000',
            '500000-1000000' => 'Rp 500,000 - Rp 1,000,000',
            '1000000-1500000' => 'Rp 1,000,000 - Rp 1,500,000',
            '1500000-3000000' => 'Rp 1,500,000 - Rp 3,000,000',
            '3000000-~' => 'Above Rp 3,000,000',
        );

        $container_filter_price = $container_wrap->add_div()->add_class('content-heading');
        $i = 0;
        foreach ($arr_price as $key => $value) {
            $filter_price_checkbox = $container_filter_price->add_div()->add_class('filter-checkbox-wrapper')->add("<div>
                <input class='input-filter input-filter-price' type='checkbox' name='price_filter[]' id='price_filter_".$i."' value='".$key."'/></div><div><label for='checkbox_filter_".$key."' class='noselect'>".$value."</label></div>
            ");
            $this->param[] = 'price_filter_'.$i;
            $i++;
        }

        foreach ($this->list_filter as $key_filter => $list_filter) {
            $container_wrap = $this->add_div()->add_class('box-category');
            $container = $container_wrap->add_div()->add_class('txt-heading-wrap');
            $container->add_div()->add(clang::__(carr::get($list_filter, 'attribute_category_name')))->add_class('txt-heading');

            $key_filter = explode(' ', $key_filter);
            $key_filter = implode('_', $key_filter);

            $container_filter = $container_wrap->add_div()->add_class('content-heading');
            
            $this->generate_form($container_filter, $key_filter, $list_filter);
        }

        if (count($this->filter_component_control) > 0) {
            foreach ($this->filter_component_control as $k => $v) {
                if (count($v) > 0) {
                    foreach ($v as $control) {
                        $control->add_listener($k)
                                ->add_handler('reload')
                                ->set_target('page-product-category')
                                ->add_param_input($this->param)
                                ->set_url(curl::base() . 'reload/reload_product_filter?source=filter&visibility=' . $this->product_visibility . '&filter_page=' . $this->filter_page . '&filter_name=' . urlencode($this->filter_name) . '&filter_category_lft=' . $this->filter_category_lft . '&filter_category_rgt=' . $this->filter_category_rgt);
                    }
                }
            }
        }

        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();
        $data_addition = '';

        foreach ($this->param as $inp) {
            if (strlen($data_addition) > 0)
                $data_addition.=',';
            $data_addition.="'" . $inp . "':$.cresenity.value('#" . $inp . "')";
        }

        $data_addition = '{' . $data_addition . '}';
        $js->appendln("           
            function reload_filter() {
                if($(this).hasClass('btn-colors')) {
                    var image = jQuery(this).find('img');
                    var rel = image.attr('rel');
                    var hidden = jQuery('#'+rel);
                    var active = image.attr('active');
                    var key = image.attr('key');

                    if(active=='0'){
                            image.attr('active','1');
                            hidden.val(key);
                            jQuery(this).addClass('active');
                    }else{
                            image.attr('active','0');
                            hidden.val('');
                            jQuery(this).removeClass('active');
                    }
                }
                
                var txt_sort_by = jQuery('#category_sort_by').val();
                var txt_sort_view = jQuery('#category_sort_view').val();

                $.cresenity.reload('page-product-category','" . curl::base() . 'reload/reload_product_filter?view_limit=\'+txt_sort_view+\'&sortby=\'+txt_sort_by+\'&source=filter_reload&visibility=' . $this->product_visibility . '&filter_page=' . $this->filter_page . '&filter_name=' . urlencode($this->filter_name) . '&filter_category_lft=' . $this->filter_category_lft . '&filter_category_rgt=' . $this->filter_category_rgt . "','get'," . $data_addition . ");
            }
            
            jQuery('.select-filter').change(function() {
                reload_filter();
            });

            $('.input-filter').click(function(){
                reload_filter();
            });
        ");

        $js->append(parent::js($indent));
        return $js->text();
    }

}
