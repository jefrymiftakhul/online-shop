<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Mar 23, 2016
     */
    class CElement_IttronmallShinjuku_SlideDeal extends CElement_SlickSlideShow{
        private $using_title = true;
        private $title = 'Slide';
        public function __construct($id = "", $tag = "div") {
            parent::__construct($id, $tag);
            // responsive slide setting
            $responsive = array();
            $responsive[] = array(
                'breakpoint' => '1200',
                'slide_to_show' => '4',
                'slide_to_scroll' => '1',
                'dots' => 'false',
                'centerMode' =>'false',
            );
            $responsive[] = array(
                'breakpoint' => '990',
                'slide_to_show' => '4',
                'slide_to_scroll' => '1',
                'dots' => 'false',
            );
            $responsive[] = array(
                'breakpoint' => '811',
                'slide_to_show' => '3',
                'slide_to_scroll' => '1',
                'dots' => 'false',
            );
           $responsive[] = array(
               'breakpoint' => '381',
               'centerPadding' =>'30px',
               'slide_to_show' => '1',
               'variableWidth','true',
               'centerMode'=>'true',
               'variableWidth'=> 'true',
               'dots' => 'false',
               'arrows' => 'false'
               
           );
            $this->dots = "false";
            $this->infinite = "true";
            $this->autoplay = false;
            $this->autoplay_speed = 5000;
            $this->slide_to_show = 4;
            $this->slide_to_scroll = 1;
            $this->responsive = $responsive;
        }
        
        public static function factory($id = "", $tag = "div") {
            return new CElement_IttronmallShinjuku_SlideDeal($id, $tag);
        }
        
        public function set_title($title){
            $this->title = $title;
            return $this;
        }
        
        public function set_using_title($bool){
            $this->using_title = $bool;
            return $this;
        }


        public function html($indent = 0) {
            $html = new CStringBuilder();
            $classes = $this->classes;
            $classes = implode(" ", $classes);

            
            if (strlen($classes) > 0) {
                $classes = " " . $classes;
            }

            $counter = 0;
            if($this->using_title){
                    $html->append('<div class="page-title"><span>'.$this->title.'</span>
                    <div class="ico-underline"></div>
                </div>');
            }
            $html->append('<div id="' . $this->id . '" class="' . $classes . '">');
          
            foreach ($this->slides as $k => $v) {
                $image_path = carr::get($v, 'image_path');
                //$image_url = image::get_url($image_path);
                $html->appendln('<div>');
                $html->appendln('   <div>');
//                $html->appendln('       <img class="img-responsive" src="' . $image_url . '"/>');
                $html->appendln(CElement_IttronmallShinjuku_Product::factory()->set_data_product($v)->set_class($classes)->set_type($this->type)->html());
                $html->appendln('   </div>');
                $html->appendln('</div>');
                
            }
            $html->append('</div>');
            $html->appendln(parent::html($indent));

            return $html->text();
        }                
    }
    