<?php

class CElement_IttronmallShinjuku_Product extends CElement {

    private $class = 'shinjuku';
    private $data_product = null;
    private $product_hot = "Hot";
    private $product_new = "New";
    private $price = 0;
    private $type = 'product';
    private $data = array();

    public function __construct($id = '') {
        parent::__construct($id);
    }

    public static function factory($id = '') {
        return new CElement_IttronmallShinjuku_Product($id);
    }

    // giving class for multiple styling purpose
    public function set_class($class) {
        $this->class = $class;
        return $this;
    }

    public function set_data_product($data) {
        $this->data_product = $data;
        return $this;
    }

    public function set_price($price) {
        $this->price = $price;
        return $this;
    }
    
    public function set_type($val){
        $this->type=$val;
        return $this;
    }
    
    private function get_image($image_name) {
        $return = curl::base() . 'application/ittronmall/default/media/img/product/no-image.png';
        if ($image_name != NULL) {
            $return = image::get_image_url($image_name, 'view_all');
        }
        return $return;
    }

    private function get_price($data_price = NULL) {
        $detail_price = $this->price;
        
        if($detail_price ==  NULL){
            $detail_price = $data_price;
            }
        $promo_price = 0;
        $price = 0;

        if (!empty($detail_price['ho_sell_price']) and ! empty($detail_price['channel_sell_price'])) {
            if ($detail_price['ho_sell_price'] > $detail_price['channel_sell_price']) {
                $price = $detail_price['ho_sell_price'];
                $promo_price = $detail_price['channel_sell_price'];
            } else {
                $price = $detail_price['channel_sell_price'];
            }
        }

        return array('promo_price' => $promo_price, 'price' => $price);
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        // data product
        $data_product = $this->data_product;
        $image_path = carr::get($data_product, 'image_path');
        $image_url = carr::get($data_product, 'image_url', NULL);
        
        $our_service_hide ='';
        $price_strike = '';
        $percent_sale = '';
        $percent_label = '';
        $promo_text ='';
        
        if (strlen($image_url) == 0) {
            $image_url = $this->get_image($image_path);
        }
        
        $name = carr::get($data_product, 'name');
        $url_key = carr::get($data_product, 'url_key');
        $is_hot_item = carr::get($this->data_product, 'is_hot_item');
  
        $is_available = carr::get($this->data_product, 'is_available');
        $new_product_date = carr::get($this->data_product, 'hot_until');
    
        // data product price
        $detail_price = carr::get($data_product, 'detail_price');
  
        $channel_sell_price = carr::get($detail_price, 'channel_sell_price');
        $price_all = $this->get_price($detail_price);
        $price_promo = carr::get($price_all, 'promo_price');
        $price_normal = carr::get($price_all, 'price');
        $promo_value = carr::get($detail_price, 'promo_value');

        $price = 'Rp ' . ctransform::format_currency($price_normal);
     
        if ($promo_value != 0) {
            $promo_type = carr::get($detail_price, 'promo_type');
            $promo_value = carr::get($detail_price, 'promo_value');
            $promo_text =  carr::get($detail_price,'promo_text');
            if ($promo_type == 'amount') {
                $price_promo = $price_normal - $promo_value;
                $price = 'Rp ' . ctransform::format_currency($price_promo);
                $price_strike = 'Rp ' . ctransform::format_currency($price_normal);
                if ($price_normal != 0) {
                    $percent_sale = round(($promo_value / $price_normal) * 100);
                }
                $percent_label = $percent_sale . '%';
            } else if ($promo_type == 'percent') {
                $price_percent = $price_normal * ($promo_value / 100);
                $price_promo = $price_normal - $price_percent;
                $price = 'Rp ' . ctransform::format_currency($price_promo);
                $price_strike = 'Rp ' . ctransform::format_currency($price_normal);
                $percent_sale = $promo_value;
                $percent_label = $percent_sale . '%';
            }
        }
     
        $product_wrapper = $this->add_div()->add_class('container-product product-wrapper ' . $this->class);
            $a_type='<a href="' . curl::base() . 'product/item/' . $url_key . '">';
            
        if($this->type=='category'){
            $a_type='<a href="' . curl::base() . 'search?keyword=&category=' . $url_key . '">';
            $our_service_hide = 'our-service-hide';
        }
        $product_wrapper->add($a_type);
        $wrapper_ico = $product_wrapper->add_div()->add_class('ico_wrapper');

        if ($is_hot_item > 0) {
            $wrapper_ico->add_div()->add_class('ico-hot-product');
            
        }
        if (strtotime($new_product_date) > strtotime(date('Y-m-d'))) {
            $wrapper_ico->add_div()->add_class('ico-new-product');
        }
         if ($promo_text > 0 ){
            $product_wrapper->add_div()->add_class('ico-sale');
        }
        
        $product_wrapper_image = $product_wrapper->add_div()->add_class('product-wrapper--image');
//                $product_wrapper_image->add_attr('style',"background-image:url('".$image_url."');");
        
        if($this->type == 'category'){
            // Background black behind image
            $product_wrapper_image->add('<div class="bg-drop"><img class="img-responsive" src="' . $image_url . '">');
            // Caption
            $product_wrapper_image->add('<div class="bg-caption">'.$name.'</div></div>');
        }else{
        $product_wrapper_image->add('<img class="img-responsive" src="' . $image_url . '">');
        }
        $product_wrapper_description = $product_wrapper->add_div()->add_class($our_service_hide.' product-wrapper--description');
        $product_title = $product_wrapper_description->add_div()->add_class('product-wrapper--description-title');
        $product_title->add($name);
        $product_price = $product_wrapper_description->add_div()->add_class('product-wrapper--description-price');
       

        if ($promo_value != 0) {
//           $promo_wrapper =  $product_price->add_div()->add_class('container-sale-off-home');
//            $promo_wrapper->add_div()->add_class('sale-off')->add('<p class="promo-price-text"> -' . $percent_label . '</p>');
            $product_price_strike = $product_price->add_div()->add_class('product-price-strike-home');
            $product_price_strike->add($price_strike);
            
             $product_price_normal = $product_price->add_div()->add_class('product-price-normal-home');
        $product_price_normal->add($price);

            
        } else {
            $product_price = $product_wrapper_description->add_div()->add_class('product-wrapper--description-price');
            $product_price_normal = $product_price->add_div()->add_class('product-price-normal');
            $product_price->add('Rp '.ctransform::format_currency($channel_sell_price));
        }
        $product_wrapper->add('</a>');

        $html->append(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();

        $js->append(parent::js($indent));
        return $js->text();
    }

}
