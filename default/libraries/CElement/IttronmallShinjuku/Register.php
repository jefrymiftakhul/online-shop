<?php

    class CElement_IttronmallShinjuku_Register extends CElement {

        protected $trigger_button;
        protected $trigger_button_label = 'Daftar';
        protected $trigger_button_class = NULL;
        protected $redirect;
        protected $reload;
        protected $use_icon;
        protected $store;
        protected $have_product;
        protected $have_gold;
        protected $have_service;
        protected $force_html;

        public function __construct($id = '') {
            parent::__construct($id);
            $this->trigger_button = false;
            $this->force_html = false;
            $this->reload = true;
            // $this->redirect = curl::base() . curl::current() . CFRouter::$query_string;
            $this->redirect = curl::base() . 'member/confirm'; 
            $this->use_icon = true;
            $this->have_product = TRUE;
        }

        public static function factory($id = '') {
            return new CElement_IttronmallShinjuku_Register($id);
        }

        public function set_redirect($redirect) {
            $this->redirect = $redirect;
            return $this;
        }

        public function set_reload($reload) {
            $this->reload = $reload;
            return $this;
        }

        public function set_trigger_button($bool) {
            $this->trigger_button = $bool;
            return $this;
        }
        
        public function set_trigger_button_label($label) {
            $this->trigger_button_label = $label;
            return $this;
        }
        
        public function set_trigger_button_class($class) {
            $this->trigger_button_class = $class;
            return $this;
        }

        public function set_icon($use_icon) {
            $this->use_icon = $use_icon;
            return $this;
        }
        
        public function set_store($val){
            $this->store = $val;
            return $this;
        }

        public function set_have_product($val){
            $this->have_product = $val;
            return $this;
        }

        public function set_have_gold($val){
            $this->have_gold = $val;
            return $this;
        }
        
        public function set_have_service($val){
            $this->have_service = $val;
            return $this;
        }
        
        public function set_force_html($bool) {
            $this->force_html = $bool;
            return $this;
        }
		
		public function html_button($index = 0) {
            $html = new CStringBuilder();
            if ($this->trigger_button) {
				
                $is_hide = '';
                
                $icon = '';
                if ($this->use_icon) {
                    $icon = "<i class='glyphicon glyphicon-user'></i> ";
                }
                $class = '';
                if (count($this->classes) > 0) {
                    $class = implode(' ', $this->classes);
                }
                $register_btn = CAction::factory()
                        ->set_label($icon .clang::__($this->trigger_button_label))
                        ->add_class($this->trigger_button_class);
                $register_btn->add_class("btn-modal-register-".$this->id." btn-register");
                $register_btn->add_class($class);
                $register_btn->add_attr('data-target', 'modal-body-'.$this->id);
                $register_btn->add_attr('data-modal', 'register_form');
                $register_btn->add_attr('data-title', clang::__("REGISTER"));
				$html->append($register_btn->html());
			}
			
			return $html->text();
		}

        public function html($index = 0) {
            $html = new CStringBuilder();

            $is_hide = '';
			$html->append($this->html_button());
            $modal = $this->add_div('custom_modal_'.$this->id)->add_class('modal');
            $modal_dialog = $modal->add_div()->add_class('modal-dialog');
            $modal_content = $modal_dialog->add_div()->add_class('modal-content clearfix');
            $modal_header = $modal_content->add_div()->add_class('modal-header'); 
            $modal_header->add("<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>");
            $modal_header->add("<h4 class='modal-title txt-title-login'>".clang::__('REGISTER')."</h4>");
            
            if ($this->force_html) {
                $register_container = $this->add_div();
            }
            else {
                $register_container = $modal_content->add_div('modal-body-'.$this->id)->add_class('modal-body register_form clearfix '.$is_hide);
            }
                    
            $form = $register_container->add_form();
            $form->add_class('registration form-auth form-62hallfamily');
            $form->set_action(curl::base() . 'auth/register');
            $form->set_ajax_submit(true);
            $form->set_validation(false);

            $row = $form->add_div()->add_class('row');

            $row_message = $row->add_div()->add_class('col-md-12')->add_div()->add_class('register-message auth-message');

            $content = $row->add_div()->add_class('col-md-12');
            $container_field = $content->add_div()->add_class('container-field');
            $container_field->add_div()->add_class('icon-register-wrapper')->add_div()->add_class('icon-register-user');
            $container_field->add_div()->add_control('', 'text')->set_name('name')->add_validation('required')->set_placeholder(clang::__('Name'));

            $content = $row->add_div()->add_class('col-md-12');
            $container_field = $content->add_div()->add_class('container-field');
            $container_field->add_div()->add_class('icon-register-wrapper')->add_div()->add_class('icon-register-email');
            $container_field->add_div()->add_control('', 'text')->set_name('email')->add_validation('required')->set_placeholder(clang::__('Email'));

            $content = $row->add_div()->add_class('col-md-12');
            $container_field = $content->add_div()->add_class('container-field');
            $container_field->add_div()->add_class('icon-register-wrapper')->add_div()->add_class('icon-register-key');
            $container_field->add_div()->add_control('', 'password')->set_name('password')->add_validation('required')->set_placeholder(clang::__('Password').' '.clang::__('(min 6 digit)'));

            $content = $row->add_div()->add_class('col-md-12');
            $container_field = $content->add_div()->add_class('container-field');
            $container_field->add_div()->add_class('icon-register-wrapper')->add_div()->add_class('icon-register-key');
            $container_field->add_div()->add_control('', 'password')->set_name('confirm_password')->add_validation('required')->set_placeholder(clang::__('Confirm Password'));

            $content = $row->add_div()->add_class('col-md-12');
            $container_field = $content->add_div()->add_class('container-field');
            $container_field->add_div()->add_class('icon-register-wrapper')->add_div()->add_class('icon-register-phone');
            $container_field->add_div()->add_control('', 'text')->set_name('phone')->add_validation('required')->set_placeholder(clang::__('Phone'))->add_class('numeric');


            $row_field = $row->add_div()->add_class('col-md-12');
        
            
            if ($this->have_product){
                $check = $row_field->add('<div class="col-md-12 margin-top-10 margin-bottom-10">
                    <label for="checkbox_newsletter">
                    <input class="margin-left-15-" type="checkbox" name="is_subscribe" id="checkbox_newsletter" value="1"/> 
                    Saya ingin menerima penawaran produk '.$this->store.' melalui email</label>
                </div>');;
            }

            $link_syarat_ketentuan = curl::base() . 'read/page/index/syarat-dan-ketentuan';
            $link_kebijakan_privacy = curl::base() . 'read/page/index/kebijakan-privasi';
            
            $subcribe = $row_field->add_div()
                    ->add('Dengan menekan Daftar Akun, saya mengkonfirmasi telah menyetujui <a target="_blank" href="'.$link_syarat_ketentuan.'" class="font-red"><u>Syarat dan Ketentuan</u></a>, serta <a target="_blank" href="'.$link_kebijakan_privacy.'" class="font-red"><u>Kebijakan Privasi</u></a> ' . $this->store);
            
            $action = $row_field
                    ->add_action()
                    ->set_label(clang::__('DAFTAR'))
                    ->add_class('btn-auth btn-imlasvegas-light btn-62hallfamily-reg')
                    ->custom_css('width', '100%')
                    ->custom_css('margin-top', '15px')
                    ->custom_css('margin-bottom', '15px')
                    ->set_submit(true);
					
            if ($this->reload == true) {
                $action->set_attr('on-reload', true);
                $row->add_control('', 'hidden')->set_name('redirect')->add_class('redirect redirect-for-register')
                        ->set_value($this->redirect);
            }
            
            $row_field->add_br();

            $html->appendln(parent::html($index));

            return $html->text();
        }

        public function js($index = 0) {
            $js = new CStringBuilder();
            
            

            
            $js->appendln("
                
                $(document).on('keypress keyup blur', '.numeric', function (event){
                    $(this).val($(this).val().replace(/[^0-9]/g,''));
                    if ((event.which < 48 || event.which > 57)) {
                     event.preventDefault();
                    }
                });
                
                $('.close_message').click(function(){
                    $('.auth-message').hide();
                    $('.register-message').hide();
                });
             console.log('.btn-modal-register-".$this->id."');
                $('.btn-modal-register-".$this->id."').click(function(event) {
                    var modal_content   = $(this).attr('data-modal');
                    var modal_title     = $(this).attr('data-title');
                    var body_target     = $(this).attr('data-target');
                    //var body            = $('.'+modal_content).html();
                    var body = '';
                       
                    if (modal_content && modal_title) {
                        if (modal_content != 'logout') {
                            if(body_target=='undefined' || body_target.length==0) {
                                body_target = '62hall-dialog';
                            }
                            $.app.show_dialog(body_target, modal_title, body, modal_content);
                            return;    

                        }
                    }
                });
                
                $('.btn-register').click(function(){
                    $('.modal-title').html('REGISTER');
                });
            ");
            
            $js->appendln(parent::js($index));
            return $js->text();
        }

    }
    