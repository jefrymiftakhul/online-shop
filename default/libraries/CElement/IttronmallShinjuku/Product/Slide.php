<?php

class CElement_IttronmallShinjuku_Product_Slide extends CElement{
    
    protected $list_product = array();
    protected $page = '';
    protected $limit = 10;

    public function __construct($id = '') {
        parent::__construct($id);
    }
    
    public static function factory($id = ''){
        return new CElement_IttronmallShinjuku_Product_Slide($id);
    }  

    public function set_list_item($data) {
        $this->list_product = $data;
        return $this;
    }

    public function set_page($val){
        $this->page = $val;
        return $this;
    }

    public function set_limit($val){
        $this->limit = $val;
        return $this;
    }

    private function get_image($image_path = '') {
        $retcode=200;
        if ($retcode == 500 or $image_path == NULL) {
            $image_path = curl::base() . 'application/lasvegas/default/media/img/product/no-image.png';
        } else {
            $image_path = $image_path;
        }
        return $image_path;
    }

    private function get_price($detail_price) {
        $promo_price = 0;
        $price = 0;

        if (!empty($detail_price['ho_sell_price']) and ! empty($detail_price['channel_sell_price'])) {
            if ($detail_price['ho_sell_price'] > $detail_price['channel_sell_price']) {
                $price = $detail_price['ho_sell_price'];
                $promo_price = $detail_price['channel_sell_price'];
            } else {
                $price = $detail_price['channel_sell_price'];
            }
        }

        return array('promo_price' => $promo_price, 'price' => $price);
    }
    
    public function html($indent=0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        
        $class = '';
        if (count($this->classes) > 0) {
            $class = implode(' ', $this->classes);
        }

        $container = $this->add_div()->add_class($class);
        $row = $container->add_div()->add_class('row row-ths');
        if (count($this->list_product) > 0) {
            foreach ($this->list_product as $product_k => $product_v) {    

                $product_v = (array) $product_v;
                if ($product_k >= $this->limit) {
                    break;
                }
                $product_id = carr::get($product_v, 'product_id');
                $url_key = carr::get($product_v, 'url_key');
                $code = carr::get($product_v, 'code');
                $name = carr::get($product_v, 'name');
                $sku = carr::get($product_v, 'sku');
                $weight = carr::get($product_v, 'weight');
//                $image_path = carr::get($product_v, 'image_path');
//                if (strlen($image_path) == 0) {
                    $image_path = carr::get($product_v, 'file_path');
//                }

                $image_name = carr::get($product_v, 'image_path');
                if(strlen($image_name)==0) {
                    $image_name = carr::get($product_v, 'image_name');
                
                }
                $image_src = $image_path;
                
            
                $image_src = image::get_image_url($image_name);
       
//                $image_src = $image_path;
                $is_hot_item = carr::get($product_v, 'is_hot_item');
                $hot_until = carr::get($product_v, 'hot_until');
                
                $new_product_date = carr::get($product_v, 'new_product_date');
                $is_available = carr::get($product_v, 'is_available');  // is_available > 0 tampilkan harga, else tampilkan "By Request"
                $quick_overview = carr::get($product_v, 'quick_overview');
                $stock = carr::get($product_v, 'stock');
                $show_minimum_stock = carr::get($product_v, 'show_minimum_stock');
                $product_data_type = carr::get($product_v, 'product_data_type');
                
                $detail_price = carr::get($product_v, 'detail_price');
                if (count($detail_price) == 0) {
                    $detail_price = $product_v;
                }
                $promo_start_date=carr::get($detail_price,'promo_start_date');
                $promo_end_date=carr::get($detail_price,'promo_end_date');
                $promo_text = carr::get($detail_price, 'promo_text');
                $promo_value = carr::get($detail_price, 'promo_value');
                
                $price_all = $this->get_price($detail_price);
                
                $price_promo = carr::get($price_all, 'promo_price');
                $price_normal = carr::get($price_all, 'price');
                
                $price = 'Rp '.ctransform::format_currency($price_normal);
                $price_strike = '';
//                if ($price_promo > 0) {
//                    $price = 'Rp '.ctransform::format_currency($price_promo);
//                    $price_strike = 'Rp '.ctransform::format_currency($price_normal);
//                }
                 $percent_sale = '';
                 $percent_label = '';
                if ($promo_value != 0){
                    $promo_type  = carr::get($detail_price, 'promo_type');
                    $promo_value  = carr::get($detail_price, 'promo_value');
                    if ($promo_type == 'amount'){
                        $price_promo = $price_normal - $promo_value;
                        $price = 'Rp '.ctransform::format_currency($price_promo);
                        $price_strike = 'Rp '.ctransform::format_currency($price_normal);
                        $percent_sale = round(($promo_value/$price_normal) * 100);
                        $percent_label = '-'.$percent_sale.'%';
                    }
                    else if ($promo_type == 'percent'){
                        $price_percent = $price_normal * ($promo_value/100);
                        $price_promo = $price_normal - $price_percent;
                        $price = 'Rp '.ctransform::format_currency($price_promo);
                        $price_strike = 'Rp '.ctransform::format_currency($price_normal);
                        $percent_sale = $promo_value;
                        $percent_label = '-'.$percent_sale.'%';
                    }
                }
                
                $page = $this->page.'/';
                if($this->page == 'product'){
                    $page = '';
                }
                $location_detail = curl::httpbase() . $page.'product/item/'.$url_key;
                $name_ori = $name;
                if (strlen($name) > 30) {
                    $name = substr($name, 0, 30);
                    
                    $new_name = '';
                    $name_arr = explode(' ', $name);
                    $end_sentance = end($name_arr);
                    $last_key = key($name_arr);
                    if(strlen($end_sentance)<=3 && strlen($end_sentance)>0){
                        unset($name_arr[$last_key]);
                        foreach ($name_arr as $key => $val) {
                            $new_name.=$val.' ';
                        }
                        $new_name = rtrim($new_name, ',.;: aA-');
                        $new_name.= ' ...';
                    }
                    else{
                        $new_name = $name.' ...';
                    }
                }
                else{
                    $new_name = $name;
                }
                
                
                $column = $row->add_div()->add_class('col-lg-5ths col-md-5ths col-sm-5ths col-xs-5ths col-xxs-5ths');
                
                $a = $column->add_elm('a')->set_attr('href', $location_detail)->set_attr('title',htmlspecialchars($name_ori));
                $container_product = $a->add_div()->add_class('container-product')
                        //->set_attr('onclick', "window.location='".$location_detail."'")
                        ;
                $container_sale = $container_product->add_div()->add_class('container-sale');
                $container_flag = $container_product->add_div()->add_class('container-flag');
                $container_product_image = $container_product->add_div()->add_class('text-center container-product-image');
                $container_product_desc = $container_product->add_div()->add_class('container-product-desc');
                $container_product_price = $container_product->add_div()->add_class('container-product-price');
                
                $container_price_strike = $container_product_price->add_div()->add_class('product-price-strike');
                $container_price_normal = $container_product_price->add_div()->add_class('product-price-normal');
                
                $container_sale_off = $container_product_price->add_div()->add_class('container-sale-off');
                
                if ($is_hot_item > 0 && date("Y-m-d")  <= $hot_until && $hot_until  != null) {
                    $container_flag->add_div()->add_class('is-hot')->add('Hot');
                }
                
                if ( date("Y-m-d")  <= $new_product_date) {
                    $container_flag->add_div()->add_class('is-new')->add('New');
                }
                
                $container_product_image->add("<img title='".htmlspecialchars($name_ori)."' alt='".htmlspecialchars($name_ori)."' src='".$image_src."'>");
                $container_product_desc->add($new_name);
                $container_price_normal->add($price);
                if ($promo_value != 0) {
                    $container_price_strike->add($price_strike);
//                    $container_sale_off->add_div()->add_class('sale-off')->add($percent_label);
                }
            }
        }

        
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent=0) {
        $js = new CStringBuilder();

        $js->append(parent::js($indent));
        return $js->text();
    }
}
