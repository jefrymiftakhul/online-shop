<?php

/**
 * Description of RajaOngkir
 *
 * @author Imam
 */
class CElement_RajaOngkir extends CElement {

  protected $product_id;

  public function __construct($id = '') {
	parent::__construct($id);
  }
  
  public function set_product_id($product_id) {
	$this->product_id = $product_id;
  }

  public static function factory($id = '') {
	return new CElement_RajaOngkir($id);
  }

  public function html($indent = 0) {
	$html = new CStringBuilder();
	$html->set_indent($indent);
	$db = CDatabase::instance();
//	$session = Session::instance();
//	$session->set('shipping_data', null);
	$row_shipping = $this->add_div()->add_class('row price-component');
	$row_shipping->add_div()->add_class('col-xs-12')->add(clang::__('Shipping Cost'));
	// getting data seller product
	$product_id = $this->product_id;
	$data_product = product::get($product_id);
	$courier = carr::get($data_product, 'shipping');
	$origin = carr::get($data_product, 'city_id');
	$weight = carr::get($data_product, 'weight');
	// get city id seller from raja ongkir
	$city_id_seller = carr::get($data_product, 'city_id');
	$city_id_seller_raja_ongkir = cdbutils::get_row('
                    select 
                        c.name as city_name,
                        p.name as province_name,
                        c.raja_ongkir_city_id,
                        c.city_id
                    from 
                        city as c
                        inner join province as p on p.province_id=c.province_id
                    where 
                        c.status > 0 
                        and c.city_id =' . $db->escape($city_id_seller));
	$row_shipping_type_wrapper = $this->add_div()->add_class('row');
	$div_shipping_type_wrapper = $row_shipping_type_wrapper->add_div()->add_class('shipping_wrapper col-xs-12');

	// getting data member which logged in
	$member_id = Session::instance()->get('member_id');
	$data_shipping_member = null;
	if ($member_id && strlen($member_id) == 0) {
	  $div_shipping_type_wrapper->add('<p>' . clang::__('Opss error occured when trying to get member address.. Please logout and login again') . '</p>');
	}
	else {
	  $data_shipping_member = member::get_shipping_address($member_id);
	  // get city id member from raja ongkir
	  $city_id_member = cobj::get($data_shipping_member, 'city_id');
	  $city_id_member_raja_ongkir = cdbutils::get_row('
                    select 
                        c.name as city_name,
                        p.name as province_name,
                        c.raja_ongkir_city_id,
                        c.city_id
                    from 
                        city as c
                        inner join province as p on p.province_id=c.province_id
                    where 
                        c.status > 0 
                        and c.city_id =' . $db->escape($city_id_member));
	  if ($data_shipping_member == null) {
		$div_shipping_type_wrapper->add('<p>' . clang::__('Please complete your shipping info') . '</p>');
	  }
	  else {
		if ($courier == 'INCLUDED') {
		  $this->add_listener('ready')
				  ->add_handler('custom')
				  ->set_js("
					$.cresenity.reload('shipping_cost_" . $this->id . "','" . curl::base() . "raja_ongkir/set_session_shipping/','get',{'shipping_data':'INCLUDED'});
				  ");
		  $div_shipping_type_wrapper->add('<span class="info_product_modal">' . clang::__('Shipping cost is free for this product') . '</span>');
		  $div_info_shipping_wrapper = $this->add_div()->add_class('row info_product');
		  $info_shipping_12 = $div_info_shipping_wrapper->add_div()->add_class('col-xs-12');
		  $info_shipping_12->add_div()->add_class('col-xs-3')->add('<p>' . clang::__('From') . '</p>');
		  $info_shipping_12->add_div()->add_class('col-xs-9')->add('<p> : ' . $city_id_seller_raja_ongkir->city_name . ', ' . $city_id_seller_raja_ongkir->province_name . '</p>');
		  $info_shipping_12->add_div()->add_class('col-xs-3')->add('<p>' . clang::__('To') . '</p>');
		  $info_shipping_12->add_div()->add_class('col-xs-9')->add('<p> : ' . $data_shipping_member->name . ', '.$data_shipping_member->address.', '.clang::__('Postal code').' '.$data_shipping_member->postal.', '. $city_id_member_raja_ongkir->city_name . ', ' . $city_id_member_raja_ongkir->province_name . '</p>');
		  $info_shipping_12->add_div()->add_class('col-xs-3')->add('<p>' . clang::__('Weight') . '</p>');
		  $info_shipping_12->add_div()->add_class('col-xs-9')->add('<p> : ' . $weight . ' gr</p>');
		  $warning_address = $this->add_div()->add_class('col-xs-12 warning_address');
		  $warning_address->add(clang::__('Please make sure your shipping address is right, you can\'t change after biding. Seller will ship your product to your shipping address setting'));
		}
		else {
		  $courier = json_decode($courier, true);
		  $json_user_courier = cdbutils::get_value('select shipping from transaction_bid where status > 0 and member_id =' . $db->escape($member_id) . ' order by transaction_date DESC');
		  $array_user_courier = json_decode($json_user_courier, true);
		  $user_courier = carr::get($array_user_courier, 'courier');
		  $user_service = carr::get($array_user_courier, 'service');
		  $list = array();
		  // generating select
		  foreach ($courier as $key => $value) {
			if ($value != null) {
			  $nama = explode("_", $key);
			  $list[$nama[1]] = ucfirst($nama[0]) . ' ' . ucfirst($nama[1]);
			}
		  }
		  $select_courier = $div_shipping_type_wrapper->add_control('shipping_type_' . $this->id, 'select')
				  ->set_list($list)
				  ->set_name('shipping_type')
				  ->set_applyjs('select2')
				  ->custom_css('width', '100%')
				  ->set_value($user_courier);
		  // get selected shipping user last bid                
		  $div_shipping_type_wrapper->add_control('courier_' . $this->id, 'hidden')->set_value($user_courier);
		  $div_shipping_type_wrapper->add_control('service_' . $this->id, 'hidden')->set_value($user_service);

		  $div_shipping_type_wrapper->add_control('origin_' . $this->id, 'hidden')->set_value($city_id_seller_raja_ongkir->raja_ongkir_city_id);
		  if ($data_shipping_member != null) {
			$div_shipping_type_wrapper->add_control('destination_' . $this->id, 'hidden')->set_value($city_id_member_raja_ongkir->raja_ongkir_city_id);
		  }
		  $div_shipping_type_wrapper->add_control('weight_' . $this->id, 'hidden')->set_value($weight);
		  $select_courier->add_listener('change')
				  ->add_handler('custom')
				  ->set_js('
					reload_shipping_cost_' . $this->id . '();
				  ');
		  $select_courier->add_listener('ready')
				  ->add_handler('custom')
				  ->set_js('
					reload_shipping_cost_' . $this->id . '();
				  ');

		  $div_info_shipping_wrapper = $div_shipping_type_wrapper->add_div()->add_class('row info_product');
		  $info_shipping_12 = $div_info_shipping_wrapper->add_div()->add_class('col-xs-12');
		  $info_shipping_12->add_div()->add_class('col-xs-3')->add('<p>' . clang::__('From') . '</p>');
		  $info_shipping_12->add_div()->add_class('col-xs-9')->add('<p> : ' . $city_id_seller_raja_ongkir->city_name . ', ' . $city_id_seller_raja_ongkir->province_name . '</p>');
		  $info_shipping_12->add_div()->add_class('col-xs-3')->add('<p>' . clang::__('To') . '</p>');
		  $info_shipping_12->add_div()->add_class('col-xs-9')->add('<p> : ' . $data_shipping_member->name . ', '.$data_shipping_member->address.', '.clang::__('Postal code').' '.$data_shipping_member->postal.', '. $city_id_member_raja_ongkir->city_name . ', ' . $city_id_member_raja_ongkir->province_name . '</p>');
		  $info_shipping_12->add_div()->add_class('col-xs-3')->add('<p>' . clang::__('Weight') . '</p>');
		  $info_shipping_12->add_div()->add_class('col-xs-9')->add('<p> : ' . $weight . ' gr</p>');
		  $div_shipping_type_wrapper->add_div('shipping_cost_' . $this->id)->add_class('col-xs-12 info_cost');
		  $warning_address = $div_shipping_type_wrapper->add_div()->add_class('col-xs-12 warning_address');
		  $warning_address->add(clang::__('Please make sure your shipping address is right, you can\'t change after biding. Seller will ship your product to your shipping address setting'));
		}
	  }
	}
	// end of else from member id == 0
	$html->append(parent::html($indent));
	return $html->text();
  }

  public function js($indent = 0) {
	$js = new CStringBuilder();

	$js->append(parent::js($indent));
	$js->append('
	  function reload_shipping_cost_' . $this->id . '(){
		var shipping_type=$("#shipping_type_' . $this->id . '").val();
		var origin=$("#origin_' . $this->id . '").val();
		var destination=$("#destination_' . $this->id . '").val();
		var weight=$("#weight_' . $this->id . '").val();
		var courier=$("#courier_' . $this->id . '").val();
		var service=$("#service_' . $this->id . '").val();
		$.cresenity.reload("shipping_cost_' . $this->id . '","' . curl::base() . 'raja_ongkir/get_cost","get",{"id":"' . $this->id . '","shipping_type":shipping_type,"origin":origin,"destination":destination,"weight":weight,"courier":courier,"service":service});
	  }
	');
	return $js->text();
  }

}
