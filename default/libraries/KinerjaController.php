<?php

class KinerjaController extends SixtyTwoHallFamilyController {

    public function __construct() {
        $app = CApp::instance();
        parent::__construct();
        $this->_page = "kinerja";
        $app->add_js("
                $('#type').val('lokal');
            ");
//        $link_list = $this->list_menu();
//        $product_category = product_category::get_product_category_menu($this->page());
//        
//        $build_kinerja_link_list = array();
//        $category_link_allowed = array('FSHN', 'elektronik', 'HPNTB', 'ETCRN');
//        
//        $page_link_allowed = array('GOLD','JASA','PULSA');
//        foreach ($product_category as $product_category_k => $product_category_v) {
//            $code = carr::get($product_category_v, 'code');
//            if (in_array($code, $category_link_allowed)) {
//                $build_kinerja_link_list[] = $product_category_v;
//            }
//        }
//        foreach ($link_list as $link_list_k => $link_list_v) {
//            $label = carr::get($link_list_v, 'label');
//            if (in_array($label, $page_link_allowed)) {
//                $build_kinerja_link_list[] = $link_list_v;
//            }
//        }
//        
//        $kinerja_link_list = $this->build_kinerja_link_list($build_kinerja_link_list);
//        
//        if (count($kinerja_link_list) == 0) {
//            $kinerja_link_list = $link_list;
//        }
//        
//        $this->_list_menu = $kinerja_link_list;
    }
    
    public function build_kinerja_link_list($list) {
        $return = array();
        
        foreach ($list as $list_k => $list_v) {
            $label = carr::get($list_v, 'label');
            $url = carr::get($list_v, 'url');
            $code = carr::get($list_v, 'code');
            if (strlen($code) > 0) {
                $name = carr::get($list_v, 'name');
                $tmp = explode(' ', $name);
                $name = carr::get($tmp, 0, $name);
                $label = strtoupper($name);
                $url = curl::base().'product/category/'.carr::get($list_v, 'category_id');
            }
            $arr['label'] = $label;
            $arr['url'] = $url;
            $return[] = $arr;
        }
        
        return $return;
    }

}
