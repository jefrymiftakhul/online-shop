<?php

class ProductController extends SixtyTwoHallFamilyController {

    protected $_label_deal_today;
    protected $_label_deal_weekly;
    protected $_label_deal_new;
    protected $_label_deal_best_sell;
    protected $_label_deal_pick_for_you;
    protected $_label_shinjuku_1;
    protected $_label_shinjuku_2;
    protected $_label_shinjuku_3;
    protected $_label_shinjuku_4;

    public function __construct() {
        $this->_page = 'product';
        parent::__construct();
        $app = CApp::instance();
        $db = CDatabase::instance();

        // $menu_header=cms::nav_menu('menu_header');
        //  if (count($menu_header) > 0) {
        //     foreach ($menu_header as $menu_header_k => $menu_header_v) {
        //         $this->_list_menu[]= array(
        //             'label' => $menu_header_v['menu_name'],
        //             'url' => $menu_header_v['menu_url'],
        //         );
        //     }
        // }
        $app->add_js("
                $('#type').val('product');
            ");
        $q = "
				select
					*
				from
					cms_deal_setting
				where
					status>0
			";
        $r = $db->query($q);
        if ($r->count() > 0) {
            foreach ($r as $row) {
                switch ($row->code) {
                    case 'deal_today' : {

                            $this->_label_deal_today = $row->name;
                            if (ccfg::get('deal_today')) {
                                $this->_label_deal_today = ccfg::get('deal_today');
                            }
                            break;
                        }
                    case 'deal_weekly' : {
                            $this->_label_deal_weekly = $row->name;
                            break;
                        }
                    case 'deal_new' : {
                            $this->_label_deal_new = $row->name;
                            break;
                        }
                    case 'deal_best_sell' : {
                            $this->_label_deal_best_sell = $row->name;
                            break;
                        }
                    case 'deal_pick_for_you' : {
                            $this->_label_deal_pick_for_you = $row->name;
                            break;
                        }
                    case 'shinjuku_1' : {
                            $this->_label_shinjuku_1 = $row->name;
                            break;
                        }
                    case 'shinjuku_2' : {
                            $this->_label_shinjuku_2 = $row->name;
                            break;
                        }
                    case 'shinjuku_3' : {
                            $this->_label_shinjuku_3 = $row->name;
                            break;
                        }
                    case 'shinjuku_4' : {
                            $this->_label_shinjuku_4 = $row->name;
                            break;
                        }
                }
            }
        }
    }

    public function deal_today() {
        return $this->_label_deal_today;
    }

    public function deal_weekly() {
        return $this->_label_deal_weekly;
    }

    public function deal_new() {
        return $this->_label_deal_new;
    }

    public function deal_best_sell() {
        return $this->_label_deal_best_sell;
    }

    public function deal_pick_for_you() {
        return $this->_label_deal_pick_for_you;
    }

    public function shinjuku_1() {
        return $this->_label_shinjuku_1;
    }

    public function shinjuku_2() {
        return $this->_label_shinjuku_2;
    }

    public function shinjuku_3() {
        return $this->_label_shinjuku_3;
    }

    public function shinjuku_4() {
        return $this->_label_shinjuku_4;
    }

}
