<?php

    /**
     *
     * @author Seians0077
     * @since  Aug 19, 2015
     * @license http://piposystem.com Piposystem
     */
    class Cart62hall {

        private $_session;

        protected function __construct($name) {
            $this->_session = Cart62hall_Session::instance($name);
        }

        public function get_item($index) {
            $cart = $this->_session->get('cart');
            if (isset($cart[$index])) {
                return $cart[$index];
            }
            return null;
        }

        public function delete_item($index) {
            $cart = $this->get_cart();
            if (isset($cart[$index])) {
                unset($cart[$index]);
            }
            $this->set_cart($cart);
            return $this;
        }

        public function regenerate() {
            $this->_session->regenerate();
            return $this;
        }

        public function clear_item() {
            $this->_session->delete('cart');
            return $this;
        }

        public function add_item($item) {
            $cart = $this->get_cart();
            $cart[$item['product_id']] = $item;
            $this->set_cart($cart);
            return $this;
        }

        public function update_item($index, $item) {
            $cart = $this->get_cart();
            $cart[$index] = $item;
            $this->set_cart($cart);
            return $this;
        }

        public function get_cart() {
            $cart = $this->_session->get('cart');
            if ($cart == null) $cart = array();
            return $cart;
        }

        public function set_cart($cart) {
            $this->_session->set('cart', $cart);
            return $this;
        }

        public function item_count() {
            $cart = $this->get_cart();
            return count($cart);
        }

        public function session_id() {
            return $this->_session->session_id();
        }

    }
    