<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Mar 23, 2016
     */
    class CIttronMallFormInput_Modal extends CFormInputModal {
        
        public function __construct($id = "") {
            parent::__construct($id);
            
            $this->classes[] = 'os-modal';
        }
        
        public static function factory($id = ''){
            return new CIttronMallFormInput_Modal($id);
        }
        
    }
    