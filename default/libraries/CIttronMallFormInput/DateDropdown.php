<?php

/**
 * Description of DateDropdown
 *
 * @author Ecko Santoso
 * @since 06 Nov 15
 */
class CIttronMallFormInput_DateDropdown extends CIttronMallFormInput {
    
    protected $min_age;
    protected $max_age;
    protected $max_year;
    protected $date_format;
    protected $display_format;
    protected $start_date;
    protected $end_date;

    public function __construct($id) {
        parent::__construct($id);
        $this->min_age = '';
        $this->max_age = '';
        $this->max_year = '';
        $this->date_format = 'yyyy-mm-dd';
        $this->display_format = 'dmy';  //myd, ymd
        $this->start_date = '';
        $this->end_date = '';
    }
    
    public static function factory($id) {
        return new CIttronMallFormInput_DateDropdown($id);
    }
    
    public function html($indent = 0) {
        $html = new CStringBuilder();
        
        $classes = $this->classes;
        $classes = implode(" ", $classes);
        if (strlen($classes) > 0) {
            $classes = " " . $classes;
        }
        
        
        $this->add('<input type="text" name="' . $this->name . '_" class="form-control date-dropdown ' . $classes . '" id="' . $this->id . '" value="' . $this->value . '"/>');
        
        
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent = 0) {
        $js = new CStringBuilder();
		$classes = $this->classes;
        $classes = implode(" ", $classes);
        if (strlen($classes) > 0) {
            $classes = " " . $classes;
        }
		
        $js->appendln("$('#".$this->id."').dateDropdowns({
                            daySuffixes : false,
                            wrapperClass : 'date-dropdowns " .$classes ."' ,
                            defaultDateFormat : '".$this->date_format."',
                            displayFormat : '".$this->display_format."',
                            submitFieldName : '".$this->name."',
                            required: true,
                            select2: true,
                            message: {
                                day: '" .clang::__('Day') ."',
                                month: '" .clang::__('Month') ."',
                                year: '" .clang::__('Year') ."',
                            },
                            monthShort: ['" .clang::__('Jan')."', '" .clang::__('Feb')."', '" .clang::__('Mar')."', '" .clang::__('Apr')."', 'May', 
                                        '" .clang::__('Jun')."', '" .clang::__('Jul')."', '" .clang::__('Aug')."', '" .clang::__('Sep')."',
                                        '" .clang::__('Oct')."', '" .clang::__('Nov')."', '" .clang::__('Dec')."'],
                            monthLong: ['" .clang::__('January')."', '" .clang::__('February')."',
                                '" .clang::__('March')."', '" .clang::__('April')."', '" .clang::__('May')."', 
                                '" .clang::__('June')."', '" .clang::__('July')."', '" .clang::__('August')."', 
                                '" .clang::__('September')."', '" .clang::__('October')."', '" .clang::__('November')."', 
                                '" .clang::__('December')."'],
                            dropdownClass: '" .$this->validation->validation_class() ."',
                ");
        if (strlen($this->min_age) > 0 && is_numeric($this->min_age)) {
            $js->appendln("minAge: ".$this->min_age.",");
        }
        if (strlen($this->max_age) > 0 && is_numeric($this->max_age)) {
            $js->appendln("maxAge: ".$this->max_age.",");
        }
        if (strlen($this->max_year) > 0 && is_numeric($this->max_year)) {
            $js->appendln("maxYear: ".$this->max_year.",");
        }
        if (strlen($this->start_date) > 0) {
            $js->appendln("startDate: '".$this->start_date."',");
        }
        if (strlen($this->end_date) > 0) {
            $js->appendln("endDate: '".$this->end_date."',");
        }
        if (strlen($this->value) > 0) {
            $js->appendln("defaultValue: '".$this->value."',");
        }
                            
        $js->appendln("});
                      $('.date-dropdowns select').select2({
                        dropdownCssClass: 'ost-select2', // apply css that makes the dropdown taller
                        containerCssClass : 'tpx-select2-container ost-select2',
                        minimumResultsForSearch: Infinity
                      });");
        $js->append(parent::js($indent));
        return $js->text();
    }
    
    
    
    public function set_min_age($int) {
        $this->min_age = $int;
        return $this;
    }
    
    public function set_max_age($int) {
        $this->max_age = $int;
        return $this;
    }
    
    public function set_max_year($int) {
        $this->max_year = $int;
        return $this;
    }
    
    public function set_format($string) {
        $this->date_format = $string;
        return $this;
    }
    
    public function set_display_format($string) {
        $this->display_format = $string;
        return $this;
    }
    
    public function set_start_date($start_date) {
        $this->start_date = $start_date;
        return $this;
    }
    
    public function set_end_date($end_date) {
        $this->end_date = $end_date;
        return $this;
    }
}
