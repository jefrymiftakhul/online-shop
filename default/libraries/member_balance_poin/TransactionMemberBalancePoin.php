<?php

    class TransactionMemberBalancePoin {

        public $ref_table = 'transaction';
        public $ref_pk = 'transaction_id';

        public static function balance($id) {
            $db = CDatabase::instance();
            $app = CApp::instance();
            $user = $app->user();
            $username='SYSTEM';
            $file=CF::get_file('data','setting');
            if(file_exists($file)){
                $setting=include $file;
                $transaction_poin=carr::get($setting,'transaction_poin');
                $amount_minimum=carr::get($transaction_poin,'amount_minimum');
                $poin_get=carr::get($transaction_poin,'poin_get');
                $repeat=carr::get($transaction_poin,'repeat');
            }
            if($user!=null){
                $username=$user->username;
            }
            $return = array();
            $q = "
                SELECT
                    t.ho_sell_price as sell_price,
                    t.org_id as org_id,
                    t.booking_code as code,
                    m.member_id as member_id,
                    mb.balance_idr as balance_idr 
                FROM 
                    transaction as t
                    inner join member as m on m.member_id=t.member_id
                    left join member_balance_poin as mb on mb.member_id=m.member_id
                WHERE 
                    t.transaction_id = " . $db->escape($id)."
            ";
            $data = cdbutils::get_row($q);
            $result = array();
            if ($data!=null) {
                $saldo_before=0;
                if(!$data->balance_idr==null){
                    $saldo_before=$data->balance_idr;
                }
                $qty_poin_get=floor($data->sell_price/$amount_minimum);
                if($qty_poin_get>1){
                    if($repeat!=1){
                        $qty_poin_get=1;
                    }
                }
                $total_poin_get=$qty_poin_get*$poin_get;
                $saldo=$saldo_before+$total_poin_get;
                $return['org_id'] = $data->org_id;
                $return['member_id'] = $data->member_id;
                $return['currency'] = 'IDR';
                $return['saldo_before'] = $saldo_before;
                $return['balance_in'] = $total_poin_get;
                $return['balance_out'] = 0;
                $return['saldo'] = $saldo;
                $return['createdby'] = $username;
                $return['module'] = 'Transaction';
                $return['description'] = 'Transaction ' . $data->code;
                $return['history_date'] = date('Y-m-d H:i:s');
            }
            $result[] = $return;
            return array(
                'data' => $result
            );
        }

    }
    