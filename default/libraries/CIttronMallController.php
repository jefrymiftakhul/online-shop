<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Mar 18, 2016
     */
    class CIttronMallController extends CController {

        public function __construct() {
            parent::__construct();

            Session::instance()->set('member_area', 0);
            Session::instance()->set('template', true);
            
            // this key "home_page" for render nav keep opened at C62HallElement_Navigation
            Session::instance()->set('home_page', false);

            // dont set default page if previous page already set
            // this code just for guest open first site page
            $app = CApp::instance();
            
            if (!crequest::is_ajax()) {
                $app->add_element('ittronmall-auth-login', 'login');
                $app->add_element('ittronmall-auth-register', 'register');
                $app->add_element('ittronmall-auth-forgot', 'forgot');
                $app->add_js('
                    jQuery(".os-shopping-cart").on("click", function(){
                        var state = jQuery(this).children("#cart-total-item").attr("state");
                        if (state == "default" || state == "new") {
                            $.ostore.cart.showItems("#cart-popover-container");
                        }
                    });'
                );
            }
        }

        private function send_email_transaction($recipient, $data) {
            $db = CDatabase::instance();
            $element_invoice = CFactory::create_element('62hall-invoice', 'element_invoicel');
            $element_invoice->set_booking_code($data['booking_code'])
                    ->set_recipient($recipient)
                    ->set_org_name($data['org_name'])
                    ->set_domain($data['domain'])
                    ->set_type_email($data['type'])
                    ->set_status($data['status'])
                    ->set_logo($data['logo']);

            $result = null;

            $type_email_transaction = NULL;
            if ($data['type'] == 'order') {
                $type_email_transaction = 'ORDER';
            }
            else if ($data['type'] == 'confirm') {
                $type_email_transaction = 'CONFIRM_BANK_TRANSFER';
            }
            else if ($data['type'] == 'payment') {
                $type_email_transaction = 'TRANSACTION_SUCCESS';
            }
            else if ($data['type'] == 'shipping') {
                $type_email_transaction = 'SHIPPING';
            }
            else if ($data['type'] == 'arrival') {
                $type_email_transaction = 'ARRIVAL';
            }
            else if ($data['type'] == 'request') {
                if ($data['status'] == 'ORDER') {
                    $type_email_transaction = 'REQUEST';
                }
                else if ($data['status'] == 'SUCCESS') {
                    $type_email_transaction = 'APPROVE_REQUEST';
                }
                else if ($data['status'] == 'FAILED') {
                    $type_email_transaction = 'REJECT_REQUEST';
                }
            }


            $cc = email::get_email_transaction('cc', $type_email_transaction);
            $bcc = email::get_email_transaction('bcc', $type_email_transaction);

            if ($data['type'] == 'request') {
                if ($data['status'] == 'ORDER') {
                    $query = "select
                                v.email as email_vendor
                              from transaction as t
                              inner join transaction_detail as td on td.transaction_id = t.transaction_id
                              inner join vendor as v on v.vendor_id = td.vendor_id
                              where
                                td.status > 0
                                and t.booking_code = " . $db->escape($data['booking_code']) . "
                                order by t.transaction_id desc
                            ";
                    $data_vendor = cdbutils::get_row($query);
                    if ($data_vendor) {
                        if (strlen($data_vendor->email_vendor)) {
                            $bcc[] = $data_vendor->email_vendor;
                        }
                    }
                }
            }
            try {
                $result = cmail::send_smtp($recipient, $data['subject'], $element_invoice->generate_email(), array(), $cc, $bcc);
            }
            catch (Exception $e) {
                
            }


            return $result;
        }

        public function send_transaction($type, $booking_code, $status = NULL) {
            $db = CDatabase::instance();
            $transaction_contact = transaction::get_transaction_contact('booking_code', $booking_code);
            if (count($transaction_contact->shipping_email) > 0) {
                //$email = 'riza.armanda@yahoo.co.id';
                $email = $transaction_contact->shipping_email;
                //$org_name = !empty($data_org->name) ? $data_org->name : NULL;

                $org = org::get_info();
                $org_code = carr::get($org, 'code');
                $item_image = carr::get($org, 'item_image');
                $org_name = carr::get($org, 'name');
                $domain = carr::get($org, 'domain');
                
                $this->send_email_transaction($email, array(
                    'logo' => curl::httpbase() . 'application/admin62hallfamily/' .$org_code  . '/upload/logo/item_image/' . $item_image,
                    'subject' => $org_name . ' - Order ' . $type . ' - ' . $status . ' - ' . $booking_code,
                    'org_name' => $org_name,
                    'domain' => $domain,
                    'type' => $type,
                    'booking_code' => $booking_code,
                    'status' => $status,
                        )
                );
                return true;
            }
            else {
                throw new Exception('Resend verification email failed.');
            }
        }

    }
    