<?php

    class BidMemberBalance {

        public $ref_table = 'transaction_bid';
        public $ref_pk = 'transaction_bid_id';
        protected $username;
        
        public function __construct() {
            $app = CApp::instance();
            $user = $app->user();
            if($user){
                $this->username=$user->username;
            }
        }
        
        
        
        public function balance($id) {
            $db = CDatabase::instance();
            $result = array();

            $q="
                select
                    p.name as product,
                    tb.vendor_sell_price as bid,
                    tb.org_id as org_id,
                    tb.shipping_price as shipping_price,
                    m.member_id as member_id,
                    m.name as member,
                    mb.balance_idr as balance_idr
                from
                    transaction_bid as tb
                    inner join product as p on p.product_id=tb.product_id
                    inner join member as m on m.member_id=tb.member_id
                    left join member_balance as mb on mb.member_id=m.member_id
                where
                    tb.product_id=".$db->escape($id)."
                order by
                    tb.transaction_date desc
            ";
            $r=$db->query($q);
            if ($r->count()>0) {
                $bider=$r[0];
                $username=$this->username;
                if(($username==null)||(strlen($username)==0)){
                    $username=$bider->member;
                }
                $saldo_before=0;
                if(!$bider->balance_idr==null){
                    $saldo_before=$bider->balance_idr;
                }
                $saldo=$saldo_before-($bider->bid+$bider->shipping_price);
                $data['org_id'] = $bider->org_id;
                $data['member_id'] = $bider->member_id;
                $data['currency'] = 'IDR';
                $data['saldo_before'] = $saldo_before;
                $data['balance_in'] = 0;
                $data['balance_out'] = $bider->bid+$bider->shipping_price;
                $data['saldo'] = $saldo;
                $data['createdby'] = $username;
                $data['module'] = 'Bid';
                $data['description'] = 'Bid For Product '.$bider->product;
                $data['history_date'] = date('Y-m-d H:i:s');
                $result[]=$data;
                if(isset($r[1])){
                    $last_bider=$r[1];
                    if($bider->member_id==$last_bider->member_id){
                        $saldo_before=$saldo;
                    }else{
                        $saldo_before=0;
                        if(!$last_bider->balance_idr==null){
                            $saldo_before=$last_bider->balance_idr;
                        }                    
                    }
                    $saldo=$saldo_before+($last_bider->bid+$last_bider->shipping_price);
                    $data['org_id'] = $last_bider->org_id;
                    $data['member_id'] = $last_bider->member_id;
                    $data['currency'] = 'IDR';
                    $data['saldo_before'] = $saldo_before;
                    $data['balance_in'] = $last_bider->bid+$last_bider->shipping_price;
                    $data['balance_out'] = 0;
                    $data['saldo'] = $saldo;
                    $data['createdby'] = $username;
                    $data['module'] = 'Refund Bid';
                    $data['description'] = 'Refund Bid For Product '.$last_bider->product;
                    $data['history_date'] = date('Y-m-d H:i:s');
                    $result[]=$data;
                }
            }
            return array(
                'data' => $result
            );
        }
                
    }
    