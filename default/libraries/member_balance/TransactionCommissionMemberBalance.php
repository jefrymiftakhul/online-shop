<?php

    class TransactionCommissionMemberBalance {

        public $ref_table = 'transaction';
        public $ref_pk = 'transaction_id';

        public function __construct($balance=null) {
            
        }
        
        public function balance($id) {
            $db = CDatabase::instance();
            $app = CApp::instance();
            $user = $app->user();
            $username='SYSTEM';
            if($user!=null){
                $username=$user->username;
            }
            
            $result = array();
            $q = '
                select
                    m.member_id as member_id,
                    m.org_id as org_id,
                    mb.balance_idr as balance_idr,
                    tmc.commission_value as commission_value,
                    t.booking_code as booking_code
                from 
                    transaction_member_category as tmc
                    inner join member as m on m.member_id=tmc.member_id
                    inner join transaction as t on t.transaction_id=tmc.transaction_id
                    left join member_balance as mb on mb.member_id=m.member_id
                where tmc.status > 0 and t.transaction_id = '.$db->escape($id).' 
                ';
            $r = $db->query($q);
            if($r->count()>0){
                foreach ( $r as $data ) {
                    $member_id = cobj::get($data, 'member_id');
                    $return = array();
                    $saldo_before=0;
                    if(!$data->balance_idr==null){
                        $saldo_before=$data->balance_idr;
                    }
                    $nominal = $data->commission_value;
                    $saldo = $saldo_before + $nominal;
                    $return['org_id'] = $data->org_id;
                    $return['member_id'] = $data->member_id;
                    $return['currency'] = 'IDR';
                    $return['saldo_before'] = $saldo_before;
                    $return['balance_in'] = $nominal;
                    $return['balance_out'] = 0;
                    $return['saldo'] = $saldo;
                    $return['createdby'] = $username;
                    $return['module'] = 'Transaction Commission';
                    $return['description'] = 'Commission for Transaction '.cobj::get($data, 'booking_code');
                    $return['history_date'] = date('Y-m-d H:i:s');

                    if($nominal != 0){
                        $result[] = $return;
                    }
                }
            }
            
            return array(
                'data' => $result
            );
        }

    }
    