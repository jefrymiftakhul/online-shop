<?php

    class ShippingMemberBalance {

        public $ref_table = 'transaction';
        public $ref_pk = 'transaction_id';
        protected $username;
        
        public function __construct() {
            $app = CApp::instance();
            $user = $app->user();
            if($user){
                $this->username=$user->username;
            }
        }
        
        
        
        public function balance($id) {
            $db = CDatabase::instance();
            $result = array();

            $q="
                select
                    p.name as product,
                    t.org_id as org_id,
                    t.shipping_price as shipping_price,
                    m.member_id as member_id,
                    m.name as member,
                    mb.balance_idr as balance_idr
                from
                    transaction as t
                    inner join transaction_detail as td on td.transaction_id=t.transaction_id
                    inner join product as p on p.product_id=td.product_id
                    inner join member as m on m.member_id=tb.member_id
                    left join member_balance as mb on mb.member_id=m.member_id
                where
                    t.transaction_id=".$db->escape($id)."
            ";
            $r=$db->query($q);
            if ($r->count()>0) {
                $bider=$r[0];
                $username=$this->username;
                if(($username==null)||(strlen($username)==0)){
                    $username=$bider->member;
                }
                $saldo_before=0;
                if(!$bider->balance_idr==null){
                    $saldo_before=$bider->balance_idr;
                }
                $saldo=$saldo_before-($bider->shipping_price);
                $data['org_id'] = $bider->org_id;
                $data['member_id'] = $bider->member_id;
                $data['currency'] = 'IDR';
                $data['saldo_before'] = $saldo_before;
                $data['balance_in'] = 0;
                $data['balance_out'] = $bider->shipping_price;
                $data['saldo'] = $saldo;
                $data['createdby'] = $username;
                $data['module'] = 'Shipping Price';
                $data['description'] = 'Shipping Price For Product '.$bider->product;
                $data['history_date'] = date('Y-m-d H:i:s');
                $result[]=$data;
            }
            return array(
                'data' => $result
            );
        }
                
    }
    