<?php

    class Commissiontransactiontrevo2MemberBalance {

        public $ref_table = 'transaction';
        public $ref_pk = 'transaction_id';
        protected static $commission_share_member;

        public function __construct($balance = null) {

            $file = CF::get_file('data', 'setting');
            if (file_exists($file)) {
                $setting = include $file;
                $this->commission_share_member = carr::get($setting, 'commission_share_member');
            }
        }

        public function set_nominal($val) {
            $this->commission_share_member = $val;
            return $this;
        }

        public function balance($id) {
            $db = CDatabase::instance();
            $app = CApp::instance();
            $user = $app->user();
            $username = 'SYSTEM';
            if ($user != null) {
                $username = $user->username;
            }

            $result = array();
            $q = '
                select t.code, t.transaction_id, t.member_id, td.trevo_driver_id, td.member_id member_register
                from transaction t
                left join transaction_trevo tt on tt.transaction_id = t.transaction_id
                left join trevo_driver td on td.trevo_driver_id = tt.trevo_driver_id
                where t.status > 0 and t.transaction_id = ' . $db->escape($id);
            $row = cdbutils::get_row($q);
            if ($row != null) {
                $transaction_code = cobj::get($row, 'code');
                $transaction_id = cobj::get($row, 'transaction_id');
                $member_id = cobj::get($row, 'member_id');
                $driver_id = cobj::get($row, 'trevo_driver_id');
                $member_register = cobj::get($row, 'member_register');                                
                $result = $this->insert_commission($member_register, $transaction_code);
            }

            return array(
                'data' => $result
            );
        }

        private function insert_commission($id, $transaction_code) {
            $db = CDatabase::instance();
            $app = CApp::instance();
            $user = $app->user();
            $username = 'SYSTEM';
            if ($user != null) {
                $username = $user->username;
            }
            $result = array();

            $data_member = member::get_commission_member_category($id);            
            if (count($data_member) > 0) {
                foreach ($data_member as $key => $val) {                    
                    $return = array();
                    $member_category_id = carr::get($val, 'member_category_id');
                    $commission_percent = carr::get($val, 'commission_percent');
                    $member_id = carr::get($val, 'member_id');

                    $q = '
                            select 
                                m.*
                                ,mb.balance_idr as balance_idr 
                                ,mc.name as member_category_name  
                            from member m 
                            left join member_balance mb on m.member_id = mb.member_id 
                            left join member_category mc on m.member_category_id = mc.member_category_id 
                            where
                                m.is_active = 1 
                                and m.status > 0 
                                and m.member_id = ' . $db->escape($member_id) . '
                            ';

                    $data = cdbutils::get_row($q);

                    if ($data != null) {
                        $saldo_before = 0;
                        if (!$data->balance_idr == null) {
                            $saldo_before = $data->balance_idr;
                        }
                        $nominal = ($commission_percent / 100) * $this->commission_share_member;
                        $saldo = $saldo_before + $nominal;
                        $return['org_id'] = $data->org_id;
                        $return['member_id'] = $data->member_id;
                        $return['currency'] = 'IDR';
                        $return['saldo_before'] = $saldo_before;
                        $return['balance_in'] = $nominal;
                        $return['balance_out'] = 0;
                        $return['saldo'] = $saldo;
                        $return['createdby'] = $username;
                        $return['module'] = 'Commission transaction trevo';
                        $return['description'] = 'Commission transaction trevo (' . $transaction_code. ')';
                        $return['history_date'] = date('Y-m-d H:i:s');
                    }

                    if ($nominal != 0) {
                        $result[] = $return;
                    }
                }
            }
            return $result;            
        }

    }
    