<?php

/*

 */

class CApiClientEngine_PaymentGatewayEngine extends CApiClientEngine {

    protected function __construct($api) {
        parent::__construct($api);
    }

    public static function factory($api) {
        return new CApiClientEngine_PaymentGatewayEngine($api);
    }
        
    public function check_price($arr_item, $transaction_type = 'buy') {
        $db = CDatabase::instance();
        $total_vendor_nta = 0;
        $total_vendor_sell_price = 0;
        $total_vendor_commission_value = 0;
        $total_ho_upselling = 0;
        $total_ho_sell_price = 0;
        $total_channel_commission_full = 0;
        $total_channel_commision_ho = 0;
        $total_promo_amount = 0;
        $total_channel_commission_share = 0;
        $total_channel_commission_share_ho = 0;
        $total_channel_commission_value = 0;
        $total_channel_updown = 0;
        $total_channel_sell_price = 0;
        $total_channel_profit = 0;
        $total_weight = 0;
        $check_price = array();
        $check_price['err_code'] = 0;
        $check_price['err_message'] = '';
        foreach ($arr_item as $key => $val) {
            if ($check_price['err_code'] == 0) {
                $item = product::get_product($val['item_id']);
                if(empty($item['product_type_id'])){                    
                    $check_price['err_code'] = 3002;
                    $check_price['err_message'] = 'error on check price';
                    break;
                }                
                $item['qty'] = $val['qty'];
//                $item['weight'] = carr::get($val,'weight')*$val['qty'];
                $total_weight += carr::get($item,'weight',0)*$val['qty'];
                if (count($item) > 0) {
                    if($item['product_type_id']==7||$item['product_type_id']==8){
                        $detail_price = $item['detail_price'];
                        $pulsa_price=$val['price']-$detail_price['ho_upselling'];
                        $db->update('product',array('sell_price_nta'=>$pulsa_price,'sell_price'=>$pulsa_price),array('product_id'=>$val['item_id']));
                        $item = product::get_product($val['item_id']);
                        $item['qty'] = $val['qty'];
                    }
                    $detail_price = $item['detail_price'];
                    $channel_price = $detail_price['channel_sell_price'];
                    $data_wholesaler = product::get_wholesales($val['item_id']);
                    
                    if(count($data_wholesaler) > 0){
                      $price_grosir = product::get_wholesales_price($data_wholesaler,$val['qty']);
                      if($price_grosir != NULL){
                          $channel_price = $price_grosir;
                      }
                    }
                    
                    if ($transaction_type == 'sell') {
                        $channel_price = $detail_price['channel_purchase_price'];
                    }
                    if ($channel_price == $val['price'] or $item['product_type_id']==7) {
                        //gold
                        if ($transaction_type == 'sell') {
                            $total_vendor_nta+=$detail_price['vendor_purchase_nta'] * $val['qty'];
                            $total_vendor_sell_price+=$detail_price['vendor_purchase_price'] * $val['qty'];
                            $total_vendor_commission_value+=$detail_price['vendor_purchase_commission_value'] * $val['qty'];
                            $total_ho_upselling+=0;
                            $total_ho_sell_price+=$detail_price['ho_purchase_price'] * $val['qty'];
                            $total_channel_commission_full+=$detail_price['channel_purchase_commission_full'] * $val['qty'];
                            $total_channel_commision_ho+=0;
                            $total_promo_amount+=0;
                            $total_channel_commission_share+=$detail_price['channel_purchase_commission_share'] * $val['qty'];
                            $total_channel_commission_share_ho+=$detail_price['channel_purchase_commission_share_ho'] * $val['qty'];
                            $total_channel_commission_value+=$detail_price['channel_purchase_commission_value'] * $val['qty'];
                            $total_channel_updown+=0;
                            $total_channel_sell_price+=$detail_price['channel_purchase_price'] * $val['qty'];
                            $total_channel_profit+=$detail_price['channel_purchase_profit'] * $val['qty'];
                        }
                        // selain gold
                        else {
                            $total_vendor_nta+=$detail_price['vendor_nta'] * $val['qty'];
                            $total_vendor_sell_price+=$detail_price['vendor_sell_price'] * $val['qty'];
                            $total_vendor_commission_value+=$detail_price['vendor_commission_value'] * $val['qty'];
                            $total_ho_upselling+=$detail_price['ho_upselling'] * $val['qty'];
                            $total_ho_sell_price+=$detail_price['ho_sell_price'] * $val['qty'];
                            $total_channel_commission_full+=$detail_price['channel_commission_full'] * $val['qty'];
                            $total_channel_commision_ho+=$detail_price['channel_commission_ho'] * $val['qty'];
                            $total_promo_amount+=$detail_price['promo_amount'] * $val['qty'];
                            $total_channel_commission_share+=$detail_price['channel_commission_share'] * $val['qty'];
                            $total_channel_commission_share_ho+=$detail_price['channel_commission_share_ho'] * $val['qty'];
                            $total_channel_commission_value+=$detail_price['channel_commission_value'] * $val['qty'];
                            $total_channel_updown+=$detail_price['channel_updown'] * $val['qty'];
                            $total_channel_sell_price+=$detail_price['channel_sell_price'] * $val['qty'];
                            $total_channel_profit+=$detail_price['channel_profit'] * $val['qty'];
                        }
                        $check_price['item'][] = $item;
                    } else {
                        $check_price['err_code'] = 102;
                        $check_price['err_message'] = 'price for ' . $item['name'] . ' not match';
                    }
                } else {
                    $check_price['err_code'] = 101;
                    $check_price['err_message'] = 'item not found';
                }
            }
        }
        $check_price['total'] = array(
            'total_weight' => $total_weight,
            'total_vendor_nta' => $total_vendor_nta,
            'total_vendor_sell_price' => $total_vendor_sell_price,
            'total_vendor_commission_value' => $total_vendor_commission_value,
            'total_ho_upselling' => $total_ho_upselling,
            'total_ho_sell_price' => $total_ho_sell_price,
            'total_channel_commission_full' => $total_channel_commission_full,
            'total_channel_commission_ho' => $total_channel_commision_ho,
            'total_promo_amount' => $total_promo_amount,
            'total_channel_commission_share' => $total_channel_commission_share,
            'total_channel_commission_share_ho' => $total_channel_commission_share_ho,
            'total_channel_commission_value' => $total_channel_commission_value,
            'total_channel_updown' => $total_channel_updown,
            'total_channel_sell_price' => $total_channel_sell_price,
            'total_channel_profit' => $total_channel_profit,
        );
        return $check_price;
    }

    public function check_stock($arr_item, $transaction_type = 'buy') {
        $db = CDatabase::instance();
        $check_stock = array();
        $check_stock['err_code'] = 0;
        $check_stock['err_message'] = '';
        foreach ($arr_item as $key => $val) {
            if ($check_stock['err_code'] == 0) {
                $item = product::get_product($val['item_id']);
                if (count($item) > 0) {
                    $stock['stock_db'] = $item['stock'];
                    $stock['qty'] = $val['qty'];
                    $check_stock['item'] = $item;
                    if ($item['stock'] < $val['qty']) {
                        $check_stock['err_code'] = 201;
                        $check_stock['err_message'] = 'stock for ' . $item['name'] . ' not enough';
                    }
                } else {
                    $check_stock['err_code'] = 101;
                    $check_stock['err_message'] = 'item not found';
                }
            }
        }
        return $check_stock;
    }

    public function login($request) {
        $api_url = $this->api->api_url();
        $err_code = 0;
        $err_message = "";
        $data = array();

        if ($err_code == 0) {
            if (strlen($api_url) == 0) {
                $err_code++;
                $err_message = "API URL EMPTY";
            }
        }
        $payment_type = carr::get($request, 'payment_type');
        if ($payment_type == null) {
            $err_code++;
            $err_message = 'Request Payment Type Not Found ';
        }

        if ($err_code == 0) {
            $post_data = array();
            $post_data['auth_id'] = $this->api->api_auth();
            $post_data['product_code'] = $this->api->product_code();
            $post_data['payment_type'] = $request['payment_type'];
            $service_name = 'Login';

            $url = $api_url . 'pg/' . $service_name;
            $response = $this->__request($service_name, $url, $post_data);
            $response_data = cjson::decode($response);
            if (!is_array($response_data) || empty($response_data)) {
                $err_code++;
                $err_message = 'Parse Error ' . ($response);
            }
        }
        if ($err_code == 0) {
            $err_code = carr::get($response_data, 'err_code');
            $err_message = carr::get($response_data, 'err_message', '');
            if (isset($response_data['session_id'])) {
                $this->session()->set('api_session_id', $response_data['session_id']);
                $this->session()->set('payment_type', $request['payment_type']);
                $this->session()->set('product_code', $this->api->product_code());
            } else {
                $err_code++;
                $err_message = 'session not found';
            }
        }
        if ($err_code > 0) {
            $this->error()->add($err_message, $err_code);
        }
        return $data;
    }

    public function get_payment_charge($request) {
        $api_url = $this->api->api_url();

        $err_code = 0;
        $err_message = "";
        $data = array();

        if ($err_code == 0) {
            if (strlen($api_url) == 0) {
                $err_code++;
                $err_message = "API URL EMPTY";
            }
        }
        if ($err_code == 0) {
            $post_data = array();

            $post_data['session_id'] = $this->api->api_session_id();
            $post_data['product_code'] = $this->api->product_code();
            $post_data['payment_type'] = $this->session()->get('payment_type');
            $post_data['currency_code'] = $request['currency_code'];
            $post_data['amount'] = $request['amount'];
            $service_name = 'GetPaymentCharge';

            $url = $api_url . 'pg/' . $service_name;
            $response = $this->__request($service_name, $url, $post_data);
            $response_data = cjson::decode($response);
            if (!is_array($response_data) || empty($response_data)) {
                $err_code++;
                $err_message = 'Parse Error ' . ($response);
            }
        }
        if ($err_code == 0) {
            $err_code = carr::get($response_data, 'err_code');
            $err_message = carr::get($response_data, 'err_message', '');
            $data = carr::get($response_data, 'data');
            $this->session()->set('currency_code', $request['currency_code']);
            $total_charge = 0;
            if ($data) {
                foreach ($data['detail'] as $key => $val) {
                    $total_charge+=$val['amount'];
                }
            } else {
                $err_code++;
            }
            $total_amount_pay = $request['amount'] + $total_charge;
            $this->session()->set('detail_charge', $data['detail']);
            $this->session()->set('total_amount_charge', $total_charge);
            $this->session()->set('total_amount_payment', $total_amount_pay);
        }
        if ($err_code > 0) {
            $this->error()->add($err_message, $err_code);
        }
        return $data;
    }

    public function payment($request) {
        $api_url = $this->api->api_url();
        $session_app = Session::instance();
        $err_code = 0;
        $err_message = "";
        $data = array();
        $session_transaction = $this->session()->get('transaction');
        $api=carr::get($session_transaction,'api');
        if ($err_code == 0) {
            if (strlen($api_url) == 0) {
                $err_code++;
                $err_message = "API URL EMPTY";
            }
        }
        if($err_code==0){
            if($this->session()->get('confirm')==0){
                if(ccfg::get('transaction_member_only')>0){
                    $member_id = $session_app->get('member_id');
                    if($member_id == null && strlen($member_id)==0){
                        $err_code++;
                        $err_message = clang::__("Harap login member sebelum melakukan transaksi");
                    }
                }
            }
        }
        if ($err_code == 0) {
            if ($this->session()->get('payment_type') != 'bank_transfer') {
                if ($this->session()->get('request') != '1') {
                    if($api==null){
                        $items = carr::get($session_transaction, 'item');
                        $check_price = $this->check_price($items, $session_transaction['type']);
                        $this->session()->set('check_price_before_payment', $check_price);
                        $err_code = $check_price['err_code'];
                        $err_message = $check_price['err_message'];
                    }
                }
            }
        }
        if ($err_code == 0) {
            if ($this->session()->get('payment_type') != 'bank_transfer') {
                if ($this->session()->get('request') != '1') {
                    if ($session_transaction['page'] != 'pulsa') {
                        $type = carr::get($session_transaction, 'type');
                        if($api==null){
                            if ($type != 'sell') {
                                $items = carr::get($session_transaction, 'item');
                                $check_stock = $this->check_stock($items, $session_transaction['type']);
                                $this->session()->set('check_stock_before_payment', $check_stock);
                                $err_code = $check_stock['err_code'];
                                $err_message = $check_stock['err_message'];
                            }
                        }
                    }
                }
            }
        }
        if($err_code==0){
            if($api!=null){
                $items = carr::get($session_transaction, 'item',array());
                $check_price=array();
                $total_vendor_nta = 0;
                $total_vendor_sell_price = 0;
                $total_vendor_commission_value = 0;
                $total_ho_upselling = 0;
                $total_ho_sell_price = 0;
                $total_channel_commission_full = 0;
                $total_channel_commision_ho = 0;
                $total_promo_amount = 0;
                $total_channel_commission_share = 0;
                $total_channel_commission_share_ho = 0;
                $total_channel_commission_value = 0;
                $total_channel_updown = 0;
                $total_channel_sell_price = 0;
                $total_channel_profit = 0;
                if(count($items)>0){
                    foreach($items as $item){
                        $qty=carr::get($item,'qty');
                        $arr_item=array(
                            "vendor_id"=>null,
                            "product_id"=>null,
                            "product_name"=>carr::get($item,'item_name'),
                            "qty"=>$qty,
                        );
                        $session_item_before_payment_detail_price['vendor_nta']=$item['price'];
                        $session_item_before_payment_detail_price['vendor_commission_value']=0;
                        $session_item_before_payment_detail_price['vendor_sell_price']=$item['price'];
                        $session_item_before_payment_detail_price['promo_amount']=0;
                        $session_item_before_payment_detail_price['ho_upselling']=0;
                        $session_item_before_payment_detail_price['ho_sell_price']=$item['price'];
                        $session_item_before_payment_detail_price['channel_commission_full']=0;
                        $session_item_before_payment_detail_price['channel_commission_ho']=0;
                        $session_item_before_payment_detail_price['channel_commission_share']=0;
                        $session_item_before_payment_detail_price['channel_commission_share_ho']=0;
                        $session_item_before_payment_detail_price['channel_commission_value']=0;
                        $session_item_before_payment_detail_price['channel_updown']=0;
                        $session_item_before_payment_detail_price['channel_sell_price']=$item['price'];
                        $session_item_before_payment_detail_price['channel_profit']=0;
                        $arr_item['detail_price']=$session_item_before_payment_detail_price;
                        $check_price['item'][]=$arr_item;
                        
                        $total_vendor_nta+=$session_item_before_payment_detail_price['vendor_nta'] * $qty;
                        $total_vendor_sell_price+=$session_item_before_payment_detail_price['vendor_sell_price'] * $qty;
                        $total_vendor_commission_value+=$session_item_before_payment_detail_price['vendor_commission_value'] * $qty;
                        $total_ho_upselling+=0;
                        $total_ho_sell_price+=$session_item_before_payment_detail_price['ho_sell_price'] * $qty;
                        $total_channel_commission_full+=$session_item_before_payment_detail_price['channel_commission_full'] * $qty;
                        $total_channel_commision_ho+=0;
                        $total_promo_amount+=0;
                        $total_channel_commission_share+=$session_item_before_payment_detail_price['channel_commission_share'] * $qty;
                        $total_channel_commission_share_ho+=$session_item_before_payment_detail_price['channel_commission_share_ho'] * $qty;
                        $total_channel_commission_value+=$session_item_before_payment_detail_price['channel_commission_value'] * $qty;
                        $total_channel_updown+=0;
                        $total_channel_sell_price+=$session_item_before_payment_detail_price['channel_sell_price'] * $qty;
                        $total_channel_profit+=$session_item_before_payment_detail_price['channel_profit'] * $qty;
                    }
                }else{
                    $err_code++;
                    $err_message="No item";
                }
                $check_price['total'] = array(
                    'total_vendor_nta' => $total_vendor_nta,
                    'total_vendor_sell_price' => $total_vendor_sell_price,
                    'total_vendor_commission_value' => $total_vendor_commission_value,
                    'total_ho_upselling' => $total_ho_upselling,
                    'total_ho_sell_price' => $total_ho_sell_price,
                    'total_channel_commission_full' => $total_channel_commission_full,
                    'total_channel_commission_ho' => $total_channel_commision_ho,
                    'total_promo_amount' => $total_promo_amount,
                    'total_channel_commission_share' => $total_channel_commission_share,
                    'total_channel_commission_share_ho' => $total_channel_commission_share_ho,
                    'total_channel_commission_value' => $total_channel_commission_value,
                    'total_channel_updown' => $total_channel_updown,
                    'total_channel_sell_price' => $total_channel_sell_price,
                    'total_channel_profit' => $total_channel_profit,
                );   
                $this->session()->set('check_price_before_payment', $check_price);
            }
        }
        if ($err_code == 0) {
            $session_contact = carr::get($session_transaction, 'contact_detail');
            $session_contact_shipping = carr::get($session_contact, 'shipping_address');
            $city_id = carr::get($session_contact_shipping, 'city_id');
            $items = carr::get($session_transaction, 'item');
            $arr_item = array();
            foreach ($items as $key => $val) {
                $arr_item[] = $val['item_id'];
            }
            $check_shipping_city = shipping::check_product_available_shipping_city($arr_item, $city_id);
            $err_code = $check_shipping_city['err_code'];
            $err_message = $check_shipping_city['err_message'];
        }

        if ($err_code == 0) {
            $before_payment = $this->before_payment();
            $err_code = $before_payment['err_code'];
            $err_message = $before_payment['err_message'];
        }
        if ($err_code == 0) {
            $back_url = $request['back_url'] . '/' . $this->session()->get('transaction_code');
            $post_data = array();
			$contact_detail=carr::get($session_transaction,'contact_detail');
			$billing_address=carr::get($contact_detail,'billing_address');
			if($billing_address){
				$billing_address['state']=carr::get($billing_address,'province');
				$contact_detail['billing_address']=$billing_address;
			}
			$shipping_address=carr::get($contact_detail,'shipping_address');
			if($billing_address){
				$shipping_address['state']=carr::get($shipping_address,'province');
				$contact_detail['shipping_address']=$shipping_address;
			}
            $post_data['session_id'] = $this->api->api_session_id();
            $post_data['product_code'] = $this->api->product_code();
            $post_data['payment_type'] = $this->session()->get('payment_type');
            $post_data['currency_code'] = $this->session()->get('currency_code');
            $post_data['back_url'] = $back_url;
            $post_data['amount'] = $session_transaction['amount_payment'];
            $post_data['items'] = $session_transaction['item_payment'];
            $post_data['customer_details'] = $contact_detail;
            if ($this->session()->get('payment_type') == 'bank_transfer') {
                $post_data['payment_info'] = $request['payment_info'];
            }
            $service_name = 'Payment';

            $url = $api_url . 'pg/' . $service_name;
            $response = $this->__request($service_name, $url, $post_data);
            $response_data = cjson::decode($response);
            if (!is_array($response_data) || empty($response_data)) {
                $err_code++;
                $err_message = 'Parse Error ' . ($response);
            }
        }
        if ($err_code == 0) {
            $err_code = carr::get($response_data, 'err_code');
            $err_message = carr::get($response_data, 'err_message', '');
            $data = carr::get($response_data, 'data');
            $this->session()->set('payment_code', $data['payment_code']);
        }
        if ($err_code == 0) {
            $update_code = $this->update_payment_code();
            $err_code = $update_code['err_code'];
            $err_message = $update_code['err_message'];
        }
        if ($err_code > 0) {
            $this->error()->add($err_message, $err_code);
        }
        if(count($data)==0){
            $data['err_code']=$err_code;
            $data['err_message']=$err_message;
        }
        return $data;
    }

    public function before_payment() {
        $db = CDatabase::instance();
        $err_code = 0;
        $err_message = '';
        $err_api_pulsa = 0;
        $transaction_id = '';
        $winner_transaction_id=null;
        $session_app = Session::instance();
        $member_id = $session_app->get('member_id');
        $is_package_product = $session_app->get('is_package_product');
//        cdbg::var_dump($is_package_product);die();
        $org_id = CF::org_id();
        if ($org_id) {
            $org = org::get_org($org_id);
        } else {
            $err_code++;
            $err_message = 'Data Merchant not Valid';
        }
        $file=CF::get_file('data','setting');
        $transaction_poin=array();
        $poin_payment_not_in=array();
        if(file_exists($file)){
            $setting=include $file;
            $transaction_poin=carr::get($setting,'transaction_poin');
            $poin_payment_not_in=carr::get($transaction_poin,'not_payment_type');
            $poin_product_not_in=carr::get($transaction_poin,'not_product_type');
        }
        $request = $this->session()->get('request');

        $session_transaction = $this->session()->get('transaction');
        $item = carr::get($session_transaction, 'item');
        $session_voucher = $session_app->get('voucher_data');
        $total_harga_diskon=0;
        $product_voucher_id = null;
        $requota = false;
        if($session_voucher != false && count($session_voucher) > 0){
            $type = $session_voucher['type'];
            $nominal_voucher = $session_voucher['nominal'];
            $total_harga = $session_transaction['total_item'];
            $product_voucher_id = $session_voucher['product_voucher_id'];
            if ($type == 'percent'){
                $total_harga_diskon = $total_harga * ($nominal_voucher/100);
            }
            else{
                $total_harga_diskon = $nominal_voucher;
            }   
            $total_harga = $total_harga - $total_harga_diskon;
            $requota = true;
        }
        
        $session_detail_charge = $this->session()->get('detail_charge', array());
        $session_item_before_payment = $this->session()->get('check_price_before_payment');
        $session_item_before_payment_total = carr::get($session_item_before_payment, 'total');
        $session_contact = carr::get($session_transaction, 'contact_detail');
        $session_contact_billing = carr::get($session_contact, 'billing_address');
        $session_contact_shipping = carr::get($session_contact, 'shipping_address');
        $session_data_get_payment_charge = $this->session()->get('api_data_get_payment_charge');
        $session_data_get_payment_charge_data = carr::get($session_data_get_payment_charge, 'data');
        $session_data_get_payment_charge_data_detail = carr::get($session_data_get_payment_charge_data, 'detail');
        $api_session_id = $this->session()->get('api_session_id');
        $transaction_type = carr::get($session_transaction, 'type');		
        $transaction_from = $this->session()->get('transaction_from');		
        $pulsa_phone_number = carr::get($session_transaction, 'phone_number');	
        
        try {
            $db->begin();
            $total_deal = 0;
            $insert_transaction = 1;
            $insert_transaction_payment = 1;
            $insert_transaction_member_category = 1;
            $insert_transaction_payment_session = 1;
            $stock_process = 1;
            $have_transaction_member_category=0;
            $api=carr::get($session_transaction,'api');
            if($api!=null){
                $stock_process=0;
            }
            
            if ($session_transaction['page']=='pulsa'){
                $stock_process=0;
            }
            if ($transaction_type == 'sell') {
                $stock_process = 0;
                $total_deal = carr::get($session_transaction, 'amount_payment', 0);
            }
            if (($this->session()->get('payment_type') == 'bank_transfer') && ($this->session()->get('confirm') == 1)) {
                $insert_transaction = 0;
                $insert_transaction_payment = 0;
                $insert_transaction_member_category = 0;
                $transaction_id = $this->session()->get('transaction_id');
                $transaction_payment_id = $this->session()->get('transaction_payment_id');
                $stock_process = 0;
            }
            if ($this->session()->get('request') == '1') {
                $insert_transaction = 0;
                $stock_process = 0;
                $insert_transaction_member_category = 0;
                $transaction_id = $this->session()->get('transaction_id');
            }
            
            if ($this->session()->get('reset_payment') == '1') {
                $insert_transaction = 0;
                $insert_transaction_member_category=0;
                $insert_transaction_payment = 1;
                $insert_transaction_payment_session = 1;
                $stock_process = 0;
                $transaction_id = $this->session()->get('transaction_id');
                $db->update('transaction_payment',array('status'=>'0'),array('transaction_id'=>$transaction_id));
            }
            if ($insert_transaction > 0) {
                //insert transaction
                $transaction_code_generate = generate_code::get_next_transaction_code();
                $hold_hour=ccfg::get('hold_hour');
                if($hold_hour==null){
                    $hold_hour=6;
                }
                $date_hold = date("Y-m-d H:i:s", strtotime("+".$hold_hour." hours", strtotime(date('Y-m-d H:i:s'))));

                $shipping_price_kg=0;                
                if($session_item_before_payment_total['total_weight']>0){
                    $shipping_price_kg=$session_transaction['total_shipping']/ceil($session_item_before_payment_total['total_weight']/1000);
                }
                $total_shipping_real_weight=$shipping_price_kg*$session_item_before_payment_total['total_weight']/1000;

                $data_transaction = array(
                    'org_id' => $org_id,
                    'org_parent_id' => $org->parent_id,
                    'member_id' => $member_id,
                    'session_id' => $this->session()->session_id(),
                    'api_session_id' => $api_session_id,
                    'code' => $transaction_code_generate,
                    'email' => $session_contact_billing['email'],
                    'date' => date('Y-m-d H:i:s'),
                    'date_hold' => $date_hold,
                    'type' => $session_transaction['type'],
                    'product_type' => $session_transaction['page'],
                    'transaction_status' => 'PENDING',
                    'order_status' => 'PENDING',
                    'payment_status' => 'PENDING',
                    'shipping_status' => 'PENDING',
                    'pulsa_status' => 'PENDING',
                    'vendor_nta' => $session_item_before_payment_total['total_vendor_nta'],
                    'vendor_commission_value' => $session_item_before_payment_total['total_vendor_commission_value'],
                    'vendor_sell_price' => $session_item_before_payment_total['total_vendor_sell_price'],
                    'total_promo' => $session_item_before_payment_total['total_promo_amount'],
                    'ho_upselling' => $session_item_before_payment_total['total_ho_upselling'],
                    'ho_sell_price' => $session_item_before_payment_total['total_ho_sell_price'],
                    'channel_commission_full' => $session_item_before_payment_total['total_channel_commission_full'],
                    'channel_commission_ho' => $session_item_before_payment_total['total_channel_commission_ho'],
                    'channel_commission_share' => $session_item_before_payment_total['total_channel_commission_share'],
                    'channel_commission_share_ho' => $session_item_before_payment_total['total_channel_commission_share_ho'],
                    'channel_commission_value' => $session_item_before_payment_total['total_channel_commission_value'],
                    'channel_updown' => $session_item_before_payment_total['total_channel_updown'],
                    'channel_sell_price' => $session_item_before_payment_total['total_channel_sell_price'],
                    'channel_profit' => $total_harga_diskon>0 ? $session_item_before_payment_total['total_channel_profit']-$total_harga_diskon : $session_item_before_payment_total['total_channel_profit'],
                    'weight' => $session_item_before_payment_total['total_weight'],
                    'shipping_price_kg' => $shipping_price_kg,
                    'total_shipping' => $session_transaction['total_shipping'],
                    'total_shipping_real_weight' => $total_shipping_real_weight,
                    'voucher_id' => $product_voucher_id,
                    'voucher_amount' => $total_harga_diskon,
                    'total_deal' => $total_deal,
                    'transaction_from' => $transaction_from,
                    'created' => date('Y-m-d H:i:s'),
                    'createdby' => $org->code,
                    'updated' => date('Y-m-d H:i:s'),
                    'updatedby' => $org->code,
                );
                $r = $db->insert('transaction', $data_transaction);
                $transaction_id = $r->insert_id();


                //insert transaction contact
                $data_transaction_contact = array(
                    'transaction_id' => $transaction_id,
                    'billing_first_name' => $session_contact_billing['first_name'],
                    'billing_last_name' => $session_contact_billing['last_name'],
                    'billing_email' => $session_contact_billing['email'],
                    'billing_phone' => $session_contact_billing['phone'],
                    'billing_address' => $session_contact_billing['address'],
                    'billing_province_id' => $session_contact_billing['province_id'],
                    'billing_province' => $session_contact_billing['province'],
                    'billing_city_id' => $session_contact_billing['city_id'],
                    'billing_city' => $session_contact_billing['city'],
                    'billing_districts_id' => $session_contact_billing['district_id'],
                    'billing_district' => $session_contact_billing['district'],
                    'billing_postal_code' => $session_contact_billing['postal_code'],
                    'shipping_first_name' => $session_contact_shipping['first_name'],
                    'shipping_last_name' => $session_contact_shipping['last_name'],
                    'shipping_email' => $session_contact_shipping['email'],
                    'shipping_phone' => $session_contact_shipping['phone'],
                    'shipping_address' => $session_contact_shipping['address'],
                    'shipping_province_id' => $session_contact_shipping['province_id'],
                    'shipping_province' => $session_contact_shipping['province'],
                    'shipping_city_id' => $session_contact_shipping['city_id'],
                    'shipping_city' => $session_contact_shipping['city'],
                    'shipping_districts_id' => $session_contact_shipping['district_id'],
                    'shipping_district' => $session_contact_shipping['district'],
                    'shipping_postal_code' => $session_contact_shipping['postal_code'],
                    'created' => date('Y-m-d H:i:s'),
                    'createdby' => $org->code,
                    'updated' => date('Y-m-d H:i:s'),
                    'updatedby' => $org->code,
                );
                $r = $db->insert('transaction_contact', $data_transaction_contact);
                
                //insert transaction history
                $data_transaction_history=array(
                    'org_id' => $org_id,
                    'org_parent_id' => $org->parent_id,
                    'transaction_id' => $transaction_id,
                    'date' => date('Y-m-d H:i:s'),
                    'transaction_status_before'=>'',
                    'transaction_status'=>'PENDING',
                    'ref_table'=>'transaction',
                    'ref_id'=>$transaction_id,
                );
                $db->insert('transaction_history',$data_transaction_history);
                
                
                //insert transaction detail
                foreach ($session_item_before_payment['item'] as $key => $val) {
                    $weight=carr::get($val,'weight','0');
                    $detail_total_shipping=$shipping_price_kg*ceil($weight/1000);
                    $detail_total_shipping_real_weight=$shipping_price_kg*$weight/1000;
                    $session_item_before_payment_detail_price = carr::get($val, 'detail_price');
                    $vendor_nta=$session_item_before_payment_detail_price['vendor_nta'];
                    $vendor_commission_value = $session_item_before_payment_detail_price['vendor_commission_value'];
                    $vendor_sell_price = $session_item_before_payment_detail_price['vendor_sell_price'];
                    $total_promo = $session_item_before_payment_detail_price['promo_amount'];
                    $ho_upselling = $session_item_before_payment_detail_price['ho_upselling'];
                    $ho_sell_price = $session_item_before_payment_detail_price['ho_sell_price'];
                    $channel_commission_full = $session_item_before_payment_detail_price['channel_commission_full'];
                    $channel_commission_ho = $session_item_before_payment_detail_price['channel_commission_ho'];
                    $channel_commission_share = $session_item_before_payment_detail_price['channel_commission_share'];
                    $channel_commission_share_ho = $session_item_before_payment_detail_price['channel_commission_share_ho'];
                    $channel_commission_value = $session_item_before_payment_detail_price['channel_commission_value'];
                    $channel_updown = $session_item_before_payment_detail_price['channel_updown'];
                    $channel_sell_price = $session_item_before_payment_detail_price['channel_sell_price'];
                    $channel_profit = $session_item_before_payment_detail_price['channel_profit'];
                    if ($transaction_type == 'sell') {
                        $vendor_nta=$session_item_before_payment_detail_price['vendor_purchase_nta'];
                        $vendor_commission_value = $session_item_before_payment_detail_price['vendor_purchase_commission_value'];
                        $vendor_sell_price = $session_item_before_payment_detail_price['vendor_purchase_price'];
                        $total_promo = $session_item_before_payment_detail_price['promo_amount'];
                        $ho_upselling = $session_item_before_payment_detail_price['ho_upselling'];
                        $ho_sell_price = $session_item_before_payment_detail_price['ho_purchase_price'];
                        $channel_commission_full = $session_item_before_payment_detail_price['channel_purchase_commission_full'];
                        $channel_commission_ho = $session_item_before_payment_detail_price['channel_commission_ho'];
                        $channel_commission_share = $session_item_before_payment_detail_price['channel_purchase_commission_share'];
                        $channel_commission_share_ho = $session_item_before_payment_detail_price['channel_purchase_commission_share_ho'];
                        $channel_commission_value = $session_item_before_payment_detail_price['channel_purchase_commission_value'];
                        $channel_updown = $session_item_before_payment_detail_price['channel_updown'];
                        $channel_sell_price = $session_item_before_payment_detail_price['channel_purchase_price'];
                        $channel_profit = $session_item_before_payment_detail_price['channel_purchase_profit'];
                    }
                    $data_transaction_detail = array(
                        'transaction_id' => $transaction_id,
                        'vendor_id' => $val['vendor_id'],
                        'product_id' => $val['product_id'],
                        'product_name' => carr::get($val,'product_name'),
                        'pulsa_phone_number' => $pulsa_phone_number,
                        'qty' => $val['qty'],
                        'vendor_nta' => $vendor_nta,
                        'vendor_commission_value' => $vendor_commission_value,
                        'vendor_sell_price' => $vendor_sell_price,
                        'total_promo' => $total_promo,
                        'ho_upselling' => $ho_upselling,
                        'ho_sell_price' => $ho_sell_price,
                        'channel_commission_full' => $channel_commission_full,
                        'channel_commission_ho' => $channel_commission_ho,
                        'channel_commission_share' => $channel_commission_share,
                        'channel_commission_share_ho' => $channel_commission_share_ho,
                        'channel_commission_value' => $channel_commission_value,
                        'channel_updown' => $channel_updown,
                        'channel_sell_price' => $channel_sell_price,
                        'channel_profit' => $channel_profit,
                        'weight' => $weight,                      
                        'shipping_price_kg' => $shipping_price_kg,
                        'total_shipping' => $detail_total_shipping,
                        'total_shipping_real_weight' => $detail_total_shipping_real_weight,
                        'created' => date('Y-m-d H:i:s'),
                        'createdby' => $org->code,
                        'updated' => date('Y-m-d H:i:s'),
                        'updatedby' => $org->code,
                    );
                    $insert_trans_detail = $db->insert('transaction_detail', $data_transaction_detail);
                    $transaction_detail_id = $insert_trans_detail->insert_id();
                    $this->session()->set('transaction_code', $transaction_code_generate);
                    $this->session()->set('payment_code', '');
                    $this->session()->set('transaction_email', $session_contact_billing['email']);
                    $this->session()->set('transaction_id', $transaction_id);
                    
                    
                    
                    //insert transaction_detail_package
                    if(carr::get($val, 'is_package_product')){
                        
                        
                        $detail_package_product=array();
                        
                        foreach ($item as $item_key => $item_val) {
                            if(carr::get($val, 'product_id')==carr::get($item_val, 'item_id')){
                                $detail_package_product = carr::get($item_val, 'detail_package_product');
                                break;
                            }
                        }
                        
//                        cdbg::var_dump($detail_package_product);
//                        die(__CLASS__ . ' - ' . __FUNCTION__);
                        
                        if(count($detail_package_product)){
                            foreach ($detail_package_product as $product_package_detail_id => $package_val) {
                                $data_trans_detail_package = array(
                                    'transaction_detail_id'=>$transaction_detail_id,
                                    'product_id'=>carr::get($package_val, 'product_id'),
                                    'created'=>date('Y-m-d H:i:s'),
                                    'createdby'=>'system',
                                );
                                $db->insert('transaction_detail_package', $data_trans_detail_package);
                            }
                        }
                    }
                }
            }
            if ($insert_transaction_payment > 0) {
                //insert transaction payment
                $transaction_payment_code_generate = generate_code::get_next_transaction_code();
                $total_charge = $this->session()->get('total_amount_charge', 0);
                $total_payment = $this->session()->get('total_amount_payment');
                if ($total_payment == null) {
                    $total_payment = $session_transaction['amount_payment'] + $total_charge;
                }
                $bank = null;
                $account_number = '';
                if ($this->session()->get('payment_type') == 'bank_transfer') {
                    $bank = $this->session()->get('bank');
                    $account_number = $this->session()->get('account_number');
                }
                $data_payment = array(
                    "transaction_id" => $transaction_id,
                    "code" => $transaction_payment_code_generate,
                    "date" => date('Y-m-d H:i:s'),
                    "payment_status" => 'PENDING',
                    "payment_type" => $this->session()->get('payment_type'),
                    "total_item" => $session_transaction['amount_payment'],
                    "total_charge" => $total_charge,
                    "total_payment" => $total_payment,
                    "bank" => $bank,
                    "account_number" => $account_number,
                    "created" => date('Y-m-d H:i:s'),
                    "createdby" => $org->code,
                    "updated" => date('Y-m-d H:i:s'),
                    "updatedby" => $org->code,
                );
                $r = $db->insert('transaction_payment', $data_payment);
                $transaction_payment_id = $r->insert_id();
                foreach ($session_detail_charge as $key => $val) {
                    $data_payment_charge = array(
                        "transaction_payment_id" => $transaction_payment_id,
                        "code" => $val['code'],
                        "name" => $val['name'],
                        "amount" => $val['amount'],
                        "created" => date('Y-m-d H:i:s'),
                        "createdby" => $org->code,
                        "updated" => date('Y-m-d H:i:s'),
                        "updatedby" => $org->code,
                    );
                    $db->insert('transaction_payment_charge', $data_payment_charge);
                }
                //calculate stock

                $this->session()->set('transaction_payment_id', $transaction_payment_id);
            }
            if ($stock_process > 0) {
                stock_process::execute('transaction', $transaction_id);
            }

            // insert transaction session
            $data_transaction_session = array(
                "org_id" => $org_id,
                "transaction_id" => $transaction_id,
                "session_id" => $this->session()->session_id(),
                "api_session_id" => $api_session_id,
                "created" => date('Y-m-d H:i:s'),
                "createdby" => $org->code,
                "updated" => date('Y-m-d H:i:s'),
                "updatedby" => $org->code,
            );
            $db->insert('transaction_session', $data_transaction_session);
            
            $org_member_id=cobj::get($org,'member_id');
            $org_parent_id=cobj::get($org,'parent_id');
            if(($member_id!=null && strlen($member_id)>0)||($org_member_id!=null && strlen($org_member_id)>0)){
                if($insert_transaction_member_category>0){
                    if($org_member_id==null){
                        $org_member_id=$member_id;
                    }
                    $commission_member=member::get_commission_member_category($org_member_id);
                    foreach($commission_member as $data_transaction_member_category){
                        $commission_value=floor($session_item_before_payment_total['total_channel_profit']*$data_transaction_member_category['commission_percent']/100);
                        $data_transaction_member_category['transaction_id']=$transaction_id;
                        $data_transaction_member_category['commission_value']=$commission_value;
                        $db->insert('transaction_member_category', $data_transaction_member_category);
                    }
                    $have_transaction_member_category=1;
                }
            }
            if(ccfg::get('have_org_commission_level')==1){
                $org_get_commission=org::calculate_org_merchant_commission($org_id);
                if(count($org_get_commission)>0){
                    foreach($org_get_commission as $org_commission){
                        $org_commission_id=carr::get($org_commission,'org_id');
                        $commission_percent=carr::get($org_commission,'commission_percent');
                        $commission_value=floor($commission_percent*$session_item_before_payment_total['total_channel_profit']/100);
                        $data_org_commission=array(
                            "transaction_id"=>$transaction_id,
                            "org_id"=>$org_commission_id,
                            "commission_percent"=>$commission_percent,
                            "commission_value"=>$commission_value,
                        );
                        $db->insert('transaction_org_commission',$data_org_commission);
                    }
                }
            }
            if($this->session()->get('payment_type')=='deposit'||$this->session()->get('payment_type')=='deposit_lucky'||$this->session()->get('payment_type')=='cod'){
                $err_member_balance=0;
                try{
                    $booking_code = generate_code::generate_booking_code($transaction_id);
                    $data = array(
                        'booking_code' => $booking_code,
                    );
                    $db->update('transaction', $data, array('transaction_id' => $transaction_id));
                    $this->session()->set('booking_code', $booking_code);
                    if($this->session()->get('payment_type')=='deposit'){
                        member_balance::history('transaction', $transaction_id);
                    }elseif($this->session()->get('payment_type')=='deposit_lucky'){
                        member_balance_lucky::history('transaction', $transaction_id);
                    }
                } catch (Exception $ex) {
                    $err_code++;
                    $err_member_balance++;
                }
                if($err_member_balance==0){
                    $data_update=array();
                    $data_update['date_success']=date('Y-m-d H:i:s');
                    $data_update['payment_status']='SUCCESS';
                    $db->update('transaction_payment',$data_update,array('transaction_payment_id'=>$transaction_payment_id));
                    $data_update['transaction_status']='SUCCESS';
                    $data_update['order_status']='SUCCESS';
                    $db->update('transaction',$data_update,array('transaction_id'=>$transaction_id));
                    if($session_transaction['page']=='luckyday'){
                        $winner_transaction_id=transaction::insert_lucky_coupon_quota($transaction_id);
                    }
                    if($have_transaction_member_category==1&&$this->session()->get('payment_type')!='cod'){
                        $db->query('update transaction set is_paid=1 where transaction_id='.$db->escape($transaction_id));
                        $db->query('update transaction_member_category set is_paid=1 where transaction_id='.$db->escape($transaction_id));
                        member_balance::history('TransactionCommission', $transaction_id);
                    }
                    if($session_transaction['page']=='pulsa'){
                        $res_pulsa=pulsa_api::issued_pulsa($transaction_id);
                        $err_api_pulsa=$res_pulsa['err_code'];
                        $update_transaction_pulsa=array();
                        if($err_api_pulsa==0){
                            $update_transaction_pulsa['pulsa_status']='SUCCESS';
                        }else{
                            $update_transaction_pulsa['pulsa_status']='SUSPECT';
                        }
                        $db->update('transaction',$update_transaction_pulsa,array("transaction_id"=>$transaction_id));
                    }
                    $q = "
                        select
                                td.*,
                                p.*
                        from
                                transaction_detail as td
                                inner join product as p on p.product_id=td.product_id
                        where
                                td.status>0
                                and td.transaction_id=" . $db->escape($transaction_id) . "
                    ";
                    $r = $db->query($q);
                    if ($r->count() > 0) {
                        foreach ($r as $row) {
                            if ($row->is_voucher > 0) {
                                for ($i = 1; $i <= $row->qty; $i++) {
                                    $transaction_voucher = array(
                                        "transaction_id" => $transaction_id,
                                        "vendor_id" => $row->vendor_id,
                                        "product_id" => $row->product_id,
                                    );
                                    $r_tv = $db->insert('transaction_voucher', $transaction_voucher);
                                    $transaction_voucher_id = $r_tv->insert_id();
                                    $voucher_code = generate_code::generate_voucher_code($transaction_voucher_id);
                                    $update_voucher_code = array(
                                        "voucher_code" => $voucher_code,
                                    );
                                    $db->update('transaction_voucher', $update_voucher_code, array("transaction_voucher_id" => $transaction_voucher_id));
                                }
                            }
                        }
                    }
                    
                    if(ccfg::get('not_have_poin')!=1){
                        $not_valid=carr::get($poin_payment_not_in,$this->session()->get('payment_type'));
                        if($not_valid==null){
                            $not_valid=carr::get($poin_product_not_in,$session_transaction['page']);
                        }
                        if($not_valid==null){
                            member_balance_poin::history('Transaction', $transaction_id);
                        }
                    }
                    
                    
                }
            }
            
        } catch (Exception $e) {
            $err_code++;
            $err_message = clang::__("system_fail") . $e->getMessage();
        }
        if ($err_code == 0){
            if ($requota){
                product_voucher::quotahistory($product_voucher_id, -1);
                
            }
        }
        if ($err_code == 0) {
            $db->commit();
        } else {
            $db->rollback();
        }
        
        $res['err_code'] = $err_code;
        $res['err_message'] = $err_message;
        $res['transaction_id'] = $transaction_id;
        $res['winner_transaction_id'] = $winner_transaction_id;
        return $res;
    }

    public function after_payment($request) {
        $db = CDatabase::instance();
        $err_code = 0;
        $err_message = '';
        try {
            $db->begin();
            //update transaction
            $data = array(
                'transaction_status' => $request['transaction_status'],
                'order_status' => $request['order_status'],
                'payment_status' => $request['payment_status'],
            );
            $db->update('transaction', $data, array('transaction_id' => $request['transaction_id']));
            // update payment
            $data = array(
                'payment_status' => $request['payment_status'],
            );
            $db->update('transaction_payment', $data, array('transaction_payment_id' => $request['transaction_payment_id']));
        } catch (Exception $e) {
            $err_code++;
            $err_message = clang::__("system_fail") . $e->getMessage();
        }
        if ($err_code == 0) {
            $db->commit();
        } else {
            $db->rollback();
        }
        $res['err_code'] = $err_code;
        $res['err_message'] = $err_message;
        return $res;
    }

    public function update_payment_code() {
        $db = CDatabase::instance();
        $err_code = 0;
        $err_message = '';
        try {
            $db->begin();
            if ($this->session()->get('request') != 1) {
                $booking_code = generate_code::generate_booking_code($this->session()->get('transaction_id'));
                $data = array(
                    'booking_code' => $booking_code,
                );
                $db->update('transaction', $data, array('transaction_id' => $this->session()->get('transaction_id')));
                $this->session()->set('booking_code', $booking_code);
            } else {
                $data = array(
                    'transaction_status' => 'PENDING',
                    'order_status' => 'PENDING',
                    'payment_status' => 'PENDING',
                );
                $db->update('transaction', $data, array('transaction_id' => $this->session()->get('transaction_id')));
            }

            $data = array(
                'payment_code' => $this->session()->get('payment_code'),
            );
            $db->update('transaction_payment', $data, array('transaction_payment_id' => $this->session()->get('transaction_payment_id')));
        } catch (Exception $e) {
            $err_code++;
            $err_message = clang::__("system_fail") . $e->getMessage();
        }
        if ($err_code == 0) {
            $db->commit();
        } else {
            $db->rollback();
        }
        $res['err_code'] = $err_code;
        $res['err_message'] = $err_message;
        return $res;
    }

    public function next_method($method) {
        switch ($method) {
            case 'login':
                return 'get_payment_charge';
                break;
        }
    }

}
