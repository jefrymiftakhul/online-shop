<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Mar 21, 2016
     */
    class CIttronMallElement extends CElement {

        protected $theme;
        protected $theme_func_name;

        public function __construct($id = "", $tag = "div") {
            parent::__construct($id, $tag);

            //$this->theme = ctheme::get_current_theme();
            //$this->theme_func_name = carr::get(C62HallTheme::$themes_func, $this->theme);
        }

        protected function call_html_theme($indent = 0) {
            $function = 'html_' . $this->theme_func_name;
            if (method_exists($this, $function)) {
                return $this->$function($indent);
            }
            return false;
        }
        
        protected function call_js_theme($indent = 0) {
            $function = 'js_' . $this->theme_func_name;
            if (method_exists($this, $function)) {
                return $this->$function($indent);
            }
            return false;
        }

        public function get_theme() {
            return $this->theme;
        }

        public function set_theme($theme) {
            $this->theme = $theme;
        }

    }
    