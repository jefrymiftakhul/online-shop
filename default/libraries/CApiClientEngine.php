<?php

class CApiClientEngine {

    protected $api;
    protected $product_code;
    protected $_last_response;
    protected $_last_request;

    protected function __construct($api) {
        $this->api = $api;
    }

    public function error() {
        return CApiClientError::instance();
    }

    public function session() {
        return CApiClientSession::instance($this->api->product_category_code());
    }


    public function last_request() {
        return $this->_last_request;
    }

    public function last_response() {
        return $this->_last_response;
    }

    public function __request($service_name, $url, $data = NULL) {
        set_time_limit(60);
        $org_id = $this->api->org_id();
        $username = $this->api->username();

        $curl = CCurl::factory(NULL);
        $curl->set_timeout(40000);
        $curl->set_useragent('Mozilla/5.0 (Windows NT 6.1; WOW64; rv:26.0) Gecko/20100101 Firefox/26.0');
        $this->session()->set('api_request_'.$service_name,$data);
        $data_json = $data;
        if ($data != NULL) {
            if (is_array($data)) {
                $data_json = cjson::encode($data);
                $data = curl::as_post_string($data);
            }
            $curl->set_raw_post($data);
        }
        $curl->set_opt(CURLOPT_SSL_VERIFYPEER, FALSE);
        $curl->set_opt(CURLOPT_SSL_VERIFYHOST, 2);
        $curl->set_opt(CURLOPT_ENCODING, 'gzip, deflate');


        $curl->set_url($url);

        CFBenchmark::start($this->session()->session_id() . '_client_payment_gateway_request');
        $this->_last_request = $data;
        $curl->exec();
        $has_error = $curl->has_error();

        $response = $curl->response();
        $this->session()->set('api_response_'.$service_name,$response);
        $this->_last_response = $response;
        $http_response_code = $curl->get_http_code();
        $benchmark = CFBenchmark::get($this->session()->session_id() . '_client_payment_gateway_request');
        $execution_time = 0;
        if (isset($benchmark['time'])) {
            $execution_time = $benchmark['time'];
        }
        //log request
        $product_category_code = $this->api->product_category_code();
        $product_code = $this->api->product_code();
        $api_session_id = $this->session()->get('api_session_id');

        $auth = $this->api->api_auth();

        $session_id = date("YmdHis") . "_" . $this->session()->session_id();
        $api_session_id = $this->session()->get('api_session_id');


        $dir_1 = substr($session_id, 0, 8);
        $dir_2 = substr($session_id, 8, 2);
        $log_path = CF::get_dir('logs');

        if (!is_dir($log_path))
            mkdir($log_path);
        $log_path .= $product_category_code . DS;
        if (!is_dir($log_path))
            mkdir($log_path);
        $log_path .= $product_code . DS;
        if (!is_dir($log_path))
            mkdir($log_path);
        $log_path .= $dir_1 . DS;
        if (!is_dir($log_path))
            mkdir($log_path);
        $log_path .= $dir_2 . DS;
        if (!is_dir($log_path))
            mkdir($log_path);
        if (strlen($api_session_id) > 0) {
            $log_path .= $api_session_id . DS;
            if (!is_dir($log_path))
                mkdir($log_path);
        }
        $request_date = date('Y-m-d H:i:s');

        $filename_request = $product_category_code . $product_code . $session_id . "_" . $api_session_id . "_" . $service_name . "_rq_" . date("His") . ".log";
        $filename_response = $product_category_code . $product_code . $session_id . "_" . $api_session_id . "_" . $service_name . "_rs_" . date("His") . ".log";

        @file_put_contents($log_path . $filename_request, $data_json);
        @file_put_contents($log_path . $filename_response, $response);

        $data = array(
            "product_code" => $product_code,
            "product_category_code" => $product_category_code,
            "request_date" => date("Y-m-d H:i:s"),
            "org_id" => $org_id,
            "username" => $username,
            "auth" => $auth,
            "url" => $url,
            "session_id" => $session_id,
            "api_session_id" => $api_session_id,
            "request" => $filename_request,
            "response" => $filename_response,
            "http_response_code" => $http_response_code,
            "execution_time" => $execution_time,
        );
        CDatabase::instance()->insert("api_client_log_request", $data);

        if (isset($has_error) && $has_error == TRUE) {
            $this->error()->add($has_error, 1010);
            return FALSE;
        }

        return $this->_last_response;
    }

}
