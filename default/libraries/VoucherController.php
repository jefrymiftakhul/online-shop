<?php
class VoucherController extends SixtyTwoHallFamilyController
{

    public function __construct()
    {
        $this->_page = "voucher";
        parent::__construct();
        $app = CApp::instance();
        $app->add_js("
                $('#type').val('voucher');
            ");

    }
}
