<?php

    class TransactionTrevoDriverBalance {

        public $ref_table = 'transaction';
        public $ref_pk = 'transaction_id';
        protected static $driver_comm = 80;

        public static function balance($id) {
            $db = CDatabase::instance();
            $app = CApp::instance();
            $user = $app->user();
            $username = 'SYSTEM';
            if ($user != null) {
                $username = $user->username;
            }
            $return = array();
            $q = "
                SELECT
                    t.channel_sell_price as sell_price,
                    t.org_id as org_id,
                    t.booking_code as code,
                    tt.trevo_driver_id as trevo_driver_id,
                    mb.balance_idr as balance_idr 
                FROM 
                    transaction as t
                    inner join transaction_trevo as tt on tt.transaction_id = t.transaction_id
                    inner join trevo_driver as m on m.trevo_driver_id=tt.trevo_driver_id
                    left join trevo_driver_balance as mb on mb.trevo_driver_id=m.trevo_driver_id
                WHERE 
                    t.transaction_id = " . $db->escape($id) . "
            ";
            $data = cdbutils::get_row($q);
            $result = array();
            if ($data != null) {
                $saldo_before = 0;
                if (!$data->balance_idr == null) {
                    $saldo_before = $data->balance_idr;
                }
                $saldo = $saldo_before + ((self::$driver_comm / 100) * $data->sell_price);
                $return['org_id'] = $data->org_id;
                $return['trevo_driver_id'] = $data->trevo_driver_id;
                $return['currency'] = 'IDR';
                $return['saldo_before'] = $saldo_before;
                $return['balance_in'] = ((self::$driver_comm / 100) * $data->sell_price);
                $return['balance_out'] = 0;
                $return['saldo'] = $saldo;
                $return['createdby'] = $username;
                $return['module'] = 'Transaction';
                $return['description'] = 'Transaction ' . $data->code;
                $return['history_date'] = date('Y-m-d H:i:s');
            }
            $result[] = $return;
            return array(
                'data' => $result
            );
        }

    }
    