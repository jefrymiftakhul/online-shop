<?php

    class CApiClientSession {

        private $session_key;
        private static $instance;

        const __ExpiredSession = 30;

        private $_have_cookies = FALSE;
        private $_session_id;
        private $_data;
        private $_product_category_code;
        private $_product_code;
        private $_session_path = NULL;
        private $_session_key = NULL;
        private static $_instance;
        private $_directory = NULL;

        private function __construct($product_category_code,$session_id=null) {
			
            $product_category_code = strtoupper($product_category_code);
            $this->_product_category_code = $product_category_code;
            $this->_session_key = '62hallfamily_api_' . $product_category_code ;
            $this->_session_id = $session_id;
            $directory = CF::get_dir();

            $this->_directory = $directory;
            if (is_array($this->_session_id)) {
                $this->_session_id = '';
            }
            if ($this->_session_id != NULL && strlen($this->_session_id) > 0) {
                $dir_1 = substr($this->_session_id, 0, 8);
                $dir_2 = substr($this->_session_id, 8, 2);
                $this->_session_path = $this->_directory . "sessions" . DS . "api" . DS . $this->_product_category_code . DS 
                        . $dir_1 . DS . $dir_2 . DS;
                $this->load();
            }
            else {
                $this->_session_path = $this->_directory . "sessions" . DS . "api" . DS . $this->_product_category_code . DS
                        . date("Ymd") . DS . date("H") . DS;
                $this->init();
            }
        }
		
		public static function factory($product_category_code){
			return new CAPIClientSession($product_category_code);
		}
        public static function instance($product_category_code,$session_id=null) {
            if (self::$instance == null || !isset(self::$instance[$product_category_code])) {
                if(($session_id ==null) || (strlen($session_id)==0)){
                        $session = Session::instance();
                        $session_id =$session->get('62hallfamily_api_' . $product_category_code);
                }
                self::$instance[$product_category_code] = new CAPIClientSession($product_category_code,$session_id);
            }
            return self::$instance[$product_category_code];
        }

        public function init() {
            $time_now = date("YmdHis");
            $prefix = $time_now;
            $prefix.= "_" . $this->_product_category_code . "_";
            $this->_session_id = uniqid($prefix);
            $session_path = "sessions" . DS . "api" . DS . $this->_product_category_code . DS 
                    . date("Ymd") . DS . date("H");
            $this->make_dir($session_path);
            $this->_data['session_id'] = $this->_session_id;
            $this->save();
            return $this;
        }

        private function make_dir($path) {
            $tmp_path = explode(DS, $path);
            $directory = $this->_directory;
            foreach ($tmp_path as $key => $t) {
                $directory .= $t . DS;
                if ((!is_dir($directory)) && !file_exists($directory))
                        mkdir($directory);
            }
        }

        public function clear() {
            //$this->_data = array();
            //$this->_data['session_id'] = $this->_session_id;
            //$this->save();
            $this->regenerate();
            return $this;
        }

        public function regenerate() {
            $this->_session_path = $this->_directory . "sessions" . DS . "api" . DS . $this->_product_category_code . DS 
                    . date("Ymd") . DS . date("H") . DS;
            $this->_data = null;
            $this->init();
            return $this;
        }

        public function set($key, $val) {
            if (!isset($this->_data[$key]) || $this->_data[$key] != $val) {
                $this->_data[$key] = $val;
                $this->save();
            }
            return $this;
        }

        public function delete($key) {
            if (isset($this->_data[$key])) {
                unset($this->_data[$key]);
                $this->save();
            }
            return $this;
        }

        public function get($key, $default = NULL) {
            if (isset($this->_data[$key])) return $this->_data[$key];
            return $default;
        }

        public function save($data = NULL) {
            $session = Session::instance();
            if ($data != NULL) $this->_data = $data;
            $filename = $this->_session_path . $this->_session_id . EXT;
            cphp::save_value($this->_data, $filename);
            $session->set($this->_session_key, $this->_session_id);
            return $this;
        }

        public function load() {
            $filename = $this->_session_path . $this->_session_id . EXT;
            if (!file_exists($filename)) {
                $this->regenerate();
                $filename = $this->_session_path . $this->_session_id . EXT;
            }
            $this->_data = cphp::load_value($filename);
            return $this;
        }

        public function data() {
            $session = Session::instance();
            $session_data = $session->get($this->session_key);
            return $session_data;
        }
		
		public function get_data() {
			return $this->_data;
		}
		
        public function session_id() {
            return $this->_session_id;
        }

        public function session_key() {
            return $this->session_key;
        }
		
		public function get_session_id(){
			return $this->_session_id;
		}
		
    }
    