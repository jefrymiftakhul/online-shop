<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Oct 13, 2015
     * @license http://piposystem.com Piposystem
     */
    class CFormInput62HallFamilyInput_Dropdown extends CFormInput62HallFamilyInput {

        protected $list;
        protected $value = '';
        protected $affect;
        protected $date_source;
        protected $date_target;

        public function __construct($id = '') {
            $this->list = array();
            $this->affect = false;
            $this->date_source = "";
            $this->date_target = "";
            parent::__construct($id);
        }

        public static function factory($id) {
            return new CFormInput62HallFamilyInput_Dropdown($id);
        }

        public function set_value($value) {
            $this->value = $value;
            return $this;
        }

        public function callback_html() {
            // overwrite this method
        }
        
        public function set_affect($bool) {
            $this->affect = $bool;
            return $this;
        }
        
        public function set_date_source($param) {
            $this->date_source = $param;
            return $this;
        }
        
        public function set_date_target($param) {
            $this->date_target = $param;
            return $this;
        }

        public function html($indent = 0) {
            $html = CStringBuilder::factory();

            $this->callback_html();

            $classes = $this->classes;
            $classes = implode(" ", $classes);
            if (strlen($classes) > 0) {
                $classes = " " . $classes;
            }
            $custom_css = $this->custom_css;
            $custom_css = crenderer::render_style($custom_css);
            if (strlen($custom_css) > 0) {
                $custom_css = ' ' . $custom_css . ' ';
            }

            $div_container = $this->add_div()->add_class('input-select-container ' . $classes)
                    ->add_attr('style', $custom_css);
            $input_group = $div_container->add_div()->add_class('input-select-group input-group');
            $input_label = $input_group->add_control($this->id . '-input', 'text')
                    ->set_attr('readonly', 'readonly')
                    ->add_class('input-select');
            $input_hidden = $input_group->add_control($this->id . '-input-value', 'hidden')->set_name($this->name)
                    ->set_attr('readonly', 'readonly')
                    ->add_class('input-select-value');

            if (strlen($this->value) > 0) {
                $input_hidden->set_value($this->value);
                $input_label->set_value(carr::get($this->list, $this->value));
            }
            $btn_dropdown = $input_group->add_div($this->id . '-input_btn')->add_class('input-select-btn input-group-addon');

            $div_options = $div_container->add_div($this->id . '-input_options')->add_class('input-options');
            foreach ($this->list as $list_k => $list_v) {
                $div_options->add_div()->set_attr('key', $list_k)->set_attr('value', $list_v)->add_class('option')->add($list_v);
            }

            $html->append(parent::html($indent));
            return $html->text();
        }

        public function js($indent = 0) {
            $js = CStringBuilder::factory();

            $js->appendln("
                jQuery('.input-select-container #" . $this->id . "-input_btn, .input-select-container #" . $this->id . "-input').on('click', function(e){
                    e.stopPropagation();
                    jQuery('.input-options').hide();
                    
                    var OptionDiv = jQuery(this).parents('.input-select-container').find('#" . $this->id . "-input_options');
                    if (dropdownOpen == false) {
                        var width_input = jQuery(this).parent().find('input').width();
                        var width_option = OptionDiv.width();
                        if (width_input > width_option) {
                            OptionDiv.css('width', (width_input+12)+'px');
                        }
                        OptionDiv.show();
                        dropdownOpen = true;
                    }
                    else {
                        OptionDiv.hide();
                        dropdownOpen = false;
                    }
                });
                
                jQuery('.input-select-container #" .$this->id ."-input_options .option').on('click', function(){
                    var value = jQuery(this).attr('value');
                    var key = jQuery(this).attr('key');
                    jQuery(this).parents('.input-select-container').find('.input-select').val(value);
                    jQuery(this).parents('.input-select-container').find('.input-select-value').val(key);
                    jQuery('.input-options').hide();
                    dropdownOpen = false;");
            if ($this->affect == true) {
                if (strlen($this->date_source) > 0 && strlen($this->date_target) > 0) {
                    $js->appendln("
                        change_startdate_enddate(value);
                            ");
                }
            }
            $js->appendln("
                });
                ");
            if ($this->affect == true) {
                if (strlen($this->date_source) > 0 && strlen($this->date_target) > 0) {
                    $js->appendln("
                        function change_startdate_enddate(value){
                            var add_night = '+'+value+'d';
                            var date2 = $('.".$this->date_source."').parent().datepicker('getDate', add_night); 
                            date2.setDate(date2.getDate()+parseInt(value)); 
                            // $('.".$this->date_target."').parent().datepicker('setStartDate', date2);
                            $('.".$this->date_target."').parent().datepicker('setDate', date2);
                        }
                            ");
                }
            }

            $js->append(parent::js($indent));
            return $js->text();
        }

    }
    