<?php

class CFormInput62HallFamilyInput_MemberAddressSelect extends CFormInputSelectSearch {

    protected $multiple;
    protected $applyjs;
    protected $all;
    protected $none;
    protected $type;
    protected $member_id;
    protected $value;
	protected $active=false;

    public function __construct($id) {
        parent::__construct($id);
        $this->all = true;
        $this->none = true;
        $this->query = "select * from member_address where status>0";
        $this->type = "select";
        $this->applyjs = "select2";
        $this->placeholder = clang::__('Select Data Contact');
        $this->format_result = 'Nama : {name}<br>Address:{address}';
        $this->format_selection = 'Nama : {name}-{address}';
        $this->search_field = array('name','address');
        $this->key_field = 'member_address_id';
    }

    public static function factory($id) {
        return new CFormInput62HallFamilyInput_MemberAddressSelect($id);
    }

    public function set_applyjs($applyjs) {
        $this->applyjs = $applyjs;
        return $this;
    }

    public function set_active($bool) {
        $this->active = $bool;
        return $this;
    }

    public function set_all($bool) {
        $this->all = $bool;
        return $this;
    }

    public function set_none($bool) {
        $this->none = $bool;
        return $this;
    }

    public function set_type($val) {
        $this->type = $val;
        return $this;
    }

    public function set_member($val) {
        $this->member_id = $val;
        return $this;
    }

    public function set_value($value) {
        $this->value = $value;
        return $this;
    }

    public static function generate_query($data) {
        $db = CDatabase::instance();
        $q = "
			select 
				member_address_id,
				name,
				address
			from 
				member_address 
			where 
				status>0
		";
        if ($data->active) {
            $q.="
				and is_active>0
			";
        }
        if (strlen($data->member_id)) {
            $q.="
				and member_id=" . $db->escape($data->member_id) . "
			";
        }
        if (strlen($data->type)) {
            $q.="
				and type=" . $db->escape($data->type) . "
			";
        }
        if ($data->all) {
            $q = "
				select
					'ALL' as member_address_id_id,
					'' as name,
					'' as address
				union all
			" . $q;
        }
        return $q;
    }

    public static function ajax($data) {
        $obj = new stdclass();
        $q = self::generate_query($data);
        $data->query = $q;
        $obj->data = $data;
        $request = array_merge($_GET, $_POST);
        echo cajax::searchselect($obj, $request);
    }

    public function create_ajax_url() {
        return CAjaxMethod::factory()
                        ->set_type('callback')
                        ->set_data('callable', array('CFormInput62HallFamilyInput_MemberAddressSelect', 'ajax'))
                        ->set_data('key_field', $this->key_field)
                        ->set_data('search_field', $this->search_field)
                        ->set_data('applyjs', $this->applyjs)
                        ->set_data('all', $this->all)
                        ->set_data('type', $this->type)
                        ->set_data('member_id', $this->member_id)
                        ->set_data('active', $this->active)
                        ->makeurl();
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        $db = CDatabase::instance();
        if ($this->value == null) {
			$this->active=true;
            $q = $this->generate_query($this);
            $r = $db->query($q);

            if ($r->count() > 0) {
                $row = $r[0];
                $this->value = $row->member_address_id;
            }
			$this->active=false;
        }
        $html->appendln(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();
        $js->set_indent($indent);
        $js->append(parent::js($indent))->br();
        return $js->text();
    }

}
