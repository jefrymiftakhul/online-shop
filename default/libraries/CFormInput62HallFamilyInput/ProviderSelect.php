<?php

class CFormInput62HallFamilyInput_ProviderSelect extends CFormInputSelectSearch {
	protected $multiple;
	protected $applyjs;
	protected $all;
	protected $none;
	protected $value;
	protected $org_id;
        protected $type;
	
        
	public function __construct($id) {
		parent::__construct($id);
		$this->org_id=CF::org_id();
		$this->all=false;
		$this->none=true;
                $this->query = "select * from provider where status>0";
                $this->type = "select";
                $this->applyjs = "select2";
		$this->placeholder = clang::__('Select Provider');
		$this->format_result = '{provider_name}';
		$this->format_selection = '{provider_name}';
		$this->search_field = array('provider_name');
		$this->key_field = 'provider_id';
	}
	
	public static function factory($id) {
		return new CFormInput62HallFamilyInput_ProviderSelect($id);
	}
	public function set_applyjs($applyjs) {
		$this->applyjs = $applyjs;
		return $this;
	}
	
        public function set_type($val){
            $this->type=$val;
            return $this;
        }


        public function set_all($bool) {
		$this->all = $bool;
		return $this;
		
	}
	public function set_none($bool) {
		$this->none = $bool;
		return $this;
		
	}	
	
	public function set_org_id($id){
		$this->org_id = $id;
		return $this;
	}
	
	public function set_value($value){
		$this->value = $value;
		return $this;
	}
        
	public static function generate_query($data){
            $db=CDatabase::instance();
            $q =  "
                select
                    *
                from
                    provider
                where
                    status>0
            ";
            if(strlen($data->type)>0){
                $q.="
                    and provider_type=".$db->escape($data->type)."
                ";    
            }
            return $q;
            
        }
        
	public static function ajax($data) {
		$db=  CDatabase::instance();
		$obj = new stdclass();
		$q=self::generate_query($data);
		$data->query = $q;
		$obj->data = $data;
		$request = array_merge($_GET,$_POST);
		echo cajax::searchselect($obj,$request);
		
	}
	
	public function create_ajax_url() {
		return CAjaxMethod::factory()
                    ->set_type('callback')
                    ->set_data('callable', array('CFormInput62HallFamilyInput_ProviderSelect', 'ajax'))
                    ->set_data('key_field', $this->key_field)
                    ->set_data('search_field', $this->search_field)
                    ->set_data('applyjs', $this->applyjs)
                    ->set_data('all', $this->all)
                    ->set_data('type', $this->type)

                    ->makeurl();
	}	

        public function html($indent=0){
		$db=CDatabase::instance();
		$html = new CStringBuilder();
		$html->set_indent($indent);
		if(strlen($this->value)==0){
			$q=$this->generate_query($this);
			$r=$db->query($q);
			if($r->count()>0){
                            $row=$r[0];
                            $this->value=$row->provider_id;
			}
		}
		$html->appendln(parent::html($indent));
		return $html->text();
	}       
}