<?php

class CFormInput62HallFamilyInput_ProvinceSelect extends CFormInputSelectSearch {

    protected $multiple;
    protected $applyjs;
    protected $all;
    protected $none;
    protected $country_id;
    protected $value;
    protected $page;
    protected $same_location_shipping_price;  


    public function __construct($id) {
        parent::__construct($id);
        $this->all = true;
        $this->none = true;
        $this->query = "select * from province where status>0";
        $this->type = "select";
        $this->applyjs = "select2";
        $this->placeholder = clang::__('Select Province');
        $this->format_result = '{name}';
        $this->format_selection = '{name}';
        $this->search_field = array('code', 'name');
        $this->key_field = 'province_id';
        $this->same_location_shipping_price=false;
    }

    public static function factory($id) {
        return new CFormInput62HallFamilyInput_ProvinceSelect($id);
    }

    public function set_applyjs($applyjs) {
        $this->applyjs = $applyjs;
        return $this;
    }
	
    public function set_page($val){
            $this->page=$val;
            return $this;
    }
	
    public function set_all($bool) {
        $this->all = $bool;
        return $this;
    }

    public function set_none($bool) {
        $this->none = $bool;
        return $this;
    }

    public function set_country_id($id) {
        $this->country_id = $id;
        return $this;
    }

    public function set_value($value) {
        $this->value = $value;
        return $this;
    }
    
    public function set_same_location_shipping_price($bol){
        $this->same_location_shipping_price=$bol;
        return $this;
    }

    public static function generate_query($data) {
        $db = CDatabase::instance();
        $org_id=CF::org_id();
        $q = "
			select 
				province_id,
				code,
				name
			from 
				province 
			where 
				status>0
		";
        if (strlen($data->country_id)) {
            $q.="
				and country_id=" . $db->escape($data->country_id) . "
			";
        }

        if($data->page=='gold') {
                $q = "
                    select
                        province_id as province_id,
                        code as code,
                        name as name
                    from
                        province
                    where province_id in (15)
                ";
        }
	if($data->same_location_shipping_price){
            $q="
                select
                    p.province_id as province_id,
                    p.code as code,
                    p.name as name
                from
                    shipping_price as sp
                    inner join city as c on c.city_id=sp.city_id
                    inner join province as p on p.province_id=c.province_id
                where
                    sp.status>0
                    and c.status>0
                    and p.status>0
                    and sp.org_id=".$db->escape($org_id)."
                group by 
                    p.province_id
            ";
        }	
        if ($data->all) {
            $q = "
				select
					'ALL' as province_id,
					'' as code,
					'ALL' as name
				union all
			" . $q;
        }
        return $q;
    }

    public static function ajax($data) {
        $obj = new stdclass();
        $q = self::generate_query($data);
        $data->query = $q;
        $obj->data = $data;
        $request = array_merge($_GET, $_POST);
        echo cajax::searchselect($obj, $request);
    }

    public function create_ajax_url() {
        return CAjaxMethod::factory()
                        ->set_type('callback')
                        ->set_data('callable', array('CFormInput62HallFamilyInput_ProvinceSelect', 'ajax'))
                        ->set_data('key_field', $this->key_field)
                        ->set_data('search_field', $this->search_field)
                        ->set_data('applyjs', $this->applyjs)
                        ->set_data('all', $this->all)
                        ->set_data('country_id', $this->country_id)
                        ->set_data('page', $this->page)
                        ->set_data('same_location_shipping_price', $this->same_location_shipping_price)
                        ->makeurl();
    }

    public function html($indent = 0) {
        $html = new CStringBuilder();
        $html->set_indent($indent);
        $db = CDatabase::instance();
        if ($this->value == null) {
            $q = $this->generate_query($this);
            $r = $db->query($q);

            if ($r->count() > 0) {
                $row = $r[0];
                $this->value = $row->province_id;
            }
        }
        $html->appendln(parent::html($indent));
        return $html->text();
    }

    public function js($indent = 0) {
        $js = new CStringBuilder();
        $js->set_indent($indent);
        $js->append(parent::js($indent))->br();
        return $js->text();
    }

}
