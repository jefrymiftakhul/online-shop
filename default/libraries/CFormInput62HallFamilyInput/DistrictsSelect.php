<?php

class CFormInput62HallFamilyInput_DistrictsSelect extends CFormInputSelectSearch {
	protected $multiple;
	protected $applyjs;
	protected $all;
	protected $none;
	protected $city_id;
	protected $value;
        protected $same_location_shipping_price;  
	
        
	public function __construct($id) {
		parent::__construct($id);
		
		$this->all=true;
		$this->none=true;
        $this->query = "select * from districts where status>0";
        $this->type = "select";
		$this->applyjs='select2';
		$this->placeholder = clang::__('Select District');
		$this->format_result = '{name}';
		$this->format_selection = '{name}';
		$this->search_field = array('name');
		$this->key_field = 'districts_id';
            $this->same_location_shipping_price=false;
	}
	
	public static function factory($id) {
		return new CFormInput62HallFamilyInput_DistrictsSelect($id);
	}
	public function set_multiple($bool) {
		$this->multiple = true;
		return $this;
	}
	public function set_applyjs($applyjs) {
		$this->applyjs = $applyjs;
		return $this;
	}
	
	public function set_all($bool) {
		$this->all = $bool;
		return $this;
		
	}
	public function set_none($bool) {
		$this->none = $bool;
		return $this;
		
	}
	
	public function set_city_id($id) {
		$this->city_id=$id;
		return $this;
	}
	
	public function set_value($value){
		$this->value = $value;
		return $this;
	}

        public function set_same_location_shipping_price($bol){
            $this->same_location_shipping_price=$bol;
            return $this;
        }
        
	public static function generate_query($data){
		$db=  CDatabase::instance();
        $org_id=CF::org_id();
		$q =  "
			select 
				districts_id,
				name
			from 
				districts 
			where 
				status>0
		";
		if(strlen($data->city_id)){
			$q.="
				and city_id=".$db->escape($data->city_id)."
			";
		}
                if($data->same_location_shipping_price){
                    $q="
                        select
                            d.districts_id as districts_id,
                            d.code as code,
                            d.name as name
                        from
                            shipping_price as sp
                            inner join districts as d on d.districts_id=sp.districts_id
                        where
                            sp.status>0
                            and d.status>0
                            and sp.org_id=".$db->escape($org_id)."
                        group by 
                            d.districts_id
                    ";
                }	
		if($data->all){
			$q="
				select
					'ALL' as districts_id,
					'ALL' as name
				union all
			".$q;
			
		}
		return $q;
	}
	
	public static function ajax($data) {
		$obj = new stdclass();
		$q=self::generate_query($data);
		$data->query = $q;
		$obj->data = $data;
		$request = array_merge($_GET,$_POST);
		echo cajax::searchselect($obj,$request);
		
	}
	
	public function create_ajax_url() {
		return CAjaxMethod::factory()
                    ->set_type('callback')
                    ->set_data('callable', array('CFormInput62HallFamilyInput_DistrictsSelect', 'ajax'))
                    ->set_data('key_field', $this->key_field)
                    ->set_data('search_field', $this->search_field)
                    ->set_data('applyjs', $this->applyjs)
                    ->set_data('all', $this->all)
                    ->set_data('city_id', $this->city_id)
                    ->set_data('same_location_shipping_price', $this->same_location_shipping_price)
                    ->makeurl();
	}	

	public function html($indent=0){
		$db=CDatabase::instance();
		$html = new CStringBuilder();
		$html->set_indent($indent);
		if(strlen($this->value)==0){
			$q=$this->generate_query($this);
			$r=$db->query($q);
			if($r->count()>0){
				$row=$r[0];
				$this->value=$row->districts_id;
			}
		}
		$html->appendln(parent::html($indent));
		return $html->text();
	}
	
	public function js($indent=0) {
		$js = new CStringBuilder();
		$js->set_indent($indent);
		$js->append(parent::js($indent))->br();
		return $js->text();
	}	
	
 	
}