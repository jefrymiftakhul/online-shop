<?php

class CFormInput62HallFamilyInput_CountrySelect extends CFormInput62HallFamilyInput {
	protected $multiple;
	protected $applyjs;
	protected $province_select;
	protected $country_name;
	protected $all;
	protected $none;
	protected $value;
        
	public function __construct($id) {
		parent::__construct($id);
		
		$this->multiple=false;
		$this->type="select";
		$this->applyjs="select2";
		$this->province_select="";
		$this->country_name="";
		
		$this->all=true;
		$this->none=true;
	}
	
	public static function factory($id) {
		return new CFormInput62HallFamilyInput_CountrySelect($id);
	}
	public function set_multiple($bool) {
		$this->multiple = true;
		return $this;
	}
	public function set_applyjs($applyjs) {
		$this->applyjs = $applyjs;
		return $this;
	}
	public function set_all($bool) {
		$this->all = $bool;
		return $this;
	}
	public function set_none($bool) {
		$this->none = $bool;
		return $this;
	}
	public function set_province_select($name) {
		$this->province_select = $name;
		return $this;
	}
	public function set_country_name($country_name) {
		$this->country_name = $country_name;
		return $this;
	}
	
        public function set_value($value){
            $this->value = $value;
            return $this;
        }
        
	public function html($indent=0) {
		$html = new CStringBuilder();
		$html->set_indent($indent);
		$db = CDatabase::instance();
		$disabled = "";
		if($this->disabled) $disabled = ' disabled="disabled"';
		$multiple = "";
		if($this->multiple) $multiple = ' multiple="multiple"';
		$name = $this->name;
		if($this->multiple) $name=$name."[]";
		$classes = $this->classes;
		$classes = implode(" ",$classes);
		if(strlen($classes)>0) $classes=" ".$classes;
		$custom_css = $this->custom_css;
		$custom_css = crenderer::render_style($custom_css);
		if(strlen($custom_css)>0) {
			$custom_css = ' style="'.$custom_css.'"';
		}
		$html->appendln('<select name="'.$name.'" id="'.$this->id.'" class="select'.$classes.$this->validation->validation_class().'"'.$custom_css.$disabled.$multiple.'>')->inc_indent()->br();
		
		$q = "select country_id, name from country where status>0";
		$country_list = cdbutils::get_list($q);
                if ($this->all) {
                    $country_list = array("ALL" => "ALL") + $country_list;
                }
                
		foreach($country_list as $k=>$v) {
			$selected = "";
			if($this->value==$k) $selected= ' selected="selected"';
                        if(strlen($this->country_name)>0) {
                            $query = '  SELECT
                                            country_id
                                        FROM
                                            country
                                        WHERE
                                            name ='.$db->escape($this->country_name);
                            $this->value = cdbutils::get_value($query);
                        }
			$html->appendln('<option value="'.$k.'"'.$selected.'>'.$v.'</option>');
		}
		
		
		$html->dec_indent()->appendln('</select>')->br();
		
		//$html->appendln('<input type="text" name="'.$this->name.'" id="'.$this->id.'" class="input-unstyled'.$this->validation->validation_class().'" value="'.$this->value.'"'.$disabled.'>')->br();
		return $html->text();	
	}
	
	
	public function js($indent=0) {
		$all_str = "false";
		if($this->all) $all_str = "true";
		$js = new CStringBuilder();
		$js->set_indent($indent);
		$js->append(parent::js($indent))->br();
		
		$callback_js = "";
		if(strlen($this->province_select)>0) {
			$callback_js .= "
				if(jQuery('#".$this->province_select."').length>0) {
					refresh_".$this->province_select."();
				}
				
			";
		}
		if(strlen($this->province_select)>0) {
			$callback_js.="
				jQuery('#".$this->id."').change(function() {
					if(jQuery('#".$this->province_select."').length>0) {
						refresh_".$this->province_select."();
					}
				});
			";
		}
		
		
		$js->appendln("
			function callback_refresh_".$this->id."() {
				".parent::js()."
				".$callback_js."
			}
			

			function refresh_".$this->id."() {
				var cctrl_".$this->id." = jQuery('#".$this->id."').parent();
				cctrl_".$this->id.".html('<i class=\"icon-spinner icon-spin icon-large \"></i>');
				cctrl_".$this->id.".addClass('loading');
				var msub_id = '';
				jQuery.ajax({
					url:'".curl::base()."AdminSixtyTwoHallFamily/country_select',
					type:'get',
					dataType:'text',
					data: {
						'all':".$all_str.",
						'ctrlid':'".$this->id."',
						'country_id':'".$this->value."',
					}
				}).done(function(data){
					
					cctrl_".$this->id.".html(data);
					cctrl_".$this->id.".removeClass('loading');
					callback_refresh_".$this->id."()
				}).error(function(data) {
				
				});
			}
		");
		$js->appendln("
			refresh_".$this->id."();
		");
		
		return $js->text();
		
	}
}