<?php
/**
 * Description of HotelSelect
 *
 * @author M. Isman Subakti
 * @since 08 Dec 2015
 */
    class CFormInput62HallFamilyInput_HotelSelect extends CFormInputSelectSearch {

        public function __construct($id = '') {
            parent::__construct($id);

            // especially for hotel-select, because there is class form-control at bootstrap 3.3, so make UI will broken
            $this->bootstrap = '3';
            $this->query = "
              SELECT * 
              FROM (
                SELECT hotel_name, city_name, hotel_code as code, concat(hotel_name, ' (', city_name, ')') as format_result, status 
                FROM hotel 
                WHERE status > 0 AND hotel_code != ''
                  
                UNION ALL
                
                SELECT ct.name,  co.name, ct.code, concat(ct.name, ' (', ct.code, ')') as format_result,  ct.status 
                FROM city ct 
                INNER JOIN country co ON ct.country_id = co.country_id AND co.status > 0 
                WHERE ct.status > 0 AND ct.code != ''
                
                UNION ALL
                
                SELECT name, '' , code, name as format_result, status 
                FROM country 
                WHERE status > 0 AND code != ''
              ) as t
            ";
            $this->type = 'select';
            $this->placeholder = clang::__('Select Hotel Name, City, or Country of Destination');
            $this->format_result = '{format_result}';
            $this->format_selection = '{format_result}';
            $this->search_field = array('name');
            $this->key_field = 'code';
        }

        public static function factory($id = '') {
            return new CFormInput62HallFamilyInput_HotelSelect($id);
        }

        public static function ajax($data){
            $db = CDatabase::instance();

            $input = $_GET;
            $q = $data->query;
            $key_field = $data->key_field;
            $search_field = $data->search_field;
            $callback = "";
            $term = "";
            $limit = "";
            $page = "";

            if (isset($input["callback"])) {
                $callback = $input["callback"];
            }
            if (isset($input["term"])) {
                $term = $input["term"];
            }
            if (isset($input["limit"])) {
                $limit = $input["limit"];
            }
            if (isset($input["page"])) {
                $page = $input["page"];
            }

            $sOrder = "";
            $where_q1 = "";
            $where_q2 = "";
            $where_q3 = "";

            if (strlen($term) > 0 && (!empty($search_field))) {
                $where_q1 .= "AND `hotel_name` LIKE '%" . $db->escape_str($term) . "%'";
                $where_q2 .= "AND ct.`name` LIKE '%" . $db->escape_str($term) . "%'";
                $where_q3 .= "AND `name` LIKE '%" . $db->escape_str($term) . "%'";
            }

            $q_total = "select sum(total)
                        from (
                         SELECT count(*) as total
                         FROM hotel
                         WHERE status > 0 AND hotel_code != '' ".$where_q1." AND hotel_in_beds > 0

                         UNION ALL

                         SELECT count(*) as total
                         FROM city ct
                         INNER JOIN country co ON ct.country_id = co.country_id AND co.status > 0
                         WHERE ct.status > 0 AND ct.code != '' ".$where_q2."

                         UNION ALL

                         SELECT count(*) as total
                         FROM country
                         WHERE status > 0 AND code != '' ".$where_q3."
                        ) a";
            $total = cdbutils::get_value($q_total);
            /* Paging */
            $sLimit = "";
            if (strlen($limit) > 0) {
                if (strlen($page) > 0) {
                    $sLimit = "LIMIT " . ((intval($page) - 1) * 10) . ", " . intval($limit);
                }
                else {
                    $sLimit = "LIMIT " . intval($limit);
                }
            }

            /* Ordering */
            $sOrder = "";
            if (strlen($sOrder) > 0) {
                $sOrder = " ORDER BY " . $sOrder;
                $temp_order_by = '';
            }

            $qfilter = "
                      SELECT *
                      FROM (
                        (
						SELECT ct.name as hotel_name,  co.name, ct.code, concat(ct.name, ' (', 'city', ')') as format_result,  ct.status
                        FROM city ct
                        INNER JOIN country co ON ct.country_id = co.country_id AND co.status > 0
                        WHERE ct.status > 0 AND ct.code != '' ".$where_q2." "
                        .$sLimit ."
						)
                        UNION ALL
						(
                        SELECT hotel_name, city_name, hotel_code as code, concat(hotel_name, ' (', city_name, ')') as format_result, status
                        FROM hotel
                        WHERE status > 0 AND hotel_code != '' ".$where_q1." AND hotel_in_beds > 0 " 
                        .$sLimit ."
						)
                        UNION ALL
						(
                        SELECT name, '' , code, name as format_result, status
                        FROM country
                        WHERE status > 0 AND code != '' ".$where_q3." "
                        .$sLimit ."
						)
                      ) h
                      /**ORDER BY format_result ASC**/
                      ";
                      
            $r = $db->query($qfilter);

            $result = $r->result(false);

            $data = array();
            foreach ($r as $row) {
                $p = array();
                foreach ($row as $k => $v) {
                    $p[$k] = ($v == null) ? "" : $v;
                }
                $p["id"] = $row[$key_field];
                //$p["id"]=$row["item_id"];
                $data[] = $p;
            }
            $result = array();
            $result["data"] = $data;
            $result["total"] = $total;

            $response = "";
            $response.= $callback . "(";
            $response.= json_encode($result);
            $response.= ")";
            return $response;
        }

        public function create_ajax_url() {
            return CAjaxMethod::factory()
                ->set_type('callback')
                ->set_data('callable', array('CFormInput62HallFamilyInput_HotelSelect', 'ajax'))
                ->set_data('query', $this->query)
                ->set_data('key_field', $this->key_field)
                ->set_data('search_field', $this->search_field)
                ->makeurl();
        }
    }
    