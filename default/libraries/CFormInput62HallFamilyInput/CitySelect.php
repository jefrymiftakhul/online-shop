<?php

class CFormInput62HallFamilyInput_CitySelect extends CFormInputSelectSearch {
	protected $multiple;
	protected $applyjs;
	protected $all;
	protected $none;
	protected $province_id;
	protected $value;
	protected $page;
        protected $same_location_shipping_price;  
	
	public function __construct($id) {
		parent::__construct($id);
		
		$this->all=true;
		$this->none=true;
        $this->query = "select * from city where status>0";
        $this->type = "select";
		$this->applyjs='select2';
		$this->placeholder = clang::__('Select City');
		$this->format_result = '{name}';
		$this->format_selection = '{name}';
		$this->search_field = array('code','name');
		$this->key_field = 'city_id';
            $this->same_location_shipping_price=false;
	}
	
	public static function factory($id) {
		return new CFormInput62HallFamilyInput_CitySelect($id);
	}
	public function set_multiple($bool) {
		$this->multiple = true;
		return $this;
	}
	public function set_applyjs($applyjs) {
		$this->applyjs = $applyjs;
		return $this;
	}
	
	public function set_page($val){
		$this->page=$val;
		return $this;
	}

	public function set_all($bool) {
		$this->all = $bool;
		return $this;
		
	}
	public function set_none($bool) {
		$this->none = $bool;
		return $this;
		
	}

	public function set_province_id($id) {
		$this->province_id=$id;
		return $this;
	}
        
	public function set_value($value){
		$this->value = $value;
		return $this;
	}
        
        public function set_same_location_shipping_price($bol){
            $this->same_location_shipping_price=$bol;
            return $this;
        }
        
	public static function generate_query($data){
		$db=CDatabase::instance();
        $org_id=CF::org_id();
		$q =  "
			select 
				city_id,
				code,
				name
			from 
				city 
			where 
				status>0
		";
		if(strlen($data->province_id)){
			$q.="
				and province_id=".$db->escape($data->province_id)."
			";
		}
		if($data->page=='gold') {
			$q = "
                        select 
                            province_id as province_id,
                            city_id as city_id,
                            code as code,
                            name as name
                        from (
                            select
                                    province_id as province_id,
                                    city_id as city_id,
                                    code as code,
                                    name as name
                            from
                                city
                            where 
                                city_id in (375,353)
                            union all
                            select
                                province_id as province_id,
                                city_id as city_id,
                                code as code,
                                name as name
                            from 
                                city
                            where 
                                city_id in(204,203)
                                and 1=0
                        ) as temp_city
                        where
                            1=1
			";
                    if(strlen($data->province_id)){
                            $q.="
                                    and province_id=".$db->escape($data->province_id)."
                            ";
                    }
		}
                if($data->same_location_shipping_price){
                    $q="
                        select
                            c.city_id as city_id,
                            c.code as code,
                            c.name as name
                        from
                            shipping_price as sp
                            inner join city as c on c.city_id=sp.city_id
                        where
                            sp.status>0
                            and c.status>0
                            and sp.org_id=".$db->escape($org_id)."
                        group by 
                            c.city_id
                    ";
                }	
		if($data->all){
			$q="
				select
					'ALL' as city_id,
					'' as code,
					'ALL' as name
				union all
			".$q;
			
		}
		return $q;
	}
	public static function ajax($data) {
		$db=  CDatabase::instance();
		$obj = new stdclass();
		$q=self::generate_query($data);
		$data->query = $q;
		$obj->data = $data;
		$request = array_merge($_GET,$_POST);
		echo cajax::searchselect($obj,$request);
		
	}
	
	public function create_ajax_url() {
		return CAjaxMethod::factory()
						->set_type('callback')
						->set_data('callable', array('CFormInput62HallFamilyInput_CitySelect', 'ajax'))
						->set_data('key_field', $this->key_field)
						->set_data('search_field', $this->search_field)
						->set_data('applyjs', $this->applyjs)
						->set_data('all', $this->all)
						->set_data('province_id', $this->province_id)
						->set_data('page', $this->page)
                                                ->set_data('same_location_shipping_price', $this->same_location_shipping_price)
						->makeurl();
	}	

	public function html($indent=0){
		$db=CDatabase::instance();
		$html = new CStringBuilder();
		$html->set_indent($indent);
		if(strlen($this->value)==0){
			$q=$this->generate_query($this);
			$r=$db->query($q);
			if($r->count()>0){
				$row=$r[0];
				$this->value=$row->city_id;
			}
		}
		$html->appendln(parent::html($indent));
		return $html->text();
	}
	
	public function js($indent=0) {
		$js = new CStringBuilder();
		$js->set_indent($indent);
		$js->append(parent::js($indent))->br();
		return $js->text();
	}	
	
}