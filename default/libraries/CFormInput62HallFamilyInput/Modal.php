<?php

/**
 *
 * @author Raymond Sugiarto
 * @since  Mar 23, 2016
 */
class CFormInput62HallFamilyInput_Modal extends CFormInputModal
{

    public function __construct($id = "")
    {
        parent::__construct($id);

        $this->classes[] = 'modal-prelove';
    }

    public static function factory($id = '')
    {
        return new CFormInput62HallFamilyInput_Modal($id);
    }

}
