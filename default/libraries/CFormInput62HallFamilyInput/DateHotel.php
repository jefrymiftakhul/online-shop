<?php

/**
 * Description of DateDropdown
 *
 * @author Ecko Santoso
 * @since 07 Dec 15
 */
class CFormInput62HallFamilyInput_DateHotel extends CFormInput62HallFamilyInput {

    protected $format;
    protected $clear_button;
    protected $start_date;
    protected $end_date;
    protected $class;
    protected $callback_change_date;
    protected $callback_show_date;
    protected $listener;
    protected $param;
    protected $target_changed;
    protected $change_element;
    protected $target_element;


    public function __construct($id) {
        parent::__construct($id);
        $this->format = 'yyyy-mm-dd';
        $this->clear_button = false;
        $this->start_date = 0;
        $this->end_date = 0;
        $this->class = array();
        $this->callback_change_date = false;
        $this->callback_show_date = false;
        $this->listener = "";
        $this->param = "";
        $this->target_changed = "";
        $this->change_element = false;
        $this->target_element = false;
    }
    
    public static function factory($id) {
        return new CFormInput62HallFamilyInput_DateHotel($id);
    }
    
    public function set_clear_button($clear_button) {
        $this->clear_button = $clear_button;
        return $this;
    }

    public function set_start_date($start_date) {
        $this->start_date = date("Y-m-d", strtotime($start_date));
        return $this;
    }

    public function set_end_date($end_date) {
        $this->end_date = date("Y-m-d", strtotime($end_date));
        return $this;
    }

    public function add_class($param) {
        $this->class = $param;
        return $this;
    }

    public function set_callback_change_date($callback_change_date){
        $this->callback_change_date = $callback_change_date;
        return $this;
    }
    public function set_callback_show_date($callback_show_date){
        $this->callback_show_date = $callback_show_date;
        return $this;
    }
    
    public function set_listener($param) {
        if (strlen($param) > 0) {
            $listener_allowed = array('change');
            if (in_array($param, $listener_allowed)) {
                $this->listener = $param;
            }
            return $this;
        }
    }
    
    public function set_param($param) {
        $this->param = $param;
        return $this;
    }
    
    public function set_target_changed($param) {
        $this->target_changed = $param;
        return $this;
    }

    public function set_change_element($param) {
        $this->change_element = $param;
        return $this;
    }

    public function set_target_element($param){
        $this->target_element = $param;
        return $this;
    }
    
    public function html($indent = 0) {
        $html = new CStringBuilder();
        
//        $classes = $this->class;
//        $classes = implode(" ", $classes);
//        if (strlen($classes) > 0) {
//            $classes = " " . $classes;
//        }
        $container = $this->add_div()->add_class('input-group date ');
        $input_datepicker = $container->add_control($this->id.'-input', 'hidden')->add_class($this->class);
        $input_datepicker->add_class('form-control');
        
        $day_name = array('Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu');
        $month_name = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        
        $day = '';
        $date = '';
        $month = '';
        $year = '';
        $display_date = '';
        
        if (strlen($this->value) > 0) {
            $input_datepicker->set_value($this->value);
            
            $day = date('w', strtotime($this->value));
            $date = date('d', strtotime($this->value));
            $month = date('n', strtotime($this->value));
            $year = date('Y', strtotime($this->value));
            $display_date = $day_name[$day].', '.$date.' '.$month_name[$month-1].' '.$year;
        }
        $container->add('<div class="input-group-addon date-hotel-display-container" id="' . $this->id . '-display-container">
                            <span class=\'date-display\' id=\'' . $this->id . '-display\'>'.$display_date.'</span>
                        </div>');
        
        $html->append("
            <style>
            .input-group-addon.date-hotel-display-container {
                text-align: left;
                background: #fff;
                height: 35px;
                border-left: 1px solid #ccc !important;
                border-radius: 0;
                position: relative;
            }
            </style>
                ");
        
        $html->append(parent::html($indent));
        return $html->text();
    }
    
    public function js($indent = 0) {
        $js = new CStringBuilder();
        
        $js->appendln("
                var days = ['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu'];
                var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                
                $('#".$this->id."-input').parent().datepicker({
                        format: 'yyyy-mm-dd',
                        autoclose: true,
                        startDate  : '" . $this->start_date . "',
                    ");
            if ($this->end_date > 0) {
                $js->append("endDate  : '" . $this->end_date . "',");
            }
            if ($this->clear_button == true) {
                $js->append("clearBtn: true,");
            }
            $js->appendln("
                }).on('show', function(e){
                ");
            if ($this->callback_show_date == true) {
                $js->appendln("
                    callback_show_" . $this->id . "(jQuery(this));");
            }
            $js->appendln("
                }).on('changeDate', function(e){
                    var is_value = $('#".$this->id."-input').val();
                    if (is_value != '') {
                        var datend = new Date(is_value);
                        
                        var day_sel = datend.getDay();
                        var temp_date = is_value.split('-');
                        var date_sel = temp_date[2];
                        var month_sel = temp_date[1];
                        var year_sel = temp_date[0];
                        
                        var day_name = days[day_sel];
                        var month_name = months[month_sel-1];
                        
                        var display_date = day_name +', '+date_sel+' '+month_name+' '+year_sel;
                        $(this).parents().find('#" . $this->id . "-display').text(display_date);
                ");
            if (strlen($this->listener) > 0) {
                if (strlen($this->param) > 0) {
                    if (strlen($this->target_changed) > 0) {
                        $js->appendln("
                            var night_select = $('.".$this->param."').find('input[type=\"hidden\"]');
                            var Night = night_select.val();
                            var TempDate = new Date(is_value);
                            
                            var add_night = '+'+Night+'d';
                            var date2 = $('#".$this->id."-input').parent().datepicker('getDate', add_night); 
                            date2.setDate(date2.getDate()+parseInt(Night)); 


                            var date3 = $('#".$this->id."-input').parent().datepicker('getDate', '+1d'); 
                            date3.setDate(date3.getDate()+parseInt(1)); 

                            var date4 = $('#".$this->id."-input').parent().datepicker('getDate', '+31d'); 
                            date4.setDate(date4.getDate()+parseInt(31)); 

                            $('.".$this->target_changed."').parent().datepicker('setStartDate', date3);
                            $('.".$this->target_changed."').parent().datepicker('setEndDate', date4);
                            $('.".$this->target_changed."').parent().datepicker('setDate', date2);
                            ");
                    }
                    if ($this->change_element == true) {
                        $js->appendln("
                            var theDate = is_value;
                            var targetElementDate = $('.".$this->target_element."').val();
                            
                            var d_start = new Date(targetElementDate),
                                d_end   = new Date(theDate),
                                d_diff  = new Date(d_end - d_start),
                                d_days  = d_diff/1000/60/60/24;                           

                            $('.".$this->param." .input-select').val(d_days);
                        ");
                    }
                }                

            }

            // if($this->change_element == true){
            //     $js->appendln("
                  
            //     ");
            // }

            $js->appendln("            
                    }
                });
                ");
        
        $js->append(parent::js($indent));
        return $js->text();
    }
}
