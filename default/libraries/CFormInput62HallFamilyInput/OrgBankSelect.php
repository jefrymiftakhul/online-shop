<?php

class CFormInput62HallFamilyInput_OrgBankSelect extends CFormInputSelectSearch {
	protected $multiple;
	protected $applyjs;
	protected $all;
	protected $none;
	protected $value;
	protected $org_id;
	
        
	public function __construct($id) {
		parent::__construct($id);
		$this->org_id=CF::org_id();
		$this->all=false;
		$this->none=true;
        $this->query = "select * from org_bank where status>0";
        $this->type = "select";
        $this->applyjs = "select2";
		$this->placeholder = clang::__('Select Bank');
		$this->format_result = '{bank}-{account_number}-{account_holder}';
		$this->format_selection = '{bank}-{account_number}-{account_holder}';
		$this->search_field = array('name','accoutn_number','account_holder');
		$this->key_field = 'org_bank_id';
	}
	
	public static function factory($id) {
		return new CFormInput62HallFamilyInput_OrgBankSelect($id);
	}
	public function set_applyjs($applyjs) {
		$this->applyjs = $applyjs;
		return $this;
	}
	
	public function set_all($bool) {
		$this->all = $bool;
		return $this;
		
	}
	public function set_none($bool) {
		$this->none = $bool;
		return $this;
		
	}	
	
	public function set_org_id($id){
		$this->org_id = $id;
		return $this;
	}
	
	public function set_value($value){
		$this->value = $value;
		return $this;
	}
	
	public static function generate_query($data){
		$db=  CDatabase::instance();
		$q =  "
			select 
				org_bank_id,
				bank,
				account_number,
				account_holder
			from 
				org_bank 
			where 
				status>0
		";
		if(strlen($data->org_id)>0){
			$q.="
				and org_id=".$db->escape($data->org_id)."
			";
		}
		if($data->all){
			$q="
				select
					'ALL' as org_bank_id,
					'ALL' as bank,
					'ALL' as account_number,
					'ALL' as account_holder
				union all
			".$q;
			
		}
		return $q;
		
	}
	
 	public static function ajax($data) {
		$obj = new stdclass();
		$q=self::generate_query($data);
		$data->query = $q;
		$obj->data = $data;
		$request = array_merge($_GET,$_POST);
		echo cajax::searchselect($obj,$request);
		
	}
	
	public function create_ajax_url() {
		return CAjaxMethod::factory()
						->set_type('callback')
						->set_data('callable', array('CFormInput62HallFamilyInput_OrgBankSelect', 'ajax'))
						->set_data('key_field', $this->key_field)
						->set_data('search_field', $this->search_field)
						->set_data('applyjs', $this->applyjs)
						->set_data('all', $this->all)
						->set_data('org_id', $this->org_id)
						->makeurl();
	}	
	
	public function html($indent=0){
		$html = new CStringBuilder();
		$html->set_indent($indent);
		$db=CDatabase::instance();
		$q=$this->generate_query($this);
		$r=$db->query($q);
		if($r->count()>0){
			$row=$r[0];
			$this->value=$row->org_bank_id;
		}
		$html->appendln(parent::html($indent));
		return $html->text();
	}
	
	public function js($indent=0) {
		$js = new CStringBuilder();
		$js->set_indent($indent);
		$js->append(parent::js($indent))->br();
		return $js->text();
	}	
       
}