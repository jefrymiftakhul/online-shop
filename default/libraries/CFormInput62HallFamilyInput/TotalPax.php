<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Sep 18, 2015
     * @license http://piposystem.com Piposystem
     */
    class CFormInput62HallFamilyInput_TotalPax extends CFormInput62HallFamilyInput_Dropdown {

        public function __construct($id = '') {
            parent::__construct($id);

            $this->list = array(0, 1, 2, 3, 4, 5, 6, 7);
            $this->value = 0;
        }

        public static function factory($id) {
            return new CFormInput62HallFamilyInput_TotalPax($id);
        }
        
    }
    