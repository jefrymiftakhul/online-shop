<?php
    class CTransaction {
        protected $username='SYSTEM';
        protected $emails=array();
        public function __construct() {
            $app=CApp::instance();
            $user=$app->user();
            if($user){
                $this->username=$user->username;
            }
            $this->emails['transaction']=array();
        }
        
        
        public function close_product($product_id,$status_transaction){
            $db=CDatabase::instance();
            $data_update=array(
                'status_transaction'=>$status_transaction,
                'updated'=>date('Y-m-d H:i:s'),
                'updatedby'=>$this->username,
            );
            $db->update('product',$data_update,array('product_id'=>$product_id));
        }
        
        public function transaction_track($transaction_id,$from=null){
            $db=CDatabase::instance();
            $row=cdbutils::get_row("
                select
                    *
                from
                    transaction
                where
                    status>0
                    and transaction_id=".$db->escape($transaction_id)."
            ");
            if(!$row==null){
                //insert to transaction_track
                $data_transaction_track=array(
                    'transaction_id'=>$row->transaction_id,
                    'date_track'=>date('Y-m-d H:i:s'),
                    'status_transaction'=>$row->status_transaction,
                    'created'=>date('Y-m-d H:i:s'),
                    'createdby'=>$this->username,
                );
                switch($from){
                    case 'email' : {
                        $data_transaction['from_email']='1';
                        break;
                    }
                    case 'admin' : {
                        $data_transaction['from_admin']='1';
                        break;
                    }
                }
                $db->insert('transaction_track',$data_transaction_track);
            }
            
        }
        
        public function transaction($product_id){
            $db=CDatabase::instance();
            $row=cdbutils::get_row("
                select
                    *
                from
                    transaction_bid
                where
                    status>0
                    and product_id=".$db->escape($product_id)."
                order by 
                    transaction_date desc
                limit 1
            ");
            if(!$row==null){
                //insert to transaction
                $code=generate_code::get_next_transaction();
                $data_transaction=array(
                    'org_id'=>$row->org_id,
                    'member_id'=>$row->member_id,
                    'product_id'=>$row->product_id,
                    'transaction_date'=>date('Y-m-d H:i:s'),
                    'code'=>$code,
                    'reference_number'=>$row->transaction_bid_id,
//                    'currency_rate'=>$row->currency_rate,
                    'vendor_sell_price'=>$row->vendor_sell_price,
//                    'shipping_price'=>$row->shipping_price,
//                    'shipping'=>$row->shipping,
//                    'vendor_sell_price_total'=>$row->vendor_sell_price_total,
                    'transaction_status'=>'SUCCESS',
                    'created'=>date('Y-m-d H:i:s'),
                    'createdby'=>$this->username,
                );
                $r=$db->insert('transaction',$data_transaction);
                $transaction_id=$r->insert_id();
                //insert transaction contact
                $row_contact=cdbutils::get_row("
                    select
                        *
                    from
                        transaction_bid_contact
                    where
                        status>0
                        and transaction_bid_id=".$db->escape($row->transaction_bid_id)."
                ");
                if($row_contact){
                    $data_transaction_contact=array(
                        "transaction_id"=>$transaction_id,
                        "type"=>$row_contact->type,
                        "name"=>$row_contact->name,
                        "phone"=>$row_contact->phone,
                        "address"=>$row_contact->address,
                        "postal"=>$row_contact->postal,
                        "email"=>$row_contact->email,
                        "country_id"=>$row_contact->country_id,
                        "province_id"=>$row_contact->province_id,
                        "city_id"=>$row_contact->city_id,
                        "districts_id"=>$row_contact->districts_id,
                        "raja_ongkir_city_id"=>$row_contact->raja_ongkir_city_id,
                        "is_active"=>$row_contact->is_active,
                        "created"=>date('Y-m-d H:i:s'),
                        "createdby"=>$this->username,
                        "updated"=>date('Y-m-d H:i:s'),
                        "updatedby"=>$this->username,
                    );
                    $r=$db->insert('transaction_contact',$data_transaction_contact);
                }
                $this->transaction_track($transaction_id);
                $this->close_product($product_id,'SOLD');
                $this->emails['transaction'][]=$transaction_id;
            }
            return $this;
        }
        
        public function cron(){
            $db=CDatabase::instance();
            $err_code=0;
            $err_message="";
            $q="
                select
                    *
                from
                    product
                where
                    status>0
                    and (end_bid_date<=".$db->escape(date('Y-m-d H:i:s'))." or already_buy=1)
                    and status_transaction='DEFAULT'    
            ";        
            $r=$db->query($q);
			cdbg::var_dump($r);
			die();
            if($r->count()>0){
                $db->begin();
                try{
                    foreach($r as $row){
                        if((!$row->total_bid==null)||($row->total_bid>0)){
                            $this->transaction($row->product_id);                           
                        }else{
                            $this->close_product($row->product_id,'CLOSED');
                            //$mbh=member_balance::history('ReturnHold',$row->product_id);
                        }
                    }
                }catch(Exception $e ){
                    $err_code++;
                    $err_message='error!'. $e;
                }
                if($err_code==0){
                    $db->commit();
                }else{
                    $db->rollback();
                    throw new Exception($err_message);
                }
                if($err_code==0){
                    //send email
                    try{
                        foreach($this->emails as $key=>$val){
                            if(is_array($val)){
                                foreach($val as $id){
                                    email::send($key,$id);
                                }
                            }
                        }
                    }catch(Exception $e ){
                        throw new Exception($e);
                    }
                }
                                
            }
            
        }
        
        
    }
    