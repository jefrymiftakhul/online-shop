<?php

    class CAPIClientError implements ArrayAccess, Iterator, Countable{
        
        private $_errors;
        private $_current_row;
        private $_err_code;
        private $_err_message;
        private static $_instance;
        
        private $_list_error = array();
        
        private function __construct() {
            $this->_errors = array();
            $this->_current_row = 0;
            $this->_err_code = 0;
            
          
        }
        
        /**
         * 
         * @return CAPIError
         */
        public static function instance(){
            if (self::$_instance == NULL) {
                self::$_instance = new CAPIClientError();
            }
            return self::$_instance;
        }
        
        public function code(){
            return $this->_err_code;
        }
        
        public function get_err_message() {
            return $this->_err_message;
        }
        
        public function add_default($err_code){
            $this->add($this->_list_error[$err_code], $err_code);
            return $this;
        }

        public function add($err_message, $err_code = "9999"){
            $this->_err_code = $err_code;
            $this->_err_message = $err_message;
            if (is_array($err_message)){
                foreach ($err_message as $key => $value) {
                    $this->_errors[] = $value;
                }
            }
            else {
                $this->_errors[] = $err_message;
            }
            return $this;
        }
        
        public function set($err_code) {
            $this->add($this->_list_error[$err_code], $err_code);
            return $this;
        }

        public function render() {
            $html = "";
            foreach ($this->_errors as $err) {
                if ($html != "")
                    $html .= PHP_EOL;
                $html .= "" . $err . "";
            }
            return $html;
        }
        
        
        
        /**
         * Countable count
         * 
         * @return int
         */
        public function count() {
            return count($this->_errors);
        }

        public function current() {
            return $this->offsetGet($this->_current_row);
        }

        public function key() {
            return $this->_current_row;
        }

        public function next() {
            ++$this->_current_row;
            return $this;
        }
        
        public function prev() {
            --$this->_current_row;
            return $this;
        }

        public function offsetExists($offset) {
            return isset($this->_errors[$offset]);
        }

        public function offsetGet($offset) {
            if ($this->offsetExists($offset))
                return $this->_errors[$offset];
            return FALSE;
        }

        public function offsetSet($offset, $value) {
            $this->_errors[$offset] = $value;
        }

        public function offsetUnset($offset) {
            unset($this->_errors[$offset]);
        }

        public function rewind() {
            $this->_current_row = 0;
            return $this;
        }

        public function valid() {
            return $this->offsetExists($this->_current_row);
        }

    }
    