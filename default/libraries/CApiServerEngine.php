<?php
    class CApiServerEngine extends ApiServerEngine {

        public function __construct($api_server) {
            parent::__construct($api_server);
            $this->clear_curl();
        }

        public function clear_curl() {
            $this->curl = array();
        }

        public function set_url_request($url_request) {
            $this->url_request = $url_request;
            return $this;
        }

        public function get_curl() {
            return $this->curl;
        }

    }
    