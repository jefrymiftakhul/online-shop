<?php

class DigitalController extends SixtyTwoHallFamilyController {

    public function __construct() {
        $this->_page = "digital";
        parent::__construct();
        $app = CApp::instance();

        $app->add_js("
                $('#type').val('digital');
            ");
    }

}
