<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Jun 22, 2016
     */
    class EmailRouter {

        protected $type;
        protected $id;
        protected $debug;
        protected $engine;
        protected $options;

        protected function __construct($type, $id, $options = array()) {
            $this->type = $type;
            $this->id = $id;
            $this->options = $options;

            if ($this->type != null) {
                $engine_name = 'Email_' . ucfirst($this->type);
                $this->engine = $engine_name::factory($this, $id);
            }
        }

        public static function factory($type, $id, $options = array()) {
            return new EmailRouter($type, $id, $options);
        }
        
        public function resend($emails = null){
            if (is_string($emails)) {
                $emails = array($emails);
            }
            
            $err_code = 0;
            $err_message = '';
            
            $resend_mode = carr::get($this->options, 'mode', email::__APPEND);
            $list_track_detail_id = carr::get($this->options, 'email_tracker_detail_id');
            
            $db = CDatabase::instance();
            $email_tracker_id = $this->id;
            $q = "SELECT * 
                FROM email_tracker et
                INNER JOIN email_tracker_detail etd ON et.email_tracker_id = etd.email_tracker_id
                WHERE et.email_tracker_id = " .$db->escape($email_tracker_id);
            $r = $db->query($q);
            foreach ($r as $k => $v) {
                $email_tracker_detail_id = cobj::get($v, 'email_tracker_detail_id');
                
                if ($list_track_detail_id === null || in_array($email_tracker_detail_id, $list_track_detail_id)) {
                    $html_path = cobj::get($v, 'html_path');
                    $message = file_get_contents($html_path);
                    $subject = cobj::get($v, 'subject');
                    if ($resend_mode == email::__APPEND) {
                        $recipients = cobj::get($v, 'recipients');
                        $recipients = json_decode($recipients);
                        $email_to_be_send = $emails + $recipients;
                    }
                    else if ($resend_mode == email::__OVERWRITE) {
                        $email_to_be_send = $emails;
                    }
                    if (count($email_to_be_send) > 0) {
                        $status_resend = 'SUCCESS';
                        try {
                            cmail::send_smtp($email_to_be_send, $subject, $message);
                        }
                        catch (Exception $exc) {
                            $status_resend = 'FAILED';
                            $err_code++;
                            $err_message = clang::__('FAILED SEND');
                        }

                        
                        $email_recipt_tracker = array();
                        $email_recipt_tracker['email_tracker_id'] = $email_tracker_id;
                        $email_recipt_tracker['detail_type'] = 'RESEND';
                        $email_recipt_tracker['status_resend'] = $status_resend;
                        $email_recipt_tracker['subject'] = $subject;
                        $email_recipt_tracker['recipients'] = json_encode($email_to_be_send);
                        $email_recipt_tracker['html_path'] = $html_path;
                        $email_recipt_tracker['created'] = date("Y-m-d H:i:s");
                        $email_recipt_tracker['createdby'] = '';
                        $email_recipt_tracker['createdip'] = crequest::remote_address();
                        $email_recipt_tracker['updated'] = date("Y-m-d H:i:s");
                        $email_recipt_tracker['updatedby'] = '';
                        $email_recipt_tracker['updatedip'] = crequest::remote_address();
                        $email_recipt_tracker['status'] = '1';
                        $db->insert('email_tracker_detail', $email_recipt_tracker);
                    }
                }
            }
            if ($err_code > 0) {
                throw new Exception($err_message);
            }
            return true;
        }

        public function send() {
            $err_code = 0;
            $err_message = '';
            $results = array();
            try {
                $results = $this->engine->execute();
            }
            catch (Exception $exc) {
                $err_code++;
                $err_message = $exc->getMessage();
            }

            $status_email = 'SUCCESS';
            if ($err_code > 0) {
                $status_email = 'FAILED';
            }

            $db = CDatabase::instance();

            // create logs directory
            $html_full_path = APPPATH .'adminittronmall/default/logs/email_tracker/'.$this->type;
            if (!file_exists($html_full_path)) {
                $html_path = APPPATH .'adminittronmall/default/logs';
                @mkdir($html_path);
                $html_path .= '/email_tracker';
                @mkdir($html_path);
                $html_path .= '/' .$this->type;
                @mkdir($html_path);
                $html_full_path = $html_path;
            }
            
            $html_full_path .= '/' .date("Y");
            @mkdir($html_full_path);
            $html_full_path .= '/' .date("m");
            @mkdir($html_full_path);
            $html_full_path .= '/' .date("d");
            @mkdir($html_full_path);
            $html_full_path .= '/' .date("H");
            @mkdir($html_full_path);
            $html_full_path .= '/' .$this->id;
            @mkdir($html_full_path);
            
            
            try {
                $options = $this->options;
                if (is_array($this->options)) {
                    $options = json_encode($this->options);
                }

                $email_tracker = array();
                $email_tracker['org_id'] = CF::org_id();
                $email_tracker['type'] = $this->type;
                $email_tracker['key_id'] = $this->id;
                $email_tracker['options'] = $options;
                $email_tracker['status_email'] = $status_email;
                $email_tracker['created'] = date("Y-m-d H:i:s");
                $email_tracker['createdby'] = '';
                $email_tracker['createdip'] = crequest::remote_address();
                $email_tracker['updated'] = date("Y-m-d H:i:s");
                $email_tracker['updatedby'] = '';
                $email_tracker['updatedip'] = crequest::remote_address();
                $email_tracker['status'] = '1';
                $r_insert = $db->insert('email_tracker', $email_tracker);
                $email_tracker_id = $r_insert->insert_id();

                $html_full_path .= '/' .$email_tracker_id;
                @mkdir($html_full_path);
            
                foreach ($results as $key => $result) {
                    $html_path = $html_full_path .'/' .$this->type .'_' .date("YmdHis") .'_' .$this->id .'_' .$email_tracker_id .'_' .$key .'.log';
                    file_put_contents($html_path, carr::get($result, 'messages'));
                    
                    $recipients = carr::get($result, 'recipients');
                    if (is_string($recipients)) {
                        $recipients = array($recipients);
                    }
                    $recipients = json_encode($recipients);
                    $email_recipt_tracker = array();
                    $email_recipt_tracker['email_tracker_id'] = $email_tracker_id;
                    $email_recipt_tracker['subject'] = carr::get($result, 'subject');
                    $email_recipt_tracker['recipients'] = $recipients;
                    $email_recipt_tracker['html_path'] = $html_path;
                    $email_recipt_tracker['created'] = date("Y-m-d H:i:s");
                    $email_recipt_tracker['createdby'] = '';
                    $email_recipt_tracker['createdip'] = crequest::remote_address();
                    $email_recipt_tracker['updated'] = date("Y-m-d H:i:s");
                    $email_recipt_tracker['updatedby'] = '';
                    $email_recipt_tracker['updatedip'] = crequest::remote_address();
                    $email_recipt_tracker['status'] = '1';
                    $db->insert('email_tracker_detail', $email_recipt_tracker);
                }
            }
            catch (Exception $exc) {
                // dont add error
                //throw $exc;
            }
            
            
            if ($err_code > 0) {
                throw new Exception($err_message);
            }

            return $results;
        }

        public function get_debug() {
            return $this->debug;
        }

        public function set_debug($debug) {
            $this->debug = $debug;
            return $this;
        }

        public function get_options() {
            return $this->options;
        }

        public function set_options($options) {
            $this->options = $options;
            return $this;
        }

        public function get_type() {
            return $this->type;
        }

        public function set_type($type) {
            $this->type = $type;
            return $this;
        }

    }
    