<?php

    /**
     *
     * @author Seians0077
     * @since  Aug 19, 2014
     * @license http://piposystem.com Piposystem
     */
    class Cart62hall_Session {

        const __ExpiredSession = 30;

        private $_have_cookies = FALSE;
        private $_session_id;
        private $_data;
        private $_product_name;
        private $_session_path = NULL;
        private $_session_key = NULL;
        private static $_instance;
        private $_directory = NULL;

        public function __construct($product_name) {
            $session = CSession::instance();
            $this->_product_name = strtolower($product_name);
            $this->_session_key = '62hall_cart_' . $this->_product_name;
            $this->_session_id = $session->get($this->_session_key);
            $directory = CF::get_dir();
            $this->_directory = $directory;
            if ($this->_session_id != NULL && strlen($this->_session_id) > 0) {
                $dir_1 = substr($this->_session_id, 0, 8);
                $dir_2 = substr($this->_session_id, 8, 2);
                $this->_session_path = $this->_directory . "sessions" . DS . "cart" . DS . $this->_product_name . DS
                        . $dir_1 . DS . $dir_2 . DS;
                $this->load();
            }
            else {
                $this->_session_path = $this->_directory . "sessions" . DS . "cart" . DS . $this->_product_name . DS
                        . date("Ymd") . DS . date("H") . DS;
                $this->init();
            }
        }

        public function init() {
            $time_now = date("YmdHis");
            $prefix = $time_now;
            $prefix.= "_" . $this->_product_name . "_";
            $this->_session_id = uniqid($prefix);
            $session_path = "sessions" . DS . "cart" . DS . $this->_product_name . DS
                    . date("Ymd") . DS . date("H");
            $this->make_dir($session_path);
            $this->_data['session_id'] = $this->_session_id;
            $this->save();
            return $this;
        }

        private function make_dir($path) {
            $tmp_path = explode(DS, $path);
            $directory = $this->_directory;
            foreach ($tmp_path as $key => $t) {
                $directory .= $t . DS;
                if ((!is_dir($directory)) && !file_exists($directory))
                        mkdir($directory);
            }
        }

        public static function instance($product_name) {
            if (self::$_instance == null) {
                self::$_instance = array();
            }
            if (!isset(self::$_instance[$product_name])) {
                self::$_instance[$product_name] = new Cart62hall_Session($product_name);
            }
            return self::$_instance[$product_name];
        }

        public function clear() {
            $this->_data = array();
            $this->_data['session_id'] = $this->_session_id;
            $this->save();
            return $this;
        }

        public function regenerate() {
            $this->_session_path = $this->_directory . "sessions" . DS . "cart" . DS . $this->_product_name . DS
                    . date("Ymd") . DS . date("H") . DS;
            $this->_data = null;
            $this->init();
            return $this;
        }

        public function set($key, $val) {
            if (!isset($this->_data[$key]) || $this->_data[$key] != $val) {
                $this->_data[$key] = $val;
                $this->save();
            }
            return $this;
        }

        public function delete($key) {
            if (isset($this->_data[$key])) {
                unset($this->_data[$key]);
                $this->save();
            }
            return $this;
        }

        public function get($key, $default = NULL) {
            if (isset($this->_data[$key])) return $this->_data[$key];
            return $default;
        }

        public function save($data = NULL) {
            $session = CSession::instance();
            if ($data != NULL) $this->_data = $data;
            $filename = $this->_session_path . $this->_session_id . EXT;
            cphp::save_value($this->_data, $filename);
            $session->set($this->_session_key, $this->_session_id);
            return $this;
        }

        public function load() {
            $filename = $this->_session_path . $this->_session_id . EXT;
            if (!file_exists($filename)) {
                $this->regenerate();
                $filename = $this->_session_path . $this->_session_id . EXT;
            }
            $this->_data = cphp::load_value($filename);
            return $this;
        }

        public function data() {
            $session = CSession::instance();
            $session_data = $session->get($this->session_key);
            return $session_data;
        }

        public function session_id() {
            return $this->_session_id;
        }

        public function session_key() {
            return $this->session_key;
        }

    }
    