<?php

    /**
     *
     * @author Seians0077
     * @since  Aug 19, 2015
     * @license http://piposystem.com Piposystem
     */
    class Cart62hall_Product extends Cart62hall {

        protected function __construct() {
            parent::__construct('product');
        }

        public function factory() {
            return new Cart62hall_Product();
        }

    }
    