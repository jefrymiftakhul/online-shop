<?php

    require_once dirname(__FILE__) . DS . 'CResourcesEncode' . EXT;

    class CResourcesEngine {

        protected $_resource_type;
        protected $_type;
        protected $_org_code;
        protected $_sizes;
        protected $_filename;

        protected function __construct($resource_type, $type, $org_code = null) {
            $this->_resource_type = strtolower($resource_type);
            $this->_type = $type;
            $this->_org_code = $org_code;
            $this->_sizes = array();
        }

        public function save($file_name, $file_request, $optional_date = '') {
            $date_now = date("Y-m-d H:i:s");
            $dir = CF::get_dir('resources');

            $org_code = $this->_org_code;
            if ($org_code == null) $org_code = 'default';
            $dir .= $org_code . DS;
            if (!is_dir($dir)) {
                mkdir($dir);
            }

            $dir .= $this->_resource_type . DS;
            if (!is_dir($dir)) {
                mkdir($dir);
            }

            $dir .= $this->_type . DS;
            if (!is_dir($dir)) {
                mkdir($dir);
            }

            if (strlen($optional_date) == 0) {
                $optional_date = date('Ymd', strtotime($date_now));
            }
            $dir .= $optional_date . DS;

            if (!is_dir($dir)) {
                mkdir($dir);
            }

            $temp_file_name = $org_code . '_' . $this->_resource_type . "_" . $this->_type . "_" . $optional_date . "_" . $file_name;
            $path = $dir . $temp_file_name;
            file_put_contents($path, $file_request);
            $this->_filename = $temp_file_name;
            return $temp_file_name;
        }

        public function get_url($filename = null) {
            if ($filename == null) $filename = $this->_filename;

            $path = curl::base(false, 'http') . 'res/show/' . CResourcesEncode::encode($filename);
//            $file_name_encode = $this->encode($file_name,self::_digit);
            return $path;
        }

        public function add_size($size_name, $options) {
            $this->_sizes[$size_name] = $options;
        }

    }
    