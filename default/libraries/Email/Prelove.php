<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Jun 22, 2016
     */
    class Email_Prelove extends Email_Engine{
        
        
        protected function __construct($router, $id){
            parent::__construct($router, $id);
        }


        public static function factory($router, $id) {
            return new Email_Prelove($router, $id);
        }
        
        
        public function execute(){
            $db = CDatabase::instance();
            $q = 'SELECT * FROM topup WHERE status > 0 AND topup_id ='.$db->escape($this->id);
            $topup_data = cdbutils::get_row($q);
            if ($topup_data == null) {
                throw new Exception('ERR[TO01]' .clang::__('Topup does not exists'));
            }
            
            $emails = $this->get_emails($topup_data);
            
            try {
                foreach ($emails as $email_k => $email) {
                    $recipients = carr::get($email, 'recipients');
                    $message = carr::get($email, 'messages');
                    $subject = carr::get($email, 'subject');

                    if ($this->router->get_debug()) {
					  
                    }
                    else {
                        cmail::send_smtp($recipients, $subject, $message);
                    }
                }
            }
            catch (Exception $exc) {
                throw new Exception('ERR[EMAIL-1]' .$exc->getMessage());
            }
            return $emails;
        }
        
        protected function get_emails($topup_data){
            $db = CDatabase::instance();
            $status_topup = cobj::get($topup_data, 'status_confirm');
            $member_id = cobj::get($topup_data,'member_id');
            $member_data = cdbutils::get_row('SELECT * FROM member WHERE member_id ='.$db->escape($member_id));
            $member_email = cobj::get($member_data,'email');
            $member_name = cobj::get($member_data,'name');
            $code = cobj::get($topup_data,'code');
			
			$data = array();
            $data['org_id']=cobj::get($topup_data,'org_id');
            $data['member_email'] = cobj::get($member_data,'email');
            $data['member_name'] = cobj::get($member_data,'name');
            $data['payment_from'] = cobj::get($topup_data,'payment_from');
            $data['bank_name'] = cobj::get($topup_data,'bank_name');
            $data['code'] = $code;
            $data['nominal_real'] = cobj::get($topup_data,'nominal_real');
            $data['nominal'] = cobj::get($topup_data,'nominal');
            $data['note'] = cobj::get($topup_data,'note');
            $data['acc_no'] = cobj::get($topup_data,'acc_no');
            $data['acc_name'] = cobj::get($topup_data,'acc_name');
            $data['confirmed'] = cobj::get($topup_data,'confirmed');
            $emails = array();			
            switch ($status_topup) {
                case 'CONFIRMED':				  
                    $recipients = array($member_email);
                    $subject = '[62HallFamily] ' .clang::__('Confirmation Top Up').' | '.$code;
                    $message = $this->get_message('confirm-buyer', $data);
                    $email['recipients'] = $recipients;
                    $email['subject'] = $subject;
                    $email['messages'] = $message;
                    $emails[] = $email;
                    
                    $email_admin = ccfg::get('email_admin');
                    foreach ($email_admin as $key => $value) {
                        $recipients = array($value);
                        $subject = '[62HallFamily] ' .clang::__('Top Up Request from').' | '.$code.' | '.$member_name;
                        $message = $this->get_message('confirm-seller', $data);
                        $email['recipients'] = $recipients;
                        $email['subject'] = $subject;
                        $email['messages'] = $message;
                        $emails[] = $email;                        
                    }
                    break;
                case 'APPROVED':
                    $recipients = array($member_email);
                    $subject = '[62HallFamily] ' .clang::__('Approval Top Up').' | '.$code;
                    $message = $this->get_message('approved-buyer', $data);
                    $email['recipients'] = $recipients;
                    $email['subject'] = $subject;
                    $email['messages'] = $message;
                    $emails[] = $email;
                    break;
                case 'REJECTED':
//                    $recipients = array($member_email);
//                    $subject = '[LASTMENIT] ' .clang::__('Your product').' '.$product_name.clang::__('is on delivery');
//                    $message = $this->get_message('ondelivery-buyer', $data);
//                    $email['recipients'] = $recipients;
//                    $email['subject'] = $subject;
//                    $email['messages'] = $message;
//                    $emails[] = $email;
                    break;
                default:
                    break;
            }			
            return $emails;
        }
        
    }
    