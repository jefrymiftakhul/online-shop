<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Jun 22, 2016
     */
    class Email_Voucher extends Email_Engine{


        protected function __construct($router, $id){
            parent::__construct($router, $id);
        }


        public static function factory($router, $id) {
            return new Email_Voucher($router, $id);
        }


        public function execute(){
            $db = CDatabase::instance();
            $q_voucher = 'select tv.*, v.name as vendor_name, v.address as vendor_address ,p.name as product_name, p.description as description, p.description_voucher as terms, p.file_path_voucher as logo_merchant
                from transaction_voucher tv 
                inner join vendor v on tv.vendor_id = v.vendor_id 
                inner join product p on p.product_id = tv.product_id 
                where tv.status > 0 and tv.transaction_id ='.$db->escape($this->id);
            $data = $db->query($q_voucher);
            if (count($data) == 0) {
                throw new Exception('ERR[TR01]' .clang::__('Transaction Voucher does not exists'));
            }
            $emails = $this->get_emails($data);

            try {
                foreach ($emails as $email_k => $email) {
                    $recipients = carr::get($email, 'recipients');
                    $message = carr::get($email, 'messages');
                    $subject = carr::get($email, 'subject');

                    if ($this->router->get_debug()) {

                    }
                    else {
                        cmail::send_smtp($recipients, $subject, $message);
                    }
                }
            }
            catch (Exception $exc) {
                throw new Exception('ERR[EMAIL-1]' .$exc->getMessage());
            }
            return $emails;
        }

        protected function get_emails($data){
            $db = CDatabase::instance();

            $q = 'SELECT t.*,tc.* , p.name as product_name, p.end_bid_date as expired
                FROM transaction t
                  INNER join transaction_contact as tc on tc.transaction_id=t.transaction_id
                  INNER JOIN transaction_detail td ON t.transaction_id = td.transaction_id
                  INNER JOIN product p ON p.product_id = td.product_id
                  WHERE t.status > 0
                AND t.transaction_id ='.$db->escape($this->id);
            $transaction = cdbutils::get_row($q);
			if($transaction == null){
                throw new Exception('ERR[TR02]'.clang::__('Detail Transaction not found'));
            }

            $member_email = cobj::get($transaction, 'shipping_email');
            $member_name = cobj::get($transaction, 'shipping_first_name').' '.cobj::get($transaction, 'shipping_last_name');
            $product_name = cobj::get($transaction,'product_name');
			$org_id = cobj::get($transaction,'org_id');

            $data_email = array();
            $data_email['member_name'] = $member_name;
            $data_email['member_email'] = $member_email;
            $data_email['org_id'] = $org_id;
            $data_email['voucher'] = $data;
            $data_email['transaction'] = $transaction;

            $org_name = cdbutils::get_value('select name from org where status > 0 and org_id ='.$db->escape($org_id));
            
            $emails = array();
            $recipients = array($member_email);
            $subject = $org_name.' - '.clang::__('Order Sukses').' '.$product_name;
            $message = $this->get_message('voucher-buyer', $data_email);
            $email['recipients'] = $recipients;
            $email['subject'] = $subject;
            $email['messages'] = $message;
            $emails[] = $email;
            return $emails;
        }

    }
