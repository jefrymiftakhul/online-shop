<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Jun 22, 2016
     */
    class Email_PreloveTransaction extends Email_Engine{
        
        
        protected function __construct($router, $id){
            parent::__construct($router, $id);
        }


        public static function factory($router, $id) {
            return new Email_PreloveTransaction($router, $id);
        }
        
        
        public function execute(){
            $transaction = transaction::get($this->id);
            if ($transaction == null) {
                throw new Exception('ERR[TR01]' .clang::__('Transaction does not exists'));
            }
            $emails = $this->get_emails($transaction);
            
            try {
                foreach ($emails as $email_k => $email) {
                    $recipients = carr::get($email, 'recipients');
                    $message = carr::get($email, 'messages');
                    $subject = carr::get($email, 'subject');

                    if ($this->router->get_debug()) {

                    }
                    else {
                        cmail::send_smtp($recipients, $subject, $message);
                    }
                }
            }
            catch (Exception $exc) {
                throw new Exception('ERR[EMAIL-1]' .$exc->getMessage());
            }
            return $emails;
        }
        
        protected function get_emails($transaction){
            $db = CDatabase::instance();
            $status_transaction = cobj::get($transaction, 'transaction_status');
            $data = array(
                'transaction_id' => $this->id,
                'member_id' => cobj::get($transaction, 'member_id'),
            );
			
			$base_url = ccfg::get('domain');
            $member_email = cobj::get($transaction, 'member_email');
            $member_id = cobj::get($transaction, 'member_id');
            $member_name = cobj::get($transaction, 'member_name');
            $seller_email = cobj::get($transaction, 'seller_email');
            $seller_name = cobj::get($transaction, 'seller_name');
            $sell_price = cobj::get($transaction, 'vendor_sell_price');
            $product_name = cobj::get($transaction, 'name');
            $product_id = cobj::get($transaction, 'product_id');
            $image_name = cobj::get($transaction, 'image_name');
            $image_url = image::get_image_url($image_name, 'view_all');
            $product_url = cobj::get($transaction, 'url_key');
			$org_id = cobj::get($transaction,'org_id');
//            $key_ondelivery = $this->id.','.$product_id.','.$member_email.',ondelivery';
//            $key_settle = $this->id.','.$product_id.','.$member_email.',settle';
//            $secure_key_ondelivery = security::encrypt($key_ondelivery);
//            $secure_key_settle = security::encrypt($key_settle);
//            $url_encode_ondelivery = urlencode($secure_key_ondelivery);
//            $url_encode_settle = urlencode($secure_key_settle);
			$org_id = CF::org_id();
			
			$encrypted_transaction_id = security::encrypt($this->id);
			$url_encode = urlencode($encrypted_transaction_id);
			
            $data = array_merge($data,array(
				'org_id' => $org_id,
                'member_email' => $member_email,
                'member_name' => $member_name,
                'seller_name' => $seller_name,
                'product_name' =>$product_name,
                'org_id' =>$org_id,
                'image_name' => $image_name,
                'image_url' => $image_url,
                'sell_price' => ctransform::thousand_separator($sell_price),
//                'product_id' => $product_id,
//                'url_product' => 'http://'.$base_url.'/prelove/'.$product_url.'-'.$product_id,
                'url_product' => 'http://'.$base_url.'/prelove/updatecontact/update_contact/'.$url_encode,
//                'url_settle' => 'http://'.$base_url.'/member/trackOrder?key='.$url_encode_settle,
            ));
            $q = 'SELECT tc.*
                    FROM transaction_contact tc
                    where tc.status > 0 AND tc.transaction_id = '.$db->escape($this->id);
            $data_shipping = cdbutils::get_row($q);	
            
            $winner_name = cobj::get($data_shipping, 'shipping_first_name').' '.cobj::get($data_shipping, 'shipping_last_name');
            $winner_address = cobj::get($data_shipping, 'shipping_address');
            $winner_phone = cobj::get($data_shipping, 'shipping_phone');
            $winner_province = cobj::get($data_shipping, 'shipping_province');
            $winner_city = cobj::get($data_shipping, 'shipping_city');
            $winner_district = cobj::get($data_shipping, 'shipping_district');
            $winner_postal = cobj::get($data_shipping, 'shipping_postal_code');
            $data_seller = array(
				'org_id' => $org_id,
                'member_email' => $seller_email,
                'buyer_name' => $member_name,
                'winner_name' => $winner_name,
                'winner_address' => $winner_address,
                'winner_phone' => $winner_phone,
                'winner_province' => $winner_province,
                'winner_city' => $winner_city,
                'winner_district' => $winner_district,
                'winner_postal' => $winner_postal,
                'member_name' => $seller_name,
                'product_name' =>$product_name,
                'image_name' => $image_name,
                'image_url' => $image_url,
                'sell_price' => ctransform::thousand_separator($sell_price),
//                'product_id' => $product_id,
//                'url_product' => 'http://'.$base_url.'/'.$product_url.'-'.$product_id,
//                'url_ondelivery' => 'http://'.$base_url.'/member/trackOrder?key='.$url_encode_ondelivery,
            );
            $emails = array();
            switch ($status_transaction) {
				case 'PENDING':
					$recipients = array($member_email);
                    $subject = clang::__('Congratulations, You are a winner of').' '.$product_name;
                    $message = $this->get_message('winner-buyer', $data);
                    $email['recipients'] = $recipients;
                    $email['subject'] = $subject;
                    $email['messages'] = $message;
                    $emails[] = $email;
					break;
                case 'SUCCESS':
                    $recipients = array($seller_email);
                    $subject = clang::__('Congratulations, Your product winned by').' '.$member_name;
                    $message = $this->get_message('winner-seller', $data_seller);
                    $email['recipients'] = $recipients;
                    $email['subject'] = $subject;
                    $email['messages'] = $message;
                    $emails[] = $email;
                    break;
                case 'ONDELIVERY':
                    $recipients = array($member_email);
                    $subject = clang::__('Your product').' '.$product_name.clang::__('is on delivery');
                    $message = $this->get_message('ondelivery-buyer', $data);
                    $email['recipients'] = $recipients;
                    $email['subject'] = $subject;
                    $email['messages'] = $message;
                    $emails[] = $email;
                    break;
                case 'SETTLE':
                    $recipients = array($seller_email);
                    $subject = clang::__('Product').' '.$product_name.' | '.clang::__('has been received by').' : '.$member_name;
                    $message = $this->get_message('settle-seller', $data_seller);
                    $email['recipients'] = $recipients;
                    $email['subject'] = $subject;
                    $email['messages'] = $message;
                    $emails[] = $email;
                    break;
                case 'FAILED':
                    $data['reason_failed']=cobj::get($transaction,'reason_failed');
                    $recipients = array($member_email);
                    $subject = clang::__('Your transaction for').' '.$product_name.' '.clang::__('is failed');
                    $message = $this->get_message('failed-buyer', $data);
                    $email['recipients'] = $recipients;
                    $email['subject'] = $subject;
                    $email['messages'] = $message;
                    $emails[] = $email;
                    
                    $data_seller['reason_failed']=cobj::get($transaction,'reason_failed');
                    $recipients = array($seller_email);
                    $subject = clang::__('Your transaction for').' '.$product_name.' '.clang::__('is failed');
                    $message = $this->get_message('failed-seller', $data_seller);
                    $email['recipients'] = $recipients;
                    $email['subject'] = $subject;
                    $email['messages'] = $message;
                    $emails[] = $email;
                    break;
                default:
                    break;
            }
            return $emails;
        }
        
    }
    