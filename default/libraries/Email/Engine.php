<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Jun 22, 2016
     */
    class Email_Engine {
        
        
        protected $id;
        protected $router;
        protected $options;
        
        protected function __construct($router, $id){
            $this->router = $router;
            $this->id = $id;
            $this->options = $router->get_options();
        }
        
        protected function get_message($view_name, $data = array()) {
            
            // routing theme
            $theme = CF::theme();
            $theme_ready = array('62hallmall', 'Imbuilding');
            $path = '';
            if (strlen($theme) > 0) {
                if (in_array($theme, $theme_ready)) {
                    $path = $theme .'/';
                }
            }
            
            $type = $this->router->get_type();
            $type = strtolower($type);
            switch ($type) {
                case 'prelovetransactionbid':
                case 'prelovetransactionclosed':
                    $type = 'prelovetransaction';
                    break;
                default:
                    break;
            }
            
            $view = CView::factory($path. 'email/' .$type .'/' .$view_name);
            foreach ($data as $key => $value) {
                $view->$key = $value;
            }
            return $view->render();
        }
    }
    