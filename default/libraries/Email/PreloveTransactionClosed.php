<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Jun 22, 2016
     */
    class Email_PreloveTransactionClosed extends Email_Engine{
        
        
        protected function __construct($router, $id){
            parent::__construct($router, $id);
        }


        public static function factory($router, $id) {
            return new Email_PreloveTransactionClosed($router, $id);
        }
        
        public function execute(){
            $db = CDatabase::instance();
            $q = 'SELECT  m.name as member_name, m.email as member_email,
                p.*
                FROM product p
                INNER JOIN member m ON p.member_id = m.member_id
                WHERE p.product_id ='.$db->escape($this->id).' AND p.status > 0';
            $r = cdbutils::get_row($q);
            if($r == null){
                throw new Exception('ERR[TR01] ' .clang::__('Product does not exists'));
            }
            
            $status_transaction = cobj::get($r, 'status_transaction');
            if($status_transaction != 'CLOSED'){
                throw new Exception('ERR[TR02] ' .clang::__('Status transaction is not closed'));
            }
            
            $q_bid = 'SELECT COUNT(transaction_bid_id) FROM transaction_bid WHERE product_id = '.$db->escape($this->id).' AND status > 0';
            $r_bid = cdbutils::get_value($q_bid);
			
            if($r_bid > 0){
                throw new Exception('ERR[TR03] ' .clang::__('This product has more than one bid'));
            }
            
            $emails = array();
			$org_id = CF::org_id();
            // email to user bid
            $member_email = cobj::get($r, 'member_email');
            $member_name = cobj::get($r, 'member_name');
            $sell_price = cobj::get($r, 'start_price');
            $product_name = cobj::get($r, 'name');
            $product_id = cobj::get($r, 'product_id');
            $image_name = cobj::get($r, 'image_name');
            $image_url = image::get_image_url($image_name, 'view_all');
            $data = array(
                'org_id' => $org_id,
                'member_email' => $member_email,
                'member_name' => $member_name,
                'image_name' => $image_name,
                'image_url' => $image_url,
                'sell_price' => ctransform::thousand_separator($sell_price),
                'product_name' => $product_name,
                'product_id' => $product_id,
            );
            $recipients = array($member_email);
            $subject = '[LASTMENIT] ' .clang::__('Product Closed without Winner');
            $message = $this->get_message('closed-seller', $data);
            $email['recipients'] = $recipients;
            $email['subject'] = $subject;
            $email['messages'] = $message;
            $emails[] = $email;
            try {
                foreach ($emails as $email_k => $email) {
                    $recipients = carr::get($email, 'recipients');
                    $message = carr::get($email, 'messages');
                    $subject = carr::get($email, 'subject');
                    // $recipients[] = 'khensin.imvu@gmail.com';
                    if ($this->router->get_debug()) {

                    }
                    else {
                        cmail::send_smtp($recipients, $subject, $message);
                    }
                }
            }
            catch (Exception $exc) {
//                die($exc->getMessage());
                throw new Exception('ERR[EMAIL-1] ' .$exc->getMessage());
            }
            return $emails;
        }
        
        
    }
    