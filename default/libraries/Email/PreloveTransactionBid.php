<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Jun 22, 2016
     */
    class Email_PreloveTransactionBid extends Email_Engine{
        
        
        protected function __construct($router, $id){
            parent::__construct($router, $id);
        }


        public static function factory($router, $id) {
            return new Email_PreloveTransactionBid($router, $id);
        }
        
        public function execute(){
            $transaction = transaction::get_bid($this->id);
            if ($transaction == null) {
                throw new Exception('ERR[TR01] ' .clang::__('Transaction does not exists'));
            }
            $transaction_lost = null;
            $transaction_bid_ref_id = cobj::get($transaction, 'transaction_bid_ref_id');
            if (strlen($transaction_bid_ref_id) > 0) {
                $transaction_lost = transaction::get_bid($transaction_bid_ref_id);
            }
            $emails = array();
            // email to user bid
            $email_bid = cobj::get($transaction, 'member_email');
            $sell_price = cobj::get($transaction, 'vendor_sell_price');
            $product_name = cobj::get($transaction, 'name');
            $product_id = cobj::get($transaction, 'product_id');
            $image_name = cobj::get($transaction, 'image_name');
            $image_url = image::get_image_url($image_name, 'view_all');
            $data = array(
                'member_email' => $email_bid,
                'member_name' => cobj::get($transaction, 'member_name'),
                'name' => cobj::get($transaction, 'name'),
                'image_name' => $image_name,
                'image_url' => $image_url,
                'org_id'=>cobj::get($transaction,'bid_org'),
                'sell_price' => ctransform::thousand_separator($sell_price),
                'product_name' => $product_name,
                'product_id' => $product_id,
            );
            $recipients = array($email_bid);
            $subject = clang::__('Email Product Offer Successfully');
            $message = $this->get_message('offer-buyer', $data);
            $email['recipients'] = $recipients;
            $email['subject'] = $subject;
            $email['messages'] = $message;
            $emails[] = $email;
            // email to user lost bid
            if ($transaction_lost != null) {
                $email_lost_bid = cobj::get($transaction_lost, 'member_email');
                $sell_price = cobj::get($transaction_lost, 'vendor_sell_price');
                $product_name = cobj::get($transaction_lost, 'name');
                $product_url = cobj::get($transaction_lost, 'url_key');
                $product_id = cobj::get($transaction_lost, 'product_id');
                $image_name = cobj::get($transaction_lost, 'image_name');
                $image_url = image::get_image_url($image_name, 'view_all');
                $data = array(
                    'member_email' => $email_lost_bid,
                    'member_name' => cobj::get($transaction_lost, 'member_name'),
                    'name' => cobj::get($transaction, 'name'),
                    'image_name' => $image_name,
                    'image_url' => $image_url,
                    'org_id'=>cobj::get($transaction,'bid_org'),
                    'sell_price' => ctransform::thousand_separator($sell_price),
                    'product_name' => $product_name,
                    'product_id' => $product_id,
                    'url_product' => curl::base().'prelove/'.$product_url.'-'.$product_id,
                );
                $recipients = array($email_lost_bid);
                $subject = clang::__('Email Product Lost Offer');
                $message = $this->get_message('lost-offer-buyer', $data);
                $email['recipients'] = $recipients;
                $email['subject'] = $subject;
                $email['messages'] = $message;
                $emails[] = $email;
            }
            
            try {
                foreach ($emails as $email_k => $email) {
                    $recipients = carr::get($email, 'recipients');
                    $message = carr::get($email, 'messages');
                    $subject = carr::get($email, 'subject');
                    // $recipients[] = 'khensin.imvu@gmail.com';
                    if ($this->router->get_debug()) {

                    }
                    else {
                        cmail::send_smtp($recipients, $subject, $message);
                    }
                }
            }
            catch (Exception $exc) {
//                die($exc->getMessage());
                throw new Exception('ERR[EMAIL-1] ' .$exc->getMessage());
            }
            return $emails;
        }
        
        
    }
    