<?php

    class Email_IMRegister extends Email_Engine{
        
        
        protected function __construct($router, $id){
            parent::__construct($router, $id);
        }


        public static function factory($router, $id) {
            return new Email_IMRegister($router, $id);
        }
        
        public function execute(){
            $db = CDatabase::instance();
            $member_data = cdbutils::get_row('select * from member where member_id = '.$db->escape($this->id).' and status > 0');
            if ($member_data == null) {
                throw new Exception('ERR[RM01] ' .clang::__('Member does not exists'));
            }
            $base_url=ccfg::get('front_end_domain');
            if($base_url==null){
                $base_url=curl::httpbase();
            }else{
                $base_url='http://'.$base_url.'/';
            }
            $email_type=carr::get($this->options,'email_type');
            $is_verified = cobj::get($member_data, 'is_verified');
            $is_approved = cobj::get($member_data, 'is_approved');
            $org_id= cobj::get($member_data,'org_id');
            $org=org::get_org($org_id);
            $org_name=cobj::get($org,'name');
			
            $emails = array();
            if($email_type==null){
                if($is_verified == 0){
                    $email_type='verification';
                }
                if($is_verified == 1 && $is_approved == 0){
                    $email_type='registration';
                }
                if($is_verified == 1 && $is_approved == 1){
                    $email_type='activation';
                }
            }
            $member_email = cobj::get($member_data,'email');
            $member_name = cobj::get($member_data,'name');
            
            $data = array(
                'org_id'=>$org_id,
                'org_name'=>$org_name,
                'member_name' => $member_name,
                'member_email' => $member_email,
                'link_verification' => verify_email::generate_link($this->id),
            );
            $recipients = array($member_email);
            //if register
            if($email_type=='verification'){
                // email to user
                $subject = '['.$org_name.'] ' .clang::__('Verification');
                $message = $this->get_message('verification-member', $data);
            }
            if($email_type=='registration'){
                // email to user
                $subject = '['.$org_name.'] ' .clang::__('Registration');
                $message = $this->get_message('register-member', $data);
            }

            foreach($recipients as $val){
                $email['recipients'] = $val;
                $email['subject'] = $subject;
                $email['messages'] = $message;
                $emails[]=$email;
            }
            try {
                foreach ($emails as $email_k => $email) {
                    $recipients = carr::get($email, 'recipients');
                    $message = carr::get($email, 'messages');
                    $subject = carr::get($email, 'subject');

                    if ($this->router->get_debug()) {

                    }
                    else {
                        cmail::send_smtp($recipients, $subject, $message);
                    }
                }
            }
            catch (Exception $exc) {
//                die($exc->getMessage());
                throw new Exception('ERR[EMAIL-1] ' .$exc->getMessage());
            }
            return $emails;
        }
        
        
    }
    