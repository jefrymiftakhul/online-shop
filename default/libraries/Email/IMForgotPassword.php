<?php

    class Email_IMForgotPassword extends Email_Engine{
        
        
        protected function __construct($router, $id){
            parent::__construct($router, $id);
        }


        public static function factory($router, $id) {
            return new Email_IMForgotPassword($router, $id);
        }
        
        public function execute(){
            $db = CDatabase::instance();
            $forgot_password = cdbutils::get_row('select * from request_forgot_password where request_forgot_password_id = '.$db->escape($this->id).' and status > 0');
            if ($forgot_password == null) {
                throw new Exception('ERR[RM01] ' .clang::__('Request Forgot Password does not exists'));
            }
            $base_url=ccfg::get('front_end_domain');
            if($base_url==null){
                $base_url=curl::httpbase();
            }else{
                $base_url='http://'.$base_url.'/';
            }
            $email_type=carr::get($this->options,'email_type');
            $member_id=cobj::get($forgot_password,'member_id');
            $member_data=cdbutils::get_row('select * from member where member_id='.$db->escape($member_id));
            $org_id= cobj::get($member_data,'org_id');
            $org=org::get_org($org_id);
            $org_name=cobj::get($org,'name');
			
            $emails = array();
            $member_email = cobj::get($member_data,'email');
            $member_name = cobj::get($member_data,'first_name').' '.cobj::get($member_data,'last_name');
            $token=cobj::get($forgot_password,'token');
            $url=$base_url.'member/forgotpassword/change/' . $token;
            $data = array(
                'org_id'=>$org_id,
                'member_name' => $member_name,
                'member_email' => $member_email,
                'url'=>$url,
            );
            $recipients = array($member_email);
            $subject = '['.$org_name.'] ' .clang::__('Request Forgot Password');
            $message = $this->get_message('request-forgot-password', $data);

            foreach($recipients as $val){
                $email['recipients'] = $val;
                $email['subject'] = $subject;
                $email['messages'] = $message;
                $emails[]=$email;
            }
            try {
                foreach ($emails as $email_k => $email) {
                    $recipients = carr::get($email, 'recipients');
                    $message = carr::get($email, 'messages');
                    $subject = carr::get($email, 'subject');

                    if ($this->router->get_debug()) {

                    }
                    else {
                        cmail::send_smtp($recipients, $subject, $message);
                    }
                }
            }
            catch (Exception $exc) {
//                die($exc->getMessage());
                throw new Exception('ERR[EMAIL-1] ' .$exc->getMessage());
            }
            return $emails;
        }
        
        
    }
    