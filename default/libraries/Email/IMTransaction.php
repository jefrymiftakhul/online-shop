<?php

class Email_IMTransaction extends Email_Engine {

    protected function __construct($router, $id) {
        parent::__construct($router, $id);
    }

    public static function factory($router, $id) {
        return new Email_IMTransaction($router, $id);
    }

    public function execute() {
        $db = CDatabase::instance();
        $q = "
                select
                    t.org_id as org_id,
                    t.booking_code as booking_code,
                    t.date as date_transaction,
                    t.transaction_status as status_transaction,
                    t.total_shipping as total_shipping,
                    t.voucher_amount as voucher_amount,
                    t.date_arrived as date_arrived,
                    tp.payment_type as payment_type,
                    tp.payment_status as status_payment,
                    tp.bank as bank,
                    tp.account_number as account_number,
                    tc.billing_email as billing_email,
                    tc.billing_first_name as billing_first_name,
                    tc.billing_last_name as billing_last_name,
                    tc.billing_address as billing_address,
                    tc.billing_province as billing_province,
                    tc.billing_city as billing_city,
                    tc.billing_phone as billing_phone,
                    tc.shipping_first_name as shipping_first_name,
                    tc.shipping_last_name as shipping_last_name,
                    tc.shipping_address as shipping_address,
                    tc.shipping_province as shipping_province,
                    tc.shipping_city as shipping_city,
                    tc.shipping_phone as shipping_phone
                from
                    transaction as t
                    inner join transaction_contact as tc on tc.transaction_id=t.transaction_id
                    left join transaction_payment as tp on tp.transaction_id=t.transaction_id
                where
                    t.status>0
                    and tc.status>0
                    #and tp.status>0
                    and t.transaction_id=" . $db->escape($this->id) . "
            ";

        $transaction = cdbutils::get_row($q);

        $q = "
                select
                    td.transaction_detail_id,
                    p.product_id,
                    p.parent_id,
                    p.is_package_product,
                    case when p.product_id is null then td.product_name else p.name end as product_name,
                    td.qty as qty,
                    td.ho_sell_price as sell_price,
                    td.channel_sell_price as sell_price_promo

                from
                    transaction_detail as td
                    left join product as p on p.product_id=td.product_id
                where
                    td.status>0
                    and td.transaction_id=" . $db->escape($this->id) . "
            ";
        $transaction_detail = $db->query($q);
        $emails = $this->get_emails($transaction, $transaction_detail);

        try {
            foreach ($emails as $email_k => $email) {
                $recipients = carr::get($email, 'recipients');
                $message = carr::get($email, 'messages');
                $subject = carr::get($email, 'subject');

                if ($this->router->get_debug()) {
                    
                } else {
                    cmail::send_smtp($recipients, $subject, $message);
                    // $headers = "MIME-Version: 1.0" . "\r\n";
                    // $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                    // mail($recipients[0], $subject, $message, $headers);
                }
            }
        } catch (Exception $exc) {
            throw new Exception('ERR[EMAIL-1]' . $exc->getMessage());
        }
        return $emails;
    }

    protected function get_emails($transaction_data, $transaction_detail) {
        $db = CDatabase::instance();
        $org_id = cobj::get($transaction_data, 'org_id');
        $org = org::get_org($org_id);
        $base_url = cobj::get($org, 'domain');

        if ($base_url == null) {
            $base_url = curl::httpbase();
        } else {
            $base_url = 'http://' . $base_url . '/';
        }

        $booking_code = cobj::get($transaction_data, 'booking_code');
        $date_transaction = cobj::get($transaction_data, 'date_transaction');
        $date_arrived = cobj::get($transaction_data, 'date_arrived');
        $status_transaction = cobj::get($transaction_data, 'status_transaction');
        $payment_type = cobj::get($transaction_data, 'payment_type');
        $status_payment = cobj::get($transaction_data, 'status_payment');
        $total_shipping = cobj::get($transaction_data, 'total_shipping');
        $bank = cobj::get($transaction_data, 'bank');
        $account_number = cobj::get($transaction_data, 'account_number');
        $voucher_amount = cobj::get($transaction_data, 'voucher_amount');

        $billing_email = cobj::get($transaction_data, 'billing_email');
        $billing_first_name = cobj::get($transaction_data, 'billing_first_name');
        $billing_last_name = cobj::get($transaction_data, 'billing_last_name');
        $billing_name = $billing_first_name . ' ' . $billing_last_name;
        $billing_address = cobj::get($transaction_data, 'billing_address');
        $billing_province = cobj::get($transaction_data, 'billing_province');
        $billing_city = cobj::get($transaction_data, 'billing_city');
        $billing_phone = cobj::get($transaction_data, 'billing_phone');

        $shipping_first_name = cobj::get($transaction_data, 'shipping_first_name');
        $shipping_last_name = cobj::get($transaction_data, 'shipping_last_name');
        $shipping_name = $shipping_first_name . ' ' . $shipping_last_name;
        $shipping_address = cobj::get($transaction_data, 'shipping_address');
        $shipping_province = cobj::get($transaction_data, 'shipping_province');
        $shipping_city = cobj::get($transaction_data, 'shipping_city');
        $shipping_phone = cobj::get($transaction_data, 'shipping_phone');
        $arr_detail = array();
//            foreach($transaction_detail as $row_td){
//                $product_name=cobj::get($row_td,'product_name');
//                $sell_price=cobj::get($row_td,'sell_price');
//                $sell_price=cobj::get($row_td,'sell_price_promo');
//                $qty=cobj::get($row_td,'qty');
//            }

        $data = array();
        $data['org_id'] = $org_id;
        $data['booking_code'] = $booking_code;
        $data['date_transaction'] = $date_transaction;
        $data['status_transaction'] = $status_transaction;
        $data['payment_type'] = $payment_type;
        $data['bank_detail'] = ' - ' . $bank . ' ' . $account_number;
        $data['status_payment'] = $status_payment;
        $data['total_shipping'] = $total_shipping;
        $data['voucher_amount'] = $voucher_amount;
        $data['billing_email'] = $billing_email;
        $data['billing_name'] = $billing_name;
        $data['billing_address'] = $billing_address;
        $data['billing_province'] = $billing_province;
        $data['billing_city'] = $billing_city;
        $data['billing_phone'] = $billing_phone;
        $data['shipping_name'] = $shipping_name;
        $data['shipping_address'] = $shipping_address;
        $data['shipping_province'] = $shipping_province;
        $data['shipping_city'] = $shipping_city;
        $data['shipping_phone'] = $shipping_phone;
        $data['detail'] = $transaction_detail;

        if (ccfg::get('no_web_front') === null || ccfg::get('no_web_front') == 0) {
            $data['button_belanja_lagi'] = true;
        }

        $emails = array();
        switch ($status_transaction) {
            case 'PENDING': {
                    $recipients = array($billing_email);
                    $subject = 'Order - ' . $booking_code;
                    if (ccfg::get('no_web_front') === null || ccfg::get('no_web_front') == 0) {
                        if ($payment_type == 'bank_transfer') {
                            $data['url_payment_confirmation'] = $base_url . 'payment/payment_confirmation/' . $this->id;
                        }
                    }
                    $data['message'] = "
                        <p>Kami telah menerima pesanan Anda. Kami akan memproses pesanan Anda setelah pembayaran di konfirmasi</p>
                    ";
                    $message = $this->get_message('order', $data);
                    break;
                }
            case 'CONFIRMED': {
                    $recipients = array($billing_email);
                    $subject = 'Konfirmasi Pembayaran - ' . $booking_code;
                    $data['message'] = "
                        <p>Kami telah menerima konfirmasi pembayaran Anda. Kami akan memproses konfirmasi pembayaran Anda</p>
                    ";
                    $message = $this->get_message('order', $data);
                    break;
                }
            case 'SUCCESS': {
                    $recipients = array($billing_email);
                    $subject = 'Pembayaran Sukses - ' . $booking_code;
                    $data['message'] = "
                        <p>Kami telah menerima pembayaran Anda. Kami akan segera mengirim pesanan Anda ke alamat pengiriman</p>
                    ";
                    $message = $this->get_message('order', $data);
                    break;
                }
            case 'ON_SHIPPING': {
                    $recipients = array($billing_email);
                    $subject = 'Pengiriman - ' . $booking_code;
                    $data['message'] = "
                        <p>Pesanan Anda telah kami kirim ke alamat pengiriman. Harap menunggu pesanan Anda tiba setelah proses pengiriman selesai</p>
                    ";
                    $message = $this->get_message('order', $data);
                    break;
                }
            case 'DELIVERED': {
                    $recipients = array($billing_email);
                    $subject = 'Terkirim - ' . $booking_code;
                    $data['message'] = "
                        <p>Pesanan Anda telah diterima pada : " . ctransform::format_datetime($date_arrived) . "</p>
                    ";
                    $message = $this->get_message('order', $data);
                    break;
                }
            case 'REQUEST': {
                    $recipients = array($billing_email);
                    $subject = 'Pre Order - ' . $booking_code;
                    $data['request'] = '1';
                    $data['message'] = "
                        <p>Request Pesanan Anda telah kami terima.  Harap menunggu konfirmasi dari kami</p>
                    ";
                    $message = $this->get_message('order', $data);
                    break;
                }
            case 'APPROVED': {
                    $recipients = array($billing_email);
                    $subject = 'Pre Order Tersedia - ' . $booking_code;
                    $data['request'] = '1';
                    $data['link_payment'] = $base_url . "/set_session_transaction_from_database/" . $this->id;
                    $data['message'] = "
                        <p>Request Pesanan Anda telah kami terima.  Harap menunggu konfirmasi dari kami</p>
                    ";
                    $message = $this->get_message('order', $data);
                    break;
                }
            case 'REJECT': {
                    $recipients = array($billing_email);
                    $subject = 'Pre Order Tidak Tersedia - ' . $booking_code;
                    $data['request'] = '1';
                    $data['message'] = "
                        <p>Mohon maaf request pesanan Anda untuk saat ini tidak tersedia</p>
                        <p>Request barang pesanan yang Anda cari adalah sebagai berikut :</p>
                    ";
                    $message = $this->get_message('order', $data);
                    break;
                }
            case 'FAILED': {
                    $recipients = array($billing_email);
                    $subject = 'Pembayaran Gagal - ' . $booking_code;
                    $data['message'] = "
                        <p>Mohon maaf request pembayaran Anda untuk pesanan ini gagal.</p>
                    ";
                    $message = $this->get_message('order', $data);
                    break;
                }
            default:
                break;
        }
        $email['recipients'] = $recipients;
        $email['subject'] = $subject;
        $email['messages'] = $message;
        $emails[] = $email;
        return $emails;
    }

}
