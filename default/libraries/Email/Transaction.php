<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Jun 22, 2016
     */
    class Email_Transaction extends Email_Engine{
        
        
        protected function __construct($router, $id){
            parent::__construct($router, $id);
        }


        public static function factory($router, $id) {
            return new Email_Transaction($router, $id);
        }
        
        
        public function execute(){
            $db = CDatabase::instance();
            $q = 'SELECT t.*,tc.* , p.name as product_name
				FROM transaction t 
                                inner join transaction_contact as tc on tc.transaction_id=t.transaction_id
			  INNER JOIN transaction_detail td ON t.transaction_id = td.transaction_id 
			  INNER JOIN product p ON p.product_id = td.product_id
			  WHERE t.status > 0 
				AND t.transaction_id ='.$db->escape($this->id);
            $transaction_data = cdbutils::get_row($q);			
            if ($transaction_data == null) {
                throw new Exception('ERR[TO01]' .clang::__('Product does not exists'));
            }
            
            $emails = $this->get_emails($transaction_data);
            
            try {
                foreach ($emails as $email_k => $email) {
                    $recipients = carr::get($email, 'recipients');
                    $message = carr::get($email, 'messages');
                    $subject = carr::get($email, 'subject');

                    if ($this->router->get_debug()) {
					  
                    }
                    else {
                        cmail::send_smtp($recipients, $subject, $message);
                    }
                }
            }
            catch (Exception $exc) {
                throw new Exception('ERR[EMAIL-1]' .$exc->getMessage());
            }
            return $emails;
        }
        
        protected function get_emails($transaction_data){
            $db = CDatabase::instance();
            $status_transaction = cobj::get($transaction_data, 'transaction_status');
            $transaction_product_type = cobj::get($transaction_data,'product_type');
            $email_winner = cobj::get($transaction_data,'email');
            $product_name = cobj::get($transaction_data,'product_name');
            $member_address = cobj::get($transaction_data,'shipping_address');
            $member_province = cobj::get($transaction_data,'shipping_province');
            $member_city = cobj::get($transaction_data,'shipping_city');
            $member_districts = cobj::get($transaction_data,'shipping_district');
            $member_postal_code = cobj::get($transaction_data,'shipping_postal_code');
            $org_id = cobj::get($transaction_data,'org_id');
			
            $data = array();
            $data['org_id']=$org_id;
            $data['product_name'] = $product_name;
            $data['member_email'] = $email_winner;
            $data['member_address'] = $member_address;
            $data['member_province'] = $member_province;
            $data['member_city'] = $member_city;
            $data['member_districts'] = $member_districts;
            $data['member_postal_code'] = $member_postal_code;
            
            $emails = array();
            switch ($status_transaction) {
                case 'SUCCESS':	
                    if($transaction_product_type=='luckyday'){
                        $recipients = array($email_winner);
                        $subject = clang::__('Winner Product').' '.substr($product_name,16).' '.clang::__('LuckyDay');
                        $message = $this->get_message('winner-buyer', $data);
                        $email['recipients'] = $recipients;
                        $email['subject'] = $subject;
                        $email['messages'] = $message;
                        $emails[] = $email;	
                        //email vendor
                        $vendor_recipients=ccfg::get('email_vendor_luckyday');
                        $email['recipients'] = $vendor_recipients;
                        $emails[] = $email;	
                        
                    }    
                    break;

                default:
                    break;
            }
            return $emails;
        }
        
    }
    