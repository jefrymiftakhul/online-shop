<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Jun 22, 2016
     */
    class Email_TransactionVendor extends Email_Engine{
        
        
        protected function __construct($router, $id){
            parent::__construct($router, $id);
        }

        public static function factory($router, $id) {
            return new Email_TransactionVendor($router, $id);
        }
        
        
        public function execute(){
            $db = CDatabase::instance();
            $q = 'SELECT t.*,tc.* , p.name as product_name
                FROM transaction t 
                                inner join transaction_contact as tc on tc.transaction_id=t.transaction_id
              INNER JOIN transaction_detail td ON t.transaction_id = td.transaction_id 
              INNER JOIN product p ON p.product_id = td.product_id
              WHERE t.status > 0 
                AND t.transaction_id ='.$db->escape($this->id);
            $transaction_data = cdbutils::get_row($q);

            if ($transaction_data == null) {
                throw new Exception('ERR[TO01]' .clang::__('Product does not exists'));
            }
            $emails = $this->get_emails($transaction_data);
            
            try {
                foreach ($emails as $email_k => $email) {
                    $recipients = carr::get($email, 'recipients');
                    $message = carr::get($email, 'messages');
                    $subject = carr::get($email, 'subject');

                    if ($this->router->get_debug()) {
					  
                    }
                    else {
                        cmail::send_smtp($recipients, $subject, $message);
                    }
                }
            }
            catch (Exception $exc) {
                throw new Exception('ERR[EMAIL-1]' .$exc->getMessage());
            }
            return $emails;
        }
        
        protected function get_emails($transaction_data){
            $db = CDatabase::instance();
            $code = cobj::get($transaction_data,'code');
            $booking_code = cobj::get($transaction_data,'booking_code');
            $transaction_status = cobj::get($transaction_data,'transaction_status');            

            $org_id = cobj::get($transaction_data,'org_id');
            if($org_id == null){
                $org_id = CF::org_id();
            }
            $data = array();
            $data['org_id']=$org_id;

            $emails = array();
            switch ($transaction_status) {
                case 'SUCCESS':	
                    $q = "SELECT v.email, v.vendor_id ,v.name as vendor_name, tc.*
                    FROM transaction_detail td
                    LEFT JOIN transaction_contact tc ON tc.transaction_id = td.transaction_id
                    LEFT JOIN vendor v ON v.vendor_id = td.vendor_id
                    WHERE td.status > 0 AND td.transaction_id = ".$db->escape($this->id)."
                    GROUP BY td.vendor_id ";
                    $detail_transaction = $db->query($q);
                    foreach ($detail_transaction as $key => $value) {
                        if($org_id !=null && (!empty($value->email) || $value->email !=null)){
                            $q2 = "SELECT p.name AS product_name,td.qty
                            FROM transaction_detail td
                            LEFT JOIN product p ON p.product_id = td.product_id
                            WHERE td.status > 0 AND td.transaction_id = ".$db->escape($this->id)." AND td.vendor_id =".$db->escape($value->vendor_id);

                            $product_vendor = $db->query($q2);
                            $data_transaction_vendor = array();
                            if($product_vendor != null){
                                foreach ($product_vendor as $pv_key => $pv_value) {
                                    $item = array(
                                        'product_name'=>$pv_value->product_name,
                                        'jumlah'=>$pv_value->qty,
                                    );
                                    array_push($data_transaction_vendor,$item);
                                }
                                $data['member_email'] = $value->email;
                                $data['member_name'] = $value->vendor_name;
                                $recipients = $value->email;
                                $data['member_shipping'] = $value;
                                $data['booking_code'] = $booking_code;
                                $data['detail_barang'] = $data_transaction_vendor;
                                $subject = clang::__('Info Transaction').' - '.$booking_code;
                                $message = $this->get_message('info-vendor', $data);
                                $email['recipients'] = $recipients;
                                $email['subject'] = $subject;
                                $email['messages'] = $message;
                                $emails[] = $email;
                            }
                        }
                        else{
                            $q2 = "SELECT p.name AS product_name,td.qty
                            FROM transaction_detail td
                            LEFT JOIN product p ON p.product_id = td.product_id
                            WHERE td.status > 0 AND td.transaction_id = ".$db->escape($this->id)." AND td.vendor_id is null";
                            $product_vendor = $db->query($q2);
                            $data_transaction_vendor = array();
                            if($product_vendor != null){
                                foreach ($product_vendor as $pv_key => $pv_value) {
                                    $item = array(
                                        'product_name'=>$pv_value->product_name,
                                        'jumlah'=>$pv_value->qty,
                                    );
                                    array_push($data_transaction_vendor,$item);
                                }
                                $email_logistic = ccfg::get('email_logistic');
                                if($email_logistic != null){
                                    foreach ($email_logistic as $key_l => $value_l) {
                                        $data['member_email'] = $value_l;
                                        $data['member_name'] = '62Hall Logistic';
                                        $recipients = array($value_l);
                                        $data['detail_barang'] = $data_transaction_vendor;
                                        $data['member_shipping'] = $value;
                                        $subject = clang::__('Info Transaction').' - '.$booking_code;
                                        $message = $this->get_message('info-vendor', $data);
                                        $email['recipients'] = $recipients;
                                        $email['subject'] = $subject;
                                        $email['messages'] = $message;
                                        $emails[] = $email; 
                                    }
                                }// end email logistic not null
                            } // end prodeuct_vendor not null
                        }// end else
                    } // end foreach
                    break;
                default:
                    break;
            }
            return $emails;
        }
        
    }
    