<?php

    /**
     *
     * @author Raymond Sugiarto
     * @since  Jun 22, 2016
     */
    class Email_Subscribe extends Email_Engine{


        protected function __construct($router, $id){
            parent::__construct($router, $id);
        }


        public static function factory($router, $id) {
            return new Email_Subscribe($router, $id);
        }


        public function execute(){
            $emails = $this->get_emails($this->id);
            try {
                foreach ($emails as $email_k => $email) {
                    $recipients = carr::get($email, 'recipients');
                    $message = carr::get($email, 'messages');
                    $subject = carr::get($email, 'subject');

                    if ($this->router->get_debug()) {

                    }
                    else {
                        cmail::send_smtp($recipients, $subject, $message);
                    }
                }
            }
            catch (Exception $exc) {
                throw new Exception('ERR[EMAIL-1]' .$exc->getMessage());
            }
            return $emails;
        }

        protected function get_emails($email_member){
            $db = CDatabase::instance();
            $org_id = CF::org_id();
            
            $org_name = '';
            if($org_id != null && strlen($org_id) > 0){
                $org_name = cdbutils::get_value('select name from org where org_id ='.$db->escape($org_id));
            }

            $data = array();
            $data['org_id']=$org_id;
            $data['member_email'] = $email_member;
            $data['org_name'] = $org_name;
            $emails = array();

            $recipients = array($email_member);
            $subject = $org_name .' '.clang::__('Subscribe Email');
            $message = $this->get_message('subscribe-member', $data);
            $email['recipients'] = $recipients;
            $email['subject'] = $subject;
            $email['messages'] = $message;
            $emails[] = $email;
            return $emails;
        }

    }
