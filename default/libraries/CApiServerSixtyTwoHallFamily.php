<?php

    class CApiServerSixtyTwoHallFamily extends ApiServer {

        protected $method_list = array();
        protected $type = '';
        protected $api_name;
        protected $validate = false;
        protected $additional_request;

        protected function __construct($type, $params) {
            parent::__construct($params);
            $error_list_file = CF::get_file('data', 'api_error');
            $error_list = array();
            if (file_exists($error_list_file)) {
                $error_list = include $error_list_file;
            }
            $services_list_file = CF::get_file('data', 'api_services');
            if (file_exists($services_list_file)) {
                $services_list = include $services_list_file;
                $this->register_service($services_list['services']);
            }
            $this->error()->set_error_list($error_list);
            $this->set_method(self::__POST);
            $this->total_args = 1;
            $this->type = $type;
            $this->api_name = '62hallserver';
            $this->accept_data_type = array('array');
            $this->additional_request = array();

//            $this->method = self::__GET_POST;
        }

        /**
         * 
         * @param type $type
         * @param type $params
         * @return CApiServerSixtyTwoHallFamily
         */
        public static function instance($type, $params) {
            if (!is_array(self::$instance)) {
                self::$instance = array();
            }
            $method = carr::get($params, 0);
            if (!isset(self::$instance[$type])) {
                self::$instance[$type] = array();
            }

            if (!isset(self::$instance[$type][$method])) {
                self::$instance[$type][$method] = new CApiServerSixtyTwoHallFamily($type, $params);
            }

            return self::$instance[$type][$method];
        }

        public function before() {
            $this->request = array_merge($_GET, $_POST);
            if (count($this->additional_request) > 0) {
                $this->request = array_merge($this->request, $this->additional_request);
            }

            $current_service = carr::get($this->req_args, 0);

            if (strlen($current_service) == 0) {
                $this->error()->add_default(1002);
            }
            if ($this->error()->code() == 0) {
                $this->current_service_name = $current_service;
            }

            if ($this->error()->code() == 0) {
                $prefix = 'CApiServer_';
                $engine_name = 'SixtyTwoHallFamilyEngine';
                $engine_class = $prefix . $engine_name;
                $this->class_service = new $engine_class($this);
            }

            $this->related_log_path = $this->type . DS;
            if ($this->type == 'session') {
                $this->related_log_path .= $this->current_service_name . DS;
            }
            else {
                if (strlen(carr::get($this->request, 'session_id')) == 0) {
                    $this->related_log_path .= '__session_error' . DS;
                }
                else {
                    $this->related_log_path .= carr::get($this->request, 'session_id') . DS;
                }
            }
            $this->related_log_path = rtrim($this->related_log_path, DS);
        }

        public function get_type() {
            return $this->type;
        }

        public function after() {
            if ($this->error()->code() == 0) {

                if (is_array($this->response)) {
                    if (isset($this->response['data']) && is_array($this->response['data'])) {
                        if ($this->type != 'ittronmall') {
                            $this->response['data']['session_id'] = $this->session->get_session_id();
                        }
                    }
                }
            }
        }

        public function get_service_name() {
            return $this->current_service_name;
        }

        public function set_post_data($param = array()) {
            if (is_array($param)) {
                if (count($param) > 0) {
                    $this->additional_request = $param;
                    return $this;
                }
            }
        }

    }
    