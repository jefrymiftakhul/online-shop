<?php

    /**
     *
     * @author Hery Kurniawan
     * @since  Oct 30, 2014
     * @license http://piposystem.com Piposystem
     */
    class CApiClientPulsaEngine extends CApiClientEngine {

        protected function __construct($api) {
            parent::__construct($api);
        }

        public static function factory($api) {
            return new CApiClientPulsaEngine($api);
        }
        public function login() {
            $error = 0;
            $error_message = '';
            
            $api_url = $this->api->api_url();
            if ($error == 0) {
                if (strlen($api_url) == 0) {
                    $error++;
                    $error_message = "API URL EMPTY";
                }
            }
            $data = array();
            if ($error == 0) {


                $post_data = array();
                $post_data['auth_id'] = $this->api->api_auth();
                $post_data['product_code'] = $this->api->product_code();
                $service_name = 'Login';

                $type = "airlines";

                if ($this->api->product_category_code() == "AI") {
                    $type = "airlines";
                    if ($this->api->product_code() == 'LS') {
                        $lion_bypass_captcha = ccfg::get('lion_bypass_captcha');
                        $lion_bypass_captcha_vendor = ccfg::get('lion_bypass_captcha_vendor');
                        if ($lion_bypass_captcha) {
                            $post_data['no_captcha'] = '1';
                            $post_data['vendor_captcha'] = $lion_bypass_captcha_vendor;
                        }
                    }
                }
                if ($this->api->product_category_code() == "KA") {
                    $type = "kai";
                }
                if ($this->api->product_category_code() == "HT") {
                    $type = "hotel";
                }
                if ($this->api->product_category_code() == "PU") {
                    $type = "pulsa";
                }
                if ($this->api->product_category_code() == "PB") {
                    $type = "ppob";
                }
                if ($this->api->product_category_code() == "DATA") {
                    $type = "data";
                }
                if ($this->api->product_category_code() == "TP") {
                    $type = "themepark";
                }
                if ($this->api->product_category_code() == "LT") {
                    $type = "landtravel";
                }
                if	($this->api->product_category_code() == "MC") {
                    $type = "merchandise";
                }
                if	($this->api->product_category_code() == "TO") {
                    $type = "tour";
                }
                $url = $api_url . $type . '/' . $service_name;


                $response = $this->__request($service_name, $url, $post_data);

                $data = cjson::decode($response);
                if (!is_array($data) || empty($data)) {
                    $error++;
                    $error_message = 'Parse Error, ' . ($response);
                    $error_message = json_last_error() . 'Parse Error, ' . cdbg::var_dump($response, true);
                }
            }
            if ($error == 0) {
                $error = carr::get($data, 'err_code');
                $error_message = carr::get($data, 'err_message', '');
            }
            if ($error == 0) {

                $api_session_id = carr::get($data, 'session_id');
                $data_data = carr::get($data, 'data');
                if ($data_data != null) {
                    $captcha_image = carr::get($data_data, 'captcha_image');
                    if ($captcha_image != null) {
                        $this->session()->set('captcha_image', $captcha_image);
                    }
                }
                $this->session()->set('api_session_id', $api_session_id);
                $data['api_session_id'] = $this->session()->get('api_session_id');
            }
            if ($error > 0) {
                $this->error()->add($error_message, $error);
            }
            return $data;
        }
        public function get_product($request) {
            $api_url = $this->api->api_url();

            $err_code = 0;
            $err_message = "";
            $data = array();

            if ( $err_code == 0 ) {
                if ( strlen($api_url) == 0 ) {
                    $err_code++;
                    $err_message = "API URL EMPTY";
                }
            }
            if ( $err_code == 0 ) {
                $post_data = array();

                $post_data['session_id'] = $this->api->api_session_id();
                $post_data['product_code'] = $this->api->product_code();
                $service_name = 'GetProduct';

                $url = $api_url . 'pulsa/' . $service_name;
                $response = $this->__request($service_name, $url, $post_data);
                $response_data = cjson::decode($response);
                if ( !is_array($response_data) || empty($response_data) ) {
                    $err_code++;
                    $err_message = 'Parse Error ' . ($response);
                }
            }
            if ( $err_code == 0 ) {
                $err_code = carr::get($response_data, 'err_code');
                $err_message = carr::get($response_data, 'err_message', '');
                $data = carr::get($response_data, 'data');
            }
            if ( $err_code > 0 ) {
                $this->error()->add($err_message, $err_code);
            }

            return $data;
        }

        public function get_price($request) {
            $api_url = $this->api->api_url();
            $err_code = 0;
            $err_message = "";
            $data = array();

            if ( $err_code == 0 ) {
                if ( strlen($api_url) == 0 ) {
                    $err_code++;
                    $err_message = "API URL EMPTY";
                }
            }
            if ( $err_code == 0 ) {
                $post_data = array();

                $post_data['session_id'] = $this->api->api_session_id();
                $post_data['product_code'] = $this->api->product_code();
                $post_data['operator_name'] = carr::get($request, "operator_name");
                $service_name = 'GetPrice';

                $url = $api_url . 'pulsa/' . $service_name;
                $response = $this->__request($service_name, $url, $post_data);
                $response_data = cjson::decode($response);
                if ( !is_array($response_data) || empty($response_data) ) {
                    $err_code++;
                    $err_message = 'Parse Error ' . ($response);
                }
            }
            if ( $err_code == 0 ) {
                $err_code = carr::get($response_data, 'err_code');
                $err_message = carr::get($response_data, 'err_message', '');
                $data = carr::get($response_data, 'data');
            }
            if ( $err_code > 0 ) {
                $this->error()->add($err_message, $err_code);
            }

            return $data;
        }

        public function get_data_transaction($request) {
            $api_url = $this->api->api_url();

            $err_code = 0;
            $err_message = "";
            $data = array();

            if ( $err_code == 0 ) {
                if ( strlen($api_url) == 0 ) {
                    $err_code++;
                    $err_message = "API URL EMPTY";
                }
            }
            if ( $err_code == 0 ) {
                $post_data = array();

                $post_data['session_id'] = $this->api->api_session_id();
                $post_data['product_code'] = $this->api->product_code();
                $post_data['start_date'] = carr::get($request, "start_date");
                $post_data['end_date'] = carr::get($request, "end_date");
                $service_name = 'DataTransaction';

                $url = $api_url . 'pulsa/' . $service_name;
                $response = $this->__request($service_name, $url, $post_data);
                $response_data = cjson::decode($response);
                if ( !is_array($response_data) || empty($response_data) ) {
                    $err_code++;
                    $err_message = 'Parse Error ' . ($response);
                }
            }
            if ( $err_code == 0 ) {
                $err_code = carr::get($response_data, 'err_code');
                $err_message = carr::get($response_data, 'err_message', '');
                $data = carr::get($response_data, 'data');
            }
            if ( $err_code > 0 ) {
                $this->error()->add($err_message, $err_code);
            }

            return $data;
        }

        public function commit($request) {

            $api_url = $this->api->api_url();

            $err_code = 0;
            $err_message = "";
            $data = array();
            $session = $this->session();

            if ( $err_code == 0 ) {
                if ( strlen($api_url) == 0 ) {
                    $err_code++;
                    $err_message = "API URL EMPTY";
                }
            }
            if ( $err_code == 0 ) {
                $post_data = array();

                $post_data['session_id'] = $this->api->api_session_id();
                $post_data['product_code'] = $this->api->product_code();
                $post_data['msisdn'] = carr::get($request, "msisdn");
                $post_data['item_code'] = carr::get($request, "item_code");
                $service_name = 'Commit';

                $url = $api_url . 'pulsa/' . $service_name;
                $response = $this->__request($service_name, $url, $post_data);
                $response_data = cjson::decode($response);
//                cdbg::var_dump($response_data);
//                die(__CLASS__);
                if ( !is_array($response_data) || empty($response_data) ) {
                    $err_code++;
                    $err_message = 'Parse Error ' . ($response);
                }
            }
            if ( $err_code == 0 ) {
                $err_code = carr::get($response_data, 'err_code');
                $err_message = carr::get($response_data, 'err_message', '');
                $data = carr::get($response_data, 'data');
                $commission = carr::get($data, 'commission');
                $vendor_sell_price = carr::get($data, 'total');
                $session->set("commission", $commission);
                $session->set("vendor_sell_price", $vendor_sell_price);
            }
            if ( $err_code > 0 ) {
                $this->error()->add($err_message, $err_code);
            }

            return $data;
        }

        public function get_transaction($request) {
            $api_url = $this->api->api_url();

            $err_code = 0;
            $err_message = "";
            $data = array();

            if ( $err_code == 0 ) {
                if ( strlen($api_url) == 0 ) {
                    $err_code++;
                    $err_message = "API URL EMPTY";
                }
            }
            if ( $err_code == 0 ) {
                $post_data = array();

                $post_data['session_id'] = $this->api->api_session_id();
                $post_data['product_code'] = $this->api->product_code();
                $post_data['transaction_code'] = carr::get($request, 'transaction_code');

                //---remark this on production---
                $post_data['transaction_code'] = "TRPU1506-00128";

                $service_name = 'GetTransaction';


                $url = $api_url . 'pulsa/' . $service_name;
                $response = $this->__request($service_name, $url, $post_data);
                $response_data = cjson::decode($response);
                if ( !is_array($response_data) || empty($response_data) ) {
                    $err_code++;
                    $err_message = 'Parse Error ' . ($response);
                }
            }
            if ( $err_code == 0 ) {
                $err_code = carr::get($response_data, 'err_code');
                $err_message = carr::get($response_data, 'err_message', '');
                $data = carr::get($response_data, 'data');
            }
            if ( $err_code > 0 ) {
                $this->error()->add($err_message, $err_code);
            }

            return $data;
        }

        public function next_method($method) {
            switch ($method) {
                case 'get_availability':
                    return 'get_itinerary';
                case 'get_itinerary':
                    return 'get_itinerary';
                    break;
            }
        }

    }
    