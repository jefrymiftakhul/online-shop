<?php
    class CApiServer_SixtyTwoHallFamillyService {
        /**
         *
         * @var CApiServer_SixtyTwoHallFamilyEngine 
         */
        protected $engine;
        
        /**
         *
         * @var type 
         */
        protected $request;
        
        /**
         *
         * @var CApiServerSixtyTwoHallFamily 
         */
        protected $api_server;
        
        /**
         *
         * @var CApiServer_Session 
         */
        protected $session;
        
        /**
         *
         * @var ApiError 
         */
        protected $error;
        
        public function __construct(CApiServer_SixtyTwoHallFamilyEngine $engine){
            $this->engine = $engine;
            $this->api_server = $this->engine->get_api_server();
            $this->request = $this->api_server->get_request();
            $this->session = $this->engine->get_session();
            $this->error = $this->engine->error();
        }
        
        public function error(){
            return $this->error;
        }
        
        
    }
    