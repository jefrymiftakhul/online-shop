<?php

    class CApiServer_SixtyTwoHallFamillyService_Data_GetLocation extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }
        

        public function execute() {
            $db = CDatabase::instance();

            $arr_data=array();
            $arr_data['country_list']=array();
            $err_code = 0;
            $err_message = '';
            $this->request;
            //$country = 'Indonesia';
            $q = "
                select
                    c.country_id as country_id,
                    c.code as country_code,
                    c.name as country_name,
                    p.province_id as province_id,
                    p.code as province_code,
                    p.name as province_name,
                    ci.city_id as city_id,
                    ci.code as city_code,
                    ci.area_code as area_code,
                    ci.name as city_name,
                    d.districts_id as districts_id,
                    d.code as districts_code,
                    d.name as districts_name
                from
                    districts d
                    inner join province p on p.province_id = d.province_id
                    inner join city ci on ci.city_id = d.city_id
                    inner join country c on c.country_id = d.country_id  
                where
                    d.status>0
                    and p.status>0
                    and ci.status>0
                    and c.status>0
                    and c.name = 'Indonesia'
                
            ";

            $r = $db->query($q);           
            foreach ($r as $row) {
                $data = array();
                //check country
                if(carr::get($arr_data['country_list'],$row->country_name)==null){
                    $arr_data['country_list'][$row->country_name]=array(
                        "country_id"=>$row->country_id,
                        "country_code"=>$row->country_code,
                        "country_name"=>$row->country_name,
                        "province_list"=>array(),
                    );
                }
                $arr_country=&$arr_data['country_list'][$row->country_name];
                //check province
                if(carr::get($arr_country['province_list'],$row->province_name)==null){
                    $arr_country['province_list'][$row->province_name]=array(
                        "province_id"=>$row->province_id,
                        "province_code"=>$row->province_code,
                        "province_name"=>$row->province_name,
                        "city_list"=>array(),
                    );
                }
                $arr_province=&$arr_country['province_list'][$row->province_name];
                //check city
                if(carr::get($arr_province['city_list'],$row->city_name)==null){
                    $arr_province['city_list'][$row->city_name]=array(
                        "city_id"=>$row->city_id,
                        "city_code"=>$row->city_code,
                        "city_name"=>$row->city_name,
                        "districts_list"=>array(),
                    );
                }
                $arr_city=&$arr_province['city_list'][$row->city_name];
                $districts=array(
                    "districts_id"=>$row->districts_id,
                    "districts_code"=>$row->districts_code,
                    "districts_name"=>$row->districts_name,
                );
                $arr_city['districts_list'][$row->districts_name]=$districts;
            }
            
            

            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $arr_data,
            );
            return $return;
        }

    }
    