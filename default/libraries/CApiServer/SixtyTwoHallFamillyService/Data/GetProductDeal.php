<?php

    class CApiServer_SixtyTwoHallFamillyService_Data_GetProductDeal extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $request = $this->request;
            $org_id = $this->session->get('org_id');
            $product_type = carr::get($request, 'product_type');

            $arr_product_type = array();
            $session_app_info = $this->session->get('response_GetAppInfo');

            $deal = array();
            $arr_product_deal = array();
            if ($session_app_info == null) {
                $err_code++;
                $err_message = 'Please Request GetAppInfo';
            }
            if ($err_code == 0) {
                $data_session_app_info = carr::get($session_app_info, 'data');
            }
            if ($product_type == null) {
                $err_code++;
                $err_message = "Product Type not Found";
            }
            if ($err_code == 0) {
                $arr_product_type[] = $product_type;
                if (strtolower($product_type) == 'all') {
                    $arr_product_type = carr::get($data_session_app_info, 'list_product_type', array());
                }
            }

            $product_deal_type = carr::get($request, 'product_deal_type');

            //get product deal
            foreach ($arr_product_type as $key => $val) {
                $q = "
                        select
                            *
                        from
                            cms_deal_setting
                        where
                            status>0
                            and product_type=".$db->escape($val)."
                    ";
                if (strlen($product_deal_type) > 0 && strtolower($product_deal_type) != "all") {
                    $q.="
                            and code=" . $db->escape($product_deal_type) . "
                        ";
                }
                $r = $db->query($q);

                if ($r->count() > 0) {
                    foreach ($r as $row) {
                        $arr_deal = array();
                        $arr_deal['product_type'] = $row->product_type;
                        $arr_deal['product_deal_type'] = $row->code;
                        $name=$row->name;
                        if(ccfg::get($row->code)){
                            $name=ccfg::get($row->code);
                        }
                        $arr_deal['product_deal_name'] = $name;
                        $deals=deal::get_deal($org_id, $product_type, $row->code);
                        $arr_products=array();
                        foreach($deals as $key_product=>$val_product){  
                            $arr = api_products_detail::convert_products_api($val_product);                            
                            $arr_products[]=$arr;
                        }
                        $arr_deal['products'] = $arr_products;
                        $deal[] = $arr_deal;
                       
                    }
                }
            }
            $arr_product_deal['products_deals']=$deal;
            $data = $arr_product_deal;
            
            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
            );
            return $return;
        }

    }
    