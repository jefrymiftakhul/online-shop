<?php

    class CApiServer_SixtyTwoHallFamillyService_Data_MemberLogin extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();

            $username = carr::get($this->request, 'username');
            $password = carr::get($this->request, 'password');
            $org_id = $this->session->get('org_id');


            $q = '
                SELECT * FROM member WHERE status > 0
                AND email = ' . $db->escape($username) . ' AND password = ' . $db->escape(md5($password)) . '
                AND org_id = ' . $db->escape($org_id) . '
              ';
            $r = cdbutils::get_row($q);
            if ($r != null) {
                $is_verified = cobj::get($r, 'is_verified');
                if ($is_verified == '1') {
                    $member_id = cobj::get($r, 'member_id');
                    $email = cobj::get($r, 'email');
                    $name = cobj::get($r, 'name');
                    $gender = cobj::get($r, 'gender');
                    $date_of_birth = cobj::get($r, 'date_of_birth');
                    $address = cobj::get($r, 'address');
                    $phone = cobj::get($r, 'phone');
                    $subscribe_product = cobj::get($r, 'is_subscribe');
                    $subscribe_service = cobj::get($r, 'is_subscribe_service');
                    $subscribe_gold = cobj::get($r, 'is_subscribe_gold');
                    $bank = cobj::get($r, 'bank');
                    $account_holder = cobj::get($r, 'account_holder');
                    $bank_account = cobj::get($r, 'bank_account');
                    $province_id = cobj::get($r, 'province_id');
                    $city_id = cobj::get($r, 'city_id');
                    $member_address = array();
                    $member_address_billing = array();
                    $member_address_shipping = array();

                    $q = "
						select
							c.name as country,
							p.name as province,
							ct.name as city,
							d.name as districts,
							ma.*
						from
							member_address as ma
							left join country as c on ma.country_id=c.country_id
							left join province as p on ma.province_id=p.province_id
							left join city as ct on ma.city_id=ct.city_id
							left join districts as d on ma.districts_id=d.districts_id
						where
							is_active>0
							and member_id=" . $db->escape($member_id) . "
					";
                    $r = $db->query($q);
                    if ($r->count() > 0) {
                        foreach ($r as $row) {
                            $arr = array();
                            $arr['id'] = $row->member_address_id;
                            $arr['name'] = $row->name;
                            $arr['phone'] = $row->phone;
                            $arr['address'] = $row->address;
                            $arr['postal'] = $row->postal;
                            $arr['email'] = $row->email;
                            $arr['country'] = $row->country;
                            $arr['province'] = $row->province;
                            $arr['city'] = $row->city;
                            $arr['districts'] = $row->districts;
                            if ($row->type == 'payment') {
                                $member_address_billing[] = $arr;
                            }
                            if ($row->type == 'shipping') {
                                $member_address_shipping[] = $arr;
                            }
                        }
                    }
                    $member_address['billing'] = $member_address_billing;
                    $member_address['shipping'] = $member_address_shipping;

                    $data['member_id'] = $member_id;
                    $data['email'] = $email;
                    $data['name'] = $name;
                    $data['gender'] = $gender;
                    $data['date_of_birth'] = $date_of_birth;
                    $data['address'] = $address;
                    $data['phone'] = $phone;
                    $data['subscribe_product'] = $subscribe_product;
                    $data['subscribe_service'] = $subscribe_service;
                    $data['subscribe_gold'] = $subscribe_gold;
                    $data['bank'] = $bank;
                    $data['account_holder'] = $account_holder;
                    $data['bank_account'] = $bank_account;
                    $data['province_id'] = $province_id;
                    $data['city_id'] = $city_id;
                    $member = $this->session->set('member_id', $member_id);
                    $session = Session::instance();
                    $session->set('member_id', $member_id);
                    $session->set('email', $email);
                    $session->set('name', $name);
                }
                else {
                    $this->error->add_default("DATERR10002");
                }
            }
            else {
                $this->error->add_default("DATERR10001");
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    