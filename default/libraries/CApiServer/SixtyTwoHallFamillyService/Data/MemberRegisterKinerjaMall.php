<?php

class CApiServer_SixtyTwoHallFamillyService_Data_MemberRegisterKinerjaMall extends CApiServer_SixtyTwoHallFamillyService {

    public function __construct($engine) {
        parent::__construct($engine);
    }

    public function execute() {
        $db = CDatabase::instance();

        $err_code = 0;
        $err_message = '';
        $data = array();

        $member_id = '';
        $ip = crequest::remote_address();
        $date = date("Y-m-d H:i:s");
        $name = carr::get($this->request, 'name');
        $email = carr::get($this->request, 'email');
        $noemail = carr::get($this->request, 'noemail', 1);
        $gender = carr::get($this->request, 'gender');

        $random_password = intval("0" . rand(1, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9));
        $password = $random_password;
        $confirm_password = $random_password;

        if ($noemail == 0) {
            $password = carr::get($this->request, 'password', NULL);  // harus sudah di md5
        }

        $phone = '';
        $org_id = $this->session->get('org_id');
        $parent_id = null;
        $q = "SELECT count(*) as total 
                FROM member
                WHERE email = " . $db->escape($email) . " 
                and org_id = " . $db->escape($org_id) . "
                and status > 0
                ";
        $total = cdbutils::get_value($q);
        if (strlen($total) > 0 && $total > 0) {
            $err_code = 10010;
            $this->error->add_default("DATERR10010");
        }

        if ($err_code == 0) {
            if (strlen($name) == 0) {
                $err_code = 10013;
                $this->error->add_default("DATERR10013");
            }
        }

        if ($err_code == 0) {
            if (strlen($email) == 0) {
                $err_code = 10014;
                $this->error->add_default("DATERR10014");
            }
        }

        if ($noemail == 1) {
            if ($err_code == 0) {
                if ($confirm_password != $password) {
                    $err_code = 10011;
                    $this->error->add_default("DATERR10011");
                }
            }
        }

        if ($noemail == 0) {
            if ($err_code == 0) {
                if ($password == NULL) {
                    $err_code = 10015;
                    $this->error->add_default("DATERR10015");
                }
            }
        }
        if (strlen($gender) > 0) {
            if (strtoupper($gender) == 'L') {
                $gender = 'male';
            }
            if (strtoupper($gender) == 'P') {
                $gender = 'female';
            }
        }


        if ($err_code == 0) {
            try {
                $tree = CTreeDB::factory('member');
                $tree->set_org_id($org_id);
                $data_member = array(
                    'org_id' => $this->session->get('org_id'),
                    'name' => $name,
                    'gender' => $gender,
                    'email' => $email,
                    'phone' => $phone,
                    'password' => md5($password),
                    'password_real' => $password,
                    'is_verified' => '1',
                    'created' => $date,
                    'createdby' => $email,
                    'createdip' => $ip
                );

                if ($noemail == 0) {
                    $data_member['password'] = $password;
                }
                //$member_id= $tree->insert($data_member, $parent_id);
                $insert_member = $db->insert('member', $data_member);
                $member_id = $insert_member->insert_id();
            } catch (Exception $ex) {
                $this->error->add_default("DATERR10012");
            }
        }

        if ($err_code == 0) {
            if ($noemail == 1) {
                $this->send_verification($member_id);
            }

            // send verification email
        }
       
        $return = array(
            'err_code' => $err_code,
            'err_message' => $this->error->get_err_message(),
            'data' => $data,
        );

        return $return;
    }

    // same with resend_verification at cms
    public function send_verification($id) {
        $db = CDatabase::instance();
        $org_id = CF::org_id();

        // data email member
        $data_email = member::get_email_member($id);
        $password_real = cdbutils::get_value('select password_real from member where member_id='.$db->escape($id));
        $data_org = org::get_org($org_id);

        if (!empty($data_email) > 0) {
            $email = $data_email->email;
            $member_id = $data_email->member_id;
            $name = $data_email->name;
            $org_name = !empty($data_org->name) ? $data_org->name : NULL;

            $this->send_email_registration($email, array(
                'logo' => curl::httpbase() . 'application/admin62hallfamily/' . CF::org_code() . '/upload/logo/item_image/' . $data_org->item_image,
                'subject' => $org_name . ' - Welcome to ' . $org_name,
                'member_id' => $member_id,
                'name' => $name,
                'password_real' => $password_real,
                'org_name' => $org_name,
                    )
            );
            return true;
        } else {
            throw new Exception('Resend verification email failed.');
        }
    }

    private function send_email_registration($recipient, $data) {
        $tbclass = $this->email_format("registrationkp");
        $format = $tbclass::format($recipient, $data);
        $result = false;
        try {
            $result = cmail::send_smtp($recipient, $format['subject'], $format['message']);
        } catch (Exception $ex) {
            //do nothing
            if (isset($_GET['debug'])) {
                cdbg::var_dump($ex->getMessage());
            }
        }
        return $result;
    }
    
    public function email_format($base_class) {
        $classname = ucfirst($base_class) . "Email";
        $file = CF::get_files('libraries', "FormatEmail/" . $classname);
        include_once $file[0];
        $tbclass = new $classname();
        return $tbclass;
    }

}
