<?php

    class CApiServer_SixtyTwoHallFamillyService_Data_GetAdvertise extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            //$this->session = CApiServer_Session::instance($this->api_server->get_type());
            $err_code = 0;
            $err_message = '';
            $request = $this->request;
            $product_type = carr::get($request, 'product_type');
            if ($product_type == null) {
                $err_code++;
                $err_message = "Product Type not Found";
            }
            $org_id = $this->session->get('org_id');

            //get advertise
            $advertise = advertise::get_advertise($org_id, $product_type);
            $data = $advertise;


            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
            );
            return $return;
        }

    }
    