<?php

/**
 * Description of GetKinerjaProductCategory
 *
 * @author Ecko Santoso
 * @since 15 Nov 16
 */
class CApiServer_SixtyTwoHallFamillyService_Data_GetKinerjaProductCategory extends CApiServer_SixtyTwoHallFamillyService {
    
    public function __construct($engine) {
        parent::__construct($engine);
    }
    
    public function execute() {
        $err_code = 0;
        $data = array();
        $kinerja = ccfg::get('kinerjapay');
        $kinerja_services = carr::get($kinerja, 'services');
        
        $url_request = carr::get($kinerja, 'api_url'). carr::get($kinerja_services, 'product_category');
        $post_data = array(
            'merchantAppCode' => carr::get($kinerja, 'merchantAppCode'),
            'merchantAppPassword' => carr::get($kinerja, 'merchantAppPassword'),
            'merchantIpAddress' => carr::get($kinerja, 'merchantIpAddress'),
        );
        
        $this->engine->set_url_request($url_request);
        $response = $this->engine->client_send_request("GetKinerjaProductCategory", cjson::encode($post_data), true)->get_response_array();
        
        $code = carr::get($response, 'code');
        $success = carr::get($response, 'success');
        $message = carr::get($response, 'message');
        $result = carr::get($response, 'result');
        
        if ($success == '1') {
            $categories = carr::get($result, 'categories');
            if (count($categories) > 0) {
                $category = carr::get($categories, 'category');
                
                // process to format 62hall category
                $product_category = array();
                $root = $this->unique_multidim_array($category, 'type');
                if (count($root) > 0) {
                    foreach ($root as $root_k => $root_v) {
                        $arr_product_category = array();
                        $arr_product_category['type'] = carr::get($root_v, 'type');
                        $arr_product_category['category_id'] = carr::get($root_v, 'id');
        //                $arr_product_category['name'] = carr::get($root_v, 'name');
                        $arr_product_category['name'] = carr::get($root_v, 'type');

                        $subnav = array();                
                        foreach ($category as $category_k => $category_v) {
                            $type = carr::get($category_v, 'type');
                            if ($type == carr::get($root_v, 'type')) {
                                $subnav[] = $category_v;
                            }
                        }
                        $subnav_1 = $this->unique_multidim_array($subnav, 'group');
                        if (count($subnav_1) > 0) {
                            foreach ($subnav_1 as $subnav_1_k => $subnav_1_v) {
                                $arr_subnav_1 = array();
                                $arr_subnav_1['type'] = carr::get($subnav_1_v, 'type');
                                $arr_subnav_1['category_id'] = carr::get($subnav_1_v, 'id');
        //                        $arr_subnav_1['name'] = carr::get($subnav_1_v, 'name');
                                $arr_subnav_1['name'] = carr::get($subnav_1_v, 'group');
                                // process
                                $subnav_2 = array();
                                foreach ($subnav as $subnav_k => $subnav_v) {
                                    if (carr::get($subnav_1_v, 'group') == carr::get($subnav_v, 'group')){
                                        $subnav_2[] = $subnav_v;
                                    }
                                }
                                $arr_subnav_1['subnav'] = $subnav_2;
                                $all_subnav_1[] = $arr_subnav_1;
                            }
                        }
                        $arr_product_category['subnav'] = $all_subnav_1;
                        $product_category[] = $arr_product_category;
                    }
                }
                
                $data = $product_category;
            }
        }
        else {
            $api_err_message = carr::get($message, 'message');
            if (is_array($api_err_message)) {
                $api_err_message = carr::get($api_err_message, 0);
            }
            $err_code = $code;
            $err_message = $api_err_message;
            $this->error()->add($err_message, $code);
        }
        
        $return = array(
            'err_code' => $err_code,
            'err_message' => $this->error->get_err_message(),
            'data' => $data,
        );
        cdbg::var_dump($return);
        die(__METHOD__);
        return $return;
    }
    
    public function unique_multidim_array($array, $key) { 
        $temp_array = array(); 
        $i = 0; 
        $key_array = array(); 

        foreach($array as $val) { 
            if (!in_array($val[$key], $key_array)) { 
                $key_array[$i] = $val[$key]; 
                $temp_array[$i] = $val; 
            } 
            $i++; 
        } 
        return $temp_array; 
    } 
}
