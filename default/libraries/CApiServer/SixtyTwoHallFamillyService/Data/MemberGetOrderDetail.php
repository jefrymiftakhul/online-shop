<?php

    class CApiServer_SixtyTwoHallFamillyService_Data_MemberGetOrderDetail extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();
            $arr_details = array();
            $arr_shippings = array();
            $arr_billings = array();


            $member_id = $this->session->get('member_id');
            $booking_code = carr::get($this->request, 'booking_code');
            //$member_id = '53';
            //$booking_code = 'B93IWAO1';

            $q_details = '
                SELECT                     
                    booking_code                   
                FROM 
                    transaction 
                WHERE 
                    status > 0
                AND member_id = ' . $db->escape($member_id) . '
              ';
            $r_row = $db->query($q_details);

            $q = '
                SELECT 
                    t.booking_code,
                    t.date,
                    t.order_status,
                    tp.payment_type,
                    t.total_shipping as total,
                    tp.total_item,
                    tp.total_charge,
                    tp.total_payment,                                                    
                    tp.payment_status                   
                FROM 
                    transaction t
            	Left join transaction_payment tp on tp.transaction_id = t.transaction_id
            	Left join transaction_contact tc on tc.transaction_id = t.transaction_id
                WHERE 
                    t.status > 0
            	AND tp.status > 0
            	AND tc.status > 0            	
                AND t.member_id = ' . $db->escape($member_id) . '                
              ';

            if (strlen($booking_code) > 0) {
                $q .= " and t.booking_code = " . $db->escape($booking_code);
            }
            $r = cdbutils::get_row($q);

            if ($r != null) {

                $booking_code = cobj::get($r, 'booking_code');
                $date = cobj::get($r, 'date');
                $order_status = cobj::get($r, 'order_status');
                $payment_type = cobj::get($r, 'payment_type');
                $total_item = cobj::get($r, 'total_item');
                $total_charge = cobj::get($r, 'total_charge');
                $total_payment = cobj::get($r, 'total_payment');
                $payment_status = cobj::get($r, 'payment_status');

                $data['booking_code'] = $booking_code;
                $data['date'] = $date;
                $data['order_status'] = $order_status;

                if ($r_row->count() > 0) {
                    $q_row = "
                                            SELECT                                                 
						p.name as product_name,
                                                td.qty,
                                                td.channel_sell_price as price

                                            FROM 
                                                transaction t
                                             left join transaction_detail td on td.transaction_id = t.transaction_id
                                             left join product p on p.product_id = td.product_id
                                            WHERE 
                                                t.status > 0
                                                AND td.status > 0
                                                AND p.status > 0
                                            AND t.booking_code = " . $db->escape($booking_code) . "
                                    ";
                    $r = $db->query($q_row);
                    if ($r->count() > 0) {
                        foreach ($r as $row) {
                            $arr_detail = array();
                            $arr_detail['product_name'] = $row->product_name;
                            $arr_detail['qty'] = $row->qty;
                            $arr_detail['price'] = $row->price;
                            $arr_details[] = $arr_detail;
                        }
                    }
                }
                $data['details'] = $arr_details;

                $data['total_item'] = $total_item;
                $data['total_charge'] = $total_charge;
                $data['total_payment'] = $total_payment;
                $data['payment_status'] = $payment_status;
                $data['payment_type'] = $payment_type;
                if ($r_row->count() > 0) {
                    $q_row = "
                                SELECT 
                                    tc.shipping_first_name,
                                    tc.shipping_address,
                                    tc.shipping_province_id,
                                    tc.shipping_province,
                                    tc.shipping_city_id,
                                    tc.shipping_city,
                                    tc.shipping_postal_code,
                                    tc.shipping_phone
                                FROM 
                                    transaction t
                                Left join transaction_payment tp on tp.transaction_id = t.transaction_id
                                Left join transaction_contact tc on tc.transaction_id = t.transaction_id
                                WHERE 
                                    t.status > 0
                                AND tp.status > 0
                                AND tc.status > 0   
                                AND t.booking_code = " . $db->escape($booking_code) . "
                        ";
                    $r = $db->query($q_row);
                    if ($r->count() > 0) {
                        foreach ($r as $row) {
                            $arr_shipping = array();
                            $arr_shipping['shipping_first_name'] = $row->shipping_first_name;
                            $arr_shipping['shipping_address'] = $row->shipping_address;
                            $arr_shipping['shipping_city_id'] = $row->shipping_city_id;
                            $arr_shipping['shipping_city'] = $row->shipping_city;
                            $arr_shipping['shipping_province_id'] = $row->shipping_province_id;
                            $arr_shipping['shipping_province'] = $row->shipping_province;
                            $arr_shipping['shipping_postal_code'] = $row->shipping_postal_code;
                            $arr_shipping['shipping_phone'] = $row->shipping_phone;
                            $arr_shippings[] = $arr_shipping;
                        }
                    }
                }
                $data['shipping'] = $arr_shippings;
                if ($r_row->count() > 0) {
                    $q_row = "
                                SELECT 
                                    tc.billing_first_name,
                                    tc.billing_address,
                                    tc.billing_province_id,
                                    tc.billing_province,
                                    tc.billing_city_id,
                                    tc.billing_city,
                                    tc.billing_postal_code,
                                    tc.billing_phone
                                FROM 
                                    transaction t
                                Left join transaction_payment tp on tp.transaction_id = t.transaction_id
                                Left join transaction_contact tc on tc.transaction_id = t.transaction_id
                                WHERE 
                                    t.status > 0
                                AND tp.status > 0
                                AND tc.status > 0 
                                AND t.booking_code = " . $db->escape($booking_code) . "
                        ";
                    $r = $db->query($q_row);
                    if ($r->count() > 0) {
                        foreach ($r as $row) {
                            $arr_billing = array();
                            $arr_billing['billing_first_name'] = $row->billing_first_name;
                            $arr_billing['billing_address'] = $row->billing_address;
                            $arr_billing['billing_city_id'] = $row->billing_city_id;
                            $arr_billing['billing_city'] = $row->billing_city;
                            $arr_billing['billing_province_id'] = $row->billing_province_id;
                            $arr_billing['billing_province'] = $row->billing_province;
                            $arr_billing['billing_postal_code'] = $row->billing_postal_code;
                            $arr_billing['billing_phone'] = $row->billing_phone;
                            $arr_billings[] = $arr_billing;
                        }
                    }
                }
                $data['billing'] = $arr_billings;
            }
            else {
                $err_code++;
                $err_message = "Data Not Found";
            }
            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
            );
            //cdbg::var_dump($data); 

            return $return;
        }

    }
    