<?php

    class CApiServer_SixtyTwoHallFamillyService_Data_MemberRegister extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();

            $ip = crequest::remote_address();
            $date = date("Y-m-d H:i:s");
            $name = carr::get($this->request, 'name');
            $email = carr::get($this->request, 'email');
            $password = carr::get($this->request, 'password');
            $confirm_password = carr::get($this->request, 'confirm_password');
            $phone = carr::get($this->request, 'phone');
            $org_id = $this->session->get('org_id');

            $q = "SELECT count(*) as total 
                FROM member
                WHERE email = " . $db->escape($email) . " 
                and org_id = " . $db->escape($org_id) . "
                and status > 0
                ";
            $total = cdbutils::get_value($q);
            if (strlen($total) > 0 && $total > 0) {
                $this->error->add_default("DATERR10010");
            }

            if ($this->error()->code() == 0) {
                if ($confirm_password != $password) {
                    $this->error->add_default("DATERR10011");
                }
            }

            if ($this->error()->code() == 0) {
                try {
                    $data_member = array(
                        'org_id' => $this->session->get('org_id'),
                        'name' => $name,
                        'email' => $email,
                        'phone' => $phone,
                        'password' => md5($password),
                        'created' => $date,
                        'createdby' => $email,
                        'createdip' => $ip
                    );
                    $r = $db->insert('member', $data_member);
					$member_id=$r->insert_id();
					
                }
                catch (Exception $ex) {
                    $this->error->add_default("DATERR10012");
                }
            }

            if ($this->error()->code() == 0) {
				SixtyTwoHallFamilyController::factory()->send_verification($member_id);
                // send verification email
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    