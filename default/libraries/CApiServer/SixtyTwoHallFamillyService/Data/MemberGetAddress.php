<?php

    class CApiServer_SixtyTwoHallFamillyService_Data_MemberGetAddress extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();


            $err_code = 0;
            $err_message = '';
            $data_all = array();


            $member_address_id = carr::get($this->request, 'member_address_id');
            $member_id = $this->session->get('member_id');

            $q = '
                SELECT 
                        ma.member_address_id,ma.member_id,ma.type,ma.name,ma.phone,ma.address,ma.postal,
                        ma.email,ma.country_id,c.name as country_name,ma.province_id,p.name as province_name, 
                        ma.city_id, ci.name as city_name, ma.districts_id, d.name as district_name
                FROM 
                        member_address ma
                        left join country c on c.country_id = ma.country_id
                        left join province p on p.province_id = ma.province_id
                        left join city ci on ci.city_id = ma.city_id
                        left join districts d on d.districts_id = ma.districts_id
                WHERE ma.status > 0                
                AND ma.member_id = ' . $db->escape($member_id) . '
              ';
            if (strlen($member_address_id) > 0) {
                $q .= " and ma.member_address_id = " . $db->escape($member_address_id);
            }

            $r = $db->query($q);


            if ($r->count() > 0) {
                foreach ($r as $row) {
                    $data = array();
                    $data['member_address_id'] = $row->member_address_id;
                    $data['member_id'] = $row->member_id;
                    $data['type'] = $row->type;
                    $data['name'] = $row->name;
                    $data['phone'] = $row->phone;
                    $data['address'] = $row->address;
                    $data['postal'] = $row->postal;
                    $data['email'] = $row->email;
                    $data['country_id'] = $row->country_id;
                    $data['country_name'] = $row->country_name;
                    $data['province_id'] = $row->province_id;
                    $data['province_name'] = $row->province_name;
                    $data['city_id'] = $row->city_id;
                    $data['city_name'] = $row->city_name;
                    $data['districts_id'] = $row->districts_id;
                    $data['district_name'] = $row->district_name;
                    $data_all[] = $data;
                }
            }

            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data_all,
            );


            return $return;
        }

    }
    