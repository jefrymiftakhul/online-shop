<?php

    class CApiServer_SixtyTwoHallFamillyService_Data_GetSlideShow extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';
            $request = $this->request;
            $product_type = carr::get($request, 'product_type');
            if ($product_type == null) {
                $err_code++;
                $err_message = "Product Type not Found";
            }
            $org_id = $this->session->get('org_id');


            //get slide
            $slide = slide::get_slide($org_id, $product_type);
            $slideshows['slideshows']=$slide;
            $data = $slideshows;
            


            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
            );
            //cdbg::var_dump($return);
            return $return;
        }

    }
    