<?php

    class CApiServer_SixtyTwoHallFamillyService_Data_MemberDeleteAddress extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();


            $ip = crequest::remote_address();
            $member_address_id = carr::get($this->request, 'member_address_id');

            //Update Member Address
            try {
                $db->update("member_address", array("status" => 0, "updated" => date("Y-m-d H:i:s"),
                    "updatedby" => $ip), array("member_address_id" => $member_address_id));
            }
            catch (Exception $ex) {
                $err_code++;
                $err_message = "Delete Address Not Succesfully";
            }

            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
            );
            return $return;
        }

    }
    