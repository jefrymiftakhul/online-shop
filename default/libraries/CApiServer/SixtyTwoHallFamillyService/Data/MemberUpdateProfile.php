<?php

    class CApiServer_SixtyTwoHallFamillyService_Data_MemberUpdateProfile extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();


            $date = date("Y-m-d H:i:s");            
            $member_id = $this->session->get('member_id');
            $name = carr::get($this->request, 'name');           
            $phone = carr::get($this->request, 'phone');
            $date_of_birth = carr::get($this->request, 'date_of_birth');
            $gender = carr::get($this->request, 'gender');
            $bank = carr::get($this->request, 'bank');
            $account_holder = carr::get($this->request, 'account_holder');
            $bank_account = carr::get($this->request, 'bank_account');
            $province_id = carr::get($this->request, 'province_id');
            $city_id = carr::get($this->request, 'city_id');
            $address = carr::get($this->request, 'address');
            
            //handling Update Profile
            if ($err_code == 0) {
                if (strlen($name) > 20) {
                    $err_code++;
                    $err_message = 'name must be least than equal to 20 characters.';
                }
            }        
            
            if ($err_code == 0) {
                if (strlen($phone) > 20) {
                    $err_code++;
                    $err_message = 'phone is required';
                }
            }
            
            if ($err_code == 0) {
                if (strlen($date_of_birth) > 20) {
                    $err_code++;
                    $err_message = 'date_of_birth is required';
                }
            }
            
            if ($err_code == 0) {
                if (strlen($gender) > 20) {
                    $err_code++;
                    $err_message = 'gender is required';
                }
            }

          

            //Update Profile
            try {
                $data_member = array(
                    'name' => $name,
                    'phone' => $phone,
                    'date_of_birth' => $date_of_birth,
                    'gender' => $gender,
                    'created' => $date,                    
                );
                if (strlen($bank) > 0) {
                    $data_member['bank'] = $bank;
                }
                if (strlen($account_holder) > 0) {
                    $data_member['account_holder'] = $account_holder;
                }
                if (strlen($bank_account) > 0) {
                    $data_member['bank_account'] = $bank_account;
                }
                if (strlen($address) > 0) {
                    $data_member['address'] = $address;
                }
                if (strlen($province_id) > 0) {
                    $data_member['province_id'] = $province_id;
                }
                if (strlen($city_id) > 0) {
                    $data_member['city_id'] = $city_id;
                }
                $db->update('member', $data_member, array("member_id" => $member_id));
            }
            catch (Exception $ex) {
                $err_code++;
                $err_message = "Update Profile Not Succesfully".$ex;
            }

            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
            );
            return $return;
        }

    }
    