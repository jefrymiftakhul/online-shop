<?php

    class CApiServer_SixtyTwoHallFamillyService_Data_GetBank extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $request = $this->request;

            $q = "
                select
                        *
                from
                        bank
                where
                        status>0				

            ";

            $r = $db->query($q);
            $arr_data = array();
            if ($r->count() > 0) {
                foreach ($r as $row) {
                    $arr_data['bank'][$row->bank_id] = $row->name;
                }
            }


            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $arr_data,
            );

            return $return;
        }

    }
    