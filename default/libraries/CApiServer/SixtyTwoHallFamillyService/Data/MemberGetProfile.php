<?php

    class CApiServer_SixtyTwoHallFamillyService_Data_MemberGetProfile extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();
            
            $member_id = $this->session->get('member_id');

            $q = '
                SELECT * FROM member WHERE status > 0                
                AND member_id = ' . $db->escape($member_id) . '
              ';
            $r = cdbutils::get_row($q);
            if ($r != null) {
                $is_verified = cobj::get($r, 'is_verified');
                if ($is_verified == '1') {
                    $member_id = cobj::get($r, 'member_id');
                    $email = cobj::get($r, 'email');
                    $name = cobj::get($r, 'name');
                    $date_of_birth = cobj::get($r, 'date_of_birth');
                    $phone = cobj::get($r, 'phone');
                    $gender = cobj::get($r, 'gender');
                    $bank = cobj::get($r, 'bank');
                    $account_holder = cobj::get($r, 'account_holder');
                    $bank_account = cobj::get($r, 'bank_account');
                    $province_id = cobj::get($r, 'province_id');
                    $city_id = cobj::get($r, 'city_id');
                    $address = cobj::get($r, 'address');

                    //$data['member_id'] = $member_id;
                    $data['name'] = $name;
                    $data['phone'] = $phone;
                    $data['date_of_birth'] = $date_of_birth;
                    $data['email'] = $email;   
                    $data['gender'] = $gender;
                    $data['bank'] = $bank;
                    $data['account_holder'] = $account_holder;
                    $data['bank_account'] = $bank_account;
                    $data['province_id'] = $province_id;
                    $data['city_id'] = $city_id;
                    $data['address'] = $address;
                }
                else {
                    $this->error->add_default("DATERR10002");
                }
            }
            else {
                $this->error->add_default("DATERR10001");
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    