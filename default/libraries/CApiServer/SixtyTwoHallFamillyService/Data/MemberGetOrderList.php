<?php

    class CApiServer_SixtyTwoHallFamillyService_Data_MemberGetOrderList extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();
            $arr_transaction = array();


            $member_id = $this->session->get('member_id');
            //$member_id = '53';


            $q_trans = "
                select 
                        t.member_id,
                        t.booking_code,
                        t.date,
                        t.date_hold,
                        t.transaction_status,
                        t.order_status,
                        t.payment_status,
                        t.shipping_status,
                        sum(td.qty) as total_qty,
                        case when type='buy' then t.channel_sell_price+t.total_shipping else t.total_deal+t.total_shipping end as total
                from 
                        transaction as t
                        Left join transaction_detail td on td.transaction_id = t.transaction_id
                WHERE 
                    t.status > 0
                    and td.status>0
                    AND t.member_id = " . $db->escape($member_id) . "
                GROUP BY
                    t.transaction_id
              ";
            $r_row = $db->query($q_trans);
            if ($r_row->count()>0) {
                
                $total_transaction = $r_row->count();
                $data['total_transaction'] = $total_transaction;


                foreach ($r_row as $row) {
                    $arr_transactions = array();
                    $arr_transactions['member_id'] = $row->member_id;
                    $arr_transactions['order_code'] = $row->booking_code;
                    $arr_transactions['date'] = $row->date;
                    $arr_transactions['hold_date'] = $row->date_hold;
                    $arr_transactions['transaction_status'] = $row->transaction_status;
                    $arr_transactions['order_status'] = $row->order_status;
                    $arr_transactions['payment_status'] = $row->payment_status;
                    $arr_transactions['shipping_status'] = $row->shipping_status;
                    $arr_transactions['qty'] = $row->total_qty;
                    $arr_transactions['total'] = $row->total;
                    $arr_transaction[] = $arr_transactions;
                }
                $data['transaction'] = $arr_transaction;
            }
            else {
                $err_code++;
                $err_message = "This member no transactions";
            }

            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
            );
           
            return $return;
        }

    }
    