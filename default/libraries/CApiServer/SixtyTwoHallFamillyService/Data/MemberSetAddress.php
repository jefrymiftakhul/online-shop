<?php

    class CApiServer_SixtyTwoHallFamillyService_Data_MemberSetAddress extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = 'Succesfully Added';


            $date = date("Y-m-d H:i:s");

            $ip = crequest::remote_address();
            $member_address_id = carr::get($this->request, 'member_address_id');
            $member_id = $this->session->get('member_id');
            $type = carr::get($this->request, 'type');
            $name = carr::get($this->request, 'name');
            $phone = carr::get($this->request, 'phone');
            $address = carr::get($this->request, 'address');
            $postal = carr::get($this->request, 'postal');
            $email = carr::get($this->request, 'email');
            $province_id = carr::get($this->request, 'province_id');
            $city_id = carr::get($this->request, 'city_id');
            $districts_id = carr::get($this->request, 'districts_id');
            
            //handling Member Address            
            if ($err_code == 0) {
                if (strlen($type) == 0) {
                    $err_code++;
                    $err_message = 'type is required';
                }
            }
            
            if ($err_code == 0) {
                if (strlen($name) == 0) {
                    $err_code++;
                    $err_message = 'name must be least than equal to 20 characters.';
                }
            }
            
            if ($err_code == 0) {
                if (strlen($phone) == 0) {
                    $err_code++;
                    $err_message = 'phone is required';
                }
            }
            
            if ($err_code == 0) {
                if (strlen($address) == 0) {
                    $err_code++;
                    $err_message = 'address is required';
                }
            }
            
            if ($err_code == 0) {
                if (strlen($postal) == 0) {
                    $err_code++;
                    $err_message = 'postal is required';
                }
            }
            
            if ($err_code == 0) {
                if (strlen($email) == 0) {
                    $err_code++;
                    $err_message = 'email is required';
                }
            }                       
                       
            
            if ($err_code == 0) {
                if ($province_id == 0) {
                    $err_code++;
                    $err_message = 'province_id is required';
                }
            }
            
            if ($err_code == 0) {
                if ($city_id == 0) {
                    $err_code++;
                    $err_message = 'city_id is required';
                }
            }
            
            if ($err_code == 0) {
                if ($districts_id == 0) {
                    $err_code++;
                    $err_message = 'districts_id is required';
                }
            }

            
            try {
                $data_member_address = array(
                    'member_id' => $member_id,
                    'type' => $type,
                    'name' => $name,
                    'phone' => $phone,
                    'address' => $address,
                    'postal' => $postal,
                    'email' => $email,                   
                    'province_id' => $province_id,
                    'city_id' => $city_id,
                    'districts_id' => $districts_id,
                    'created' => $date,
                    'createdby' => $email,
                    'createdip' => $ip,
                );

                $update_member_address = array(
                    'member_id' => $member_id,
                    'type' => $type,
                    'name' => $name,
                    'phone' => $phone,
                    'address' => $address,
                    'postal' => $postal,
                    'email' => $email,                    
                    'province_id' => $province_id,
                    'city_id' => $city_id,
                    'districts_id' => $districts_id,
                    'updated' => $date,
                    'updatedby' => $email,
                    'updatedip' => $ip,
                );
                if ($member_address_id == null) {
                    //insert Member Address
                    $r = $db->insert('member_address', $data_member_address);
                }
                else {
                    //Update Member Address
                    $r = $db->update('member_address', $update_member_address, array("member_address_id" => $member_address_id));
                }
            }
            catch (Exception $ex) {
                $err_code++;
                $err_message = "Set Address Not Succesfully".$ex;
            }

            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
                
            );
            return $return;
        }

    }
    