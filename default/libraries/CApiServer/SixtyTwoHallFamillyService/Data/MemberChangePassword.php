<?php

    class CApiServer_SixtyTwoHallFamillyService_Data_MemberChangePassword extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            


            $ip = crequest::remote_address();
            $date = date("Y-m-d H:i:s");
            $member_id = $this->session->get('member_id');
            $password = carr::get($this->request, 'password');
            $confirm_password = carr::get($this->request, 'confirm_password');

            if ($this->error()->code() == 0) {
                if ($confirm_password != $password) {
                    $this->error->add_default("DATERR10011");
                }
            }           
           
            
            if ($err_code == 0) {
                if (strlen($password) > 20) {
                    $err_code++;
                    $err_message = 'password is required';
                }
            }
            

            //Set Change Password
            try {
                $data_member = array(
                    'password' => md5($password),
                    'created' => $date,
                    'createdby' => $ip,
                );
                $r = $db->update('member', $data_member, array("member_id" => $member_id));
            }
            catch (Exception $ex) {
                $this->error->add_default("DATERR10012").$ex;
            }
            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,                
            );
            return $return;
        }

    }
    