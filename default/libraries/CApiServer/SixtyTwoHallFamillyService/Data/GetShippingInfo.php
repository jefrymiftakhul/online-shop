<?php

    class CApiServer_SixtyTwoHallFamillyService_Data_GetShippingInfo extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();
            
            $data_shipping = array();
           

            $err_code = 0;
            $err_message = '';
            $request = $this->request;

            //$product_type = carr::get($request, 'product_type');
            $districts = carr::get($request, 'districts');
            $q = "
                  select 
                        st.name as shipping_type, 
                        d.name as district,
                        sp.sell_price as price
                  from 
                        shipping_type st
                    left join shipping_price sp on sp.shipping_type_id = st.shipping_type_id
                    left join districts d on d.districts_id = sp.districts_id
                  where 
                        st.status > 0
                        and sp.status > 0
                        and d.status > 0
                ";

//            if (strlen($product_type) > 0) {
//                $q.="and t.product_type = " . $db->escape($product_type);
//            }
            
            if (strlen($districts) > 0) {
                $q.="and d.name = " . $db->escape($districts);
            }
            
           
            $r = $db->query($q);
            
            $arr_data = array();
            if ($r->count() > 0) {
                foreach ($r as $row) {
                    $data = array();
                    $data['shipping_type'] = $row->shipping_type;
                    $data['price'] = $row->price;
                    $arr_data[$row->shipping_type] = $data;
                }
            }
            $data_shipping['shipping_info'] = $arr_data;
            

            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data_shipping,
            );

            //cdbg::var_dump($data_shipping);
            return $return;
        }

    }
    