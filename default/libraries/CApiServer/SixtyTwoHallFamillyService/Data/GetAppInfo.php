<?php

    class CApiServer_SixtyTwoHallFamillyService_Data_GetAppInfo extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            //$this->session = CApiServer_Session::instance($this->api_server->get_type());
            $err_code = 0;
            $err_message = '';
            $request = $this->request;
            $org_id = $this->session->get('org_id');
            //get org
            $appInfo = org::get_org($org_id);
            if($appInfo==null){
                $err_code='1009';
                $err_message='Merchant Not Found';
            }
            
            $data = array();
            $list_payment_type = array();
            $list_product_type = array();
            if($err_code==0){
                if ($appInfo->have_product > 0) {
                    $list_product_type[] = 'product';
                }
                if ($appInfo->have_service > 0) {
                    $list_product_type[] = 'service';
                }
                if ($appInfo->have_gold > 0) {
                    $list_product_type[] = 'gold';
                }

                $data_payment=CF::get_file('data','data_payment');

                if(!file_exists($data_payment)){
                    $err_code=1203;
                    $err_message='Bank Transfer Not Available';
                }
                if($err_code==0){
                    $data_payment_type= include $data_payment;                
                }

                $image_url = curl::base() . 'application/admin62hallfamily/' . $appInfo->code . '/upload/logo/mobile_image/' . $appInfo->mobile_image;
                if($appInfo->mobile_image==null||(strlen($appInfo->mobile_image)==0)){
                    $image_url = curl::base() . 'application/admin62hallfamily/' . $appInfo->code . '/upload/logo/item_image/' . $appInfo->item_image;
                }

                $bank_transfer = ccfg::get('have_bank_transfer');
                $credit_card = ccfg::get('have_credit_card');
                $mandiri_ib = ccfg::get('have_mandiri_ib');
                $bca_klik_pay = ccfg::get('have_bca_klik_pay');


                $data['org_id'] = $appInfo->org_id;
                $data['code'] = $appInfo->code;
                $data['name'] = $appInfo->name;
                $data['item_favicon'] = $appInfo->item_favicon;
                $data['item_image'] = $image_url;
                $data['have_product'] = $appInfo->have_product;
                $data['have_service'] = $appInfo->have_service;
                $data['have_gold'] = $appInfo->have_gold;
                $data['have_zopim'] = $appInfo->have_zopim;
                $data['have_register'] = $appInfo->have_register;
                $data['have_deposit'] = $appInfo->have_deposit;
                $data['have_topup_transfer'] = $appInfo->have_topup_transfer;
                $data['have_member_get_member'] = $appInfo->have_member_get_member;
                $data['domain_admin'] = ccfg::get('domain_admin');
                $data['list_product_type'] = $list_product_type;
                if($bank_transfer > 0){
                    $list_payment_type['bank_transfer'] = carr::get($data_payment_type,'bank_transfer');
                }
                $list_payment_type['credit_card'] = carr::get($data_payment_type,'credit_card');
                $internet_banking=carr::get($data_payment_type,'internet_banking',array());
                if($bca_klik_pay>0){
                    $list_payment_type['internet_banking']['bca_klik_pay'] = carr::get($internet_banking,'bca_klik_pay');            
                }
                if($mandiri_ib>0){
                    $list_payment_type['internet_banking']['mandiri_ib'] = carr::get($internet_banking,'mandiri_ib');
                }
                $data['list_payment_type'] = $list_payment_type;

                // contact us

                $arr_option=array();
                $q="
                        select
                                *
                        from
                                cms_options
                        where
                                org_id=".$db->escape($org_id)."
                ";
                $r=$db->query($q);
                if($r->count()>0){
                    foreach($r as $row){
                        $arr_option[$row->option_name]=$row->option_value;
                    }
                }
                $arr_contact_us=array();
                $arr_contact_us['content']=carr::get($arr_option,'contact_us');
                $arr_contact_us['phone']=carr::get($arr_option,'contact_us_phone_1');
                $arr_contact_us['mobile']=carr::get($arr_option,'contact_us_mobile_1');

                $data['contact_us']=$arr_contact_us;
            }
            
            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
            );
            
            return $return;
        }

    }
    