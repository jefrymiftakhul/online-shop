<?php

    /**
     *
     * @author Khumbaka
     * @since  Dec 28, 2016
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_Login extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        private function login($auth_id) {
            
            $result = file_get_contents(curl::httpbase().'api/session/Login?auth_id=' . $auth_id);
            $result = json_decode($result, true);
            $err_code = carr::get($result, 'err_code');
            $session_id = null;
            if ($err_code > 0) {
                $this->error->add_default(1005);
            }
            else {
                $sess_data = carr::get($result, 'data');
                $session_id = carr::get($sess_data, 'session_id');
            }
            return $session_id;
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();

            $auth_id = carr::get($this->request, 'auth_id');
            $org_id = CF::org_id();            
            $org_code = "";
            $org_name = "";
            $q = "
                SELECT * 
                FROM org
                WHERE 
                    status>0
                    and md5(concat(auth_type,org_id,code)) = " . $db->escape($auth_id) . " 
                ";
            
            $row = cdbutils::get_row($q);
            if($row!=null){
                $org_id=cobj::get($row, 'org_id');
                $org_code=cobj::get($row, 'code');
                $org_name=cobj::get($row, 'name');
            }

            try {
                $member_rules = array(
                    'auth_id' => array('required'),
                );
                Helpers_Validation_Api::validate($member_rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error()->code() == 0) {
                $session_id = $this->login($auth_id);
                $data['session_id'] = $session_id;
            }

            if ($this->error()->code() == 0 && $session_id != null) {
                $slide = slide::get_slide($org_id, 'product', 0, 'landing_page');
                $data['slide'] = $slide;
            }
            $data['org'] = $org_id;
            $data['code'] = $org_code;
            $data['name'] = $org_name;
            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    