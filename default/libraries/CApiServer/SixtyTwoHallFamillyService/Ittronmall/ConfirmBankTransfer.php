<?php

    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_ConfirmBankTransfer extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';
            $data = array();
            $request = $this->request;

            $org_id = CF::org_id();
            $org_id = $this->session->get('org_id');
            $order_code = carr::get($request, 'order_code');
            $email = carr::get($request, 'email');
            $full_name = carr::get($request, 'full_name');
            $account_number = carr::get($request, 'account_number');
            $bank = carr::get($request, 'bank');
            $transfer_date = carr::get($request, 'transfer_date');
            $transfer_amount = carr::get($request, 'transfer_amount');

            try {
                $order_rules = array(
                    'order_code' => array('required'),
                    'email' => array('required'),
                    'full_name' => array('required'),
                    'account_number' => array('required'),
                    'bank' => array('required'),
                    'transfer_date' => array('required'),
                    'transfer_amount' => array('required'),
                );
                Helpers_Validation_Api::validate($order_rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error->code() == 0) {
                $transaction_id = transaction::get_transaction_id_from_booking($order_code, $email, $org_id);
                $data_transaction = transaction::get_transaction_all($transaction_id);                
                if($data_transaction==null){
                    $this->error->add('transaction not found',3999);
                }
                if($this->error->code()==0){
                    $org_parent_id = carr::get($data_transaction, 'org_parent_id');
                    $transaction_payment_id = carr::get($data_transaction, 'transaction_payment_id');
                    $transaction = carr::get($data_transaction, 'transaction');
                }
                if ($this->error->code()==0) {
                    try {
                        $db->begin();
                        $data_update_transaction = array(
                            'transaction_status' => 'CONFIRMED',
                            'order_status' => 'CONFIRMED',
                            'payment_status' => 'CONFIRMED',
                        );

                        $db->update('transaction', $data_update_transaction, array('transaction_id' => $transaction_id));

                        $data_update_transaction_payment = array(
                            'bank_from' => $bank,
                            'account_number_from' => $account_number,
                            'account_holder_from' => $full_name,
                            'transfer_date' => $transfer_date,
                            'payment_status' => 'CONFIRMED',
                            'amount' => $transfer_amount,                            
                            'transfer_amount' => $transfer_amount,                            
                        );

                        $db->update('transaction_payment', $data_update_transaction_payment, array('transaction_payment_id' => $transaction_payment_id));
                        // insert transaction history
                        $data_transaction_history = array(
                            'org_id' => $org_id,
                            'org_parent_id' => $org_parent_id,
                            'transaction_id' => $transaction_id,
                            'date' => date('Y-m-d H:i:s'),
                            'transaction_status_before' => 'PENDING',
                            'transaction_status' => 'CONFIRMED',
                            'ref_table' => 'transaction',
                            'ref_id' => $transaction_id,
                        );
                        $db->insert('transaction_history', $data_transaction_history);
                    }
                    catch (Exception $ex) {
                        $err_code = 1306;
                        $err_message = 'fail on confirm';
                    }
                    if ($err_code == 0) {
                        $db->commit();
                        $data['order_code'] = $order_code;
                        $data['email'] = $email;
                    }
                    else {
                        $db->rollback();
                    }
                }
            }
            if ($this->error->code() == 0) {
                email::send('IMTransaction',$transaction_id);
                //SixtyTwoHallFamilyController::factory('api')->send_transaction('confirm', $data['order_code']);
            }
            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
            );
            return $return;
        }

    }
    