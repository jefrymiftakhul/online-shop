<?php
// include_once('Veritrans.php');
class CApiServer_SixtyTwoHallFamillyService_Ittronmall_VtWebCallback extends CApiServer_SixtyTwoHallFamillyService {

    public function __construct($engine) {
        parent::__construct($engine);
    }

    public function execute() {
        $db = CDatabase::instance();

        $err_code = 0;
        $err_message = '';
        $data = array();

        $ip = crequest::remote_address();
        $date = date("Y-m-d H:i:s");
        // $session_id = carr::get($this->request, 'session_id');
        // $title = carr::get($this->request, 'title');
        // $review = carr::get($this->request, 'review');
        // $star = carr::get($this->request, 'star');
        // $transaction_detail_id = carr::get($this->request, 'transaction_detail_id');
        // $org_id = $this->session->get('org_id');
        $notification = json_decode(file_get_contents('php://input'), true);
        $order_id = carr::get($notification, 'order_id');
        $transaction_status = carr::get($notification, 'transaction_status');
        $transaction_id = carr::get($notification, 'transaction_id');
        $fraud_status = carr::get($notification, 'fraud_status');
        $result = "";
        $r = cdbutils::get_row("
        SELECT t.transaction_id, t.booking_code, t.payment_status, cm.registration_id, cm.api_access_key, t.booking_code 
        FROM transaction t
        INNER JOIN cloud_messaging cm ON cm.cloud_messaging_id = t.cloud_messaging_id
        INNER JOIN transaction_payment tp ON tp.transaction_id = t.transaction_id
        WHERE tp.payment_code = " . $db->escape($order_id));
        if($r != null) {
            $options = array();
            $options['booking_code'] = $r->booking_code;
            $payment_status = $r->payment_status;
            $transaction_id = $r->transaction_id;
            
            // ------------------
            // $payment_status = "PENDING";
            // $transaction_id = "123213123";
            // $options['booking_code'] = "AAAA";
            // ------------------
            
            $status_payment = "";
            $message = "";
            $title = "";
            if ($transaction_status == 'settlement' && $fraud_status == 'accept') {
                $status_payment = 'SUCCESS';
                $title = "Payment";
                $message = "Booking code " . $options['booking_code'] . " has been successfully at " . date("d-m-Y H:i:s");
            } else if ($transaction_status == 'capture' && $fraud_status == 'accept') {
                $status_payment = 'SUCCESS';
                $title = "Payment";
                $message = "Booking code " . $options['booking_code'] . " has been successfully at " . date("d-m-Y H:i:s");
            } else if ($transaction_status == 'capture' && $fraud_status == 'challenge') {
                $status_payment = 'PENDING';
            } else if ($transaction_status == 'deny' || $fraud_status == 'deny') {
                $status_payment = 'FAILED';
                $title = "Payment";
                $message = "Booking code " . $options['booking_code'] . " has been rejected at " . date("d-m-Y H:i:s");
            } else if ($transaction_status == 'settlement') {
                $status_payment = 'SUCCESS';
                $title = "Payment";
                $message = "Booking code " . $options['booking_code'] . " has been successfully at " . date("d-m-Y H:i:s");
            } else if ($transaction_status == 'pending') {
                $status_payment = 'PENDING';
            } else {
                $status_payment = 'FAILED';
                $title = "Payment";
                $message = "Booking code " . $options['booking_code'] . " has been fails at " . date("d-m-Y H:i:s");
            }
            if($payment_status == 'PENDING' && ($status_payment == 'SUCCESS' || $status_payment == 'PENDING')) {
                api_payment::midtrans_success($options);
                $data = array(
                    'transaction_status' => 'SUCCESS',
                    'order_status' => 'SUCCESS',
                    'payment_status' => 'SUCCESS',
                );
                $db->update('transaction', $data, array('transaction_id' => $transaction_id));
                $data = array(
                    'data_notif' => json_encode($notification),
                    'payment_status' => 'SUCCESS',
                );
                $db->update('transaction_payment', $data, array('transaction_id' => $transaction_id));

                $msg = array (
                    'body'      => $message,
                    'title'     => $title,
                    'vibrate'   => 1,
                    'sound'     => 1,
                );
                $fields = array (
                    'registration_ids'  => array( $r->registration_id ),
                    'notification'      => $msg
                );
                 
                $headers = array (
                    'Authorization: key=' . $r->api_access_key,
                    'Content-Type: application/json'
                );
                 
                $ch = curl_init();
                curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                curl_setopt( $ch,CURLOPT_POST, true );
                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                $result = curl_exec($ch );
            }
        } else {
            $err_code++;
            $err_message = "TRANSACTION NOT FOUND";
        }

        $data = array();
        $return = array(
            'err_code' => $err_code,
            'err_message' => $err_message,
            'data' => $result,
        );
        clog::activity("1", "VtWebCallback", json_encode($return));
        return $return;
    }

}
    