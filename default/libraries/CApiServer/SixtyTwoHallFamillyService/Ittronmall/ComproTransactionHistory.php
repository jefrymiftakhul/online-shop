<?php
/**
 * Description of TransactionHistory
 *
 * @author JIttron
 */
class CApiServer_SixtyTwoHallFamillyService_Ittronmall_ComproTransactionHistory extends CApiServer_SixtyTwoHallFamillyService {
    
    public function __construct($engine) {
        parent::__construct($engine);
    }
    
    public function execute() {
        
        $db = CDatabase::instance();
        $err_code = 0;
        $err_message = '';
        $data=array();

        //if(isset($this->request['app_id'])) 
        $parent_id = cdbutils::get_value("select org_id from org where code='compromall' and status>0");
        $app_id = carr::get($this->request, 'app_id', NULL);
        $transaction_status = carr::get($this->request, 'transaction_status', 'SUCCESS');
        $start_date = carr::get($this->request, 'start_date');
        $end_date = carr::get($this->request, 'end_date');
        
        try {
            $rules = array(
                'start_date' => array('required', 'date'),
                'end_date' => array('required', 'date'),
            );
            Helpers_Validation_Api::validate($rules, $this->request);
        }
        catch (Helpers_Validation_Api_Exception $e) {
            $err_code++;
            $err_message = $e->getMessage();
            $this->error->add($err_message, 2999);
        }
        
        
        
        if($err_code==0){
            
            $q = "
                select
                    t.transaction_id,
                    t.updated, 
                    t.order_status,
                    o.org_id,
                    o.code as org_code,
                    o.name as org_name,
                    t.type as transaction_type,
                    t.product_type as product_type,
                    t.date as date,
                    t.booking_code as booking_code,
                    t.ho_sell_price as ho_sell_price,
                    t.total_promo as total_promo,
                    t.channel_updown as channel_updown,
                    t.vendor_sell_price as channel_sell_price,
                    t.channel_commission_value as channel_commission_value,
                    t.vendor_nta as vendor_nta,
                    t.vendor_sell_price - t.vendor_nta as channel_profit,
                    t.transaction_status as transaction_status,
                    t.code as transaction_code, tp.payment_status,
                    t.total_shipping,
                    IF (t.transaction_status != 'Pending', tp.payment_code, '') as payment_code,
                    CONCAT(tc.billing_first_name,' ',tc.billing_last_name) as billing_name,
                    IF( (IFNULL(t.member_id,0))>0, 'YES', 'NO') as is_member,
                    IF( (IFNULL(t.is_paid,0))>0, 'YES', 'NO') as is_paid
                    ,p.product_id
                    ,p.name as product_name
                    ,p.sku 
                    ,v.name as vendor_name
                    ,v.vendor_id
                    ,v.vendor_code 
                    ,td.vendor_nta as vendor_nta_detail
                    ,td.channel_sell_price - td.ho_upselling as channel_sell_price_detail
                from
                    transaction as t
                    inner join transaction_detail as td on td.transaction_id=t.transaction_id
                    inner join product as p on p.product_id=td.product_id 
                    inner join vendor as v on v.vendor_id=td.vendor_id 
                    
                    inner join transaction_contact as tc on tc.transaction_id=t.transaction_id
                    left join org as o on o.org_id=t.org_id
                    left join transaction_payment as tp on (tp.transaction_id = t.transaction_id and tp.status>0)
                where
                    t.status>0 
                    and date(t.date)>=" . $db->escape($start_date)." 
                    and date(t.date)<=" . $db->escape($end_date)." 
                    and t.transaction_status = " . $db->escape($transaction_status)." 
                    and o.merchant_type='compromall' 
                ";
            
            if(strlen($app_id)>0){
                $q .= ' and t.org_id='.$db->escape($app_id).' ';
            }
            
            $q .= ' ORDER BY t.updated DESC ';
            
            $result = $db->query($q);
            
            $app_arr=array();
            
            if($result->count()>0){
                foreach ($result as $key => $val) {
                    
                    $app_id = cobj::get($val, 'org_id');
                    $app_name = cobj::get($val, 'org_name');
                    $booking_code = cobj::get($val, 'booking_code');
                    $vendor_id = cobj::get($val, 'vendor_id');
                    $vendor_name = cobj::get($val, 'vendor_name');
                    $vendor_code = cobj::get($val, 'vendor_code');
                    $product_id = cobj::get($val, 'product_id');
                    $product_name = cobj::get($val, 'product_name');
                    $sku = cobj::get($val, 'sku');
                    $ongkir = cobj::get($val, 'total_shipping');
                    
                    $product_arr=array();
                    $product_arr['vendor_code']=$vendor_code;
                    $product_arr['vendor_name']=$vendor_name;
                    $product_arr['product_name']=$product_name;
                    $product_arr['sku']=$sku;
                    $product_arr['nta']=cobj::get($val, 'vendor_nta_detail');
                    $product_arr['sell_price']=cobj::get($val, 'channel_sell_price_detail');
                    
                    $products=array();
                    $products[]=$product_arr;            
                    
                    if(!isset($data[$booking_code])){
                        $app_arr['booking_code']=$booking_code;
                        $app_arr['total_order']=1;
                        $app_arr['app_id']=$app_id;
                        $app_arr['app_name']=$app_name;
                        $app_arr['ongkir']=$ongkir;
                        $app_arr['nta_total']=cobj::get($val, 'vendor_nta');
                        $app_arr['sell_price_total']=cobj::get($val, 'channel_sell_price');
                        $app_arr['products']=$products;
                        $data[$booking_code] = $app_arr;
                    }
                    else{
                        $data[$booking_code]['total_order']+=1;
                        $data[$booking_code]['products'][]=$product_arr;
                    }
                }
            }
        }
        
        $data = array_values($data);
        $return = array(
            'err_code' => $err_code,
            'err_message' => $err_message,
            'data' => $data,
        );
        
        return $return;
        
    }
}
