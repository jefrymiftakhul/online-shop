<?php

    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_MemberSetAddress extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = 'Succesfully Added';
            $date = date("Y-m-d H:i:s");
            $ip = crequest::remote_address();
            $member_id = carr::get($this->request, 'member_id');
            $type = carr::get($this->request, 'type'); //INSERT OR UPDATE
            $billing_member_address_id = carr::get($this->request, 'billing_member_address_id');
            $shipping_member_address_id = carr::get($this->request, 'shipping_member_address_id');
            $billing_name = carr::get($this->request, 'billing_name');
            $shipping_name = carr::get($this->request, 'shipping_name');
            $billing_phone = carr::get($this->request, 'billing_phone');
            $shipping_phone = carr::get($this->request, 'shipping_phone');
            $billing_address = carr::get($this->request, 'billing_address');
            $shipping_address = carr::get($this->request, 'shipping_address');
            $billing_postal = carr::get($this->request, 'billing_postal');
            $shipping_postal = carr::get($this->request, 'shipping_postal');
            $billing_email = carr::get($this->request, 'billing_email');
            $shipping_email = carr::get($this->request, 'shipping_email');
            $billing_province_id = carr::get($this->request, 'billing_province_id');
            $shipping_province_id = carr::get($this->request, 'shipping_province_id');
            $billing_city_id = carr::get($this->request, 'billing_city_id');
            $shipping_city_id = carr::get($this->request, 'shipping_city_id');
            $billing_districts_id = carr::get($this->request, 'billing_districts_id');
            $shipping_districts_id = carr::get($this->request, 'shipping_districts_id');

            //handling Member Address        
            try {
                $order_rules = array(
                    'member_id' => array('rules' => array('required'),),
                    'type' => array('rules' => array('required'),),
                    'billing_member_address_id' => array('rules' => array('required'),),
                    'shipping_member_address_id' => array('rules' => array('required'),),
                    'billing_name' => array('rules' => array('required'),),
                    'shipping_name' => array('rules' => array('required'),),
                    'billing_phone' => array('rules' => array('required'),),
                    'shipping_phone' => array('rules' => array('required'),),
                    'billing_address' => array('rules' => array('required'),),
                    'shipping_address' => array('rules' => array('required'),),
                    'billing_postal' => array('rules' => array('required'),),
                    'shipping_postal' => array('rules' => array('required'),),
                    'billing_email' => array('rules' => array('required'),),
                    'shipping_email' => array('rules' => array('required'),),
                    'billing_province_id' => array('rules' => array('required'),),
                    'shipping_province_id' => array('rules' => array('required'),),
                    'billing_city_id' => array('rules' => array('required'),),
                    'shipping_city_id' => array('rules' => array('required'),),
                    'billing_districts_id' => array('rules' => array('required'),),
                    'shipping_districts_id' => array('rules' => array('required'),),
                );
                Helpers_Validation_Api::validate($order_rules, $this->request, 2);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }
            if ($this->error->code() == 0) {

                try {
                    $billing_data_member_address = array(
                        'member_id' => $member_id,
                        'type' => 'billing',
                        'name' => $billing_name,
                        'phone' => $billing_phone,
                        'address' => $billing_address,
                        'postal' => $billing_postal,
                        'email' => $billing_email,
                        'province_id' => $billing_province_id,
                        'city_id' => $billing_city_id,
                        'districts_id' => $billing_districts_id,
                        'created' => $date,
                        'createdby' => $billing_email,
                        'createdip' => $ip,
                    );
                    $shipping_data_member_address = array(
                        'member_id' => $member_id,
                        'type' => 'shipping',
                        'name' => $shipping_name,
                        'phone' => $shipping_phone,
                        'address' => $shipping_address,
                        'postal' => $shipping_postal,
                        'email' => $shipping_email,
                        'province_id' => $shipping_province_id,
                        'city_id' => $shipping_city_id,
                        'districts_id' => $shipping_districts_id,
                        'created' => $date,
                        'createdby' => $shipping_email,
                        'createdip' => $ip,
                    );
                    if ($type == 'INSERT') {
                        //insert Member Address
                        $r = $db->insert('member_address', $billing_data_member_address);
                        $r = $db->insert('member_address', $shipping_data_member_address);
                    }
                    if ($type == 'UPDATE') {
                        //Update Member Address
                        $data = 'Succesfully Updated';
                        $r = $db->update('member_address', $billing_data_member_address, array("member_address_id" => $billing_member_address_id, "type" => 'billing'));
                        $r = $db->update('member_address', $shipping_data_member_address, array("member_address_id" => $shipping_member_address_id, "type" => 'shipping'));
                    }
                }
                catch (Exception $ex) {
                    $err_code++;
                    $err_message = "Set Address Not Succesfully" . $ex;
                }
            }

            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
            );
            return $return;
        }

    }
    