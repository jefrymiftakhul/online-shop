<?php

    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_MemberListAddress extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data_all = array();

            $member_id = carr::get($this->request, 'member_id');

            try {
                $order_rules = array(
                    'member_id' => array(
                        'rules' => array('required'),
                    ),
                );
                Helpers_Validation_Api::validate($order_rules, $this->request, 2);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error->code() == 0) {
                //return array of address 
                //data {billing{1=>[member_addr_id,name,address],2,3,4}, shipping{1,2,3,4}}) 
                //order member_addr_id desc
                $q = '
                SELECT 
                        ma.member_address_id,ma.member_id,ma.type,ma.name,ma.phone,ma.address,ma.postal,
                        ma.email,ma.country_id,c.name as country_name,ma.province_id,p.name as province_name, 
                        ma.city_id, ci.name as city_name, ma.districts_id, d.name as district_name
                FROM 
                        member_address ma
                        left join country c on c.country_id = ma.country_id
                        left join province p on p.province_id = ma.province_id
                        left join city ci on ci.city_id = ma.city_id
                        left join districts d on d.districts_id = ma.districts_id
                WHERE ma.status > 0                
                AND ma.member_id = ' . $db->escape($member_id) . '
                ORDER BY ma.member_address_id desc
              ';

                $r = $db->query($q);
                if ($r->count() > 0) {
                    $billing = array();
                    $shipping = array();
                    foreach ($r as $row) {
                        $arr_data = array();
                        $arr_data['member_address_id'] = $row->member_address_id;
                        $arr_data['name'] = $row->name;
                        $arr_data['address'] = $row->address;
                        if ($row->type == 'billing') {
                            $billing[] = $arr_data;
                        }
                        if ($row->type == 'shipping') {
                            $shipping[] = $arr_data;
                        }
                    }
                    $data_all[] = array('billing' => $billing);
                    $data_all[] = array('shipping' => $shipping);
                }
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data_all,
            );

            return $return;
        }

    }
    