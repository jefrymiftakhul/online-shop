<?php

/**
 * Description of Register
 *
 * @author JIttron
 */
class CApiServer_SixtyTwoHallFamillyService_Ittronmall_ComproCheckDomain extends CApiServer_SixtyTwoHallFamillyService {

    public function __construct($engine) {
        parent::__construct($engine);
    }

    public function execute() {


        $db = CDatabase::instance();
        $err_code = 0;
        $err_message = '';
        $data = array();

        $parent_id = cdbutils::get_value("select org_id from org where code='compromall' and status>0");
        $domain = carr::get($this->request, 'domain');

        try {
            $rules = array(
                'domain' => array('required'),
            );
            Helpers_Validation_Api::validate($rules, $this->request);
        } catch (Helpers_Validation_Api_Exception $e) {
            $err_code++;
            $err_message = $e->getMessage();
            $this->error->add($err_message, 2999);
        }

        $response_domain = '';
        $response_name = '';
        $response_app_id = '';

        if ($err_code == 0) {
            //validate domain
            $valid_domain = compromall::valid_domain($domain);
            if (!$valid_domain) {
                $err_code++;
                $err_message = 'Domain invalid/Reserved';
            }
        }


        if ($err_code == 0) {
            $domain_status = cdbutils::get_row('select * from org where status=1 and domain=' . $db->escape($domain) . ' ');
            $api_domain_status = cdbutils::get_row('select * from org where status=1 and domain=' . $db->escape('api.'.$domain) . ' ');
            if ($domain_status != null) {
                $response_domain = $domain_status->domain;
                $response_name = $domain_status->name;
                $response_app_id = $domain_status->org_id;
            }
            if ($domain_status == null && $api_domain_status!=null ) {
                $response_domain = $domain;
                $response_name = $api_domain_status->name;
                $response_app_id = $api_domain_status->org_id;
            }
        }

        if ($err_code == 0) {
            $data = array(
                'app_id' => $response_app_id,
                'domain' => $response_domain,
                'name' => $response_name,
            );
        }

        $return = array(
            'err_code' => $err_code,
            'err_message' => $err_message,
            'data' => $data,
        );

        return $return;
    }

}
