<?php

    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_MemberUpdateProfile extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {                           
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();

            $date = date("Y-m-d H:i:s");
            $member_id = carr::get($this->request, 'member_id');
            $name = carr::get($this->request, 'name');
            $phone = carr::get($this->request, 'phone');
            $date_of_birth = carr::get($this->request, 'date_of_birth');
            $gender = carr::get($this->request, 'gender');
            $bank = carr::get($this->request, 'bank');
            $account_holder = carr::get($this->request, 'account_holder');
            $bank_account = carr::get($this->request, 'bank_account');
            $province_id = carr::get($this->request, 'province_id');
            $city_id = carr::get($this->request, 'city_id');
            $address = carr::get($this->request, 'address');
            $subscribe = carr::get($this->request, 'subscribe');
            
            if($this->error->code()==0){
                if(empty($member_id)){
                    $this->error->add('Member is required.', 2999);
                }
            }

            if($this->error->code()==0){
                if(empty($name)){
                    $this->error->add('Name is required.', 2999);
                }
            }
            
            if($this->error->code()==0){
                if(empty($phone)){
                    $this->error->add('Phone is required.', 2999);
                }
            }
            
            if($this->error->code()==0){
                if(empty($date_of_birth)){
                    $this->error->add('Date of birth is required.', 2999);
                }
            }
            
            if($this->error->code()==0){
                if(empty($gender)){
                    $this->error->add('Gender is required.', 2999);
                }
            }
            
//            try {
//                $order_rules = array(
//                    'member_id' => array(
//                        'rules' => array('required'),
//                    ),
//                    'name' => array(
//                        'rules' => array('required'),
//                    ),
//                    'phone' => array(
//                        'rules' => array('required'),
//                    ),
//                    'date_of_birth' => array(
//                        'rules' => array('required'),
//                    ),
//                    'gender' => array(
//                        'rules' => array('required'),
//                    ),                    
//                );
//                Helpers_Validation_Api::validate($order_rules, $this->request, 2);
//            }
//            catch (Helpers_Validation_Api_Exception $e) {
//                $err_code++;
//                $err_message = $e->getMessage();
//                $this->error->add($err_message, 2999);
//            }
//            
            
            
            $file_name_generated = "";
            if ($this->error->code() == 0) {                
                if (!empty($_FILES['image_name']['tmp_name'])) {
                    $filename = $_FILES['image_name']['name'];
                    $fileTemp = strlen($filename);
                    if(strlen($filename) > 0) {
                        $resource = CResources::factory("image", "profile");
                        $resource->add_size('view_profile', array(
                            'width' => '150',
                            'height' => '150',
                            'proportional' => true,
                            'crop' => true,
                        ));
                        $resource->add_size('view_thumbnail', array(
                            'width' => '30',
                            'height' => '30',
                            'proportional' => true,
                            'crop' => true,
                        ));
                        $path = file_get_contents($_FILES['image_name']['tmp_name']);
                        $file_name_generated = $resource->save($filename, $path);
                        $image_url = $resource->get_url($file_name_generated);
                    }
                }
            }
            $data = array();
            if ($this->error->code() == 0) {
                //Update Profile
                try {
                    $data_member = array(
                        'name' => $name,
                        'phone' => $phone,
                        'date_of_birth' => $date_of_birth,
                        'gender' => $gender,
                        'is_subscribe' => $subscribe,
                        'created' => $date,
                    );
                    if (strlen($bank) > 0) {
                        $data_member['bank'] = $bank;
                    }
                    if (strlen($account_holder) > 0) {
                        $data_member['account_holder'] = $account_holder;
                    }
                    if (strlen($bank_account) > 0) {
                        $data_member['bank_account'] = $bank_account;
                    }
                    if (strlen($address) > 0) {
                        $data_member['address'] = $address;
                    }
                    if (strlen($province_id) > 0) {
                        $data_member['province_id'] = $province_id;
                    }
                    if (strlen($city_id) > 0) {
                        $data_member['city_id'] = $city_id;
                    }
                    if(strlen($file_name_generated) > 0) {
                        $data_member['image_name'] = $file_name_generated;
                    }
                    $db->update('member', $data_member, array("member_id" => $member_id));
                    $data = $data_member;
                }
                catch (Exception $e) {
                    $this->error->add('Update Profile Not Succesfully '.$e->getMessage(), 2999);
                }
            }
            
            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data
            );
            return $return;
        }

    }
    