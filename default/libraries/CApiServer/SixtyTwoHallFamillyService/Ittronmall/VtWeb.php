<?php
// include_once('Veritrans.php');
class CApiServer_SixtyTwoHallFamillyService_Ittronmall_VtWeb extends CApiServer_SixtyTwoHallFamillyService {
    private $server_key = 'VT-server-_gT3UfXa3EuJAHIBda3iX81b';
    private $sandbox_url = 'https://api.sandbox.midtrans.com/v2';
    private $production_url = 'https://api.midtrans.com/v2';
    private $production = false;

    public function __construct($engine) {
        parent::__construct($engine);
    }

    public function execute() {
        $db = CDatabase::instance();

        $err_code = 0;
        $err_message = '';
        $data = array();

        $ip = crequest::remote_address();
        $date = date("Y-m-d H:i:s");
        $session_id = carr::get($this->request, 'session_id');
        $title = carr::get($this->request, 'title');
        $review = carr::get($this->request, 'review');
        $star = carr::get($this->request, 'star');
        $transaction_detail_id = carr::get($this->request, 'transaction_detail_id');
        $org_id = $this->session->get('org_id');
        // Veritrans_Config::$serverKey = 'VT-server-01M5ZjMz38HRZQun1H-5dLS0';
        // Veritrans_Config::$isProduction = true;
        // Veritrans_Config::$serverKey = 'VT-server-_gT3UfXa3EuJAHIBda3iX81b';
        $transaction_details = array(
          'order_id' => rand(),
          'gross_amount' => 145000, // no decimal allowed for creditcard
          );
        // Optional
        $item1_details = array(
            'id' => 'a1',
            'price' => 50000,
            'quantity' => 2,
            'name' => "Apple"
            );
        // Optional
        $item2_details = array(
            'id' => 'a2',
            'price' => 45000,
            'quantity' => 1,
            'name' => "Orange"
            );
        // Optional
        $item_details = array ($item1_details, $item2_details);
        // Optional
        $billing_address = array(
            'first_name'    => "Andri",
            'last_name'     => "Litani",
            'address'       => "Mangga 20",
            'city'          => "Jakarta",
            'postal_code'   => "16602",
            'phone'         => "081122334455",
            'country_code'  => 'IDN'
            );
        // Optional
        $shipping_address = array(
            'first_name'    => "Obet",
            'last_name'     => "Supriadi",
            'address'       => "Manggis 90",
            'city'          => "Jakarta",
            'postal_code'   => "16601",
            'phone'         => "08113366345",
            'country_code'  => 'IDN'
            );
        // Optional
        $customer_details = array(
            'first_name'    => "Andri",
            'last_name'     => "Litani",
            'email'         => "andri@litani.com",
            'phone'         => "081122334455",
            'billing_address'  => $billing_address,
            'shipping_address' => $shipping_address
            );
        // Fill transaction details
        $transaction = array(
            "vtweb" => array (
              "enabled_payments" => array('mandiri_clickpay', 'cimb_clicks', 'bri_epay', 'telkomsel_cash', 'xl_tunai', 'mandiri_ecash', 'indosat_dompetku', 'indosat_dompetku'),
              "credit_card_3d_secure" => true,
            ),
            'transaction_details' => $transaction_details,
            'customer_details' => $customer_details,
            'item_details' => $item_details,
            );
        // $url = Veritrans_VtWeb::getRedirectionUrl($transaction);

        $payloads = array(
            'payment_type' => 'vtweb',
            'vtweb' => array(
            // 'enabled_payments' => array('credit_card'),
                'credit_card_3d_secure' => true
            )
        );

        if (array_key_exists('item_details', $transaction)) {
            $gross_amount = 0;
            foreach ($transaction['item_details'] as $item) {
                $gross_amount += $item['quantity'] * $item['price'];
            }
            $payloads['transaction_details']['gross_amount'] = $gross_amount;
        }

        $payloads = array_replace_recursive($payloads, $transaction);

        $ch = curl_init();
        $url = $this->sandbox_url;
        if($this->production) {
            $url = $this->production_url;
        }

        $curl_options = array(
            CURLOPT_URL => $url . '/charge',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Accept: application/json',
                'Authorization: Basic ' . base64_encode($this->server_key . ':')
            ),
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CAINFO => dirname(__FILE__) . DS . 'cacert.pem'
        );

        $curl_options[CURLOPT_POST] = 1;
        if ($payloads) {
            $body = json_encode($payloads);
            $curl_options[CURLOPT_POSTFIELDS] = $body;
        } else {
            $curl_options[CURLOPT_POSTFIELDS] = '';
        }

        curl_setopt_array($ch, $curl_options);
        $result = curl_exec($ch);
        $result_array = json_decode($result);
        if (!in_array($result_array->status_code, array(200, 201, 202, 407))) {
            $message = '(' . $result_array->status_code . '): ' . $result_array->status_message;
            $err_code++;
            $err_message = $message;
        }

        $data['session_id'] = $session_id;
        if ($result_array->redirect_url) {
            $data['url'] = $result_array->redirect_url;
        } else {
            $err_code = $result_array->status_code;
            $err_message = $result_array->status_message;
        }

        $return = array(
            'err_code' => $err_code,
            'err_message' => $err_message,
            'data' => $data,
        );
        return $return;
    }

}
    