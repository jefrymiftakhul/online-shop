<?php

    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_GetShippingPrice extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';
            $data = array();
            $request = $this->request;
            $setting_ittronmall = CF::get_file('data', 'setting_ittronmall');
            if (!file_exists($setting_ittronmall)) {
                $err_message = 'file setting_ittronmall not found';
                $this->error->add($err_message, 2999);
            }

            if ($this->error->code() == 0) {
                $setting_ittronmall_arr = include $setting_ittronmall;
                $api = $setting_ittronmall_arr['api'];
                //$city_origin = $setting_ittronmall_arr['city_destination'];
                $org_id = $this->session->get('org_id');
                $city_origin = cdbutils::get_value('select city_id from org where org_id='.$db->escape($org_id).' ');
                $weight = carr::get($request, 'weight_shipping');
                $code = carr::get($request, 'code'); //jne, tiki, pos
                $service_type = carr::get($request, 'service_type'); // oke, yes, ctcoke
                $city_shipping = carr::get($request, 'city_shipping');
                $districts_shipping = carr::get($request, 'district_id');
                $r = cdbutils::get_row('select ori.raja_ongkir_city_id origin, ori.city_id'
                                . ', des.raja_ongkir_city_id destination, des.city_id'
                                . ' from city ori, city des'
                                . ' where ori.city_id = ' . $db->escape($city_origin)
                                . ' and des.city_id = ' . $db->escape($city_shipping));
                if ($r != null) {
                    $origin = $r->origin;
                    $destination = $r->destination;                    
                    
                    $data_response = raja_ongkir::calculate_cost($api, $origin, $destination, $weight, $code, $service_type);

                    $err_code = carr::get($data_response, 'err_code', 0);
                    if ($err_code > 0) {
                        $err_message = carr::get($data_response, 'err_message', '');
                    }
                    else {
                        $data_response = carr::get($data_response, 0);
                        $data_costs = carr::get($data_response, 'cost');
                        
                        // <editor-fold defaultstate="collapsed" desc="promo free ongkir">

                        $promo_expedition=expedition_promo::calculate_promo($districts_shipping);
                        $promo_expedition_amount=0;
                        $is_free=0;
                        if($promo_expedition!=null){
                            $promo_expedition_amount=carr::get($promo_expedition,'discount',0);
                            $is_free=carr::get($promo_expedition,'is_free');
                        }
                        if(count($data_costs)>0){
                            foreach ($data_costs as $key => $val) {
                                $tarif = $val['value']-$promo_expedition_amount;
                                if($tarif<0){
                                    $tarif=0;
                                }
                                if($is_free==1){
                                    $tarif=0;
                                }

                                $data_costs[$key]['value'] = $tarif;
                            }    
                        }
                        // </editor-fold>
                        
                        $data_response['cost'] = $data_costs;
                        
                        $data = $data_response;
                    }
                }
            }

            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
            );
            return $return;
        }

    }
    