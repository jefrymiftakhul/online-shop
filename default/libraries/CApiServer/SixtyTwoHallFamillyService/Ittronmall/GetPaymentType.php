<?php

    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_GetPaymentType extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();
            $org_id = CF::org_id();
            $org_id = $this->session->get('org_id');

            $q = "select * from org_bank where org_id = " . $db->escape($org_id) . " and status > 0";
            $r = $db->query($q);
            if ($r != null) {
                $data_transfer = array();
                $data_kartu_kredit = array();
                $data_internet_banking = array();
                foreach ($r as $v) {
                    $arr_data = array(
                        'bank' => $v->bank,
                        'account_number' => $v->account_number,
                        'account_holder' => $v->account_holder,
                    );
                    $data_transfer[] = $arr_data;
                }
                $data['transfer'] = $data_transfer;
                $data['kartu_kredit'] = $data_kartu_kredit;
                $data['internet_banking'] = $data_internet_banking;
            }

            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
            );
            
            return $return;
        }

    }
    