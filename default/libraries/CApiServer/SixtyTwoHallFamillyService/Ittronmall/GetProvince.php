<?php

    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_GetProvince extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';            

            $org_id = $this->session->get('org_id');
            $country_id = carr::get($this->request, 'country_id');

            $q = "
                select
                        *
                from
                        province
                where
                        status>0
            ";

            if (strlen($country_id) > 0) {
                $q.="and country_id = " . $db->escape($country_id);
            }

            $r = $db->query($q);
            $arr_data = array();
            if ($r->count() > 0) {
                foreach ($r as $row) {
                    $data = array();
                    $data['province_id'] = $row->province_id;
                    $data['country_id'] = $row->country_id;
                    $data['code'] = $row->code;
                    $data['name'] = $row->name;
                    $arr_data[] = $data;
                }
            }

            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $arr_data,
            );

            return $return;
        }

    }
    