<?php

    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_MemberLogin extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();

            $email = carr::get($this->request, 'email');
            $password = carr::get($this->request, 'password');
            $registration_id = carr::get($this->request, 'registration_id');
            $org_id = $this->session->get('org_id');

            try {
                $order_rules = array(
                    'email' => array(
                        'rules' => array('required'),
                    ),
                    'password' => array(
                        'rules' => array('required'),
                    ),
                );
                Helpers_Validation_Api::validate($order_rules, $this->request, 2);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error->code() == 0) {
                $q = '
                SELECT * FROM member WHERE status > 0
                AND email = ' . $db->escape($email) . ' AND password = ' . $db->escape(md5($password)) . '
                AND org_id = ' . $db->escape($org_id) . '
              ';
                
                $r = cdbutils::get_row($q);
                if ($r != null) {
                    $is_verified = cobj::get($r, 'is_verified');
                    $is_active = cobj::get($r, 'is_active');
                    if ($is_verified == '1') {
                        if ($is_active == '1') {
                            try {
                                $member_id = cobj::get($r, 'member_id');
                                $email = cobj::get($r, 'email');
                                $name = cobj::get($r, 'name');
                                $gender = cobj::get($r, 'gender');
                                $date_of_birth = cobj::get($r, 'date_of_birth');
                                $address = cobj::get($r, 'address');
                                $phone = cobj::get($r, 'phone');
                                $subscribe_product = cobj::get($r, 'is_subscribe');
                                $subscribe_service = cobj::get($r, 'is_subscribe_service');
                                $subscribe_gold = cobj::get($r, 'is_subscribe_gold');
                                $bank = cobj::get($r, 'bank');
                                $account_holder = cobj::get($r, 'account_holder');
                                $bank_account = cobj::get($r, 'bank_account');
                                $province_id = cobj::get($r, 'province_id');
                                $city_id = cobj::get($r, 'city_id');
                                $image_name = cobj::get($r, 'image_name');
                                $member_address = array();
                                $member_address_billing = array();
                                $member_address_shipping = array();

                                $q = "
                                select
                                        c.name as country,
                                        p.name as province,
                                        ct.name as city,
                                        d.name as districts,
                                        ma.*
                                from
                                        member_address as ma
                                        left join country as c on ma.country_id=c.country_id
                                        left join province as p on ma.province_id=p.province_id
                                        left join city as ct on ma.city_id=ct.city_id
                                        left join districts as d on ma.districts_id=d.districts_id
                                where
                                        is_active>0
                                        and member_id=" . $db->escape($member_id) . "
                                ";
                                $r = $db->query($q);
                                if ($r->count() > 0) {
                                    foreach ($r as $row) {
                                        $arr = array();
                                        $arr['id'] = $row->member_address_id;
                                        $arr['name'] = $row->name;
                                        $arr['phone'] = $row->phone;
                                        $arr['address'] = $row->address;
                                        $arr['postal'] = $row->postal;
                                        $arr['email'] = $row->email;
                                        $arr['country'] = $row->country;
                                        $arr['province'] = $row->province;
                                        $arr['city'] = $row->city;
                                        $arr['districts'] = $row->districts;
                                        if ($row->type == 'payment') {
                                            $member_address_billing[] = $arr;
                                        }
                                        if ($row->type == 'shipping') {
                                            $member_address_shipping[] = $arr;
                                        }
                                    }
                                }
                                $member_address['billing'] = $member_address_billing;
                                $member_address['shipping'] = $member_address_shipping;

                                $data['member_id'] = $member_id;
                                $data['email'] = $email;
                                $data['name'] = $name;
                                $data['gender'] = $gender;
                                $data['date_of_birth'] = $date_of_birth;
                                $data['address'] = $address;
                                $data['phone'] = $phone;
                                $data['subscribe_product'] = $subscribe_product;
                                $data['subscribe_service'] = $subscribe_service;
                                $data['subscribe_gold'] = $subscribe_gold;
                                $data['bank'] = $bank;
                                $data['account_holder'] = $account_holder;
                                $data['bank_account'] = $bank_account;
                                $data['province_id'] = $province_id;
                                $data['city_id'] = $city_id;
                                $resource = CResources::factory("image", "profile");
                                if (strlen($image_name) > 0) {
                                    $data['image_url'] = $resource->get_url($image_name);
                                }
                                else {
                                    $data['image_url'] = '';
                                }

                                //update cloud_messaging
                                if(strlen($registration_id)>0) {
                                    $data_cloud = array(
                                        'member_id'=>$member_id,
                                    );
                                    $where_cloud = array(
                                        'registration_id'=>$registration_id,
                                    );
                                    $db->update('cloud_messaging',$data_cloud,$where_cloud);
                                }
                            } catch(Exception $ex) {
                                $this->error->add('DB Error',99998);    
                            }
                        }
                        else {
                            $this->error->add('Maaf user anda telah dinonaktifkan, silahkan hubungi administrator...');
                        }    
                    }
                    else {
                        $this->error->add_default(10002);
                    }
                }
                else {
                    $this->error->add_default(10001);
                }
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    