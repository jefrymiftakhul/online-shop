<?php

    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_GetInfo extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $err_code = 0;
            $err_message = '';
            $data = array();
            $org_id = CF::org_id();
            $org_id = $this->session->get('org_id');
            $db = CDatabase::instance();
            $q = 'select distinct cms_terms_id from cms_menu where status > 0 and org_id = ' . $db->escape($org_id);
            $res = $db->query($q);

            $menu_leftside = array();
            if ($res != null) {
                foreach ($res as $k => $v) {
                    $q = 'select slug from cms_terms where status > 0 and cms_terms_id = ' . $db->escape($v->cms_terms_id);
                    $slug = cdbutils::get_value($q);
                    $temp = cms::nav_menu($slug);
                    $menu_leftside = array_merge($menu_leftside, $temp);
                    if (count($menu_leftside) > 0) {
                        foreach ($menu_leftside as $k => $v) {
                            $detail = array();
                            $id = $v['menu_object_id'];
                            $page = cms::page($id, $org_id);
                            if (count($page) > 0) {
                                $detail = $page;
                                $menu_leftside[$k]['detail'] = $detail;
                            }
                        }
                    }
                    else {
                        $menu_leftside = array();
                    }
                }
            }
           // cdbg::var_dump($menu_leftside);
           // exit;
            $q = "select t.name,t.slug, t.cms_terms_id  
            from cms_term_taxonomy as tt 
            inner join cms_terms as t on t.cms_terms_id=tt.cms_terms_id 
            where t.status > 0 
            and tt.taxonomy='category' 
            and t.slug='Artikel' 
            and tt.org_id=" . $db->escape($org_id);

            $r = cdbutils::get_row($q);
            $slug = '';
            
            if ($r != null) {
                $cms_terms_id = $r->cms_terms_id;
                $slug = $r->slug;
                
            }
             
            $detail = cms::get_post('', $slug, '');
            foreach ($menu_leftside as $k => $v) {
                if ($v['menu_object_id'] == $r->cms_terms_id) {
                    $menu_leftside[$k]['detail'] = $detail;
                    if (count($detail) == 1) {
                        $menu_leftside[$k]['detail'][0]['post_title'] = $menu_leftside[$k]['menu_name'];
                    }
                    break;
                }
            }

            $data['menu'] = $menu_leftside;
            $page = 'product';
            $slide = slide::get_slide($org_id, $page, 0);
            $data['slide'] = $slide;
            $product_category = product_category::get_product_category_menu($page);
            $data['product_category'] = $product_category;
            $product_deal = $this->get_product_deal($org_id, $page);
            $data['product_deal'] = $product_deal;
            $temp = ccfg::get('shipping_courier');
            $temp_service = ccfg::get('shipping_service_use');
            if(count($temp)>0){
                foreach ($temp as $k => $v) {
                    $service = array();
                    foreach ($temp_service as $k1 => $v1) {
                        if ($k == $k1) {
                            foreach ($v1 as $k2 => $v2) {
                                $service[] = array(
                                    'id' => $k2,
                                    'value' => $v2,
                                );
                            }
                        }
                    }
                    $arr_shipping = array(
                        'id' => $k,
                        'value' => $v,
                        'service' => $service,
                    );
                    $data['shipping_courier'][] = $arr_shipping;
                }
            }
            // contact us
            $db = CDatabase::instance();
            $arr_option = array();
            $q = " 
                select
                    *
                from
                    cms_options
                where
                    org_id=" . $db->escape($org_id) . "
            ";
            $r = $db->query($q);
            if ($r->count() > 0) {
                foreach ($r as $row) {
                    $arr_option[$row->option_name] = $row->option_value;
                }
            }
            $arr_contact_us = array();
            $arr_contact_us['content'] = carr::get($arr_option, 'contact_us');
            $arr_contact_us['phone'] = carr::get($arr_option, 'contact_us_phone_1');
            $arr_contact_us['mobile'] = carr::get($arr_option, 'contact_us_mobile_1');
            $arr_contact_us['ig'] = carr::get($arr_option, 'media_ig');
            $arr_contact_us['fb'] = carr::get($arr_option, 'media_fb');
            $arr_contact_us['tw'] = carr::get($arr_option, 'media_tw');
            $arr_contact_us['gp'] = carr::get($arr_option, 'media_gp');
            $data["contact_us"] = $arr_contact_us;
            $store_status = array();
            $close_store = cdbutils::get_row('select is_close_store,close_reason from org where org_id = '. $db->escape($org_id) ." ");
            $store_status['is_close_store'] = cobj::get($close_store, 'is_close_store');
            $store_status['close_reason'] = cobj::get($close_store, 'close_reason');
            $data['store_status'] = $store_status;

            $mid = cdbutils::get_row("
                    SELECT o.mid_prod, o.mid_api_dev, o.mid_api_prod, o.zopim_id, o.city_id, c.name as city_name
                    FROM org o 
                    left join city c on o.city_id = c.city_id 
                    WHERE org_id = " . $db->escape($org_id));
            $mid_prod_stat = $mid->mid_prod;
            $mid_api_dev = $mid->mid_api_dev;
            $mid_api_prod = $mid->mid_api_prod;
            $zopim_id = $mid->zopim_id;
            $use_mid_payment = "0";
            if($mid_prod_stat == 0 && strlen($mid_api_dev) > 0) {
                $use_mid_payment = "1";
            } else if($mid_prod_stat == 0 && strlen($mid_api_prod) > 0) {
                $use_mid_payment = "1";
            }
            $data['use_mid_payment'] = $use_mid_payment;
            $data['zopim_id'] = $zopim_id;
            $use_zopim = "0";
            if(strlen($zopim_id) > 0) {
                $use_zopim = "1";
            }
            $data['use_zopim'] = $use_zopim;
            $data['org_id'] = $org_id;
            $data['origin_city_id'] = cobj::get($mid, 'city_id');
            $data['origin_city_name'] = cobj::get($mid, 'city_name');
            // foreach ($temp_service as $k => $v) {
            //     $detail = array();
            //     foreach ($v as $k1 => $v1) {
            //         $detail[] = array(
            //             'id_service' => $k1,
            //             'value' => $v1,
            //         );
            //     }
            //     $arr_shipping_use = array(
            //         'id_shipping' => $k,
            //         'value' => $detail,
            //     );
            //     $data['shipping_service_use'][] = $arr_shipping_use;
            // }
            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            if(isset($_GET['debug'])){
                cdbg::var_dump($return);
                die;
            }
            return $return;
        }

        private function get_product_deal($org_id, $product_type) {
            $db = CDatabase::instance();
            $org=org::get_org($org_id);
            $merchant_category=cobj::get($org,'merchant_category');
            $q = "
                select
                    *
                from
                    cms_deal_setting
                where
                    status>0 
                    and merchant_category=".$db->escape($merchant_category)."
                    and product_type=" . $db->escape($product_type) . " LIMIT 20
            ";
            $r = $db->query($q);
            $deal = array();
            if ($r->count() > 0) {
                foreach ($r as $row) {
                    $arr_deal = array();
                    $arr_deal['product_type'] = $row->product_type;
                    $arr_deal['product_deal_type'] = $row->code;
                    $name = $row->name;
                    if (ccfg::get($row->code)) {
                        $name = ccfg::get($row->code);
                    }
                    $arr_deal['product_deal_name'] = $name;
                    $deals = deal::get_deal($org_id, $product_type, $row->code);
                    $arr_products = array();
                    foreach ($deals as $key_product => $val_product) {
                        $arr = api_products_detail::convert_products_api($val_product);
                        $arr_products[] = $arr;
                    }
                    $arr_deal['products'] = $arr_products;
                    $deal[] = $arr_deal;
                }
            }
            return $deal;
        }

    }
    