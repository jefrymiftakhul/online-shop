<?php
    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_GetFavoriteList extends CApiServer_SixtyTwoHallFamillyService {
        public function __construct($engine) {
            parent::__construct($engine);
        }
        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';
            $data = array();
            $arr_details = array();
            $arr_shippings = array();
            $arr_billings = array();
            $org_id = $this->session->get('org_id');
            $member_id = carr::get($this->request, 'member_id');
            try {
                $order_rules = array(
                    'member_id' => array('required'),
                );
                Helpers_Validation_Api::validate($order_rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }
            if ($this->error->code() == 0) {
                $q = '
                SELECT 
                    mw.member_wishlist_id,
                    mw.member_id,
                    mw.product_id                 
                FROM 
                    member_wishlist mw
                WHERE 
                    mw.status > 0            	  	
                AND mw.member_id = ' . $db->escape($member_id) . ' 
                ORDER BY mw.created DESC
                ';
                $r = $db->query($q);
                if ($r != null) {
                    foreach ($r as $k => $v) {
                        $data_detail_product = product::get_product($v->product_id);
                        $data_detail_product = api_products_detail::convert_products_api($data_detail_product);
                        $arr_data = array(
                            'member_wishlist_id' => cobj::get($v, 'member_wishlist_id'),
                            'product_id' => cobj::get($v, 'product_id'),
                            'product' => $data_detail_product
                        );
                        $data[] = $arr_data;
                    }
                }
                else {
                    $err_code++;
                    $err_message = "Data Not Found";
                    $this->error->add($err_message, 2999);
                }
            }
            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
            );
            return $return;
        }
    }
    