<?php

class CApiServer_SixtyTwoHallFamillyService_Ittronmall_Order extends CApiServer_SixtyTwoHallFamillyService {
    private $server_key = '';
    // private $server_key = 'VT-server-_gT3UfXa3EuJAHIBda3iX81b';

    // private $server_key = 'VT-server-01M5ZjMz38HRZQun1H-5dLS0';
    private $sandbox_url = 'https://api.sandbox.midtrans.com/v2';
    private $production_url = 'https://api.midtrans.com/v2';
    private $production = false;

    public function __construct($engine) {
        parent::__construct($engine);
    }

    public function execute() {
        $db = CDatabase::instance();
        $err_code = 0;
        $err_message = '';
        $data = array();
        $request = $this->request;
        //$org_id = CF::org_id();
        $org_id = $this->session->get('org_id');
        $payment_type = carr::get($request, 'payment_type');
        $cloud_messaging_id = carr::get($request, 'cloud_messaging_id');
        $payment_type_code = carr::get($request, 'payment_type_code', 'bca');
        $member_id = carr::get($request, 'member_id');
        $total_item = carr::get($request, 'total_item');
        $total_shipping = carr::get($request, 'total_shipping');
        $amount_payment = carr::get($request, 'amount_payment');
        $arr_item = carr::get($request, 'item');
        $arr_item_payment = carr::get($request, 'item_payment');
        $arr_contact_detail = carr::get($request, 'contact_detail');

        if ($this->error->code() == 0) {
            try {
                $order_rules = array(
                    'payment_type' => array(
                        'rules' => array('required'),
                    ),
                    'payment_type_code' => array(
                        'rules' => array('required'),
                    ),
                    'total_item' => array(
                        'rules' => array('required'),
                    ),
                    'total_shipping' => array(
                        'rules' => array('required'),
                    ),
                    'amount_payment' => array(
                        'rules' => array('required'),
                    ),
                    'item' => array(
                        'data_type' => 'array2D',
                        'field' => array(
                            'item_id' => array('rules' => array('required')),
                            'item_code' => array('rules' => array('required')),
                            'item_name' => array('rules' => array('required')),
                            'qty' => array('rules' => array('required')),
                            'price' => array('rules' => array('required')),
                        ),
                    ),
                    'item_payment' => array(
                        'data_type' => 'array2D',
                        'field' => array(
                            'item_id' => array('rules' => array('required')),
                            'item_code' => array('rules' => array('required')),
                            'item_name' => array('rules' => array('required')),
                            'qty' => array('rules' => array('required')),
                            'price' => array('rules' => array('required')),
                        ),
                    ),
                    'contact_detail' => array(
                        'data_type' => 'array',
                        'field' => array(
                            'first_name' => array('rules' => array('required')),
                            'last_name' => array('rules' => array('required')),
                            'email' => array('rules' => array('required', 'email')),
                            'phone' => array('rules' => array('required')),
                            'billing_address' => array(
                                'data_type' => 'array',
                                'field' => array(
                                    'first_name' => array('rules' => array('required')),
                                    'last_name' => array('rules' => array('required')),
                                    'address' => array('rules' => array('required')),
                                    'email' => array('rules' => array('required', 'email')),
                                    'province_id' => array('rules' => array('required')),
                                    'province' => array('rules' => array('required')),
                                    'city_id' => array('rules' => array('required')),
                                    'city' => array('rules' => array('required')),
                                    'district_id' => array('rules' => array('required')),
                                    'district' => array('rules' => array('required')),
                                    'postal_code' => array('rules' => array('required')),
                                    'phone' => array('rules' => array('required')),
                                    'country_code' => array('rules' => array('required')),
                                )
                            ),
                            'shipping_address' => array(
                                'data_type' => 'array',
                                'field' => array(
                                    'first_name' => array('rules' => array('required')),
                                    'last_name' => array('rules' => array('required')),
                                    'address' => array('rules' => array('required')),
                                    'email' => array('rules' => array('required', 'email')),
                                    'province_id' => array('rules' => array('required')),
                                    'province' => array('rules' => array('required')),
                                    'city_id' => array('rules' => array('required')),
                                    'city' => array('rules' => array('required')),
                                    'district_id' => array('rules' => array('required')),
                                    'district' => array('rules' => array('required')),
                                    'postal_code' => array('rules' => array('required')),
                                    'phone' => array('rules' => array('required')),
                                    'country_code' => array('rules' => array('required')),
                                )
                            ),
                        ),
                    ),
                );
                Helpers_Validation_Api::validate($order_rules, $request, 2);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }
        }
        $data['a'] = $this->error->code();
        $data['b'] = $err_message;

        if ($this->error->code() == 0) {
            $transaction_email = carr::get($arr_contact_detail, 'email');
            $member_id=carr::get($arr_contact_detail,'member_id');
            $session_pg = CApiClientSession::instance('PG');
            $session_pg_id = $session_pg->get('session_id');
            $arr_transaction = array(
                'page' => 'product',
                'api' => null,
                'type' => 'buy',
                'session_card_id' => null,
                'item' => $arr_item,
                'item_payment' => $arr_item_payment,
                'total_item' => $total_item,
                'total_shipping' => $total_shipping,
                'amount_payment' => $amount_payment,
                'contact_detail' => $arr_contact_detail,
            );
            $session_pg->set('transaction', $arr_transaction);
            $session_pg->set('payment_type', $payment_type);
            $session_pg->set('transaction_email', $transaction_email);
            $session_pg->set('transaction_from', 'MOBILE');

            $options = array();
            $options['session_pg_id'] = $session_pg_id;
            $options['payment_type'] = $payment_type;
            $options['payment_type_code'] = $payment_type_code;
            if ($payment_type == 'bank_transfer') {
                $file_payment_type = CF::get_file('data', 'data_payment');
                $data_payment_type = array();
                if (!file_exists($file_payment_type)) {
                    $err_code = 1203;
                    $err_message = 'File Data Payment Type Not Available';
                }
                if ($err_code == 0) {
                    $data_payment_type = include $file_payment_type;
                }
                $bank_transfer = carr::get($data_payment_type, 'bank_transfer');
                $product_code = carr::get($bank_transfer, 'product_code');
                $detail_bank = carr::get($bank_transfer, 'detail_bank');
                $bank = carr::get($detail_bank, $payment_type_code);
                $session_pg->set('bank', carr::get($bank, 'name'));
                $session_pg->set('account_number', carr::get($bank, 'account_number'));
                if ($err_code == 0) {
                    if ((!$member_id == null) || (strlen($member_id) > 0)) {
                        $session = Session::instance();
                        $session->set('member_id', $member_id);
                    }
                    $result = api_payment::insert_bank_transfer($options);
                    if ($result['err_code'] > 0) {
                        $err_code = $result['err_code'];
                        $err_message = $result['err_message'];
                    }
                }
                $data['order_code'] = $session_pg->get('booking_code');
                $data['email'] = $session_pg->get('transaction_email');
            }
        }
        if($payment_type == 'credit_card' || $payment_type == 'online_banking') {
            $r = cdbutils::get_row("
            SELECT mid_api_dev, mid_api_prod, mid_prod 
            FROM org 
            WHERE org_id = " . $db->escape($org_id));
            if($r->mid_prod == "0") {
                $this->production = false;
            } else {
                $this->production = true;
            }
            if($this->production) {
                if(strlen($r->mid_api_prod) > 0) {
                    $this->server_key = $r->mid_api_prod;
                } else {
                    $err_code = 1299;
                    $err_message = 'Payment Gateway not Available';
                }
            } else {
                if(strlen($r->mid_api_dev) > 0) {
                    $this->server_key = $r->mid_api_dev;
                } else {
                    $err_code = 1299;
                    $err_message = 'Payment Gateway not Available';
                }
            }
            $data['payment_type']=($payment_type == 'credit_card');
            $options['payment_status'] = "PENDING";
            $file_payment_type = CF::get_file('data', 'data_payment');
            $data_payment_type = array();
            if (!file_exists($file_payment_type)) {
                $err_code = 1203;
                $err_message = 'File Data Payment Type Not Available';
            }
            if ($err_code == 0) {
                $data_payment_type = include $file_payment_type;
            }
            if($payment_type == 'online_banking') {
                $bank_transfer = carr::get($data_payment_type, 'online_banking');
            }
            // $product_code = carr::get($bank_transfer, 'product_code');
            // $detail_bank = carr::get($bank_transfer, 'detail_bank');
            // $bank = carr::get($detail_bank, $payment_type_code);
            // $session_pg->set('bank', carr::get($bank, 'name'));
            // $session_pg->set('account_number', carr::get($bank, 'account_number'));
            if ($err_code == 0) {
                $payment_code = rand(10, 99) . date('ymdhis') . rand(10, 99);
                $payment_list = array('credit_card');
                if($payment_type == 'online_banking') {
                    $payment_list = array('mandiri_clickpay', 'cimb_clicks', 'bri_epay', 'telkomsel_cash', 'xl_tunai', 'mandiri_ecash', 'indosat_dompetku', 'indosat_dompetku');
                }
                $result = self::call_vt($payment_code, $amount_payment, $arr_item_payment, $arr_contact_detail, $payment_list);
                $data['payload'] = $result["data"]['payload'];
                if ($result['err_code'] > 0) {
                    $err_code = $result['err_code'];
                    $err_message = $result['err_message'];
                } else {
                    $data['url'] = $result["data"]["url"];
                    $data_midtrans = $result["data"]["result"];
                    if ((!$member_id == null) || (strlen($member_id) > 0)) {
                        $session = Session::instance();
                        $session->set('member_id', $member_id);
                    }
                    $result = api_payment::insert_midtrans($options);
                    if ($result['err_code'] > 0) {
                        $data['err_code_api_payment'] = $result['err_code'];
                        $data['err_message_api_payment'] = $result['err_message'];
                        $err_code = $result['err_code'];
                        $err_message = $result['err_message'];
                    } else {
                        $save = array(
                            'payment_vendor' => 'MD',
                            'payment_code' => $payment_code,
                            'data_notif' => $data_midtrans,
                        );
                        
                        $db->update('transaction_payment', $save, array('transaction_payment_id' => $session_pg->get('transaction_payment_id')));
                    }
                }
            }
            $data['order_code'] = $session_pg->get('booking_code');
            $data['email'] = $session_pg->get('transaction_email');
        }
        if ($err_code == 0) {
            $save = array(
                'cloud_messaging_id' => $cloud_messaging_id
            );
            $db->update('transaction', $save, array('transaction_id' => $session_pg->get('transaction_id')));
        }

        $return = array(
            'err_code' => $err_code,
            'err_message' => $err_message,
            'data' => $data,
        );
        return $return;
    }

    private function call_vt($transaction_id, $amount_payment, $arr_item_payment, $arr_contact_detail, $payment_list) {
        $err_code = 0;
        $err_message = '';
        $transaction_details = array(
          'order_id' => $transaction_id,
          'gross_amount' => $amount_payment, // no decimal allowed for creditcard
          );
        $item_list = array();
        foreach ($arr_item_payment as $temp) {
            $item_details = array(
                'id' => $temp['item_id'],
                'price' => $temp['price'],
                'quantity' => $temp['qty'],
                'name' => $temp['item_code']
            );
            $item_list[] = $item_details;
        }
        // Optional
        $billing_address = array(
            'first_name'    => $arr_contact_detail["billing_address"]["first_name"],
            'last_name'     => " ",
            'address'       => $arr_contact_detail["billing_address"]["address"],
            'city'          => $arr_contact_detail["billing_address"]["city"],
            'postal_code'   => $arr_contact_detail["billing_address"]["postal_code"],
            'phone'         => $arr_contact_detail["billing_address"]["phone"],
            'country_code'  => 'IDN'
            );
        // Optional
        $shipping_address = array(
            'first_name'    => $arr_contact_detail["shipping_address"]["first_name"],
            'last_name'     => " ",
            'address'       => $arr_contact_detail["shipping_address"]["address"],
            'city'          => $arr_contact_detail["shipping_address"]["city"],
            'postal_code'   => $arr_contact_detail["shipping_address"]["postal_code"],
            'phone'         => $arr_contact_detail["shipping_address"]["phone"],
            'country_code'  => 'IDN'
            );
        // Optional
        $customer_details = array(
            'first_name'    => $arr_contact_detail['first_name'],
            'last_name'     => " ",
            'email'         => $arr_contact_detail['email'],
            'phone'         => $arr_contact_detail['phone'],
            'billing_address'  => $billing_address,
            'shipping_address' => $shipping_address
            );
        // Fill transaction details
        $transaction = array(
            'transaction_details' => $transaction_details,
            'customer_details' => $customer_details,
            'item_details' => $item_list,
            );
        // $url = Veritrans_VtWeb::getRedirectionUrl($transaction);

        $payloads = array(
            'payment_type' => 'vtweb',
            'vtweb' => array(
            'enabled_payments' => $payment_list,
                'credit_card_3d_secure' => true
            )
        );

        if (array_key_exists('item_details', $transaction)) {
            // $gross_amount = 0;
            // foreach ($transaction['item_details'] as $item) {
            //     $gross_amount += $item['quantity'] * $item['price'];
            // }
            // $payloads['transaction_details']['gross_amount'] = $amount_payment;
        }

        $payloads = array_replace_recursive($payloads, $transaction);

        $ch = curl_init();
        $url = $this->sandbox_url;
        if($this->production) {
            $url = $this->production_url;
        }

        $curl_options = array(
            CURLOPT_URL => $url . '/charge',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Accept: application/json',
                'Authorization: Basic ' . base64_encode($this->server_key . ':')
            ),
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CAINFO => dirname(__FILE__) . DS . 'cacert.pem'
        );

        $curl_options[CURLOPT_POST] = 1;
        if ($payloads) {
            $body = json_encode($payloads);
            $curl_options[CURLOPT_POSTFIELDS] = $body;
        } else {
            $curl_options[CURLOPT_POSTFIELDS] = '';
        }

        curl_setopt_array($ch, $curl_options);
        $result = curl_exec($ch);
        // print_r($payloads);
        // print_r($result);
        // die();
        $data = array();
        if(strlen($result) > 0) {
            $result_array = json_decode($result);
            if (!in_array($result_array->status_code, array(200, 201, 202, 407))) {
                $message = '(' . $result_array->status_code . '): ' . $result_array->status_message;
                $err_code++;
                $err_message = $message;
            }
            if ($err_code == 0) {
                $data['url'] = $result_array->redirect_url;
                $data['result'] = $result;
            } else {
                $err_code = $result_array->status_code;
                $err_message = $result_array->status_message;
            }
        } else {
            $err_code++;
            $err_message = "No response from Payment Gateway, please call customer support for more information";
        }
        $data["payload"] = $payloads;
        clog::activity("1", "X2", json_encode($data));
        $return = array(
            'err_code' => $err_code,
            'err_message' => $err_message,
            'data' => $data,
        );
        return $return;
    }
}
    