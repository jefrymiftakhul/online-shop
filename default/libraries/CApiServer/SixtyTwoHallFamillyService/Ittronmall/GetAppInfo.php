<?php

    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_GetAppInfo extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();
            
            $err_code = 0;
            $err_message = '';
            $request = $this->request;
            $org_id = CF::org_id();
            
            $menu_leftside = cms::nav_menu('menu_left_help');
            foreach ($menu_leftside as $k => $v) {
                $detail = array();

                $id = $v['menu_object_id'];                
                $page = cms::page($id, $org_id);

                if (count($page) > 0) {
                    $detail = $page;
                }
                else {
                    $slug_category = '';
                    foreach (cms::category_list() as $k1 => $v1) {
                        $term_id = $v1['cms_terms_id'];
                        $slug_category = $v1['slug'];
                        if ($term_id == $id) {
                            break;
                        }
                    }
                    $detail = cms::get_post('', $slug_category, '');
                }
                $menu_leftside[$k]['detail'] = $detail;
            }
            
            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $menu_leftside,
            );
            
            return $return;
        }

    }
    