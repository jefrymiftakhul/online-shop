<?php

/**
 * Description of Register
 *
 * @author JIttron
 */
class CApiServer_SixtyTwoHallFamillyService_Ittronmall_ComproRegister extends CApiServer_SixtyTwoHallFamillyService {

    public function __construct($engine) {
        parent::__construct($engine);
    }

    public function execute() {

        /**
         * Induk
         * Kode
         * Domain
         * username
         * name // nama organisasi
         */
        $db = CDatabase::instance();
        $err_code = 0;
        $err_message = '';
        $data = array();

        //cdbg::var_dump($this->request);

        $parent_id = cdbutils::get_value("select org_id from org where code='compromall' and status>0");
        $name = carr::get($this->request, 'name');
        $username = carr::get($this->request, 'username');
        $password = carr::get($this->request, 'password');
        $domain = carr::get($this->request, 'domain');
        $zopim_id = carr::get($this->request, 'zopim_id', '3f0O8JfuAqbw4Wo8zmKFtjdOX6JxKanJ');


        try {
            $rules = array(
                'name' => array('required'),
                'username' => array('required'),
                'password' => array('required'),
                'domain' => array('required'),
            );
            Helpers_Validation_Api::validate($rules, $this->request);
        } catch (Helpers_Validation_Api_Exception $e) {
            $err_code++;
            $err_message = $e->getMessage();
            $this->error->add($err_message, 2999);
        }

        if ($err_code == 0) {
            //validate domain
            $valid_domain = compromall::valid_domain($domain);
            if (!$valid_domain) {
                $err_code++;
                $err_message = 'Domain invalid/Reserved';
            }
        }
        if ($err_code == 0) {
            $domain_status = cdbutils::get_row('select * from org where status=1 and domain=' . $db->escape($domain) . ' ');
            if ($domain_status != null) {
                $err_code++;
                $err_message = 'Domain already exist';
            }
        }



        if ($err_code == 0) {
            try {
                $db->begin();

                $tree = CTreeDB::factory('org');

                $code = strtolower(str_replace(' ', '-', $name));
                $code = cstr::sanitize($name);
                $data_org = array(
                    'parent_id' => $parent_id,
                    //'member_id' => $member_id,
                    'name' => $name,
                    'code' => $code,
                    'domain' => 'api.'.$domain,
                    'theme' => '62hallfamily',
                    'org_category' => 'Express',
                    'zopim_id' => $zopim_id,
                    'have_product' => 1,
                    'have_service' => 0,
                    'have_zopim' => 1,
                    'type' => 'ittronmall',
                    'merchant_type' => 'compromall',
                    'merchant_category' => 'compromall',
                    'created' => date('Y-m-d H:i:s'),
                    'createdby' => 'api',
                );
                $insert = $db->insert('org', $data_org);
                $org_id = $insert->insert_id();
                //$password = rand(1, 50000);

                $tree->rebuild_tree_all();

                $data_user = array(
                    "username" => $username,
                    "role_id" => ccfg::get('downline_role'),
                    "description" => 'User of ' . $code,
                    "org_id" => $org_id,
                    "vendor_id" => null,
                    "password" => md5($password),
                    "created" => date("Y-m-d H:i:s"),
                    "createdby" => 'api',
                    "updated" => date("Y-m-d H:i:s"),
                    "updatedby" => 'api',
                    'created' => date('Y-m-d H:i:s'),
                    'createdby' => 'api',
                );
                $db->insert('users', $data_user);
            } catch (Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
            }
        }

        // <editor-fold defaultstate="collapsed" desc="inset cms (pindah di first login)">
        if ($err_code == 0) {
            if (isset($org_id)) {
                $data = array(
                    'org_id' => $org_id,
                );

                //facebook
                $data_fb = array_merge($data, array(
                    'option_name' => 'media_fb',
                    'autoload' => 'yes',
                ));
                $db->insert('cms_options', $data_fb);

                //twitter
                $data_tw = array_merge($data, array(
                    'option_name' => 'media_tw',
                    'autoload' => 'yes',
                ));
                $db->insert('cms_options', $data_tw);

                //instagram
                $data_ig = array_merge($data, array(
                    'option_name' => 'media_ig',
                    'autoload' => 'yes',
                ));
                $db->insert('cms_options', $data_ig);

                //google+
                $data_gp = array_merge($data, array(
                    'option_name' => 'media_gp',
                    'autoload' => 'yes',
                ));
                $db->insert('cms_options', $data_gp);


                $data_org = array(
                    'type' => 'ittronmall',
                    'org_id' => $org_id,
                    'org_category' => 'Express',
                    'domain' => $domain,
                    'code' => $code,
                    'name' => $name,
                    'have_product' => 1,
                    'have_service' => 0,
                    'have_gold' => 0,
                    'merchant_type' => 'compromall',
                );

                //create data org
                $file_org = DOCROOT . 'data' . DS . 'org' . DS . $code . '.php';

                $data_org_file = array(
                    "org_id" => $org_id,
                    "org_code" => $code,
                    "org_name" => $name,
                );

                cphp::save_value($data_org_file, $file_org);

                $app_id = '892';
                $app_code = "ittronmall";
                $domain_front = 'api.' . $domain;
                $file_domain = DOCROOT . 'data' . DS . 'domain' . DS . $domain_front . '.php';

                $data_domain = array(
                    "app_id" => $app_id,
                    "app_code" => $app_code,
                    "org_id" => $org_id,
                    "org_code" => $code,
                    "store_id" => null,
                    "store_code" => null,
                    "domain" => $domain_front,
                );
                cphp::save_value($data_domain, $file_domain);

                $app_id = '891';
                $app_code = "adminittronmall";
                $domain_back = $domain;
                $file_domain = DOCROOT . 'data' . DS . 'domain' . DS . $domain_back . '.php';
                $data_domain = array(
                    "app_id" => $app_id,
                    "app_code" => $app_code,
                    "org_id" => $org_id,
                    "org_code" => $code,
                    "store_id" => null,
                    "store_code" => null,
                    "domain" => $domain_back,
                );

                cphp::save_value($data_domain, $file_domain);

                //create app setting for domain front
                $path = (DOCROOT . 'application' . DS . 'ittronmall' . DS . $code);
                if (!is_dir($path)) {
                    mkdir($path);
                }
                $path = (DOCROOT . 'application' . DS . 'ittronmall' . DS . $code . DS . 'config');
                if (!is_dir($path)) {
                    mkdir($path);
                }
                $file_app = $path . DS . 'app.php';


                $data_app = array(
                    //smtp
                    "smtp_from" => "noreply@" . $domain,
                    //org
                    "org_id" => $org_id,
                    "have_clock" => false,
                    "have_user_login" => false,
                    "have_user_access" => true,
                    "have_user_permission" => true,
                    "title" => $name,
                    "domain_admin" => "" . $domain,
                    "domain_front" => "api." . $domain,
                    "default_timezone" => "Asia/Jakarta",
                    "set_timezone" => true,
                    "multilang" => false,
                    "decimal_separator" => " ",
                    "thousand_separator" => ".",
                    "decimal_digit" => "0",
                    "date_formatted" => "d-m-Y",
                    "long_date_formatted" => "d-m-Y H:i:s",
                    "require_js" => false,
                    "compromall_system" => true,
                    'api_url' => 'http://62pay.co.id/api/',
                    'api_auth' => 'b27d10bffb120d6796d1fc4fe0447c40',
                    'api_domain_server' => 'http://62pay.co.id/',
                    'no_web_front' => '1',
                    'compromall_system' => true,
                    'have_wholesale' => true,
                    'have_confirm_product' => true,
                    'hold_hour' => '24', // in hour
                    'shipping_courier' => array(
                        'jne' => 'JNE',
                        'tiki' => 'TIKI',
                        'pos' => 'POS Indonesia',
                    ),
                    'shipping_service_use' => array(
                        'jne' => array(
                            'CTCOKE' => 'CTCOKE',
                            'REG' => 'REG',
                            'YES' => 'YES',
                            'CTC' => 'CTC',
                        ),
                        'tiki' => array(
                            'REG' => 'REG',
                        ),
                        'pos' => array(
                            'Surat Kilat Khusus' => 'Express Sameday Barang',
                            'Express Sameday Barang' => 'Paket Kilat Khusus',
                            'Express Next Day Barang' => 'Express Next Day Barang',
                        ),
                    ),
                );
                cphp::save_value($data_app, $file_app);

                //create app setting for domain admin
                $path = (DOCROOT . 'application' . DS . 'adminittronmall' . DS . $code);
                if (!is_dir($path)) {
                    mkdir($path);
                }
                $path = (DOCROOT . 'application' . DS . 'adminittronmall' . DS . $code . DS . 'config');
                if (!is_dir($path)) {
                    mkdir($path);
                }
                $file_app = $path . DS . 'app.php';


                $data_app = array(
                    "smtp_from" => "noreply@" . $domain,
                    //org
                    "org_id" => $org_id,
                    "title" => 'Admin ' . $name,
                    "domain_front" => "api." . $domain,
                    "domain_admin" => $domain,
                    "default_timezone" => "Asia/Jakarta",
                    "set_timezone" => true,
                    "multilang" => false,
                    "decimal_separator" => " ",
                    "thousand_separator" => ".",
                    "decimal_digit" => "0",
                    "date_formatted" => "d-m-Y",
                    "long_date_formatted" => "d-m-Y H:i:s",
                    "require_js" => false,
                    "compromall_system" => true,
                    'api_url' => 'http://62pay.co.id/api/',
                    'api_auth' => 'b27d10bffb120d6796d1fc4fe0447c40',
                    'api_domain_server' => 'http://62pay.co.id/',
                    'no_web_front' => '1',
                    'compromall_system' => true,
                    'have_wholesale' => true,
                    'have_confirm_product' => true,
                    'hold_hour' => '24', // in hour
                    'shipping_courier' => array(
                        'jne' => 'JNE',
                        'tiki' => 'TIKI',
                        'pos' => 'POS Indonesia',
                    ),
                    'shipping_service_use' => array(
                        'jne' => array(
                            'CTCOKE' => 'CTCOKE',
                            'REG' => 'REG',
                            'YES' => 'YES',
                            'CTC' => 'CTC',
                        ),
                        'tiki' => array(
                            'REG' => 'REG',
                        ),
                        'pos' => array(
                            'Surat Kilat Khusus' => 'Express Sameday Barang',
                            'Express Sameday Barang' => 'Paket Kilat Khusus',
                            'Express Next Day Barang' => 'Express Next Day Barang',
                        ),
                    ),
                );
                cphp::save_value($data_app, $file_app);


                //CMS
                if (strlen($parent_id) > 0) {
                    $data_org['parent_id'] = $parent_id;
                }
                $create_cms = createorg::create_cms($data_org);
            }
        }
        // </editor-fold>

        if ($err_code == 0) {
            $db->commit();
            $data = array(
                'app_id' => $org_id,
                'domain' => $domain,
                'name' => $name,
                'username' => $username,
                'password' => $password,
            );
        } else {
            $db->rollback();
        }

        $return = array(
            'err_code' => $err_code,
            'err_message' => $err_message,
            'data' => $data,
        );

        return $return;
    }

}
