<?php

    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_MemberGetProfile extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();

            $member_id = carr::get($this->request, 'member_id');
            $org_id = $this->session->get('org_id');

            try {
                $order_rules = array(
                    'member_id' => array(
                        'rules' => array('required'),
                    ),
                );
                Helpers_Validation_Api::validate($order_rules, $this->request, 2);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error->code() == 0) {
                $q = '
                SELECT * FROM member WHERE status > 0                
                AND member_id = ' . $db->escape($member_id) . '
              ';
                $r = cdbutils::get_row($q);
                if ($r != null) {
                    $is_verified = cobj::get($r, 'is_verified');
                    if ($is_verified == '1') {
                        $member_id = cobj::get($r, 'member_id');
                        $email = cobj::get($r, 'email');
                        $name = cobj::get($r, 'name');
                        $date_of_birth = cobj::get($r, 'date_of_birth');
                        $phone = cobj::get($r, 'phone');
                        $gender = cobj::get($r, 'gender');
                        $bank = cobj::get($r, 'bank');
                        $account_holder = cobj::get($r, 'account_holder');
                        $bank_account = cobj::get($r, 'bank_account');
                        $province_id = cobj::get($r, 'province_id');
                        $city_id = cobj::get($r, 'city_id');
                        $address = cobj::get($r, 'address');
                        $is_subscribe = cobj::get($r, 'is_subscribe');

                        //$data['member_id'] = $member_id;
                        $data['name'] = $name;
                        $data['phone'] = $phone;
                        $data['date_of_birth'] = $date_of_birth;
                        $data['email'] = $email;
                        $data['gender'] = $gender;
                        $data['subscribe'] = $is_subscribe;
                        $data['bank'] = $bank;
                        $data['account_holder'] = $account_holder;
                        $data['bank_account'] = $bank_account;
                        $data['province_id'] = $province_id;
                        $data['city_id'] = $city_id;
                        $data['address'] = $address;

                        $point = null;
                        $not_have_point = ccfg::get("not_have_point");
                        if ($not_have_point != '1') {
                            $point = 0;
                            $point_temp = member::get_member_balance_point($member_id);
                            if ($point_temp != null) {
                                $point = $point_temp;
                            }
                        }
                        $data['balance_point'] = $point;

                        $r_image_name = cobj::get($r, 'image_name');
                        if (!empty($r_image_name)) {
                            $image = image::get_image_url_front($r_image_name, 'view_profile');
                            $data['image_url'] = $image;
                        }
                        else {
//                            $this->error->add_default(2014);
                            $data['image_url'] = null;
                        }
                    }
                    else {
                        $this->error->add_default(10002);
                    }
                }
                else {
                    $this->error->add_default(10001);
                }
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    