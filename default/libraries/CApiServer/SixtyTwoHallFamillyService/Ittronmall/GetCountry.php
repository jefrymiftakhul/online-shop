<?php

    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_GetCountry extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';            

            $org_id = $this->session->get('org_id');
            
            $q = "
                select
                    *
                from
                    country			
                where
                    status>0				
            ";

            $r = $db->query($q);
            $arr_data = array();

            if ($r->count() > 0) {
                foreach ($r as $row) {
                    $data = array();
                    $data['country_id'] = $row->country_id;
                    $data['code'] = $row->code;
                    $data['name'] = $row->name;
                    $data['phone_code'] = $row->phone_code;
                    $arr_data[] = $data;
                }
            }

            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $arr_data,
            );

            return $return;
        }

    }
    