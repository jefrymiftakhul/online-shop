<?php
/**
 * Description of GetMessageList
 *
 * @author JIttron
 */
class CApiServer_SixtyTwoHallFamillyService_Ittronmall_GetMessageList extends CApiServer_SixtyTwoHallFamillyService {
    
    public function __construct($engine) {
        parent::__construct($engine);
    }

    public function execute() {
        
        $db = CDatabase::instance();
        $err_code = 0;
        $err_message = '';
        $data = array();
        
        $org_id=$this->session->get('org_id');
        
        $member_id=carr::get($this->request, 'member_id');
        
        if(empty($member_id)){
            $err_code++;
            $err_message='member_id is required.';
            $this->error->add($err_message, 2999);
        }
        
        if($err_code==0){
            $q='
                select msg.*, m.name as member_name, dm.name department_name  
                from message msg
                left join member m on msg.member_id=m.member_id
                left join department_msg dm on msg.department_msg_id=dm.department_msg_id
                where 
                    msg.member_id='.$db->escape($member_id).' and msg.status=1
                    and msg.org_id='.$db->escape($org_id).' 
                ';
            $result = $db->query($q);
            if($result->count()>0){
                foreach ($result as $key => $val) {
                    
                    //departement / department
                    $message_arr=array();
                    $message_arr['message_id']=cobj::get($val, 'message_id');
                    $message_arr['member_id']=cobj::get($val, 'member_id');
                    $message_arr['departement_message_id']=cobj::get($val, 'department_msg_id');
                    $message_arr['member_name']=cobj::get($val, 'member_name');
                    $message_arr['departement_name']=cobj::get($val, 'department_name');
                    $message_arr['subject']=cobj::get($val, 'subject');
                    $message_arr['notes']=cobj::get($val, 'notes');
                    $data[]=$message_arr;
                }
            }
        }
        
        $return = array(
            'err_code' => $err_code,
            'err_message' => $err_message,
            'data' => $data,
        );

        return $return;
    }
}
