<?php
    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_GetProductDetail extends CApiServer_SixtyTwoHallFamillyService {
        public function __construct($engine) {
            parent::__construct($engine);
        }
        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';
            $data = array();
            $org_id = $this->session->get('org_id');
            $row_org = org::get_org($org_id);
            $q='
                select o.org_id 
                    ,p.name as provice_name
                    ,c.name as city_name 
                from org o 
                inner join province p on o.province_id=p.province_id
                inner join city c on o.city_id=c.city_id
                where o.org_id='.$db->escape($org_id).' 
                    and o.status=1 
                ';
            $row_org = cdbutils::get_row($q);
            
            
            $request = $this->request;
            $product_type = carr::get($request, 'product_type');
            $member_id = carr::get($request, 'member_id');
            $sku = carr::get($request, 'sku');
            $product_type_id = cdbutils::get_value("select product_type_id from product_type where name=" . $db->escape($product_type));
            if ($product_type_id == null) {
                $err_code++;
                $err_message = "Product Type not Found";
            }
            $product_id = null;
            if ($err_code == 0) {
                // where org_id tidak dipake karna untuk mengatasi product share
                $product_id = cdbutils::get_value("
					select 
						product_id 
					from 
						product 
					where 
						status>0 
						and product_type_id=" . $db->escape($product_type_id) . "
						and sku=" . $db->escape($sku) . "
						#and org_id=" . $db->escape($org_id) . " 
                                                    
				");
            }
            if ($product_id == null) {
                $err_code++;
                $err_message = "Product not Found";
            }
            if ($err_code == 0) {
                $rating = cdbutils::get_value("
                    SELECT (SUM(r.star) / COUNT(r.transaction_detail_id)) AS rating
                    FROM transaction_detail td
                    INNER JOIN transaction t ON t.transaction_id = td.transaction_id
                    RIGHT JOIN review r ON r.transaction_detail_id = td.transaction_detail_id
                    WHERE td.product_id = " . $db->escape($product_id) . "
                    AND t.org_id = " . $db->escape($org_id) . " AND r.status > 0");
                $number_rating = cdbutils::get_value("
                    SELECT COUNT(r.review_id) AS rating
                    FROM review r
                    RIGHT JOIN transaction_detail td ON r.transaction_detail_id = td.transaction_detail_id
                    INNER JOIN transaction t ON t.transaction_id = td.transaction_id
                    WHERE td.product_id = " . $db->escape($product_id) . "
                    AND t.org_id = " . $db->escape($org_id) . " AND r.status > 0 ");
                $favorite_number = cdbutils::get_value("
                    SELECT COUNT(member_wishlist_id) AS favorite_number
                    FROM member_wishlist 
                    WHERE product_id = " . $db->escape($product_id) . "
                    AND status > 0");
                $is_favorite = "0";
                if(strlen($member_id) > 0) {
                    $favorite = cdbutils::get_value("
                        SELECT COUNT(member_wishlist_id) AS favorite
                        FROM member_wishlist 
                        WHERE product_id = " . $db->escape($product_id) . "
                        AND member_id = " . $db->escape($member_id) . "
                        AND status > 0");
                    if($favorite > 0) {
                        $is_favorite = "1";
                    }
                }
                $data_detail_product = product::get_product($product_id);
                $data_detail_product = api_products_detail::convert_products_api($data_detail_product);
                $option_get_attribute_all['product_id'] = $product_id;
                $data_attribute_category = product::get_attribute_all($option_get_attribute_all);
                $data_shipping_info = array();
                $arr_shipping_info = shipping::get_shipping_city_info($product_id);
                $data_shipping_info = $arr_shipping_info;
//                if (count($data_shipping_info) == 0) {
//                    $data_shipping_info['JAWA (2-14 hari kerja)']['city']['all'] = true;
//                    $data_shipping_info['LUAR JAWA (14-28 hari kerja)']['city']['all'] = true;
//                }
                
                if (count($data_shipping_info) == 0) {
                    $data_shipping_info['Barang dikirim dari '.cobj::get($row_org, 'city_name').' ']['city']['all'] = false;
                    //$data_shipping_info['Barang dikirim dari '.cobj::get($row_org, 'city_name').' ']['city']['surabaya'] = true;
                }
                
                $new_arr_shipping_info = array();
                $shipping_info_text = "";
                foreach($data_shipping_info as $key => $value) {
                    $city_old = $value['city'];
                    $city_new = array();
                    $shipping_info_text .= $key;
                    $all_city = false;
                    foreach($city_old as $key1 => $value1) {
                        if($key1 == "all" && $value1) {
                            $all_city = true;
                            //$shipping_info_text .= " for all city";
                        } else {
                            //$shipping_info_text .= ", for city:<br/>";
                        }
                        if(!$all_city) {
                            //$shipping_info_text .= $key1 . "<br/>";
                        }
                        $city_new[] = array(
                            "id" => $key1,
                            "enable" => ($value1 ? 1 : 0)
                        );
                    }
                    $new_arr_shipping_info[] = array(
                        "id" => $key,
                        "data" => array(
                            "city" => $city_new,
                            )
                        );
                    //$shipping_info_text .= "<br/>";
                }
                
                if($rating > 5) {
                	$rating = 5;
                }
                if($rating < 0) {
                	$rating = 0;
                }
                $data['product'] = $data_detail_product;
                $data['product']['product_image'] = product::get_product_image_lv($product_id);
                $data['shipping_info'] = $new_arr_shipping_info;
                $data['shipping_info_text'] = $shipping_info_text;
                $data['attribute'] = $data_attribute_category;
                $data['product']['rating'] = $rating;
                $data['product']['number_rating'] = $number_rating;
                $data['product']['favorite'] = $is_favorite;
                $data['product']['favorite_number'] = $favorite_number;
                $data['product_related'] = $this->get_product_related($product_id);
                $data['review'] = $this->get_review($product_id);
                $data['wholesaler'] = $this->get_wholesales($product_id);
            }
            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
            );
            
            return $return;
        }
        public function get_product_related($product_id) {
            $err_code = 0;
            $err_message = '';
            $db = CDatabase::instance();
            $product_related = array();
            
            $q_cat = "select pc.* from product as p inner join product_category pc on p.product_category_id=pc.product_category_id where p.product_id=".$db->escape($product_id);
            $row_cat = cdbutils::get_row($q_cat);

            $q_row = "
                select
                        p.*
                from
                        product p
                        inner join product_category as pc on p.product_category_id=pc.product_category_id
                where
                        p.status>0
                        and p.status_confirm = 'CONFIRMED'
                        and p.is_active = '1'
                        and p.product_id <> ".$db->escape($product_id)." 
                        and p.parent_id is null 
                        and p.product_category_id=".$db->escape($row_cat->product_category_id)."
                        and p.product_type_id=1
                        and p.org_id= ".$db->escape($this->session->get('org_id'))."
                order by rand() 
                limit 12;

            ";

            $r = $db->query($q_row);
            if ($r->count() > 0) {
                foreach ($r as $v) {
                    $arr_product_related = array();
                    $arr_product_related['product_id'] = $v->product_id;
                    $arr_product_related['url_key'] = $v->url_key;
                    $arr_product_related['product_code'] = $v->code;
                    $arr_product_related['product_name'] = $v->name;
                    $arr_product_related['product_sku'] = $v->sku;
                    $arr_product_related['weight'] = $v->weight;
                    $arr_product_related['stock'] = $v->stock;
                    $arr_product_related['image_url'] = $v->file_path;
                    $arr_detail_price = array();
                    $arr_detail_price['sell_price_start'] = $v->sell_price_start;
                    $arr_detail_price['sell_price_end'] = $v->sell_price_end;
                    $arr_detail_price['sell_price'] = $v->sell_price;
                    $arr_detail_price['sell_price_promo'] = $v->sell_price;
                    $arr_detail_price['promo_text'] = 0;
                    $arr_product_related['detail_price'] = $arr_detail_price;
                    $arr_product_related['quick_overview'] = $v->description;
                    $arr_product_related['show_minimum_stock'] = $v->show_minimum_stock;
                    $arr_product_related['overview'] = $v->overview;
                    $arr_product_related['spesification'] = $v->spesification;
                    $arr_product_related['faq'] = $v->faq;
                    $arr_product_related['is_available'] = $v->is_available;
                    $arr_flag = array();
                    $arr_flag['hot'] = 0;
                    $arr_flag['new'] = 1;
                    $arr_product_related['flag'] = $arr_flag;
                    $product_related[] = $arr_product_related;
                }
            }
            
            return $product_related;
        }

        public function get_review($product_id) {
            $err_code = 0;
            $err_message = '';
            $db = CDatabase::instance();
            //$org_id = CF::org_id();
            $org_id = $this->session->get('org_id');
            $reviews = array();
            $q = "
                SELECT r.star, r.title, r.review
				FROM transaction_detail td
				INNER JOIN transaction t ON t.transaction_id = td.transaction_id
				RIGHT JOIN review r ON r.transaction_detail_id = td.transaction_detail_id
				WHERE td.product_id = " . $db->escape($product_id) . "
                AND t.org_id = " . $db->escape($org_id) . "
                AND r.status > 0
                LIMIT 5";
            $r = $db->query($q);
            if ($r->count() > 0) {
                foreach ($r as $v) {
                    $review = array();
                    $review['star'] = $v->star;
                    $review['title'] = $v->title;
                    $review['review'] = $v->review;
                    $reviews[] = $review;
                }
            }

            return $reviews;
        }
        public function get_wholesales($product_id) {
            $err_code = 0;
            $err_message = '';
            $db = CDatabase::instance();
            //$org_id = CF::org_id();
            $org_id = $this->session->get('org_id');
            $wholesales = array();
            $q = "
                SELECT qty, price, description from product_price_wholesaler where status > 0 and product_id = " . $db->escape($product_id) . " ";
//                AND org_id = " . $db->escape($org_id) . " ";
            $r = $db->query($q);
            if ($r->count() > 0) {
                foreach ($r as $v) {
                    $wholesale = array();
                    $wholesale['qty'] = $v->qty;
                    $wholesale['price'] = $v->price;
                    $wholesale['description'] = $v->description;
                    $wholesales[] = $wholesale;
                }
            }

            return $wholesales;
        }
    }
    