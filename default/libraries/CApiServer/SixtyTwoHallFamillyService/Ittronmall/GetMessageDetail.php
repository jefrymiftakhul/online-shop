<?php
/**
 * Description of GetMessageDetail
 *
 * @author JIttron
 */
class CApiServer_SixtyTwoHallFamillyService_Ittronmall_GetMessageDetail extends CApiServer_SixtyTwoHallFamillyService {
    
    public function __construct($engine) {
        parent::__construct($engine);
    }

    public function execute() {
        
        $db = CDatabase::instance();
        $err_code = 0;
        $err_message = '';
        $data = array();
        
        $org_id=$this->session->get('org_id');
        
        //$member_id=carr::get($this->request, 'member_id');
        $message_id=carr::get($this->request, 'message_id');
        $message=array();
        $subject='';
        
        try {
            $rules = array(
                //'member_id' => array('required'),
                'message_id' => array('required'),
            );
            Helpers_Validation_Api::validate($rules, $this->request);
        } catch (Helpers_Validation_Api_Exception $e) {
            $err_code++;
            $err_message = $e->getMessage();
            $this->error->add($err_message, 2999);
        }
        
        if($err_code==0){
            $q='
                select 
                    msg.subject 
                    ,md.message_type
                    #,md.subject  
                    ,md.body   
                    ,m.name as member_name
                    ,dm.name department_name  
                from message msg
                left join member m on msg.member_id=m.member_id
                left join department_msg dm on msg.department_msg_id=dm.department_msg_id
                left join message_detail md on msg.message_id = md.message_id 
                where msg.org_id='.$db->escape($org_id).' and msg.message_id='.$db->escape($message_id).' and msg.status=1 
                ';
            
            $result = $db->query($q);            
            
            if($result->count()>0){
                foreach ($result as $key => $val) {
                    $message_arr=array();
                    $subject=cobj::get($val, 'subject');
                    $message_arr['message_type']=cobj::get($val, 'message_type');
                    $message_arr['body']=cobj::get($val, 'body');
                    $message[]=$message_arr;
                }
                
                $data['subject']=$subject;
                $data['detail_message']=$message;
            }
        }
        
        $return = array(
            'err_code' => $err_code,
            'err_message' => $err_message,
            'data' => $data,
        );

        return $return;
    }
}
