<?php
/**
 * Description of GetDepartment
 *
 * @author JIttron
 */
class CApiServer_SixtyTwoHallFamillyService_Ittronmall_GetDepartmentMsg extends CApiServer_SixtyTwoHallFamillyService{
    public function __construct($engine) {
        parent::__construct($engine);
    }

    public function execute() {
        
        $db = CDatabase::instance();
        $err_code = 0;
        $err_message = '';
        $data = array();
        
        $org_id=$this->session->get('org_id');        
        $department_id=carr::get($this->request, 'department_id');
        
        if($err_code==0){
            $q=' select * from department_msg where org_id='.$db->escape($org_id).' and status=1 and is_active=1 ';
            if(!empty($department_id)) $q.=' and department_msg_id='.$db->escape($department_id).' ';
            
            $result=$db->query($q);
            if($result->count()>0){
                foreach ($result as $key => $val) {
                    $department_arr=array();
                    $department_arr['department_id']=cobj::get($val, 'department_msg_id');
                    $department_arr['name']=cobj::get($val, 'name');
                    $department_arr['email']=cobj::get($val, 'email');
                    $department_arr['notes']=cobj::get($val, 'notes');
                    $data[]=$department_arr;
                }
            }
        }
        
        $return = array(
            'err_code' => $err_code,
            'err_message' => $err_message,
            'data' => $data,
        );

        return $return;
    }
}
