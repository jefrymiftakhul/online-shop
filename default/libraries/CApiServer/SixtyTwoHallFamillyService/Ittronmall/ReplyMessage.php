<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ReplyMessage
 *
 * @author JIttron
 */
class CApiServer_SixtyTwoHallFamillyService_Ittronmall_ReplyMessage extends CApiServer_SixtyTwoHallFamillyService {

    public function __construct($engine) {
        parent::__construct($engine);
    }

    public function execute() {
        $db = CDatabase::instance();
        $err_code = 0;
        $err_message = '';
        $data = array();

        $org_id = $this->session->get('org_id');
        $message_id = carr::get($this->request, 'message_id');
        $from = carr::get($this->request, 'from');
        $to = carr::get($this->request, 'to');
        $subject = carr::get($this->request, 'subject');
        $body = carr::get($this->request, 'body');
        $action_to = carr::get($this->request, 'action_to', 'department');

        if (empty($message_id)) {
            try {
                $rules = array(
                    'from' => array('required'),
                    'to' => array('required'),
                    'subject' => array('required'),
                    'body' => array('required'),
                );
                Helpers_Validation_Api::validate($rules, $this->request);
            } catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }
        } else {
            if (empty(trim($body))) {
                $err_code++;
                $err_message = 'body is required';
                $this->error->add($err_message, 2999);
            }
        }

        if ($err_code == 0) {
            try {
                $db->begin();

                if (empty($message_id)) {
                    msg_ticket::send_message($from, $to, $subject, $body, $action_to, $org_id);
                } else {
                    $subject = cdbutils::get_value('select subject from message where message_id=' . $db->escape($message_id) . ' and status=1 ');
                    msg_ticket::reply_to_department($message_id, $subject, $body, $org_id);
                }
            } catch (Exception $ex) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }
        }

        if ($err_code == 0) {
            //send email to department email
            $org = cdbutils::get_row("select * from org where org_id=" . $db->escape($org_id));
            $member = cdbutils::get_row("select * from member where member_id=" . $db->escape($from));
            $department_msg = cdbutils::get_row("select * from department_msg where department_msg_id=" . $db->escape($to));
            if ($department_msg != null && strlen($department_msg->email) > 0) {
                $recipient = array($department_msg->email);
                $bcc = array();
                $email_subject = "New Message [" . $org->name . "] - " . $subject;
                $message = "Dear Dept. " . $department_msg->name . ",<br />"
                        . "<br />"
                        . "Member Name: <strong>" . $member->name . "</strong><br />"
                        . "Member Email: <strong>" . $member->email . "</strong><br />"
                        . "<br />"
                        . "Subject: <strong>" . $subject . "</strong><br />"
                        . "Message: " . $body . "<br />"
                        . "<br />"
                        . "<br />"
                        . "Thank You<br />"
                        . "";
                $attachments = array();
                $bcc = array('joko@ittron.co.id', 'harunrosiandi77@gmail.com', 'hery@ittron.co.id');
                $cc = array();
                try {
                    cmail::send_smtp($recipient, $email_subject, $message, $attachments, $cc, $bcc);
                } catch (Exception $e) {
                    clog::log('error_email_reply_email_api.php', 'error_email', $e->getMessage());
                }
            }
          
        }

        if ($err_code == 0) {
            $db->commit();
        }

        $return = array(
            'err_code' => $err_code,
            'err_message' => $err_message,
            'data' => $data,
        );

        return $return;
    }

}
