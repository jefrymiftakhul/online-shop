<?php

    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_GetLogo extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';            
            $arr_data = array();

            $org_id = carr::get($this->request, 'org_id');
            
//            $imgsrc_drawer_background_login = '';
//            $imgsrc_drawer_background = '';
//            $imgsrc_top_logo = '';
//            $imgsrc_top_logo_colored = '';
            
            try {
                $rules = array(
                    'org_id' => array('required'),
                );
                Helpers_Validation_Api::validate($rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }
            
            if($err_code==0){
                $row = cdbutils::get_row('select * from mobile_app_requirement where org_id='.$db->escape($org_id).' and status=1 ');
                if($row!=null){
                    $imgsrc_drawer_background_login = cobj::get($row, 'filepath_drawer_background_without_logo');
                    $imgsrc_drawer_background = cobj::get($row, 'filepath_drawer_background_with_logo');
                    $imgsrc_top_logo = cobj::get($row, 'filepath_top_logo');
                    $imgsrc_top_logo_colored = cobj::get($row, 'filepath_top_logo_colored');

                    $arr_data['drawer_background_without_logo'] = $imgsrc_drawer_background_login;
                    $arr_data['drawer_background_with_logo'] = $imgsrc_drawer_background;
                    $arr_data['top_logo'] = $imgsrc_top_logo;
                    $arr_data['top_logo_colored'] = $imgsrc_top_logo_colored;
                }
            }
            
            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $arr_data,
            );
            
            return $return;
        }

    }
    