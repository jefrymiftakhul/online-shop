<?php

    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_MemberRegister extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();

            $ip = crequest::remote_address();
            $date = date("Y-m-d H:i:s");
            $session_id = carr::get($this->request, 'session_id');
            $name = carr::get($this->request, 'name');
            $email = carr::get($this->request, 'email');
            $password = carr::get($this->request, 'password');
            $phone = carr::get($this->request, 'phone');
            $confirm_password = carr::get($this->request, 'confirm_password');
            $subscribe = carr::get($this->request, 'subscribe');
            //$org_id = CF::org_id();
            $org_id = $this->session->get('org_id');

            try {
                $order_rules = array(
                    'name' => array(
                        'rules' => array('required'),
                    ),
                    'email' => array(
                        'rules' => array('required', 'email'),
                    ),
                    'password' => array(
                        'rules' => array('required'),
                    ),
                    'confirm_password' => array(
                        'rules' => array('required'),
                    ),
                );
                Helpers_Validation_Api::validate($order_rules, $this->request, 2);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = 'Gagal Melakukan Register. Mohon maaf, '.$e->getMessage();
                $this->error->add($err_message, 2999);                
            }

            if ($this->error->code() == 0) {
                $q = "SELECT count(*) as total 
                FROM member
                WHERE email = " . $db->escape($email) . " 
                and org_id = " . $db->escape($org_id) . "
                and status > 0
                ";
                $total = cdbutils::get_value($q);
                if (strlen($total) > 0 && $total > 0) {
                    $q = "SELECT count(*) as total 
                    FROM member
                    WHERE email = " . $db->escape($email) . " 
                    and org_id = " . $db->escape($org_id) . "
                    and status > 0 and is_verified = 0";
                    $total = cdbutils::get_value($q);
                    if (strlen($total) > 0 && $total > 0) {
                        $err_code++;
                        $this->error->add_default(10016);
                    }
                    else {
                        $err_code++;
                        $this->error->add_default(10010);
                    }
                }
            }

            if ($this->error->code() == 0) {
                if ($confirm_password != $password) {
                    $this->error->add_default(10011);
                }
            }

            if ($this->error->code() == 0) {
                try {
                    $db->begin();
                    $data_member = array(
                        'org_id' => $org_id,
                        'name' => $name,
                        'email' => $email,
                        'is_subscribe' => $subscribe,
                        'password' => md5($password),
                        'created' => $date,
                        'createdby' => $email,
                        'createdip' => $ip
                    );
                    if($phone != null) {
                        $data_member = array_merge($data_member, 
                            array('phone' => $phone)
                        );
                    }
                    $r = $db->insert('member', $data_member);
                    $member_id = $r->insert_id();
                }
                catch (Exception $ex) {
                    $this->error->add_default(10012);
                }
                if ($this->error->code() == 0) {
                    $db->commit();
                }
                else {
                    $db->rollback();
                }
            }

            if ($this->error->code() == 0) {
                email::send('IMRegister', $member_id);
                //SixtyTwoHallFamilyController::factory('api')->send_verification($member_id);
                // send verification email
            }

            if ($this->error->code() == 0) {
                $data['session_id'] = $session_id;
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    