<?php
    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_GetOrder extends CApiServer_SixtyTwoHallFamillyService {
        public function __construct($engine) {
            parent::__construct($engine);
        }
        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';
            $data = array();
            $arr_details = array();
            $arr_shippings = array();
            $arr_billings = array();
            $org_id = $this->session->get('org_id');
            $order_code = carr::get($this->request, 'order_code');
            $transaction_email = carr::get($this->request, 'email');
            try {
                $order_rules = array(
                    'order_code' => array('required'),
                    'email' => array('required'),
                );
                Helpers_Validation_Api::validate($order_rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }
            if ($this->error->code() == 0) {
                $q = '
                SELECT 
                    t.booking_code,
                    t.date,
                    t.date_hold,
                    t.email,
                    t.order_status,
                    t.transaction_status,
                    t.shipping_status,
                    t.total_shipping,
                    tp.payment_type,
                    tp.bank,
                    tp.account_number,
                    t.total_shipping as total,                    
                    SUM(td.qty) AS total_item,
                    tp.total_charge,
                    tp.total_payment,                                                    
                    tp.payment_status,    
                    tp.data_notif,
                    ts.expedition_no,
                    ts.expedition_name,
                    t.total_shipping               
                FROM 
                    transaction t
                Left join transaction_payment tp on tp.transaction_id = t.transaction_id
            	Left join transaction_shipping ts on ts.transaction_id = t.transaction_id
            	left join transaction_detail td on td.transaction_id = t.transaction_id
                Left join transaction_contact tc on tc.transaction_id = t.transaction_id
                WHERE 
                    t.status > 0            	  	
                AND t.booking_code = ' . $db->escape($order_code) . '   
                AND t.email = ' . $db->escape($transaction_email) . '     
              ';
                $r = cdbutils::get_row($q);
                $r_row = $db->query($q);
                if ($r != null) {
                    $booking_code = cobj::get($r, 'booking_code');
                    if ($booking_code == null) {
                        $err_message = "Data Not Found";
                        $this->error->add($err_message, 2999);
                        $return = array(
                            'err_code' => $err_code,
                            'err_message' => $err_message,
                            'data' => $data,
                        );
                        return $return;
                    }
                    $transaction_email = cobj::get($r, 'email');
                    $date = cobj::get($r, 'date');
                    $order_status = cobj::get($r, 'order_status');
                    $payment_type = cobj::get($r, 'payment_type');
                    $bank = cobj::get($r, 'bank');
                    $account_number = cobj::get($r, 'account_number');
                    $total_item = cobj::get($r, 'total_item');
                    $total_charge = cobj::get($r, 'total_charge');
                    $total_payment = cobj::get($r, 'total_payment');
                    $total_shipping = cobj::get($r, 'total_shipping');
                    $payment_status = cobj::get($r, 'payment_status');
                    $transaction_status = cobj::get($r, 'transaction_status');
                    $shipping_status = cobj::get($r, 'shipping_status');
                    $expedition_no = cobj::get($r, 'expedition_no');
                    $expedition_name = cobj::get($r, 'expedition_name');
                    $total_shipping = cobj::get($r, 'total_shipping');
                    $hold_date = cobj::get($r, 'date_hold');
                    $data['order_code'] = $booking_code;
                    $data['transaction_email'] = $transaction_email;
                    $data['date'] = $date;
                    $data['hold_date'] = $hold_date;
                    $data['order_status'] = $order_status;
                    $data['transaction_status'] = $transaction_status;
                    $data['expedition_name'] = $expedition_name;
                    $data['expedition_no'] = $expedition_no;
                    $data['total_shipping'] = $total_shipping;
                    $data['shipping_status'] = $shipping_status;
                    $data_notif = cobj::get($r, 'data_notif');
                    $data['midtrans'] = array(
                        'status_code' => '',
                        'status_message' => '',
                        'redirect_url' => ''
                    );
                    if(strlen($data_notif) > 0) {
                        $data_notif = json_decode($data_notif, true);
                        if(isset($data_notif["status_code"])) {
                            $data['midtrans']['status_code'] = $data_notif["status_code"];
                        }
                        if(isset($data_notif["status_message"])) {
                            $data['midtrans']['status_message'] = $data_notif["status_message"];
                        }
                        if(isset($data_notif["redirect_url"])) {
                            $data['midtrans']['redirect_url'] = $data_notif["redirect_url"];
                        }
                    }
                    if ($r_row->count() > 0) {
                        $q_row = "
                        SELECT                                                 
                            p.name as product_name,
                            td.qty,
                            p.file_path as image_url, 						  
                            p.image_name,    
                            td.channel_sell_price as price,
                            r.review,
                            r.title,
                            r.star,
                            td.transaction_detail_id
                        FROM 
                            transaction t
                        left join transaction_detail td on td.transaction_id = t.transaction_id
                        left join product p on p.product_id = td.product_id
                        left join review r on r.transaction_detail_id = td.transaction_detail_id
                        WHERE 
                            t.status > 0
                            AND td.status > 0
                            AND p.status > 0
                        AND t.booking_code = " . $db->escape($order_code) . "
                        ";
                        $r = $db->query($q_row);
                        if ($r->count() > 0) {
                            foreach ($r as $row) {
                                $arr_detail = array();
                                $arr_detail['product_name'] = $row->product_name;
                                $arr_detail['transaction_detail_id'] = $row->transaction_detail_id;
                                $arr_detail['image_url'] = $row->image_url; //image::get_image_url($row->image_name, 'view_all');
                                $arr_detail['qty'] = $row->qty;
                                $arr_detail['price'] = $row->price;
                                $arr_detail['title'] = $row->title;
                                $arr_detail['review'] = $row->review;
                                $arr_detail['star'] = $row->star;
                                $arr_details[] = $arr_detail;
                            }
                        }
                    }
                    $data['details'] = $arr_details;
                    $data['total_item'] = $total_item;
                    $data['total_charge'] = $total_charge;
                    $data['total_payment'] = $total_payment;
                    $data['total_shipping'] = $total_shipping;
                    $data['payment_status'] = $payment_status;
                    $data['payment_type'] = $payment_type;
                    $data['bank'] = $bank;
                    $data['account_number'] = $account_number;
                    if ($r_row->count() > 0) {
                        $q_row = "
                                SELECT 
                                    tc.shipping_first_name,
                                    tc.shipping_email,
                                    tc.shipping_phone,
                                    tc.shipping_address,
                                    tc.shipping_province_id,
                                    tc.shipping_province,
                                    tc.shipping_city_id,
                                    tc.shipping_city,
                                    tc.shipping_districts_id,
                                    tc.shipping_district,
                                    tc.shipping_postal_code,
                                    tc.shipping_phone
                                FROM 
                                    transaction t
                                Left join transaction_payment tp on tp.transaction_id = t.transaction_id
                                Left join transaction_contact tc on tc.transaction_id = t.transaction_id
                                WHERE 
                                    t.status > 0                               
                                AND t.booking_code = " . $db->escape($order_code) . "
                        ";
                        $r = $db->query($q_row);
                        if ($r->count() > 0) {
                            foreach ($r as $row) {
                                $arr_shipping = array();
                                $arr_shipping['shipping_first_name'] = $row->shipping_first_name;
                                $arr_shipping['shipping_email'] = $row->shipping_email;
                                $arr_shipping['shipping_phone'] = $row->shipping_phone;
                                $arr_shipping['shipping_address'] = $row->shipping_address;
                                $arr_shipping['shipping_city_id'] = $row->shipping_city_id;
                                $arr_shipping['shipping_city'] = $row->shipping_city;
                                $arr_shipping['shipping_province_id'] = $row->shipping_province_id;
                                $arr_shipping['shipping_province'] = $row->shipping_province;
                                $arr_shipping['shipping_districts_id'] = $row->shipping_districts_id;
                                $arr_shipping['shipping_district'] = $row->shipping_district;
                                $arr_shipping['shipping_postal_code'] = $row->shipping_postal_code;
                                $arr_shipping['shipping_phone'] = $row->shipping_phone;
                                $arr_shippings[] = $arr_shipping;
                            }
                        }
                    }
                    $data['shipping'] = $arr_shippings;
                    if ($r_row->count() > 0) {
                        $q_row = "
                                SELECT 
                                    tc.billing_first_name,
                                    tc.billing_address,
                                    tc.billing_email,
                                    tc.billing_phone,
                                    tc.billing_province_id,
                                    tc.billing_province,
                                    tc.billing_city_id,
                                    tc.billing_city,
                                    tc.billing_districts_id,
                                    tc.billing_district,
                                    tc.billing_postal_code,
                                    tc.billing_phone
                                FROM 
                                    transaction t
                                Left join transaction_payment tp on tp.transaction_id = t.transaction_id
                                Left join transaction_contact tc on tc.transaction_id = t.transaction_id
                                WHERE 
                                    t.status > 0                               
                                AND t.booking_code = " . $db->escape($order_code) . "
                        ";
                        $r = $db->query($q_row);
                        if ($r->count() > 0) {
                            foreach ($r as $row) {
                                $arr_billing = array();
                                $arr_billing['billing_first_name'] = $row->billing_first_name;
                                $arr_billing['billing_email'] = $row->billing_email;
                                $arr_billing['billing_phone'] = $row->billing_phone;
                                $arr_billing['billing_address'] = $row->billing_address;
                                $arr_billing['billing_city_id'] = $row->billing_city_id;
                                $arr_billing['billing_city'] = $row->billing_city;
                                $arr_billing['billing_province_id'] = $row->billing_province_id;
                                $arr_billing['billing_province'] = $row->billing_province;
                                $arr_billing['billing_districts_id'] = $row->billing_districts_id;
                                $arr_billing['billing_district'] = $row->billing_district;
                                $arr_billing['billing_postal_code'] = $row->billing_postal_code;
                                $arr_billing['billing_phone'] = $row->billing_phone;
                                $arr_billings[] = $arr_billing;
                            }
                        }
                    }
                    $data['billing'] = $arr_billings;
                }
                else {
                    $err_code++;
                    $err_message = "Data Not Found";
                    $this->error->add($err_message, 2999);
                }
            }
            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
            );
            return $return;
        }
    }
    