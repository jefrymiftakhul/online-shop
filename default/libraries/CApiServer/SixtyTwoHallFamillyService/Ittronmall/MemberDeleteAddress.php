<?php

    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_MemberDeleteAddress extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();

            $ip = crequest::remote_address();            
            $member_address_id = carr::get($this->request, 'member_address_id');

            try {
                $order_rules = array(
                    'member_address_id' => array(
                        'rules' => array('required'),
                    ),
                );
                Helpers_Validation_Api::validate($order_rules, $this->request, 2);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error->code() == 0) {
                //Update Member Address
                try {
                    $db->update("member_address", array("status" => 0, "updated" => date("Y-m-d H:i:s"),
                        "updatedby" => $ip), array("member_address_id" => $member_address_id));
                }
                catch (Exception $ex) {
                    $err_code++;
                    $err_message = "Delete Address Not Succesfully";
                    $this->error->add($err_message, 2999);
                }
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
            );
            return $return;
        }

    }
    