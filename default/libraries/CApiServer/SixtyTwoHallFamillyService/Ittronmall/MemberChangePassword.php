<?php

    /**
     *
     * @author Khumbaka
     * @since  Jan 09, 2017
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_MemberChangePassword extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';

            $ip = crequest::remote_address();
            $date = date("Y-m-d H:i:s");
            $org_id = $this->session->get('org_id');
            $member_id = carr::get($this->request, 'member_id');
            $current_password = carr::get($this->request, 'current_password');
            $password = carr::get($this->request, 'password');
            $confirm_password = carr::get($this->request, 'confirm_password');

            try {
                $order_rules = array(
                    'member_id' => array(
                        'rules' => array('required'),
                    ),
                    'current_password' => array(
                        'rules' => array('required'),
                    ),
                    'password' => array(
                        'rules' => array('required'),
                    ),
                    'confirm_password' => array(
                        'rules' => array('required'),
                    ),
                );
                Helpers_Validation_Api::validate($order_rules, $this->request, 2);

                if ($confirm_password != $password) {
                    $this->error->add_default(10011);
                }
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error->code() == 0) {
                $is_exists = cdbutils::get_value('select count(*) from member where member_id = ' . $db->escape($member_id) . ' and password = md5(' . $db->escape($current_password) . ')');
                if ($is_exists == 0) {
                    $err_code++;
                    $err_message = "Your password didn't match with current password!";
                    $this->error->add($err_message, 2999);
                }
            }

            if ($this->error->code() == 0) {
                try {
                    $data_member = array(
                        'password' => md5($password),
                        'created' => $date,
                        'createdby' => $ip,
                    );
                    $r = $db->update('member', $data_member, array("member_id" => $member_id));
                }
                catch (Exception $ex) {
                    $this->error->add_default(10012) . $ex;
                }
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
            );
            return $return;
        }

    }
    