<?php

    /**
     *
     * @author Khumbaka
     * @since  Jan 09, 2017
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_MemberForgotPassword extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        private function get_random_str($type = 'alpha') {
            $length = 3;
            if ($type == 'alpha') {
                $characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            }
            if ($type == 'numeric') {
                $characters = "0123456789";
            }
            $string = "";
            $max = strlen($characters) - 1;
            for ($p = 0; $p < $length; $p++) {
                @$string .= $characters[mt_rand(0, $max)];
            }
            return $string;
        }

        private function generate_random_password() {
            $alpha = $this->get_random_str('alpha');
            $numeric = $this->get_random_str('numeric');
            return str_shuffle($alpha . $numeric);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();
            $org_id = $this->session->get('org_id');

            $email = carr::get($this->request, 'email');

            try {
                $order_rules = array(
                    'email' => array(
                        'rules' => array('required'),
                    ),
                );
                Helpers_Validation_Api::validate($order_rules, $this->request, 2);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error->code() == 0) {
//                $q = '
//                SELECT * FROM member m                 
//                WHERE m.status > 0
//                AND m.email = ' . $db->escape($email) . ' AND m.org_id = ' . $db->escape($org_id);
//                $r = cdbutils::get_row($q);
//                if ($r == null) {
//                    $this->error->add_default(2021);
//                }
                $result = compromall::forgot_password($email);
                if ($result['err_code'] > 0) {
                    $this->error->add($result['err_message'], 2999);
                }
            }

//            if ($this->error->code() == 0) {
//                $random_password = $this->generate_random_password();
//                $data_member = array(
//                    'password' => md5($random_password)
//                );
//                $where = array(
//                    'email' => $email
//                );
//                $db->update('member', $data_member, $where);
//
//                cmail::send_smtp($email, 'Forgot Password', 'Your new password is: ' . $random_password);
//            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    