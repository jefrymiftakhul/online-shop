<?php

class CApiServer_SixtyTwoHallFamillyService_Ittronmall_ProductReview extends CApiServer_SixtyTwoHallFamillyService {

    public function __construct($engine) {
        parent::__construct($engine);
    }

    public function execute() {
        $db = CDatabase::instance();

        $err_code = 0;
        $err_message = '';
        $data = array();

        $ip = crequest::remote_address();
        $date = date("Y-m-d H:i:s");
        $session_id = carr::get($this->request, 'session_id');
        $title = carr::get($this->request, 'title');
        $review = carr::get($this->request, 'review');
        $star = carr::get($this->request, 'star');
        $transaction_detail_id = carr::get($this->request, 'transaction_detail_id');
        $org_id = $this->session->get('org_id');

        try {
            $order_rules = array(
                'title' => array(
                    'rules' => array('required'),
                ),
                'review' => array(
                    'rules' => array('required'),
                ),
                'transaction_detail_id' => array(
                    'rules' => array('required'),
                ),
                'star' => array(
                    'rules' => array('required'),
                )
            );
            Helpers_Validation_Api::validate($order_rules, $this->request, 2);
        }
        catch (Helpers_Validation_Api_Exception $e) {
            $err_code++;
            $err_message = $e->getMessage();
            $this->error->add($err_message, 2999);
        }

        if ($this->error->code() == 0) {
            try {
                $db->begin();
                $data_member = array(
                    'title' => $title,
                    'review' => $review,
                    'star' => $star,
                    'transaction_detail_id' => $transaction_detail_id,
                    'created' => $date,
                    'createdby' => "",
                );
                $r = $db->insert('review', $data_member);
                $member_id = $r->insert_id();
            }
            catch (Exception $ex) {
                print_r($ex);
                die();
                $this->error->add_default(10012);
            }
            if ($this->error->code() == 0) {
                $db->commit();
            }
            else {
                $db->rollback();
            }
        }

        if ($this->error->code() == 0) {
            $data['session_id'] = $session_id;
        }

        $return = array(
            'err_code' => $this->error->code(),
            'err_message' => $this->error->get_err_message(),
            'data' => $data,
        );
        return $return;
    }

}
    