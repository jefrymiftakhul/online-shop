<?php

    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_MemberResendVerification extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();

            $ip = crequest::remote_address();
            $date = date("Y-m-d H:i:s");
            $session_id = carr::get($this->request, 'session_id');
            $email = carr::get($this->request, 'email');
            //$org_id = CF::org_id();
            $org_id = $this->session->get('org_id');

            try {
                $order_rules = array(
                    'email' => array(
                        'rules' => array('required', 'email'),
                    ),
                );
                Helpers_Validation_Api::validate($order_rules, $this->request, 2);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error->code() == 0) {
                $q = "SELECT member_id FROM member
                WHERE email = " . $db->escape($email) . " 
                and org_id = " . $db->escape($org_id) . "
                and status > 0";
                $r = cdbutils::get_row($q);
                if ($r != null) {
                    $member_id = $r->member_id;
                }
                else {                    
                    $this->error->add_default(2021);
                }
            }

            if ($this->error->code() == 0) {
                email::send('IMRegister', $member_id);  
                //SixtyTwoHallFamilyController::factory('api')->send_verification($member_id);
            }

            if ($this->error->code() == 0) {
                $data['session_id'] = $session_id;
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    