<?php

class CApiServer_SixtyTwoHallFamillyService_Ittronmall_ProductFavorite extends CApiServer_SixtyTwoHallFamillyService {

    public function __construct($engine) {
        parent::__construct($engine);
    }

    public function execute() {
        $db = CDatabase::instance();

        $err_code = 0;
        $err_message = '';
        $data = array();

        $ip = crequest::remote_address();
        $date = date("Y-m-d H:i:s");
        $session_id = carr::get($this->request, 'session_id');
        $member_id = carr::get($this->request, 'member_id');
        $product_id = carr::get($this->request, 'product_id');
        $org_id = $this->session->get('org_id');

        try {
            $order_rules = array(
                'member_id' => array(
                    'rules' => array('required'),
                ),
                'product_id' => array(
                    'rules' => array('required'),
                )
            );
            Helpers_Validation_Api::validate($order_rules, $this->request, 2);
        } catch (Helpers_Validation_Api_Exception $e) {
            $err_code++;
            $err_message = $e->getMessage();
            $this->error->add($err_message, 2999);
        }

        if ($this->error->code() == 0) {
            try {
                $db->begin();
                $favorite_number = cdbutils::get_row("
                    SELECT member_wishlist_id, status
                    FROM member_wishlist 
                    WHERE product_id = " . $db->escape($product_id) . "
                    AND member_id = " . $db->escape($member_id) . "
                    AND status > 0");
                if($favorite_number != null) {
                    $status = 0;
                    if($favorite_number->status == "1") {
                        $status = 1;
                    }
                    $data = array(
                        'status' => $status
                    );
                    $db->update('member_wishlist', array(
                        'status' => 0
                    ), array('member_id' => $member_id, 
                    'product_id' => $product_id)
                    );
                } else {
                    $data_favorite = array(
                        'member_id' => $member_id,
                        'product_id' => $product_id,
                        'created' => $date,
                        'createdby' => $member_id,
                    );
                    $r = $db->insert('member_wishlist', $data_favorite);
                    $member_wishlist_id = $r->insert_id();
                }
            } catch (Exception $ex) {
                $this->error->add_default(10012);
            }
            if ($this->error->code() == 0) {
                $db->commit();
            }
            else {
                $db->rollback();
            }
        }

        if ($this->error->code() == 0) {
            $data['session_id'] = $session_id;
        }

        $return = array(
            'err_code' => $this->error->code(),
            'err_message' => $this->error->get_err_message(),
            'data' => $data,
        );
        return $return;
    }

}
    