<?php

    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_MemberGetAddress extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data_all = array();

            $member_id = carr::get($this->request, 'member_id');
            $billing_member_address_id = carr::get($this->request, 'billing_member_address_id');
            $shipping_member_address_id = carr::get($this->request, 'shipping_member_address_id');

            try {
                $order_rules = array(
                    'member_id' => array(
                        'rules' => array('required'),
                    ),
                    'billing_member_address_id' => array(
                        'rules' => array('required'),
                    ),
                    'shipping_member_address_id' => array(
                        'rules' => array('required'),
                    ),
                );
                Helpers_Validation_Api::validate($order_rules, $this->request, 2);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error->code() == 0) {
                //return two array of address 
                //data {billing{1=>[member_addr_id,name,address,...],2,3,4}, shipping{1,2,3,4}}) 
                //order member_addr_id desc
                $q = '
                SELECT 
                        ma.member_address_id,ma.member_id,ma.type,ma.name,ma.phone,ma.address,ma.postal,
                        ma.email,ma.country_id,c.name as country_name,ma.province_id,p.name as province_name, 
                        ma.city_id, ci.name as city_name, ma.districts_id, d.name as district_name
                FROM 
                        member_address ma
                        left join country c on c.country_id = ma.country_id
                        left join province p on p.province_id = ma.province_id
                        left join city ci on ci.city_id = ma.city_id
                        left join districts d on d.districts_id = ma.districts_id
                WHERE ma.status > 0                
                AND ma.member_id = ' . $db->escape($member_id);
                if (strlen($billing_member_address_id) > 0) {
                    $q .= ' AND ma.member_address_id = ' . $db->escape($billing_member_address_id);
                }
                if (strlen($shipping_member_address_id) > 0) {
                    $q .= ' OR ma.member_address_id = ' . $db->escape($shipping_member_address_id);
                }                
                $r = $db->query($q);
                if ($r->count() > 0) {
                    foreach ($r as $row) {                        
                        $arr_data = array();
                        $arr_data['member_address_id'] = $row->member_address_id;
                        $arr_data['member_id'] = $row->member_id;
                        $arr_data['type'] = $row->type;
                        $arr_data['name'] = $row->name;
                        $arr_data['phone'] = $row->phone;
                        $arr_data['address'] = $row->address;
                        $arr_data['postal'] = $row->postal;
                        $arr_data['email'] = $row->email;
                        $arr_data['country_id'] = $row->country_id;
                        $arr_data['country_name'] = $row->country_name;
                        $arr_data['province_id'] = $row->province_id;
                        $arr_data['province_name'] = $row->province_name;
                        $arr_data['city_id'] = $row->city_id;
                        $arr_data['city_name'] = $row->city_name;
                        $arr_data['districts_id'] = $row->districts_id;
                        $arr_data['district_name'] = $row->district_name;
                        $data = array();
                        $data[$row->type] = $arr_data;
                        $data_all[] = $data;
                    }
                }
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data_all,
            );


            return $return;
        }

    }
    