<?php
    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_GetListOrder extends CApiServer_SixtyTwoHallFamillyService {
        public function __construct($engine) {
            parent::__construct($engine);
        }
        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';
            $data = array();
            $arr_details = array();
            $arr_shippings = array();
            $arr_billings = array();
            $org_id = $this->session->get('org_id');
            $member_id = carr::get($this->request, 'member_id');
            try {
                $order_rules = array(
                    'member_id' => array('required'),
                );
                Helpers_Validation_Api::validate($order_rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }
            if ($this->error->code() == 0) {
                $q = '
                SELECT 
                    t.booking_code,
                    t.date,
                    t.date_hold,
                    t.email,
                    t.order_status,
                    t.transaction_status,
                    t.shipping_status,
                    tp.payment_type,
                    tp.bank,
                    tp.account_number,
                    t.total_shipping as total,                    
                    (SELECT COUNT(*) FROM transaction_detail WHERE transaction_id = t.transaction_id) AS qty,
                    tp.total_charge,
                    tp.total_payment,                                                    
                    tp.payment_status                   
                FROM 
                    transaction t
            	Left join transaction_payment tp on tp.transaction_id = t.transaction_id
                WHERE 
                    t.status > 0            	  	
                AND t.member_id = ' . $db->escape($member_id) . ' 
                ORDER BY t.date DESC
                ';
                $r = $db->query($q);
                if ($r != null) {
                    foreach ($r as $k => $v) {
                        $booking_code = cobj::get($v, 'booking_code');
                        $transaction_email = cobj::get($v, 'email');
                        $date = cobj::get($v, 'date');
                        $order_status = cobj::get($v, 'order_status');
                        $payment_type = cobj::get($v, 'payment_type');
                        $bank = cobj::get($v, 'bank');
                        $account_number = cobj::get($v, 'account_number');
                        $total_item = cobj::get($v, 'qty');
                        $total_charge = cobj::get($v, 'total_charge');
                        $total_payment = cobj::get($v, 'total_payment');
                        $payment_status = cobj::get($v, 'payment_status');
                        $transaction_status = cobj::get($v, 'transaction_status');
                        $shipping_status = cobj::get($v, 'shipping_status');
                        $hold_date = cobj::get($v, 'date_hold');
                        $arr_data = array();
                        $arr_data['order_code'] = $booking_code;
                        $arr_data['transaction_email'] = $transaction_email;
                        $arr_data['date'] = $date;
                        $arr_data['hold_date'] = $hold_date;
                        $arr_data['order_status'] = $order_status;
                        $arr_data['transaction_status'] = $transaction_status;
                        $arr_data['shipping_status'] = $shipping_status;
                        $arr_data['total_item'] = $total_item;
                        $arr_data['total_charge'] = $total_charge;
                        $arr_data['total_payment'] = $total_payment;
                        $arr_data['payment_status'] = $payment_status;
                        $arr_data['payment_type'] = $payment_type;
                        $arr_data['bank'] = $bank;
                        $arr_data['account_number'] = $account_number;
                        $data[] = $arr_data;
                    }
                }
                else {
                    $err_code++;
                    $err_message = "Data Not Found";
                    $this->error->add($err_message, 2999);
                }
            }
            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
            );
            return $return;
        }
    }
    