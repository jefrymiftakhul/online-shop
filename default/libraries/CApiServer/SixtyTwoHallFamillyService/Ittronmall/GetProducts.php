<?php

    class CApiServer_SixtyTwoHallFamillyService_Ittronmall_GetProducts extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';
            $request = $this->request;
            $product_type = carr::get($request, 'product_type');
            $product_category_code = carr::get($request, 'product_category_code');
            $sku = carr::get($request, 'sku');
            $keyword = carr::get($request, 'keyword');
            $visibility = carr::get($request, 'visibility');
            $limit_start = carr::get($request, 'limit_start', 0);
            $limit_end = carr::get($request, 'limit_end', 10);
            $order_by = carr::get($request, 'order_by');
            $minprice = carr::get($request, 'min_price');
            $maxprice = carr::get($request, 'max_price');
            //$org_id = carr::get($request, 'org_id');
            $attributes = carr::get($request, 'attributes', array());
            //get filter data
            $org_id = isset($request['org_id']) ? $request['org_id'] :  $this->session->get('org_id');;
            $options = array();
            $options['product_type'] = $product_type;
            $options['org_id'] = $org_id;
            if ($product_category_code != null) {
                $product_category_id = cdbutils::get_value('select product_category_id from product_category where code=' . $db->escape($product_category_code) . " and org_id=" . $db->escape($org_id));
                $options['product_category_id'] = $product_category_id;
            }
            if ($keyword != null) {
                $options['search'] = $keyword;
            }
            if ($visibility != null) {
                $options['visibility'] = $visibility;
            }
//            $options['available'] = 1;
            //get attribute
            $list_attribute = product::get_attributes_array($options);
            //get count product
            $total_product = product::get_count($options);
            //get min price
            $options['min_price'] = $minprice;
            $min_price = product::get_min_price($options);
            //get max price
            $options['max_price'] = $maxprice;
            $max_price = product::get_max_price($options);
            if ($min_price == null) {
                $min_price = 0;
            }
            if ($max_price == null) {
                $max_price = 0;
            }
            $range_price = array(
                'min' => $min_price,
                'max' => $max_price,
            );
            //list sort by 
            $list_order_by = array(
                'diskon' => clang::__('Diskon'),
                'min_price' => clang::__('Termurah'),
                'max_price' => clang::__('Termahal'),
                'is_new' => clang::__('Terbaru'),
                'popular' => clang::__('Populer'),
                'A-Z' => clang::__('A-Z'),
                'Z-A' => clang::__('Z-A'),
            );
            $listorderby = array();
            foreach ($list_order_by as $kk => $vv) {
                $arr_order_by = array();
                $arr_order_by['order_id'] = $kk;
                $arr_order_by['order_name'] = $vv;
                $listorderby[] = $arr_order_by;
            }
            //limit
            $options['limit_start'] = $limit_start;
            $options['limit_end'] = $limit_end;
            //range_price
            $data_filters = array();
            foreach ($range_price as $kk => $vv) {
                $arr_data_filter = array(
                    'id' => $kk,
                    'value' => $vv,
                );
                $data_filters[] = $arr_data_filter;
            }
            $arr_filter = array(
                'filter_id' => 'range_price',
                'data' => $data_filters,
            );
            $filters[] = $arr_filter;
            //attribute
            $data_attribute = array();
            foreach ($list_attribute as $kk => $vv) {
                $attribute = array();
                foreach ($vv["attribute"] as $k1 => $v1) { 
                    $attribute[] = array(
                        "attribute_id" => $k1,
                        "attribute_key" => $v1["attribute_key"],
                        "attribute_url_key" => $v1["attribute_url_key"],
                        "file_path" => $v1["file_path"],
                    );
                }
                $arr_data_attribute = array(
                    'id' => $vv['attribute_category_id'],
                    'name' => $vv['attribute_category_name'],
                    'code' => $kk,
                    'value' => $attribute,
                );
                $data_attribute[] = $arr_data_attribute;
            }
            $arr_filter = array(
                'filter_id' => 'attribute',
                'data' => $data_attribute,
            );
            $filters[] = $arr_filter;
            //get product
            $options['sortby'] = $order_by;
            $arr_attribute = array();
            foreach ($attributes as $cat => $arr_attr) {
                if (is_array($arr_attr)) {
                    foreach ($arr_attr as $attr) {
                        $arr_attribute[$cat][] = $attr;
                    }
                }
            }
            $options['attributes'] = $arr_attribute;
            $product = product::array_result_product($options);
            foreach($product as $kk => $vv) {
                if($vv["detail_price"]["promo_text"] == "0.00") {
                    $product[$kk]["detail_price"]["promo_text"] = "0";
                }
            }
            
            $org_id_store = $this->session->get('org_id');
            $q = '
                select c.name 
                from org o 
                left join city c on o.city_id=c.city_id 
                where c.status=1 
                ';
            $city_origin = cdbutils::get_value($q);
            
            $data = array();
            $data['product_total'] = $total_product;
            $data['shipping_origin'] = $city_origin;
            $data['order_by'] = $listorderby;
            $data['filter'] = $filters;
            $data['product'] = $product;
            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
            );
            //$this->response = $return;
            return $return;
        }

    }
    