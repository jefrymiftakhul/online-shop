<?php
// include_once('Veritrans.php');
class CApiServer_SixtyTwoHallFamillyService_Ittronmall_Devices extends CApiServer_SixtyTwoHallFamillyService {

    public function __construct($engine) {
        parent::__construct($engine);
    }

    public function execute() {
        $db = CDatabase::instance();

        $err_code = 0;
        $err_message = '';
        $data = array();

        $ip = crequest::remote_address();
        $date = date("Y-m-d H:i:s");
        $manufacturer = carr::get($this->request, 'manufacturer');
        $model = carr::get($this->request, 'model');
        $serial = carr::get($this->request, 'serial');
        $version = carr::get($this->request, 'version');
        $platform = carr::get($this->request, 'platform');
        $registration_id = carr::get($this->request, 'registration_id');
        $org_id = CF::org_id();
        
        //selain lasvegas ambil dari session
        if($org_id!=620494 && $org_id!=621230 && $org_id!=621229){
            $org_id = $this->session->get('org_id');
        }
        
        $devices_id = "";
        $exist = cdbutils::get_row('SELECT * 
            FROM devices 
            WHERE manufacturer = "' . $manufacturer . '"
            AND model = "' . $model . '"
            AND platform = "' . $platform . '"
            AND serial = "' . $serial . '"
            AND version = "' . $version . '"');
        if (count($exist) == 0) {
            $data = array(
                "available" => "1",
                "cordova" => "NATIVE",
                "isVirtual" => "0",
                "manufacturer" => $manufacturer,
                "model" => $model,
                "platform" => $platform,
                "serial" => $serial,
                "uuid" => "",
                "version" => $version,
                "last_access_org_id" => $org_id,
                "last_access" => date("Y-m-d H:i:s"),
            );
            try {
                $r = $db->insert("devices", $data);
                $devices_id = $r->insert_id();
            }
            catch (Exception $ex) {
                $err_code++;
                $err_message = 'Failed devices';
            }
        } else {
            $devices_id = $exist->devices_id;
        }
        if($err_code == 0) {
            $cloud_messaging_id = "";
            $r = cdbutils::get_row("SELECT sender_id, api_access_key FROM org WHERE org_id = " . $db->escape($org_id));
            $available = cdbutils::get_row('SELECT * 
               FROM cloud_messaging 
               WHERE org_id = "' . $org_id . '"
               AND sender_id = "' . $r->sender_id . '"
               AND registration_id = "' . $registration_id . '"
               AND api_access_key = "' . $r->api_access_key . '"');
            if (count($available) == 0) {
                $data = array(
                    "org_id" => $org_id,
                    "devices_id" => $devices_id,
                    "sender_id" => $r->sender_id,
                    "registration_id" => $registration_id,
                    "api_access_key" => $r->api_access_key,
                    "last_request" => date("Y-m-d H:i:s"),
                );
                try {
                    $r = $db->insert("cloud_messaging", $data);
                    $cloud_messaging_id = $r->insert_id();
                }
                catch (Exception $ex) {
                    $err_code++;
                    $err_message = 'Failed Save Cloud Messaging';
                }
            } else {
                $cloud_messaging_id = $available->cloud_messaging_id;
            }
        }
        $data = array(
            "cloud_messaging_id" => $cloud_messaging_id,
        );
        $return = array(
            'err_code' => $err_code,
            'err_message' => $err_message,
            'data' => $data,
        );
        return $return;
    }

}
    