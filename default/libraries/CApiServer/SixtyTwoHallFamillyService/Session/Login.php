<?php

    /**
     *
     * @author Jevan
     * @since  Aug 5, 2015
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_Session_Login extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $this->session = CApiServer_Session::instance($this->api_server->get_type());
            $err_code = 0;
            $err_message = '';
            $ip_address = $this->api_server->get_req_ip_address();
            $auth_id = carr::get($this->request, 'auth_id');
            $this->session->set('auth_id', $auth_id);

            // validation auth_id with auth_type,org_id,org_code

            $q = "
				SELECT 
					* 
				FROM 
					org
				WHERE 
					status>0
					and md5(concat(auth_type,org_id,code)) = " . $db->escape($auth_id) . "
                ";
            $r = cdbutils::get_row($q);
            $data = array();
            if ($r != null) {
                $this->session->set('org_id', $r->org_id);
                $this->session->set('org_code', $r->code);
                $this->session->set('ip_address', $ip_address);
                $data['session_id'] = $this->session->get('session_id');
            }
            else {
                $err_code++;
                $err_message = 'Login Failed';
            }
            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
            );
            return $return;
        }

    }
    