<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 10, 2016
     * @license http://ittron.co.id ITtron Indonesia
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoDriver_SwitchDevice extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();

            $driver_id = carr::get($this->request, 'driver_id');
            $device_id = carr::get($this->request, 'device_id');

            try {
                $device_rules = array(
                    'driver_id' => array('required'),
                    'device_id' => array('required'),
                );
                Helpers_Validation_Api::validate($device_rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error()->code() == 0) {
                $q = 'select token from trevo_device where trevo_driver_id = ' . $db->escape($driver_id) . ' order by trevo_device_id desc';
                $r = $db->query($q);
                if ($r != null) {
                    foreach ($r as $k => $v) {
                        if($k > 0){
                            $token = array();
                            $token[] = cobj::get($v, 'token');
                            $message_notif = "";
                            $result_json = trevo::send_gcm_notify($token, 'Switch Device', $message_notif, 'logout', '', 'driver');
                            $result = json_decode($result_json, true);
                        }
                    }
                }
            }
            
            if ($this->error()->code() == 0) {
                $org_id = ccfg::get('org_id');
                if ($org_id) {
                    $org = org::get_org($org_id);
                }
                else {
                    $err_message = 'Data Merchant not Valid';
                    $this->error->add($err_message, 2999);
                }

                $data_device = array(
                    'status' => 0,
                    'trevo_driver_id' => null,
                    'updated' => date('Y-m-d H:i:s'),
                    'updatedby' => $org->code,
                );
                $where = array('trevo_driver_id' => $driver_id);
                $update = $db->update('trevo_device', $data_device, $where);

                $data_device = array(
                    'trevo_driver_id' => $driver_id,
                    'status' => 1,
                    'updated' => date('Y-m-d H:i:s'),
                    'updatedby' => $org->code,
                );
                $where = array('trevo_device_id' => $device_id);
                $db->update('trevo_device', $data_device, $where);
            }            

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    