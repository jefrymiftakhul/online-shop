<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 14, 2016
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoDriver_GetProfilePicture extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';
            $data = array();

            $driver_id = carr::get($this->request, 'driver_id');
            try {
                $user_rules = array(
                    'driver_id' => array('required', 'numeric')
                );
                Helpers_Validation_Api::validate($user_rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error()->code() == 0) {
                $q = "select d.trevo_driver_id, d.profile_pict_url 
                from trevo_driver d               
                where d.status > 0 and d.status_approval = 'APPROVED' and d.trevo_driver_id = " . $db->escape($driver_id);
                $r = cdbutils::get_row($q);
                if ($r != null) {
                    $r_driver_id = cobj::get($r, 'trevo_driver_id');
                    $r_profile_pict_url = cobj::get($r, 'profile_pict_url');
                    if (strlen($r_profile_pict_url) > 0) {
                        $data['driver_id'] = $r_driver_id;
                        $data['image_url'] = $r_profile_pict_url;
                    }
                    else {
                        $this->error()->add_default(2014);
                    }
                }
                else {
                    $this->error()->add_default(2003);
                }
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    