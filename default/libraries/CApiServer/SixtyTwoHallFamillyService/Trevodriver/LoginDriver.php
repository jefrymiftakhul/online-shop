<?php

    /**
     *
     * @author Khumbaka
     * @since  Oct 26, 2016
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoDriver_LoginDriver extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();

            $device_id = carr::get($this->request, 'device_id');
            $username = carr::get($this->request, 'username');
            $password = carr::get($this->request, 'password');
            $org_id = $this->session->get('org_id');

            try {
                $member_rules = array(
                    'device_id' => array('required', 'numeric'),
                    'username' => array('required'),
                    'password' => array('required'),
                );
                Helpers_Validation_Api::validate($member_rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error()->code() == 0) {
                $q = '
                SELECT * 
                FROM trevo_driver dr                 
                WHERE dr.status > 0
                AND dr.status_approval = "APPROVED"
                AND dr.username = ' . $db->escape($username) . ' AND dr.password = ' . $db->escape(md5($password)) . '
                AND dr.org_id = ' . $db->escape($org_id);
                $r = cdbutils::get_row($q);
                if ($r != null) {

                    $trevo_driver_id = cobj::get($r, 'trevo_driver_id');

                    $q = 'select * from trevo_device where status > 0 and trevo_driver_id = ' . $db->escape($trevo_driver_id);
                    $rd = cdbutils::get_row($q);
                    $rd_trevo_driver_id = cobj::get($rd, 'trevo_driver_id');
                    $rd_trevo_device_id = cobj::get($rd, 'trevo_device_id');
                    if ($rd_trevo_driver_id == null) {
                        $rd_trevo_device_id = $device_id;
                        $org_id = ccfg::get('org_id');
                        if ($org_id) {
                            $org = org::get_org($org_id);
                        }
                        else {
                            $err_message = 'Data Merchant not Valid';
                            $this->error->add($err_message, 2999);
                        }
                        $data_driver = array(
                            'status' => 1,
                            'trevo_driver_id' => $trevo_driver_id,
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => $org->code,
                        );
                        $db->update('trevo_device', $data_driver, array('trevo_device_id' => $device_id));
                    }
                    else {
                        if ($device_id != $rd_trevo_device_id) {
//                            $this->error()->add_default(2006);
                            $err_code = 2006;
                            $err_message = 'Wrong Device';
                        }
                    }

                    $id_card_number = cobj::get($r, 'id_card_number');
                    $gender = cobj::get($r, 'gender', '');
                    $name = cobj::get($r, 'name');
                    $username = cobj::get($r, 'username');
                    $address = cobj::get($r, 'address');
                    $city = cobj::get($r, 'city');
                    $phone_number = cobj::get($r, 'phone_number');
                    $birthdate = cobj::get($r, 'birthdate');
                    $vehicle_plate_number = cobj::get($r, 'vehicle_plate_number');
                    $latitude = cobj::get($r, 'latitude');
                    $longitude = cobj::get($r, 'longitude');

                    $data['trevo_driver_id'] = $trevo_driver_id;
                    $data['trevo_device_id'] = $rd_trevo_device_id;
                    $data['id_card_number'] = $id_card_number;
                    $data['gender'] = $gender;
                    $data['name'] = $name;
                    $data['username'] = $username;
                    $data['address'] = $address;
                    $data['city'] = $city;
                    $data['phone_number'] = $phone_number;
                    $data['birthdate'] = $birthdate;
                    $data['vehicle_plate_number'] = $vehicle_plate_number;
                    $data['latitude'] = $latitude;
                    $data['longitude'] = $longitude;

                    $this->session->set('trevo_driver_id', $trevo_driver_id);
                }
                else {
                    $this->error()->add_default(2012);
                }
            }

            $return = array(
                'err_code' => ($err_code > 0) ? $err_code : $this->error->code(),
                'err_message' => (strlen($err_message) > 0) ? $err_message : $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    