<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 7, 2016
     * @license http://ittron.co.id ITtron Indonesia
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoDriver_GetOrderDetail extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';
            $data = array();

            $transaction_id = carr::get($this->request, 'transaction_id');

            try {
                $order_rules = array(
                    'transaction_id' => array('required', 'numeric'),
                );
                Helpers_Validation_Api::validate($order_rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error()->code() == 0) {
                $q = "select tt.transaction_id
                , tt.order_status
                , t.code
                , t.booking_code
                , t.org_id
                , t.member_id
                , tt.trevo_driver_id
                , tt.eta_picked_duration
                , tt.cost
                , tt.created
                , tt.pickup_time
                , tt.pickup_location_address
                , tt.pickup_location_latitude
                , tt.pickup_location_longitude                
                , tt.arrival_address
                , tt.arrival_latitude
                , tt.arrival_longitude
                , tt.passenger_name
                , tt.passenger_phone_number
                , tt.review_rating
                , tt.review_comment
                , tt.order_date
                , tt.order_status
                from transaction t 
                left join transaction_trevo tt on t.transaction_id = tt.transaction_id                 
                where t.status > 0 and tt.status > 0
                and t.transaction_id = " . $db->escape($transaction_id);
                $r = cdbutils::get_row($q);

                if ($r != null) {
//                    $data['org_id'] = cobj::get($r, 'org_id');
//                    $data['order_status'] = cobj::get($r, 'order_status');
//                    $data['code'] = cobj::get($r, 'code');
//                    $data['booking_code'] = cobj::get($r, 'booking_code');
                    $data['transaction_id'] = cobj::get($r, 'transaction_id');
                    $data['member_id'] = cobj::get($r, 'member_id');
                    $data['driver_id'] = cobj::get($r, 'trevo_driver_id');
                    $data['duration'] = cobj::get($r, 'eta_picked_duration');
                    $data['total_price'] = ctransform::thousand_separator(cobj::get($r, 'cost'));
                    $data['created_date'] = cobj::get($r, 'created');
                    $data['pickup_time'] = cobj::get($r, 'pickup_time');
                    $data['pickup_location'] = array(
                        'address' => cobj::get($r, 'pickup_location_address'),
                        'latitude' => cobj::get($r, 'pickup_location_latitude'),
                        'longitude' => cobj::get($r, 'pickup_location_longitude'),
                    );
                    $data['destination_location'] = array(
                        'address' => cobj::get($r, 'arrival_address'),
                        'latitude' => cobj::get($r, 'arrival_latitude'),
                        'longitude' => cobj::get($r, 'arrival_longitude'),
                    );
                    $data['passenger_detail'] = array(
                        'name' => cobj::get($r, 'passenger_name'),
                        'phone_number' => cobj::get($r, 'passenger_phone_number'),
                    );
                    $data['order_date'] = date('d F Y, H:i', strtotime(cobj::get($r, 'order_date')));                    
                    $data['status'] = cobj::get($r, 'order_status');
                    $data['review_rating'] = cobj::get($r, 'review_rating');
                    $data['review_comment'] = cobj::get($r, 'review_comment');
                }
                else {
                    $this->error()->add_default(2004);
                }
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    