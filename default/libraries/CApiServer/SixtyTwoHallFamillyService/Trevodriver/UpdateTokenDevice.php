<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 08, 2016
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoDriver_UpdateTokenDevice extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();

            $device_id = carr::get($this->request, 'device_id');
            $token = carr::get($this->request, 'token');
            $model = carr::get($this->request, 'model');
            $os_version = carr::get($this->request, 'os_version');
            $driver_id = $this->request['driver_id'] = $this->session->get('trevo_driver_id');

            try {
                $device_rules = array(
                    'device_id' => array('required'),
                    'token' => array('required'),
                );
                Helpers_Validation_Api::validate($device_rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error()->code() == 0) {
                $org_id = ccfg::get('org_id');
                if ($org_id) {
                    $org = org::get_org($org_id);
                }
                else {
                    $err_message = 'Data Merchant not Valid';
                    $this->error->add($err_message, 2999);
                }

                $q = "select * 
                from trevo_device             
                where status > 0 and trevo_device_id = " . $db->escape($device_id) . " and trevo_driver_id = " . $db->escape($driver_id);
                $r = cdbutils::get_row($q);
                if ($r != null) {
                    $data_device = array(
                        'token' => $token,
                        'updated' => date('Y-m-d H:i:s'),
                        'updatedby' => $org->code,
                    );
                    $where = array('trevo_device_id' => $device_id);
                    $db->update('trevo_device', $data_device, $where);
                }
                else {
                    $data_device = array(
                        'token' => $token,
                        'model' => $model,
                        'os_version' => $os_version,
                        'created' => date('Y-m-d H:i:s'),
                        'createdby' => $org->code,
                    );
                    $res = $db->insert('trevo_device', $data_device);
                    $device_id = $res->insert_id();
                }
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    