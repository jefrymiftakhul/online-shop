<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 7, 2016
     * @license http://ittron.co.id ITtron Indonesia
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoDriver_SetCanceledOrder extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();

            $transaction_id = carr::get($this->request, 'transaction_id');
            $is_canceled = carr::get($this->request, 'is_canceled');
            $trevo_driver_id = $this->session->get('trevo_driver_id');

            try {
                $order_rules = array(
                    'transaction_id' => array('required', 'numeric'),
                    'is_canceled' => array('required', 'boolean'),
                );
                Helpers_Validation_Api::validate($order_rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error()->code() == 0) {
                $is_canceled = filter_var($is_canceled, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
                if ($is_canceled) {
                    $q = "select *
                        from transaction t
                        left join transaction_trevo tt on t.transaction_id = tt.transaction_id
                        where t.status > 0 
                        and tt.status > 0                        
                        and (tt.order_status = 'DELIVERED' or tt.order_status = 'PICK')
                        and t.transaction_id = " . $db->escape($transaction_id) . "
                        and tt.trevo_driver_id = " . $db->escape($trevo_driver_id);
                    $r = cdbutils::get_row($q);
                    if ($r != null) {
                        $org_id = ccfg::get('org_id');
                        if ($org_id) {
                            $org = org::get_org($org_id);
                        }
                        else {
                            $err_message = 'Data Merchant not Valid';
                            $this->error->add($err_message, 2999);
                        }
                        $data = array(
                            'order_status' => 'CANCELED',
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => $org->code,
                        );
                        $db->update('transaction_trevo', $data, array('transaction_trevo_id' => cobj::get($r, 'transaction_trevo_id')));

                        $data = array(
                            'transaction_status' => 'CANCELED',
                            'order_status' => 'CANCELED',
                            'payment_status' => 'CANCELED',
                            'shipping_status' => 'CANCELED',
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => $org->code,
                        );
                        $db->update('transaction', $data, array('transaction_id' => $transaction_id));

                        //push notif
                        $q = "select * from trevo_device where status > 0 and member_id = " . $db->escape(cobj::get($r, 'member_id'));
                        $rtdv = cdbutils::get_row($q);

                        $token = array();
                        $token[] = cobj::get($rtdv, 'token');
                        $message_notif = 'Pengemudi membatalkan pesanan anda';
                        $result_json = trevo::send_gcm_notify($token, 'Trevo', $message_notif, 'notification', '', 'member');
                        $result = json_decode($result_json, true);

                        $q = "select * from trevo_driver where status > 0 and trevo_driver_id = " . $db->escape($trevo_driver_id);
                        $rtd = cdbutils::get_row($q);
                        if ($rtd != null) {
                            $latitude = cobj::get($rtd, 'latitude');
                            $longitude = cobj::get($rtd, 'longitude');                                                        
                        }

                        $data_json = array(
                            'duration' => 0,
                            'latitude' => $latitude,
                            'longitude' => $longitude,
                        );
                        $json = json_encode($data_json);
                        $token = array();
                        $token[] = cobj::get($rtdv, 'token');
                        $message_notif = '';
                        $result_json = trevo::send_gcm_notify($token, 'Trevo', $message_notif, 'location', $json, 'member');
                        $result = json_decode($result_json, true);
//                        cdbg::var_dump($result);
                    }
                    else {
                        $this->error()->add_default(2004);
                    }
                }
                else {
                    $this->error()->add_default(2008);
                }
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    