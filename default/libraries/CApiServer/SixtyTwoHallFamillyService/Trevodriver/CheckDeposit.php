<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 17, 2016
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoDriver_CheckDeposit extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';
            $data = array();

            $trevo_driver_id = $this->request['driver_id'] = $this->session->get('trevo_driver_id');

            try {
                $device_rules = array(
                    'driver_id' => array('required', 'numeric')
                );
                Helpers_Validation_Api::validate($device_rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error()->code() == 0) {
                $q = "select m.trevo_driver_id, balance_idr from trevo_driver m
                left join trevo_driver_balance mb on m.trevo_driver_id = mb.trevo_driver_id
                where m.status > 0 and m.trevo_driver_id = " . $db->escape($trevo_driver_id);
                $r = cdbutils::get_row($q);

                if ($r != null) {
                    $r_trevo_driver_id = cobj::get($r, 'trevo_driver_id');
                    $r_balance_idr = cobj::get($r, 'balance_idr', 0);
                    $data['saldo'] = ctransform::thousand_separator($r_balance_idr);
                }
                else {
                    $this->error()->add_default(2001);
                }
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    