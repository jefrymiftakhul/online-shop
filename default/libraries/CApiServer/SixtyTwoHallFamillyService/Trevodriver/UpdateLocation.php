<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 02, 2016
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoDriver_UpdateLocation extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();

            $transaction_id = carr::get($this->request, 'transaction_id');
            $driver_id = $this->request['driver_id'] = $this->session->get('trevo_driver_id');
            $latitude = carr::get($this->request, 'latitude');
            $longitude = carr::get($this->request, 'longitude');

            $type = 'single';
            $datetime = null;
            if (is_array($latitude)) {
                $type = 'multi';
                $datetime = carr::get($this->request, 'datetime');
            }

            try {
                $history_rules = array(
                    'driver_id' => array('required'),
                    'latitude' => array('required'),
                    'longitude' => array('required')
                );
                Helpers_Validation_Api::validate($history_rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error()->code() == 0 && $type == 'single') {
                $q = "select * from trevo_driver where status > 0 and status_approval = 'APPROVED' and trevo_driver_id = " . $db->escape($driver_id);
                $r = cdbutils::get_row($q);
                if ($r != null) {
                    $org_id = ccfg::get('org_id');
                    if ($org_id) {
                        $org = org::get_org($org_id);
                    }
                    else {
                        $err_message = 'Data Merchant not Valid';
                        $this->error->add($err_message, 2999);
                    }

                    $data_history = array(
                        'latitude' => $latitude,
                        'longitude' => $longitude,
                        'updated' => date('Y-m-d H:i:s'),
                        'updatedby' => $org->code,
                    );
                    $where = array('trevo_driver_id' => $driver_id);
                    $db->update('trevo_driver', $data_history, $where);

                    $data_history = array(
                        'trevo_driver_id' => $driver_id,
                        'latitude' => $latitude,
                        'longitude' => $longitude,
                        'created' => date('Y-m-d H:i:s'),
                        'createdby' => $org->code,
                    );
                    $db->insert('trevo_driver_track_history', $data_history);

//                    pengecekan ada g transaction dengan driver_id=x dan status_transaction='PICK'
//                    jika ada, hitung durasi dan kirimkan melalui push notif
                    $q = "select * 
                    from transaction_trevo tt
                    left join transaction t on tt.transaction_id = t.transaction_id
                    left join trevo_device d on t.member_id = d.member_id
                    where tt.status > 0 and t.status > 0
                    and tt.order_status <> 'DELIVERED'
                    and tt.order_status <> 'CANCELED'
                    and tt.trevo_driver_id = " . $db->escape($driver_id);
                    $r = cdbutils::get_row($q);
                    if ($r != null) {
                        $pickup_location_latitude = cobj::get($r, 'pickup_location_latitude');
                        $pickup_location_longitude = cobj::get($r, 'pickup_location_longitude');
                        $distance = trevo::calculate_distance($latitude, $longitude, $pickup_location_latitude, $pickup_location_longitude);
                        $duration_num = $distance['duration'];

                        //push notif
                        $token = array();
                        $token[] = cobj::get($r, 'token');
                        $message_notif = '';
                        $data_json = array(
                            'duration' => $duration_num,
                            'latitude' => $latitude,
                            'longitude' => $longitude,
                        );
                        $json = json_encode($data_json);
                        $result_json = trevo::send_gcm_notify($token, 'Trevo', $message_notif, 'location', $json, 'member');
                        $result = json_decode($result_json, true);
                    }
                }
                else {
                    $this->error()->add_default(2003);
                }
            }
            if ($this->error()->code() == 0 && $type == 'multi') {
                $q = "select * from trevo_driver where status > 0 and status_approval = 'APPROVED' and trevo_driver_id = " . $db->escape($driver_id);
                $r = cdbutils::get_row($q);
                if ($r != null) {

                    $org_id = ccfg::get('org_id');
                    if ($org_id) {
                        $org = org::get_org($org_id);
                    }
                    else {
                        $err_message = 'Data Merchant not Valid';
                        $this->error->add($err_message, 2999);
                    }

                    foreach ($latitude as $k => $v) {
                        $data_history = array(
                            'latitude' => $latitude[$k],
                            'longitude' => $longitude[$k],
                            'updated' => $datetime[$k],
                            'updatedby' => $org->code,
                        );
                        $where = array('trevo_driver_id' => $driver_id);
                        $db->update('trevo_driver', $data_history, $where);

                        $data_history = array(
                            'trevo_driver_id' => $driver_id,
                            'latitude' => $latitude[$k],
                            'longitude' => $longitude[$k],
                            'created' => $datetime[$k],
                            'createdby' => $org->code,
                        );
                        $db->insert('trevo_driver_track_history', $data_history);

//                    pengecekan ada g transaction dengan driver_id=x dan status_transaction='PICK'
//                    jika ada, hitung durasi dan kirimkan melalui push notif
                        $q = "select * 
                        from transaction_trevo tt
                        left join transaction t on tt.transaction_id = t.transaction_id
                        left join trevo_device d on t.member_id = d.member_id
                        where tt.status > 0 and t.status > 0
                        and tt.order_status <> 'DELIVERED'
                        and tt.order_status <> 'CANCELED'
                        and tt.trevo_driver_id = " . $db->escape($driver_id);
                        $r = cdbutils::get_row($q);
                        if ($r != null) {
                            $pickup_location_latitude = cobj::get($r, 'pickup_location_latitude');
                            $pickup_location_longitude = cobj::get($r, 'pickup_location_longitude');
                            $distance = trevo::calculate_distance($latitude[$k], $longitude[$k], $pickup_location_latitude, $pickup_location_longitude);
                            $duration_num = $distance['duration'];

                            //push notif
                            $token = array();
                            $token[] = cobj::get($r, 'token');
                            $message_notif = '';
                            $data_json = array(
                                'duration' => $duration_num,
                                'latitude' => $latitude[$k],
                                'longitude' => $longitude[$k],
                            );
                            $json = json_encode($data_json);
                            $result_json = trevo::send_gcm_notify($token, 'Trevo', $message_notif, 'location', $json, 'member');
                            $result = json_decode($result_json, true);
                        }
                    }
                }
                else {
                    $this->error()->add_default(2003);
                }
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    