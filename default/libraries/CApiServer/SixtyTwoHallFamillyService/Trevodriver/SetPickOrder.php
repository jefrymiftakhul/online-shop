<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 7, 2016
     * @license http://ittron.co.id ITtron Indonesia
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoDriver_SetPickOrder extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();

            $transaction_id = carr::get($this->request, 'transaction_id');
            $is_pick = carr::get($this->request, 'is_pick');
            $trevo_driver_id = $this->session->get('trevo_driver_id');

            try {
                $order_rules = array(
                    'transaction_id' => array('required', 'numeric'),
                    'is_pick' => array('required', 'boolean'),
                );
                Helpers_Validation_Api::validate($order_rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error()->code() == 0) {
                $is_pick = filter_var($is_pick, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
                if ($is_pick) {
                    $is_valid = true;

                    $q = "select * 
                    from transaction_trevo 
                    where status > 0 
                    and order_status in ('PICK', 'PICKED')
                    and trevo_driver_id = " . $db->escape($trevo_driver_id);
                    $r = cdbutils::get_row($q);
                    if ($r != null) {
                        $is_valid = false;
                    }

                    if ($is_valid) {
                        $q = "select *
                        from transaction t
                        left join transaction_trevo tt on t.transaction_id = tt.transaction_id
                        where t.status > 0 
                        and tt.status > 0                        
                        and tt.order_status = 'PENDING'
                        and t.transaction_id = " . $db->escape($transaction_id);
                        $r = cdbutils::get_row($q);
                        if ($r != null) {
//                            $trevo_driver_id = cobj::get($r, 'trevo_driver_id');
                            $q = "select * from trevo_driver where status > 0 and trevo_driver_id = " . $db->escape($trevo_driver_id);
                            $rtd = cdbutils::get_row($q);
                            if ($rtd != null) {
                                $latitude = cobj::get($rtd, 'latitude');
                                $longitude = cobj::get($rtd, 'longitude');
                                $pickup_location_latitude = cobj::get($r, 'pickup_location_latitude');
                                $pickup_location_longitude = cobj::get($r, 'pickup_location_longitude');
                                $distance = trevo::calculate_distance($latitude, $longitude, $pickup_location_latitude, $pickup_location_longitude);
                                $duration_num = round($distance['duration']);
                                $eta_picked_duration = $duration_num;
                                $eta_picked_time = date('Y-m-d H:i:s', strtotime(date('YmdHis') . '+' . $duration_num . ' minute'));
                                $actual_pick_time = date('Y-m-d H:i:s');
                            }

                            $org_id = ccfg::get('org_id');
                            if ($org_id) {
                                $org = org::get_org($org_id);
                            }
                            else {
                                $err_message = 'Data Merchant not Valid';
                                $this->error->add($err_message, 2999);
                            }
                            $data = array(
                                'trevo_driver_id' => $trevo_driver_id,
                                'order_status' => 'PICK',
                                'eta_picked_time' => $eta_picked_time,
                                'eta_picked_duration' => $eta_picked_duration,
                                'actual_pick_time' => $actual_pick_time,
                                'updated' => date('Y-m-d H:i:s'),
                                'updatedby' => $org->code,
                            );
                            $db->update('transaction_trevo', $data, array('transaction_trevo_id' => cobj::get($r, 'transaction_trevo_id')));

                            //push notif
                            $q = "select * from trevo_device where status > 0 and member_id = " . $db->escape(cobj::get($r, 'member_id'));
                            $rtdv = cdbutils::get_row($q);

                            $token = array();
                            $token[] = cobj::get($rtdv, 'token');
                            $message_notif = 'Pengemudi sedang menuju tempat anda';
                            $result_json = trevo::send_gcm_notify($token, 'Trevo', $message_notif, 'notification', '', 'member');
                            $result = json_decode($result_json, true);
//                            cdbg::var_dump($result);

                            $data_json = array(
                                'duration' => $duration_num,
                                'latitude' => $latitude,
                                'longitude' => $longitude,
                            );
                            $json = json_encode($data_json);
                            $token = array();
                            $token[] = cobj::get($rtdv, 'token');
                            $message_notif = '';
                            $result_json = trevo::send_gcm_notify($token, 'Trevo', $message_notif, 'location', $json, 'member');
                            $result = json_decode($result_json, true);

                            //push notif order
                            $q = 'select * from trevo_push_notif where transaction_id = ' . $db->escape($transaction_id);
                            $r = $db->query($q);
                            if (count($r) > 0) {
                                foreach ($r as $k => $v) {
                                    $transaction_id = cobj::get($v, 'transaction_id');
                                    $trevo_driver_id = cobj::get($v, 'trevo_driver_id');
                                    $token = cobj::get($v, 'token');
                                    $push_type = cobj::get($v, 'push_type');
                                    $service_type = cobj::get($v, 'service_type');
                                    $token = array();
                                    $token[] = $token;
                                    $result_json = trevo::send_gcm_notify($token, 'Trevo', '', $push_type, '', $service_type);
                                    $result = json_decode($result_json, true);
                                }
                                $db->query('delete from trevo_push_notif where transaction_id = ' . $db->escape($transaction_id));
                            }
//                            cdbg::var_dump($result);                                                        
                        }
                        else {
                            $this->error()->add_default(2004);
                        }
                    }
                    else {
                        $this->error()->add_default(2017);
                    }
                }
                else {
                    $this->error()->add_default(2008);
                }
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    