<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 10, 2016
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoDriver_CheckVersionApp extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();

            $version_id = carr::get($this->request, 'version_id');
            $q = 'select * 
                from trevo_version_app v where status > 0
                order by trevo_version_app_id desc 
                limit 1';
            $r = cdbutils::get_row($q);
            if ($r != null) {
                $db_version_id = cobj::get($r, 'version_id');
                if ($version_id == $db_version_id) {
                    $this->session->set('version_id', $version_id);
                }
                else {
                    $this->error()->add_default(2010);
                }
            }
            else {
                $this->error()->add_default(2000);
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    