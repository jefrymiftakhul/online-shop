<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 18, 2016
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoDriver_PrivacyPolicy extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();

            if ($this->error()->code() == 0) {
                $q = "select * from trevo_privacy_policy order by trevo_privacy_policy_id desc limit 1";
                $r = cdbutils::get_row($q);
                if ($r != null) {
                    $data['privacy_policy'] = cobj::get($r, 'privacy_policy');
                }
                else {
                    $this->error()->add_default(2018);
                }
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    