<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 15, 2016
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoDriver_GetDriverById extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();

            $driver_id = carr::get($this->request, 'driver_id');
            $org_id = $this->session->get('org_id');

            try {
                $driver_rules = array(
                    'driver_id' => array('required', 'numeric'),
                );
                Helpers_Validation_Api::validate($driver_rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error()->code() == 0) {
                $q = '
                SELECT * FROM trevo_driver d                 
                WHERE d.status > 0
                AND d.status_approval = "APPROVED"
                AND d.trevo_driver_id = ' . $db->escape($driver_id) . '
                AND d.org_id = ' . $db->escape($org_id);
                $r = cdbutils::get_row($q);
                if ($r != null) {
                    $trevo_driver_id = cobj::get($r, 'trevo_driver_id');

                    $q = 'select * from trevo_device where status > 0 and trevo_driver_id = ' . $db->escape($trevo_driver_id);
                    $rd = cdbutils::get_row($q);
                    $rd_trevo_device_id = cobj::get($rd, 'trevo_device_id');

                    $id_card_number = cobj::get($r, 'id_card_number');
                    $name = cobj::get($r, 'name');
                    $username = cobj::get($r, 'username');
                    $gender = cobj::get($r, 'gender');
                    $address = cobj::get($r, 'address');
                    $city = cobj::get($r, 'city');
                    $phone_number = cobj::get($r, 'phone_number');
                    $birthdate = cobj::get($r, 'birthdate');
                    $vehicle_plate_number = cobj::get($r, 'vehicle_plate_number');
                    $latitude = cobj::get($r, 'latitude');
                    $longitude = cobj::get($r, 'longitude');

                    //review rating avg
                    $q = "select avg(review_rating) review_rating_avg
                    from transaction_trevo 
                    where status > 0
                    and order_status = 'DELIVERED' 
                    and trevo_driver_id = " . $db->escape($trevo_driver_id);
                    $r = cdbutils::get_row($q);
                    $review_rating_avg = 0;
                    if ($r != null) {
                        $review_rating_avg = number_format(cobj::get($r, 'review_rating_avg'), 2);                        
                    }
                    $data['trevo_driver_id'] = $trevo_driver_id;
                    $data['trevo_device_id'] = $rd_trevo_device_id;
                    $data['id_card_number'] = $id_card_number;
                    $data['name'] = $name;
                    $data['username'] = $username;
                    $data['gender'] = $gender;
                    $data['address'] = $address;
                    $data['city'] = $city;
                    $data['phone_number'] = $phone_number;
                    $data['birthdate'] = $birthdate;
                    $data['vehicle_plate_number'] = $vehicle_plate_number;
                    $data['latitude'] = $latitude;
                    $data['longitude'] = $longitude;
                    $data['review_rating_avg'] = $review_rating_avg;

                    $this->session->set('trevo_driver_id', $trevo_driver_id);
                }
                else {
                    $this->error()->add_default(2012);
                }
            }

            $return = array(
                'err_code' => ($err_code > 0) ? $err_code : $this->error->code(),
                'err_message' => (strlen($err_message) > 0) ? $err_message : $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    