<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 08, 2016
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoMember_CheckOrder extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';
            $data = array();

            $pickup_location = carr::get($this->request, 'pickup_location');
            $pickup_location_address = carr::get($pickup_location, 'address');
            $pickup_location_latitude = carr::get($pickup_location, 'latitude');
            $pickup_location_longitude = carr::get($pickup_location, 'longitude');

            $destination_location = carr::get($this->request, 'destination_location');
            $destination_location_address = carr::get($destination_location, 'address');
            $destination_location_latitude = carr::get($destination_location, 'latitude');
            $destination_location_longitude = carr::get($destination_location, 'longitude');

            $vehicle_type = carr::get($this->request, 'vehicle_type');
            
            try {
                $order_rules = array(
                    'pickup_location' => array(
                        'data_type' => 'array',
                        'field' => array(
                            'address' => array('rules' => array('required')),
                            'latitude' => array('rules' => array('required')),
                            'longitude' => array('rules' => array('required')),
                        ),
                    ),
                    'destination_location' => array(
                        'data_type' => 'array',
                        'field' => array(
                            'address' => array('rules' => array('required')),
                            'latitude' => array('rules' => array('required')),
                            'longitude' => array('rules' => array('required')),
                        ),
                    ),
                    'vehicle_type' => array(
                        'rules' => array('required'),
                    ),
                );
                Helpers_Validation_Api::validate($order_rules, $this->request, 2);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error()->code() == 0) {
                $org_id = ccfg::get('org_id');
                if ($org_id) {
                    $org = org::get_org($org_id);
                }
                else {
                    $err_message = 'Data Merchant not Valid';
                    $this->error->add($err_message, 2999);
                }
            }

            if ($this->error()->code() == 0) {
                $distance = trevo::calculate_distance($pickup_location_latitude, $pickup_location_longitude, $destination_location_latitude, $destination_location_longitude);
                $distance_num = $distance['distance'];
                $duration_num = $distance['duration'];
                $data = array();                
                $data['total_price'] = ctransform::thousand_separator(trevo::count_price($distance_num, $vehicle_type));
                $data['distance'] = $distance_num;
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    