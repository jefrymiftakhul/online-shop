<?php

    /**
     *
     * @author Khumbaka
     * @since  Oct 28, 2016
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoMember_AddPhoneNumber extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';
            $data = array();

            $member_id = $this->request['member_id'] = $this->session->get('member_id');
            $phone_number = carr::get($this->request, 'phone_number');

            try {
                $device_rules = array(
                    'member_id' => array('required', 'numeric'),
                    'phone_number' => array('required', array('phone' => 'numeric'))
                );
                Helpers_Validation_Api::validate($device_rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error()->code() == 0) {
                $q = "select * 
                from trevo_device 
                where status > 0 
                and member_id = " . $db->escape($member_id);
                $r = cdbutils::get_row($q);

                if ($r != null) {
                    $r_trevo_device_id = cobj::get($r, 'trevo_device_id');
                    $r_phone_number = cobj::get($r, 'phone_number');
                    if ($r_phone_number == null) {
                        $org_id = ccfg::get('org_id');
                        if ($org_id) {
                            $org = org::get_org($org_id);
                        }
                        else {
                            $err_message = 'Data Merchant not Valid';
                            $this->error->add($err_message, 2999);
                        }
                        $data_device = array(
                            'phone_number' => $phone_number,
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => $org->code,
                        );
                        $where = array('trevo_device_id' => $r_trevo_device_id);
                        $db->update('trevo_device', $data_device, $where);
                    }
                    else {
                        $this->error()->add_default(2007);
                    }
                }
                else {
                    $this->error()->add_default(2012);
                }
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    