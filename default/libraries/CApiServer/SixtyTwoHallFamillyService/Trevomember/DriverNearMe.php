<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 02, 2016
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoMember_DriverNearMe extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';
            $data = array();

//            $member_id = $this->request['member_id'] = $this->session->get('member_id');
            $latitude = carr::get($this->request, 'latitude');
            $longitude = carr::get($this->request, 'longitude');

            try {
                $driver_history_rules = array(
//                    'member_id' => array('required', 'numeric'),
                    'latitude' => array('required', 'numeric'),
                    'longitude' => array('required', 'numeric'),
                );
                Helpers_Validation_Api::validate($driver_history_rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error()->code() == 0) {
                $data = trevo::driver_near_me($latitude, $longitude);
                if (count($data) == 0) {                    
                    $this->error()->add_default(2013);
                }
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

        private function distance($lat1, $lon1, $lat2, $lon2, $unit = 'K') {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);
            if ($unit == "K") {
                return ($miles * 1.609344);
            }
            else if ($unit == "N") {
                return ($miles * 0.8684);
            }
            else {
                return $miles;
            }
        }

    }
    