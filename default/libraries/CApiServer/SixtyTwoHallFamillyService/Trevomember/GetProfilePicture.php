<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 14, 2016
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoMember_GetProfilePicture extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';
            $data = array();

            $member_id = carr::get($this->request, 'member_id');
            try {
                $member_rules = array(
                    'member_id' => array('required', 'numeric')
                );
                Helpers_Validation_Api::validate($member_rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error()->code() == 0) {
                $q = "select m.member_id, m.image_name 
                from member m                
                where m.status > 0 and m.member_id = " . $db->escape($member_id);
                $r = cdbutils::get_row($q);
                if ($r != null) {
                    $r_member_id = cobj::get($r, 'member_id');
                    $r_image_name = cobj::get($r, 'image_name');
                    if (!empty($r_image_name)) {
                        $image = image::get_image_url_front($r_image_name, 'view_profile');
                        $data['member_id'] = $r_member_id;
                        $data['image_url'] = $image;
                        $data['session']=$this->session->get_data();
                    }
                    else {
                        $this->error()->add_default(2014);
                    }
                }
                else {
                    $this->error()->add_default(2001);
                }
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    