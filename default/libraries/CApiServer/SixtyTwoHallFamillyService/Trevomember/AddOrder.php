<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 02, 2016
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoMember_AddOrder extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';
            $data = array();

            $pickup_location = carr::get($this->request, 'pickup_location');
            $pickup_location_address = carr::get($pickup_location, 'address');
            $pickup_location_latitude = carr::get($pickup_location, 'latitude');
            $pickup_location_longitude = carr::get($pickup_location, 'longitude');

            $destination_location = carr::get($this->request, 'destination_location');
            $destination_location_address = carr::get($destination_location, 'address');
            $destination_location_latitude = carr::get($destination_location, 'latitude');
            $destination_location_longitude = carr::get($destination_location, 'longitude');

            $member_name = carr::get($this->request, 'member_name');
            $member_phone = carr::get($this->request, 'member_phone');

            $vehicle_type = carr::get($this->request, 'vehicle_type');

            try {
                $order_rules = array(
                    'pickup_location' => array(
                        'data_type' => 'array',
                        'field' => array(
                            'address' => array('rules' => array('required')),
                            'latitude' => array('rules' => array('required')),
                            'longitude' => array('rules' => array('required')),
                        ),
                    ),
                    'destination_location' => array(
                        'data_type' => 'array',
                        'field' => array(
                            'address' => array('rules' => array('required')),
                            'latitude' => array('rules' => array('required')),
                            'longitude' => array('rules' => array('required')),
                        ),
                    ),
                    'member_name' => array(
                        'rules' => array('required'),
                    ),
                    'member_phone' => array(
                        'rules' => array('required'),
                    ),
                    'vehicle_type' => array(
                        'rules' => array('required'),
                    ),
                );
                Helpers_Validation_Api::validate($order_rules, $this->request, 2);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error()->code() == 0) {
                $org_id = ccfg::get('org_id');
                if ($org_id) {
                    $org = org::get_org($org_id);
                }
                else {
                    $err_message = 'Data Merchant not Valid';
                    $this->error->add($err_message, 2999);
                }
            }

            if ($this->error()->code() == 0) {
                try {

                    $distance = trevo::calculate_distance($pickup_location_latitude, $pickup_location_longitude, $destination_location_latitude, $destination_location_longitude);
                    $distance_num = $distance['distance'];
                    $duration_num = $distance['duration'];
                    $total_price = trevo::count_price($distance_num, $vehicle_type);

                    $db->begin();

                    $response = $this->session->get('response_LoginMember');

                    //insert transaction
                    $transaction_code_generate = generate_code::get_next_transaction_code();
                    $date_hold = date("Y-m-d H:i:s", strtotime("+2 hours", strtotime(date('Y-m-d H:i:s'))));

                    $data_transaction = array(
                        'org_id' => $org_id,
                        'org_parent_id' => $org->parent_id,
                        'member_id' => $this->session->get('member_id'),
                        'session_id' => $this->session->get('session_id'),
                        'api_session_id' => $this->session->get('session_id'),
                        'code' => $transaction_code_generate,
                        'email' => $response['data']['email'],
                        'date' => date('Y-m-d H:i:s'),
                        'date_hold' => $date_hold,
                        'type' => 'sell',
                        'product_type' => 'trevo',
                        'transaction_status' => 'PENDING',
                        'order_status' => 'PENDING',
                        'payment_status' => 'PENDING',
                        'shipping_status' => 'PENDING',
                        'vendor_nta' => 0,
                        'vendor_commission_value' => 0,
                        'vendor_sell_price' => 0,
                        'total_promo' => 0,
                        'ho_upselling' => 0,
                        'ho_sell_price' => 0,
                        'channel_commission_full' => 0,
                        'channel_commission_ho' => 0,
                        'channel_commission_share' => 0,
                        'channel_commission_share_ho' => 0,
                        'channel_commission_value' => 0,
                        'channel_sell_price' => $total_price,
                        'channel_profit' => 0,
                        'total_shipping' => 0,
                        'total_deal' => 0,
                        'created' => date('Y-m-d H:i:s'),
                        'createdby' => $org->code,
                        'updated' => date('Y-m-d H:i:s'),
                        'updatedby' => $org->code,
                    );

                    $r = $db->insert('transaction', $data_transaction);
                    $transaction_id = $r->insert_id();

                    //insert transaction trevo
                    $data_transaction_trevo = array(
                        'transaction_id' => $transaction_id,
//                        'trevo_driver_id' => $this->session->get('trevo_driver_id'),
                        'order_date' => date('Y-m-d H:i:s'),
                        'order_status' => 'PENDING',
                        'passenger_name' => $member_name,
                        'passenger_phone_number' => $member_phone,
                        'pickup_location_address' => $pickup_location_address,
                        'pickup_location_longitude' => $pickup_location_longitude,
                        'pickup_location_latitude' => $pickup_location_latitude,
                        'arrival_address' => $destination_location_address,
                        'arrival_longitude' => $destination_location_longitude,
                        'arrival_latitude' => $destination_location_latitude,
                        'cost' => $total_price,
                        'review_rating' => 0,
                        'review_comment' => '',
                        'vehicle_type' => $vehicle_type,
                        'created' => date('Y-m-d H:i:s'),
                        'createdby' => $org->code,
                        'updated' => date('Y-m-d H:i:s'),
                        'updatedby' => $org->code,
                    );
                    $db->insert('transaction_trevo', $data_transaction_trevo);
                    $this->session->set('transaction_code', $transaction_code_generate);
                    $this->session->set('payment_code', '');
                    $this->session->set('transaction_email', $response['data']['email']);
                    $this->session->set('transaction_id', $transaction_id);

                    // insert transaction session
                    $data_transaction_session = array(
                        "org_id" => $org_id,
                        "transaction_id" => $transaction_id,
                        "session_id" => $this->session->get('session_id'),
                        "api_session_id" => $this->session->get('session_id'),
                        "created" => date('Y-m-d H:i:s'),
                        "createdby" => $org->code,
                        "updated" => date('Y-m-d H:i:s'),
                        "updatedby" => $org->code,
                    );
                    $db->insert('transaction_session', $data_transaction_session);
                }
                catch (Exception $e) {
                    $err_code++;
                    $err_message = clang::__("system_fail") . $e->getMessage();
//                    cdbg::var_dump($err_message);
                    $this->error->add($err_message, 2999);
                }
                if ($err_code == 0) {
                    $db->commit();
                }
                else {
                    $db->rollback();
                }

                if ($this->error()->code() == 0) {
                    try {
                        member_balance::history('transaction', $transaction_id);

                        $data = array(
                            'transaction_status' => 'SUCCESS',
                            'order_status' => 'SUCCESS',
                            'payment_status' => 'SUCCESS',
                            'shipping_status' => 'PENDING',
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => $org->code,
                        );
                        $db->update('transaction', $data, array('transaction_id' => $transaction_id));
                    }
                    catch (Exception $ex) {

                        $data = array(
                            'transaction_status' => 'FAILED',
                            'order_status' => 'FAILED',
                            'payment_status' => 'FAILED',
                            'shipping_status' => 'PENDING',
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => $org->code,
                        );
                        $db->update('transaction', $data, array('transaction_id' => $transaction_id));

                        $err_code++;
                        $err_message = $ex->getMessage();
                        $this->error->add($err_message, 2999);
                    }
                }

                // update booking code & insert transaction history
                if ($this->error()->code() == 0) {
                    try {
                        $booking_code = generate_code::generate_booking_code($this->session->get('transaction_id'));
                        $data = array(
                            'booking_code' => $booking_code,
                        );
                        $db->update('transaction', $data, array('transaction_id' => $this->session->get('transaction_id')));
                        $this->session->set('booking_code', $booking_code);
                        $data_transaction_history['transaction_id'] = $this->session->get('transaction_id');
                        $data_transaction_history['ref_id'] = $this->session->get('transaction_id');
                        $db->insert('transaction_history', $data_transaction_history);
                    }
                    catch (Exception $exc) {
                        $err_code++;
                        $err_message = clang::__("system_fail") . $e->getMessage();
                        $this->error->add($err_message, 2999);
                    }
                }

                $data = array();
                $data['transaction_id'] = $this->session->get('transaction_id');
                $data['transaction_code'] = $this->session->get('transaction_code');
                $data['total_price'] = ctransform::thousand_separator($total_price);
                $data['distance'] = $distance_num;

                if (!trevo::push_notif($transaction_id)) {
                    $this->error()->add_default(2015);
                }
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    