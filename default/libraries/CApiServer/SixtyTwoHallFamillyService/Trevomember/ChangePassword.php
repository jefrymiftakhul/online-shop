<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 18, 2016
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoMember_ChangePassword extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        private function get_random_str($type = 'alpha') {
            $length = 3;
            if ($type == 'alpha') {
                $characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            }
            if ($type == 'numeric') {
                $characters = "0123456789";
            }
            $string = "";
            $max = strlen($characters) - 1;
            for ($p = 0; $p < $length; $p++) {
                @$string .= $characters[mt_rand(0, $max)];
            }
            return $string;
        }

        private function generate_random_password() {
            $alpha = $this->get_random_str('alpha');
            $numeric = $this->get_random_str('numeric');
            return str_shuffle($alpha . $numeric);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();

            $member_id = carr::get($this->request, 'member_id');
            $new_password = carr::get($this->request, 'new_password');

            try {
                $member_rules = array(
                    'member_id' => array('required'),
                    'new_password' => array('required'),
                );
                Helpers_Validation_Api::validate($member_rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error()->code() == 0) {
//                $random_password = $this->generate_random_password();
                $data_member = array(
                    'password' => md5($new_password)
                );
                $where = array(
                    'member_id' => $member_id
                );
                $db->update('member', $data_member, $where);
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    