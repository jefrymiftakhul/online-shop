<?php

    /**
     *
     * @author Khumbaka
     * @since  Oct 26, 2016
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoMember_LoginMember extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();

            $device_id = carr::get($this->request, 'device_id');
            $username = carr::get($this->request, 'username');
            $password = carr::get($this->request, 'password');
            $org_id = ccfg::get('org_id');

            try {
                $member_rules = array(
                    'device_id' => array('required', 'numeric'),
                    'username' => array('required'),
                    'password' => array('required'),
                );
                Helpers_Validation_Api::validate($member_rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error()->code() == 0) {
                $q = '
                SELECT * FROM member m                 
                WHERE m.status > 0
                AND m.email = ' . $db->escape($username) . ' AND m.password = ' . $db->escape(md5($password)) . '
                AND m.org_id = ' . $db->escape($org_id);
                $r = cdbutils::get_row($q);
                if ($r != null) {
                    $is_verified = cobj::get($r, 'is_verified');
                    if ($is_verified == '1') {

                        $member_id = cobj::get($r, 'member_id');

                        $this->session->set('member_id', $member_id);

                        $q = 'select * from trevo_device where status > 0 and member_id = ' . $db->escape($member_id);
                        $rd = cdbutils::get_row($q);
                        $rd_member_id = cobj::get($rd, 'member_id');
                        $rd_trevo_device_id = cobj::get($rd, 'trevo_device_id');
                        if ($rd_member_id == null) {
                            $rd_trevo_device_id = $device_id;
                        }
                        else {
                            if ($device_id != $rd_trevo_device_id) {
//                                $this->error()->add_default(2006);
                                $err_code = 2006;
                                $err_message = 'Wrong Device';
                            }
                        }
                        $org_id = ccfg::get('org_id');
                        if ($org_id) {
                            $org = org::get_org($org_id);
                        }
                        else {
                            $err_message = 'Data Merchant not Valid';
                            $this->error->add($err_message, 2999);
                        }
                        $data_member = array(
                            'status' => 1,
                            'member_id' => $member_id,
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => $org->code,
                        );
                        $db->update('trevo_device', $data_member, array('trevo_device_id' => $device_id));

                        $email = cobj::get($r, 'email');
                        $name = cobj::get($r, 'name');
                        $gender = cobj::get($r, 'gender');
                        $date_of_birth = cobj::get($r, 'date_of_birth');
                        $address = cobj::get($r, 'address');
                        $phone = cobj::get($r, 'phone');
                        $subscribe_product = cobj::get($r, 'is_subscribe');
                        $subscribe_service = cobj::get($r, 'is_subscribe_service');
                        $subscribe_gold = cobj::get($r, 'is_subscribe_gold');
                        $member_address = array();
                        $member_address_billing = array();
                        $member_address_shipping = array();

                        $q = "
						select
							c.name as country,
							p.name as province,
							ct.name as city,
							d.name as districts,
							ma.*
						from
							member_address as ma
							left join country as c on ma.country_id=c.country_id
							left join province as p on ma.province_id=p.province_id
							left join city as ct on ma.city_id=ct.city_id
							left join districts as d on ma.districts_id=d.districts_id
						where
							is_active>0
							and member_id=" . $db->escape($member_id) . "
					";
                        $r = $db->query($q);
                        if ($r->count() > 0) {
                            foreach ($r as $row) {
                                $arr = array();
                                $arr['id'] = $row->member_address_id;
                                $arr['name'] = $row->name;
                                $arr['phone'] = $row->phone;
                                $arr['address'] = $row->address;
                                $arr['postal'] = $row->postal;
                                $arr['email'] = $row->email;
                                $arr['country'] = $row->country;
                                $arr['province'] = $row->province;
                                $arr['city'] = $row->city;
                                $arr['districts'] = $row->districts;
                                if ($row->type == 'payment') {
                                    $member_address_billing[] = $arr;
                                }
                                if ($row->type == 'shipping') {
                                    $member_address_shipping[] = $arr;
                                }
                            }
                        }
                        $member_address['billing'] = $member_address_billing;
                        $member_address['shipping'] = $member_address_shipping;

                        $data['member_id'] = $member_id;
                        $data['trevo_device_id'] = $rd_trevo_device_id;
                        $data['email'] = $email;
                        $data['name'] = $name;
                        $data['gender'] = $gender;
                        $data['date_of_birth'] = $date_of_birth;
                        $data['address'] = $address;
                        $data['phone'] = $phone;
                        $data['subscribe_product'] = $subscribe_product;
                        $data['subscribe_service'] = $subscribe_service;
                        $data['subscribe_gold'] = $subscribe_gold;
                        $data['member_address'] = $member_address;
                        $data['trevo_phone_number'] =  cobj::get($rd, 'phone_number', '');
                        $data['session']=$this->session->get_data();
                        if ($r != null) {
                            $trevo_phone_number = cobj::get($rd, 'phone_number', '');
                            $data['trevo_phone_number'] = $trevo_phone_number;
                        }
                    }
                    else {
                        $this->error()->add_default(2011);
                    }
                }
                else {
                    $this->error()->add_default(2012);
                }
            }

            $return = array(
                'err_code' => ($err_code > 0) ? $err_code : $this->error->code(),
                'err_message' => (strlen($err_message) > 0) ? $err_message : $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    