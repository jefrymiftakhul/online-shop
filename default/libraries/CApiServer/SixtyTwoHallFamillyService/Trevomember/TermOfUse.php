<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 18, 2016
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoMember_TermOfUse extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data = array();

            if ($this->error()->code() == 0) {
                $q = "select * from trevo_term_of_use order by trevo_term_of_use_id desc limit 1";
                $r = cdbutils::get_row($q);
                if ($r != null) {
                    $data['term_of_use'] = cobj::get($r, 'term_of_use');
                }
                else {
                    $this->error()->add_default(2019);
                }
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    