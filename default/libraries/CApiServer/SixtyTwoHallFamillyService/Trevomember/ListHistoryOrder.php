<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 02, 2016
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoMember_ListHistoryOrder extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';
            $data['data'] = array();

            if ($this->error()->code() == 0) {
                $q = "select t.transaction_id, t.vehicle_type, coalesce(d.name, '') driver_name
                , t.pickup_location_address, t.pickup_location_latitude, t.pickup_location_longitude
                , t.arrival_address, t.arrival_latitude, t.arrival_longitude
                , t.cost, t.review_rating, t.review_comment, t.order_date, t.order_status
                from transaction_trevo t
                left join trevo_driver d on t.trevo_driver_id = d.trevo_driver_id
                left join transaction tr on tr.transaction_id = t.transaction_id
                where t.status > 0 and tr.status > 0 and tr.member_id = " . $db->escape($this->session->get('member_id'))
                . " order by t.transaction_trevo_id desc";
                $r = $db->query($q);
                if ($r != null) {
                    foreach ($r as $k => $v) {
                        $arr_data = array();
                        $arr_data['transaction_id'] = cobj::get($v, 'transaction_id');
                        $arr_data['vehicle_type'] = cobj::get($v, 'vehicle_type');
                        $arr_data['driver_name'] = cobj::get($v, 'driver_name');
                        $arr_data['pickup_location_address'] = cobj::get($v, 'pickup_location_address');
                        $arr_data['pickup_location_latitude'] = cobj::get($v, 'pickup_location_latitude');
                        $arr_data['pickup_location_longitude'] = cobj::get($v, 'pickup_location_longitude');
                        $arr_data['arrival_address'] = cobj::get($v, 'arrival_address');
                        $arr_data['arrival_latitude'] = cobj::get($v, 'arrival_latitude');
                        $arr_data['arrival_longitude'] = cobj::get($v, 'arrival_longitude');
                        $arr_data['cost'] = cobj::get($v, 'cost');
                        $arr_data['review_rating'] = cobj::get($v, 'review_rating');
                        $arr_data['review_comment'] = cobj::get($v, 'review_comment');
                        $arr_data['order_date'] = date('d F Y, H:i', strtotime(cobj::get($v, 'order_date')));
                        $arr_data['order_status'] = cobj::get($v, 'order_status');
                        array_push($data['data'], $arr_data);
                    }
                }
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    