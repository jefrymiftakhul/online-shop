<?php

    /**
     *
     * @author Khumbaka
     * @since  Dec 01, 2016
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoMember_CheckAddOrder extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';
            $data = array();

            $member_id = carr::get($this->request, 'member_id');

            try {
                $order_rules = array(
                    'member_id' => array(
                        'rules' => array('required', 'numeric'),
                    ),
                );
                Helpers_Validation_Api::validate($order_rules, $this->request, 2);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error()->code() == 0) {
                $org_id = ccfg::get('org_id');
                if ($org_id) {
                    $org = org::get_org($org_id);
                }
                else {
                    $err_message = 'Data Merchant not Valid';
                    $this->error->add($err_message, 2999);
                }
            }

            if ($this->error()->code() == 0) {
                $q = "select tt.order_status from transaction_trevo tt
                    left join transaction t on tt.transaction_id = t.transaction_id
                    where tt.status > 0 and t.status > 0 and t.member_id = " . $db->escape($member_id);
                $r = $db->query($q);
                if ($r != null) {
                    foreach ($r as $k => $v) {
                        $order_status = cobj::get($v, 'order_status');
                        if ($order_status == 'PICK' || $order_status == 'PICKED') {                            
                            $this->error()->add_default(2020);
                            break;
                        }
                    }
                }
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),                
            );
            return $return;
        }

    }
    