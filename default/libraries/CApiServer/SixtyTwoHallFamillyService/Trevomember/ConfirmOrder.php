<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 02, 2016
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoMember_ConfirmOrder extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';
            $data = array();

            $transaction_id = carr::get($this->request, 'transaction_id');
            $is_confirm = carr::get($this->request, 'is_confirm');

            try {
                $confirm_rules = array(
                    'transaction_id' => array('required', 'numeric'),
                    'is_confirm' => array('required', 'boolean'),
                );
                Helpers_Validation_Api::validate($confirm_rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error()->code() == 0) {
                $is_confirm = filter_var($is_confirm, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
                if (!$is_confirm) {
                    try {
                        $org_id = ccfg::get('org_id');
                        if ($org_id) {
                            $org = org::get_org($org_id);
                        }
                        else {
                            $err_message = 'Data Merchant not Valid';
                            $this->error->add($err_message, 2999);
                        }
                        $data = array(
                            'transaction_status' => 'FAILED',
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => $org->code,
                        );
                        $db->update('transaction', $data, array('transaction_id' => $this->session->get('transaction_id')));

                        $data = array(
                            'order_status' => 'CANCELED',
                            'updated' => date('Y-m-d H:i:s'),
                            'updatedby' => $org->code,
                        );
                        $db->update('transaction_trevo', $data, array('transaction_id' => $this->session->get('transaction_id')));
                    }
                    catch (Exception $ex) {
                        $err_code++;
                        $err_message = clang::__("system_fail") . $e->getMessage();
                        $this->error->add($err_message, 2999);
                    }
                }

                $data = array();                
                $data['transaction_id'] = $this->session->get('transaction_id');
                $data['transaction_code'] = $this->session->get('transaction_code');
                $data['total_price'] = 0;
                $data['distance'] = 0;
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    