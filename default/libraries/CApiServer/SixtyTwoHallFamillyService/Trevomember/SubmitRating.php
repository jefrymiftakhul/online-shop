<?php

    /**
     *
     * @author Khumbaka
     * @since  Nov 17, 2016
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_TrevoMember_SubmitRating extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';
            $data = array();

            $transaction_id = carr::get($this->request, 'transaction_id');
            $driver_id = carr::get($this->request, 'driver_id');
            $review_rating = carr::get($this->request, 'review_rating');
            $review_comment = carr::get($this->request, 'review_comment');

            try {
                $rating_rules = array(
                    'transaction_id' => array('required', 'numeric'),
                    'driver_id' => array('required', 'numeric'),
                    'review_rating' => array('required', 'numeric'),
                    'review_comment' => array('required'),
                );
                Helpers_Validation_Api::validate($rating_rules, $this->request);
            }
            catch (Helpers_Validation_Api_Exception $e) {
                $err_code++;
                $err_message = $e->getMessage();
                $this->error->add($err_message, 2999);
            }

            if ($this->error()->code() == 0) {
                if ($review_rating < 1 || $review_rating > 5) {
                    $err_code++;
                    $err_message = 'Rating should be number (1 - 5)';
                    $this->error->add($err_message, 2999);                    
                }
            }

            if ($this->error()->code() == 0) {
                try {
                    $org_id = ccfg::get('org_id');
                    if ($org_id) {
                        $org = org::get_org($org_id);
                    }
                    else {
                        $err_message = 'Data Merchant not Valid';
                        $this->error->add($err_message, 2999);
                    }
                    $data = array(
                        'review_rating' => $review_rating,
                        'review_comment' => $review_comment,
                        'updated' => date('Y-m-d H:i:s'),
                        'updatedby' => $org->code,
                    );
                    $where = array(
                        'transaction_id' => $transaction_id,
                        'trevo_driver_id' => $driver_id,
                        'order_status' => 'DELIVERED',
                    );
                    $db->update('transaction_trevo', $data, $where);

                    $q = 'select * from trevo_driver where status_approval = "APPROVED" and trevo_driver_id = ' . $db->escape($driver_id);
                    $r = cdbutils::get_row($q);
                    if ($r != null) {
                        $driver_name = cobj::get($r, 'name');
                    }

                    $data['transaction_id'] = $transaction_id;
                    $data['driver_id'] = $driver_id;
                    $data['driver_name'] = $driver_name;
                }
                catch (Exception $ex) {
                    $err_code = 2999;
                    $err_message = $ex->getMessage();
                    $this->error->add($err_message, 2999);
                }
            }

            $return = array(
                'err_code' => $this->error->code(),
                'err_message' => $this->error->get_err_message(),
                'data' => $data,
            );
            return $return;
        }

    }
    