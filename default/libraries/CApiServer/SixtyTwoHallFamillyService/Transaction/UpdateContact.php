<?php

    class CApiServer_SixtyTwoHallFamillyService_Transaction_UpdateContact extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';



            $ip = crequest::remote_address();
            $date = date("Y-m-d H:i:s");
            $data=array();
            //shipping
            $request=$this->request;
            $request_contact=carr::get($request,'contact_detail',array());

            $shipping=carr::get($request_contact,'shipping');
            $shipping_name = carr::get($shipping, 'name');
            $shipping_email = carr::get($shipping, 'email');
            $shipping_address = carr::get($shipping, 'address');
            $shipping_province = carr::get($shipping, 'province');
            $shipping_city = carr::get($shipping, 'city');
            $shipping_districts = carr::get($shipping, 'districts');
            $shipping_postal_code = carr::get($shipping, 'postal_code');
            $shipping_phone = carr::get($shipping, 'phone');
            //billing
            $billing=carr::get($request_contact,'billing');
            $billing_name = carr::get($billing, 'name');
            $billing_email = carr::get($billing, 'email');
            $billing_address = carr::get($billing, 'address');
            $billing_province = carr::get($billing, 'province');
            $billing_city = carr::get($billing, 'city');
            $billing_districts_id = carr::get($billing, 'districts_id');
            $billing_districts = carr::get($billing, 'districts');
            $billing_postal_code = carr::get($billing, 'postal_code');
            $billing_phone = carr::get($billing, 'phone');

            //Update Contact
            if($err_code==0){
                try {

                    $arr_contact = array();
                    $arr_billing_contact = array();
                    $arr_shipping_contact = array();
                    $arr_billing_name = explode(' ', trim($billing_name), 2);
                    $arr_shipping_name = explode(' ', trim($shipping_name), 2);
                    $billing_first_name = trim($billing_name);
                    $billing_last_name = '';
                    $shipping_first_name = trim($shipping_name);
                    $shipping_last_name = '';
                    $billing_province_id=cdbutils::get_value('select province_id from province where status>0 and name='.$db->escape($billing_province));
                    $billing_city_id=cdbutils::get_value('select city_id from city where status>0 and province_id='.$db->escape($billing_province_id).' and name='.$db->escape($billing_city));
                    $billing_districts_id=cdbutils::get_value('select districts_id from districts where status>0 and province_id='.$db->escape($billing_province_id).' and city_id='.$db->escape($billing_city_id).' and name='.$db->escape($billing_districts));
                    $shipping_province_id=  cdbutils::get_value('select province_id from province where status>0 and name='.$db->escape($shipping_province));
                    $shipping_city_id= cdbutils::get_value('select city_id from city where status>0 and province_id='.$db->escape($shipping_province_id).' and name='.$db->escape($shipping_city));
                    $shipping_districts_id=cdbutils::get_value('select districts_id from districts where status>0 and province_id='.$db->escape($shipping_province_id).' and city_id='.$db->escape($shipping_city_id).' and name='.$db->escape($shipping_districts));
                    if (count($arr_billing_name) == 2) {
                        $billing_first_name = $arr_billing_name[0];
                        $billing_last_name = $arr_billing_name[1];
                    }

                    if (count($arr_shipping_name) == 2) {
                        $shipping_first_name = $arr_shipping_name[0];
                        $shipping_last_name = $arr_shipping_name[1];
                    }
                    //handling billing
                    if ($err_code == 0) {
                        if (strlen($billing_first_name) > 20) {
                            $err_code++;
                            $error_message = 'Billing first name must be least than equal to 20 characters.';
                        }
                    }

                    if ($err_code == 0) {
                        if (strlen($billing_last_name) > 20) {
                            $err_code++;
                            $error_message = 'Billing last name must be least than equal to 20 characters.';
                        }
                    }

                    if ($err_code == 0) {
                        if (strlen($billing_address) > 200) {
                            $err_code++;
                            $error_message = 'Billing address must be least than equal to 200 characters.';
                        }
                    }

                    if ($err_code == 0) {
                        if (strlen($billing_city) > 100) {
                            $error++;
                            $error_message = 'Billing city must be least than equal to 100 characters.';
                        }
                    }

                    if ($err_code == 0) {
                        if (strlen($billing_postal_code) > 10) {
                            $err_code++;
                            $error_message = 'Billing postal code must be least than equal to 10 characters.';
                        }
                    }
                    if ($err_code == 0) {
                        if (strlen($billing_phone) > 19) {
                            $err_code++;
                            $error_message = 'Billing phone must be least than equal to 19 characters.';
                        }
                    }

                    //handling shipping
                    if ($err_code == 0) {
                        if (strlen($shipping_first_name) > 20) {
                            $err_code++;
                            $error_message = 'Shipping first name must be least than equal to 20 characters.';
                        }
                    }

                    if ($err_code == 0) {
                        if (strlen($shipping_last_name) > 20) {
                            $err_code++;
                            $error_message = 'Shipping last name must be least than equal to 20 characters.';
                        }
                    }

                    if ($err_code == 0) {
                        if (strlen($shipping_address) > 200) {
                            $error++;
                            $error_message = 'Shipping address must be least than equal to 200 characters.';
                        }
                    }

                    if ($err_code == 0) {
                        if (strlen($shipping_city) > 100) {
                            $err_code++;
                            $error_message = 'Shipping city must be least than equal to 100 characters.';
                        }
                    }

                    if ($err_code == 0) {
                        if (strlen($shipping_postal_code) > 10) {
                            $err_code++;
                            $error_message = 'Shipping postal code must be least than equal to 10 characters.';
                        }
                    }
                    if ($err_code == 0) {
                        if (strlen($shipping_phone) > 19) {
                            $err_code++;
                            $error_message = 'Shipping phone must be least than equal to 19 characters.';
                        }
                    }

                    if ($err_code == 0) {

                        $arr_billing_contact['first_name'] = $billing_first_name;
                        $arr_billing_contact['last_name'] = $billing_last_name;
                        $arr_billing_contact['address'] = $billing_address;
                        $arr_billing_contact['email'] = $billing_email;
                        $arr_billing_contact['province_id'] = $billing_province_id;
                        $arr_billing_contact['province'] = $billing_province;
                        $arr_billing_contact['city_id'] = $billing_city_id;
                        $arr_billing_contact['city'] = $billing_city;
                        $arr_billing_contact['district_id'] = $billing_districts_id;
                        $arr_billing_contact['district'] = $billing_districts;
                        $arr_billing_contact['postal_code'] = $billing_postal_code;
                        $arr_billing_contact['phone'] = $billing_phone;
                        $arr_billing_contact['country_code'] = 'IDN';

                        $arr_shipping_contact['first_name'] = $shipping_first_name;
                        $arr_shipping_contact['last_name'] = $shipping_last_name;
                        $arr_shipping_contact['address'] = $shipping_address;
                        $arr_shipping_contact['email'] = $shipping_email;
                        $arr_shipping_contact['province_id'] = $shipping_province_id;
                        $arr_shipping_contact['province'] = $shipping_province;
                        $arr_shipping_contact['city_id'] = $shipping_city_id;
                        $arr_shipping_contact['city'] = $shipping_city;
                        $arr_shipping_contact['district_id'] = $shipping_districts_id;
                        $arr_shipping_contact['district'] = $shipping_districts;
                        $arr_shipping_contact['postal_code'] = $shipping_postal_code;
                        $arr_shipping_contact['phone'] = $shipping_phone;
                        $arr_shipping_contact['country_code'] = 'IDN';


                        $arr_contact['first_name'] = $arr_billing_contact['first_name'];
                        $arr_contact['last_name'] = $arr_billing_contact['last_name'];
                        $arr_contact['email'] = $arr_billing_contact['email'];
                        $arr_contact['phone'] = $arr_billing_contact['phone'];
                        $arr_contact['billing_address'] = $arr_billing_contact;
                        $arr_contact['shipping_address'] = $arr_shipping_contact;
                        //$arr_contact['same_billing_shipping'] = $check_same;
                    }
                    $data['contact_detail']=$arr_contact;
                    $session_transaction=$this->session->get('transaction');
                    $session_transaction['contact_detail']=$arr_contact;
                    $this->session->set('transaction',$session_transaction);
                }
                catch (Exception $ex) {
                    $err_code++;
                    $err_message = "Update Contact Not Succesfully ".$ex;
                }
            }
            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data'=>$data,
            );

            return $return;
        }

    }
    