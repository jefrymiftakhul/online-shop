<?php

    class CApiServer_SixtyTwoHallFamillyService_Transaction_Payment extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $request=$this->request;
            $payment_type=carr::get($request,'payment_type');
            $payment_type_code=carr::get($request,'payment_type_code');
			$product_request=$this->session->get('request');
            $data=array();
            //checking
			if ($product_request == NULL) {
                if($err_code==0){
                    if($payment_type==null){
                        $err_code='1303';
                    }
                }
                if($err_code==0){
                    if($payment_type_code==null){
                        $err_code='1304';
                    }
                }
            }      
			
            if($err_code==0){
				$member_id=$this->session->get('member_id');
                $session_pg_id=$this->session->get('session_pg_id');
                $session_transaction=$this->session->get('transaction');
                
				$session_contact = carr::get($session_transaction, 'contact_detail');
                $transaction_email=carr::get($session_contact,'email');
                $session_pg = CApiClientSession::instance('PG',$session_pg_id);
                $session_pg_id=$session_pg->get('session_id');
                $session_pg->set('transaction', $session_transaction);
                $session_pg->set('payment_type', $payment_type);
                $session_pg->set('transaction_email', $transaction_email);
                $session_pg->set('transaction_from', 'MOBILE');

				if($product_request){
                    $session_pg->set('request', '1');
                }

				
                $options=array();
                $options['payment_type']=$payment_type;
                $options['payment_type_code']=$payment_type_code;
                if($payment_type=='bank_transfer'){
                    $file_payment_type=CF::get_file('data','data_payment');
                    $data_payment_type=array();
                    if(!file_exists($file_payment_type)){
                        $err_code=1203;
                        $err_message='File Data Payment Type Not Available';
                    }
                    if($err_code==0){
                        $data_payment_type= include $file_payment_type;                
                    }
                    $bank_transfer=carr::get($data_payment_type,'bank_transfer');
                    $product_code=carr::get($bank_transfer,'product_code');
                    $detail_bank=carr::get($bank_transfer,'detail_bank');
                    $bank=carr::get($detail_bank,$payment_type_code);
                    $session_pg->set('bank', carr::get($bank,'name'));
                    $session_pg->set('account_number', carr::get($bank,'account_number'));
                    if($err_code==0){
						if((!$member_id==null)||(strlen($member_id)>0)){
							$session = Session::instance();
							$session->set('member_id', $member_id);
						}
                        api_payment::insert_bank_transfer($options);
                    }
                    $data['order_code']=$session_pg->get('booking_code');
                    $data['email']=$session_pg->get('transaction_email');
                }
                $this->session->set('session_pg_id',$session_pg_id);
            }
            
            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
            );
            //cdbg::var_dump($data_all);
            return $return;
        }

    }
    