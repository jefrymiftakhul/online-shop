<?php

    class CApiServer_SixtyTwoHallFamillyService_Transaction_ConfirmBankTransfer extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';            
            $request=$this->request;
            $order_code=carr::get($request,'order_code');
            $email=carr::get($request,'email');
            $confirm_data=carr::get($request,'confirm_data');
            $org_id = $this->session->get('org_id');
            $data=array();
            //checking
            if($err_code==0){
                if($order_code==null){
                    $err_code='1401';
                }
            }
            if($err_code==0){
                if($email==null){
                    $err_code='1402';
                }
            }
            if($err_code==0){
                $transaction_id=transaction::get_transaction_id_from_booking($order_code,$email,$org_id);
                $data_transaction=transaction::get_transaction_all($transaction_id);
                $org_parent_id=carr::get($data_transaction,'org_parent_id');
                $transaction_payment_id=carr::get($data_transaction,'transaction_payment_id');
                $transaction=carr::get($data_transaction,'transaction');
                $product_type=carr::get($transaction,'page');
                $payment_type = carr::get($data_transaction, 'payment_type');
                $product_code = carr::get($data_transaction, 'product_code', 'IT');
                $session_pg_id=$this->session->get('session_pg_id');
                $session_pg = CApiClientSession::instance('PG',$session_pg_id);
                $session_pg_id=$session_pg->get_session_id();
				$api_client = CApiClient::instance('PG', 'IT');
                //$api_client->set_session_id($session_pg_id);
                //$api_client->set_session_type($product_type);
                foreach($data_transaction as $key=>$val){
                    $api_client->session()->set($key,$val);
                }
                $session_pg->set('confirm','1');
                
                
                // check from confirm data
                
                $bank_to=carr::get($data_transaction,'bank');
                $account_number=carr::get($data_transaction,'account_number');
                $bank_from=carr::get($confirm_data,'bank_from');
                $account_number_from=carr::get($confirm_data,'account_number_from');
                $description=carr::get($confirm_data,'description');
                $transfer_date=carr::get($confirm_data,'transfer_date');
                $arr_bank_transfer=array(
                    'bank_to'=>$bank_to,
                    'account_number_to'=>$account_number,
                    'bank_from'=>$bank_from,
                    'account_number_from'=>$account_number_from,
                    'description'=>$description,
                    'transfer_date'=>$transfer_date,
                );
                $session_pg->set('bank_transfer',$arr_bank_transfer);
                //login
                if ($err_code == 0) {
                    // Request Confirmation to api
                    $request = array(
                        'product_code' => $product_code,
                        'payment_type' => $payment_type,
                    );
                    $resp_login = $api_client->exec('login', $request);
                    
                    // get payment charge
                    if ($resp_login !== false) {
                        $request['currency_code'] = 'IDR';
                        $request['amount'] = carr::get($data_transaction,'total_amount_payment');
                        $resp_payment_charge = $api_client->exec('get_payment_charge', $request);
                        if ($resp_payment_charge === false) {
                            $err_code='1302';
                        }else{
                            $err_code=carr::get($resp_payment_charge,'err_code');
                        }
                    }
                    else {
                        $err_code='1301';
                    }
                }
                // payment
                if ($err_code == 0) {
                    $request['payment_info']['bank'] = $bank_to;
                    $request['payment_info']['bank_transfer'] = $arr_bank_transfer;
                    $request['action'] = 'confirm';
                    $request['back_url'] = '';
                    $resp_payment = $api_client->exec('payment', $request);
                    if ($resp_payment === false) {
                        $err_code='1305';
                    }else{
                        $err_code=carr::get($resp_payment,'err_code');
                    }
                }           
                if($err_code==0){
                    try {
                        $db->begin();
                        $data_update_transaction = array(
                            'transaction_status' => 'CONFIRMED',
                            'order_status' => 'CONFIRMED',
                            'payment_status' => 'CONFIRMED',
                        );

                        $db->update('transaction', $data_update_transaction, array('transaction_id' => $transaction_id));
                        
                        $data_update_transaction_payment = array(
                            'bank_from' => $bank_from,
                            'account_number_from' => $account_number_from,
                            'payment_status' => 'CONFIRMED',
                        );

                        $db->update('transaction_payment', $data_update_transaction_payment, array('transaction_payment_id' => $transaction_payment_id));
                        // insert transaction history
                        $data_transaction_history=array(
                            'org_id' => $org_id,
                            'org_parent_id' => $org_parent_id,
                            'transaction_id' => $transaction_id,
                            'date' => date('Y-m-d H:i:s'),
                            'transaction_status_before'=>'PENDING',
                            'transaction_status'=>'CONFIRMED',
                            'ref_table'=>'transaction',
                            'ref_id'=>$transaction_id,
                        );
                        $db->insert('transaction_history',$data_transaction_history);
                    }catch (Exception $ex) {
                        $err_code=1306;
                        $err_message = 'fail on confirm';
                    }
                    if($err_code==0){
                        $db->commit();
                        $data['order_code']=$order_code;
                        $data['email']=$email;
						
                    }else{
                        $db->rollback();
                    }
                }
            }
            if($err_code==0){
                SixtyTwoHallFamilyController::factory()->send_transaction('confirm', $data['order_code']);
            }
            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
            );
            return $return;
        }

    }
    