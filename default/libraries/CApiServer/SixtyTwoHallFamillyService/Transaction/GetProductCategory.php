<?php

    class CApiServer_SixtyTwoHallFamillyService_Transaction_GetProductCategory extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';

            $request = $this->request;
            $product_type = carr::get($request, 'product_type');
            if ($product_type == null) {
                $err_code++;
                $err_message = "Product Type not Found";
            }

            $product_category = product_category::get_product_category_menu($product_type);
            $data = $product_category;
            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
            );
            //$this->response = $return;
            return $return;
        }

    }
    