<?php

    /**
     *
     * @author Jevan
     * @since  Aug 5, 2015
     * @license http://piposystem.com Piposystem
     */
    class CApiServer_SixtyTwoHallFamillyService_Transaction_GetProducts extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $request = $this->request;
            $product_type = carr::get($request, 'product_type');
            $product_category_code = carr::get($request, 'product_category_code');
            $sku = carr::get($request, 'sku');
            $keyword = carr::get($request, 'keyword');
            $visibility = carr::get($request, 'visibility');
            $limit_start = carr::get($request, 'limit_start', 0);
            $limit_end = carr::get($request, 'limit_end', 10);
            $order_by = carr::get($request, 'order_by');
            $attributes = carr::get($request, 'attributes',array());
            //get filter data
            $options = array();
            $options['product_type'] = $product_type;
            if ($product_category_code != null) {
                $product_category_id = cdbutils::get_value('select product_category_id from product_category where code=' . $db->escape($product_category_code));
                $options['product_category_id'] = $product_category_id;
            }
            if ($keyword != null) {
                $options['search'] = $keyword;
            }
            if ($visibility != null) {
                $options['visibility'] = $visibility;
            }
//            $options['available'] = 1;
            //get attribute
            $list_attribute = product::get_attributes_array($options);

            //get count product
            $total_product = product::get_count($options);

            //get max and min price
            $min_price = product::get_min_price($options);
            $max_price = product::get_max_price($options);
            if ($min_price == null) {
                $min_price = 0;
            }
            if ($max_price == null) {
                $max_price = 0;
            }
            $range_price = array(
                'min' => $min_price,
                'max' => $max_price,
            );
            //list sort by 
            $list_order_by = array(
                'diskon' => clang::__('Diskon'),
                'min_price' => clang::__('Termurah'),
                'max_price' => clang::__('Termahal'),
                'is_new' => clang::__('Terbaru'),
                'popular' => clang::__('Popular'),
                'A-Z' => clang::__('A-Z'),
                'Z-A' => clang::__('Z-A'),
            );
            //limit
            $options['limit_start'] = $limit_start;
            $options['limit_end'] = $limit_end;
            $arr_filter = array();
            $arr_filter['order_by'] = $list_order_by;
            $arr_filter['range_price'] = $range_price;
            $arr_filter['attribute'] = $list_attribute;
            //get product
            $options['sortby']=$order_by;
            $arr_attribute=array();
            foreach($attributes as $cat=>$arr_attr){
                if(is_array($arr_attr)){
                    foreach($arr_attr as $attr){
                        $arr_attribute[$cat][]=$attr;
                    }
                }
            }
            $options['attributes'] = $arr_attribute;
            
            
            $product = product::array_result_product($options);
            $data = array();
            $data['product_total'] = $total_product;
            $data['filter'] = $arr_filter;
            $data['product'] = $product;

            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
            );
//            cdbg::var_dump($data);
            //$this->response = $return;
            return $return;
        }

    }
    