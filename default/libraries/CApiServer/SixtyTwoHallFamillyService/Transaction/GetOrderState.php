<?php

    class CApiServer_SixtyTwoHallFamillyService_Transaction_GetOrderState extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $data_all = array();
            $data = array();
            $contact = array();
            $arr_order = array();


            $member_id = $this->session->get('member_id');
            //$member_id = '53';            

            $q = '
                    select         
                        p.name as item_name,
                        td.qty,
                        p.sku,
                        p.sell_price
                    from
                        transaction t
                    left join transaction_detail td on td.transaction_id = t.transaction_id
                    left join product p on p.product_id = td.product_id
                    where
                        t.status>0
                    and t.member_id = ' . $db->escape($member_id) . '
              ';

            $r_row = $db->query($q);
           
            if ($r_row->count() > 0) {
                    foreach ($r_row as $row) {
                        $arr_items = array();
                        $arr_items['sku'] = $row->sku;
                        $arr_items['item_name'] = $row->item_name;
                        $arr_items['qty'] = $row->qty;
                        $arr_items['sell_price'] = $row->sell_price;
                        $arr_items['subtotal'] = $row->sell_price * $row->qty;
                        $arr_order[] = $arr_items;
                    }                   
                
                $data ['items'] = $arr_order;
                
                    $q_row = "
                                SELECT 
                                    tc.shipping_first_name,
                                    tc.shipping_address,
                                    tc.shipping_province_id,
                                    tc.shipping_province,
                                    tc.shipping_city_id,
                                    tc.shipping_city,
                                    tc.shipping_postal_code,
                                    tc.shipping_email,
                                    tc.shipping_phone
                                FROM 
                                    transaction t
                                Left join transaction_payment tp on tp.transaction_id = t.transaction_id
                                Left join transaction_contact tc on tc.transaction_id = t.transaction_id
                                WHERE 
                                    t.status > 0
                                AND t.member_id = " . $db->escape($member_id) . "
                        ";
                    $r = $db->query($q_row);
                    if ($r->count() > 0) {
                        foreach ($r as $row) {
                            $arr_shipping = array();
                            $arr_shipping['shipping_first_name'] = $row->shipping_first_name;
                            $arr_shipping['shipping_address'] = $row->shipping_address;
                            $arr_shipping['shipping_city_id'] = $row->shipping_city_id;
                            $arr_shipping['shipping_city'] = $row->shipping_city;
                            $arr_shipping['shipping_province_id'] = $row->shipping_province_id;
                            $arr_shipping['shipping_province'] = $row->shipping_province;
                            $arr_shipping['shipping_postal_code'] = $row->shipping_postal_code;
                            $arr_shipping['shipping_phone'] = $row->shipping_phone;
                            $arr_shipping['shipping_email'] = $row->shipping_email;
                            $arr_shippings[] = $arr_shipping;
                        }
                    }
                
                $datas['shipping'] = $arr_shippings;
                
                    $q_row = "
                                SELECT 
                                    tc.billing_first_name,
                                    tc.billing_address,
                                    tc.billing_province_id,
                                    tc.billing_province,
                                    tc.billing_city_id,
                                    tc.billing_city,
                                    tc.billing_postal_code,
                                    tc.billing_email,
                                    tc.billing_phone
                                FROM 
                                    transaction t
                                Left join transaction_payment tp on tp.transaction_id = t.transaction_id
                                Left join transaction_contact tc on tc.transaction_id = t.transaction_id
                                WHERE 
                                    t.status > 0
                                AND t.member_id = " . $db->escape($member_id) . "
                        ";
                    $r = $db->query($q_row);
                    if ($r->count() > 0) {
                        foreach ($r as $row) {
                            $arr_billing = array();
                            $arr_billing['billing_first_name'] = $row->billing_first_name;
                            $arr_billing['billing_address'] = $row->billing_address;
                            $arr_billing['billing_city_id'] = $row->billing_city_id;
                            $arr_billing['billing_city'] = $row->billing_city;
                            $arr_billing['billing_province_id'] = $row->billing_province_id;
                            $arr_billing['billing_province'] = $row->billing_province;
                            $arr_billing['billing_postal_code'] = $row->billing_postal_code;
                            $arr_billing['billing_phone'] = $row->billing_phone;
                            $arr_billing['billing_email'] = $row->billing_email;
                            $arr_billings[] = $arr_billing;
                        }
                    }
                
                $datas['billing'] = $arr_billings;
                $contact['contact'] = $datas;
                $data['charges'] = $contact;
                $data_all['data'] = $data;
            }else {
                $err_code++;
                $err_message = "No Booking";
            }

            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data_all,
            );            
            return $return;
        }

    }
    