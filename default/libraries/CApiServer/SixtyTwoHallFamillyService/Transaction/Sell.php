<?php

    class CApiServer_SixtyTwoHallFamillyService_Transaction_Sell extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $request=$this->request;
            $transaction=array();
            $transaction_request=carr::get($request,'transaction',array());
            $item=carr::get($transaction_request,'item',array());
            $item_payment=carr::get($transaction_request,'item_payment',array());
            $type=carr::get($transaction_request,'type');
            $product_type=carr::get($transaction_request,'product_type');
            $total_item=carr::get($transaction_request,'total_item');
            $total_shipping=carr::get($transaction_request,'total_shipping');
            $amount_payment=carr::get($transaction_request,'amount_payment');
            $type=carr::get($transaction_request,'type');
            $product_type=carr::get($transaction_request,'product_type');
            $product_request=carr::get($request,'product_request');
            $check_price=array();
            $check_stock=array();
            if($err_code==0){
                if(count($item)==0){
                    $err_code=1201;
                    $err_message='No Item for Transaction';
                }
            }
            if(!$product_request){
                if($err_code==0){
                    //check price
                    $check_price=  transaction_validation::check_price($item);
                    $err_code=$check_price['err_code'];
                }
                if($err_code==0){
                    $check_stock= transaction_validation::check_stock($item);
                    $err_code=$check_stock['err_code'];
                }
            }
            if($err_code==0){
                $transaction['item']=$item;
                $transaction['item_payment']=$item_payment;
                $transaction['total_item']=$total_item;
                $transaction['total_shipping']=$total_shipping;
                $transaction['amount_payment']=$amount_payment;
                $transaction['type']=$type;
                $transaction['page']=$product_type;
                
            }
            $this->session->set('transaction',$transaction);
            $this->session->set('check_price',$check_price);
            $this->session->set('check_stock',$check_stock);
            $this->session->set('request',$product_request);
            $data='';
            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
            );
            //cdbg::var_dump($data_all);
            return $return;
        }

    }
    