<?php

/**
 * Description of GetKinerjaProductCategory
 *
 * @author Ecko Santoso
 * @since 15 Nov 16
 */
class CApiServer_SixtyTwoHallFamillyService_Transaction_KP_GetProductCategory extends CApiServer_SixtyTwoHallFamillyService {
    
    public function __construct($engine) {
        parent::__construct($engine);
    }
    
    public function execute() {
        $err_code = 0;
        $data = array();
        $kinerja = ccfg::get('kinerjapay');
        $kinerja_services = carr::get($kinerja, 'services');
        
        $url_request = carr::get($kinerja, 'api_url'). carr::get($kinerja_services, 'product_category');
        $post_data = array(
            'merchantAppCode' => carr::get($kinerja, 'merchantAppCode'),
            'merchantAppPassword' => carr::get($kinerja, 'merchantAppPassword'),
            'merchantIpAddress' => carr::get($kinerja, 'merchantIpAddress'),
        );
        
//        cdbg::var_dump($url_request);
        
        $product_code  = $this->engine->get_product_code();
        $this->engine->set_url_request($url_request);
        $response = $this->engine->client_send_request("GetKinerjaProductCategory", cjson::encode($post_data), true)->get_response_array();
        
        $code = carr::get($response, 'code');
        $success = carr::get($response, 'success');
        $message = carr::get($response, 'message');
        $result = carr::get($response, 'result');
//        cphp::save_value($result, 'oke.php');
//        die();
        
        if (isset($_GET['debug'])) {
            cdbg::var_dump($result);
        }
        
        if ($success == '1') {
            $categories = carr::get($result, 'categories');
            if (count($categories) > 0) {
                $category = carr::get($categories, 'category');
                
                /*
                 * Manual add product parent I LOVE INDONESIA
                 * dari api kinerjapay tidak dapat response data I LOVE INDONESIA (per tanggal 06-12-2016)
                 */
                $category[] = array(
                    'type' => 'Product',
                    'name' => 'I LOVE INDONESIA',
                    'group' => 'I LOVE INDONESIA',
                    'parentID' => 0,
                    'id' => '102'
                );
                
                $recategory = array();
                foreach ($category as $k => $v) {
                    $parent_id = carr::get($v, 'parentID');
                    $group = carr::get($v, 'group');
                    if (is_array($parent_id)) {
                        if (count($parent_id) > 0) {
                            $parent_id = carr::get($parent_id, 0);
                        }
                        else {
                            $parent_id = NULL;
                        }
                    }
                    if (is_array($group)) {
                        if (count($group) > 0) {
                            $group = carr::get($group, 0);
                        }
                        else {
                            $group = NULL;
                        }
                    }
                    $arr_cat['category_id'] = carr::get($v, 'id');
                    $arr_cat['parent_id'] = $parent_id;
                    $arr_cat['name'] = carr::get($v, 'name');
                    $arr_cat['type'] = carr::get($v, 'type');
                    $arr_cat['group'] = $group;
                    $recategory[] = $arr_cat;
                }
                
                // process to format 62hall category
                $product_category = self::build_tree($recategory);
                
                $data = $product_category;
            }
        }
        else {
            $api_err_message = carr::get($message, 'message');
            if (is_array($api_err_message)) {
                $api_err_message = carr::get($api_err_message, 0);
            }
            $err_code = $code;
            $err_message = $api_err_message;
            $this->error()->add($err_message, $code);
        }
        
        $return = array(
            'err_code' => $err_code,
            'err_message' => $this->error->get_err_message(),
            'data' => $data,
        );
        if (isset($_GET['debug'])) {
            cdbg::var_dump($return);
        }
        $this->session->set('product_category_kp', $data);
        return $return;
    }
    
    public function build_tree(array $array, $parentId = 0){
        $return = array();
        
        foreach ($array as $k => $v) {
            $product_category_id = carr::get($v, 'category_id', 0);
            $name = carr::get($v, 'name');
            $parent_id = carr::get($v, 'parent_id');
            $group = carr::get($v, 'group');
            $type = carr::get($v, 'type');
            
            if (is_array($parent_id)) {
                if (count($parent_id) > 0) {
                    $parent_id = carr::get($parent_id, 0);
                }
                else {
                    $parent_id = NULL;
                }
            }
            if (is_array($group)) {
                if (count($group) > 0) {
                    $group = carr::get($group, 0);
                }
                else {
                    $group = NULL;
                }
            }
            
            $arr['category_id'] = $product_category_id;
            $arr['name'] = $name;            
            $arr['type'] = strtolower($type);            
            $arr['group'] = strtolower($group);
            $arr['vendor'] = 'KP';
            if ($parent_id == $parentId) {
                $children = self::build_tree($array, $product_category_id);
                if ($children) {
                    $arr['subnav'] = $children;
                }
                $return[] = $arr;
            }
        }
        return $return;
    }
}
