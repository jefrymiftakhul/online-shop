<?php

    class CApiServer_SixtyTwoHallFamillyService_Transaction_GetPaymentCharge extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();

            $err_code = 0;
            $err_message = '';
            $session_pg_id=$this->session->get('session_pg_id');
            $request=$this->request;
            $session = CApiClientSession::instance('PG', $session_pg_id);
            $payment_type=carr::get($request,'payment_type');
            $currency_code=carr::get($request,'currency_code');
            $amount=carr::get($request,'amount');
            $product_code='SG';
            
            if($payment_type=='bank_transfer'){
                $product_code='IT';
            }
            $session->set('payment_type',$payment_type);
            $session->set('currency_code',$currency_code);
            $session->set('amount',$amount);
            
            //do login
            $login_post_data=array(
                "product_code"=>$product_code,
                "payment_type"=>$payment_type,
            );
            $api_login = CApiClient::instance('PG', strtoupper($product_code))->exec('login', $login_post_data);
            $err_code_login=carr::get($api_login,'err_code');
            if(!$err_code_login==0){
                $err_code=1301;
                $err_message="Failed Login PG";
            }
            $api_get_payment_charge=null;
            if($err_code==0){
                $api_login_data=carr::get($api_login,'data',array());
                $payment_charge_post_data=array(
                    "product_code"=>$product_code,
                    "payment_type"=>$payment_type,
                    "currency_code"=>$currency_code,
                    "amount"=>$amount,
                );
                $api_get_payment_charge = CApiClient::instance('PG', strtoupper($product_code))->exec('get_payment_charge', $payment_charge_post_data);
            }
            if($api_get_payment_charge){
                $err_code_payment_charge=carr::get($api_get_payment_charge,'err_code');
                if(!$err_code_payment_charge==0){
                    $err_code=1302;
                    $err_message=carr::get($api_get_payment_charge,'err_message');
                }
            }
            if($err_code==0){
                $data=carr::get($api_get_payment_charge,'data');
            }
            
            
            $ip = crequest::remote_address();
            $date = date("Y-m-d H:i:s");
            
            //Update Contact
            if($err_code==0){
                try {
                    
                }
                catch (Exception $ex) {
                    $err_code++;
                    $err_message = "";
                }
            }
            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data'=>$data,
            );

            return $return;
        }

    }
    