<?php

    class CApiServer_SixtyTwoHallFamillyService_Transaction_GetProductDetail extends CApiServer_SixtyTwoHallFamillyService {

        public function __construct($engine) {
            parent::__construct($engine);
        }

        public function execute() {
            $db = CDatabase::instance();
            $err_code = 0;
            $err_message = '';
            $data = array();

            $request = $this->request;
            $product_type = carr::get($request, 'product_type');
            $sku = carr::get($request, 'sku');
            $product_type_id = cdbutils::get_value("select product_type_id from product_type where name=" . $db->escape($product_type));
            if ($product_type_id == null) {
                $err_code++;
                $err_message = "Product Type not Found";
            }
            $product_id = null;
            if ($err_code == 0) {
                $product_id = cdbutils::get_value("
					select 
						product_id 
					from 
						product 
					where 
						status>0 
						and product_type_id=" . $db->escape($product_type_id) . "
						and sku=" . $db->escape($sku) . "
				");
            }
            if ($product_id == null) {
                $err_code++;
                $err_message = "Product not Found";
            }
            if ($err_code == 0) {
                $data_detail_product = product::get_product($product_id);
                $data_detail_product = api_products_detail::convert_products_api($data_detail_product);
                $option_get_attribute_all['product_id']=$product_id;
                $data_attribute_category = product::get_attribute_all($option_get_attribute_all);
//                $data_attribute_category = product::get_attribute_category_all($product_id);
                $data_shipping_info = array();
                $arr_shipping_info = shipping::get_shipping_city_info($product_id);
                $data_shipping_info = $arr_shipping_info;
                if (count($data_shipping_info) == 0) {
                    $data_shipping_info['JAWA (2-14 hari kerja']['city']['all'] = true;
                    $data_shipping_info['LUAR JAWA (14-28 hari kerja)']['city']['all'] = true;
                }
//                                if(count($arr_shipping_info)>0){
//                                    foreach($arr_shipping_info as $key=>$val){
//                                        $shipping_info=array();
//                                        $shipping_info['province']=$key;
//                                        $city=carr::get($val,'city');
//                                        $shipping_info['city']=$city;
//                                        $data_shipping_info[]=$shipping_info;
//                                    }
//                                }else{
//                                    $shipping_info=array();
//                                    $shipping_info['province']='JAWA (2-14 hari kerja)';
//                                    $shipping_info['city']['all']=true;
//                                    $data_shipping_info[]=$shipping_info;
//
//                                    $shipping_info=array();
//                                    $shipping_info['province']='LUAR JAWA (14-28 hari kerja)';
//                                    $shipping_info['city']['all']=true;
//                                    $data_shipping_info[]=$shipping_info;
//                                }
                $data['product'] = $data_detail_product;
                $data['shipping_info'] = $data_shipping_info;
                $data['attribute'] = $data_attribute_category;
            }
            $return = array(
                'err_code' => $err_code,
                'err_message' => $err_message,
                'data' => $data,
            );

            //$this->response = $return;
            return $return;
        }

    }
    