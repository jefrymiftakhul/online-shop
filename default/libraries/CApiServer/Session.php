<?php

    class CApiServer_Session extends ApiSession {

        const __FOLDER = 'api_server';

        protected $type;
        protected static $server_instance;
        
        public function __construct($type, $session_id = null, $have_cookies = false) {
            $this->type = $type;
            parent::__construct($session_id, $have_cookies);
            $this->using_fopen=true;
        }

        public static function instance($type, $session_id = null, $have_cookies = false) {
            if (self::$instance == null) {
                self::$instance = array();
            }
            if (!isset(self::$instance[$type])) {
                self::$instance[$type] = new CApiServer_Session($type, $session_id, $have_cookies);
            }
            return self::$instance[$type];
        }

        public function callback_init() {
            $prefix = $this->type . date("YmdHis");
            $this->session_id = uniqid($prefix);

            $this->session_path = $this->default_session_path . self::__FOLDER . DS . $this->type . DS . date("Ymd")
                    . DS . date("H") . DS;
            if ($this->have_cookies == true) {
                $this->cookies_path .= date("Ymd") . $this->type . DS
                        . DS . date("H") . DS;
            }
        }

        public function load() {
            $this->callback_load();
            $filename = $this->session_path . $this->session_id . EXT;

            if (!file_exists($filename)) {
                $this->error()->add_default(1006);
            }

            $this->data = cphp::load_value($filename);
            if (!is_array($this->data)) {
                $this->data = array();
            }
            return $this;
        }

        public function callback_load() {
            $session_id = $this->get_session_id();
            $type_length = strlen($this->type);
            $date_ymd = substr($session_id, $type_length, 8);
            $data_h = substr($session_id, ($type_length) + 8, 2);

            $this->session_path = $this->default_session_path . self::__FOLDER . DS . $this->type . DS . $date_ymd
                    . DS . $data_h . DS;
            if ($this->have_cookies == true) {
                $this->cookies_path .= date("Ymd") . $this->type . DS
                        . DS . date("H") . DS;
            }
        }

    }
    