<?php
/**
 *
 * @author Khumbaka
 * @since  Aug 5, 2015
 * @license http://piposystem.com Piposystem
 */
class CApiServer_SixtyTwoHallFamilyEngine extends CApiServerEngine {
    protected $current_service = null;
    protected $service_engine = null;
    protected $product_code = null;
    protected $type = null;
    public function __construct(ApiServer $api_server) {
        parent::__construct($api_server);
        
        $curr_service = $this->api_server->get_current_service_name();
        $this->type = $this->api_server->get_type();
        $this->prefix_log_file_name = 'SERVER_';
        $session_id = null;
        if ($this->error()->code() == 0) {
            if (strtolower($this->type) != 'session') {
                $session_id = carr::get($this->request, 'session_id');
                if($this->type != 'trevodriver' && $this->type != 'trevomember' && $this->type != 'ittronmall') {                    
                    if (strlen($session_id) == 0) {
                        $this->error()->add_default(1007);
                    }
                }
            }
            // get last session
            try {
                if ($this->error()->code() == 0) {
                    $session = CApiServer_Session::instance('session', $session_id);
                    $this->set_session($session);
                    $this->api_server->set_session($session);
                }
            } catch (Exception $ex) {
                $this->error()->add_default(1008);
            }
        }
        // PREPARING FOR LOG
        $service_name = $api_server->get_current_service_name();
        $product_code = carr::get($this->request, 'product_code');
        if (strlen($product_code) > 0) {
            $this->product_code = $product_code;
        }
        $this->related_log_path = '';
        $org_code = 'DEF';
        if ($this->session instanceof ApiSession) {
            $session_id = $this->session->get_session_id();
            $org_code = $this->session->get('org_code');
        }
        if ($service_name == 'Login') {
            $this->related_log_path .= $this->type . DS . $service_name . DS
                    . date("Ymd") . DS . date("H");
        } else {
            $this->related_log_path .= $this->type . DS . $org_code . DS
                    . date("Ymd") . DS . date("H") . DS;
            if ($this->product_code != NULL) {
                $this->related_log_path .= $this->product_code . DS;
            }
            $this->related_log_path .= $session_id . DS;
        }
//            $session_id = null;
//            $prefix_file_name = 'SERVER_' . $session_id;
//            if ($session_id == null) {
//                $prefix_file_name = 'SERVER_' . 'DEF';
//            }
//            $this->log_filename = $prefix_file_name . '_' . $service_name . "_" . mt_rand(10000, 99999) . "_" . date("His") . '_';
        // END PREPARING FOR LOG
        if ($this->error()->code() == 0) {
            $this->session->set('URLTarget', curl::httpbase() . '/api/' . $this->type . '/');
        }
    }
    public function exec() {
        $curr_service = $this->api_server->get_current_service_name();
        if ($this->error()->code() == 0) {
            $this->session->set('type', $this->type);
            $service_engine_name = 'CApiServer_SixtyTwoHallFamillyService_' . ucfirst($this->type);
            if ($this->product_code != NULL) {
                $service_engine_name .= '_' . $this->product_code;
            }
            $service_engine_name .= '_' . $curr_service;
            
            $this->service_engine = new $service_engine_name($this);
            parent::exec($curr_service);
        }
        return $this->response;
    }
    public function Login() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetFavoriteList() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function ProductFavorite() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function VtWeb() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function Devices() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function VtWebCallback() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function ProductReview() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function MemberLogin() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function MemberRegister() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function MemberRegisterKinerjaMall() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetKinerjaPoin() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetKinerjaProductCategory() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function MemberGetProfile() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function MemberUpdateProfile() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function MemberGetAddress() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    
    public function MemberListAddress() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function MemberSetAddress() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }       
    public function MemberDeleteAddress() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function MemberChangePassword() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function MemberGetOrderList() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function Sell() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetOrderState() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function UpdateContact() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function Payment() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function ConfirmBankTransfer() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function MemberGetOrderDetail() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetAppInfo() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetBank() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetSlideShow() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetAdvertise() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetProductDeal() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetCountry() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetProvince() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetCity() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetDistricts() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetLocation() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetShippingInfo() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    
    public function GetShippingPrice() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetProducts() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetProductCategory() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetProductDetail() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetOrderStatus() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    
    public function GetOrder() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    
    public function GetListOrder() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetPaymentCharge() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    //TREVO MEMBER
    public function LoginMember() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetMemberById() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    //TREVO DRIVER
    public function LoginDriver() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetDriverById() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    //TREVO 
    public function CheckVersionApp() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function StoreDeviceDetail() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function UpdateTokenDevice() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function AddPhoneNumber() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function CheckDeposit() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function DriverNearMe() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function AddOrder() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    
    public function CheckAddOrder() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function CheckOrder() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetBookingOrder() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function ConfirmOrder() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetSummaryOrder() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function ListHistoryOrder() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    
    public function ListNewOrder() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetOrderDetail() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function UpdateLocation() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function SetPickOrder() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function SetPickedOrder() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function SetDeliveredOrder() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function SetCanceledOrder() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function SwitchDevice() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function GetProfilePicture() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    
    public function SubmitRating() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    
    public function ChangePassword() {
	if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    
    public function ForgotPassword() {
	if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    
    public function MemberForgotPassword() {
	if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function PrivacyPolicy() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function TermOfUse() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    public function client_send_request($service_name, $data = null, $exec = false, $obj_callback = null) {
        $using_session = true;
        $is_search = false;
//            if ($service_name == 'Login' || $service_name == 'GetAvailability' || $service_name == 'GetItineraryPrice') {
//                $using_session = false;
//            }
//            if ($service_name == 'GetAvailability') {
//                $is_search = true;
//            }
//            if ($using_session == true) {
//                $this->prepare_log($service_name);
//            }
        if ($using_session == true) {
            $this->session->set('Request_api_' . $service_name, $data);
        }
        $this->send_request($service_name, $data, $exec, $obj_callback);
        // processing data
        $this->response_array = json_decode($this->response, TRUE);
        if (json_last_error() > 0) {
            // do error
            $this->error()->add_default(10001, $this->response);
        }
        if ($this->error()->code() == 0) {
            $resp_error = carr::get($this->response_array, 'err_code');
            $resp_error_message = carr::get($this->response_array, 'err_message');
            if ($resp_error > 0) {
                $this->error()->add_default($resp_error, 'Error[' . $resp_error . ']' . $resp_error_message);
            }
        }
        if ($using_session == true) {
            $this->session->set('Response_api_' . $service_name, $this->response_array);
        }
        return $this;
    }
    public function get_response_array() {
        return $this->response_array;
    }
    public function get_product_code() {
        return $this->product_code;
    }
    
    //ITTRONMALL
    public function GetInfo() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    
    public function GetPaymentType() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    
    public function Order() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    
    public function MemberResendVerification() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    
    public function ComproRegister() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    
    public function ComproTransactionHistory() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    
    public function ComproCheckDomain() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    
    public function GetLogo() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    
    public function GetMessageList() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    
    public function GetMessageDetail() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    
    public function GetDepartmentMsg() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
    
    public function ReplyMessage() {
        if ($this->error()->code() == 0) {
            $response_exec = $this->service_engine->execute();
        }
        return $response_exec;
    }
}
