<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Expedition_SiCepat {
    
    protected $url;
    protected $api_key;
    
    public function __construct() {
        $this->url = "http://api.sicepat.com/customer/";
        //api key 62hall
        $this->api_key = "f6067351d69c2bc90c25d689030ace5f";
    }
    
    public function request($method_name, $options=array()) {
        
        $additional_param = '';
        
        if(count($options)>0){
            foreach ($options as $key => $val) {
                if(is_array($val))continue;
                $additional_param .= '&'.$key.'='.$val.'';
            }
        }        
        $url = $this->url.$method_name."?api-key=".$this->api_key.$additional_param;
        $curl = CCurl::factory($url);
        $curl->exec();
        $response = $curl->response();
        return $response;
    }
    
    public function origin() {
        $response = $this->request('origin');
        $data = json_decode($response,true);
        return $data;
    }
    public function destination() {
        $response = $this->request('destination');
        $data = json_decode($response,true);
        return $data;
    }
    
    public function get_cost($options=array()) {
        
        if(empty($options)) return null;
        $response = $this->request('tariff', $options);
        $data = json_decode($response,true);
        return $data;
    }
    
}