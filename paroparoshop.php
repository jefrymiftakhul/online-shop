<?php

$manager = CManager::instance();
$db = CDatabase::instance();
$org_id = CF::org_id();
$theme = CF::theme();

// $theme = 'ittronmall';
//cdbg::var_dump($theme);
//die;
$theme_ready = array('62hallmall','IttronmallShinjuku', 'Imbuilding', 'Imlasvegas','62hallfamily', 'Imparoparo');
$path = '';
if (strlen($theme) > 0) {
    if (in_array($theme, $theme_ready)) {
        $path = $theme .'/';
    }
}


$path_element = str_replace('/', '_', $path);

$manager->register_element('62hall-user', 'CElement_User');
$manager->register_element('62hall-search', 'CElement_Search');
$manager->register_element('62hall-menu', 'CElement_'.$path_element.'Menu');
$manager->register_element('62hall-advertise', 'CElement_Advertise');
$manager->register_element('62hall-advertise-responsive', 'CElement_AdvertiseResponsive');
$manager->register_element('62hall-slide-show', 'CElement_Slideshow');
$manager->register_element('62hall-slide-swiper', 'CElement_Slideswiper');
$manager->register_element('62hall-register', 'CElement_Register');
$manager->register_element('62hall-login', 'CElement_Login');
$manager->register_element('62hall-update-contact', 'CElement_UpdateContact');
$manager->register_element('62hall-invoice', 'CElement_Invoice');
$manager->register_element('62hall-filter', 'CElement_'.$path_element.'Filter');
$manager->register_element('62hall-paging', 'CElement_Paging');

// Product
$manager->register_element('62hall-product', 'CElement_'.$path_element.'Product_Product');
$manager->register_element('62hall-menu-product', 'CElement_Product_Menu');
$manager->register_element('62hall-menu-product-horizontal', 'CElement_Product_MenuHorizontal');
$manager->register_element('62hall-menu-product-category', 'CElement_Product_MenuProductResponsive');
$manager->register_element('62hall-menu-product-category-k24', 'CElement_Product_MenuProductResponsiveK24');
$manager->register_element('62hall-slide-product', 'CElement_'.$path_element.'Product_Slide');
$manager->register_element('62hall-swiper-product', 'CElement_'.$path_element.'Product_Swiper');
$manager->register_element('62hall-slide-product-responsive', 'CElement_Product_SlideResponsive');
$manager->register_element('62hall-overview-product', 'CElement_'.$path_element.'Product_Overview');
$manager->register_element('62hall-shipping-info', 'CElement_'.$path_element.'ShippingInfo');
$manager->register_element('62hall-subscribe', 'CElement_Product_Subscribe');

$manager->register_element('62hall-detail-product', 'CElement_'.$path_element.'Product_Detail');
$manager->register_element('62hall-detail-info', 'CElement_Product_DetailInfo');
$manager->register_element('62hall-galery-product', 'CElement_'.$path_element.'Product_Gallery');

$manager->register_element('62hall-category-menu', 'CElement_'.$path_element.'Product_CategoryMenu');
$manager->register_element('62hall-category-advertise', 'CElement_Product_CategoryAdvertise');

$manager->register_element('62hall-form-request', 'CElement_FormRequest');

$manager->register_element('62hall-product-cart', 'CElement_Product_ProductCart');

$manager->register_control('dropdown', 'CFormInput62HallFamilyInput_Dropdown');
$manager->register_control('date-dropdown', 'CFormInput62HallFamilyInput_DateDropdown');
$manager->register_control('date-hotel', 'CFormInput62HallFamilyInput_DateHotel');

// Luckyday
$manager->register_element('62hall-luckyday-product', 'CElement_Luckyday_Product');
$manager->register_element('62hall-luckyday-progress', 'CElement_Luckyday_ProgressBar');
$manager->register_element('62hall-luckyday-detail', 'CElement_Luckyday_Detail');
$manager->register_element('62hall-luckyday-winner', 'CElement_Luckyday_Winner');

// Modal 
$manager->register_element('62hall-modal-address', 'CElement_ModalAddress');

// Page
$manager->register_element('62hall-page-sidebar', 'CElement_Page_Sidebar');

// CMS Publication
$manager->register_element('62hall-cms-publication', 'CElement_Cms_Publication');

// CMS Testimonial
$manager->register_element('62hall-cms-testimonial', 'CElement_Cms_Testimonial');

// cms video

$manager->register_element('62hall-cms-video', 'CElement_Video');
$manager->register_element('62hall-cms-video-gallery', 'CElement_VideoGallery');

//select
$manager->register_control('province-select', 'CFormInput62HallFamilyInput_ProvinceSelect');
$manager->register_control('city-select', 'CFormInput62HallFamilyInput_CitySelect');
$manager->register_control('districts-select', 'CFormInput62HallFamilyInput_DistrictsSelect');
$manager->register_control('country-select', 'CFormInput62HallFamilyInput_CountrySelect');
$manager->register_control('bank-select', 'CFormInput62HallFamilyInput_BankSelect');
$manager->register_control('member-address-select', 'CFormInput62HallFamilyInput_MemberAddressSelect');
$manager->register_control('org-bank-select', 'CFormInput62HallFamilyInput_OrgBankSelect');
$manager->register_control('org-topup-nominal-select', 'CFormInput62HallFamilyInput_OrgTopupNominalSelect');
$manager->register_control('provider-select', 'CFormInput62HallFamilyInput_ProviderSelect');
$manager->register_control('product-select', 'CFormInput62HallFamilyInput_ProductSelect');

// Shinjuku Theme
$manager->register_element('shinjuku-deal', 'CElement_IttronmallShinjuku_SlideDeal');
$manager->register_element('fitgloss-deal', 'CElement_IttronmallFitgloss_SlideDeal');
$manager->register_element('lastmenit-deal', 'CElement_IttronmallLastmenit_SlideDeal');
$manager->register_element('lastmenit-slide-show', 'CElement_IttronmallLastmenit_SlideShow');
$manager->register_element('fitgloss-invoice', 'CElement_IttronmallFitgloss_Invoice');
$manager->register_element('fitgloss-product-detail', 'CElement_IttronmallFitgloss_Product_Detail');
$manager->register_element('goldenrama-deal', 'CElement_IttronmallGoldenrama_SlideDeal');
$manager->register_element('slide-menu', 'CElement_SlideMenu');

//Livemall Theme
$manager->register_element('livemall-menu', 'CElement_IttronmallLivemall_Menu');
$manager->register_element('livemall-slide', 'CElement_IttronmallLivemall_SlideResponsive');
$manager->register_element('livemall-deal', 'CElement_IttronmallLivemall_SlideDeal');
$manager->register_element('livemall-advertise-responsive', 'CElement_IttronmallLivemall_AdvertiseResponsive');

//prelove -control
$manager->register_control('62hall-modal', 'CFormInput62HallFamilyInput_Modal');
$manager->register_control('prelove-modal', 'CFormInput62HallFamilyInput_Modal');

// Product Slider
$manager->register_element('product-slider', 'CElement_ProductSlider');
// tree select
$manager->register_element('tree-select', 'CElement_Tree');


$app = CApp::instance();
$theme = CF::theme();
$vjspath = $theme.'/js/general';
if(!CView::exists($vjspath)) {
    $vjspath = 'js/general';
    
}
$vjs = CView::factory($vjspath);
$vjs_val = str_replace('<script>', '', $vjs->render());

$app->add_js($vjs_val);

$custom_js_file = CView::factory($path.'js/google_login');
$custom_js = str_replace('<script>', '', $custom_js_file->render());

$app->add_custom_js($custom_js);
